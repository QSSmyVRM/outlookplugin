﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using MyVrm.WebServices.Data;

namespace MyVrm.WebServices.Data
{
	internal class GetMCUProfilesRequest : ServiceRequestBase<GetMCUProfilesResponse>
	{
		internal string _MCUId;

        public GetMCUProfilesRequest(MyVrmService service) : base(service)
        {
        }

        #region Overrides of ServiceRequestBase

        internal override string GetXmlElementName()
        {
            return "GetMCUProfiles";
        }

        internal override string GetCommandName()
        {
            return "GetMCUProfiles";
        }

        internal override string GetResponseXmlElementName()
        {
			return "GetMCUProfiles";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            UserId.WriteToXml(writer, "UserID");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "MCUId", _MCUId);
        }

        #endregion
    }
	//
	internal class GetMCUProfilesResponse : ServiceResponse
	{
		private readonly List<MCUProfile> _mcuProfiles = new List<MCUProfile>();

		public ReadOnlyCollection<MCUProfile> MCUProfiles
		{
			get
			{
				return new ReadOnlyCollection<MCUProfile>(_mcuProfiles);
			}
		}

		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{
			base.ReadElementsFromXml(reader);
			_mcuProfiles.Clear();
			do
			{
				reader.Read();
				if (reader.IsStartElement(XmlNamespace.NotSpecified, "Profile"))
				{
					var profile = new MCUProfile();
					profile.LoadFromXml(reader, "Profile");
					_mcuProfiles.Add(profile);
				}
				else
				{
					reader.SkipCurrentElement();
				}
			} while (!reader.IsEndElement(XmlNamespace.NotSpecified, "GetMCUProfiles"));
		}
	}
	//
	public class MCUProfileId : ServiceId
	{
		//public static readonly MCUId Default;

		//static MCUId()
		//{
		//    Default = new MCUId("-1");
		//}

		internal MCUProfileId()
		{
		}

		public MCUProfileId(string id) : base(id)
		{
		}

		//public int IdAsInt()
		//{
		//    int i = 0;
		//    if( int.TryParse(Id, out i))
		//        return i;
		//    return -1;
		//}

		//public override string ToString()
		//{
		//    if (Id != null)
		//    {
		//        return Id;
		//    }
		//    return string.Empty;
		//}

		#region Overrides of ServiceId

		internal override string GetXmlElementName()
		{
			return "Id";
		}

		#endregion
	}

	public class MCUProfile : ComplexProperty
	{
		public static readonly MCUProfile MCUProfileNone = new MCUProfile("-1", Strings.TxtNone);
		public static readonly MCUProfile MCUProfileNoItems = new MCUProfile("0", Strings.TxtNoItems);

		public MCUProfile()
		{
		}

		internal MCUProfile(string id, string name)
		{
			Id = new MCUProfileId(id);
			Name = name;
		}
		public MCUProfile(MCUProfile value)
		{
			Id = new MCUProfileId(value.Id.Id);
			Name = value.Name;
		}

		//internal MCUProfile(MCUProfileId id, string name)
		//{
		//    Id = id;
		//    Name = name;
		//}

		public MCUProfileId Id { get; private set; }
		public string Name { get; set; }
		public string ProfileId
		{
			get { return Id.ToString(); }
			//set { Id = new ProfileId(value); }
		}

		public override object Clone()
		{
			MCUProfile copy = new MCUProfile();
			copy.Id = (MCUProfileId)Id.Clone();
			copy.Name = string.Copy(Name);
			return copy;
		}

		internal override void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
		{
			do
			{
				reader.Read();
				if (reader.LocalName == "Id")
				{
					Id = new MCUProfileId();
					Id.LoadFromXml(reader, "Id");
				}
				else if (reader.LocalName == "Name")
				{
					Name = reader.ReadElementValue(XmlNamespace.NotSpecified, "Name");
				}
				else
				{
					reader.SkipCurrentElement();
				}
			} while (!reader.IsEndElement(XmlNamespace.NotSpecified, xmlElementName));
		}
	}
}
