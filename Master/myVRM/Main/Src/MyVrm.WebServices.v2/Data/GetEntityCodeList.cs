﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
    
      
    internal class GetEntityCodeList : ServiceRequestBase<GetEntityCodeListResponse>
    {
        
        public enum CrossSilo
        {
            No = 0,
            Yes = 1
        }

        internal CrossSilo _isCrossSilo;
        public int languageid = 1;
        public GetEntityCodeList(MyVrmService service)
            : base(service)
        {
            _isCrossSilo = CrossSilo.No;
            languageid = 1;
            //int languageid = 1;
        }

        #region Overrides of ServiceRequestBase

        internal override string GetXmlElementName()
        {
            return "GetEntityCodes";
        }

        internal override string GetCommandName()
        {
            return "GetEntityCodes";
        }

        internal override string GetResponseXmlElementName()
        {
            return "NewDataSet";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            UserId.WriteToXml(writer, "userID");
            OrganizationId.WriteToXml(writer);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "languageid", languageid);
            //writer.WriteElementValue(XmlNamespace.NotSpecified, "isCrossSilo", _isCrossSilo);
        }

        #endregion
    }
    //
    public class GetEntityCodeListResponse : ServiceResponse
    {
        private readonly List<EntityCode> _EntityCodes = new List<EntityCode>();

        public /*ReadOnly*/Collection<EntityCode> EntityCodes
        {
            get
            {
                return new /*ReadOnly*/Collection<EntityCode>(_EntityCodes);
            }
        }

        internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
        {
            base.ReadElementsFromXml(reader);
            _EntityCodes.Clear();
            do
            {
                reader.Read();
                if (reader.IsStartElement(XmlNamespace.NotSpecified, "Table"))
                {
                    var user = new EntityCode();
                    user.LoadFromXml(reader, "Table");
                    _EntityCodes.Add(user);
                }
                else
                {
                    reader.SkipCurrentElement();
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "NewDataSet"));
        }
    }


    public class EntityCode : ComplexProperty
    {
        public int OptionID { get; private set; }
        public string Caption { get; set; }
       
        public override object Clone()
        {
            EntityCode copy = new EntityCode();
            copy.OptionID = OptionID;
            copy.Caption = string.Copy(Caption);
            return copy;
        }

        internal override void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
        {
            do
            {
                reader.Read();
                switch (reader.LocalName)
                {
                    case "OptionID":
                        {
                            OptionID = reader.ReadElementValue<int>(XmlNamespace.NotSpecified, "OptionID");
                            break;
                        }
                    case "Caption":
                        {
                            Caption = reader.ReadElementValue(XmlNamespace.NotSpecified, "Caption");
                            break;
                        }
                    default:
                        {
                            reader.SkipCurrentElement();
                            break;
                        }
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, xmlElementName));
        }
        
    }
}
