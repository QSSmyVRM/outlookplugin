﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Text;

namespace MyVrm.WebServices.Data
{
    internal class EndpointAddress
    {
        internal string Address { get; set; }
        internal string ConferenceCode { get; set; }
        internal string LeaderPin { get; set; }
   

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append(Address);
            if (!string.IsNullOrEmpty(ConferenceCode))
            {
                sb.Append("D");
                sb.Append(ConferenceCode);
            }
            if (!string.IsNullOrEmpty(LeaderPin))
            {
                sb.Append("+");
                sb.Append(LeaderPin);
            }
            return sb.ToString();
        }

		/// <summary>
		/// Possible input address formats:
		/// Dconf code+leader pin, "numerical data"Dconf code+leader pin	- conf code and leader pin are both defined
		/// Dconf code, "numerical data"Dconf code							- conf code is only defined
		/// +leader pin, "numerical data"+leader pin						- leader pin is only defined
		/// "numerical data", "", null										- none of conf code and leader pin is defined
		/// </summary>
		/// <param name="value">Address value</param>
		/// <returns>EndpointAddress</returns>
        internal static EndpointAddress Parse(string value)
        {
			const string confCodeMarker = "D";
			const string leaderPinMarker = "+";

			if (value == null)
				throw new ArgumentNullException("value");

			var address = new EndpointAddress();
			int length = value.Length;

			//If there is something to parse
			if (length > 0)
			{
				if (value.Contains(confCodeMarker) || value.Contains(leaderPinMarker))
				{
					int indxD = value.IndexOf(confCodeMarker); // -1 if not found
					int indxPlus = value.IndexOf(leaderPinMarker); // -1 if not found
					int iPartLen = -1;

					if (indxD != -1 && indxD < length - 1) //contains conf code
					{
						iPartLen = indxPlus != -1 ? indxPlus - indxD - 1 : length - indxD - 1;
						address.ConferenceCode = value.Substring(indxD + 1, iPartLen);
						address.Address = value.Substring(0, indxD); //assign first part of address
					}
					if (indxPlus != -1 && indxPlus < length - 1) //contains leader pin
					{
						iPartLen = length - indxPlus - 1;
						address.LeaderPin = value.Substring(indxPlus + 1, iPartLen);
						if (address.Address == null) //if address is not already set 
							address.Address = value.Substring(0, indxPlus); //assign first part of address
					}
				}
				else
					address.Address = value.Substring(0, length);
			}
			
        	return address;
        }
    }
}
