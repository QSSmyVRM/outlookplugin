﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    public class CateringWorkOrderMenu : ComplexProperty
    {
        public int ProviderMenuId { get; set; }
        public uint Quantity { get; set; }

		public override object Clone()//CopyTo(CateringWorkOrderMenu menu)
		{
			//if (menu == null)
			CateringWorkOrderMenu menu = new CateringWorkOrderMenu();
			menu.ProviderMenuId = ProviderMenuId;
			menu.Quantity = Quantity;
			return menu;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(this, obj))
				return true;

			CateringWorkOrderMenu menu = obj as CateringWorkOrderMenu;
			if (menu == null)
				return false;
			if (menu.ProviderMenuId != ProviderMenuId)
				return false;
			if (menu.Quantity != Quantity)
				return false;
			
			return true;
		}

        internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
        {
            switch(reader.LocalName)
            {
                case "ID":
                {
                    ProviderMenuId = reader.ReadElementValue<int>();
                    return true;
                }
                case "Quantity":
                {
                    Quantity = reader.ReadElementValue<uint>();
                    return true;
                }
            }
            return false;
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            base.WriteElementsToXml(writer);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "ID", ProviderMenuId);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "Quantity", Quantity);
        }

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
    }
}
