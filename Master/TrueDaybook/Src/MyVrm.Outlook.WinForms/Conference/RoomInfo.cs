﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Policy;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout;
using DevExpress.XtraLayout.Utils;
using MyVrm.WebServices.Data;
using MyVrm.WebServices.Maps;
using Image = System.Drawing.Image;

namespace MyVrm.Outlook.WinForms.Conference
{
	public partial class RoomInfo : MyVrm.Outlook.WinForms.Dialog
	{
		private RoomId _roomId;
		private Room _roomProfile;
		//private LocationDetails _info;
		public RoomInfo(RoomId roomId)
		{
			InitializeComponent();
			ApplyEnabled = false;
			ApplyVisible = false;
			OkVisible = false;
			CancelBnText = "Close";
			_roomId = roomId;
			GetInfo();
		}

		private void layoutControlGroup4_Click(object sender, EventArgs e)
		{

		}

		private void GetInfo()
		{
			//_info = new LocationDetails();//TEMPORARY COMMENTED!!!! MyVrmService.Service.GetLocationDetails(_roomId).LocationDetails;
			//PublicRoom stubRoom = PublicRoomList.GetStubData().Find(room => room.ID.ToString() == _roomId.Id);
			//if( stubRoom != null )
			//{
			//    _info.LocationName = stubRoom.Name;
			//    _info.StateName = stubRoom.State;
			//    pictureEdit1.Image = stubRoom.Picture;
			//    _info.Capacity = stubRoom.Capacity;
			//    _info.CitySuburb = stubRoom.Address;
			//}
			_roomProfile = MyVrmAddin.Instance.GetRoomFromCache(_roomId ) ;//, true);
			//Room roomProfile = MyVrmAddin.Instance.GetRoomFromCache(new RoomId(_roomId.Id));
				//MyVrmService.Service.GetRoom(new RoomId(_roomId.Id));
			if (_roomProfile != null)
			{
				//_info.LocationName = roomProfile.Name;
				//_info.StateName = roomProfile.State;
				pictureEdit1.Image = null;
				if (_roomProfile.RoomImages.Count > 0)
				{
					//pictureEdit1.Image = Image.FromStream(roomProfile.RoomImages[0].AsStream());

					using (var imageStream = _roomProfile.RoomImages[0].AsStream())
					{
						pictureEdit1.Image = Image.FromStream(imageStream);
					}
				}
				//pictureEdit1.Image = roomProfile.RoomImages != null ? selRoom.RoomImages[0] : null; //??
				else
				{
					//_roomProfile.PublicRoomData.ImgLink = "http://yandex.st/www/1.301/yaru/i/logo.png";
					if (!string.IsNullOrEmpty(_roomProfile.PublicRoomData.ImgLink))
					{
						try
						{
							MyVrmAddin.TraceSource.TraceInfoEx("Get {0}", _roomProfile.PublicRoomData.ImgLink);
							WebRequest req = WebRequest.Create(_roomProfile.PublicRoomData.ImgLink);
							using (WebResponse response = req.GetResponse())
							{
								if (response != null)
								{
									using (Stream stream = response.GetResponseStream())
									{
										if (stream != null)
										{
											pictureEdit1.Image = Image.FromStream(stream);
											stream.Close();
										}
									}
								}
							}
							MyVrmAddin.TraceSource.TraceInfoEx("Received {0}", _roomProfile.PublicRoomData.ImgLink);
						}
						catch (Exception exception)
						{
							MyVrmAddin.TraceSource.TraceWarning("{0} download failed: {1}", _roomProfile.PublicRoomData.ImgLink, exception.Message);
						}
					}
				}
			}
		}

		private static void HideGroup(LayoutControlGroup layoutGroup)
		{
			//layoutGroup.Visibility = LayoutVisibility.Never;
			//layoutGroup.Clear();
			//layoutGroup.Dispose();
			layoutGroup.Expanded = false;
		}

		private /*static*/ bool HideEmptyTimePriceData( DateTime start, DateTime end, double price, LayoutControlGroup layoutGroup,
			LabelControl textTime, LabelControl textPrice)
		{
			bool hasData = false;

			//If no data hide section
			if (start == DateTime.MinValue && end == DateTime.MinValue && price == 0)
			{
				//layoutGroup.Clear();
				//layoutGroup.Dispose();
				
				//textTime.Dispose();
				//textPrice.Dispose();
				textTime.Text = Strings.RoomInfoDlgNoDataTxt;
				textPrice.Text = Strings.RoomInfoDlgNoDataTxt;
				//HideGroup(layoutGroup);
			}
			else
			{
				textTime.Text = start.ToShortTimeString() + "-" + end.ToShortTimeString();
				textPrice.Text = price.ToString("C");
				hasData = true;
			}
			return hasData;
		}

		private /*static*/ bool HideEmptyTimePriceData(string start, string end, string price, 
			LabelControl textTime, LabelControl textPrice)
		{
			bool hasData = false;

			//If no data hide section
			if (string.IsNullOrEmpty(start) && string.IsNullOrEmpty(end) && string.IsNullOrEmpty(price))
			{
				//layoutGroup.Clear();
				//layoutGroup.Dispose();

				//textTime.Dispose();
				//textPrice.Dispose();
				textTime.Text = Strings.RoomInfoDlgNoDataTxt;
				textPrice.Text = Strings.RoomInfoDlgNoDataTxt;
				//HideGroup(layoutGroup);
			}
			else
			{
				textTime.Text = start + "-" + end;
				textPrice.Text = price;//.ToString("C");
				hasData = true;
			}
			return hasData;
		}

		private static bool HideEmptyDataField(LabelControl field, LabelControl lable, string data)
		{
			bool hasData = false;

			field.Text = data;
			if (string.IsNullOrEmpty(data))
			{
				//lable.Dispose();
				//field.Dispose();
			}
			else
			{
				hasData = true;
			}

			return hasData;
		}
		private static bool HideEmptyDataField(MemoEdit field, LabelControl lable, string data)
		{
			bool hasData = false;

			field.Text = data;
			if (string.IsNullOrEmpty(data))
			{
				//lable.Dispose();
				//field.Dispose();
			}
			else
			{
				hasData = true;
			}

			return hasData;
		}


		void SetDisplayedData()
		{
			if (_roomProfile == null)
			{
				labelNoImage.Dispose();
				labelNoMap.Dispose();
				return;
			}

			//Picture
			groupPicture.Text = _roomProfile.Name ?? " ";
			if( pictureEdit1.Image != null)//_info.imageLocation != null)
			{
				labelNoImage.Visible = false;
				labelNoImage.Dispose();
				groupPicture.Invalidate();
				groupPicture.Update();

				//pictureEdit1.Image = Image.FromFile(_info.imageLocation);
			}
			else
			{
				pictureEdit1.Visible = false;
				pictureEdit1.Dispose();

				groupPicture.Invalidate();
				groupPicture.Update();

				labelNoImage.Visible = true;
				labelNoImage.Dock = DockStyle.Fill;
				//HideGroup(groupPicture);
			}
			
			//Map
			if (!string.IsNullOrEmpty(_roomProfile.Latitude) ||
				!string.IsNullOrEmpty(_roomProfile.Longitude)) 
				//_info.geoCodeAddress != null )
			{
				labelNoMap.Visible = false;
				labelNoMap.Dispose();
				
				groupMap.Invalidate();
				groupMap.Update();
				//http://www.whygo.eu/GoogleLocMapPop.asp?LID=13962&start_day=25&start_month=5&start_year=2012&start_hr=&start_min=&start_ampm=&jtt=y
				//http://www.whygo.eu/GoogleLocMapPop.asp?LID=5533&start_day=26&start_month=12&start_year=2011&start_hr=9&start_min=0&start_ampm=am&jtt=y
				//GoogleMapsUri uri = new GoogleMapsUri
				//                           {
				//                               Location = _roomProfile.PublicRoomData.GeoCodeAddress,
				//                               Latitude = _roomProfile.Latitude,
				//                               Longitude = _roomProfile.Longitude
				//                           };//.ToString();

				//webBrowser1.Url = new Uri(uri.ToString());//_info.geoCodeAddress); //?????


				string URL = string.Format("http://maps.google.com/maps/api/staticmap?center={0},{1}&zoom=12&size=300x300&maptype=roadmap&sensor=true&markers=color:blue%7Clabel:W%7C{0},{1}", _roomProfile.Latitude, _roomProfile.Longitude);
					//"http://maps.google.com/maps/api/staticmap?center={0},{1}&zoom=12&size=200x200&markers=color:blue%7Clabel:S%7C{0},{1}&markers=size:mid&sensor=true", _roomProfile.Latitude, _roomProfile.Longitude);
				//URL = "http://maps.google.com/maps/api/staticmap?center=40.714728,-73.998672&zoom=12&size=400x400&maptype=roadmap&sensor=true";
				try
				{
					//var vv = new Uri(URL);
					//webBrowser1.Url = new Uri("http://maps.google.com/maps/api/staticmap?center=40.714728,-73.998672&zoom=12&size=300x300&maptype=roadmap&sensor=true&markers=color:blue%7Clabel:W%7C40.714728,-73.998672");
					webBrowser1.Url = new Uri(URL);
				}
				catch (Exception exception)
				{
					throw exception;
				}
				
				//webBrowser1.Url = new Uri("http://www.whygo.eu/GoogleLocMapPop.asp?LID=13962&start_day=25&start_month=5&start_year=2012&start_hr=&start_min=&start_ampm=&jtt=y", UriKind.Absolute);
			}		
			else
			{
				webBrowser1.Visible = false;
				webBrowser1.Dispose();

				groupMap.Invalidate();
				groupMap.Update();

				labelNoMap.Visible = true;
				labelNoMap.Dock = DockStyle.Fill;
				//HideGroup(groupMap);
			}
			
			//Location
			int isLocationDefined = 0;

			string countryName = ManageRooms.ManageRoomStaticData.CountriesDict.FirstOrDefault(a => a.Key == _roomProfile.PublicRoomData.Country ).Value ;
			var found = ManageRooms.ManageRoomStaticData.StatesDict.FirstOrDefault(a => a.Value == _roomProfile.PublicRoomData.Country &&
				                                                           a.Key.First == _roomProfile.PublicRoomData.State).Key;
			string stateName = found != null ? found.Second : string.Empty;
			string location = string.Empty;

			if (string.IsNullOrEmpty(countryName))
				countryName = Strings.RoomInfoDlgNoDataTxt; 
			if (string.IsNullOrEmpty(stateName))
				stateName = Strings.RoomInfoDlgNoDataTxt;
			string cityName = string.IsNullOrEmpty(_roomProfile.City) ? Strings.RoomInfoDlgNoDataTxt : _roomProfile.City;
			location += countryName + "/";
			location += stateName + "/";
			location += cityName;

			isLocationDefined += HideEmptyDataField(textCountrySateSuburb, labelCountrySateSuburb, /*_roomProfile.City*/location) ? 1 : 0;
			//textCountrySateSuburb.Text = _info.CitySuburb;
			//if (_info.CitySuburb == null || _info.CitySuburb.Length <= 0)
			//{
			//    labelCountrySateSuburb.Dispose();
			//    textCountrySateSuburb.Dispose();
			//}
			//else
			//{
			//    isLocationDefined = true;
			//}
			isLocationDefined += HideEmptyDataField(textLocationName, labelLocationName, _roomProfile.Name) ? 1 : 0;
			//textLocationName.Text = _info.LocationName;
			//if (_info.LocationName == null || _info.LocationName.Length <= 0)
			//{
			//    labelLocationName.Dispose();
			//    textLocationName.Dispose();
			//}
			//else
			//{
			//    isLocationDefined = true;
			//}
			isLocationDefined += HideEmptyDataField(textPhoneNumber, labelPhoneNumber, _roomProfile.Phone) ? 1 : 0;
			//textPhoneNumber.Text = _info.generalPhone;
			//if (_info.generalPhone == null || _info.generalPhone.Length <= 0)
			//{
			//    labelPhoneNumber.Dispose();
			//    textPhoneNumber.Dispose();
			//}
			//else
			//{
			//    isLocationDefined = true;
			//}
		//	isLocationDefined += HideEmptyDataField(textFaxNumber, labelFaxNumber, ""/*_roomProfile.???? NO SUCH FIELD???*/) ? 1 : 0;
			////textFaxNumber.Text = _info.
			////if (_info.generalPhone == null || _info.generalPhone.Length <= 0)
			//if (textFaxNumber.Text == null || textFaxNumber.Text.Length <= 0)
			//{
			//    labelFaxNumber.Dispose();
			//    textFaxNumber.Dispose();
			//}
			//else
			//{
			//    isLocationDefined = true;
			//}

			if (isLocationDefined == 0)
			{
				//HideGroup(groupSiteLocation);
				//groupSiteLocation.Visibility = LayoutVisibility.Never;
			}
			else
			{
				groupSiteLocation.Invalidate();
				groupSiteLocation.Update();
			}

			//Price & hours
			int isPriceDefined = 0;

			isPriceDefined += HideEmptyTimePriceData( _roomProfile.PublicRoomData.EarlyHoursStart,
				_roomProfile.PublicRoomData.EarlyHoursEnd, _roomProfile.PublicRoomData.EarlyHoursCost,
				textEarlyHrsTime, textEarlyHrsPrice) ? 1 : 0;
				//HideEmptyTimePriceData(_info.earlyHoursStart, _info.earlyHoursEnd, _info.earlyHoursPrice, layoutEarlyHoursGroup,
				//textEarlyHrsTime, textEarlyHrsPrice) ? 1 : 0;

			////If no data hide section
			//if (_info.earlyHoursStart == DateTime.MinValue && _info.earlyHoursEnd == DateTime.MinValue && _info.earlyHoursPrice == 0)
			//{
			//    layoutEarlyHoursGroup.Visibility = LayoutVisibility.Never;
			//    layoutEarlyHoursGroup.Clear();
			//    layoutEarlyHoursGroup.Dispose();
			//}
			//else
			//{
			//    textEarlyHrsTime.Text = _info.earlyHoursStart.ToShortTimeString() + "-" + _info.earlyHoursEnd.ToShortTimeString();
			//    textEarlyHrsPrice.Text = _info.earlyHoursPrice.ToString("C");
			//    isPriceDefined = true;
			//}

			isPriceDefined += HideEmptyTimePriceData( _roomProfile.PublicRoomData.OfficeHoursStart,
				_roomProfile.PublicRoomData.OfficeHoursEnd, _roomProfile.PublicRoomData.OfficeHoursCost,
				textOfficeHrsTime, textOfficeHrsPrice) ? 1 : 0;
				//HideEmptyTimePriceData(_info.openHoursStart, _info.openHoursEnd, _info.openHoursPrice, layoutOfficeHoursGroup,
				//textOfficeHrsTime, textOfficeHrsPrice) ? 1 : 0;

			////If no data hide section
			//if (_info.openHoursStart == DateTime.MinValue && _info.openHoursEnd == DateTime.MinValue && _info.openHoursPrice == 0)
			//{
			//    layoutOfficeHoursGroup.Visibility = LayoutVisibility.Never;
			//    layoutOfficeHoursGroup.Clear();
			//    layoutOfficeHoursGroup.Dispose();
			//}
			//else
			//{
			//    textOfficeHrsTime.Text = _info.openHoursStart.ToShortTimeString() + "-" + _info.openHoursEnd.ToShortTimeString();
			//    textOfficeHrsPrice.Text = _info.openHoursPrice.ToString("C");
			//    isPriceDefined = true;
			//}

			isPriceDefined += HideEmptyTimePriceData( _roomProfile.PublicRoomData.AfterHoursStart,
				_roomProfile.PublicRoomData.AfterHoursEnd, _roomProfile.PublicRoomData.AfterHoursCost,
				textAfterHrsTime, textAfterHrsPrice) ? 1 : 0;
				//HideEmptyTimePriceData(_info.afterHoursStart, _info.afterHoursEnd, _info.afterHoursPrice, layoutAfterHoursGroup,
				//textAfterHrsTime, textAfterHrsPrice) ? 1 : 0;
			////If no data hide section
			//if (_info.afterHoursStart == DateTime.MinValue && _info.afterHoursEnd == DateTime.MinValue && _info.afterHoursPrice == 0)
			//{
			//    layoutAfterHoursGroup.Visibility = LayoutVisibility.Never;
			//    layoutAfterHoursGroup.Clear();
			//    layoutAfterHoursGroup.Dispose();
			//}
			//else
			//{
			//    textAfterHrsTime.Text = _info.afterHoursStart.ToShortTimeString() + "-" + _info.afterHoursEnd.ToShortTimeString();
			//    textAfterHrsPrice.Text = _info.afterHoursPrice.ToString("C");
			//    isPriceDefined = true;
			//}

			isPriceDefined += HideEmptyTimePriceData( _roomProfile.PublicRoomData.CrazyHoursStart,
				_roomProfile.PublicRoomData.CrazyHoursEnd, _roomProfile.PublicRoomData.CrazyHoursCost,
				textLateHrsTime, textLateHrsPrice) ? 1 : 0;
				//HideEmptyTimePriceData(_info.crazyHoursStart, _info.crazyHoursEnd, _info.crazyHoursPrice, layoutLateHoursGroup,
				//textLateHrsTime, textLateHrsPrice) ? 1 : 0;

			////If no data hide section
			//if (_info.crazyHoursStart == DateTime.MinValue && _info.crazyHoursEnd == DateTime.MinValue && _info.crazyHoursPrice == 0)
			//{
			//    layoutLateHoursGroup.Visibility = LayoutVisibility.Never;
			//    layoutLateHoursGroup.Clear();
			//    layoutLateHoursGroup.Dispose();
			//}
			//else
			//{
			//    textLateHrsTime.Text = _info.crazyHoursStart.ToShortTimeString() + "-" + _info.crazyHoursEnd.ToShortTimeString();
			//    textLateHrsPrice.Text = _info.crazyHoursPrice.ToString("C");
			//    isPriceDefined = true;
			//}

			if (isPriceDefined == 0)
			{
				//HideGroup(groupHoursAndPrice);//groupHoursAndPrice.Visibility = LayoutVisibility.Never;
			}
			else
			{
				groupHoursAndPrice.Invalidate();
				groupHoursAndPrice.Update();
			}

			//Technical Details
			int isTechnicalDetailsDefined = 0;

			//string endpointName = string.Empty;
			string strISDNIP = string.Empty;
			var cursor = Cursor.Current;
			Cursor.Current = Cursors.WaitCursor;
			try
			{
				//var _endpoint = MyVrmService.Service.GetEndpoint(_roomProfile.EndpointId);
				//endpointName = _endpoint.Name;

				if (!string.IsNullOrEmpty(_roomProfile.PublicRoomData.isdnAddress))
					strISDNIP = "ISDN";
				if (!string.IsNullOrEmpty(_roomProfile.PublicRoomData.ipAddress))
				{
					if (!string.IsNullOrEmpty(strISDNIP))
						strISDNIP += " or ";
					strISDNIP += "IP";
				}

			}
			catch (Exception exception)
			{
				MyVrmAddin.TraceSource.TraceException(exception);
			}
			finally
			{
				Cursor.Current = cursor;
			}
			isTechnicalDetailsDefined += HideEmptyDataField(textISDN, labelISDN, strISDNIP /*endpointName*/) ? 1 : 0;
			//textISDN.Text = _info.isIPCapable; //????????
			isTechnicalDetailsDefined += HideEmptyDataField(textEquipmentType, labelEquipmentType, _roomProfile.PublicRoomData.AUXEquipment /*equipmentDescription*/) ? 1 : 0;
			//textEquipmentType.Text = _info.equipmentDescription;
			isTechnicalDetailsDefined += HideEmptyDataField(textSDHD, labelSDHD, _roomProfile.PublicRoomData.IsHDCapable == 0 ? "No" : "Standard and High Definition"  ) ? 1 : 0;
			//textSDHD.Text = _info.isHDCapable;
			if (isTechnicalDetailsDefined == 0)
			{
				//HideGroup(groupTechnicalDetails);
			}
			else
			{
				groupTechnicalDetails.Invalidate();
				groupTechnicalDetails.Update();
			}

			//Description
			int isDescriptionDefined = 0;

			isDescriptionDefined += HideEmptyDataField(textRoomCapacity, labelRoomCapacity,
				_roomProfile.Capacity != 0 ? _roomProfile.Capacity.ToString() : "") ? 1 : 0;
			//textRoomCapacity.Text = _info.Capacity.ToString();
			isDescriptionDefined += HideEmptyDataField(textOpenHours, labelOpenHours, _roomProfile.PublicRoomData.OpenHour) ? 1 : 0;
			//textOpenHours.Text = _info.openHours;

			string ipText = string.Empty;
			switch(_roomProfile.PublicRoomData.IsInternetCapable)
			{
				case 0:
					ipText = "No";
					break;
				case 1:
					ipText = "Yes - Free of Charge";
					break;
				case 2:
					ipText = "Yes";
				break;
			}
			isDescriptionDefined += HideEmptyDataField(textInternetAvailable, labelInternetAvailable, ipText) ? 1 : 0;
			//textInternetAvailable.Text = _info.isInternetCapable;
			isDescriptionDefined += HideEmptyDataField(textDescription, labelDescription, _roomProfile.PublicRoomData.Description) ? 1 : 0;
			//textDescription.Text = _info.Description;
			isDescriptionDefined += HideEmptyDataField(/*textNotes*/memoNotes, labelNotes, _roomProfile.PublicRoomData.ExtraNotes) ? 1 : 0;
			//textNotes.Text = _info.extraNotes;

			if (isDescriptionDefined == 0)
			{
				//HideGroup(groupSiteDescription);
			}
			else
			{
				groupSiteDescription.Invalidate();
				groupSiteDescription.Update();
			}
		}

		void SetDlgTexts()
		{
			Text = Strings.RoomInfoDlgCaption;
			groupMap.Text = Strings.RoomInfoDlgMapTxt;

			groupSiteLocation.Text = Strings.RoomInfoDlgLocationTxt;
			labelCountrySateSuburb.Text = Strings.RoomInfoDlgCountryStateSuburbTxt;
			labelLocationName.Text = Strings.RoomInfoDlgLocationNameTxt;
			labelPhoneNumber.Text = Strings.RoomInfoDlgPhoneNumberTxt;
			//labelFaxNumber.Text = Strings.RoomInfoDlgFaxNumberTxt;
			
			groupHoursAndPrice.Text = Strings.RoomInfoDlgHoursPriceTxt + ", " + _roomProfile.PublicRoomData.CurrencyType; //????
			labelEarlyHrs.Text = Strings.RoomInfoDlgEarlyHrsTxt;
			labelOfficeHrs.Text = Strings.RoomInfoDlgOfficeHrsTxt;
			labelAfterHrs.Text = Strings.RoomInfoDlgAfterHrsTxt;
			labelLateHrs.Text = Strings.RoomInfoDlgLateHrsTxt;
			
			groupTechnicalDetails.Text = Strings.RoomInfoDlgTechnicalDetailsTxt;
			labelISDN.Text = Strings.RoomInfoDlgISDNIPCapable;
			labelEquipmentType.Text = Strings.RoomInfoDlgEquipmentTypeTxt;
			labelSDHD.Text = Strings.RoomInfoDlgSDHDCapableTxt;

			groupSiteDescription.Text = Strings.RoomInfoDlgSiteDescriptionTxt;
			labelRoomCapacity.Text = Strings.RoomInfoDlgRoomCapacityTxt;
			labelOpenHours.Text = Strings.RoomInfoDlgOpenHoursTxt;
			labelNotes.Text = Strings.RoomInfoDlgNotesTxt;
			labelDescription.Text = Strings.RoomInfoDlgDescriptionTxt;
			labelInternetAvailable.Text = Strings.RoomInfoDlgInternetAvailableTxt;
		}

		private void RoomInfo_Load(object sender, EventArgs e)
		{
			SetDlgTexts();
			SetDisplayedData();
			
		}
		void RoomInfo_Shown(object sender, System.EventArgs e)
		{
			bool groupSiteLocationState = groupSiteLocation.Expanded;
			bool groupHoursAndPriceState = groupHoursAndPrice.Expanded;
			bool groupTechnicalDetailsState = groupTechnicalDetails.Expanded;
			bool groupSiteDescriptionState = groupSiteDescription.Expanded;

			groupSiteLocation.Expanded = false;
			groupHoursAndPrice.Expanded = false;
			groupTechnicalDetails.Expanded = false;
			groupSiteDescription.Expanded = false;
			//Refresh();
			//Invalidate(true);
			//Update();
			if (groupSiteLocationState)
				groupSiteLocation.Expanded = true;
			if (groupHoursAndPriceState)
				groupHoursAndPrice.Expanded = true;
			if (groupTechnicalDetailsState)
				groupTechnicalDetails.Expanded = true;
			if (groupSiteDescriptionState)
				groupSiteDescription.Expanded = true;
			Refresh();
		}

		//Show picture in a separate window
		private void pictureEdit1_EditValueChanged(object sender, EventArgs e)
		{

		}

		private void layoutControlItem14_Click(object sender, EventArgs e)
		{
			Process.Start(webBrowser1.Url.AbsoluteUri);
		}

		private void layoutControlItem14_DoubleClick(object sender, EventArgs e)
		{

		}

		private void layoutControlItem14_MouseDown(object sender, MouseEventArgs e)
		{

		}
		
	}
}
