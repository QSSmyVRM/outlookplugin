﻿using System;
using Microsoft.Office.Interop.Excel;

namespace MyVrm.DataImport.Excel.Extensions
{
    public static class RangeExtensions
    {
        public static string ValueAsString(this Range range)
        {
            return Convert.ToString(range.Value2);
        }

        public static int ValueAsInt(this Range range)
        {
            return Convert.ToInt32(range.Value2);
        }

        public static TimeZoneInfo ValueAsTimeZoneInfo(this Range range)
        {
            return TimeZoneInfo.FindSystemTimeZoneById(ValueAsString(range));
        }
    }
}
