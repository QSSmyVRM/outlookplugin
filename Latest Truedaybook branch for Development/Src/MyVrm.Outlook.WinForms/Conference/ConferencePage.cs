﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System.ComponentModel;
using System.Windows.Forms;

namespace MyVrm.Outlook.WinForms.Conference
{
    public partial class ConferencePage : Page
    {
        private bool _readOnly;

        public ConferencePage()
        {
            InitializeComponent();
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false),
         EditorBrowsable(EditorBrowsableState.Never)]
        public ConferenceWrapper Conference
        {
            get
            {
                return (ConferenceWrapper) conferenceBindingSource.DataSource;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false),
         EditorBrowsable(EditorBrowsableState.Never)]
        public BindingSource ConferenceBindingSource
        {
            get
            {
                return conferenceBindingSource;
            }
        }

        public bool ReadOnly
        {
            get
            {
                return _readOnly;
            }
            set
            {
                if (value != _readOnly)
                {
                    _readOnly = value;
                    OnReadOnlyChanged(new ReadOnlyChangedEventArgs(_readOnly));
                }
            }
        }

        protected virtual void OnReadOnlyChanged(ReadOnlyChangedEventArgs e)
        {
        }
    }
}