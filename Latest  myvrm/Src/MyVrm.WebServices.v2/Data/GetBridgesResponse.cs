﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace MyVrm.WebServices.Data
{
    internal class GetBridgesResponse : ServiceResponse
    {
        private readonly List<BridgeName> _bridgeNames = new List<BridgeName>();

        public ReadOnlyCollection<BridgeName> BridgeNames
        {
            get
            {
                return new ReadOnlyCollection<BridgeName>(_bridgeNames);
            }
        }

        internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
        {
            base.ReadElementsFromXml(reader);
            _bridgeNames.Clear();
            do
            {
                reader.Read();
                if (reader.IsStartElement(XmlNamespace.NotSpecified, "Bridge"))
                {
                    var bridgeName = new BridgeName();
                    bridgeName.LoadFromXml(reader, "Bridge");
                    _bridgeNames.Add(bridgeName);
                }
                else
                {
                    reader.SkipCurrentElement();
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "Bridges"));
        }
    }
}
