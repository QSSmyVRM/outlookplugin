﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using Microsoft.Office.Interop.Outlook;

namespace MyVrm.Outlook
{
    public class InspectorWrapper
    {
        public InspectorWrapper(Inspector inspector)
        {
            try
            {
                if (inspector == null)
                    throw new ArgumentNullException("inspector");
                Id = Guid.NewGuid();
                Inspector = inspector;
                ((InspectorEvents_10_Event)Inspector).Close += OnInspectorClose;
                Initialize();
            }
            catch (System.Exception exception) //ZD 102062 start
            {
                MyVrmAddin.TraceSource.TraceException(exception);
            }
        }

        public delegate void CloseHandler(object sender, EventArgs e);
        public event CloseHandler Closed;

        public Inspector Inspector { get; private set; }

        public Guid Id { get; private set; }

        public static InspectorWrapper GetWrapperFor(Inspector inspector)
        {
            try
            {
                if (inspector == null)
                    throw new ArgumentNullException("inspector");
                if (inspector.CurrentItem is AppointmentItem)
                {
                    return new AppointmentInspectorWrapper(inspector);
                }
            }
            catch (System.Exception exception) //ZD 102062 start
            {
                MyVrmAddin.TraceSource.TraceException(exception);
            }
            return null;
        }

        protected virtual void Initialize()
        {
        }

        protected virtual void Close()
        {
        }

        protected void OnInspectorClose()
        {
            Close();
            ((InspectorEvents_10_Event)Inspector).Close -= OnInspectorClose;
            Inspector = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();
            if (Closed != null)
            {
                Closed(this, EventArgs.Empty);
            }
        }
    }
}