﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System.Collections.Generic;
using System.Linq;

namespace MyVrm.WebServices.Data
{
	//Request
	class SetPreferedRoomRequest : ServiceRequestBase<SetPreferedRoomResponse>
	{
		internal SetPreferedRoomRequest(MyVrmService service)
			: base(service)
		{
		}

		public IEnumerable<RoomId> RoomList { get; set; }

		#region Overrides of ServiceRequestBase<GetTemplateResponse>

		internal override string GetXmlElementName()
		{
			return "login";
		}

		internal override string GetCommandName()
		{
			return Constants.SetPreferedRoomCommandName; 
		}

		internal override string GetResponseXmlElementName()
		{
			return "success"; 
		}

		internal override SetPreferedRoomResponse ParseResponse(MyVrmServiceXmlReader reader)
		{
			var response = new SetPreferedRoomResponse();
			response.ReadElementsFromXml(reader);
			return response;
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			if (UserId != null)
				UserId.WriteToXml(writer, "userID");
			writer.WriteElementValue(XmlNamespace.NotSpecified, "location", string.Join(",", RoomList.Select( id => id.ToString()).ToArray()));
		}

		#endregion
	}

	//Response
	public class SetPreferedRoomResponse : ServiceResponse
	{
		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{
			reader.Read(); //???
		}
	}
}
