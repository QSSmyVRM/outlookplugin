﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace MyVrm.WebServices.Data
{
    public enum BridgeStatus
    {
        Active = 1,
        Maintenance = 2,
        Disabled = 3
    }

    [ServiceObjectDefinition("bridge")]
    public class Bridge : ServiceObject
    {
        public Bridge(MyVrmService service) : base(service)
        {
        }

        public BridgeId Id
        {
            get
            {
                return (BridgeId)PropertyBag[BridgeSchema.Id];
            }
        }

        public string Name
        {
            get
            {
                return (string) PropertyBag[BridgeSchema.Name];
            }
            set
            {
                PropertyBag[BridgeSchema.Name] = value;
            }
        }

        public string Login
        {
            get
            {
                return (string)PropertyBag[BridgeSchema.Login];
            }
            set
            {
                PropertyBag[BridgeSchema.Login] = value;
            }
        }

        public string Password
        {
            get
            {
                return (string)PropertyBag[BridgeSchema.Password];
            }
            set
            {
                PropertyBag[BridgeSchema.Password] = value;
            }
        }

        public TimeZoneInfo TimeZone
        {
            get
            {
                return (TimeZoneInfo) PropertyBag[BridgeSchema.TimeZone];
            }
            set
            {
                PropertyBag[BridgeSchema.TimeZone] = value;
            }
        }

        public int MaxAudioCalls
        {
            get
            {
				int iRet = 0;
				if (PropertyBag[BridgeSchema.MaxAudioCalls] != null)
					int.TryParse(PropertyBag[BridgeSchema.MaxAudioCalls].ToString(), out iRet);
				return iRet; //(int)PropertyBag[BridgeSchema.MaxAudioCalls];
            }
            set
            {
                PropertyBag[BridgeSchema.MaxAudioCalls] = value;
            }
        }

        public int MaxVideoCalls
        {
            get
            {
                int iRet = 0;
				if (PropertyBag[BridgeSchema.MaxVideoCalls] != null)
					int.TryParse(PropertyBag[BridgeSchema.MaxVideoCalls].ToString(), out iRet);
				return iRet; //(int)PropertyBag[BridgeSchema.MaxVideoCalls];
            }
            set
            {
                PropertyBag[BridgeSchema.MaxVideoCalls] = value;
            }
        }

        public int Vendor
        {
            get
            {
                int iRet = 0;
				if (PropertyBag[BridgeSchema.Vendor] != null)
					int.TryParse(PropertyBag[BridgeSchema.Vendor].ToString(), out iRet);
				return iRet; //(int)PropertyBag[BridgeSchema.Vendor];
            }
            set
            {
                PropertyBag[BridgeSchema.Vendor] = value;
            }
        }

        public BridgeStatus Status
        {
            get
            {
                return (BridgeStatus)PropertyBag[BridgeSchema.Status];
            }
            set
            {
                PropertyBag[BridgeSchema.Status] = value;
            }
        }

        public bool IsVirtual
        {
            get
            {
				bool bRet = false;
				if (PropertyBag[BridgeSchema.IsVirtual] != null)
					bool.TryParse(PropertyBag[BridgeSchema.IsVirtual].ToString(), out bRet);
            	return bRet; //(bool)PropertyBag[BridgeSchema.IsVirtual];
            }
            set
            {
                PropertyBag[BridgeSchema.IsVirtual] = value;
            }
        }

        public UserId AdministratorId
        {
            get
            {
                return ((BridgeAdministrator)PropertyBag[BridgeSchema.Administrator]).Id;
            }
            set
            {
                ((BridgeAdministrator)PropertyBag[BridgeSchema.Administrator]).Id = value;
            }
        }

        public string FirmwareVersion
        {
            get
            {
                return (string)PropertyBag[BridgeSchema.FirmwareVersion];
            }
            set
            {
                PropertyBag[BridgeSchema.FirmwareVersion] = value;
            }
        }

        public int PercentOfReservedPorts
        {
            get
            {
				int iRet = 0;
				if (PropertyBag[BridgeSchema.PercentOfReservedPorts] != null)
					int.TryParse(PropertyBag[BridgeSchema.PercentOfReservedPorts].ToString(), out iRet);
				return iRet; //(int)PropertyBag[BridgeSchema.PercentOfReservedPorts];
            }
            set
            {
                PropertyBag[BridgeSchema.PercentOfReservedPorts] = value;
            }
        }

        public ApproverIdCollection Approvers
        {
            get
            {
                return (ApproverIdCollection) PropertyBag[BridgeSchema.Approvers];
            }
        }

        public BridgeDetails Details
        {
            get
            {
                return (BridgeDetails) PropertyBag[BridgeSchema.Details];
            }
            set
            {
                PropertyBag[BridgeSchema.Details] = value;
            }
        }

        public bool IsdnThresholdAlert
        {
            get
            {
            	bool bRet = false;
				if (PropertyBag[BridgeSchema.IsdnThresholdAlert] != null)
            		bool.TryParse(PropertyBag[BridgeSchema.IsdnThresholdAlert].ToString(), out bRet);
            	return bRet;// (bool)PropertyBag[BridgeSchema.IsdnThresholdAlert];
            }
            set
            {
                PropertyBag[BridgeSchema.IsdnThresholdAlert] = value;
            }
        }

        public bool MalfunctionAlert
        {
            get
            {
				bool bRet = false;
				if (PropertyBag[BridgeSchema.MalfunctionAlert] != null)
					bool.TryParse(PropertyBag[BridgeSchema.MalfunctionAlert].ToString(), out bRet);
				return bRet;//(bool)PropertyBag[BridgeSchema.MalfunctionAlert];
            }
            set
            {
                PropertyBag[BridgeSchema.MalfunctionAlert] = value;
            }
        }

        public void Save()
        {
            Service.SetBridge(this);
        }

        #region Overrides of ServiceObject

        internal override PropertyDefinition GetIdPropertyDefinition()
        {
            return BridgeSchema.Id;
        }

        internal override ServiceObjectSchema GetSchema()
        {
            return BridgeSchema.Instance;
        }

        #endregion
    }
}
