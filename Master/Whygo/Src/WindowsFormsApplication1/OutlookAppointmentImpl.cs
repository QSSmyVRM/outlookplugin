﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using MyVrm.Outlook;
using MyVrm.WebServices.Data;

namespace WindowsFormsApplication1
{
    internal class OutlookAppointmentImpl : OutlookAppointment
    {
        private List<OutlookRecipient> _recipients = new List<OutlookRecipient>();

        private string _subject;
        private OutlookRecipientImpl _currentUser;


        public OutlookAppointmentImpl(OutlookRecipientImpl currentUser)
        {
            _currentUser = currentUser;
        }

        public override string Subject
        {
            get { return _subject; }
            set
            {
                _subject = value;
                NotifyPropertyChanged("Subject");
            }
        }

        private DateTime _start;
        public override DateTime Start
        {
            get { return _start; }
            set
            {
                _start = value;
                NotifyPropertyChanged("Start");
            }
        }

        private DateTime _end;
        public override DateTime End
        {
            get { return _end; }
            set
            {
                _end = value;
                NotifyPropertyChanged("End");
            }
        }

        public override TimeZoneInfo StartTimeZone
        {
            get { return TimeZoneInfo.Local; }
            set
            {
                
            }
        }

        public override TimeZoneInfo EndTimeZone
        {
            get { return TimeZoneInfo.Local; }
            set {  }
        }

        public override TimeSpan Duration
        {
            get; set;
        }

        public override DateTime StartInStartTimeZone
        {
            get { return Start; }
            set { Start = value; }
        }

        public override DateTime EndInEndTimeZone
        {
            get { return End; }
            set { End = value; }
        }

        private string _location;
        public override string Location
        {
            get { return _location; }
            set
            {
                _location = value;
                NotifyPropertyChanged("Location");
            }
        }


        private ConferenceId _conferenceId;
        public override ConferenceId ConferenceId
        {
            get { return _conferenceId; }
            set
            {
                _conferenceId = value;
                NotifyPropertyChanged("ConferenceId");  
            }
        }

        public override string GlobalAppointmentId
        {
            get { return null; }
        }

        public override event PropertyChangedEventHandler PropertyChanged;
        
        public override event CancelEventHandler Saving;
        public override event CancelEventHandler Sending;
        public override event CancelEventHandler BeforeDelete;

        public override ReadOnlyCollection<OutlookRecipient> Recipients
        {
            get
            {
                if (_recipients == null)
                {
                    _recipients = new List<OutlookRecipient>();    
                }
                return new ReadOnlyCollection<OutlookRecipient>(_recipients);
            }
        }

		public override void SetRecipients(IEnumerable<string> arg)
		{
			ClearRecipients();

			//???
			//foreach (string outlookRecipientMail in arg)
			//{
			//    Recipient recipient = _recipients.Add(outlookRecipientMail);
			//    bool bret = recipient.Resolve();
			//    if( !recipient.Resolved )
			//    {
			//        _item.Recipients.Remove(recipient.Index);
			//    }
			//}
		}

		public override void ClearRecipients()
		{
			try
			{
				while (_recipients.Count > 0)
					_recipients.Remove(_recipients[_recipients.Count]);
			}
			catch (Exception e)
			{
				MyVrmService.TraceSource.TraceInformation("_recipients.Remove Exception {0}", e.ToString());
				throw;
			}
		}
        public override OutlookRecipient CurrentUser
        {
            get
            {
                return _currentUser;    
            }
        }

        private string _body;
        public override string Body
        {
            get { return _body; }
            set
            {
                _body = value;
                NotifyPropertyChanged("Body");
            }
        }

        public override string HtmlBody
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public override string RtfBody
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        private bool _isRecurring;
        public override bool IsRecurring
        {
            get
            {
                return _isRecurring;
            }
            set
            {
                _isRecurring = value;
                NotifyPropertyChanged("IsRecurring");
            }
        }

        public override RecurrenceState RecurrenceState
        {
            get; set;
        }

        public override void RemoveRecipient(string email)
        {
            
        }

        public override FreeBusyStatus BusyStatus
        {
            get; set;
        }

        public override RecurrencePattern GetRecurrencePattern(TimeZoneInfo timeZone)
        {
            RecurrencePattern recurrencePattern = new RecurrencePattern(timeZone);
            recurrencePattern.RecurrenceType = RecurrenceType.Yearly;
            recurrencePattern.StartDate = Start.Date;
            recurrencePattern.StartTime = Start.TimeOfDay;
            recurrencePattern.EndTime = End.TimeOfDay;
            recurrencePattern.MonthOfYear = 10;
            recurrencePattern.DayOfMonth = 15;
            recurrencePattern.Interval = 1;
            //recurrencePattern.DaysOfWeek = DaysOfWeek.Monday | DaysOfWeek.Tuesday | DaysOfWeek.Wednesday;
            //recurrencePattern.Interval = 1;
            return recurrencePattern;
        }

        public override OutlookAppointment GetOccurrence(DateTime occurrenceTime)
        {
            throw new NotImplementedException();
        }

        public override void Save()
        {
            throw new NotImplementedException();
        }

        public override void Delete()
        {
            throw new NotImplementedException();
        }

        public override void SetNonOutlookProperty(bool value)
        {
            throw new NotImplementedException();
        }

        public override bool GetNonOutlookProperty()
        {
            throw new NotImplementedException();
        }

        public override void Dispose()
        {
            throw new NotImplementedException();
        }

        public void AddRecipient(OutlookRecipientImpl recipient)
        {
            _recipients.Add(recipient);
        }

        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        private void NotifySaving(ref bool cancel)
        {
            if (Saving != null)
            {
                var args = new CancelEventArgs();
                Saving(this, args);
                cancel = args.Cancel;
            }
        }
    }
}
