﻿namespace MyVrm.WebServices.Data
{
    public class CateringProviderMenuItem : ComplexProperty
    {
        public int Id { get; internal set; }
        public string Name { get; internal set; }

		public override object Clone()
		{
			CateringProviderMenuItem copy = new CateringProviderMenuItem();
			copy.Name = string.Copy(Name);
			copy.Id = Id;

			return copy;
		}

        public override string ToString()
        {
            return !string.IsNullOrEmpty(Name) ? Name : base.ToString();
        }

        internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
        {
            switch(reader.LocalName)
            {
                case "ID":
                {
                    Id = reader.ReadElementValue<int>();
                    return true;
                }
                case "Name":
                {
                    Name = reader.ReadElementValue();
                    return true;
                }
            }
            return false;
        }
    }
}
