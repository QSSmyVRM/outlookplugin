#pragma once

#include "Stdafx.h"

namespace MyVrm 
{
	namespace MAPI
	{
		[System::Serializable]
		public enum class MapiTags : ULONG
		{
			PidTagPrimarySmtpAddress = 0x39fe001f,
			PidTagAddressType = 0x3002001f
		};

		typedef array<MapiTags> MapiTagsArray;
	}
}