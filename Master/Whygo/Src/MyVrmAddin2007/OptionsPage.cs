﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.ComponentModel;
using System.Reflection;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Outlook;
using MyVrm.Outlook.WinForms;
using Form=System.Windows.Forms.Form;

namespace MyVrmAddin2007
{
    [ComVisible(true)]
    [Guid("66EFE48E-672D-4b33-AB43-7BDF17F76D8E"), ProgId("myVRMAddin.OptionsPage")]
    public partial class OptionsPage : UserControl, PropertyPage
    {
        private bool _dirty;
        private PropertyPageSite _propertyPageSite;

        public OptionsPage()
        {
            InitializeComponent();
        }

        #region PropertyPage Members

        public void Apply()
        {
            try
            {
                var cancelEventArgs = new CancelEventArgs(false);
                optionsControl.Apply(cancelEventArgs);
                _dirty = cancelEventArgs.Cancel;
            }
            catch (System.Exception e)
            {
                UIHelper.ShowError(e.Message);
                throw;
            }
        }

        public bool Dirty
        {
            get { return _dirty; }
        }

        public void GetPageInfo(ref string HelpFile, ref int HelpContext)
        {
        }

        #endregion

        [DispId(-518)]
        public string TabCaption
        {
            get
            {
                return Strings.SchedulerOptionsPageTitle;
            }
        }

        protected override void OnHandleDestroyed(EventArgs e)
        {
            // Since Outlook doesn't call Dispose method of IDispose this control never release resources.
            // So we catch OnHandleDestroyed event and explicitly call Dispose method of Component class.
            Dispose();
            base.OnHandleDestroyed(e);
        }

        void optionsControl_OptionsChanged(object sender, EventArgs args)
        {
            _dirty = true;
            if (_propertyPageSite != null)
            {
                _propertyPageSite.OnStatusChange();
            }
        }

        private void OptionsPage_Load(object sender, EventArgs e)
        {
            _propertyPageSite = GetPropertyPageSite();
            optionsControl.OptionsChanged += optionsControl_OptionsChanged;
        }

        PropertyPageSite GetPropertyPageSite()
        {
            string windowsFormsStrongName = typeof(Form).Assembly.FullName;
            Type oleObjectType =
                Type.GetType(Assembly.CreateQualifiedName(windowsFormsStrongName,
                                                                            "System.Windows.Forms.UnsafeNativeMethods"))
                    .GetNestedType("IOleObject");
            MethodInfo getClientSetMethodInfo = oleObjectType.GetMethod("GetClientSite");
            return getClientSetMethodInfo.Invoke(this, null) as PropertyPageSite;
        }
    }
}