﻿using System;
using System.Linq;
using Microsoft.Office.Interop.Excel.Extensions;
using MyVrm.DataImport.Excel.Extensions;
using MyVrm.WebServices.Data;
using ListObject=Microsoft.Office.Tools.Excel.ListObject;

namespace MyVrm.DataImport.Excel
{
    public class UsersTable : ExcelTable
    {
        private readonly ExcelTableColumn _idColumn;
        private readonly ExcelTableColumn _firstNameColumn;
        private readonly ExcelTableColumn _lastNameColumn;
        private readonly ExcelTableColumn _loginColumn;
        private readonly ExcelTableColumn _passwordColumn;
        private readonly ExcelTableColumn _emailColumn;
        private readonly ExcelTableColumn _timeZoneColumn;
        private readonly ExcelTableColumn _userRoleColumn;
        private readonly ExcelTableColumn _bridgeColumn;

        public UsersTable(ListObject listObject, MyVrmService service, ValidationListManager validationListManager) : 
            base(listObject, service, validationListManager)
        {
            _idColumn = new ExcelTableColumn(ListObject.ListColumns[1]) { IsReadOnly = true };
            _firstNameColumn = new ExcelTableColumn(ListObject.ListColumns[2]) { IsRequired = true };
            _lastNameColumn = new ExcelTableColumn(ListObject.ListColumns[3]) { IsRequired = true };
            _loginColumn = new ExcelTableColumn(ListObject.ListColumns[4]) { IsRequired = true };
            _passwordColumn = new ExcelTableColumn(ListObject.ListColumns[5]) { IsRequired = true };
            _emailColumn = new ExcelTableColumn(ListObject.ListColumns[6]) { IsRequired = true };
            _timeZoneColumn = new ExcelTableColumn(ListObject.ListColumns[7])
                                  {
                                      HasFixedSetOfValues = true,
                                      ValuesRangeName = ValidationListManager.GetTimezoneRange()
                                  };
            _userRoleColumn = new ExcelTableColumn(ListObject.ListColumns[8])
                                  {
                                      IsRequired = true,
                                      HasFixedSetOfValues = true,
                                      ValuesRangeName = ValidationListManager.GetUserRoleNamesRange()
                                  };
            _bridgeColumn = new ExcelTableColumn(ListObject.ListColumns[9]) { HasFixedSetOfValues = true};

            TableColumns.AddRange(new[]
                                      {
                                          _idColumn, _firstNameColumn, _lastNameColumn, _loginColumn, _passwordColumn,
                                          _emailColumn,
                                          _timeZoneColumn, _userRoleColumn, _bridgeColumn
                                      });

            Initialize();
        }

        #region Overrides of ExcelTable

        protected override void Save(SaveErrorCollection errors)
        {
            var userRoles = Service.GetUserRoles();
            foreach (var listRow in ListObject.ListRows.Items())
            {
                var user = new User(Service);
                try
                {
                    user.BridgeId = BridgeId.Default;
                    user.Status.Deleted = false;
                    user.SavedSearchesId = -1;

                    var range = listRow.Range;
                    user.UserName.FirstName = range.Item(1, _firstNameColumn.Index).ValueAsString();
                    user.UserName.LastName = range.Item(1, _lastNameColumn.Index).ValueAsString();
                    user.Login = range.Item(1, _loginColumn.Index).ValueAsString();
                    user.Password = range.Item(1, _passwordColumn.Index).ValueAsString();
                    user.Email = range.Item(1, _emailColumn.Index).ValueAsString();
                    user.TimeZone = range.Item(1, _timeZoneColumn.Index).ValueAsTimeZoneInfo();
                    var roleName = range.Item(1, _userRoleColumn.Index).ValueAsString();
                    user.Role =
                        userRoles.First(match => match.Name.Equals(roleName, StringComparison.OrdinalIgnoreCase)).Id;

                    user.Save();
                }
                catch (Exception e)
                {
                    errors.Add(new SaveError(user.UserName.ToString(), e));
                }
            }
        }

        #endregion
    }
}
