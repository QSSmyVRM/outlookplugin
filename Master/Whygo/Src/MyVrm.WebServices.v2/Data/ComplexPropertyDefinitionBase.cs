﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.WebServices.Data
{
    internal abstract class ComplexPropertyDefinitionBase : PropertyDefinition
    {
        internal ComplexPropertyDefinitionBase(string xmlElementName) : base(xmlElementName)
        {
        }

        internal ComplexPropertyDefinitionBase(string xmlElementName, PropertyDefinitionFlags flags) : base(xmlElementName, flags)
        {
        }

        internal ComplexPropertyDefinitionBase(string xmlElementName, string xmlElementNameForWrite, PropertyDefinitionFlags flags)
            : base(xmlElementName, xmlElementNameForWrite, flags)
        {
        }

        internal abstract ComplexProperty CreatePropertyInstance(ServiceObject owner);

        internal virtual void InternalLoadFromXml(MyVrmServiceXmlReader reader, PropertyBag propertyBag)
        {
            object value;
            if (!propertyBag.TryGetValue(this, out value))
            {
                value = CreatePropertyInstance(propertyBag.Owner);
            }
            ((ComplexProperty) value).LoadFromXml(reader, reader.LocalName);
            propertyBag[this] = value;
        }

        internal sealed override void LoadPropertyValueFromXml(MyVrmServiceXmlReader reader, PropertyBag propertyBag)
        {
            reader.EnsureCurrentNodeIsStartElement(XmlNamespace.NotSpecified, XmlElementName);
            if (!reader.IsEmptyElement || reader.HasAttributes)
            {
                InternalLoadFromXml(reader, propertyBag);
            }
            reader.ReadEndElementIfNecessary(XmlNamespace.NotSpecified, XmlElementName);
        }
        
        internal override void WritePropertyValueToXml(MyVrmXmlWriter writer, PropertyBag propertyBag)
        {
            var property = (ComplexProperty)propertyBag[this];
            if (property != null)
            {
                property.WriteToXml(writer, XmlElementNameForWrite);
            }
        }
    }
}
