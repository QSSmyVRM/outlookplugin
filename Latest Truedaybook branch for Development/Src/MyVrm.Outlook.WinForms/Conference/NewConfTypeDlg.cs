﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraLayout.Utils;

namespace MyVrm.Outlook.WinForms.Conference
{
	public partial class NewConfTypeDlg : Dialog
	{
		public int ConfId { get; set; }

		public NewConfTypeDlg()
		{
			InitializeComponent();
			ApplyEnabled = false;
			ApplyVisible = false;
		}

		private void NewConfTypeDlg_Load(object sender, EventArgs e)
		{
			SetVisibility(true);
		}

		void SetVisibility(bool forDynamic)
		{
			radioButtonDynamic.Checked = forDynamic;
			radioButtonStatic.Checked = !forDynamic;

			layoutControlItem6.Visibility = forDynamic ? LayoutVisibility.Never : LayoutVisibility.Always;
			layoutControlItem5.Visibility = forDynamic ? LayoutVisibility.Never : LayoutVisibility.Always;
			if (radioButtonStatic.Checked)
				textEditConfId.Focus();
		}

		private void radioButtonDynamic_CheckedChanged(object sender, EventArgs e)
		{
			RadioButton rb = sender as RadioButton;

			if (rb != null)
				SetVisibility(rb.Checked);
		}

		private void NewConfTypeDlg_FormClosing(object sender, FormClosingEventArgs e)
		{
			int _confId = 0;

			if (((NewConfTypeDlg)sender).DialogResult == DialogResult.OK)
			{
				if (radioButtonStatic.Checked)
				{
					int.TryParse(textEditConfId.Text, out _confId);
					if (_confId == 0)
					{
						MessageBox.Show(null, "Static conference ID cannot be 0.", MyVrmAddin.ProductDisplayName, MessageBoxButtons.OK,
						                MessageBoxIcon.Exclamation);

						e.Cancel = true;
					}
				}
				ConfId = _confId;
			}
		}
	}
}
