﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.Outlook.WinForms.Options
{
	partial class OptionsAsDlg
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.optionsControl = new MyVrm.Outlook.WinForms.Options.OptionsControl();
			((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).BeginInit();
			this.ContentPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// ContentPanel
			// 
			this.ContentPanel.Controls.Add(this.optionsControl);
			this.ContentPanel.Size = new System.Drawing.Size(375, 350);
			// 
			// optionsControl
			// 
			this.optionsControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.optionsControl.Location = new System.Drawing.Point(0, 0);
			this.optionsControl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.optionsControl.Name = "optionsControl";
			this.optionsControl.Size = new System.Drawing.Size(375, 350);
			this.optionsControl.TabIndex = 0;
			// 
			// OptionsAsDlg
			// 
			this.ApplyEnabled = true;
			this.ApplyVisible = true;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
			this.ClientSize = new System.Drawing.Size(399, 406);
			this.Name = "OptionsAsDlg";
			((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).EndInit();
			this.ContentPanel.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion
		public OptionsControl optionsControl;
	}
}
