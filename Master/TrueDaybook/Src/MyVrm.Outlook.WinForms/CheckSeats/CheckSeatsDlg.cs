﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraCharts;
using DevExpress.XtraEditors.Controls;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook.WinForms.CheckSeats
{
	public partial class CheckSeatsDlg : Dialog
	{
		const string LevelName = "MaxSeats";
		ToolTipController toolTipController = new ToolTipController();
		
		private int _maxAvailSeats { set; get; }
		public int MaxAvailSeats
		{
			set { _maxAvailSeats = value; }
			get { return _maxAvailSeats; }
		}

		public DateTime ConfStart = DateTime.Now;

		DateTime GetMinDataTime()
		{
			return ConfStart.Date.CompareTo(DateTime.Now.Date) < 0 ? ConfStart.Date : DateTime.Now;
		}

		internal DateTime _maxDate { set; get; }

		private string _confId;
		public string ConfId
		{
			set { _confId = value; }
			get { return _confId; }
		}

		public bool OpenOptionsDlg;
		public CheckSeatsDlg()
		{
			InitializeComponent();
			ApplyEnabled = false;
			ApplyVisible = false;
			OkEnabled = false;
			OkVisible = false;
			CancelVisible = true;
			CancelEnabled = true;
			CancelBnText = Strings.CloseBtnTxt;
			Text = Strings.CheckAvailableCaptionTxt;
			bnRefresh.Text = Strings.RefreshDisplayedDataBnTxt;

			//Customize displaying days 
			radioGroupDisplayMode.Properties.Items.Clear();
			int maxDays = 5;
			for (int i = 1; i <= maxDays; i++)
			{
				radioGroupDisplayMode.Properties.Items.Add( new RadioGroupItem( i, string.Format("{0} {1}", i, i == 1 ? Strings.TxtDay : Strings.TxtDays)));
			}
			OpenOptionsDlg = false;
		}

		private void CheckSeatsDlg_Load(object sender, EventArgs e)
		{
			XYDiagram diagram = (XYDiagram)chartControl.Diagram;

			diagram.AxisX.Range.AlwaysShowZeroLevel = true;
			diagram.AxisX.Range.SetInternalMinMaxValues(0, 24);
			diagram.AxisX.Range.SetMinMaxValues(GetMinDataTime().Date, ConfStart.AddDays(1).Date);

			_maxDate = ConfStart.AddMonths(1).Date;
			diagram.AxisX.Range.ScrollingRange.SetMinMaxValues(GetMinDataTime().Date, _maxDate);
			
			diagram.AxisX.DateTimeGridAlignment = DateTimeMeasurementUnit.Hour;
			diagram.AxisX.DateTimeMeasureUnit = DateTimeMeasurementUnit.Minute;

			diagram.AxisX.DateTimeOptions.Format = DateTimeFormat.Custom;
			diagram.AxisX.DateTimeOptions.FormatString = "HH:mm";

			var td = MyVrmService.TrueDayServices;// new TrueDayServices();
			try
			{
				//MaxAvailSeats = td.GetMaxAvail(false);
				MaxAvailSeats = MyVrmService.Service.TDGetMaximumAvailableSeats();

				diagram.AxisY.Range.AlwaysShowZeroLevel = true;
				diagram.AxisY.Range.SetMinMaxValues(0, ((MaxAvailSeats + 10)/10)*10);
					//correct top border to display data more accurate

				diagram.AxisY.ConstantLines.GetConstantLineByName(LevelName).AxisValue = MaxAvailSeats;
				diagram.AxisY.ConstantLines.GetConstantLineByName(LevelName).LegendText =
					string.Format("{0} ({1})", Strings.TxtMaximumAvailiableSeats, MaxAvailSeats);

				string note = !string.IsNullOrEmpty(ConfId) /*> 0*/ ? Strings.ExcludingCurrentConfLegendTxt : string.Empty;
				chartControl.Series[0].Name = Strings.SeatsAvailabilityLegendTxt + " " + note;

				diagram.SecondaryAxesX[0].DateTimeGridAlignment = DateTimeMeasurementUnit.Day;
				diagram.SecondaryAxesX[0].DateTimeMeasureUnit = DateTimeMeasurementUnit.Hour;
				diagram.SecondaryAxesX[0].DateTimeOptions.Format = DateTimeFormat.MonthAndDay;

				diagram.SecondaryAxesX[0].GridLines.Visible = true;
				diagram.SecondaryAxesX[0].Tickmarks.MinorVisible = false;
				diagram.SecondaryAxesX[0].Range.ScrollingRange.SetMinMaxValues(0,
				                                                               (_maxDate.Date.Subtract(GetMinDataTime().Date)).
				                                                               	TotalDays);
				AddSecondaryAxesXData(GetMinDataTime().Date);
				//diagram.SecondaryAxesX[0].Range.Auto = true;
				diagram.SecondaryAxesX[0].Range.ScrollingRange.Auto = true;

				diagram.AxisX.Range.MinValue = ConfStart;
				radioGroupDisplayMode.SelectedIndex = 4; //Set 5 day mode //0; //Set 1 day mode
				
			}
			catch (NoCredentialsException ex)
			{
				UIHelper.ShowError(ex.Message);
				OpenOptionsDlg = true;
				Close();
			}
			catch (Exception ex)
			{
				UIHelper.ShowError(ex.Message);
				Close();
			}
		}

		//Add secondary axes lables for a month transfered in param dateTime
		private void AddSecondaryAxesXData(DateTime dateTime)
		{
			XYDiagram diagram = (XYDiagram)chartControl.Diagram;

			DateTime rightMostDate = (DateTime) diagram.AxisX.Range.ScrollingRange.MaxValue;
			int numDays = (int)(rightMostDate.Subtract(dateTime.Date)).TotalDays;
			DateTime date = dateTime.Date;
			for (int day = diagram.SecondaryAxesX[0].CustomLabels.Count; day <= numDays; day++ )
			{
				diagram.SecondaryAxesX[0].CustomLabels.Add(new CustomAxisLabel(date.AddDays(day).ToString("D"), day));
			}
		}

		//Get data to display
		private void GetDateData(DateTime dt, bool doUpdate)
		{
			Cursor cursor = Cursor.Current;
			Cursor.Current = Cursors.WaitCursor;

			for (int i = 0; i < chartControl.Series[0].Points.Count; )
			{
				SeriesPoint pt = chartControl.Series[0].Points[i];
				if (pt.DateTimeArgument.Date.CompareTo(dt) == 0)
				{
					if (doUpdate == false) //already obtained - keep them, or delete & update (on Refresh bn)
					{
						//i++;
						//continue;//
						return;
					}
					else
					{
						chartControl.Series[0].Points.Remove(pt);
						continue;
					}
				}
				i++;
			}
			
			//TrueDaybookService_v2 trueDayServices = MyVrmService.TrueDayServices;
			AvailSeatsCollection2 ret = new AvailSeatsCollection2();
			try
			{
				//ret = trueDayServices.GetAvailSeats(dt, dt.AddDays(1).Date.Subtract(new TimeSpan(0,0,1,0)), ConfId, false); //???????????
				ret = MyVrmService.Service.TDGetAvailSeats(dt, dt.AddDays(1).Date.Subtract(new TimeSpan(0, 0, 1, 0)), ConfId);

				//chartControl.Series[0].Points.Clear();
				chartControl.Series[0].Points.Add(new SeriesPoint(dt.Date, 0));

				//chartControl.Series[0].Points.Add(new SeriesPoint(dt.Date.AddHours(1), 2));
				//chartControl.Series[0].Points.Add(new SeriesPoint(dt.Date.AddHours(3), 5));
				//chartControl.Series[0].Points.Add(new SeriesPoint(dt.Date.AddHours(4), 0));
				//chartControl.Series[0].Points.Add(new SeriesPoint(dt.Date.AddHours(5), 4));

				//chartControl.Series[0].Points.Add(new SeriesPoint(dt.AddDays(1).Date, 0));


				DateTime aDateTime = dt;
				if (ret != null)
				{
					foreach (AvailSeats2 data in ret)
					{
						aDateTime = data.Start;//dt.AddTicks(data.TimeSpan.Ticks);//aDateTime.AddTicks(data.TimeSpan.Ticks);
						SeriesPoint sp = new SeriesPoint(aDateTime, data.Seats);

						chartControl.Series[0].Points.Add(sp);
						//Application.DoEvents();
					}
				}
				Application.DoEvents();
				//chartControl.Series[0].Points.Add(new SeriesPoint(new DateTime(dt.Year, dt.Month, dt.Day, 23, 59, 59), 0));
			}
			catch (Exception ex)
			{
				UIHelper.ShowError(ex.Message);
			}
			finally
			{
				Cursor.Current = cursor;
			}
		}

		//Show/Hide series point data as a tooltip
		private void chartControl_ObjectHotTracked(object sender, HotTrackEventArgs e)
		{
			// Obtain the series point under the test point.
			SeriesPoint trackedPoint = e.HitInfo.SeriesPoint;
			SeriesPoint trackedPointAdd = e.AdditionalObject as SeriesPoint;

			bool bSet = false;
			if (e.HitInfo.InConstantLine)
			{
				toolTipController.ShowHint(string.Format("{0}: {1}", Strings.TxtMaximumAvailiableSeats, MaxAvailSeats));
				bSet = true;
			}

			if (!bSet && e.HitInfo.InSeries && (trackedPoint != null || trackedPointAdd != null)) 
			{
				SeriesPoint seriesPoint = trackedPoint ?? trackedPointAdd;
				
				int i = 0;
				int.TryParse(seriesPoint.Values.GetValue(0).ToString(), out i);
				toolTipController.ShowHint(string.Format("{0}, {1}", seriesPoint.DateTimeArgument.ToString("G"),
													i > 0 ? Strings.TxtBusySeats + i : Strings.TxtFreeSeats ));
				bSet = true;
			}
			//else
			if(!bSet )
			{
				toolTipController.HideHint();
			}
		}

		//Scroll processing
		private void chartControl_Scroll(object sender, ChartScrollEventArgs e)
		{
			XYDiagram diagram = (XYDiagram)chartControl.Diagram;
			bool isIncremented = false;
			bool isDecremented = false;
			int mode = (int)radioGroupDisplayMode.Properties.Items[radioGroupDisplayMode.SelectedIndex].Value;

			if (e.Type == ChartScrollEventType.ThumbPosition)
			{
				DateTime oldIncTime = e.OldAxisXRange != null ? (DateTime)(e.OldAxisXRange.MaxValue) : DateTime.FromOADate(0);
				DateTime newIncTime = e.OldAxisXRange != null ? (DateTime)(e.NewAxisXRange.MaxValue) : DateTime.FromOADate(0);
				isIncremented = oldIncTime.CompareTo(newIncTime) < 0 && oldIncTime.Day != newIncTime.Day;
				isDecremented = oldIncTime.CompareTo(newIncTime) > 0 && oldIncTime.Day != newIncTime.Day;
			}
			// ->
			if (e.Type == ChartScrollEventType.LargeIncrement || e.Type == ChartScrollEventType.SmallIncrement ||
					e.Type == ChartScrollEventType.ThumbPosition && isIncremented)
			{
				DateTime rightMostDataTime = (DateTime)(diagram.AxisX.Range.MaxValue);
				  
				diagram.AxisX.Range.MaxValue = ((DateTime) (diagram.AxisX.Range.MaxValue)).AddDays(1).Date;
				diagram.AxisX.Range.MinValue = ((DateTime)(diagram.AxisX.Range.MaxValue)).Date.Subtract(new TimeSpan(mode, 0, 0, 0));

				if (e.Type == ChartScrollEventType.LargeIncrement || e.Type == ChartScrollEventType.SmallIncrement)
					GetDateData(rightMostDataTime.Date, false); //just add next 1 day data
				else
					UpdateData(false); //add an interval

				int iDays = ((DateTime)diagram.AxisX.Range.MinValue).Date.Subtract(GetMinDataTime().Date).Days;
				diagram.SecondaryAxesX[0].Range.SetMinMaxValues(iDays, iDays + mode);
			}
			else // <-
			{
				if (e.Type == ChartScrollEventType.LargeDecrement || e.Type == ChartScrollEventType.SmallDecrement ||
						e.Type == ChartScrollEventType.ThumbPosition && isDecremented)
				{
					if (((DateTime)(diagram.AxisX.Range.MinValue)).Date.CompareTo(GetMinDataTime().Date) > 0)
					{
						if (e.Type == ChartScrollEventType.LargeDecrement || e.Type == ChartScrollEventType.ThumbPosition)
						{
							diagram.AxisX.Range.MinValue = ((DateTime) (diagram.AxisX.Range.MinValue)).Date;
							diagram.AxisX.Range.MaxValue = ((DateTime) (diagram.AxisX.Range.MaxValue)).Date;
						}
						else
						{
							diagram.AxisX.Range.MinValue = ((DateTime)(diagram.AxisX.Range.MinValue)).Date.Subtract(new TimeSpan(1, 0, 0, 0));
							diagram.AxisX.Range.MaxValue = ((DateTime)(diagram.AxisX.Range.MaxValue)).Date.Subtract(new TimeSpan(1, 0, 0, 0));
						}
					}
					if ((DateTime)diagram.AxisX.Range.MinValue >= GetMinDataTime().Date)
					{
						for (int i = 0; i < mode; i++)
						{
							GetDateData(((DateTime)(diagram.AxisX.Range.MinValue)).AddDays(i).Date, false);
						}
					}
					int iDays = ((DateTime)diagram.AxisX.Range.MinValue).Date.Subtract(GetMinDataTime().Date/*DateTime.Now.Date*/).Days;
					diagram.SecondaryAxesX[0].Range.SetMinMaxValues(iDays, iDays + mode);
				}
			}
			if(!isIncremented && !isDecremented) //ajust
			{
				if (((DateTime)diagram.AxisX.Range.MaxValue).Date == _maxDate.Date)
					diagram.AxisX.Range.MaxValue = ((DateTime) (diagram.AxisX.Range.MaxValue)).Date;
				if( ((DateTime) diagram.AxisX.Range.MinValue).Date == GetMinDataTime().Date)
					diagram.AxisX.Range.MinValue = ((DateTime)diagram.AxisX.Range.MinValue).Date;
			}
		}

		private void radioGroupDisplayMode_SelectedIndexChanged(object sender, EventArgs e)
		{
			int mode = (int) radioGroupDisplayMode.Properties.Items[radioGroupDisplayMode.SelectedIndex].Value;
			if(mode != 0)
			{
				XYDiagram diagram = (XYDiagram) chartControl.Diagram;
				diagram.AxisX.Range.MaxValue = ((DateTime)(diagram.AxisX.Range.MinValue)).AddDays(mode);
				int iDays = ((DateTime)diagram.AxisX.Range.MinValue).Date.Subtract(GetMinDataTime().Date).Days;
				diagram.SecondaryAxesX[0].Range.SetMinMaxValues(iDays, iDays + mode);
				for (int i = 0; i < mode; i++)
				{
					GetDateData(((DateTime)(diagram.AxisX.Range.MinValue)).AddDays(i).Date, false);
				}
			}
		}

		private void UpdateData(bool doUpdate)
		{
			XYDiagram diagram = (XYDiagram)chartControl.Diagram;
			int mode = (int)radioGroupDisplayMode.Properties.Items[radioGroupDisplayMode.SelectedIndex].Value;
			for (int i = 0; i < mode; i++)
			{
				GetDateData(((DateTime)(diagram.AxisX.Range.MinValue)).AddDays(i).Date, doUpdate);
			}
		}
		private void bnRefresh_Click(object sender, EventArgs e)
		{
			UpdateData(true);
		}

		/*
		//------ NOT USED because of UI seems like doesn't support multythreading
		void GetSeatsWithBW(DateTime dt, bool doUpdate)
		{
			MyVrmAddin.TraceSource.TraceInfoEx("GetSeatsWithBW In");
			Cursor cursor = Cursor.Current;
			Cursor.Current = Cursors.WaitCursor;

			try
			{
				while (backgroundWorkerSeats.IsBusy)
				{
					backgroundWorkerSeats.CancelAsync();
					Thread.Sleep(100);
					Application.DoEvents();
				}
				Dictionary<DateTime, bool> arg = new Dictionary<DateTime, bool>();
				arg.Add(dt, doUpdate);
				backgroundWorkerSeats.RunWorkerAsync(arg);
			}
			finally
			{
				Cursor.Current = cursor;
				MyVrmAddin.TraceSource.TraceInfoEx("GetSeatsWithBW Out");
			}
		}

		private void backgroundWorkerSeats_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
		{
			if (!backgroundWorkerSeats.CancellationPending)
			{
				//Application.DoEvents();
				Dictionary<DateTime, bool> arg = (Dictionary<DateTime, bool>)e.Argument;
				DateTime dt = arg.ElementAt(0).Key;
				bool doUpdate = arg.ElementAt(0).Value;
				//Application.DoEvents();
				GetDateData(dt, doUpdate);
				//Application.DoEvents();
			}
		}
		 * */
	}
}
