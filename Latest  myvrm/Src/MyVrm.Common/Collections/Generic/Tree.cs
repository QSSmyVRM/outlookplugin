﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.Common.Collections.Generic
{
    /// <summary>
    /// Represents a strongly typed tree of objects.
    /// </summary>
    /// <typeparam name="T">The type of elements in the tree.</typeparam>
    public class Tree<T>
    {
        private readonly TreeNodeList<T> _rootNodes = new TreeNodeList<T>(null);

        public TreeNodeList<T> Nodes
        {
            get { return _rootNodes; }
        }
    }
}
