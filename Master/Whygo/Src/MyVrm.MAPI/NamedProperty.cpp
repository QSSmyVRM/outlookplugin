#include "StdAfx.h"
#include "NamedProperty.h"

namespace MyVrm
{
	namespace MAPI
	{
		using namespace System::Runtime::InteropServices;

		NamedProperty::NamedProperty(System::Guid guid, int id, System::Type^ type)
		{
			this->kind = NamedPropertyKind::Id;
			this->guid = guid;
			this->id = id;
			this->type = type;
		}

		NamedProperty::NamedProperty(System::Guid guid, String^ name, System::Type^ type)
		{
			this->kind = NamedPropertyKind::String;
			this->guid = guid;
			this->name = name;
			this->type = type;
		}

		NamedPropertyKind NamedProperty::Kind::get()
		{
			return this->kind;
		}

		System::Guid NamedProperty::Guid::get()
		{
			return this->guid;
		}

		int NamedProperty::Id::get()
		{
			return this->id;
		}

		String^ NamedProperty::Name::get()
		{
			return this->name;
		}

		System::Type^ NamedProperty::Type::get()
		{
			return this->type;
		}

		int NamedProperty::GetBytesToMarshal()
		{
			int size = sizeof(MAPINAMEID);
			size += sizeof(GUID);
			if (Kind == NamedPropertyKind::String)
			{
				size += (Name->Length + 1) * sizeof(WCHAR);
			}
			return size;
		}

		void NamedProperty::MarshalToNative(MAPINAMEID* pPropName, BYTE** ppData)
		{
			BYTE* pData = (BYTE*)*ppData;
			*ppData += sizeof(GUID);
			array<Byte>^ guidArray =  this->guid.ToByteArray();
			Marshal::Copy(guidArray, 0, IntPtr(pData), guidArray->Length);
			pPropName->lpguid = (LPGUID)pData;
			pPropName->ulKind = (ULONG)Kind;
			if (Kind == NamedPropertyKind::String)
			{
				WCHAR* pName = (WCHAR*)*ppData;
				pPropName->Kind.lpwstrName = (LPWSTR)pName;
				ppData += (Name->Length + 1) * sizeof(WCHAR);
				for each(System::Char ch in Name)
				{
					pName[0] = ch;
					pName++;
				}
				pName[0] = '\0';
			}
			else
			{
				pPropName->Kind.lID = Id;
			}
		}
	}
}
