﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    public sealed class OccurrenceInfoCollection : ComplexPropertyCollection<OccurrenceInfo>
    {
        private const string InstanceElementName = "instance";
        private const string AppointmentTimeElementName = "appointmentTime";

        internal AppointmentTime AppointmentTime { get; set; }

		public override object Clone()
		{
			OccurrenceInfoCollection copy = new OccurrenceInfoCollection();
			foreach (var item in this)
			{
				copy.Add((OccurrenceInfo)item.Clone()); //???????
			}

			return copy;
		}

        public void Add(OccurrenceInfo occurrenceInfo)
        {
            InternalAdd(occurrenceInfo);
        }

        public bool Remove(OccurrenceInfo occurrenceInfo)
        {
            return InternalRemove(occurrenceInfo);
        }

		public void Clear()
		{
			InternalClear();
		}

        #region Overrides of ComplexPropertyCollection<OccurrenceInfo>

        internal override OccurrenceInfo CreateComplexProperty(string xmlElementName)
        {
            return xmlElementName == InstanceElementName ? new OccurrenceInfo() : null;
        }

        internal override string GetCollectionItemXmlElementName(OccurrenceInfo complexProperty)
        {
            return InstanceElementName;
        }

        internal override void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
        {
            reader.EnsureCurrentNodeIsStartElement(XmlNamespace.NotSpecified, xmlElementName);
            if (!reader.IsEmptyElement)
            {
                do
                {
                    reader.Read();
                    if (reader.IsStartElement())
                    {
                        var complexProperty = CreateComplexProperty(reader.LocalName);
                        if (complexProperty != null)
                        {
                            complexProperty.LoadFromXml(reader, reader.LocalName);
                            InternalAdd(complexProperty);
                        }
                        else if (reader.LocalName == AppointmentTimeElementName)
                        {
                            AppointmentTime = new AppointmentTime();
                            AppointmentTime.LoadFromXml(reader, AppointmentTimeElementName);
                        }
                        else
                        {
                            reader.SkipCurrentElement();
                        }
                    }
                }
                while (!reader.IsEndElement(XmlNamespace.NotSpecified, xmlElementName));
            }
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            AppointmentTime.WriteToXml(writer, AppointmentTimeElementName);
            base.WriteElementsToXml(writer);
        }

        #endregion
    }
}
