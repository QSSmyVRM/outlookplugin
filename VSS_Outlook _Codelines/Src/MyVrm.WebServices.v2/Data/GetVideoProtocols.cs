﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	internal class GetVideoProtocolsRequest : ServiceRequestBase<GetVideoProtocolsResponse>
	{
		public GetVideoProtocolsRequest(MyVrmService service)
			: base(service)
		{
		}

		#region Overrides of ServiceRequestBase

		internal override string GetXmlElementName()
		{
			return "GetVideoProtocols";
		}

		internal override string GetCommandName()
		{
			return "GetVideoProtocols";
		}

		internal override string GetResponseXmlElementName()
		{
			return "GetVideoProtocols";
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			UserId.WriteToXml(writer, "UserID");
		}

		#endregion
	}
	//
	public class GetVideoProtocolsResponse : ServiceResponse
	{
		private readonly Dictionary<int, string> _videoProtocols = new Dictionary<int, string>();

		public Dictionary<int, string> VideoProtocols
		{
			get
			{
				return new Dictionary<int, string>(_videoProtocols);
			}
		}

		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{
			base.ReadElementsFromXml(reader);
			_videoProtocols.Clear();
			do
			{
				reader.Read();
				if (reader.IsStartElement(XmlNamespace.NotSpecified, "Protocol"))
				{
					int i = reader.ReadElementValue<int>(XmlNamespace.NotSpecified, "ID");
					string name = reader.ReadElementValue(XmlNamespace.NotSpecified, "Name");
					_videoProtocols.Add(i, name);
				}
				else
				{
					reader.SkipCurrentElement();
				}
			} while (!reader.IsEndElement(XmlNamespace.NotSpecified, "GetVideoProtocols"));
		}
	}
}