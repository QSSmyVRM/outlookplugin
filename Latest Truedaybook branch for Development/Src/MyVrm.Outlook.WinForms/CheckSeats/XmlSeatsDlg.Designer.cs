﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.Outlook.WinForms
{
	partial class XmlSeatsDlg
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
			this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.labelColor5 = new DevExpress.XtraEditors.LabelControl();
			this.labelColor1 = new DevExpress.XtraEditors.LabelControl();
			this.memoText3 = new DevExpress.XtraEditors.MemoEdit();
			this.memoText4 = new DevExpress.XtraEditors.MemoEdit();
			this.labelColor4 = new DevExpress.XtraEditors.LabelControl();
			this.labelColor3 = new DevExpress.XtraEditors.LabelControl();
			this.labelColor2 = new DevExpress.XtraEditors.LabelControl();
			this.memoText2 = new DevExpress.XtraEditors.MemoEdit();
			this.memoText1 = new DevExpress.XtraEditors.MemoEdit();
			this.memoText5 = new DevExpress.XtraEditors.MemoEdit();
			this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
			this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
			this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
			this.multiEditorRowProperties1 = new DevExpress.XtraVerticalGrid.Rows.MultiEditorRowProperties();
			this.multiEditorRowProperties2 = new DevExpress.XtraVerticalGrid.Rows.MultiEditorRowProperties();
			this.multiEditorRowProperties3 = new DevExpress.XtraVerticalGrid.Rows.MultiEditorRowProperties();
			((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).BeginInit();
			this.ContentPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
			this.layoutControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
			this.layoutControl2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.memoText3.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.memoText4.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.memoText2.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.memoText1.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.memoText5.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
			this.SuspendLayout();
			// 
			// ContentPanel
			// 
			this.ContentPanel.Controls.Add(this.layoutControl1);
			this.ContentPanel.Size = new System.Drawing.Size(1167, 194);
			// 
			// layoutControl1
			// 
			this.layoutControl1.Controls.Add(this.layoutControl2);
			this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.layoutControl1.Location = new System.Drawing.Point(0, 0);
			this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.layoutControl1.Name = "layoutControl1";
			this.layoutControl1.Root = this.layoutControlGroup1;
			this.layoutControl1.Size = new System.Drawing.Size(1167, 194);
			this.layoutControl1.TabIndex = 0;
			this.layoutControl1.Text = "layoutControl1";
			// 
			// layoutControl2
			// 
			this.layoutControl2.Controls.Add(this.label4);
			this.layoutControl2.Controls.Add(this.label3);
			this.layoutControl2.Controls.Add(this.label2);
			this.layoutControl2.Controls.Add(this.label1);
			this.layoutControl2.Controls.Add(this.labelColor5);
			this.layoutControl2.Controls.Add(this.labelColor1);
			this.layoutControl2.Controls.Add(this.memoText3);
			this.layoutControl2.Controls.Add(this.memoText4);
			this.layoutControl2.Controls.Add(this.labelColor4);
			this.layoutControl2.Controls.Add(this.labelColor3);
			this.layoutControl2.Controls.Add(this.labelColor2);
			this.layoutControl2.Controls.Add(this.memoText2);
			this.layoutControl2.Controls.Add(this.memoText1);
			this.layoutControl2.Controls.Add(this.memoText5);
			this.layoutControl2.Location = new System.Drawing.Point(12, 12);
			this.layoutControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.layoutControl2.Name = "layoutControl2";
			this.layoutControl2.Root = this.layoutControlGroup2;
			this.layoutControl2.Size = new System.Drawing.Size(1143, 170);
			this.layoutControl2.TabIndex = 4;
			this.layoutControl2.Text = "layoutControl2";
			// 
			// label4
			// 
			this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(240)))), ((int)(((byte)(116)))));
			this.label4.Location = new System.Drawing.Point(886, 12);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(44, 20);
			this.label4.TabIndex = 19;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(715, 12);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(167, 20);
			this.label3.TabIndex = 18;
			this.label3.Text = "Totally Free";
			// 
			// label2
			// 
			this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(254)))), ((int)(((byte)(101)))));
			this.label2.Location = new System.Drawing.Point(651, 12);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(50, 20);
			this.label2.TabIndex = 17;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(944, 12);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(187, 20);
			this.label1.TabIndex = 16;
			this.label1.Text = "Partially Free";
			// 
			// labelColor5
			// 
			this.labelColor5.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(240)))), ((int)(((byte)(116)))));
			this.labelColor5.Appearance.Options.UseBackColor = true;
			this.labelColor5.Location = new System.Drawing.Point(908, 101);
			this.labelColor5.Name = "labelColor5";
			this.labelColor5.Size = new System.Drawing.Size(223, 57);
			this.labelColor5.StyleController = this.layoutControl2;
			this.labelColor5.TabIndex = 15;
			// 
			// labelColor1
			// 
			this.labelColor1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(254)))), ((int)(((byte)(101)))));
			this.labelColor1.Appearance.Options.UseBackColor = true;
			this.labelColor1.Location = new System.Drawing.Point(12, 101);
			this.labelColor1.Name = "labelColor1";
			this.labelColor1.Size = new System.Drawing.Size(209, 57);
			this.labelColor1.StyleController = this.layoutControl2;
			this.labelColor1.TabIndex = 10;
			// 
			// memoText3
			// 
			this.memoText3.Location = new System.Drawing.Point(453, 40);
			this.memoText3.Name = "memoText3";
			this.memoText3.Properties.AcceptsReturn = false;
			this.memoText3.Properties.AllowFocused = false;
			this.memoText3.Properties.ReadOnly = true;
			this.memoText3.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
			this.memoText3.Size = new System.Drawing.Size(223, 57);
			this.memoText3.StyleController = this.layoutControl2;
			this.memoText3.TabIndex = 9;
			// 
			// memoText4
			// 
			this.memoText4.Location = new System.Drawing.Point(680, 40);
			this.memoText4.Name = "memoText4";
			this.memoText4.Properties.AcceptsReturn = false;
			this.memoText4.Properties.AllowFocused = false;
			this.memoText4.Properties.ReadOnly = true;
			this.memoText4.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
			this.memoText4.Size = new System.Drawing.Size(224, 57);
			this.memoText4.StyleController = this.layoutControl2;
			this.memoText4.TabIndex = 7;
			// 
			// labelColor4
			// 
			this.labelColor4.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(254)))), ((int)(((byte)(101)))));
			this.labelColor4.Appearance.Options.UseBackColor = true;
			this.labelColor4.Location = new System.Drawing.Point(680, 101);
			this.labelColor4.Name = "labelColor4";
			this.labelColor4.Size = new System.Drawing.Size(224, 57);
			this.labelColor4.StyleController = this.layoutControl2;
			this.labelColor4.TabIndex = 14;
			// 
			// labelColor3
			// 
			this.labelColor3.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(254)))), ((int)(((byte)(101)))));
			this.labelColor3.Appearance.Options.UseBackColor = true;
			this.labelColor3.Location = new System.Drawing.Point(453, 101);
			this.labelColor3.Name = "labelColor3";
			this.labelColor3.Size = new System.Drawing.Size(223, 57);
			this.labelColor3.StyleController = this.layoutControl2;
			this.labelColor3.TabIndex = 13;
			// 
			// labelColor2
			// 
			this.labelColor2.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(240)))), ((int)(((byte)(116)))));
			this.labelColor2.Appearance.Options.UseBackColor = true;
			this.labelColor2.Location = new System.Drawing.Point(225, 101);
			this.labelColor2.Name = "labelColor2";
			this.labelColor2.Size = new System.Drawing.Size(224, 57);
			this.labelColor2.StyleController = this.layoutControl2;
			this.labelColor2.TabIndex = 12;
			// 
			// memoText2
			// 
			this.memoText2.Location = new System.Drawing.Point(225, 40);
			this.memoText2.Name = "memoText2";
			this.memoText2.Properties.AcceptsReturn = false;
			this.memoText2.Properties.AllowFocused = false;
			this.memoText2.Properties.ReadOnly = true;
			this.memoText2.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
			this.memoText2.Size = new System.Drawing.Size(224, 57);
			this.memoText2.StyleController = this.layoutControl2;
			this.memoText2.TabIndex = 8;
			// 
			// memoText1
			// 
           
			this.memoText1.EditValue = "\r\n\r\n    43\r\n    54\r\n    65";
			this.memoText1.Location = new System.Drawing.Point(12, 40);
			this.memoText1.Name = "memoText1";
			this.memoText1.Properties.AcceptsReturn = false;
			this.memoText1.Properties.AllowFocused = false;
			this.memoText1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
			this.memoText1.Properties.Appearance.BackColor = System.Drawing.Color.White;
			this.memoText1.Properties.Appearance.Options.UseBackColor = true;
			this.memoText1.Properties.ReadOnly = true;
			this.memoText1.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
			this.memoText1.Size = new System.Drawing.Size(209, 57);
			this.memoText1.StyleController = this.layoutControl2;
			this.memoText1.TabIndex = 5;
			// 
			// memoText5
			// 
			this.memoText5.EditValue = "";
			this.memoText5.Location = new System.Drawing.Point(908, 40);
			this.memoText5.Name = "memoText5";
			this.memoText5.Properties.AcceptsReturn = false;
			this.memoText5.Properties.AllowFocused = false;
			this.memoText5.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
			this.memoText5.Properties.ReadOnly = true;
			this.memoText5.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
			this.memoText5.Size = new System.Drawing.Size(223, 57);
			this.memoText5.StyleController = this.layoutControl2;
			this.memoText5.TabIndex = 6;
			// 
			// layoutControlGroup2
			// 
			this.layoutControlGroup2.AppearanceGroup.BackColor = System.Drawing.Color.White;
			this.layoutControlGroup2.AppearanceGroup.Options.UseBackColor = true;
			this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
			this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
			this.layoutControlGroup2.GroupBordersVisible = false;
			this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem2,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem8,
            this.emptySpaceItem1,
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.emptySpaceItem2});
			this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup2.Name = "Root";
			this.layoutControlGroup2.Size = new System.Drawing.Size(1143, 170);
			this.layoutControlGroup2.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup2.Text = "Root";
			this.layoutControlGroup2.TextVisible = false;
			// 
			// layoutControlItem3
			// 
			this.layoutControlItem3.Control = this.memoText1;
			this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
			this.layoutControlItem3.Location = new System.Drawing.Point(0, 28);
			this.layoutControlItem3.MinSize = new System.Drawing.Size(1, 1);
			this.layoutControlItem3.Name = "layoutControlItem3";
			this.layoutControlItem3.Size = new System.Drawing.Size(213, 61);
			this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem3.Text = "layoutControlItem3";
			this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem3.TextToControlDistance = 0;
			this.layoutControlItem3.TextVisible = false;
			// 
			// layoutControlItem4
			// 
			this.layoutControlItem4.Control = this.memoText5;
			this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
			this.layoutControlItem4.Location = new System.Drawing.Point(896, 28);
			this.layoutControlItem4.MinSize = new System.Drawing.Size(1, 1);
			this.layoutControlItem4.Name = "layoutControlItem4";
			this.layoutControlItem4.Size = new System.Drawing.Size(227, 61);
			this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem4.Text = "layoutControlItem4";
			this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem4.TextToControlDistance = 0;
			this.layoutControlItem4.TextVisible = false;
			// 
			// layoutControlItem2
			// 
			this.layoutControlItem2.Control = this.memoText4;
			this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
			this.layoutControlItem2.Location = new System.Drawing.Point(668, 28);
			this.layoutControlItem2.MinSize = new System.Drawing.Size(1, 1);
			this.layoutControlItem2.Name = "layoutControlItem2";
			this.layoutControlItem2.Size = new System.Drawing.Size(228, 61);
			this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem2.Text = "layoutControlItem2";
			this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem2.TextToControlDistance = 0;
			this.layoutControlItem2.TextVisible = false;
			// 
			// layoutControlItem5
			// 
			this.layoutControlItem5.Control = this.memoText2;
			this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
			this.layoutControlItem5.Location = new System.Drawing.Point(213, 28);
			this.layoutControlItem5.MinSize = new System.Drawing.Size(1, 1);
			this.layoutControlItem5.Name = "layoutControlItem5";
			this.layoutControlItem5.Size = new System.Drawing.Size(228, 61);
			this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem5.Text = "layoutControlItem5";
			this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem5.TextToControlDistance = 0;
			this.layoutControlItem5.TextVisible = false;
			// 
			// layoutControlItem6
			// 
			this.layoutControlItem6.Control = this.memoText3;
			this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
			this.layoutControlItem6.Location = new System.Drawing.Point(441, 28);
			this.layoutControlItem6.MinSize = new System.Drawing.Size(1, 1);
			this.layoutControlItem6.Name = "layoutControlItem6";
			this.layoutControlItem6.Size = new System.Drawing.Size(227, 61);
			this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem6.Text = "layoutControlItem6";
			this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem6.TextToControlDistance = 0;
			this.layoutControlItem6.TextVisible = false;
			// 
			// layoutControlItem7
			// 
			this.layoutControlItem7.Control = this.labelColor1;
			this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
			this.layoutControlItem7.Location = new System.Drawing.Point(0, 89);
			this.layoutControlItem7.MinSize = new System.Drawing.Size(4, 20);
			this.layoutControlItem7.Name = "layoutControlItem7";
			this.layoutControlItem7.Size = new System.Drawing.Size(213, 61);
			this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem7.Text = "layoutControlItem7";
			this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem7.TextToControlDistance = 0;
			this.layoutControlItem7.TextVisible = false;
			// 
			// layoutControlItem9
			// 
			this.layoutControlItem9.Control = this.labelColor2;
			this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
			this.layoutControlItem9.Location = new System.Drawing.Point(213, 89);
			this.layoutControlItem9.MinSize = new System.Drawing.Size(79, 20);
			this.layoutControlItem9.Name = "layoutControlItem9";
			this.layoutControlItem9.Size = new System.Drawing.Size(228, 61);
			this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem9.Text = "layoutControlItem9";
			this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem9.TextToControlDistance = 0;
			this.layoutControlItem9.TextVisible = false;
			// 
			// layoutControlItem10
			// 
			this.layoutControlItem10.Control = this.labelColor3;
			this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
			this.layoutControlItem10.Location = new System.Drawing.Point(441, 89);
			this.layoutControlItem10.MinSize = new System.Drawing.Size(79, 20);
			this.layoutControlItem10.Name = "layoutControlItem10";
			this.layoutControlItem10.Size = new System.Drawing.Size(227, 61);
			this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem10.Text = "layoutControlItem10";
			this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem10.TextToControlDistance = 0;
			this.layoutControlItem10.TextVisible = false;
			// 
			// layoutControlItem11
			// 
			this.layoutControlItem11.Control = this.labelColor4;
			this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
			this.layoutControlItem11.Location = new System.Drawing.Point(668, 89);
			this.layoutControlItem11.MinSize = new System.Drawing.Size(79, 20);
			this.layoutControlItem11.Name = "layoutControlItem11";
			this.layoutControlItem11.Size = new System.Drawing.Size(228, 61);
			this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem11.Text = "layoutControlItem11";
			this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem11.TextToControlDistance = 0;
			this.layoutControlItem11.TextVisible = false;
			// 
			// layoutControlItem8
			// 
			this.layoutControlItem8.Control = this.labelColor5;
			this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
			this.layoutControlItem8.Location = new System.Drawing.Point(896, 89);
			this.layoutControlItem8.MinSize = new System.Drawing.Size(79, 20);
			this.layoutControlItem8.Name = "layoutControlItem8";
			this.layoutControlItem8.Size = new System.Drawing.Size(227, 61);
			this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem8.Text = "layoutControlItem8";
			this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem8.TextToControlDistance = 0;
			this.layoutControlItem8.TextVisible = false;
			// 
			// emptySpaceItem1
			// 
			this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
			this.emptySpaceItem1.Location = new System.Drawing.Point(0, 24);
			this.emptySpaceItem1.MinSize = new System.Drawing.Size(1, 1);
			this.emptySpaceItem1.Name = "emptySpaceItem1";
			this.emptySpaceItem1.Size = new System.Drawing.Size(1123, 4);
			this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.emptySpaceItem1.Text = "emptySpaceItem1";
			this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
			// 
			// layoutControlItem12
			// 
			this.layoutControlItem12.Control = this.label1;
			this.layoutControlItem12.CustomizationFormText = "layoutControlItem12";
			this.layoutControlItem12.Location = new System.Drawing.Point(922, 0);
			this.layoutControlItem12.MinSize = new System.Drawing.Size(24, 24);
			this.layoutControlItem12.Name = "layoutControlItem12";
			this.layoutControlItem12.Size = new System.Drawing.Size(201, 24);
			this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem12.Spacing = new DevExpress.XtraLayout.Utils.Padding(10, 0, 0, 0);
			this.layoutControlItem12.Text = "layoutControlItem12";
			this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem12.TextToControlDistance = 0;
			this.layoutControlItem12.TextVisible = false;
			// 
			// layoutControlItem13
			// 
			this.layoutControlItem13.Control = this.label2;
			this.layoutControlItem13.CustomizationFormText = "layoutControlItem13";
			this.layoutControlItem13.Location = new System.Drawing.Point(639, 0);
			this.layoutControlItem13.MinSize = new System.Drawing.Size(24, 24);
			this.layoutControlItem13.Name = "layoutControlItem13";
			this.layoutControlItem13.Size = new System.Drawing.Size(54, 24);
			this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem13.Text = "layoutControlItem13";
			this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem13.TextToControlDistance = 0;
			this.layoutControlItem13.TextVisible = false;
			// 
			// layoutControlItem14
			// 
			this.layoutControlItem14.Control = this.label3;
			this.layoutControlItem14.CustomizationFormText = "layoutControlItem14";
			this.layoutControlItem14.Location = new System.Drawing.Point(693, 0);
			this.layoutControlItem14.MinSize = new System.Drawing.Size(24, 24);
			this.layoutControlItem14.Name = "layoutControlItem14";
			this.layoutControlItem14.Size = new System.Drawing.Size(181, 24);
			this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem14.Spacing = new DevExpress.XtraLayout.Utils.Padding(10, 0, 0, 0);
			this.layoutControlItem14.Text = "layoutControlItem14";
			this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem14.TextToControlDistance = 0;
			this.layoutControlItem14.TextVisible = false;
			// 
			// layoutControlItem15
			// 
			this.layoutControlItem15.Control = this.label4;
			this.layoutControlItem15.CustomizationFormText = "layoutControlItem15";
			this.layoutControlItem15.Location = new System.Drawing.Point(874, 0);
			this.layoutControlItem15.MinSize = new System.Drawing.Size(24, 24);
			this.layoutControlItem15.Name = "layoutControlItem15";
			this.layoutControlItem15.Size = new System.Drawing.Size(48, 24);
			this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem15.Text = "layoutControlItem15";
			this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem15.TextToControlDistance = 0;
			this.layoutControlItem15.TextVisible = false;
			// 
			// emptySpaceItem2
			// 
			this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
			this.emptySpaceItem2.Location = new System.Drawing.Point(0, 0);
			this.emptySpaceItem2.MinSize = new System.Drawing.Size(1, 1);
			this.emptySpaceItem2.Name = "emptySpaceItem2";
			this.emptySpaceItem2.Size = new System.Drawing.Size(639, 24);
			this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.emptySpaceItem2.Text = "emptySpaceItem2";
			this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
			// 
			// layoutControlGroup1
			// 
			this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
			this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
			this.layoutControlGroup1.GroupBordersVisible = false;
			this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
			this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup1.Name = "layoutControlGroup1";
			this.layoutControlGroup1.Size = new System.Drawing.Size(1167, 194);
			this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup1.Text = "layoutControlGroup1";
			this.layoutControlGroup1.TextVisible = false;
			// 
			// layoutControlItem1
			// 
			this.layoutControlItem1.Control = this.layoutControl2;
			this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
			this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem1.MinSize = new System.Drawing.Size(216, 24);
			this.layoutControlItem1.Name = "layoutControlItem1";
			this.layoutControlItem1.Size = new System.Drawing.Size(1147, 174);
			this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem1.Text = "layoutControlItem1";
			this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem1.TextToControlDistance = 0;
			this.layoutControlItem1.TextVisible = false;
			// 
			// multiEditorRowProperties1
			// 
			this.multiEditorRowProperties1.CustomizationCaption = null;
			this.multiEditorRowProperties1.Value = "1 str";
			// 
			// multiEditorRowProperties2
			// 
			this.multiEditorRowProperties2.CustomizationCaption = null;
			this.multiEditorRowProperties2.ReadOnly = true;
			this.multiEditorRowProperties2.Value = "2 str";
			// 
			// multiEditorRowProperties3
			// 
			this.multiEditorRowProperties3.CustomizationCaption = null;
			this.multiEditorRowProperties3.ReadOnly = true;
			this.multiEditorRowProperties3.Value = "3 str";
			// 
			// XmlSeatsDlg
			// 
			this.ApplyEnabled = true;
			this.ApplyVisible = true;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.ClientSize = new System.Drawing.Size(1191, 250);
			this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.Name = "XmlSeatsDlg";
			this.Load += new System.EventHandler(this.SeatsXML_Load);
			((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).EndInit();
			this.ContentPanel.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
			this.layoutControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
			this.layoutControl2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.memoText3.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.memoText4.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.memoText2.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.memoText1.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.memoText5.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private DevExpress.XtraLayout.LayoutControl layoutControl1;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private DevExpress.XtraLayout.LayoutControl layoutControl2;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
		private DevExpress.XtraVerticalGrid.Rows.MultiEditorRowProperties multiEditorRowProperties1;
		private DevExpress.XtraVerticalGrid.Rows.MultiEditorRowProperties multiEditorRowProperties2;
		private DevExpress.XtraVerticalGrid.Rows.MultiEditorRowProperties multiEditorRowProperties3;
		private DevExpress.XtraEditors.MemoEdit memoText1;
		private DevExpress.XtraEditors.MemoEdit memoText5;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
		private DevExpress.XtraEditors.MemoEdit memoText4;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
		private DevExpress.XtraEditors.MemoEdit memoText3;
		private DevExpress.XtraEditors.MemoEdit memoText2;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
		private DevExpress.XtraEditors.LabelControl labelColor1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
		private DevExpress.XtraEditors.LabelControl labelColor4;
		private DevExpress.XtraEditors.LabelControl labelColor3;
		private DevExpress.XtraEditors.LabelControl labelColor2;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
		private DevExpress.XtraEditors.LabelControl labelColor5;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
		private System.Windows.Forms.Label label1;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
	}
}
