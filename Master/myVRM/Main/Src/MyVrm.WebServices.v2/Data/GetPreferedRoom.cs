﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	//Request
	class GetPreferedRoomRequest : ServiceRequestBase<GetPreferedRoomResponse>
	{
		public string RoomList;

		internal GetPreferedRoomRequest(MyVrmService service)
			: base(service)
		{
		}

		#region Overrides of ServiceRequestBase<GetTemplateResponse>

		internal override string GetXmlElementName()
		{
			return "login";
		}

		internal override string GetCommandName()
		{
			return Constants.GetPreferedRoomCommandName; 
		}

		internal override string GetResponseXmlElementName()
		{
			return "GetPreferedRoom"; 
		}

		internal override GetPreferedRoomResponse ParseResponse(MyVrmServiceXmlReader reader)
		{
			var response = new GetPreferedRoomResponse();
			response.ReadElementsFromXml(reader);
			return response;
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			if (UserId != null)
				UserId.WriteToXml(writer, "userID");
		}

		#endregion
	}

	//Response
	public class GetPreferedRoomResponse : ServiceResponse
	{
		//Tags
		const string Room = "Room";
		const string Rooms = "Rooms";
		const string RoomId = "RoomID";

		//Result
		public List<RoomId> RoomIds = new List<RoomId>();

		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{
			bool IsEmpltyList = false;
			//Forward till rooms tag
			while (!reader.IsStartElement(XmlNamespace.NotSpecified, Rooms))
			{
				//If no favorites - get out
				if (reader.IsEndElement(XmlNamespace.NotSpecified, "GetPreferedRoom"))
				{
					IsEmpltyList = true;
					break;
				} 
				reader.Read();
			}
			if (!IsEmpltyList)
			{
				do
				{
					reader.Read();
					if (reader.IsStartElement(XmlNamespace.NotSpecified, Room))
					{
						reader.Read();
						if (reader.LocalName == RoomId)
						{
							int iRet = 0;
							int.TryParse(reader.ReadElementValue(), out iRet);
							RoomIds.Add(new RoomId(iRet.ToString()));
						}
					}
					else
					{
						reader.SkipCurrentElement();
					}
				} while (!reader.IsEndElement(XmlNamespace.NotSpecified, Rooms));
			}
		}
	}
}
