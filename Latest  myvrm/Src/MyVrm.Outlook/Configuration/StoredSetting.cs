﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System.Configuration;
using System.Runtime.InteropServices;
using System.Xml;

namespace MyVrm.Outlook.Configuration
{
    [StructLayout(LayoutKind.Sequential)]
    internal struct StoredSetting
    {
        internal StoredSetting(SettingsSerializeAs serializeAs, XmlNode value) : this()
        {
            SerializeAs = serializeAs;
            Value = value;
        }

        internal SettingsSerializeAs SerializeAs { get; set; }
        internal XmlNode Value { get; set; }
    }

}
