﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	[Serializable]
	public class PublicFields : ComplexProperty
	{
		public int WhygoRoomID;
		public string Address;
		public string AUXEquipment;
		public List<int> AUXEquipmentList;
		public int Speed;
		public /*int*/string Type;
		public string Description;
		public string ExtraNotes;
        public string GenericSellPrice;
        public string GeoCodeAddress;
        public string InternetBiller;
        public string InternetPriceCurrency;
        public int IsAutomatic;
        public int IsHDCapable;
        public int IsInternetCapable;
        public int IsInternetFree;
        public int IsIPCapable;
		public int IsISDNCapable;
		public string ImgLink;
        public int IsIPCConnectionCapable;
        public int IsIPDedicated;
        public int IsTP;
        public int isVCCapable;
        public string CurrencyType;
        public int IsEarlyHoursEnabled;
        public string EarlyHoursStart;
        public string EarlyHoursEnd;
        public string EarlyHoursCost;
		public string OpenHour; 
		public int IsOfficeHoursEnabled;
        public string OfficeHoursStart;
        public string OfficeHoursEnd;
        public string OfficeHoursCost;
        public int IsAfterHourEnabled;
        public string AfterHoursStart;
        public string AfterHoursEnd;
        public string AfterHoursCost;
        public int IsCrazyHoursSupported;
        public string CrazyHoursStart;
        public string CrazyHoursEnd;
        public string CrazyHoursCost;
		public int Is24HoursEnabled;
		public int State;
		public int Country;
 // <Country>0</Country> - duplication from the same tag from the upper level
		public List<int> CateringOptionsList;//<CateringOptions>1,3</CateringOptions> 
		public string CateringOptions;
		public int Layout; //<Layout>1</Layout> 
		public string DefaultEquipment;//<DefaultEquipment>some eq</DefaultEquipment> 
		public ResponsiblePerson SiteCordinator;
		public ResponsiblePerson Manager;
		public ResponsiblePerson TechnicalContact;
		public string ipSpeed; //<Speed>768</Speed>
		public string ipAddress; //<Address>127.0.0.1</Address>
		public string isdnSpeed; //<Speed>768</Speed>
		public string isdnAddress;
		public string RoomImageName;
		public string RoomImages;
//- <IP>
//  <Speed>512</Speed> 
//  <Address>512</Address> 
//  </IP>
//- <ISDN>
//  <Speed>1</Speed> 
//  <Address>128</Address> 
//  </ISDN>
		public string PublicRoomLastModified;//<PublicRoomLastModified>7/18/2012 6:06:24 PM</PublicRoomLastModified> 

		public override object Clone()
		{
			PublicFields copy = new PublicFields();
			copy.WhygoRoomID = WhygoRoomID;
			copy.Address = Address;
			copy.AUXEquipment = AUXEquipment;
			copy.AUXEquipmentList = new List<int>(AUXEquipmentList); 
			copy.Speed = Speed;
			copy.Type = Type;
			copy.Description = Description;
			copy.ExtraNotes = ExtraNotes;
			copy.GenericSellPrice = GenericSellPrice;
			copy.GeoCodeAddress = GeoCodeAddress;
			copy.InternetBiller = InternetBiller;
			copy.InternetPriceCurrency = InternetPriceCurrency;
			copy.IsAutomatic = IsAutomatic;
			copy.IsHDCapable = IsHDCapable;
			copy.IsInternetCapable = IsInternetCapable;
			copy.IsInternetFree = IsInternetFree;
			copy.IsIPCapable = IsIPCapable;
			copy.IsISDNCapable = IsISDNCapable;
			copy.ImgLink = ImgLink;
			copy.IsIPCConnectionCapable = IsIPCConnectionCapable;
			copy.IsIPDedicated = IsIPDedicated;
			copy.IsTP = IsTP;
			copy.isVCCapable = isVCCapable;
			copy.CurrencyType = CurrencyType;
			copy.IsEarlyHoursEnabled = IsEarlyHoursEnabled;
			copy.EarlyHoursStart = EarlyHoursStart;
			copy.EarlyHoursEnd = EarlyHoursEnd;
			copy.EarlyHoursCost = EarlyHoursCost;
			copy.OpenHour = OpenHour; //suppose IsOfficeHoursSupported indeed !!!!!!!!!!!!????????????
			copy.IsOfficeHoursEnabled = IsOfficeHoursEnabled;
			copy.OfficeHoursStart = OfficeHoursStart;
			copy.OfficeHoursEnd = OfficeHoursEnd;
			copy.OfficeHoursCost = OfficeHoursCost;
			copy.IsAfterHourEnabled = IsAfterHourEnabled;
			copy.AfterHoursStart = AfterHoursStart;
			copy.AfterHoursEnd = AfterHoursEnd;
			copy.AfterHoursCost = AfterHoursCost;
			copy.IsCrazyHoursSupported = IsCrazyHoursSupported;
			copy.CrazyHoursStart = CrazyHoursStart;
			copy.CrazyHoursEnd = CrazyHoursEnd;
			copy.CrazyHoursCost = CrazyHoursCost;
			copy.Is24HoursEnabled = Is24HoursEnabled;

			copy.State = State;
			copy.Country = Country;
			copy.CateringOptionsList = new List<int>(CateringOptionsList);
			copy.CateringOptions = CateringOptions;
			copy.Layout = Layout; 
			copy.DefaultEquipment = DefaultEquipment;
			copy.SiteCordinator = SiteCordinator;
			copy.Manager = Manager;
			copy.TechnicalContact = TechnicalContact;
			copy.ipSpeed = ipSpeed; 
			copy.ipAddress = ipAddress; 
			copy.isdnSpeed = isdnSpeed; 
			copy.isdnAddress = isdnAddress;
			copy.RoomImageName = RoomImageName;
			copy.RoomImages = RoomImages;
			return copy;
		}

		internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
		{
			switch (reader.LocalName)
			{
				case "WhygoRoomID":
					{
						WhygoRoomID = reader.ReadElementValue<int>();
						return true;
					}
				case "Address":
					{
						Address = reader.ReadElementValue();
						return true;
					}
				case "AUXEquipment":
					{
						AUXEquipment = reader.ReadElementValue();
						AUXEquipmentList = new List<int>();
						string[] ret = AUXEquipment.Split(new[] { ',' });
						int res;
						foreach (var s in ret)
						{
							int.TryParse(s, out res);
							if(res != 0)
								AUXEquipmentList.Add(res);
						}
						return true;
					}

				case "Speed":
					{
						Speed = reader.ReadElementValue<int>();
						return true;
					}
				case "Type":
					{
						Type = reader.ReadElementValue(); //Video/Audio 
						return true;
					}
				case "Description":
					{
						Description = reader.ReadElementValue();
						return true;
					}
				case "ExtraNotes":
					{
						ExtraNotes = reader.ReadElementValue();
						return true;
					}
				case "GenericSellPrice":
					{
						GenericSellPrice = reader.ReadElementValue();
						return true;
					}
				case "GeoCodeAddress":
					{
						GeoCodeAddress = reader.ReadElementValue();
						return true;
					}
				case "InternetBiller":
					{
						InternetBiller = reader.ReadElementValue();
						return true;
					}
				case "InternetPriceCurrency":
					{
						InternetPriceCurrency = reader.ReadElementValue();
						return true;
					}
				case "IsAutomatic":
					{
						IsAutomatic = reader.ReadElementValue<int>();
						return true;
					}
				case "IsHDCapable":
					{
						IsHDCapable = reader.ReadElementValue<int>();
						return true;
					}
				case "IsInternetCapable":
					{
						IsInternetCapable = reader.ReadElementValue<int>();
						return true;
					}
				case "IsInternetFree":
					{
						IsInternetFree = reader.ReadElementValue<int>();
						return true;
					}
				case "IsIPCapable":
					{
						IsIPCapable = reader.ReadElementValue<int>();
						return true;
					}
				case "IsISDNCapable":
					{
						IsISDNCapable = reader.ReadElementValue<int>();
						return true;
					}
				case "ImgLink":
					{
						ImgLink = reader.ReadElementValue();
						return true;
					}
				case "IsIPCConnectionCapable":
					{
						IsIPCConnectionCapable = reader.ReadElementValue<int>();
						return true;
					}
				case "IsIPDedicated":
					{
						IsIPDedicated = reader.ReadElementValue<int>();
						return true;
					}
				case "IsTP":
					{
						IsTP = reader.ReadElementValue<int>();
						return true;
					}
				case "isVCCapable":
					{
						isVCCapable = reader.ReadElementValue<int>();
						return true;
					}
				case "CurrencyType":
					{
						CurrencyType = reader.ReadElementValue();
						return true;
					}
				case "IsEarlyHoursEnabled":
					{
						IsEarlyHoursEnabled = reader.ReadElementValue<int>();
						return true;
					}
				case "EarlyHoursStart":
					{
						EarlyHoursStart = reader.ReadElementValue();
						return true;
					}
				case "EarlyHoursEnd":
					{
						EarlyHoursEnd = reader.ReadElementValue();
						return true;
					}
				case "EarlyHoursCost":
					{
						EarlyHoursCost = reader.ReadElementValue();
						return true;
					}
				case "OpenHour":
					{
						OpenHour = reader.ReadElementValue();
						//int.TryParse(OpenHour, out IsOfficeHoursSupported);
						return true;
					}
				case "IsOfficeHoursEnabled":
					{
						IsOfficeHoursEnabled = reader.ReadElementValue<int>();
						return true;
					}
				case "OfficeHoursStart":
					{
						OfficeHoursStart = reader.ReadElementValue();
						return true;
					}
				case "OfficeHoursEnd":
					{
						OfficeHoursEnd = reader.ReadElementValue();
						return true;
					}
				case "OfficeHoursCost":
					{
						OfficeHoursCost = reader.ReadElementValue();
						return true;
					}
				case "IsAfterHourEnabled":
					{
						IsAfterHourEnabled = reader.ReadElementValue<int>();
						return true;
					}
				case "AfterHoursStart":
					{
						AfterHoursStart = reader.ReadElementValue();
						return true;
					}
				case "AfterHoursEnd":
					{
						AfterHoursEnd = reader.ReadElementValue();
						return true;
					}
				case "AfterHoursCost":
					{
						AfterHoursCost = reader.ReadElementValue();
						return true;
					}
				case "IsCrazyHoursSupported":
					{
						IsCrazyHoursSupported = reader.ReadElementValue<int>();
						return true;
					}
				case "CrazyHoursStart":
					{
						CrazyHoursStart = reader.ReadElementValue();
						return true;
					}
				case "CrazyHoursEnd":
					{
						CrazyHoursEnd = reader.ReadElementValue();
						return true;
					}
				case "CrazyHoursCost":
					{
						CrazyHoursCost = reader.ReadElementValue();
						return true;
					}
				case "Is24HoursEnabled":
					{
						Is24HoursEnabled = reader.ReadElementValue<int>();
						return true;
					}
				case "State" :
					{
						string val = reader.ReadElementValue();
						int.TryParse(val, out State);
						//State = reader.ReadElementValue<int>();
						return true;
					}
				case "Country":
					{
						string val = reader.ReadElementValue();
						int.TryParse(val, out Country);
						//Country = reader.ReadElementValue<int>();
						return true;
					}
				case "CateringOptions" :
					{
						CateringOptions = reader.ReadElementValue();
						CateringOptionsList = new List<int>();
						string[] ret = CateringOptions.Split(new [] {','});
						int res;
						foreach (var s in ret)
						{
							int.TryParse(s, out res);
							if(res != 0)
								CateringOptionsList.Add(res);
						}
						return true;
					}
				case "Layout" :
					{
						string val = reader.ReadElementValue();
						int.TryParse(val, out Layout);
						//Layout = reader.ReadElementValue<int>();
						return true;
					}
				case "DefaultEquipment" :
					{
						DefaultEquipment = reader.ReadElementValue();
						return true;
					}
				case "SiteCordinator" :
					{
						SiteCordinator = new ResponsiblePerson(WhyGoManagedRoom.GetPerson(WhyGoManagedRoom.PersonType.SiteCordinator));
						reader.Read();
						SiteCordinator.Name = reader.ReadElementValue();
						reader.SkipCurrentElement();
						reader.Read();
						SiteCordinator.Email = reader.ReadElementValue();
						reader.SkipCurrentElement();
						reader.Read();
						SiteCordinator.Phone = reader.ReadElementValue();
						return true;
					}
				case "Manager" :
					{
						Manager = new ResponsiblePerson(WhyGoManagedRoom.GetPerson(WhyGoManagedRoom.PersonType.Manager));
						reader.Read();
						Manager.Name = reader.ReadElementValue();
						reader.SkipCurrentElement();
						reader.Read();
						Manager.Email = reader.ReadElementValue();
						reader.SkipCurrentElement();
						reader.Read();
						Manager.Phone = reader.ReadElementValue();
						return true;
					}
				case "TechnicalContact" :
					{
						TechnicalContact = new ResponsiblePerson(WhyGoManagedRoom.GetPerson(WhyGoManagedRoom.PersonType.TechnicalContact));
						reader.Read();
						TechnicalContact.Name = reader.ReadElementValue();
						reader.SkipCurrentElement();
						reader.Read();
						TechnicalContact.Email = reader.ReadElementValue();
						reader.SkipCurrentElement();
						reader.Read();
						TechnicalContact.Phone = reader.ReadElementValue();
						return true;
					}
				case "IP" :
					{
						reader.Read();
						ipSpeed = reader.ReadElementValue();
						reader.SkipCurrentElement();
						reader.Read();
						ipAddress = reader.ReadElementValue();
						return true;
					}
				case "ISDN" :
					{
						reader.Read();
						isdnSpeed = reader.ReadElementValue();
						reader.SkipCurrentElement();
						reader.Read();
						isdnAddress = reader.ReadElementValue();
						return true;
					}
				case "RoomImageName" :
					{
						RoomImageName = reader.ReadElementValue();
						return true;
					}
				case "RoomImages":
					{
						RoomImages = reader.ReadElementValue();
						return true;
					}
			}
			return false;
		}

		internal void AddToListNonNull(List<string> list, string str)
		{
			if (!string.IsNullOrEmpty(str))
				list.Add(str);
		}

		public override string ToString()
		{
			var list = new List<string>();
			
			AddToListNonNull(list, WhygoRoomID.ToString());
			AddToListNonNull(list, Address);
			AddToListNonNull(list, AUXEquipment);
			AddToListNonNull(list, Speed.ToString());
			AddToListNonNull(list, Type);
			AddToListNonNull(list, Description);
			AddToListNonNull(list, ExtraNotes);
			AddToListNonNull(list, GenericSellPrice);
			AddToListNonNull(list, GeoCodeAddress);
			AddToListNonNull(list, InternetBiller);
			AddToListNonNull(list, InternetPriceCurrency);
			AddToListNonNull(list, IsAutomatic.ToString());
			AddToListNonNull(list, IsHDCapable.ToString());
			AddToListNonNull(list, IsInternetCapable.ToString());
			AddToListNonNull(list, IsInternetFree.ToString());
			AddToListNonNull(list, IsIPCapable.ToString());
			AddToListNonNull(list, IsISDNCapable.ToString());
			AddToListNonNull(list, ImgLink);
			AddToListNonNull(list, IsIPCConnectionCapable.ToString());
			AddToListNonNull(list, IsIPDedicated.ToString());
			AddToListNonNull(list, IsTP.ToString());
			AddToListNonNull(list, isVCCapable.ToString());
			AddToListNonNull(list, CurrencyType);
			AddToListNonNull(list, IsEarlyHoursEnabled.ToString());
			AddToListNonNull(list, EarlyHoursStart);
			AddToListNonNull(list, EarlyHoursEnd);
			AddToListNonNull(list, EarlyHoursCost);
			AddToListNonNull(list, OpenHour); //suppose IsOfficeHoursSupported indeed !!!!!!!!!!!!????????????
			AddToListNonNull(list, IsOfficeHoursEnabled.ToString());
			AddToListNonNull(list, OfficeHoursStart);
			AddToListNonNull(list, OfficeHoursEnd);
			AddToListNonNull(list, OfficeHoursCost);
			AddToListNonNull(list, IsAfterHourEnabled.ToString());
			AddToListNonNull(list, AfterHoursStart);
			AddToListNonNull(list, AfterHoursEnd);
			AddToListNonNull(list, AfterHoursCost);
			AddToListNonNull(list, IsCrazyHoursSupported.ToString());
			AddToListNonNull(list, CrazyHoursStart);
			AddToListNonNull(list, CrazyHoursEnd);
			AddToListNonNull(list, CrazyHoursCost);
			AddToListNonNull(list, Is24HoursEnabled.ToString());
			AddToListNonNull(list, State.ToString());
			AddToListNonNull(list, Country.ToString());
			AddToListNonNull(list, CateringOptions);
			AddToListNonNull(list, Layout.ToString());
			AddToListNonNull(list,  DefaultEquipment);
			AddToListNonNull(list, SiteCordinator.Name);
			AddToListNonNull(list, SiteCordinator.Email);
			AddToListNonNull(list, SiteCordinator.Phone);
			AddToListNonNull(list, Manager.Name);
			AddToListNonNull(list, Manager.Email);
			AddToListNonNull(list, Manager.Phone);
			AddToListNonNull(list, TechnicalContact.Name);
			AddToListNonNull(list, TechnicalContact.Email);
			AddToListNonNull(list, TechnicalContact.Phone);
			AddToListNonNull(list, ipSpeed);
			AddToListNonNull(list, ipAddress);
			AddToListNonNull(list, isdnSpeed);
			AddToListNonNull(list, isdnAddress);
			AddToListNonNull(list, RoomImageName);
			AddToListNonNull(list, RoomImages); 

			return string.Join(", ", list.ToArray());
		}
	}
}
