﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Microsoft.Office.Interop.Outlook;

namespace MyVrm.Outlook
{
    public class ObjectKeyedByWindowCollection<TObject> : ReadOnlyCollection<TObject> where TObject : class
    {
        private readonly Dictionary<object, IList<TObject>> _objectsByWindow = new Dictionary<object, IList<TObject>>();
        private readonly Dictionary<TObject, object> _objectToWindow = new Dictionary<TObject, object>();

        public ObjectKeyedByWindowCollection(IList<TObject> list)
            : base(list)
        {
        }

        public void Add(TObject obj, object key)
        {
            if (obj == null)
                throw new ArgumentNullException("obj");
            if (key == null)
                throw new ArgumentNullException("key");
            if (_objectToWindow.ContainsKey(obj))
            {
                throw new ArgumentException(string.Empty, "obj");
            }
            Items.Add(obj);
            AddToMultiFormDictionary(obj, key, _objectsByWindow);
            _objectToWindow[obj] = key;
        }

        public bool Remove(TObject item)
        {
            var removed = Items.Remove(item);
            if (removed)
            {
                var keyPairForItem = GetKeyPairForItem(item);
                _objectToWindow.Remove(item);
                _objectsByWindow.Remove(keyPairForItem.Key);
            }
            return removed;
        }

        public bool TryGetValue(Inspector inspector, out IList<TObject> value)
        {
            return _objectsByWindow.TryGetValue(inspector, out value);
        }

        private void AddToMultiFormDictionary(TObject form, object key, IDictionary<object, IList<TObject>> dictionary)
        {
            IList<TObject> list = null;
            if (!dictionary.TryGetValue(key, out list))
            {
                list = new List<TObject>();
                dictionary.Add(key, list);
            }
            list.Add(form);
        }

        private KeyValuePair<object, TObject> GetKeyPairForItem(TObject item)
        {
            return new KeyValuePair<object, TObject>(_objectToWindow[item], item);
        }
    }
}
