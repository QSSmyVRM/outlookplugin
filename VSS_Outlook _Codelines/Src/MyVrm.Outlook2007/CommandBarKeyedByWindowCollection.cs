﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Outlook;

namespace MyVrm.Outlook
{
    public class CommandBarKeyedByWindowCollection : ObjectKeyedByWindowCollection<OfficeCommandBar>
    {
        public CommandBarKeyedByWindowCollection() : base(new List<OfficeCommandBar>())
        {
        }
    }
}
