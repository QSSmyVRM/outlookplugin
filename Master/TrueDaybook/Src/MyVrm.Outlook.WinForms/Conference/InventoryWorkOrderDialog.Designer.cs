﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.Outlook.WinForms.Conference
{
    partial class InventoryWorkOrderDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.deliveryTypeComboBox = new MyVrm.Outlook.WinForms.EnumComboBoxEdit();
            this.workOrderItemsList = new DevExpress.XtraTreeList.TreeList();
            this.itemNameColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.serialNumberColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.itemImageColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.itemImageRepositoryItemPictureEdit = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.commentsColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemMemoEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.descriptionColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.itemQuantityColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.requestedQuantityColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.quantityRepositoryItemSpinEdit = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.roomSetComboBox = new DevExpress.XtraEditors.ComboBoxEdit();
            this.roomNameLabel = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).BeginInit();
            this.ContentPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deliveryTypeComboBox.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.workOrderItemsList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemImageRepositoryItemPictureEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.quantityRepositoryItemSpinEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.roomSetComboBox.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // ContentPanel
            // 
            this.ContentPanel.Controls.Add(this.layoutControl1);
            this.ContentPanel.Size = new System.Drawing.Size(600, 388);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.deliveryTypeComboBox);
            this.layoutControl1.Controls.Add(this.workOrderItemsList);
            this.layoutControl1.Controls.Add(this.roomSetComboBox);
            this.layoutControl1.Controls.Add(this.roomNameLabel);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(600, 388);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // deliveryTypeComboBox
            // 
            this.deliveryTypeComboBox.Location = new System.Drawing.Point(72, 22);
            this.deliveryTypeComboBox.Name = "deliveryTypeComboBox";
            this.deliveryTypeComboBox.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deliveryTypeComboBox.Properties.NullText = "Please select...";
            this.deliveryTypeComboBox.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.deliveryTypeComboBox.Size = new System.Drawing.Size(194, 20);
            this.deliveryTypeComboBox.StyleController = this.layoutControl1;
            this.deliveryTypeComboBox.TabIndex = 7;
            // 
            // workOrderItemsList
            // 
            this.workOrderItemsList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.itemNameColumn,
            this.serialNumberColumn,
            this.itemImageColumn,
            this.commentsColumn,
            this.descriptionColumn,
            this.itemQuantityColumn,
            this.requestedQuantityColumn});
            this.workOrderItemsList.Location = new System.Drawing.Point(2, 46);
            this.workOrderItemsList.Name = "workOrderItemsList";
            this.workOrderItemsList.OptionsView.ShowButtons = false;
            this.workOrderItemsList.OptionsView.ShowIndicator = false;
            this.workOrderItemsList.OptionsView.ShowRoot = false;
            this.workOrderItemsList.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.quantityRepositoryItemSpinEdit,
            this.itemImageRepositoryItemPictureEdit,
            this.repositoryItemMemoEdit});
            this.workOrderItemsList.Size = new System.Drawing.Size(596, 332);
            this.workOrderItemsList.TabIndex = 6;
            // 
            // itemNameColumn
            // 
            this.itemNameColumn.Caption = "Name";
            this.itemNameColumn.FieldName = "Name";
            this.itemNameColumn.Name = "itemNameColumn";
            this.itemNameColumn.OptionsColumn.AllowEdit = false;
            this.itemNameColumn.OptionsColumn.AllowFocus = false;
            this.itemNameColumn.OptionsColumn.AllowMove = false;
            this.itemNameColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.itemNameColumn.OptionsColumn.ReadOnly = true;
            this.itemNameColumn.OptionsColumn.ShowInCustomizationForm = false;
            this.itemNameColumn.Visible = true;
            this.itemNameColumn.VisibleIndex = 0;
            this.itemNameColumn.Width = 68;
            // 
            // serialNumberColumn
            // 
            this.serialNumberColumn.Caption = "Serial #";
            this.serialNumberColumn.FieldName = "SerialNumber";
            this.serialNumberColumn.Name = "serialNumberColumn";
            this.serialNumberColumn.OptionsColumn.AllowEdit = false;
            this.serialNumberColumn.OptionsColumn.AllowFocus = false;
            this.serialNumberColumn.OptionsColumn.AllowMove = false;
            this.serialNumberColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.serialNumberColumn.OptionsColumn.ReadOnly = true;
            this.serialNumberColumn.OptionsColumn.ShowInCustomizationForm = false;
            this.serialNumberColumn.Visible = true;
            this.serialNumberColumn.VisibleIndex = 1;
            this.serialNumberColumn.Width = 78;
            // 
            // itemImageColumn
            // 
            this.itemImageColumn.Caption = "Image";
            this.itemImageColumn.ColumnEdit = this.itemImageRepositoryItemPictureEdit;
            this.itemImageColumn.FieldName = "Image";
            this.itemImageColumn.Name = "itemImageColumn";
            this.itemImageColumn.OptionsColumn.AllowEdit = false;
            this.itemImageColumn.OptionsColumn.AllowFocus = false;
            this.itemImageColumn.OptionsColumn.AllowMove = false;
            this.itemImageColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.itemImageColumn.OptionsColumn.ReadOnly = true;
            this.itemImageColumn.OptionsColumn.ShowInCustomizationForm = false;
            this.itemImageColumn.Visible = true;
            this.itemImageColumn.VisibleIndex = 2;
            this.itemImageColumn.Width = 70;
            // 
            // itemImageRepositoryItemPictureEdit
            // 
            this.itemImageRepositoryItemPictureEdit.Name = "itemImageRepositoryItemPictureEdit";
            this.itemImageRepositoryItemPictureEdit.ReadOnly = true;
            this.itemImageRepositoryItemPictureEdit.ShowMenu = false;
            this.itemImageRepositoryItemPictureEdit.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            // 
            // commentsColumn
            // 
            this.commentsColumn.Caption = "Comments";
            this.commentsColumn.ColumnEdit = this.repositoryItemMemoEdit;
            this.commentsColumn.FieldName = "Comments";
            this.commentsColumn.Name = "commentsColumn";
            this.commentsColumn.OptionsColumn.AllowEdit = false;
            this.commentsColumn.OptionsColumn.AllowFocus = false;
            this.commentsColumn.OptionsColumn.AllowMove = false;
            this.commentsColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.commentsColumn.OptionsColumn.ReadOnly = true;
            this.commentsColumn.OptionsColumn.ShowInCustomizationForm = false;
            this.commentsColumn.Visible = true;
            this.commentsColumn.VisibleIndex = 3;
            this.commentsColumn.Width = 90;
            // 
            // repositoryItemMemoEdit
            // 
            this.repositoryItemMemoEdit.LinesCount = 3;
            this.repositoryItemMemoEdit.Name = "repositoryItemMemoEdit";
            this.repositoryItemMemoEdit.ReadOnly = true;
            // 
            // descriptionColumn
            // 
            this.descriptionColumn.Caption = "Description";
            this.descriptionColumn.ColumnEdit = this.repositoryItemMemoEdit;
            this.descriptionColumn.FieldName = "Description";
            this.descriptionColumn.Name = "descriptionColumn";
            this.descriptionColumn.OptionsColumn.AllowEdit = false;
            this.descriptionColumn.OptionsColumn.AllowFocus = false;
            this.descriptionColumn.OptionsColumn.AllowMove = false;
            this.descriptionColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.descriptionColumn.OptionsColumn.ReadOnly = true;
            this.descriptionColumn.OptionsColumn.ShowInCustomizationForm = false;
            this.descriptionColumn.Visible = true;
            this.descriptionColumn.VisibleIndex = 4;
            this.descriptionColumn.Width = 93;
            // 
            // itemQuantityColumn
            // 
            this.itemQuantityColumn.Caption = "Quantity in Hand";
            this.itemQuantityColumn.FieldName = "Quantity";
            this.itemQuantityColumn.Name = "itemQuantityColumn";
            this.itemQuantityColumn.OptionsColumn.AllowEdit = false;
            this.itemQuantityColumn.OptionsColumn.AllowFocus = false;
            this.itemQuantityColumn.OptionsColumn.AllowMove = false;
            this.itemQuantityColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.itemQuantityColumn.OptionsColumn.ReadOnly = true;
            this.itemQuantityColumn.OptionsColumn.ShowInCustomizationForm = false;
            this.itemQuantityColumn.Visible = true;
            this.itemQuantityColumn.VisibleIndex = 5;
            this.itemQuantityColumn.Width = 121;
            // 
            // requestedQuantityColumn
            // 
            this.requestedQuantityColumn.Caption = "Requested Quantity";
            this.requestedQuantityColumn.ColumnEdit = this.quantityRepositoryItemSpinEdit;
            this.requestedQuantityColumn.FieldName = "RequestedQuantity";
            this.requestedQuantityColumn.Name = "requestedQuantityColumn";
            this.requestedQuantityColumn.OptionsColumn.AllowMove = false;
            this.requestedQuantityColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.requestedQuantityColumn.OptionsColumn.ShowInCustomizationForm = false;
            this.requestedQuantityColumn.Visible = true;
            this.requestedQuantityColumn.VisibleIndex = 6;
            this.requestedQuantityColumn.Width = 137;
            // 
            // quantityRepositoryItemSpinEdit
            // 
            this.quantityRepositoryItemSpinEdit.AutoHeight = false;
            this.quantityRepositoryItemSpinEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.quantityRepositoryItemSpinEdit.IsFloatValue = false;
            this.quantityRepositoryItemSpinEdit.Mask.EditMask = "N00";
            this.quantityRepositoryItemSpinEdit.Name = "quantityRepositoryItemSpinEdit";
            this.quantityRepositoryItemSpinEdit.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.quantityRepositoryItemSpinEdit_EditValueChanging);
            // 
            // roomSetComboBox
            // 
            this.roomSetComboBox.Location = new System.Drawing.Point(340, 2);
            this.roomSetComboBox.Name = "roomSetComboBox";
            this.roomSetComboBox.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.roomSetComboBox.Properties.NullText = "Select one...";
            this.roomSetComboBox.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.roomSetComboBox.Size = new System.Drawing.Size(173, 20);
            this.roomSetComboBox.StyleController = this.layoutControl1;
            this.roomSetComboBox.TabIndex = 5;
            this.roomSetComboBox.SelectedIndexChanged += new System.EventHandler(this.roomSetComboBox_SelectedIndexChanged);
            // 
            // roomNameLabel
            // 
            this.roomNameLabel.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.roomNameLabel.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.roomNameLabel.Appearance.Options.UseBackColor = true;
            this.roomNameLabel.Appearance.Options.UseFont = true;
            this.roomNameLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.roomNameLabel.Location = new System.Drawing.Point(72, 2);
            this.roomNameLabel.Name = "roomNameLabel";
            this.roomNameLabel.Size = new System.Drawing.Size(194, 16);
            this.roomNameLabel.StyleController = this.layoutControl1;
            this.roomNameLabel.TabIndex = 4;
            this.roomNameLabel.Text = "[Room]";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 8);
            this.layoutControlGroup1.Size = new System.Drawing.Size(600, 388);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.Control = this.roomNameLabel;
            this.layoutControlItem1.CustomizationFormText = "Room";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(268, 20);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(268, 20);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(268, 20);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "Room";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(66, 16);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.workOrderItemsList;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 44);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(600, 336);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.roomSetComboBox;
            this.layoutControlItem2.CustomizationFormText = "Set Name";
            this.layoutControlItem2.Location = new System.Drawing.Point(268, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(247, 44);
            this.layoutControlItem2.Text = "Set Name";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(66, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.deliveryTypeComboBox;
            this.layoutControlItem4.CustomizationFormText = "Delivery Type";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 20);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(268, 24);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(268, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(268, 24);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "Delivery Type";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(66, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(515, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(85, 44);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // AudioVisualWorkOrderDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(624, 444);
            this.MinimumSize = new System.Drawing.Size(640, 480);
            this.Name = "InventoryWorkOrderDialog";
            this.Text = "Work Order";
            this.Load += new System.EventHandler(this.AudioVisualWorkOrderDialog_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AudioVisualWorkOrderDialog_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).EndInit();
            this.ContentPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.deliveryTypeComboBox.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.workOrderItemsList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemImageRepositoryItemPictureEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.quantityRepositoryItemSpinEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.roomSetComboBox.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.LabelControl roomNameLabel;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.ComboBoxEdit roomSetComboBox;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraTreeList.TreeList workOrderItemsList;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraTreeList.Columns.TreeListColumn itemNameColumn;
        private DevExpress.XtraTreeList.Columns.TreeListColumn serialNumberColumn;
        private DevExpress.XtraTreeList.Columns.TreeListColumn itemImageColumn;
        private DevExpress.XtraTreeList.Columns.TreeListColumn commentsColumn;
        private DevExpress.XtraTreeList.Columns.TreeListColumn descriptionColumn;
        private DevExpress.XtraTreeList.Columns.TreeListColumn itemQuantityColumn;
        private DevExpress.XtraTreeList.Columns.TreeListColumn requestedQuantityColumn;
        private MyVrm.Outlook.WinForms.EnumComboBoxEdit deliveryTypeComboBox;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit quantityRepositoryItemSpinEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit itemImageRepositoryItemPictureEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
    }
}
