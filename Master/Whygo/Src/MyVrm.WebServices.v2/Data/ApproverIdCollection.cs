﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System.Collections;
using System.Collections.Generic;
using System.Xml;

namespace MyVrm.WebServices.Data
{
    public class ApproverIdCollection : ComplexProperty, IEnumerable<int>
    {
        private readonly List<int> _approverIds;

		public override object Clone()
		{
			ApproverIdCollection copy = new ApproverIdCollection();
			foreach (var item in _approverIds)
			{
				copy._approverIds.Add(item); //???????
			}

			return copy;
		}

        internal ApproverIdCollection()
        {
            _approverIds = new List<int>();
        }

        public int Count
        {
            get
            {
                return _approverIds.Count;
            }
        }

        public int this[int index]
        {
            get
            {
                return _approverIds[index];
            }
        }

        #region Implementation of IEnumerable

        public IEnumerator<int> GetEnumerator()
        {
            return _approverIds.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        internal override void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
        {
            reader.EnsureCurrentNodeIsStartElement(XmlNamespace.NotSpecified, xmlElementName);
            if (!reader.IsEmptyElement)
            {
                do
                {
                    reader.Read();
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        _approverIds.Add(reader.ReadElementValue<int>(XmlNamespace.NotSpecified, "ID"));
                    }
                } while (!reader.IsEndElement(XmlNamespace.NotSpecified, xmlElementName));
            }
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            foreach (var approverId in _approverIds)
            {
                writer.WriteElementValue(XmlNamespace.NotSpecified, "ID", approverId);
            }
            writer.WriteStartElement(XmlNamespace.NotSpecified, "approver");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "ID", null);
            writer.WriteEndElement();
            writer.WriteStartElement(XmlNamespace.NotSpecified, "approver"); 
            writer.WriteElementValue(XmlNamespace.NotSpecified, "ID", null);
            writer.WriteEndElement();
            writer.WriteStartElement(XmlNamespace.NotSpecified, "approver"); 
            writer.WriteElementValue(XmlNamespace.NotSpecified, "ID", null);
            writer.WriteEndElement();
        }
    }
}
