﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using DevExpress.XtraTab;
using MyVrm.WebServices.Data;
using MyVrm.Common.ComponentModel;

namespace MyVrm.Outlook.WinForms.Conference
{
    public partial class ConferenceControlBase : BaseControl
    {
        private ConferenceWrapper _conference;

        private readonly List<ConferencePage> _pages = new List<ConferencePage>();

        public ConferenceControlBase()
        {
            InitializeComponent();

            conferenceBindingSource.DataSource = typeof (ConferenceWrapper);

            conferenceBindingSource.DataSourceChanged += ConferenceBindingSourceDataSourceChanged;
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
         Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        public ConferenceWrapper Conference
        {
            get
            {
                return _conference;
            }
            set
            {
                if (_conference != value)
                {
                    if (_conference != null)
                    {
                        _conference.PropertyChanged -= ConferencePropertyChanged;
                        _conference.BeforeSave -= ConferenceBeforeSave;
                    	_conference.ExternalUpdate -= ConferenceExternalUpdate;
                    }
                    _conference = value;
                    conferenceBindingSource.DataSource = _conference;
                    if (_conference != null)
                    {
                        _conference.PropertyChanged += ConferencePropertyChanged;
                        _conference.BeforeSave += ConferenceBeforeSave;
						_conference.ExternalUpdate += ConferenceExternalUpdate;
                    }
                }
            }
        }

        protected List<ConferencePage> Pages
        {
            get { return _pages; }
        }


        private void Apply()
        {
            var e = new CancelEventArgs(false);
            AudioVideoConferenceAVSettingsPage _audioVideoConferenceAVSettingsPage = new AudioVideoConferenceAVSettingsPage();
            var cursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            bool isRoomConference = false;
            DataList<string> Allpages = new DataList<string>();
            try
            {
                
               if (MyVrmService.Service.IsAVEnabled == false || MyVrmService.Service.OrganizationOptions.IsAdvancedAudioVideoParamsEnabled == false) //ZD 101838 104336 
                   AddPage(_audioVideoConferenceAVSettingsPage);

                   for (int i = 0; i < Pages.Count;i++)
                   {
                       Allpages.Add(Pages[i].ToString());
                   }
                   if (!Allpages.Contains("MyVrm.Outlook.WinForms.Conference.AudioVideoConferenceAVSettingsPage"))
                   { 
                   //if (!Pages.Contains(_audioVideoConferenceAVSettingsPage)) //ZD 104336 start{
                       isRoomConference = true;
                       AddPage(_audioVideoConferenceAVSettingsPage);
                   }//ZD 104336  End

                foreach (var page in Pages)
                {
                   
                    page.Apply(e);
                    if (e.Cancel)
                    {
                        throw new SaveConferenceCancelledException();
                    }
                }
                if (MyVrmService.Service.IsAVEnabled == false || MyVrmService.Service.OrganizationOptions.IsAdvancedAudioVideoParamsEnabled == false || isRoomConference == true) //ZD 101838 //ZD 104336
                   RemovePage(_audioVideoConferenceAVSettingsPage);

            }
            finally
            {
                Cursor.Current = cursor;
                if (MyVrmService.Service.IsAVEnabled == false || MyVrmService.Service.OrganizationOptions.IsAdvancedAudioVideoParamsEnabled == false || isRoomConference == true) //ZD 101838 //ZD 104336
                    RemovePage(_audioVideoConferenceAVSettingsPage);
            }
        }

        protected void RemovePage(ConferencePage page)
        {
            XtraTabPage tabPageToRemove = null;

            foreach (XtraTabPage tabPage in tabControl.TabPages)
            {
                if (tabPage.Controls.Contains(page))
                {
                    tabPageToRemove = tabPage;
                    break;
                }
            }
            if (tabPageToRemove != null)
            {
                tabControl.TabPages.Remove(tabPageToRemove);
                Pages.Remove(page);
            }
        }

        protected virtual void ResetPages()
        {
        }

		protected void AddPage(ConferencePage page)
		{
			var tabPage = new XtraTabPage {Text = page.Text};
			page.ConferenceBindingSource.DataSource = conferenceBindingSource.DataSource;
			page.Dock = DockStyle.Fill;
			tabPage.Controls.Add(page);
			tabControl.TabPages.Add(tabPage);
			Pages.Add(page);
		}

    	private void ConferenceBindingSourceDataSourceChanged(object sender, EventArgs e)
        {
            var cursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                ResetPages();
                Pages.ForEach(page =>
                              {
                                  page.ReadOnly = !_conference.IsAllowedToModify;
                                  page.ConferenceBindingSource.DataSource =
                                      conferenceBindingSource.DataSource;
                              });
            }
            finally
            {
                Cursor.Current = cursor;
            }
        }

        private void ConferencePropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch(e.PropertyName)
            {
                case "Type":
				case "VMRType":
                {
                    ResetPages();
                    break;
                }
            }
        }

        void ConferenceBeforeSave(object sender, CancelEventArgs e)
        {
            Apply();
        }

		protected virtual void ConferenceExternalUpdate(object sender, EventArgs e)
		{
			ConferenceBindingSourceDataSourceChanged(sender, e);
		}

		public virtual void SelectedPageChanging(object sender, TabPageChangingEventArgs e)
		{
		}
    }
}