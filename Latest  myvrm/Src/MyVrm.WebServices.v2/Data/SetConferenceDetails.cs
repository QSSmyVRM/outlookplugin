﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
    internal class SetConferenceDetails:ServiceResponse
    {
        internal ConferenceId ConferenceId { get; private set; }
        public ParticipantCollection OutExternalParticipants { get; private set; }
        public ConfGuestRoomCollection OutConfRoomCollection { get; private set; }// ZD 101343 

        internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
        {
            reader.ReadStartElement(XmlNamespace.NotSpecified, "conferences");
            reader.ReadStartElement(XmlNamespace.NotSpecified, "conference");
            do
            {
                reader.Read();
                if (reader.IsStartElement(XmlNamespace.NotSpecified, "confID"))
                {
                    var conferenceId = new ConferenceId();
                    conferenceId.LoadFromXml(reader, "confID");
                    ConferenceId = conferenceId;
                }
                else
                {
                    //if (reader.IsStartElement(XmlNamespace.NotSpecified, "invited"))
                    //{
                    //    OutExternalParticipants = new ParticipantCollection();
                    //    while (!reader.IsEndElement(XmlNamespace.NotSpecified, "invited"))
                    //    {
                    //        reader.Read();
                    //        if (reader.IsStartElement(XmlNamespace.NotSpecified, "party"))
                    //        {
                    //            Participant user = new Participant();
                    //            user.LoadFromXml(reader, "party");
                    //            OutExternalParticipants.Add(user);
                    //        }
                    //    }
                    //}
                    // ZD 101343 starts

                    //if (reader.IsStartElement(XmlNamespace.NotSpecified, "GuestRooms"))
                    //{
                    //    OutConfRoomCollection = new ConfGuestRoomCollection();
                    //    while (!reader.IsEndElement(XmlNamespace.NotSpecified, "GuestRooms"))
                    //    {
                    //        reader.Read();
                    //        if (reader.IsStartElement(XmlNamespace.NotSpecified, "Endpoint"))
                    //        {
                    //            ConfGuestRoom guestRm = new ConfGuestRoom();
                    //            guestRm.LoadFromXml(reader, "Endpoint");
                    //            OutConfRoomCollection.Add(guestRm);
                    //        }
                    //    }
                    //}
                    //else
                    //{
                        reader.SkipCurrentElement();
                   // }

                    // ZD 101343 Ends

                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "conferences"));
            //reader.ReadEndElement(XmlNamespace.NotSpecified, "conferences");
        }
    }
    internal class SetConferenceDetailsRequest : ServiceRequestBase<SetConferenceDetails>
    {
        public SetConferenceDetailsRequest(MyVrmService service)
            : base(service)
        {
        }

        public Conference Conference { get; set; }

       internal override string GetXmlElementName()
        {
            return "conference";
        }

        internal override string GetCommandName()
        {
            return "SetConferenceDetails";
        }

        internal override string GetResponseXmlElementName()
        {
            return "setConferenceDetails";
        }
        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            UserId.WriteToXml(writer, "userID");
            Conference.WriteToXml(writer);
        }

    }
}
