﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;

namespace MyVrm.WebServices.Data
{
    internal static class TimeZoneConvertion
    {
        private static readonly Dictionary<int, string> MyVrmIdToTimeZoneId;

        static TimeZoneConvertion()
        {
            MyVrmIdToTimeZoneId = new Dictionary<int, string>
                                            {
                                                {1, "Afghanistan Standard Time"},
                                                {2, "Alaskan Standard Time"},
                                                {3, "Arab Standard Time"},
                                                {4, "Arabian Standard Time"},
                                                {5, "Arabic Standard Time"},
                                                // {, "Argentina Standard Time"},
                                                {6, "Atlantic Standard Time"},
                                                {7, "AUS Central Standard Time"},
                                                {8, "AUS Eastern Standard Time"},
                                                //{, "Azerbaijan Standard Time"},
                                                {9, "Azores Standard Time"},
                                                {10, "Canada Central Standard Time"},
                                                {11, "Cape Verde Standard Time"},
                                                {12, "Caucasus Standard Time"},
                                                {13, "Cen. Australia Standard Time"},
                                                {14, "Central America Standard Time"},
                                                {15, "Central Asia Standard Time"},
                                                //{, "Central Brazilian Standard Time"},
                                                {16, "Central Europe Standard Time"},
                                                {17, "Central European Standard Time"},
                                                {18, "Central Pacific Standard Time"},
                                                {19, "Central Standard Time"},
                                                //{, "Central Standard Time (Mexico)"},
                                                {20, "China Standard Time"},
                                                {21, "Dateline Standard Time"},
                                                {22, "E. Africa Standard Time"},
                                                {23, "E. Australia Standard Time"},
                                                {24, "E. Europe Standard Time"},
                                                {25, "E. South America Standard Time"},
                                                {26, "Eastern Standard Time"},
                                                {27, "Egypt Standard Time"},
                                                {28, "Ekaterinburg Standard Time"},
                                                {29, "Fiji Standard Time"},
                                                {30, "FLE Standard Time"},
                                                //{, "Georgian Standard Time"}
                                                {31, "GMT Standard Time"},
                                                {32, "Greenland Standard Time"},
                                                {33, "UTC"},
                                                {34, "GTB Standard Time"},
                                                {35, "Hawaiian Standard Time"},
                                                {36, "India Standard Time"},
                                                {37, "Iran Standard Time"},
                                                {38, "Israel Standard Time"},
                                                // {, "Jordan Standard Time"}
                                                {39, "Korea Standard Time"},
                                                // {, "Mauritius Standard Time"}
                                                {40, "Mexico Standard Time"}, // ???
                                                {41, "Mid-Atlantic Standard Time"},
                                                // {, "Middle East Standard Time"}
                                                // {, "Montevideo Standard Time"}
                                                // {, "Morocco Standard Time"}
                                                {42, "Mountain Standard Time"},
                                                // {, "Mountain Standard Time (Mexico)"}
                                                {43, "Myanmar Standard Time"},
                                                {44, "N. Central Asia Standard Time"},
                                                // {, "Namibia Standard Time"}
                                                {45, "Nepal Standard Time"},
                                                {46, "New Zealand Standard Time"},
                                                {47, "Newfoundland Standard Time"},
                                                {48, "North Asia East Standard Time"},
                                                {49, "North Asia Standard Time"},
                                                {50, "Pacific SA Standard Time"},
                                                {51, "Pacific Standard Time"},
                                                // {, "Pacific Standard Time (Mexico)"}
                                                // {, "Pakistan Standard Time"}
                                                {52, "Romance Standard Time"},
                                                {53, "Russian Standard Time"},
                                                {54, "SA Eastern Standard Time"},
                                                {55, "SA Pacific Standard Time"},
                                                {56, "SA Western Standard Time"},
                                                {57, "Samoa Standard Time"},
                                                {58, "SE Asia Standard Time"},
                                                {59, "Singapore Standard Time"},
                                                {60, "South Africa Standard Time"},
                                                {61, "Sri Lanka Standard Time"},
                                                {62, "Taipei Standard Time"},
                                                {63, "Tasmania Standard Time"},
                                                {64, "Tokyo Standard Time"},
                                                {65, "Tonga Standard Time"},
                                                {66, "US Eastern Standard Time"},
                                                {67, "US Mountain Standard Time"},
                                                // {, "Venezuela Standard Time"}
                                                {68, "Vladivostok Standard Time"},
                                                {69, "W. Australia Standard Time"},
                                                {70, "W. Central Africa Standard Time"},
                                                {71, "W. Europe Standard Time"},
                                                {72, "West Asia Standard Time"},
                                                {73, "West Pacific Standard Time"},
                                                {74, "Yakutsk Standard Time"},


                                                {99,"UTC-11"},
                                                {91,"Pacific Standard Time (Mexico)"},
                                                {89,"Mountain Standard Time (Mexico)"},
                                                {101,"Venezuela Standard Time"},
                                                {80,"Central Brazilian Standard Time"},
                                                {93,"Paraguay Standard Time"},
                                                {77,"Argentina Standard Time"},
                                                {88,"Montevideo Standard Time"},
                                                {100,"UTC-02"},
                                                {102,"Morocco Standard Time"},
                                                {90,"Namibia Standard Time"},
                                                {83,"Jordan Standard Time"},
                                                {87,"Middle East Standard Time"},
                                                {94,"Syria Standard Time"},
                                                {95,"Turkey Standard Time"},
                                                {78,"Azerbaijan Standard Time"},
                                                {82,"Georgian Standard Time"},
                                                {86,"Mauritius Standard Time"},
                                                {92,"Pakistan Standard Time"},
                                                {79,"Bangladesh Standard Time"},
                                                {96,"Ulaanbaatar Standard Time"},
                                                {85,"Magadan Standard Time"},
                                                {98,"UTC+12"},
                                                {81,"Central Standard Time (Mexico)"},
                                                {76,"Greenwich Standard Time"}
                                              //  {98,"Kamchatka Standard Time"}

                                            };
        }

        public static TimeZoneInfo ConvertToTimeZoneInfo(int myVrmTimeZoneId)
        {
            string timeZoneId;
            if (myVrmTimeZoneId == 81)
            {
                return GetMexicanStandardTimeZone();
            }
            else
            {
                if (MyVrmIdToTimeZoneId.TryGetValue(myVrmTimeZoneId, out timeZoneId))
                {
                    return TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
                }
            }
            return null;
        }

        private static TimeZoneInfo GetMexicanStandardTimeZone()
        {
            TimeZoneInfo tmInfo = null;
            try
            {
                tmInfo = TimeZoneInfo.FindSystemTimeZoneById("Mexico Standard Time");
            }
            catch
            {
            }
            if (tmInfo == null)
            {
                try
                {
                    tmInfo = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time (Mexico)");
                }
                catch
                {
                }
            }
            return tmInfo;

        }

        public static int ConvertToTimeZoneId(TimeZoneInfo timeZoneInfo)
        {
            foreach (var zoneIdsToStandardName in MyVrmIdToTimeZoneId)
            {
                if (zoneIdsToStandardName.Value.Equals(timeZoneInfo.Id, StringComparison.CurrentCultureIgnoreCase))
                {
                    return zoneIdsToStandardName.Key;                    
                }
            }
            throw new ArgumentOutOfRangeException("timeZoneInfo");
        }
    }
}
