﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace CallWebApi.Classes
{
    /// <summary>
    /// Class for sending authentication response with success
    /// </summary>
    public class AuthResponse : BaseResponse
    {
        /// <summary>
        /// Authentication ticket for storing on client and authentication on further requests
        /// </summary>
        public Guid AuthenticationTicket { set; get; }

        /// <summary>
        /// Type of user (1 - Power User, 0 - User)
        /// </summary>
        public int UserType { set; get; }
    }
}