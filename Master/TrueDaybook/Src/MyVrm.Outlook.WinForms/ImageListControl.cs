/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraLayout;
using DevExpress.XtraLayout.Utils;

namespace MyVrm.Outlook.WinForms
{
    public partial class ImageListControl : XtraUserControl
    {
        public ImageListControl()
        {
            InitializeComponent();
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
         Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        public WebServices.Data.Image[] Images
        {
            set
            {
                layoutControl.Root.Items.Clear();
                if (value == null)
                {
                    return;
                }
                layoutControl.BeginUpdate();
                try
                {
                    LayoutControlItem lastItem = null;
                    var rowWidth = 0;
                    foreach (var image in value)
                    {
                        if (image == null) continue;
                        var item = new LayoutControlItem();
                        if (lastItem == null || (layoutControl.Width - 100) < rowWidth)
                        {
                            item = layoutControl.Root.AddItem(item);
                        }
                        else
                        {
                            layoutControl.AddItem(item, lastItem, InsertType.Right);
                        }
                        item.TextVisible = false;
                        item.SizeConstraintsType = SizeConstraintsType.SupportHorzAlignment;
                        var pictureEdit = new PictureEdit
                                              {
                                                  Properties =
                                                      {
                                                          ShowMenu = false,
                                                          ReadOnly = true,
                                                          SizeMode = PictureSizeMode.Zoom
                                                      },
                                                  AutoSizeInLayoutControl = true,
                                                  Dock = DockStyle.Fill,
                                                  Image = image.IsValid ? Image.FromStream(image.AsStream()) : null
                                              };
                        var popupContainerControl = new PopupContainerControl();
                        popupContainerControl.Controls.Add(pictureEdit);
                        var popupContainerEdit = new PopupContainerEdit
                                                     {
                                                         Properties = {PopupControl = popupContainerControl},
                                                         Text = image.Name
                                                     };
                        item.Control = popupContainerEdit;
                        rowWidth += item.MinSize.Width;
                        lastItem = item;
                    }
                    layoutControl.BestFit();
                }
                finally
                {
                    layoutControl.EndUpdate();
                }
            }
        }
    }
}
