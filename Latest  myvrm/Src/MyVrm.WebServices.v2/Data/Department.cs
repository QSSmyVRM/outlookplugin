﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace MyVrm.WebServices.Data
{
    [Serializable]
    public class Department
    {
        public int Id { get; internal set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return Name ?? "";
        }
		public object Clone()
		{
			return new Department { Id = Id, Name = Name };
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(this, obj))
				return true;

			Department department = obj as Department;
			if (department == null)
				return false;
			if (department.Id !=Id)
				return false;
			if (department.Name != Name)
				return false;

			return true;
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
    }
}
