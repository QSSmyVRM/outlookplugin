﻿using System;
using System.Linq;
using Microsoft.Office.Interop.Excel.Extensions;
using MyVrm.DataImport.Excel.Extensions;
using MyVrm.WebServices.Data;
using ListObject=Microsoft.Office.Tools.Excel.ListObject;

namespace MyVrm.DataImport.Excel
{
    public class BridgesTable : ExcelTable
    {
        private readonly ExcelTableColumn _idColumn;
        private readonly ExcelTableColumn _nameColumn;
        private readonly ExcelTableColumn _loginColumn;
        private readonly ExcelTableColumn _passwordColumn;
        private readonly ExcelTableColumn _typeColumn;
        private readonly ExcelTableColumn _timeZoneColumn;
        private readonly ExcelTableColumn _controlPortIpAddressColumn;
        private readonly ExcelTableColumn _firmwareVersionColumn;

        public BridgesTable(ListObject listObject, MyVrmService service, ValidationListManager validationListManager) : base(listObject, service, validationListManager)
        {
            _idColumn = new ExcelTableColumn(ListObject.ListColumns[1], typeof (string)) {IsReadOnly = true};
            _nameColumn = new ExcelTableColumn(ListObject.ListColumns[2], typeof (string)) {IsRequired = true};
            _loginColumn = new ExcelTableColumn(ListObject.ListColumns[3], typeof(string)) { IsRequired = true };
            _passwordColumn = new ExcelTableColumn(ListObject.ListColumns[4], typeof(string)) { IsRequired = true };
            _typeColumn = new ExcelTableColumn(ListObject.ListColumns[5], typeof (string))
                                {
                                    IsRequired = true,
                                    ValuesRangeName = ValidationListManager.GetBridgeTypesRange(),
                                    HasFixedSetOfValues = true
                                };
            _timeZoneColumn = new ExcelTableColumn(ListObject.ListColumns[6], typeof(string))
                                    {
                                        IsRequired = true,
                                        ValuesRangeName = ValidationListManager.GetTimezoneRange(),
                                        HasFixedSetOfValues = true
                                    };
            _controlPortIpAddressColumn = new ExcelTableColumn(ListObject.ListColumns[7], typeof(string)) { IsRequired = true };
            _firmwareVersionColumn = new ExcelTableColumn(ListObject.ListColumns[8], typeof(string)) { IsRequired = true };

            TableColumns.AddRange(new[]
                                  {
                                      _idColumn, _nameColumn, _loginColumn, _passwordColumn, _typeColumn, _timeZoneColumn,
                                      _controlPortIpAddressColumn, _firmwareVersionColumn
                                  });
            Initialize();   
        }

        #region Overrides of ExcelTable

        protected override void Save(SaveErrorCollection errors)
        {
            var bridgeTypes = Service.GetBridgeTypes();
            foreach (var listRow in ListObject.ListRows.Items())
            {
                var bridge = new Bridge(Service)
                {
                    IsVirtual = false,
                    AdministratorId = UserId.DefaultUserId,
                    MaxAudioCalls = 1,
                    MaxVideoCalls = 1,
                    Status = BridgeStatus.Active,
                    IsdnThresholdAlert = false,
                    MalfunctionAlert = false,
                    PercentOfReservedPorts = 20
                };
                try
                {
                    var range = listRow.Range;
                    bridge.Name = range.Item(1, _nameColumn.Index).ValueAsString();
                    bridge.Login = range.Item(1, _loginColumn.Index).ValueAsString();
                    bridge.Password = range.Item(1, _passwordColumn.Index).ValueAsString();
                    var vendorName = range.Item(1, _typeColumn.Index).ValueAsString();
                    var bridgeType = bridgeTypes.FirstOrDefault(match => match.Name == vendorName);
                    bridge.Vendor = bridgeType.Id;
                    bridge.TimeZone = range.Item(1, _timeZoneColumn.Index).ValueAsTimeZoneInfo();
                    bridge.Details.ControlPortIpAddress = range.Item(1, _controlPortIpAddressColumn.Index).ValueAsString();
                    bridge.FirmwareVersion = range.Item(1, _firmwareVersionColumn.Index).ValueAsString();
                    bridge.Save();
                }
                catch (Exception e)
                {
                    errors.Add(new SaveError(bridge.Name, e));
                }
            } 
        }

        #endregion
    }
}
