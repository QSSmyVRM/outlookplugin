﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.WebServices.Data
{
    public enum EmailClientType
    {
        None = -1,
        MSOutlook = 1
    }

    public class UserSchema : ServiceObjectSchema
    {
        public static readonly PropertyDefinition UserId;
        public static readonly PropertyDefinition UserName;
        public static readonly PropertyDefinition Login;
        public static readonly PropertyDefinition Password;
        public static readonly PropertyDefinition Email;
        public static readonly PropertyDefinition AlternativeEmail;
        public static readonly PropertyDefinition SendBothEmail;
        public static readonly PropertyDefinition EmailClient;
        public static readonly PropertyDefinition SavedSearchesId;
        public static readonly PropertyDefinition UserInterfaceId;
        public static readonly PropertyDefinition TimeZone;
        public static readonly PropertyDefinition PreferredRoomId;
        public static readonly PropertyDefinition LineRate;
        public static readonly PropertyDefinition VideoProtocol;
        public static readonly PropertyDefinition IpOrISDNAddress;
        public static readonly PropertyDefinition ConnectionType;
        public static readonly PropertyDefinition VideoEquipmentId;
        public static readonly PropertyDefinition IsOutside;
        public static readonly PropertyDefinition AddressType;
        public static readonly PropertyDefinition GroupId;
        public static readonly PropertyDefinition CcGroupId;
        public static readonly PropertyDefinition RoleId;
        public static readonly PropertyDefinition InitialTime;
        public static readonly PropertyDefinition ExpiryDate;
        public static readonly PropertyDefinition BridgeId;
        public static readonly PropertyDefinition Departments;
        public static readonly PropertyDefinition Status;
        public static readonly PropertyDefinition PreferredLocations;
        public static readonly PropertyDefinition AudioAddon;
        public static readonly PropertyDefinition ConferenceCode;
        public static readonly PropertyDefinition LeaderPin;
        public static readonly PropertyDefinition ApiPortNumber;

        internal static readonly UserSchema Instance;

        static UserSchema()
        {
            UserId = new ComplexPropertyDefinition<UserId>("userID", () => new UserId());
            UserName = new ComplexPropertyDefinition<UserName>("userName",
                                                               PropertyDefinitionFlags.AutoInstantiateOnRead |
                                                               PropertyDefinitionFlags.CanSet, () => new UserName());
            Login = new StringPropertyDefinition("login", PropertyDefinitionFlags.CanSet);
            Password = new StringPropertyDefinition("password", PropertyDefinitionFlags.CanSet);
            Email = new StringPropertyDefinition("userEmail", PropertyDefinitionFlags.CanSet);
            AlternativeEmail = new StringPropertyDefinition("alternativeEmail", PropertyDefinitionFlags.CanSet);
            SendBothEmail = new BoolPropertyDefinition("sendBoth", PropertyDefinitionFlags.CanSet);
            EmailClient = new GenericPropertyDefinition<EmailClientType>("emailClient", PropertyDefinitionFlags.CanSet);
            SavedSearchesId = new IntPropertyDefinition("SavedSearch", PropertyDefinitionFlags.CanSet);
            UserInterfaceId = new IntPropertyDefinition("userInterface", PropertyDefinitionFlags.CanSet);
            TimeZone = new TimeZonePropertyDefinition("timeZone", PropertyDefinitionFlags.CanSet);
            PreferredRoomId = new IntPropertyDefinition("location", PropertyDefinitionFlags.CanSet);
            LineRate = new GenericPropertyDefinition<LineRateEnum>("lineRateID", PropertyDefinitionFlags.CanSet);
            VideoProtocol = new IntPropertyDefinition("videoProtocol", PropertyDefinitionFlags.CanSet);
            IpOrISDNAddress = new StringPropertyDefinition("IPISDNAddress", PropertyDefinitionFlags.CanSet);
            ConnectionType = new IntPropertyDefinition("connectionType", PropertyDefinitionFlags.CanSet);
            VideoEquipmentId = new IntPropertyDefinition("videoEquipmentID", PropertyDefinitionFlags.CanSet);
            IsOutside = new BoolPropertyDefinition("isOutside", PropertyDefinitionFlags.CanSet);
            AddressType = new IntPropertyDefinition("addressTypeID", PropertyDefinitionFlags.CanSet);
            GroupId = new IntPropertyDefinition("group", PropertyDefinitionFlags.CanSet);
            CcGroupId = new IntPropertyDefinition("ccGroup", PropertyDefinitionFlags.CanSet);
            RoleId = new ComplexPropertyDefinition<UserRoleId>("roleID", PropertyDefinitionFlags.CanSet, () => new UserRoleId());
            InitialTime = new TimeSpanPropertyDefinition("initialTime", PropertyDefinitionFlags.CanSet);
            ExpiryDate = new DatePropertyDefinition("expiryDate", PropertyDefinitionFlags.CanSet);
            BridgeId = new ComplexPropertyDefinition<BridgeId>("bridgeID", PropertyDefinitionFlags.CanSet, () => new BridgeId());
            Status = new ComplexPropertyDefinition<UserStatus>("status",
                                                               PropertyDefinitionFlags.AutoInstantiateOnRead |
                                                               PropertyDefinitionFlags.CanSet,
                                                               () => new UserStatus());
            PreferredLocations = new ComplexPropertyDefinition<FavoriteRoomCollection>("locationList",
                                                                             PropertyDefinitionFlags.
                                                                                 AutoInstantiateOnRead,
                                                                             () => new FavoriteRoomCollection());
            AudioAddon = new BoolPropertyDefinition("Audioaddon", PropertyDefinitionFlags.CanSet);
            ConferenceCode = new StringPropertyDefinition("conferenceCode", PropertyDefinitionFlags.CanSet);
            LeaderPin = new StringPropertyDefinition("leaderPin", PropertyDefinitionFlags.CanSet);
            IpOrISDNAddress = new StringPropertyDefinition("IPISDNAddress", PropertyDefinitionFlags.CanSet);
            ApiPortNumber = new IntPropertyDefinition("APIPortNo", PropertyDefinitionFlags.CanSet);

            Instance = new UserSchema();
        }

        internal override void RegisterProperties()
        {
            base.RegisterProperties();
            RegisterProperty(UserId);
            RegisterProperty(UserName);
            RegisterProperty(Login);
            RegisterProperty(Password);
            RegisterProperty(Email);
            RegisterProperty(AlternativeEmail);
            RegisterProperty(SendBothEmail);
            RegisterProperty(EmailClient);
            RegisterProperty(SavedSearchesId);
            RegisterProperty(UserInterfaceId);
            RegisterProperty(TimeZone);
            RegisterProperty(PreferredRoomId);
            RegisterProperty(LineRate);
            RegisterProperty(VideoProtocol);
            RegisterProperty(IpOrISDNAddress);
            RegisterProperty(ConnectionType);
            RegisterProperty(VideoEquipmentId);
            RegisterProperty(IsOutside);
            RegisterProperty(AddressType);
            RegisterProperty(GroupId);
            RegisterProperty(CcGroupId);
            RegisterProperty(RoleId);
            RegisterProperty(InitialTime);
            RegisterProperty(ExpiryDate);
            RegisterProperty(BridgeId);
            RegisterProperty(Status);
            RegisterProperty(PreferredLocations);
            RegisterProperty(AudioAddon);
            RegisterProperty(ConferenceCode);
            RegisterProperty(LeaderPin);
            RegisterProperty(ApiPortNumber);
        }
    }
}
