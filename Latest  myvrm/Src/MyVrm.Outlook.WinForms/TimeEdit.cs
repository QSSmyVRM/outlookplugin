﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace MyVrm.Outlook.WinForms
{
    public class TimeEdit : DevExpress.XtraEditors.TimeEdit
    {
        protected override void OnSpin(DevExpress.XtraEditors.Controls.SpinEventArgs e)
        {
            TimeSpan time = Time.TimeOfDay;
            DateTime date = Time.Date;
            if (SelectionStart == 0)
            {
                if ((e.IsSpinUp && time.Hours == 23))
                {
                    time = TimeSpan.FromHours(0);
                }
                else if (!e.IsSpinUp && time.Hours == 0)
                {
                    time = TimeSpan.FromHours(23);
                }
                else
                {
                    time += TimeSpan.FromHours(e.IsSpinUp ? 1 : -1);
                }
                Time = date + time;
                e.Handled = true;
            }
            else if (((time.Hours == 10 || time.Hours == 11 || time.Hours == 12 || time.Hours == 22 || time.Hours == 23 || time.Hours == 24) &&  SelectionStart == 3) || SelectionStart == 2)
            {
                int selStart = SelectionStart;
                if ((e.IsSpinUp && time.Minutes == 59))
                {
                    time += TimeSpan.FromMinutes(1);
                }
                else if (!e.IsSpinUp && time.Minutes == 0)
                {
                    time += TimeSpan.FromMinutes(-1);
                }
                else
                {
                    time += TimeSpan.FromMinutes(e.IsSpinUp ? 1 : -1);
                }
                Time = date + time;
                SelectionStart = selStart;
                SelectionLength = 2;
                e.Handled = true;
            }
            else
            {
                base.OnSpin(e);    
            }
        }
    }
}
