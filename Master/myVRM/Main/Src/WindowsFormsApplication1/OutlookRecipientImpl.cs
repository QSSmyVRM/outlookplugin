﻿using System;
using System.Collections.Generic;
using MyVrm.Outlook;

namespace WindowsFormsApplication1
{
    internal class OutlookRecipientImpl : OutlookRecipient
    {
        private string _name;
        private string _smtpAddress;

        internal OutlookRecipientImpl(string name, string smtpAddress)
        {
            _name = name;
            _smtpAddress = smtpAddress;
        }

        public override string Name
        {
            get { return _name; }
        }

        public override string SmtpAddress
        {
            get
            {
                return _smtpAddress;
            }
        }

        protected override List<FreeBusyInfo> GetFreeBusyFromDefaultCalendarFolder(DateTime start, DateTime end)
        {
            return new List<FreeBusyInfo>();
        }

        protected override string GetFreeBusyInternal(DateTime start, int minutesPerChar)
        {
            return "";
        }

        public override void Dispose()
        {
        }
    }
}
