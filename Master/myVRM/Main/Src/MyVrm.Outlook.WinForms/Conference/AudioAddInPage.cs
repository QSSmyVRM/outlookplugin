﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook.WinForms.Conference
{
	public partial class AudioAddInPage : ConferencePage
	{
		private ReadOnlyCollection<AudioUser> _audioUsers;
		//private Collection<AudioConferenceBridge> _AudioConferenceBridges = new Collection<AudioConferenceBridge>();
		private int _MaxEntryCount;
		private int MaxEntryCount
		{
			get
			{
				_MaxEntryCount = Math.Min(_audioUsers.Count, Properties.Settings.Default.MaxAudioAddOnsNumber);
				return _MaxEntryCount;
			}
		}

		/// <summary>
		/// Ctor
		/// </summary>
		public AudioAddInPage()
		{
			InitializeComponent();
			Text = Strings.AudioAddonPageText;
			ConferenceBindingSource.DataSourceChanged += ConferenceBindingSource_DataSourceChanged;
			bar1.Text = Strings.ToolsBarText;
			treeListAudioConferenceBridgeColumn.Caption = Strings.AudioConferenceBridgeColumnText;
			treeListLeaderPinColumn.Caption = Strings.LeaderPinColumnText;
			treeListParticipantCodeColumn.Caption = Strings.ParticipantCodeColumnText;
			repositoryItemEnumComboBox1.NullText = "<" + Strings.SelectBridgeText +">";
			barAddEntry.Caption = Strings.AddEntryMenuItemText;
			barDelEntry.Caption = Strings.DeleteEntryMenuItemText;
		}

        protected override void OnReadOnlyChanged(ReadOnlyChangedEventArgs e)
        {
            base.OnReadOnlyChanged(e);
            bar1.Visible = !e.ReadOnly;
            treeList.Enabled = !e.ReadOnly;
        }

		/// <summary>
		/// Event hanler, called on page load
		/// </summary>
		void ConferenceBindingSource_DataSourceChanged(object sender, EventArgs e)
		{
			if (Conference != null)
			{
				Cursor cursor = Cursor.Current;
				Cursor.Current = Cursors.WaitCursor;

				try
				{
					if (_audioUsers == null)
						_audioUsers = MyVrmService.Service.GetAudioUsers();
					Fill();
					DisEnable_barMenuItems();
				}
				finally
				{
					Cursor.Current = cursor;
				}
			}
		}

		/// <summary>
		/// Page controls' init
		/// </summary>
		protected void Fill()
		{
			AudioUser selUser = null;
			
			if (Conference != null)
			{
				foreach (var endPoint in Conference.AudioConferenceBridges)
				{
					if (_audioUsers != null)
					{
						foreach (AudioUser audioUser in _audioUsers)
						{
							if (endPoint.Endpoint.Id == audioUser.Id)
							{
								selUser = audioUser;
								break;
							}
						}
						treeList.BeginUnboundLoad();

						TreeListNode addedNode = treeList.AppendNode(null, null);
						repositoryItemEnumComboBox1.Items.Clear();
						foreach (var User in _audioUsers)
						{
							repositoryItemEnumComboBox1.Items.Add(User);
						}
						addedNode.SetValue(treeListAudioConferenceBridgeColumn, selUser);
						addedNode.SetValue(treeListLeaderPinColumn, endPoint.LeaderPin);
						addedNode.SetValue(treeListParticipantCodeColumn, endPoint.ConferenceCode);
						treeList.EndUnboundLoad();
					}
				}
			}
		}

		/// <summary>
		/// Enable or disable "Delete Entry" menu bar item
		/// </summary>
		protected void DisEnable_barMenuItems()
		{
			barDelEntry.Enabled = treeList.Nodes.Count > 0 ;
			barAddEntry.Enabled = treeList.Nodes.Count < MaxEntryCount; 
		}

		/// <summary>
		/// Delete an entry 
		/// </summary>
		private void barDelEntry_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			MyVrmAddin.TraceSource.TraceInformation("barDelEntry_ItemClick");

			if (treeList.Nodes.Count > 0)
				treeList.DeleteNode(treeList.Nodes.TreeList.FocusedNode);
			DisEnable_barMenuItems();
			if (Conference != null)
			{
				Conference.Appointment.SetNonOutlookProperty(true);
			}
		}

		/// <summary>
		/// Add an entry 
		/// </summary>
		private void barAddEntry_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			MyVrmAddin.TraceSource.TraceInformation("barAddEntry_ItemClick");

			treeList.PostEditor();

			if (treeList.Nodes.Count > 0 && treeList.FocusedNode != null 
				&& treeList.FocusedNode.GetValue(treeListAudioConferenceBridgeColumn) == null)
			{
				UIHelper.ShowError(Strings.DefineAudioConferenceBridgeValue);
				SetFocusOnAudioConferenceBridgeColumn(treeList.FocusedNode);
				return;
			}

			if (treeList.Nodes.Count < MaxEntryCount ) 
			{
				treeList.BeginUnboundLoad();

				TreeListNode addedNode = treeList.AppendNode(null, null);
				repositoryItemEnumComboBox1.Items.Clear();
				if (_audioUsers != null)
				{
					foreach (var User in _audioUsers)
					{
						repositoryItemEnumComboBox1.Items.Add(User);
					}
				}
				SetFocusOnAudioConferenceBridgeColumn(addedNode);

				treeList.EndUnboundLoad();
			}
			DisEnable_barMenuItems();
			if (Conference != null )
				Conference.Appointment.SetNonOutlookProperty(true);
		}

		/// <summary>
		/// Select, focus and expand combo in Audio Conference Bridge Column
		/// </summary>
		private void SetFocusOnAudioConferenceBridgeColumn(TreeListNode aNode)
		{
			treeList.FocusedNode = aNode;
			treeList.Selection.Set(aNode);
			aNode.Selected = true;
			treeList.FocusedColumn = treeListAudioConferenceBridgeColumn;

			treeList.Focus();
			treeList.ShowEditor();
			if (treeList.ActiveEditor != null)
			{
				treeList.ActiveEditor.CausesValidation = true;
				treeList.ActiveEditor.SendKey(new KeyEventArgs(Keys.Space));
				treeList.ActiveEditor.SendKeyUp(new KeyEventArgs(Keys.Space));
				if (treeList.ActiveEditor is PopupBaseEdit)
					(treeList.ActiveEditor as PopupBaseEdit).ShowPopup();
			}
		}

		/// <summary>
		/// Check duplicate values of Audio Conference Bridge
		/// </summary>
		public void repositoryItemEnumComboBox1_EditValueChanging(object sender, ChangingEventArgs e)
		{
			//Check already selected
			int nodeCount = treeList.Nodes.Count;

			var audioUser = e != null && e.NewValue!= null ? (AudioUser)(e.NewValue): null;
			var user = audioUser != null ? MyVrmAddin.Instance.GetUserFromCache(audioUser.Id) : null;
			if (user != null)
			{
				treeList.FocusedNode.SetValue(treeListLeaderPinColumn, user.LeaderPin );
				treeList.FocusedNode.SetValue(treeListParticipantCodeColumn, user.ConferenceCode);
			}

			for (int i = 0; i < nodeCount && nodeCount > 1; i++)
			{
				if ((treeList.Nodes[i])[treeListAudioConferenceBridgeColumn] != null &&
					(treeList.Nodes[i])[treeListAudioConferenceBridgeColumn].ToString().CompareTo(e.NewValue.ToString()) == 0)
				{
					if(e != null)
						e.Cancel = true;

					UIHelper.ShowError(e.NewValue + " " + Strings.BridgeValueIsAlreadySelected);
					SetFocusOnAudioConferenceBridgeColumn(treeList.FocusedNode);
				}
			}
			if (Conference != null)
				Conference.Appointment.SetNonOutlookProperty(true);
		}

		/// <summary>
		/// Check if Audio Conference Bridge value is not defined when user tries to edit another column
		/// </summary>
		private void treeList_FocusedColumnChanged(object sender, FocusedColumnChangedEventArgs e)
		{
			if (e != null && e.OldColumn != null)
			{
				if( e.OldColumn == treeListAudioConferenceBridgeColumn
					&& treeList.FocusedNode[treeListAudioConferenceBridgeColumn] == null)
				{
					UIHelper.ShowError(Strings.DefineAudioConferenceBridgeValue);
					SetFocusOnAudioConferenceBridgeColumn(treeList.FocusedNode);
				}
			}
		}

		/// <summary>
		/// Check if Audio Conference Bridge value is not defined when user tries to edit another "row" (node)
		/// </summary>
		private void treeList_FocusedNodeChanged(object sender, FocusedNodeChangedEventArgs e)
		{
			if (treeList.Nodes.Count > 1 && e != null)
			{
				if( e.OldNode[treeListAudioConferenceBridgeColumn]== null)
				{
					UIHelper.ShowError(Strings.DefineAudioConferenceBridgeValue);
					SetFocusOnAudioConferenceBridgeColumn(e.OldNode);
				}
			}
		}

		/// <summary>
		/// Check page state
		/// </summary>
		/// <returns>
		/// Return true if Audio Conference Bridge value is not defined
		/// </returns>
		private bool IsPageInvalid()
		{
			int nodeCount = treeList.Nodes.Count;
			for (int i = 0; i < nodeCount; i++)
			{
				if ((treeList.Nodes[i])[treeListAudioConferenceBridgeColumn] == null)
				{
					UIHelper.ShowError(Strings.DefineAudioConferenceBridgeValue);
					treeList.FocusedColumn = treeListAudioConferenceBridgeColumn;
					return true;
				}
			}
			return false;
		}

		/// <summary>
		/// On tab switching
		/// </summary>
		private void AudioAddInPage_Validating(object sender, CancelEventArgs e)
		{
			e.Cancel = IsPageInvalid();
			if (e.Cancel)
			{
//				treeList.FocusedColumn = treeListAudioConferenceBridgeColumn;
				/* !
				 * The call of SetFocusOnAudioConferenceBridgeColumn() provokes call of AudioAddInPage_Validating() twice -
				 * so, just set a focused column as AudioConferenceBridgeColumn
				 * ! 
				 */
			}
		}

		/// <summary>
		/// On conference save
		/// </summary>
		protected override void OnApplying(CancelEventArgs e)
		{
			treeList.PostEditor();
			base.OnApplying(e);
			if (e.Cancel)
			{
				return;
			}
			e.Cancel = IsPageInvalid();

			if (e.Cancel == false)
				Save();
		}

		/// <summary>
		/// Save page data
		/// </summary>
		private void Save()
		{
			if (Conference != null)
			{
				Conference.AudioConferenceBridges.Clear();

				foreach (TreeListNode aNode in treeList.Nodes)
				{
					var audioUser = (AudioUser) aNode[0];
					var user = MyVrmAddin.Instance.GetUserFromCache(audioUser.Id);
					AudioConferenceBridge aAudioConferenceBridge = new AudioConferenceBridge(new ConferenceEndpoint
					                                                                         	{
					                                                                         		Type = ConferenceEndpointType.User,
					                                                                         		Id = user.Id,
					                                                                         		UseDefault = false,
					                                                                         		BridgeId = user.BridgeId,
					                                                                         		Address = user.IpOrISDNAddress,
					                                                                         		AddressType = user.AddressType,
					                                                                         		VideoEquipment = -1,
					                                                                         		Bandwidth = user.LineRate,
					                                                                         		// audio
					                                                                         		ConnectionType = 1,
					                                                                         		IsOutside = user.IsOutside,
					                                                                         		// IP
					                                                                         		DefaultProtocol = 1,
					                                                                         		Connection = MediaType.AudioOnly,
					                                                                         		Caller =
					                                                                         			ConferenceEndpointCallMode.None,
					                                                                         		ApiPortNumber = user.ApiPortNumber
					                                                                         	})
					                                               	{
					                                               		Name = user.UserName.FirstName,
					                                               		ConferenceCode = aNode[2] != null ? aNode[2].ToString() : "",
					                                               		LeaderPin = aNode[1] != null ? aNode[1].ToString() : ""
					                                               	};
					Conference.AudioConferenceBridges.Add(aAudioConferenceBridge);
				}
			}
		}

		private void LeaderPinAndParticipantCode_EditValueChanging(object sender, ChangingEventArgs e)
		{
			if (Conference != null)
				Conference.Appointment.SetNonOutlookProperty(true);
		}
	}
}
