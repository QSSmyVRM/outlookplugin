﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;

namespace MyVrm.WebServices.Data
{
	[Serializable]
	public class ResponsiblePerson : ComplexProperty
	{
		public string TagName;
		public string Name;
		public string Email;
		public string Phone;

		public ResponsiblePerson(string tag)
		{
			TagName = tag;
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			writer.WriteStartElement(Namespace, TagName);
			writer.WriteElementValue(Namespace, "Name", Name);
			writer.WriteElementValue(Namespace, "Email", Email);
			writer.WriteElementValue(Namespace, "Phone", Phone);
			writer.WriteEndElement();
			//base.WriteElementsToXml(writer);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(this, obj))
				return true;

			ResponsiblePerson person = obj as ResponsiblePerson;
			if (person == null)
				return false;
			if (person.TagName != TagName )
				return false;
			if (person.Name != Name)
				return false;
			if (person.Phone != Phone)
				return false;
			if (person.Email != Email)
				return false;
			return true;
		}

		public override object Clone()
		{
			ResponsiblePerson dest = new ResponsiblePerson(TagName);
			//dest.TagName = TagName;
			dest.Name = Name;
			dest.Phone = Phone;
			dest.Email = Email;
			return dest;
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		//#region Implementation of ISerializable
		//[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
		//public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		//{
		//    if (info == null)
		//        throw new ArgumentNullException("info");

		//    info.AddValue("Name", Name, typeof(string));
		//    info.AddValue("Email", Email, typeof(string));
		//    info.AddValue("Phone", Phone, typeof(string));
		//}

		//#endregion
	}
}
