﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	public class TimeSlot
	{
		public DateTime Start { set; get; }
		public DateTime End { set; get; }
		public bool IsFree { set; get; }
	}
	public class Avail
	{
		public DateTime startDateTime { set; get; }
		public DateTime endDateTime{ set; get; }
		public int locationID { set; get; }
		public bool isFree{ set; get; }
		public List<TimeSlot> timeSlots = new List<TimeSlot>();
	}

	//Request
	class GetAvailabilityRequest : ServiceRequestBase<GetAvailabilityResponse>
	{
		public RoomId RoomId;

		internal GetAvailabilityRequest(MyVrmService service)
			: base(service)
		{
		}

		#region Overrides of ServiceRequestBase<GetTemplateResponse>

		internal override string GetXmlElementName()
		{
			return "login";
		}

		internal override string GetCommandName()
		{
			return Constants.GetAvailabilityCommandName;
		}

		internal override string GetResponseXmlElementName()
		{
			return "GetAvailability";
		}

		internal override GetAvailabilityResponse ParseResponse(MyVrmServiceXmlReader reader)
		{
			var response = new GetAvailabilityResponse();
			response.ReadElementsFromXml(reader);
			return response;
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			if (UserId != null)
				UserId.WriteToXml(writer, "userID");
		}

		#endregion
	}

	//Response
	public class GetAvailabilityResponse : ServiceResponse
	{
		//Result 
		public Avail Availability = new Avail();

		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{

		}
	}
}
