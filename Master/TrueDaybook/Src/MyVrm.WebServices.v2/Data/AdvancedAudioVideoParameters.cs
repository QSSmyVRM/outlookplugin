﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using MyVrm.Common;

namespace MyVrm.WebServices.Data
{
    public enum AudioVideoProtocol
    {
        [LocalizedDescription("AudioVideoProtocolIp", typeof(Strings))]
        Ip = 1,
        [LocalizedDescription("AudioVideoProtocolIsdn", typeof(Strings))]
        Isdn,
        [LocalizedDescription("AudioVideoProtocolIpIsdnSip", typeof(Strings))]
        IpIsdnSip
    }

    public enum AudioVideoType
    {
        [LocalizedDescription("AudioVideoTypeNone", typeof(Strings))]
        None = 1,
        [LocalizedDescription("AudioVideoTypeAudioOnly", typeof(Strings))]
        AudioOnly,
        [LocalizedDescription("AudioVideoTypeAudioVideo", typeof(Strings))]
        AudioVideo
    }

    public enum MediaType
    {
        [LocalizedDescription("MediaTypeNoAudioVideo", typeof(Strings))]
        NoAudioVideo = 0,
        [LocalizedDescription("MediaTypeAudioOnly", typeof(Strings))]
        AudioOnly = 1,
        [LocalizedDescription("MediaTypeAudioVideo", typeof(Strings))]
        AudioVideo = 2
    }

    public enum AudioCodecEnum
    {
        [LocalizedDescription("AudioCodecAuto", typeof(Strings))]
        Auto = 0,
        [LocalizedDescription("AudioCodecG728Bitrate16KbitsPerSec", typeof(Strings))]
        G728Bitrate16KbitsPerSec,
        [LocalizedDescription("AudioCodecG722Bitrate24KbitsPerSec", typeof(Strings))]
        G722Bitrate24KbitsPerSec,
        [LocalizedDescription("AudioCodecG722Bitrate32KbitsPerSec", typeof(Strings))]
        G722Bitrate32KbitsPerSec,
        [LocalizedDescription("AudioCodecG722G711Bitrate56KbitsPerSec", typeof(Strings))]
        G722G711Bitrate56KbitsPerSec,
        [LocalizedDescription("AudioCodecG711Bitrate56KbitsPerSec", typeof(Strings))]
        G711Bitrate56KbitsPerSec
    }

    public enum VideoCodecEnum
    {
        [LocalizedDescription("VideoCodecAuto", typeof(Strings))]
        Auto = 0,
        [LocalizedDescription("VideoCodecH261", typeof(Strings))]
        H261,
        [LocalizedDescription("VideoCodecH263", typeof(Strings))]
        H263,
        [LocalizedDescription("VideoCodecH264", typeof(Strings))]
        H264
    }

    public enum VideoModeType
    {
		[LocalizedDescription("VideoModeUnassigned", typeof(Strings))]
		Unassigned = 0,
        [LocalizedDescription("VideoModeSwitchedVideo", typeof(Strings))]
        SwitchedVideo,
        [LocalizedDescription("VideoModeTranscoding", typeof(Strings))]
        Transcoding,
        [LocalizedDescription("VideoModeContinuousPresence", typeof(Strings))]
        ContinuousPresence
    }

    public enum LineRateEnum
    {
        [LocalizedDescription("LineRate56Kbps1Channel", typeof(Strings))]
        Rate56Kbps1Channel = 56,
        [LocalizedDescription("LineRate64Kbps1Channel", typeof(Strings))]
        Rate64Kbps1Channel = 64,
        [LocalizedDescription("LineRate128Kbps2Channels", typeof(Strings))]
        Rate128Kbps2Channels = 128,
        [LocalizedDescription("LineRate256Kbps4Channels", typeof(Strings))]
        Rate256Kbps4Channels = 256,
        [LocalizedDescription("LineRate384Kbps6Channels", typeof(Strings))]
        Rate384Kbps6Channels = 384,
        [LocalizedDescription("LineRate512Kbps8Channels", typeof(Strings))]
        Rate512Kbps8Channels = 512,
        [LocalizedDescription("LineRate768Kbps12Channels", typeof(Strings))]
        Rate768Kbps12Channels = 768,
        [LocalizedDescription("LineRate1Mbps18Channels", typeof(Strings))]
        Rate1Mbps18Channels = 1024,
        [LocalizedDescription("LineRate1472Kbps23Channelsl", typeof(Strings))]
        Rate1472Kbps23Channels = 1472,
        [LocalizedDescription("LineRate1500Mbps24Channels", typeof(Strings))]
        Rate1500Mbps24Channels = 1500,
        [LocalizedDescription("LineRate2Mbps30Channels", typeof(Strings))]
        Rate2Mbps30Channels = 2048,
    }
    /// <summary>
    /// Represents advanced audio/video parameters for a conference
    /// </summary>
    public class AdvancedAudioVideoParameters : ComplexProperty
    {
        private const string MaxAudioPartTagName = "maxAudioPart";
        private const string MaxVideoPartTagName = "maxVideoPart";

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(this, obj))
				return true;

			AdvancedAudioVideoParameters parameters = obj as AdvancedAudioVideoParameters;
			if (parameters == null)
				return false;
			if( parameters.AudioCodec != AudioCodec )
				return false;
			if( parameters.ConferenceOnPort != ConferenceOnPort )
				return false;
			if( parameters.IsDualStreamed != IsDualStreamed )
				return false;
			if( parameters.IsEncrypted != IsEncrypted )
				return false;
			if( parameters.IsLecture != IsLecture )
				return false;
			if( parameters.MaximumAudioPorts != MaximumAudioPorts )
				return false;
			if( parameters.MaximumLineRateId != MaximumLineRateId )
				return false;
			if( parameters.MaximumVideoPorts != MaximumVideoPorts )
				return false;
			if( parameters.RestrictAudioVideo != RestrictAudioVideo )
				return false;
			if( parameters.RestrictProtocol != RestrictProtocol )
				return false;
			if( parameters.SingleDialin != SingleDialin )
				return false;
			if( parameters.VideoCodec != VideoCodec )
				return false;
			if( parameters.VideoLayout != VideoLayout )
				return false;
			if( parameters.VideoMode != VideoMode )
				return false;
			return true;
		}

		public override object Clone()//CopyTo(AdvancedAudioVideoParameters dest)
		{
			AdvancedAudioVideoParameters dest = new AdvancedAudioVideoParameters();
			dest.AudioCodec = AudioCodec;
			dest.ConferenceOnPort = ConferenceOnPort;
			dest.IsDualStreamed = IsDualStreamed;
			dest.IsEncrypted = IsEncrypted;
			dest.IsLecture = IsLecture;
			dest.MaximumAudioPorts = MaximumAudioPorts;
			dest.MaximumLineRateId = MaximumLineRateId;
			dest.MaximumVideoPorts = MaximumVideoPorts;
			dest.RestrictAudioVideo = RestrictAudioVideo;
			dest.RestrictProtocol = RestrictProtocol;
			dest.SingleDialin = SingleDialin;
			dest.VideoCodec = VideoCodec;
			dest.VideoLayout = VideoLayout;
			dest.VideoMode = VideoMode;
			return dest;
		}

    	public AdvancedAudioVideoParameters()
        {
            MaximumAudioPorts = 0;
            MaximumVideoPorts = 0;
            RestrictProtocol = AudioVideoProtocol.Ip;
            RestrictAudioVideo = AudioVideoType.AudioVideo;
            VideoLayout = 1;
            MaximumLineRateId = LineRateEnum.Rate384Kbps6Channels;
            AudioCodec = AudioCodecEnum.Auto;
            VideoCodec = VideoCodecEnum.Auto;
            IsDualStreamed = false;
            ConferenceOnPort = false;
            IsEncrypted = false;
            IsLecture = false;
            VideoMode = VideoModeType.ContinuousPresence;
        }
        /// <summary>
        /// Gets or sets maximum audio ports.
        /// </summary>
        public int MaximumAudioPorts { get; set; }
        /// <summary>
        /// Gets or sets maximum video ports.
        /// </summary>
        public int MaximumVideoPorts { get; set; }
        /// <summary>
        /// Gets or sets audio/video protcol.
        /// </summary>
        public AudioVideoProtocol RestrictProtocol { get; set; }
        /// <summary>
        /// Gets or sets audio/video settings.
        /// </summary>
        public AudioVideoType RestrictAudioVideo { get; set; }
        /// <summary>
        /// Gets or sets image number for video layout.
        /// </summary>
        public int VideoLayout { get; set; }
        /// <summary>
        /// Gets or sets linerate for connection.
        /// </summary>
        public LineRateEnum MaximumLineRateId { get; set; }
        /// <summary>
        /// Gets or sets audio codec.
        /// </summary>
        public AudioCodecEnum AudioCodec { get; set; }
        /// <summary>
        /// Gets or sets video codec.
        /// </summary>
        public VideoCodecEnum VideoCodec { get; set; }
        /// <summary>
        /// Gets or sets dual stream mode.
        /// </summary>
        public bool IsDualStreamed { get; set; }
        /// <summary>
        /// Gets or sets conference on port setting for MCU.
        /// </summary>
        public bool ConferenceOnPort { get; set; }
        /// <summary>
        /// Gets or sets encryption mode.
        /// </summary>
        public bool IsEncrypted { get; set; }
        /// <summary>
        /// Gets or sets lecture mode.
        /// </summary>
        public bool IsLecture { get; set; }
        /// <summary>
        /// Gets or sets video mode.
        /// </summary>
        public VideoModeType VideoMode { get; set; }
        /// <summary>
        /// Gets or sets dialin mode for ISDN calls.
        /// </summary>
        public bool SingleDialin { get; set; }

        internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
        {
            switch(reader.LocalName)
            {
                case MaxAudioPartTagName:
                {
                    MaximumAudioPorts = reader.ReadValue<int>();
                    return true;
                }
                case MaxVideoPartTagName:
                {
                    MaximumVideoPorts = reader.ReadValue<int>();
                    return true;
                }
                case "restrictProtocol":
                {
                    RestrictProtocol = reader.ReadValue<AudioVideoProtocol>();
                    return true;
                }
                case "restrictAV":
                {
                    RestrictAudioVideo = reader.ReadValue<AudioVideoType>();
                    return true;
                }
                case "videoLayout":
                {
                    VideoLayout = reader.ReadValue<int>();
                    return true;
                }
                case "maxLineRateID":
                {
                    MaximumLineRateId = reader.ReadValue<LineRateEnum>();
                    return true;
                }
                case "audioCodec":
                {
                    AudioCodec = reader.ReadValue<AudioCodecEnum>();
                    return true;
                }
                case "videoCodec":
                {
                    VideoCodec = reader.ReadValue<VideoCodecEnum>();
                    return true;
                }
                case "dualStream":
                {
                    IsDualStreamed = Utilities.BoolStringToBool(reader.ReadValue());
                    return true;
                }
                case "confOnPort":
                {
                    ConferenceOnPort = Utilities.BoolStringToBool(reader.ReadValue());
                    return true;
                }
                case "encryption":
                {
                    IsEncrypted = Utilities.BoolStringToBool(reader.ReadValue());
                    return true;
                }
                case "lectureMode":
                {
                    IsLecture = Utilities.BoolStringToBool(reader.ReadValue());
                    return true;
                }
                case "VideoMode":
                {
                	try
                	{
						VideoMode = reader.ReadValue<VideoModeType>();
                	}
					catch (ArgumentException)
                	{
                		VideoMode = VideoModeType.Unassigned;
                	}
                    return true;
                }
                case "SingleDialin":
                {
                    SingleDialin = Utilities.BoolStringToBool(reader.ReadValue());
                    return true;
                }
            }
            return false;
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            base.WriteElementsToXml(writer);
            writer.WriteElementValue(XmlNamespace.NotSpecified, MaxAudioPartTagName, MaximumAudioPorts);
            writer.WriteElementValue(XmlNamespace.NotSpecified, MaxVideoPartTagName, MaximumVideoPorts);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "restrictProtocol", RestrictProtocol);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "restrictAV", RestrictAudioVideo);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "videoLayout", VideoLayout);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "maxLineRateID", MaximumLineRateId);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "audioCodec", AudioCodec);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "videoCodec", VideoCodec);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "dualStream", IsDualStreamed);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "confOnPort", ConferenceOnPort);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "encryption", IsEncrypted);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "lectureMode", IsLecture);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "VideoMode", VideoMode);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "SingleDialin", SingleDialin);
        }

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
    }
}
