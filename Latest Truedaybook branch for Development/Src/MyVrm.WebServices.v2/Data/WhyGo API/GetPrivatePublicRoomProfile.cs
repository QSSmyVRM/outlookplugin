﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	class GetPrivatePublicRoomProfileRequest : ServiceRequestBase<GetPrivatePublicRoomProfileResponse>
	{
		internal RoomId RoomID { get; set; }

		internal GetPrivatePublicRoomProfileRequest(MyVrmService service)
			: base(service)
		{
		}

		#region Overrides of ServiceRequestBase<GetTemplateResponse>

		internal override string GetXmlElementName()
		{
			return "GetPrivatePublicRooms";
		}

		internal override string GetCommandName()
		{
			return Constants.GetPrivatePublicRoomProfileCommandName;
		}

		internal override string GetResponseXmlElementName()
		{
			return "GetPrivatePublicRoomList";
		}

		internal override GetPrivatePublicRoomProfileResponse ParseResponse(MyVrmServiceXmlReader reader)
		{
			var response = new GetPrivatePublicRoomProfileResponse();
			response.ReadElementsFromXml(reader);
			return response;
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			if (UserId != null)
				UserId.WriteToXml(writer, "UserID");
		}

		#endregion
	}

	//Response
	public class GetPrivatePublicRoomProfileResponse : ServiceResponse
	{
		//Result
		public List<WGManagedRoom> WGRoomList = new List<WGManagedRoom>();

		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{
			//do
			//{
			//    //Forward till "GetPrivatePublicRoomList" element 
			//    while (!reader.IsEndElement(XmlNamespace.NotSpecified, "GetPrivatePublicRoomList"))
			//    {
			//        while (!reader.IsStartElement(XmlNamespace.NotSpecified, "GetPrivatePublicRoomList"))
			//            reader.Read();

			//        if (reader.LocalName == "GetPrivatePublicRoomList")
			//        {
			//            WGRoomList.Clear();
			//        }

			//        while (!reader.IsEndElement(XmlNamespace.NotSpecified, "GetPrivatePublicRoomList"))
			//        {
			//            while (!reader.IsStartElement(XmlNamespace.NotSpecified, "RoomID") &&
			//                    !reader.IsEndElement(XmlNamespace.NotSpecified, "GetPrivatePublicRoomList"))
			//                reader.Read();

			//            WGManagedRoom room = null;
			//            if (reader.LocalName == "RoomID")
			//            {
			//                room = new WGManagedRoom();
			//                room.RoomID = reader.ReadElementValue<int>();
			//                reader.Read();
			//            }
			//            if (reader.LocalName == "RoomName" && room != null)
			//            {
			//                room.RoomName = reader.ReadElementValue();
			//                reader.Read();
			//            }
			//            if (room != null)
			//                WGRoomList.Add(room);
			//        }
			//    }

			//} while (!reader.IsEndElement(XmlNamespace.NotSpecified, "GetPrivatePublicRoomList")); //read till the "GetPrivatePublicRoomList" node end 
		}
	}
}