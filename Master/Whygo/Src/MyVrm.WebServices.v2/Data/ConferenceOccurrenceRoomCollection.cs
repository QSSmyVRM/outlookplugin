﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
    public class ConferenceOccurrenceRoomCollection : ComplexPropertyCollection<ConferenceOccurrenceRoom>
    {
        #region Overrides of ComplexPropertyCollection<ConferenceOccurrenceRoom>

		public override object Clone()
		{
			ConferenceOccurrenceRoomCollection copy = new ConferenceOccurrenceRoomCollection();
			foreach (var item in this)
			{
				copy.InternalAdd((ConferenceOccurrenceRoom)item.Clone()); //???????
			}

			return copy;
		}

        internal override ConferenceOccurrenceRoom CreateComplexProperty(string xmlElementName)
        {
            return new ConferenceOccurrenceRoom();
        }

        internal override string GetCollectionItemXmlElementName(ConferenceOccurrenceRoom complexProperty)
        {
            return "location";
        }

        #endregion
    }
}
