﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;

namespace MyVrm.WebServices.Data
{
    internal class SearchProviderMenusRequest : ServiceRequestBase<SearchProviderMenusResponse>
    {
        internal SearchProviderMenusRequest(MyVrmService service) : base(service)
        {
        }

        internal RoomId RoomId { get; set; }
        internal int CateringServiceId { get; set;}

        #region Overrides of ServiceRequestBase

        internal override string GetXmlElementName()
        {
            return "SearchProviderMenus";
        }

        internal override string GetCommandName()
        {
            return "SearchProviderMenus";
        }

        internal override string GetResponseXmlElementName()
        {
            return "SearchProviderMenus";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            UserId.WriteToXml(writer, "UserID");
            RoomId.WriteToXml(writer, "RoomID");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "ServiceID", CateringServiceId);
        }

        #endregion
    }
}
