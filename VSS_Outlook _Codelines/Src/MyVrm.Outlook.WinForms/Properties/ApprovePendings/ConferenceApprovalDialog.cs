﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using MyVrm.WebServices.Data;
using MyVrm.Outlook.WinForms.Conference;

namespace MyVrm.Outlook.WinForms.ApprovePendings
{
    public partial class ConferenceApprovalDialog : MyVrm.Outlook.WinForms.Dialog
    {
        List<ConfInfo> _foundConfs = new List<ConfInfo>();
       public List<ConfInfo> _entire = new List<ConfInfo>();
        List<TransformedConfInfo> _transformedFoundConfs = new List<TransformedConfInfo>();
        String strDRoomName = String.Empty;
        Int32 intRoomID = 0;
      //  private readonly OutlookAppointmentImplCollection _appointments = new OutlookAppointmentImplCollection();
        public List<ConfInfo> FoundConfs
        {
            set { _foundConfs = value; }
            get { return _foundConfs; }
        }
        public List<ConfInfo> ListEntire
        {
            set { _entire = value; }
            get { return _entire; }
        }
        public Int32 itFlag = 0;
        public List<TransformedConfInfo> TransformedFoundConfs
        {
            set { _transformedFoundConfs = value; }
            get { return _transformedFoundConfs; }
        }

        public ConferenceApprovalDialog()
        {
            InitializeComponent();
            ApplyVisible = true;
            ApplyEnabled = true;
            ApplyText = Strings.ApplyBtnText;
            //_entire = ListEnt;
            uniqueIDColumn.Caption = Strings.UniqueIDHeaderTxt;
            //instanceTypeColumn.Caption = Strings.InstanceTypeHeaderTxt;
            conferenceNameColumn.Caption = Strings.ConferenceNameHeaderTxt;
            dataTimeColumn.Caption = Strings.ConferenceDataTimeHeaderTxt;
            conferenceTypeColumn.Caption = Strings.ConferenceTypeHeaderTxt;
            durationColumn.Caption = Strings.DurationHeaderTxt;
            approvalTypeColumn.Caption = Strings.NeedApproveForHeaderTxt;
           // AlternateSelection.Caption = "Alternate";
            decisionColumn.Caption = Strings.DecisionHeaderTxt;
            commentColumn.Caption = Strings.CommentHeaderTxt;
            durationColumn.Caption = Strings.DurationHeaderTxt; //ZD 102888
            Text = Strings.ConferenceApproveCaptionTxt;

            treeList1.BeginUpdate();
            treeList1.ToolTipController = new ToolTipController();
            treeList1.ToolTipController.GetActiveObjectInfo += ToolTipController_GetActiveObjectInfo;
            treeList1.EndUpdate();
        }

        private TransformedConfInfo _transformedConfInfo;
        public TransformedConfInfo TransformedConfInfoProp
        {
            set { _transformedConfInfo = value; }
            get { return _transformedConfInfo; }
        }
        void Transform()
        {
            int iKeyer = 1;

            _transformedFoundConfs.Clear();
            foreach (ConfInfo confInfo in FoundConfs)
            {
                foreach (ApprovalUnit approvalUnit in confInfo.ApprovedEntity.ApprovalUnitList)
                {
                    TransformedConfInfo tc = new TransformedConfInfo();
                    tc.ConfInfo = confInfo;
                    tc.AppUnit = approvalUnit;
                    if (tc.AppUnit.Name == string.Empty)
                        tc.AppUnit.Name = Strings.SystemTxt;//"System";
                    switch (tc.ConfInfo.Type)
                    {
                        case ConfType.RoomConference:
                            tc.ConfTypeName = Strings.RoomOnlyTxt;// "Room Only";
                            break;
                        case ConfType.PointToPoint:
                            tc.ConfTypeName = Strings.PointToPointTxt;// "Point-To-Point";
                            break;
                        case ConfType.AudioVideo:
                            tc.ConfTypeName = Strings.AudioVideoTxt;// "Audio/Video";
                            break;
                        case ConfType.VMR:
                            tc.ConfTypeName = Strings.VMRTxt;// "VMR";
                            break;
                        case ConfType.AudioOnly:
                            tc.ConfTypeName = Strings.AudioOnlyTxt;// "Audio Only";
                            break;
                        case ConfType.Hotdesking:
                            tc.ConfTypeName = Strings.Hotdesking;// "Hotdesking";
                            break;
                    }
                    tc.ParentID = 0;
                    tc.KeyID = confInfo.IsRecur &&
                        approvalUnit == confInfo.ApprovedEntity.ApprovalUnitList[confInfo.ApprovedEntity.ApprovalUnitList.Count - 1] ?
                        tc.ConfInfo.ConferenceUniqueId : iKeyer++;

                    tc.InstanceType = confInfo.IsRecur ? InstanceType.RecurrenceMain : InstanceType.Regular;
                    tc.InstanceType = tc.KeyID == tc.ConfInfo.ConferenceUniqueId ? InstanceType.RecurrenceMainExpandable : tc.InstanceType;
                    _transformedFoundConfs.Add(tc);
                }
            }
        }
        // ZD 101270 starts
        private void treeList1_CellValueChanging(object sender, DevExpress.XtraTreeList.CellValueChangedEventArgs e)
        {
            if (treeList1.FocusedNode != null)
            {
                String StrRoomName = String.Empty;
                TransformedConfInfo foundConf;
                if(!e.Node.HasChildren)
                    foundConf = TransformedFoundConfs.Find(val => val.KeyID == ((int)(e.Node.Id)) + 1);
                else
                    foundConf = TransformedFoundConfs.Find(
                        val => val.ConfInfo.ConferenceUniqueId == (int)(e.Node[uniqueIDColumn]));
                ApprovedEntry newEntry = new ApprovedEntry();
                newEntry.Level = foundConf.ConfInfo.ApprovedEntity.Level;
                if (e.Column == decisionColumn)
                {
                    //if (e.Value.ToString() == "Alternate") //Strings.Alternate
                    if (e.Value.ToString() == "Alternate") //ZD 102888
                    {
                        if (newEntry.Level == Level.MCU || newEntry.Level == Level.System)
                        {
                            UIHelper.ShowMessage(Strings.Alternateselection); //ZD 102888
                            //foundConf.AppUnit.Decision = Decision.Undecided;
                            e.Node[decisionColumn] = "Undecided";
                        }
                        else
                        {
                            using (var dlg = new FavoriteRoomSelectionDialog() { })
                            {
                                dlg.Text = Strings.AlternateRoomSelection; //ZD 103440 // ZD 103667
                                if (dlg.ShowDialog() == DialogResult.OK)
                                {
                                    if (dlg.SelectedRooms.Length > 1)
                                    {
                                        UIHelper.ShowError(Strings.RoomAlternateselection); //ZD 102888
                                        //foundConf.AppUnit.Decision  = Decision.Undecided ;
                                        e.Node[decisionColumn] = "Undecided";
                                    }
                                    else
                                    {
                                        StrRoomName += MyVrmAddin.Instance.GetRoomFromCache(new RoomId(dlg.SelectedRooms[0].ConfEndPt.Id.Id)).Name;
                                        if (intRoomID == 0)
                                        {
                                            intRoomID = foundConf.AppUnit.ID;
                                        }
                                        foundConf.AppUnit.ID = Convert.ToInt32(dlg.SelectedRooms[0].ConfEndPt.Id.Id);
                                        if(string.IsNullOrEmpty(strDRoomName))
                                        strDRoomName = e.Node[approvalTypeColumn].ToString();
                                        // ConfInfo conf=e.Node[uniqueIDColumn]
                                        e.Node[approvalTypeColumn] = StrRoomName;
                                       // foundConf.AppUnit.Decision = Decision.Alternate;
                                        //e.Node[decisionColumn] = "Alternate";
                                        e.Node[decisionColumn] = Strings.Alternate; //ZD 102888
                                    }
                                }

                            }
                        }
                    }
                    else 
                    {
                        if (!String.IsNullOrEmpty(strDRoomName))
                        {
                            e.Node[approvalTypeColumn] = strDRoomName;
                            //TransformedConfInfo foundConf = TransformedFoundConfs.Find(val => val.ConfInfo.ConferenceUniqueId == (int)(e.Node[uniqueIDColumn]));
                            foundConf.AppUnit.ID = intRoomID;
                            strDRoomName = "";
                            intRoomID = 0;
                           // foundConf.AppUnit.Decision = Decision.Undecided;
                            //e.Node[decisionColumn] = "Undecided";
                        }
                    }

                }
            }
        }
        // ZD 101270 ends

        private void OnLoad(object sender, EventArgs e)
        {
            Transform();
            // ZD 101270 starts
            //List<System.Collections.DictionaryEntry> ds = new List<DictionaryEntry>();
            //ds.Add(new DictionaryEntry(strAlternate.Alternate, "Alternate"/*"Alternate"*/));
            //ds.Add(new DictionaryEntry(strAlternate.Normal, "Normal"/*"Normal"*/));
            //repositoryalternateselection.DataSource = ds;
            //repositoryalternateselection.DisplayMember = "Value";
            //repositoryalternateselection.ValueMember = "Key";  
            // ZD 101270 ends
            // repositoryalternateselection.ReadOnly = false;

            List<System.Collections.DictionaryEntry> de = new List<DictionaryEntry>();
            de.Add(new DictionaryEntry(Decision.Undecided, Strings.UndecidedTxt/*"Undecided"*/));
            de.Add(new DictionaryEntry(Decision.Approved, Strings.ApprovedTxt/*"Approved"*/));
            de.Add(new DictionaryEntry(Decision.Denied, Strings.DeniedTxt/*"Denied"*/));
            de.Add(new DictionaryEntry(Decision.Alternate,Strings.Alternate /*Alternate*/)); //ZD 102888
            repositoryDecisionComboBox.DataSource = de;
            repositoryDecisionComboBox.DisplayMember = "Value";
            repositoryDecisionComboBox.ValueMember = "Key";

            treeList1.KeyFieldName = "KeyID";
            treeList1.ParentFieldName = "ParentID";

            treeList1.DataSource = TransformedFoundConfs;
            SetupExpandables();
        }

        private void OnCustomDrawNodeCell(object sender, DevExpress.XtraTreeList.CustomDrawNodeCellEventArgs e)
        {
            if (e.Node != null && (InstanceType)e.Node[instanceTypeColumn] != InstanceType.RecurrenceOccurence)
            {
                e.Appearance.BackColor = e.Node.Focused ? SystemColors.HotTrack : Color.LightBlue;//Azure;
                //e.Appearance.ForeColor = e.Node.Focused ? Color.Snow/* DarkBlue*/ : Color.Black;
                //e.Appearance.BackColor = //RGB(36, 112, 190);//SystemColors.HotTrack;//Color.SteelBlue //
            }

            if (e.Column == approvalTypeColumn && e.Node != null && e.Node.Tag != null)
            {
                switch ((Level)e.Node.Tag)
                {
                    case Level.Room:
                        e.Appearance.BackColor = Color.LightCoral;
                        break;
                    case Level.MCU:
                        e.Appearance.BackColor = Color.LightSkyBlue;
                        break;
                    case Level.System:
                        e.Appearance.BackColor = Color.Snow;
                        e.Appearance.ForeColor = Color.Black;
                        //    e.Appearance.BackColor = Color.LightGray; 
                        break;
                }
            }

            if (e.Column == uniqueIDColumn || e.Column == conferenceNameColumn ||
                e.Column == dataTimeColumn || e.Column == conferenceTypeColumn || e.Column == durationColumn)
            {
                TransformedConfInfo foundConf = TransformedFoundConfs.Find(
                        val => val.ConfInfo.ConferenceUniqueId == (int)(e.Node[uniqueIDColumn]));
                if (e.Node != null && (string)e.Node[approvalTypeColumn] != foundConf.ConfInfo.ApprovedEntity.ApprovalUnitList[0].Name &&
                    foundConf.ConfInfo.ApprovedEntity.ApprovalUnitList[0].Name != string.Empty)
                {
                    e.Appearance.ForeColor = e.Appearance.BackColor;
                }
            }
        }

        static bool _doBlock;
        private void OnBeforeExpand(object sender, DevExpress.XtraTreeList.BeforeExpandEventArgs e)
        {
            if (e.Node.Nodes.Count == 0 && _doBlock == false)
            {
                int uniqueID = (int)e.Node[uniqueIDColumn];
                var cursor = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                try
                {
                    TransformedConfInfo conf = TransformedFoundConfs.Find(
                        val => val.ConfInfo.ConferenceUniqueId == uniqueID);
                    string conferenceId = conf.ConfInfo.ConferenceId;

                    int iCommaNdx = conferenceId.IndexOf(",");

                    if (iCommaNdx != -1)
                        conferenceId = conferenceId.Substring(0, iCommaNdx); //get main conference ID

                    SearchConferenceResponse response = MyVrmService.Service.SearchConference(TApprovalPending.Approval, string.Empty,
                                                                                              conferenceId,  //null,
                                                                                              (string)e.Node[conferenceNameColumn], 0,
                                                                                              TConferenceSearchType.undefined,
                                                                                              TConferenceStatus.Pending,
                                                                                              0, DateTime.MaxValue, DateTime.MinValue,
                                                                                              TPublic.undefined,
                                                                                              TRecurrenceStyle.Single,
                                                                                              1 /*???*/,
                                                                                              TSortBy.ConferenceStartDateTime,
                                                                                              TSelectionType.Any,
                                                                                              string.Empty, null);
                    foreach (ConfInfo confInfo in response.foundConfs)
                    {
                        //if (confInfo.ConferenceUniqueId == uniqueID)
                        //    continue;
                        foreach (ApprovalUnit approvalUnit in confInfo.ApprovedEntity.ApprovalUnitList)
                        {
                            TransformedConfInfo tc = new TransformedConfInfo();
                            tc.ConfInfo = confInfo;
                            tc.AppUnit = approvalUnit;
                            if (tc.AppUnit.Name == string.Empty)
                                tc.AppUnit.Name = Strings.SystemTxt;//"System";
                            switch (tc.ConfInfo.Type)
                            {
                                case ConfType.RoomConference:
                                    tc.ConfTypeName = Strings.RoomOnlyTxt;//"Room Only";
                                    break;
                                case ConfType.PointToPoint:
                                    tc.ConfTypeName = Strings.PointToPointTxt;//"Point-To-Point";
                                    break;
                                case ConfType.AudioVideo:
                                    tc.ConfTypeName = Strings.AudioVideoTxt;//"Audio/Video";
                                    break;
                                case ConfType.VMR:
                                    tc.ConfTypeName = Strings.VMRTxt;//"VMR";
                                    break;
                                case ConfType.AudioOnly:
                                    tc.ConfTypeName = Strings.AudioOnlyTxt;//"Audio Only";
                                    break;
                                case ConfType.Hotdesking:
                                    tc.ConfTypeName = Strings.Hotdesking;// "Hotdesking";
                                    break;
                            }
                            tc.ParentID = uniqueID;
                            tc.KeyID = TransformedFoundConfs.Count + 1;
                            tc.InstanceType = InstanceType.RecurrenceOccurence;
                            _transformedFoundConfs.Add(tc);
                        }
                    }
                }
                catch (Exception exception)
                {
                    MyVrmAddin.TraceSource.TraceException(exception, true);
                    UIHelper.ShowMessage(exception.Message, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                finally
                {
                    Cursor.Current = cursor;
                }

                _doBlock = true;
                e.Node.Expanded = true;
                treeList1.RefreshDataSource();
                SetupExpandables();

                _doBlock = false;
            }
        }
        void SetupExpandables()
        {
            foreach (TreeListNode node in treeList1.Nodes)
            {
                if (node != null)
                {
                    TransformedConfInfo conf = TransformedFoundConfs.Find(
                        val => val.ConfInfo.ConferenceUniqueId == (int)(node[uniqueIDColumn]));

                    node.Tag = conf.ConfInfo.ApprovedEntity.Level;
                    if ((InstanceType)node[instanceTypeColumn] == InstanceType.RecurrenceMainExpandable)
                    {
                        if (node.Nodes.Count > 0) //has occurances
                        {
                            foreach (TreeListNode childNode in node.Nodes)
                            {
                                TransformedConfInfo childConf = TransformedFoundConfs.Find(val => val.ConfInfo.ConferenceUniqueId ==
                                                                                                (int)(childNode[uniqueIDColumn]));

                                childNode.Tag = childConf.ConfInfo.ApprovedEntity.Level;
                            }
                        }
                        node.HasChildren = true;
                    }
                    //if (conf.ConfInfo.IsRecur && (InstanceType)node[instanceTypeColumn] == InstanceType.RecurrenceMain)
                    //{
                    //    tc.Expandable = tc.KeyID == tc.ConfInfo.ConferenceUniqueId;
                    //    node.HasChildren = true;
                    //}
                }
            }
        }
        void ToolTipController_GetActiveObjectInfo(object sender, DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventArgs e)
        {
            TreeListHitInfo hit = treeList1.CalcHitInfo(e.ControlMousePosition);

            //if (hit != null && hit.Column != null)
            //{
            //    MyVrmService.TraceSource.TraceInformation(hit.Column.Caption);
            //}

            if (hit != null && hit.Column == approvalTypeColumn)
            {
                TreeListNode curNode = hit.Node;
                if (curNode != null && curNode.Tag != null)
                {
                    string tip = string.Empty;
                    switch ((Level)curNode.Tag)
                    {
                        case Level.Room:
                            tip = Strings.RoomApprovalTooltipTxt;//"Room approval";
                            break;
                        case Level.MCU:
                            tip = Strings.MCUApprovalTooltipTxt;//"MCU approval";
                            break;
                        case Level.System:
                            tip = Strings.SystemApprovalTooltipTxt;//"System approval";
                            break;
                    }

                    e.Info = new ToolTipControlInfo(curNode, string.Empty,
                                                      tip, true, ToolTipIconType.None, DefaultBoolean.True);
                }
            }
            if (hit != null && hit.Column == uniqueIDColumn)
            {
                TreeListNode curNode = hit.Node;
                if (curNode != null)
                {
                    string tip = string.Empty;

                    switch ((InstanceType)curNode[instanceTypeColumn])
                    {
                        case InstanceType.RecurrenceMain:
                        case InstanceType.RecurrenceMainExpandable:
                            tip = Strings.RecurrentConferenceTooltipTxt;//"Recurrent conference";
                            break;
                        case InstanceType.RecurrenceOccurence:
                            tip = Strings.RecurrentConferenceOccurenceTooltipTxt;//"Recurrent conference occurence";
                            break;
                        case InstanceType.Regular:
                            tip = Strings.NormalConferenceTooltipTxt;//"Normal conference";
                            break;
                    }

                    if (tip.Length > 0)
                    {
                        e.Info = new ToolTipControlInfo(curNode, string.Empty, tip, true, ToolTipIconType.None, DefaultBoolean.True);
                    }
                }
            }
        }

        //Called on Apply button
        private void OnClosing(object sender, FormClosingEventArgs e)
        {
            if (((ConferenceApprovalDialog)sender).DialogResult == DialogResult.Retry || ((ConferenceApprovalDialog)sender).DialogResult == DialogResult.OK)
            {
                List<TransformedConfInfo> changedConfs =
                            TransformedFoundConfs.FindAll(conf => conf.AppUnit.Decision != Decision.Undecided);
                if (changedConfs.Count > 0)
                {
                    List<ConferenceToApprove> conferenceToApproves = new List<ConferenceToApprove>();
                    foreach (TransformedConfInfo transformedConfInfo in changedConfs)
                    {
                        string[] mainID = transformedConfInfo.ConfInfo.ConferenceId.Split(',');
                        ConferenceToApprove conf = new ConferenceToApprove()
                                                    {
                                                        ConfID = transformedConfInfo.ConfInfo.IsRecur
                                                                    ? mainID[0]
                                                                    : transformedConfInfo.ConfInfo.ConferenceId,
                                                        InstanceType = transformedConfInfo.ConfInfo.IsRecur ? "I" : "A",
                                                    };
                        ApprovedEntry newEntry = new ApprovedEntry();
                        newEntry.Level = transformedConfInfo.ConfInfo.ApprovedEntity.Level;
                        switch (transformedConfInfo.ConfInfo.ApprovedEntity.Level)
                        {
                            case Level.System:
                                ApprovalSystem sys = new ApprovalSystem()
                                                        {
                                                            Comment = transformedConfInfo.AppUnit.Comment,
                                                            Decision = transformedConfInfo.AppUnit.Decision,
                                                            ID = transformedConfInfo.AppUnit.ID,
                                                            PrevID = 0
                                                        };
                                newEntry.ApprovalUnitList.Add(sys);
                                break;
                            case Level.Room:
                                if (transformedConfInfo.AppUnit.Decision != Decision.Alternate)// ZD 101270
                                {
                                    ApprovalRoom room = new ApprovalRoom()
                                                            {
                                                                Comment = transformedConfInfo.AppUnit.Comment,
                                                                Decision = transformedConfInfo.AppUnit.Decision,
                                                                ID = transformedConfInfo.AppUnit.ID,
                                                                PrevID = 0
                                                            };
                                    newEntry.ApprovalUnitList.Add(room);
                                    break;
                                }
                                else// ZD 101270 starts
                                  {
                                    itFlag = 1;
                                    ApprovalRoom room = new ApprovalRoom()
                                    {
                                        Comment = transformedConfInfo.AppUnit.Comment,
                                        Decision = Decision.Approved,
                                        ID = transformedConfInfo.AppUnit.ID,
                                        PrevID = intRoomID
                                    };
                                    
                                    newEntry.ApprovalUnitList.Add(room);
                                  //  var conferenceId = transformedConfInfo.ConfInfo.ConferenceId;
                                   
                                  
                                    intRoomID = 0;
                                    strDRoomName = "";
                                     break;
                                }// ZD 101270 Ends
                            case Level.MCU:
                                ApprovalMCU mcu = new ApprovalMCU() 
                                                    {
                                                        Comment = transformedConfInfo.AppUnit.Comment,
                                                        Decision = transformedConfInfo.AppUnit.Decision,
                                                        ID = transformedConfInfo.AppUnit.ID,
                                                        PrevID = 0// ZD 101270
                                                    };
                                newEntry.ApprovalUnitList.Add(mcu);
                                break;

                        }
                        conf.PartyInfoList.Add(newEntry);
                        conferenceToApproves.Add(conf);
                    }

                    var cursor = Cursor.Current;
                    Cursor.Current = Cursors.WaitCursor;
                    try
                    {
                        SetApproveConferenceResponse responce = MyVrmService.Service.SetApproveConference(conferenceToApproves);
                    }
                    catch (Exception exception)
                    {
                        MyVrmAddin.TraceSource.TraceException(exception, true);
                        UIHelper.ShowMessage(exception.Message, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    finally
                    {
                        Cursor.Current = cursor;
                    }
                }
            }

            //Re-draw
            try
            {
                if (((ConferenceApprovalDialog)sender).DialogResult == DialogResult.Retry)
                {
                    //FoundConfs.Clear();
                    treeList1.ClearNodes();

                    GetConfsForApprove(out _foundConfs);
                    if (FoundConfs.Count > 0)
                    {
                        Transform();
                        treeList1.RefreshDataSource();
                        SetupExpandables();
                        e.Cancel = true;
                    }
                    else
                    {
                        UIHelper.ShowMessage(Strings.NoMoreConferencesToApproveMsg, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = false;
                    }
                }
            }
            catch (Exception exception)
            {
                MyVrmAddin.TraceSource.TraceException(exception, true);
                UIHelper.ShowMessage(exception.Message, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public static void GetConfsForApprove(out List<ConfInfo> FoundConfs)
        {
            var cursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            FoundConfs = new List<ConfInfo>();

            try
            {
                //Get pending conferences
                SearchConferenceResponse response = MyVrmService.Service.SearchConference(TApprovalPending.Approval, string.Empty,
                                                                                          null, string.Empty, 0,
                                                                                          TConferenceSearchType.undefined,
                                                                                          TConferenceStatus.Pending,
                                                                                          0, DateTime.MaxValue, DateTime.MinValue,
                                                                                          TPublic.undefined,
                                                                                          TRecurrenceStyle.Recurrent /* ?? Single*/,
                                                                                          1 /*???*/, TSortBy.ConferenceStartDateTime,
                                                                                          TSelectionType.Any,
                                                                                          string.Empty, null);
                if (response != null)
                    FoundConfs = response.foundConfs;
            }
            catch (Exception exception)
            {
                MyVrmAddin.TraceSource.TraceException(exception, true);
                UIHelper.ShowMessage(exception.Message, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                Cursor.Current = cursor;
            }
        }

        private void OnAfterExpand(object sender, NodeEventArgs e)
        {
            //			SetupExpandables();

            treeList1.MakeNodeVisible(e.Node);
            treeList1.FocusedNode = e.Node;
            //e.Node.Expanded = true;
        }
    }
}
