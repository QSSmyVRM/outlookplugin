﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System.Text;

namespace MyVrm.WebServices.Data
{
    public enum ParticipantInvitationMode
    {
        CcOnly = 0,
        External = 1,
        Room = 2
    }

    public class Participant : ComplexProperty
    {
		public override object Clone()//CopyTo(Participant participant)
		{
			//if (participant == null)
			Participant participant = new Participant();
			participant.AudioVideoMode = AudioVideoMode;
			participant.Email = Email;
			participant.FirstName = FirstName;
			participant.InvitationMode = InvitationMode;
			participant.LastName = LastName;
			participant.Notify = Notify;
			if (UserId != null)
				participant.UserId = new UserId(UserId.Id);
			return participant;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(this, obj))
				return true;

			Participant participant = obj as Participant;
			if (participant == null)
				return false;
			if (participant.AudioVideoMode != AudioVideoMode)
				return false;
			if (participant.Email != Email)
				return false;
			if (participant.FirstName != FirstName)
				return false;
			if (participant.InvitationMode != InvitationMode)
				return false;
			if (participant.LastName != LastName)
				return false;
			if (participant.Notify != Notify)
				return false;
			if (participant.UserId.Id != UserId.Id)
				return false;
			
			return true;
		}

        internal Participant()
        {
            InvitationMode = ParticipantInvitationMode.Room;
            AudioVideoMode = MediaType.AudioVideo;
        }

        public Participant(UserId id)
        {
            UserId = id;
        }
        
        public Participant(string firstName, string lastName, string email)
        {
            UserId = UserId.New;
            FirstName = firstName;
            LastName = lastName;
            Email = email;
        }

        public Participant(string email)
        {
            UserId = UserId.New;
            Email = email;
        }

        public UserId UserId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public bool Notify { get; set; }

        public MediaType AudioVideoMode { get; set; }

        public ParticipantInvitationMode InvitationMode { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            if (!string.IsNullOrEmpty(FirstName) || !string.IsNullOrEmpty(LastName))
            {
                sb.Append(FirstName + " " + LastName);
            }
            if (!string.IsNullOrEmpty(Email))
            {
                if (sb.Length > 0)
                    sb.Append(" (" + Email + ")");
                else
                    sb.Append(Email);
            }
            else
            {
                sb.Append(base.ToString());
            }
            return sb.ToString();
        }

        internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
        {
            switch(reader.LocalName)
            {
                case "partyID":
                {
                    UserId = new UserId();
                    UserId.LoadFromXml(reader, "partyID");
                    return true;
                }
                case "partyFirstName":
                {
                    FirstName = reader.ReadValue();
                    return true;
                }
                case "partyLastName":
                {
                    LastName = reader.ReadValue();
                    return true;
                }
                case "partyEmail":
                {
                    Email = reader.ReadValue();
                    return true;
                }
                case "partyInvite":
                {
                    InvitationMode = reader.ReadValue<ParticipantInvitationMode>();
                    return true;
                }
                case "partyNotify":
                {
                    Notify = Utilities.BoolStringToBool(reader.ReadValue());
                    return true;
                }
                case "partyAudVid":
                {
                    AudioVideoMode = reader.ReadValue<MediaType>();
                    return true;
                }
            }
            return false;
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            UserId.WriteToXml(writer, "partyID");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "partyFirstName", FirstName);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "partyLastName", LastName);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "partyEmail", Email);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "partyInvite", InvitationMode);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "partyNotify", Notify);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "partyAudVid", AudioVideoMode);
        }

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
    }
}
