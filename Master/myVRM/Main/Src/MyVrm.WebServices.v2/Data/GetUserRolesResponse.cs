﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    internal class GetUserRolesResponse : ServiceResponse
    {
        public GetUserRolesResponse()
        {
            UserRoles = new UserRoleCollection();
        }

        internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
        {
            reader.ReadStartElement(XmlNamespace.NotSpecified, "roles");
            UserRoles.LoadFromXml(reader, "roles");
        }

        internal UserRoleCollection UserRoles { get; private set; }
    }
}
