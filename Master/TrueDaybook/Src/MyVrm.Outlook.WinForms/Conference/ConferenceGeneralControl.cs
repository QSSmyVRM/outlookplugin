﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System.Windows.Forms;
using DevExpress.Utils;
using MyVrm.Common.Collections;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook.WinForms.Conference
{
    public partial class ConferenceGeneralControl : ConferenceAwareControl
    {

        public ConferenceGeneralControl()
        {
            InitializeComponent();
            conferencePasswordEdit.Visible = false;
            publicConferenceCheckEdit.Visible = false;
            
            conferenceTypeEdit.Visible = false;
            layoutControlItem16.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            layoutControlItem1.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            layoutControlItem6.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            
			conferencePasswordEdit.SuperTip.Items.Clear();
			conferencePasswordEdit.SuperTip.Items.Add(Strings.ConferencePasswordLableText);
			
			publicConferenceCheckEdit.SuperTip.Items.Clear();
        	publicConferenceCheckEdit.SuperTip.Items.Add(Strings.PublicConferenceLableText);
			publicConferenceCheckEdit.SuperTip.Items.Add(Strings.PublicConferenceCheckBoxText);
			
			conferenceTypeEdit.SuperTip.Items.Clear();
        	conferenceTypeEdit.SuperTip.Items.Add(Strings.ConferenceTypeLableText);
			conferenceTypeEdit.SuperTip.Items.Add(Strings.SelectConferenceTypeToolTipText);

			layoutControlItem1.Text = Strings.ConferenceTypeLableText + ":";
			layoutControlItem1.CustomizationFormText = Strings.ConferenceTypeLableText + ":";
			layoutControlItem16.Text = Strings.ConferencePasswordLableText + ":";
			layoutControlItem16.CustomizationFormText = Strings.ConferencePasswordLableText + ":";
			publicConferenceCheckEdit.Properties.Caption = Strings.PublicConferenceLableText;
			conferenceTypeEdit.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(conferenceTypeEdit_EditValueChanging);
        }

		void conferenceTypeEdit_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
		{
			if (e.NewValue != e.OldValue && e.OldValue != null)
			{
				if (Conference != null)
					Conference.Appointment.SetNonOutlookProperty(true);
			}
		}

        protected override void OnReadOnlyChanged(ReadOnlyChangedEventArgs e)
        {
            conferenceTypeEdit.Enabled = !e.ReadOnly;
            conferencePasswordEdit.Enabled = !e.ReadOnly;
            publicConferenceCheckEdit.Enabled = !e.ReadOnly;
        }

        private void ConferenceGeneralControl_Load(object sender, System.EventArgs e)
        {
            //if (!DesignMode)
            //{
            //    conferenceTypeEdit.Properties.Items.AddRange(
            //        new EnumArrayListSource(MyVrmService.Service.OrganizationOptions.EnabledConferenceTypes,
            //                                typeof (ConferenceType)).GetList());

            //    conferencePasswordEdit.DataBindings.Add("EditValue", ConferenceBindingSource, "Password", true,
            //                                            DataSourceUpdateMode.OnPropertyChanged);
            //    conferenceTypeEdit.DataBindings.Add("EditValue", ConferenceBindingSource, "Type", true,
            //                                        DataSourceUpdateMode.OnPropertyChanged);
            //    publicConferenceCheckEdit.DataBindings.Add("Checked", ConferenceBindingSource, "IsPublic", true,
            //                                               DataSourceUpdateMode.OnPropertyChanged);
            //}
        }
    }
}