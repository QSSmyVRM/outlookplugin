﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

namespace MyVrm.Outlook.WinForms
{
    /// <summary>
    /// Represents a combo box used to specify a time zone.
    /// </summary>
    public class TimeZoneComboBoxEdit : ComboBoxEdit
    {
        private readonly ReadOnlyCollection<TimeZoneInfo> _timeZones;

        public TimeZoneComboBoxEdit()
        {
            _timeZones = TimeZoneInfo.GetSystemTimeZones();
            Properties.Sorted = false;
            Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
            if (!DesignMode)
            {
                Properties.Items.AddRange(_timeZones);
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false),
         EditorBrowsable(EditorBrowsableState.Never)]
        public TimeZoneInfo TimeZone
        {
            get
            {
                return (TimeZoneInfo) SelectedItem;
            }
            set
            {
                SelectedIndex = _timeZones.IndexOf(value);
            }
        }
    }
}
