﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.WebServices.Data
{
    public class WorkOrderCollection : WorkOrderCollectionBase<WorkOrder>
    {
		public override object Clone()
		{
			WorkOrderCollection copy = new WorkOrderCollection();
			foreach (var item in this)
			{
				copy.Add((WorkOrder)item.Clone()); //???????
			}

			return copy;
		}

        internal override void Save()
        {
            // Delete removed work orders
            foreach (var workOrder in RemovedWorkOrders)
            {
                Conference.Service.DeleteWorkOrder(Conference.Id, workOrder.Id);
            }
            // Add new or update modified work orders
            if (Count > 0)
            {
                Conference.Service.SaveConferenceWorkOrders(Conference.Id, this);
            }
            ClearChangeLog();
        }
    }
}
