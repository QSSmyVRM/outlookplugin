﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System.Collections.Generic;

namespace MyVrm.WebServices.Data.Files
{
    internal class UploadFilesResponse : ServiceResponse
    {
        private const string FilesElementName = "Files";
        private const string FileElementName = "File";

        private readonly List<string> _paths = new List<string>();

        internal IEnumerable<string> Paths
        {
            get { return _paths; }
        }

        internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
        {
            reader.Read();
            if (reader.IsStartElement(XmlNamespace.NotSpecified, FilesElementName))
            {
                do
                {
                    reader.Read();
                    if (reader.IsStartElement(XmlNamespace.NotSpecified, FileElementName))
                    {
                        _paths.Add(reader.ReadValue());
                    }
                } while (reader.IsEndElement(XmlNamespace.NotSpecified, FilesElementName));
                reader.ReadEndElement(XmlNamespace.NotSpecified, FilesElementName);
            }
        }
    }
}