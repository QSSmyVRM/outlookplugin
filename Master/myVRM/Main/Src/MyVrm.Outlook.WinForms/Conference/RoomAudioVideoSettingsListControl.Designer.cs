/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.Outlook.WinForms.Conference
{
    partial class RoomAudioVideoSettingsListControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			this.bindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.endpointsTreeList = new DevExpress.XtraTreeList.TreeList();
			this.roomNameColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.endpointColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.endpointRepositoryItem = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
			this.endpointProfileColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.selectedBrigeProfileColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.callerColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.useDefaultColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.useDefaultRepositoryItem = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
			this.bridgeNameColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.repositoryBridgeComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
			this.repositoryBridgeProfileNameComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
			((System.ComponentModel.ISupportInitialize)(this.bindingSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.endpointsTreeList)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.endpointRepositoryItem)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.useDefaultRepositoryItem)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.repositoryBridgeComboBox)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.repositoryBridgeProfileNameComboBox)).BeginInit();
			this.SuspendLayout();
			// 
			// bindingSource
			// 
			this.bindingSource.DataSourceChanged += new System.EventHandler(this.bindingSource_DataSourceChanged);
			this.bindingSource.ListChanged += new System.ComponentModel.ListChangedEventHandler(this.bindingSource_ListChanged);
			// 
			// endpointsTreeList
			// 
			this.endpointsTreeList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.roomNameColumn,
            this.endpointColumn,
            this.endpointProfileColumn,
            this.selectedBrigeProfileColumn,
            this.callerColumn,
            this.useDefaultColumn,
            this.bridgeNameColumn});
			this.endpointsTreeList.DataSource = this.bindingSource;
			this.endpointsTreeList.Dock = System.Windows.Forms.DockStyle.Fill;
			this.endpointsTreeList.Location = new System.Drawing.Point(0, 0);
			this.endpointsTreeList.Name = "endpointsTreeList";
			this.endpointsTreeList.OptionsView.ShowButtons = false;
			this.endpointsTreeList.OptionsView.ShowHorzLines = false;
			this.endpointsTreeList.OptionsView.ShowIndicator = false;
			this.endpointsTreeList.OptionsView.ShowRoot = false;
			this.endpointsTreeList.OptionsView.ShowVertLines = false;
			this.endpointsTreeList.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.useDefaultRepositoryItem,
            this.endpointRepositoryItem,
            this.repositoryBridgeComboBox,
            this.repositoryBridgeProfileNameComboBox});
			this.endpointsTreeList.ShowButtonMode = DevExpress.XtraTreeList.ShowButtonModeEnum.ShowAlways;
			this.endpointsTreeList.Size = new System.Drawing.Size(493, 203);
			this.endpointsTreeList.TabIndex = 0;
			this.endpointsTreeList.CustomNodeCellEdit += new DevExpress.XtraTreeList.GetCustomNodeCellEditEventHandler(this.endpointsTreeList_CustomNodeCellEdit);
			this.endpointsTreeList.CellValueChanging += new DevExpress.XtraTreeList.CellValueChangedEventHandler(this.endpointsTreeList_CellValueChanging);
			this.endpointsTreeList.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.endpointsTreeList_ShowingEditor);
			// 
			// roomNameColumn
			// 
			this.roomNameColumn.Caption = "Room Name";
			this.roomNameColumn.FieldName = "RoomName";
			this.roomNameColumn.Name = "roomNameColumn";
			this.roomNameColumn.OptionsColumn.AllowEdit = false;
			this.roomNameColumn.OptionsColumn.AllowFocus = false;
			this.roomNameColumn.OptionsColumn.AllowMove = false;
			this.roomNameColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
			this.roomNameColumn.OptionsColumn.AllowSort = false;
			this.roomNameColumn.OptionsColumn.ReadOnly = true;
			this.roomNameColumn.OptionsColumn.ShowInCustomizationForm = false;
			this.roomNameColumn.Visible = true;
			this.roomNameColumn.VisibleIndex = 0;
			this.roomNameColumn.Width = 83;
			// 
			// endpointColumn
			// 
			this.endpointColumn.Caption = "Endpoint";
			this.endpointColumn.ColumnEdit = this.endpointRepositoryItem;
			this.endpointColumn.FieldName = "Name";
			this.endpointColumn.Name = "endpointColumn";
			this.endpointColumn.OptionsColumn.AllowEdit = false;
			this.endpointColumn.OptionsColumn.AllowFocus = false;
			this.endpointColumn.OptionsColumn.AllowMove = false;
			this.endpointColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
			this.endpointColumn.OptionsColumn.AllowSort = false;
			this.endpointColumn.OptionsColumn.ReadOnly = true;
			this.endpointColumn.OptionsColumn.ShowInCustomizationForm = false;
			this.endpointColumn.Visible = true;
			this.endpointColumn.VisibleIndex = 1;
			this.endpointColumn.Width = 62;
			// 
			// endpointRepositoryItem
			// 
			this.endpointRepositoryItem.AutoHeight = false;
			this.endpointRepositoryItem.Name = "endpointRepositoryItem";
			this.endpointRepositoryItem.NullText = "There are no endpoints for this room";
			this.endpointRepositoryItem.ReadOnly = true;
			// 
			// endpointProfileColumn
			// 
			this.endpointProfileColumn.AppearanceCell.Options.UseTextOptions = true;
			this.endpointProfileColumn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.endpointProfileColumn.Caption = "Endpoint Profile";
			this.endpointProfileColumn.FieldName = "ProfileId";
			this.endpointProfileColumn.Name = "endpointProfileColumn";
			this.endpointProfileColumn.OptionsColumn.AllowMove = false;
			this.endpointProfileColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
			this.endpointProfileColumn.OptionsColumn.AllowSort = false;
			this.endpointProfileColumn.OptionsColumn.ShowInCustomizationForm = false;
			this.endpointProfileColumn.Visible = true;
			this.endpointProfileColumn.VisibleIndex = 2;
			this.endpointProfileColumn.Width = 102;
			// 
			// selectedBrigeProfileColumn
			// 
			this.selectedBrigeProfileColumn.AppearanceCell.Options.UseTextOptions = true;
			this.selectedBrigeProfileColumn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.selectedBrigeProfileColumn.Caption = "Select Profile";
			this.selectedBrigeProfileColumn.FieldName = "BridgeProfileName";
			this.selectedBrigeProfileColumn.Name = "selectedBrigeProfileColumn";
			this.selectedBrigeProfileColumn.OptionsColumn.AllowMove = false;
			this.selectedBrigeProfileColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
			this.selectedBrigeProfileColumn.OptionsColumn.AllowSort = false;
			this.selectedBrigeProfileColumn.OptionsColumn.ShowInCustomizationForm = false;
			this.selectedBrigeProfileColumn.Visible = true;
			this.selectedBrigeProfileColumn.VisibleIndex = 4;
			this.selectedBrigeProfileColumn.Width = 102;
			// 
			// callerColumn
			// 
			this.callerColumn.AppearanceCell.Options.UseTextOptions = true;
			this.callerColumn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.callerColumn.Caption = "Caller/Callee";
			this.callerColumn.FieldName = "Caller";
			this.callerColumn.Name = "callerColumn";
			this.callerColumn.OptionsColumn.AllowMove = false;
			this.callerColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
			this.callerColumn.OptionsColumn.AllowSort = false;
			this.callerColumn.OptionsColumn.ShowInCustomizationForm = false;
			this.callerColumn.Visible = true;
			this.callerColumn.VisibleIndex = 5;
			this.callerColumn.Width = 85;
			// 
			// useDefaultColumn
			// 
			this.useDefaultColumn.Caption = "Use Default";
			this.useDefaultColumn.ColumnEdit = this.useDefaultRepositoryItem;
			this.useDefaultColumn.FieldName = "UseDefault";
			this.useDefaultColumn.Name = "useDefaultColumn";
			this.useDefaultColumn.OptionsColumn.AllowMove = false;
			this.useDefaultColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
			this.useDefaultColumn.OptionsColumn.AllowSort = false;
			this.useDefaultColumn.OptionsColumn.ShowInCustomizationForm = false;
			this.useDefaultColumn.Visible = true;
			this.useDefaultColumn.VisibleIndex = 6;
			this.useDefaultColumn.Width = 78;
			// 
			// useDefaultRepositoryItem
			// 
			this.useDefaultRepositoryItem.AutoHeight = false;
			this.useDefaultRepositoryItem.Name = "useDefaultRepositoryItem";
			this.useDefaultRepositoryItem.CheckedChanged += new System.EventHandler(this.ColumnEdit_EditValueChanged);
			// 
			// bridgeNameColumn
			// 
			this.bridgeNameColumn.Caption = "Bridge Name";
			this.bridgeNameColumn.ColumnEdit = this.repositoryBridgeComboBox;
			this.bridgeNameColumn.FieldName = "BridgeName";
			this.bridgeNameColumn.Name = "bridgeNameColumn";
			this.bridgeNameColumn.OptionsColumn.AllowMove = false;
			this.bridgeNameColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
			this.bridgeNameColumn.OptionsColumn.AllowSort = false;
			this.bridgeNameColumn.OptionsColumn.ShowInCustomizationForm = false;
			this.bridgeNameColumn.Visible = true;
			this.bridgeNameColumn.VisibleIndex = 3;
			this.bridgeNameColumn.Width = 86;
			// 
			// repositoryBridgeComboBox
			// 
			this.repositoryBridgeComboBox.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.repositoryBridgeComboBox.AutoHeight = false;
			this.repositoryBridgeComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.repositoryBridgeComboBox.Name = "repositoryBridgeComboBox";
			this.repositoryBridgeComboBox.NullText = "<Select bridge>";
			this.repositoryBridgeComboBox.NullValuePrompt = "<Select bridge>";
			this.repositoryBridgeComboBox.NullValuePromptShowForEmptyValue = true;
			this.repositoryBridgeComboBox.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
			// 
			// repositoryBridgeProfileNameComboBox
			// 
			this.repositoryBridgeProfileNameComboBox.AutoHeight = false;
			this.repositoryBridgeProfileNameComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.repositoryBridgeProfileNameComboBox.Name = "repositoryBridgeProfileNameComboBox";
			this.repositoryBridgeProfileNameComboBox.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
			// 
			// RoomAudioVideoSettingsListControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.endpointsTreeList);
			this.Name = "RoomAudioVideoSettingsListControl";
			this.Size = new System.Drawing.Size(493, 203);
			this.Load += new System.EventHandler(this.RoomAudioVideoSettingsListControl_Load);
			((System.ComponentModel.ISupportInitialize)(this.bindingSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.endpointsTreeList)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.endpointRepositoryItem)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.useDefaultRepositoryItem)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.repositoryBridgeComboBox)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.repositoryBridgeProfileNameComboBox)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource bindingSource;
        private DevExpress.XtraTreeList.TreeList endpointsTreeList;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit useDefaultRepositoryItem;
        protected DevExpress.XtraTreeList.Columns.TreeListColumn roomNameColumn;
        protected DevExpress.XtraTreeList.Columns.TreeListColumn endpointColumn;
        protected DevExpress.XtraTreeList.Columns.TreeListColumn endpointProfileColumn;
        protected DevExpress.XtraTreeList.Columns.TreeListColumn callerColumn;

        protected DevExpress.XtraTreeList.Columns.TreeListColumn useDefaultColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit endpointRepositoryItem;
		protected DevExpress.XtraTreeList.Columns.TreeListColumn bridgeNameColumn;
		private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryBridgeComboBox;
		protected DevExpress.XtraTreeList.Columns.TreeListColumn selectedBrigeProfileColumn;
		private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryBridgeProfileNameComboBox;
    }
}