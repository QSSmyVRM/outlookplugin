﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace MyVrm.WebServices.Data
{
    public class LdapSettings : ComplexProperty
    {
        private LdapSyncSchedule _syncSchedule;

        public string Server { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public int Port { get; set; }
        public int Timeout { get; set; }
        public LdapSyncSchedule SyncSchedule 
        { 
            get { return _syncSchedule ?? (_syncSchedule = new LdapSyncSchedule()); }
        }
        public string LoginKey { get; set; }
        public TimeSpan SyncTime { get; set; }
        public string SearchFilter { get; set; }
        public string Prefix { get; set; }

		public override object Clone()
		{
			LdapSettings copy = new LdapSettings();
			copy.Login = string.Copy(Login);
			copy.LoginKey = string.Copy(LoginKey);
			copy.Password = string.Copy(Password);
			copy.Port = Port;
			copy.Prefix = string.Copy(Prefix);
			copy.SearchFilter = string.Copy(SearchFilter);
			copy.Server = string.Copy(Server);
			copy._syncSchedule = (LdapSyncSchedule)_syncSchedule.Clone();
			copy.SyncTime = new TimeSpan(SyncTime.Ticks);
			copy.Timeout = Timeout;

			return copy;
		}
        internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
        {
            switch(reader.LocalName)
            {
                case "serverAddress":
                {
                    Server = reader.ReadElementValue();
                    return true;
                }
                case "loginName":
                {
                    Login = reader.ReadElementValue();
                    return true;
                }
                case "loginPassword":
                {
                    Password = reader.ReadElementValue();
                    return true;
                }
                case "portNo":
                {
                    Port = reader.ReadElementValue<int>();
                    return true;
                }
                case "connectionTimeout":
                {
                    Timeout = reader.ReadElementValue<int>();
                    return true;
                }
                case "scheduler":
                {
                    SyncSchedule.LoadFromXml(reader, reader.LocalName);
                    return true;
                }
                case "loginkey":
                {
                    LoginKey = reader.ReadElementValue();
                    return true;
                }
                case "synctime":
                {
                    SyncTime = Utilities.TimeOfDayStringToTimeSpan(reader.ReadElementValue());
                    return true;
                }
                case "searchfilter":
                {
                    SearchFilter = reader.ReadElementValue();
                    return true;
                }
                case "LDAPPrefix":
                {
                    Prefix = reader.ReadElementValue();
                    return true;
                }
            }
            return false;
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            writer.WriteElementValue(XmlNamespace.NotSpecified, "serverAddress", Server);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "loginName", Login);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "loginPassword", Password);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "portNo", Port);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "connectionTimeout", Timeout);
            SyncSchedule.WriteToXml(writer, "scheduler");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "loginkey", LoginKey);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "synctime", Utilities.TimeSpanToTimeOfDayString(SyncTime));
            writer.WriteElementValue(XmlNamespace.NotSpecified, "searchfilter", SearchFilter);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "LDAPPrefix", Prefix);
        }
    }
}
