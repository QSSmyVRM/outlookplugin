﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
    internal class ByteArrayPropertyDefinition : TypedPropertyDefinition
    {
        public ByteArrayPropertyDefinition(string xmlElementName) : base(xmlElementName)
        {
            PropertyType = typeof (byte[]);
        }

        public ByteArrayPropertyDefinition(string xmlElementName, PropertyDefinitionFlags flags) : base(xmlElementName, flags)
        {
            PropertyType = typeof(byte[]);
        }

        #region Overrides of TypedPropertyDefinition

        internal override object Parse(string value)
        {
            return Convert.FromBase64String(value);
        }

        internal override string ToString(object value)
        {
            return Convert.ToBase64String((byte[]) value);
        }
        #endregion
    }
}
