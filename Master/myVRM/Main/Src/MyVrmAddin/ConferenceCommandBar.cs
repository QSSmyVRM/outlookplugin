﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Outlook;
using MyVrm.Outlook;
using MyVrm.Outlook.WinForms;
using MyVrm.Outlook.WinForms.Calendar;
using MyVrm.Outlook.WinForms.Conference;
using MyVrm.Outlook.WinForms.Templates;
using MyVrm.WebServices.Data;
using MyVrmAddin2003.Properties;
using AxHost = MyVrm.Outlook.WinForms.AxHost;
using Exception = System.Exception;
using Image = System.Drawing.Image;

namespace MyVrmAddin2003
{
    public class ConferenceCommandBar : OfficeCommandBar
    {
        private readonly AppointmentInspectorWrapper _appointmentInspectorWrapper;
        private CommandBarButton _enableConferenceButton;
        private CommandBarButton _showRoomsCalendarButton;
        private CommandBarPopup _templatesButton;
        private CommandBarButton _saveAsTemplateButton;
        private CommandBarButton _refreshTemplatesButton;
        private readonly List<CommandBarButton> _templateButtons = new List<CommandBarButton>();

        private CommandBarButton _favoriteRoomsButton;
        private string selectedTemplateID;

        public ConferenceCommandBar(AppointmentInspectorWrapper appointmentInspectorWrapper)
        {
            _appointmentInspectorWrapper = appointmentInspectorWrapper;
			Name = MyVrmAddin.ProductDisplayName;//Strings.CommandBarName;
        }

        public bool IsEnableConferenceEnabled
        {
            get { return _enableConferenceButton.Enabled; }
            set { _enableConferenceButton.Enabled = value; }
        }

        protected override void Initialize()
        {
            base.Initialize();
            // Enable Conference button
            _enableConferenceButton = (CommandBarButton)CommandBar.Controls.Add(MsoControlType.msoControlButton, Type.Missing, Type.Missing, 1, true);
            _enableConferenceButton.Caption = Strings.EnableConferenceButtonCaption;
            _enableConferenceButton.Style = MsoButtonStyle.msoButtonIconAndCaption;
			_enableConferenceButton.Picture = AxHost.GetIPictureDispFromPicture((Image) MyVrmAddin.Logo16);//Resources.myVRMConference);
        	_enableConferenceButton.Mask = AxHost.GetMaskImage((Image) MyVrmAddin.Logo16);//Resources.myVRMConference);
            _enableConferenceButton.Click += EnableConferenceButtonClick;
            // Show Rooms Calendar button
            _showRoomsCalendarButton = (CommandBarButton)CommandBar.Controls.Add(MsoControlType.msoControlButton, Type.Missing, Type.Missing, 2, true);
            _showRoomsCalendarButton.Caption = Strings.ShowRoomsCalendarButtonCaption;
            _showRoomsCalendarButton.Style = MsoButtonStyle.msoButtonIconAndCaption;
            _showRoomsCalendarButton.Picture = AxHost.GetIPictureDispFromPicture(Resources.RoomsCalendar);
            _showRoomsCalendarButton.Mask = AxHost.GetMaskImage(Resources.RoomsCalendar);
            _showRoomsCalendarButton.Click += ShowRoomsCalendarButtonClick;
            // Create from template button
            _templatesButton  = (CommandBarPopup)CommandBar.Controls.Add(MsoControlType.msoControlPopup, Type.Missing, Type.Missing, 3, true);
            _templatesButton.BeginGroup = true;
            _templatesButton.Caption = Strings.CreateFromTemplateButtonCaption;
            _refreshTemplatesButton = (CommandBarButton)_templatesButton.Controls.Add(MsoControlType.msoControlButton, Type.Missing, Type.Missing, 1, true);
            _refreshTemplatesButton.Caption = Strings.RefreshTemplateListMenuItemCaption;
            _refreshTemplatesButton.Style = MsoButtonStyle.msoButtonCaption;
            _refreshTemplatesButton.Click += RefreshTemplatesButtonClick;
            // Save As Template button
            _saveAsTemplateButton = (CommandBarButton)CommandBar.Controls.Add(MsoControlType.msoControlButton, Type.Missing, Type.Missing, 4, true);
            _saveAsTemplateButton.Caption = Strings.SaveAsTemplateButtonCaption;
            _saveAsTemplateButton.Style = MsoButtonStyle.msoButtonIconAndCaption;
            _saveAsTemplateButton.Picture = AxHost.GetIPictureDispFromPicture(Resources.SaveAsTemplate);
            _saveAsTemplateButton.Mask = AxHost.GetMaskImage(Resources.SaveAsTemplate);
            _saveAsTemplateButton.Click += SaveAsTemplateButtonClick;
            //Favorite rooms button
            _favoriteRoomsButton = (CommandBarButton)CommandBar.Controls.Add(MsoControlType.msoControlButton, Type.Missing, Type.Missing, 5, true);
            _favoriteRoomsButton.BeginGroup = true;
            _favoriteRoomsButton.Caption = Strings.FavoriteRoomsButtonCaption;
            _favoriteRoomsButton.Style = MsoButtonStyle.msoButtonIconAndCaption;
            _favoriteRoomsButton.Picture = AxHost.GetIPictureDispFromPicture(Resources.FavoriteRooms);
            _favoriteRoomsButton.Mask = AxHost.GetMaskImage(Resources.FavoriteRooms);
            _favoriteRoomsButton.Click += FavoriteRoomsButtonClick;
            CommandBar.Visible = true;

            selectedTemplateID = string.Empty;
            DisEnable();
        }

        protected override void OnClose()
        {
            _enableConferenceButton.Click -= EnableConferenceButtonClick;
            _enableConferenceButton.Delete(true);
            Marshal.ReleaseComObject(_enableConferenceButton);
            _enableConferenceButton = null;

            _showRoomsCalendarButton.Click -= ShowRoomsCalendarButtonClick;
            _showRoomsCalendarButton.Delete(true);
            Marshal.ReleaseComObject(_showRoomsCalendarButton);
            _showRoomsCalendarButton = null;

            _refreshTemplatesButton.Click -= RefreshTemplatesButtonClick;
            _refreshTemplatesButton.Delete(true);
            Marshal.ReleaseComObject(_refreshTemplatesButton);
            _refreshTemplatesButton = null;

            ClearTemplateButtonList();

            _templatesButton.Delete(true);
            Marshal.ReleaseComObject(_templatesButton);
            _templatesButton = null;

            _saveAsTemplateButton.Click -= SaveAsTemplateButtonClick;
            _saveAsTemplateButton.Delete(true);
            Marshal.ReleaseComObject(_saveAsTemplateButton);
            _saveAsTemplateButton = null;

            _favoriteRoomsButton.Click -= FavoriteRoomsButtonClick;
            _favoriteRoomsButton.Delete(true);
            Marshal.ReleaseComObject(_favoriteRoomsButton);
            _favoriteRoomsButton = null;

            base.OnClose();
        }

        private void ClearTemplateButtonList()
        {
            foreach (var templateButton in _templateButtons)
            {
                templateButton.Click -= CreateFromTemplateClick;
                templateButton.Delete(true);
                Marshal.ReleaseComObject(templateButton);
            }
            _templateButtons.Clear();
        }

        void RefreshTemplatesButtonClick(CommandBarButton Ctrl, ref bool CancelDefault)
        {
            // clear template list

            GetTemplateListResponse ret = null;

            var cursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                ret = MyVrmService.Service.GetTemplateList(GetTemplateListResponse.SortByType.ByName);
            }
            catch (Exception exception)
            {
                MyVrmAddin.TraceSource.TraceException(exception, true);
                UIHelper.ShowMessage(exception.Message, MessageBoxButtons.OK,
                                         MessageBoxIcon.Exclamation);
            }
            finally
            {
                Cursor.Current = cursor;
            }

            if (ret != null && ret.templateList != null)
            {
                ClearTemplateButtonList();

                // Add menu items next to Refresh list item
                var firstPos = _refreshTemplatesButton.Index + 1;
                var pos = firstPos;
                foreach (var templateInfo in ret.templateList)
                {
                    var rt =
                        (CommandBarButton)
                        _templatesButton.Controls.Add(MsoControlType.msoControlButton, Type.Missing, Type.Missing, pos,
                                                      true);
                    rt.Caption = templateInfo.Name;
                    rt.Visible = true;
                    rt.Tag = templateInfo.ID;
                    rt.Enabled = true;
                    rt.State = (selectedTemplateID == templateInfo.ID) ? MsoButtonState.msoButtonDown : MsoButtonState.msoButtonUp ;
                    rt.BeginGroup = pos == firstPos;
                    rt.Click += CreateFromTemplateClick;

                    _templateButtons.Add(rt);
                    pos++;
                }
            }
        }

        void CreateFromTemplateClick(CommandBarButton ctrl, ref bool cancelDefault)
        {
            // Clear checked state for buttons
            foreach (var templateButton in _templateButtons)
            {
                templateButton.State = MsoButtonState.msoButtonUp;
            }
            // Set checked state for clicked button
            ctrl.State = MsoButtonState.msoButtonDown;
            selectedTemplateID = string.Empty;
            if (ctrl.State == MsoButtonState.msoButtonDown)
                selectedTemplateID = ctrl.Tag;


            if (selectedTemplateID.Trim().Length == 0 || int.Parse(selectedTemplateID) <= 0)
                return;


            GetTemplateResponse response = null;
            var cursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                response = MyVrmService.Service.GetTemplate(int.Parse(selectedTemplateID));
            }
            catch (Exception exception)
            {
                MyVrmAddin.TraceSource.TraceException(exception, true);
                UIHelper.ShowMessage(exception.Message, MessageBoxButtons.OK,
                                         MessageBoxIcon.Exclamation);
            }
            finally
            {
                Cursor.Current = cursor;
            }

            if (response != null)
            {
                OutlookAppointmentImpl apptImpl;
                var conferenceWrapper = Globals.ThisAddIn.GetConferenceWrapperForAppointment(
                    _appointmentInspectorWrapper.Item, out apptImpl);

                if (conferenceWrapper != null)
                {
                    conferenceWrapper.AfterSave -= DisEnable_AfterSave;
                    conferenceWrapper.FillFromTemplate(response.Conference);
                    conferenceWrapper.AfterSave += DisEnable_AfterSave;
                }
            }
        }

        private void DisEnable()
        {
            _templatesButton.Enabled = !Globals.ThisAddIn.HasConferenceWrapper(_appointmentInspectorWrapper.Item);
            _saveAsTemplateButton.Enabled = !_templatesButton.Enabled;
        }

        public void DisEnable_AfterSave(object sender, EventArgs e)
        {
            DisEnable();

            OutlookAppointmentImpl apptImpl;
            var conferenceWrapper = Globals.ThisAddIn.GetConferenceWrapperForAppointment(
                    _appointmentInspectorWrapper.Item, out apptImpl);
            if (conferenceWrapper != null)
                conferenceWrapper.AfterSave -= DisEnable_AfterSave;
        }

        public static void ShowRoomsCalendarButtonClick(CommandBarButton ctrl, ref bool cancelDefault)
        {
            try
            {
                using (var roomsCalendar = new RoomsCalendarDialog())
                {
                    roomsCalendar.ShowDialog();
                }
            }
            catch (Exception exception)
            {
                MyVrmAddin.TraceSource.TraceException(exception);
                UIHelper.ShowError(exception.Message);
                cancelDefault = true;
            }
        }

        void EnableConferenceButtonClick(CommandBarButton ctrl, ref bool cancelDefault)
        {
            try
            {
                Globals.ThisAddIn.ShowMyVRMForm(_appointmentInspectorWrapper);
                IsEnableConferenceEnabled = false;
            }
            catch (Exception exception)
            {
                MyVrmAddin.TraceSource.TraceException(exception);
                UIHelper.ShowError(exception.Message);
                cancelDefault = true;
            }
        }

        void SaveAsTemplateButtonClick(CommandBarButton ctrl, ref bool cancelDefault)
        {
            try
            {
                OutlookAppointmentImpl apptImpl;
                var conferenceWrapper = Globals.ThisAddIn.GetConferenceWrapperForAppointment(_appointmentInspectorWrapper.Item, out apptImpl);
                using (var editTemplateDialog = new SaveAsConferenceTemplateDialog(conferenceWrapper))
                {
                    editTemplateDialog.ShowDialog();
                }
            }
            catch (Exception exception)
            {
                MyVrmAddin.TraceSource.TraceException(exception);
                UIHelper.ShowError(exception.Message);
                cancelDefault = true;
            }
        }

        //private 
		public static void FavoriteRoomsButtonClick(CommandBarButton ctrl, ref bool canceldefault)
        {
            var cursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                //Read current favorites
                var response = MyVrmService.Service.GetPreferedRoom();

                //Run selection dlg
                using (var dlg = new FavoriteRoomSelectionDialog())
                {
                    if (response != null)
                        dlg.SelectedRooms = response.RoomIds.ToArray();

                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        //Store setup favorites
                        MyVrmService.Service.SetPreferedRoom(dlg.SelectedRooms);
                    }
                }
            }
            catch (Exception exception)
            {
                MyVrmAddin.TraceSource.TraceException(exception, true);
                UIHelper.ShowMessage(exception.Message, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                Cursor.Current = cursor;
            }
        }
    }
}
