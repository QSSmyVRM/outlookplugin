﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Threading;

namespace MyVrm.Common.Threading
{
    public sealed class WorkItem
    {
        private WaitCallback _callback;
        private object _state;
        private ExecutionContext _ctx;

        internal WorkItem(WaitCallback wc, object state, ExecutionContext ctx)
        {
            _callback = wc; _state = state; _ctx = ctx;
        }

        internal WaitCallback Callback { get { return _callback; } }
        internal object State { get { return _state; } }
        internal ExecutionContext Context { get { return _ctx; } }
    }

    public enum WorkItemStatus { Completed, Queued, Executing, Aborted }


    public class AdvancedThreadPool : IDisposable
    {
        private int _remainingWorkItems = 1;
        private ManualResetEvent _done = new ManualResetEvent(false);
        private LinkedList<WorkItem> _callbacks = new LinkedList<WorkItem>();
        private Dictionary<WorkItem, Thread> _threads = new Dictionary<WorkItem, Thread>();


        public bool HasWorkingThreads
        {
            get { return _threads.Count > 0; }
        }

        public WorkItem QueueUserWorkItem(WaitCallback callback)
        {
            return QueueUserWorkItem(callback, null);
        }

        public WorkItem QueueUserWorkItem(WaitCallback callback, object state)
        {
            if (callback == null) 
                throw new ArgumentNullException("callback");
            ThrowIfDisposed();
            var item = new WorkItem(callback, state, ExecutionContext.Capture());
            lock (_callbacks) _callbacks.AddLast(item);
            lock (_done) _remainingWorkItems++;
            ThreadPool.QueueUserWorkItem(HandleItem);
            return item;
        }

        public bool WaitOne() { return WaitOne(-1, false); }

        public bool WaitOne(TimeSpan timeout, bool exitContext)
        {
            return WaitOne((int)timeout.TotalMilliseconds, exitContext);
        }

        public bool WaitOne(int millisecondsTimeout, bool exitContext)
        {
            ThrowIfDisposed();
            DoneWorkItem();
            bool rv = _done.WaitOne(millisecondsTimeout, exitContext);
            lock (_done)
            {
                if (rv)
                {
                    _remainingWorkItems = 1;
                    _done.Reset();
                }
                else _remainingWorkItems++;
            }
            return rv;
        }

        public WorkItemStatus Cancel(WorkItem item, bool allowAbort)
        {
            if (item == null) throw new ArgumentNullException("item");
            lock (_callbacks)
            {
                LinkedListNode<WorkItem> node = _callbacks.Find(item);
                if (node != null)
                {
                    _callbacks.Remove(node);
                    return WorkItemStatus.Queued;
                }
                if (_threads.ContainsKey(item))
                {
                    if (allowAbort)
                    {
                        _threads[item].Abort();
                        _threads.Remove(item);
                        return WorkItemStatus.Aborted;
                    }
                    return WorkItemStatus.Executing;
                }
                return WorkItemStatus.Completed;
            }
        }

        private void HandleItem(object ignored)
        {
            WorkItem item = null;
            try
            {
                lock (_callbacks)
                {
                    if (_callbacks.Count > 0)
                    {
                        item = _callbacks.First.Value;
                        _callbacks.RemoveFirst();
                    }
                    if (item == null) return;
                    _threads.Add(item, Thread.CurrentThread);

                } ExecutionContext.Run(item.Context,
                    delegate { item.Callback(item.State); }, null);
            }
            finally
            {
                lock (_callbacks)
                {
                    if (item != null) _threads.Remove(item);
                    DoneWorkItem();
                }
            }
        }


        //private void HandleWorkItem(object state)
        //{
        //    var qc = (QueuedCallback)state;
        //    try
        //    {
        //        qc.Callback(qc.State);
        //    }
        //    finally
        //    {
        //        DoneWorkItem();
        //    }
        //}

        private void DoneWorkItem()
        {
            lock (_done)
            {
                --_remainingWorkItems;
                if (_remainingWorkItems == 0) _done.Set();
            }
        }

        //private class QueuedCallback
        //{
        //    public WaitCallback Callback;
        //    public object State;
        //}

        private void ThrowIfDisposed()
        {
            if (_done == null) throw new ObjectDisposedException(GetType().Name);
        }

        public void Dispose()
        {
            if (_done != null)
            {
                ((IDisposable)_done).Dispose();
                _done = null;
            }
        }
    }

}