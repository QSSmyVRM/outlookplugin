﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System.Collections.Generic;

namespace MyVrm.WebServices.Data
{
	//Request
	internal class GetTemplateListRequest : ServiceRequestBase<GetTemplateListResponse>
	{
		internal string Name;
		internal string Descr;
		internal bool IsPublic;
		internal bool IsDefault;
		public GetTemplateListResponse.SortByType SortBy;

		internal GetTemplateListRequest(MyVrmService service)
			: base(service)
		{
		}

		#region Overrides of ServiceRequestBase<GetTemplateListResponse>

		internal override string GetXmlElementName()
		{
			return "login";
		}

		internal override string GetCommandName()
		{
			return Constants.GetTemplateListCommandName;
		}

		internal override string GetResponseXmlElementName()
		{
			return "templates"; 
		}

		internal override GetTemplateListResponse ParseResponse(MyVrmServiceXmlReader reader)
		{
			var response = new GetTemplateListResponse();
			response.ReadElementsFromXml(reader);
			return response;
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			if (UserId != null)
				UserId.WriteToXml(writer, "userID");
			writer.WriteElementValue(XmlNamespace.NotSpecified, "sortBy", SortBy);
		}

		#endregion
	}

	//Response
	public class GetTemplateListResponse : ServiceResponse
	{
		public enum SortByType
		{
			ByName = 1,
			ByOwner = 2,
			ByPublic = 3,
			ById = 4
		}

		public List<TemplateInfo> templateList = new List<TemplateInfo>();
		//Tags
		private const string Template = "template";
		private const string Templates = "templates";

		//Read template info and template conference instance 
		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{
		    do
		    {
		        //Forward till "template" element 
		        while (!reader.IsEndElement(XmlNamespace.NotSpecified, Template))
		        {
					TemplateInfo templateInfo = null ;
		            while (!reader.IsStartElement(XmlNamespace.NotSpecified, Template) //new entry or
							&& !reader.IsEndElement(XmlNamespace.NotSpecified, Templates)) //empty list
		                reader.Read();
					if (reader.IsEndElement(XmlNamespace.NotSpecified, Templates))
						break;
		            if (reader.LocalName == Template)
		            {
		                templateInfo = new TemplateInfo();
		            }

		            while (!reader.IsEndElement(XmlNamespace.NotSpecified, Template))
		            {
						if (templateInfo != null)
			                templateInfo.TryReadElementFromXml(reader);
		            }
					if (templateInfo != null)
						templateList.Add(templateInfo);
		        }
				if (!reader.IsEndElement(XmlNamespace.NotSpecified, Templates))
					reader.Read();

			} while (!reader.IsEndElement(XmlNamespace.NotSpecified, Templates)); //read till the "templates" node end 
		}
	}
}
