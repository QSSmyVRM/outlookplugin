﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	public class ConferenceToApprove : ComplexProperty
	{
		private string _confID;
		public string ConfID
		{
			set { _confID = value; }
			get { return _confID; }
		}

		private string _instanceType;
		public string InstanceType 
		{ 
			set { _instanceType = value; }
			get { return _instanceType; } 
		}

		private List<ApprovedEntry> _partyInfoList;
		public List<ApprovedEntry> PartyInfoList
		{
			set { _partyInfoList = value; }
			get { return _partyInfoList; }
		}

		public override object Clone()
		{
			ConferenceToApprove copy = new ConferenceToApprove();
			copy._confID = string.Copy(_confID);
			copy._instanceType = string.Copy(_instanceType);
			copy._partyInfoList = new List<ApprovedEntry>(_partyInfoList);

			return copy;
		}

		public ConferenceToApprove()
		{
			_partyInfoList = new List<ApprovedEntry>();
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			writer.WriteStartElement(XmlNamespace.NotSpecified, "conference");
			writer.WriteElementValue(XmlNamespace.NotSpecified, "confID", _confID);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "instanceType", _instanceType);
			foreach (ApprovedEntry partyInfo in _partyInfoList)
			{
				partyInfo.WriteElementsToXml(writer);
			}
			writer.WriteEndElement();
		}
	}
}
