/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace MyVrm.Common
{
    /// <summary>
    /// Represents enumeration member with localized description
    /// </summary>
    public class LocalizedEnum : IComparable, ICloneable
    {
        private readonly Enum _value;
        private readonly string _text;
        /// <summary>
        /// Initialize a new instance of <see cref="LocalizedEnum"/>.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="value"></param>
        public LocalizedEnum(string text, Enum value)
        {
            _value = value;
            _text = text;
        }
        /// <summary>
        /// Gets enumeration member
        /// </summary>
        public Enum Value
        {
            get { return _value; }
        }
        /// <summary>
        /// Gets localized description of enumeration member.
        /// </summary>
        public string Text
        {
            get { return _text; }
        }

        #region IComparable Members

        ///<summary>
        ///Compares the current instance with another object of the same type.
        ///</summary>
        ///
        ///<returns>
        ///A 32-bit signed integer that indicates the relative order of the objects being compared. The return value has these meanings: Value Meaning Less than zero This instance is less than obj. Zero This instance is equal to obj. Greater than zero This instance is greater than obj. 
        ///</returns>
        ///
        ///<param name="obj">An object to compare with this instance. </param>
        ///<exception cref="T:System.ArgumentException">obj is not the same type as this instance. </exception><filterpriority>2</filterpriority>
        public int CompareTo(object obj)
        {
            if (obj == null)
            {
                return 1;
            }
            var enum2 = obj as LocalizedEnum;
            if (enum2 == null)
            {
                throw new ArgumentException();
            }
            return Text.CompareTo(enum2.Text);
        }

        #endregion

        /// <summary>
        /// Returns a value indicating whether this instance is equal to a specified object.
        /// </summary>
        /// <param name="obj">The object to compare with this instance.</param>
        /// <returns><b>true</b> if o is a <see cref="LocalizedEnum"/> that has the same value as this instance; otherwise, <b>false.</b></returns>
        public override bool Equals(object obj)
        {
            return obj != null && obj is LocalizedEnum && Value.Equals((obj as LocalizedEnum).Value);
        }
        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>The hash code for this instance.</returns>
        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
        /// <summary>
        /// Returns a <see cref="string"/> representation of the value of this instance in registry format.
        /// </summary>
        /// <returns>Localized description of enumeration member.</returns>
        public override string ToString()
        {
            return Text;
        }

        #region ICloneable Members
        /// <summary>
        /// Creates a new <see cref="LocalizedEnum"/> that is a copy of the current instance.
        /// </summary>
        /// <returns>A new <see cref="LocalizedEnum"/> that is a copy of this instance.</returns>
        public object Clone()
        {
            return new LocalizedEnum(Text, Value);
        }

        #endregion
    }
}