﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;

namespace MyVrm.WebServices.Data
{
    public class ConfigurationSettings
    {
        private readonly EmailSystemSettings _emailSystem = new EmailSystemSettings();
        private readonly LdapSettings _ldap = new LdapSettings();
    	private int _systemTimeZoneID;

        public EmailSystemSettings EmailSystem
        {
            get
            {
                return _emailSystem;
            }
        }

        public LdapSettings Ldap
        {
            get
            {
                return _ldap;
            }
        }

    	public int SystemTimeZoneID
    	{
			get { return _systemTimeZoneID; }
    	}

        internal void WriteToXml(MyVrmXmlWriter writer, string xmlElementName)
        {
            if (writer == null) 
                throw new ArgumentNullException("writer");
            if (xmlElementName == null) 
                throw new ArgumentNullException("xmlElementName");
            writer.WriteStartElement(XmlNamespace.NotSpecified, xmlElementName);
            EmailSystem.WriteToXml(writer, "emailSystem");
            Ldap.WriteToXml(writer, "LDAP");
            writer.WriteEndElement();
        }

        internal void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
        {
            do
            {
                reader.Read();
				if (reader.LocalName == "SystemTimeZoneID")
				{
					_systemTimeZoneID = reader.ReadElementValue<int>();
//					TimeZoneInfo ti = TimeZoneConvertion.ConvertToTimeZoneInfo(_systemTimeZoneID);
				}
				else
				{
					if (reader.LocalName == "emailSystem")
					{
						EmailSystem.LoadFromXml(reader, "emailSystem");
					}
					else if (reader.LocalName == "LDAP")
					{
						Ldap.LoadFromXml(reader, "LDAP");
					}
					else
					{
						reader.SkipCurrentElement();
					}
				}
                
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, xmlElementName));
        }
    }
}
