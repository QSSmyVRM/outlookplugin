/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using DevExpress.XtraEditors;

namespace MyVrm.Outlook.WinForms.Conference
{
    public partial class UserAudioVideoSettingsControl : XtraUserControl
    {
        public UserAudioVideoSettingsControl()
        {
            InitializeComponent();
			layoutControlItem3.CustomizationFormText = Strings.SelectCallerCalleeLableText;
			layoutControlItem3.Text = Strings.SelectCallerCalleeLableText;
			layoutControlItem6.Text = Strings.ConnectionTypeLableText;
			layoutControlItem7.Text = Strings.AddressLableText;
			layoutControlItem8.Text = Strings.URLLableText;
			layoutControlItem9.Text = Strings.ProtocolLableText;
			layoutControlItem10.Text = Strings.EquipmentLableText;
			layoutControlItem4.Text = Strings.LineRateLableText;
			layoutControlItem11.Text = Strings.AddressTypeLableText;
			layoutControlItem12.Text = Strings.ConnectionLableText;

			layoutControlItem6.CustomizationFormText = Strings.ConnectionTypeLableText;
			layoutControlItem7.CustomizationFormText = Strings.AddressLableText;
			layoutControlItem8.CustomizationFormText = Strings.URLLableText;
			layoutControlItem9.CustomizationFormText = Strings.ProtocolLableText;
			layoutControlItem10.CustomizationFormText = Strings.EquipmentLableText;
			layoutControlItem4.CustomizationFormText = Strings.LineRateLableText;
			layoutControlItem11.CustomizationFormText = Strings.AddressTypeLableText;
			layoutControlItem12.CustomizationFormText = Strings.ConnectionLableText;

			checkEdit1.Properties.Caption = Strings.OutsideNWCaptionText;
			checkEdit3.Properties.Caption = Strings.UseDefault;
        }
    }
}