﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;

namespace MyVrm.WebServices.Data
{
    internal class SetConfigurationSettingsRequest : ServiceRequestBase<SetConfigurationSettingsResponse>
    {
        private ConfigurationSettings _settings;

        public SetConfigurationSettingsRequest(MyVrmService service) : base(service)
        {
        }

        public ConfigurationSettings Settings
        {
            get { return _settings; }
            set { _settings = value; }
        }

        #region Overrides of ServiceRequestBase

        internal override string GetXmlElementName()
        {
            return "superAdmin";
        }

        internal override string GetCommandName()
        {
            return "SetSuperAdmin";
        }

        internal override string GetResponseXmlElementName()
        {
            return "preference";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            UserId.WriteToXml(writer);
            _settings.WriteToXml(writer, "preference");
        }

        #endregion
    }
}
