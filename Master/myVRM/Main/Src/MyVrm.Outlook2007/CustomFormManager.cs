﻿using System;
using System.Windows.Forms;
using Microsoft.Office.Interop.Outlook;
using Microsoft.Office.Tools;
using Microsoft.Vbe.Interop.Forms;
using Exception = System.Exception;
using Pages = Microsoft.Office.Interop.Outlook.Pages;

namespace MyVrm.Outlook
{
    public class CustomFormManager
    {
        internal class CustomFormManagerEventSink
        {
            private CustomForm _form;
            private Inspector _inspector;
            public CustomFormManagerEventSink(CustomForm form, Inspector inspector)
            {
                _form = form;
                _inspector = inspector;
                ((InspectorEvents_10_Event)_inspector).Close += OnClose;
            }

            public event EventHandler Close;
            private void OnClose()
            {
                ((InspectorEvents_10_Event)_inspector).Close -= OnClose;
                _inspector = null;
                _form = null;
                if (Close != null)
                {
                    Close(this, EventArgs.Empty);
                }
            }
        }
        private readonly CustomFormKeyedByWindowCollection _customForms = new CustomFormKeyedByWindowCollection();
        private AddIn _addIn;

        public CustomFormManager(AddIn addIn)
        {
            if (addIn == null) 
                throw new ArgumentNullException("addIn");
            _addIn = addIn;
        }

        public void CreateCustomForm(CustomFormControl control, Inspector inspector)
        {
            if (control == null) 
                throw new ArgumentNullException("control");
            if (inspector == null) 
                throw new ArgumentNullException("inspector");
            try
            {
                var pages = inspector.ModifiedFormPages as Pages;
                if (pages != null)
                {
                    var page = pages.Add(control.Text);
                    var userForm = (UserForm) page;
                    var customForm = new CustomForm(userForm, inspector);
                    customForm.AssignControl(control);
                    CustomForms.Add(control, inspector);
                    var eventSink = new CustomFormManagerEventSink(customForm, inspector);
                    eventSink.Close += delegate
                                           {
                                               try
                                               {
                                                   try
                                                   {
                                                       customForm.OnShutdown();
                                                   }
                                                   catch (Exception e)
                                                   {
                                                       ShowError(e, customForm.InternalName);
                                                   }
                                               }
                                               finally
                                               {
                                                   CustomForms.Remove(control);
                                                   customForm.Dispose();
                                               }
                                           };
                    customForm.OnStartup();
                    inspector.ShowFormPage(control.Text);
                }
            }
            catch (Exception e)
            {
                ShowError(e, control.Text);
            }
        }

        public CustomFormKeyedByWindowCollection CustomForms
        {
            get { return _customForms; }
        }

        private static void ShowError(Exception ex, string customFormName)
        {
            using (var dialog = new ThreadExceptionDialog(ex))
            {
                dialog.Text = string.Format(Strings.CustomFormErrorDialogTitle, customFormName);
                dialog.ShowDialog();
            }
        }
    }
}
