﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System.ComponentModel;

namespace MyVrm.Outlook.WinForms
{
    public class Page : BaseControl
    {
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), EditorBrowsable(EditorBrowsableState.Always), Browsable(true)]
        public override sealed string Text
        {
            get { return base.Text; }
            set { base.Text = value; }
        }
        public void Apply(CancelEventArgs e)
        {
            OnApplying(e);
        }

        protected virtual void OnApplying(CancelEventArgs e)
        {
        }
    }
}
