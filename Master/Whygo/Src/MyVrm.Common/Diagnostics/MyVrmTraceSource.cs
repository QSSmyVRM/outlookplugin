﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Diagnostics;
using System.Globalization;
using System.Threading;

namespace MyVrm.Common.Diagnostics
{
    public class MyVrmTraceSource
    {
        private TraceSource _traceSource;

        public MyVrmTraceSource(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }
            Name = name;
        }

        public TraceListenerCollection Listeners
        {
            get { return TraceSource.Listeners; }
        }

        public string Name { get; private set; }
        
        public SourceSwitch Switch 
        { 
            get { return TraceSource.Switch; } 
            set { TraceSource.Switch = value; }
        }


        public void TraceException(Exception exception)
        {
            WriteException(exception, true, "{0}:{1}\n{2}");
        }

        public void TraceException(Exception exception, bool traceInnerMostException)
        {
            WriteException(exception, traceInnerMostException, "{0}:{1}\n{2}");
        }

        public void TraceError(string message)
        {
            TraceSource.TraceEvent(TraceEventType.Error, 0, message);
        }

        public void TraceError(string message, params object[] args)
        {
            TraceSource.TraceEvent(TraceEventType.Error, 0, message, args);
        }

        public void TraceInformation(string message)
        {
            TraceSource.TraceEvent(TraceEventType.Information, 0, message);
        }

        public void TraceInformation(string message, params object[] args)
        {
            TraceSource.TraceEvent(TraceEventType.Information, 0, message, args);
        }

		public void TraceInfoEx(string Text)
		{
			TraceInformation("* PID = {0}, TID = {1}, {2} {3}",
				Process.GetCurrentProcess().Id, Thread.CurrentThread.ManagedThreadId,
				DateTime.Now.ToString("dd.mm.yyyy HH:mm:ss:ffff"), Text);
		}

		public void TraceInfoEx(string Text, params object[] args)
		{
			TraceInfoEx(string.Format(Text, args));
		}

        public void TraceWarning(string message, params object[] args)
        {
            TraceSource.TraceEvent(TraceEventType.Warning, 0, message, args);
        }

        public void TraceWarning(string message)
        {
            TraceSource.TraceEvent(TraceEventType.Warning, 0, message);
        }

        private TraceSource TraceSource
        {
            get { return _traceSource ?? (_traceSource = new TraceSource(Name)); }
        }

        private static Exception GetInnerMostException(Exception exception)
        {
            while (exception.InnerException != null)
            {
                exception = exception.InnerException;
            }
            return exception;
        }

        private void WriteException(Exception exception, bool traceInnerMostException, string format)
        {
            string message = string.Format(CultureInfo.InvariantCulture, format,
                                           new object[] { exception.GetType().ToString(), exception.Message, exception.StackTrace });
            TraceError(message);
            if (traceInnerMostException && exception.InnerException != null)
            {
                var innerMostException = GetInnerMostException(exception);
                WriteException(innerMostException, false, "Inner most {0}:{1}\n{2}");
            }
        }
    }
}
