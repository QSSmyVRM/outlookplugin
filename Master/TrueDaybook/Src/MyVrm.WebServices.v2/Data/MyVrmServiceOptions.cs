﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.WebServices.Data
{
    public class MyVrmServiceOptions
    {
        internal MyVrmServiceOptions()
        {
            AllowNonHostModifyConference = false;
        }
        /// <summary>
        /// Allow/disable conference modification (edit, delete, etc.) even if the current user is not conference host.
        /// </summary>
        public bool AllowNonHostModifyConference { get; set; }
    }
}
