﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System.ComponentModel;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook.WinForms.Conference
{
    public class AudioConferenceBridge : INotifyPropertyChanged
    {
        private readonly ConferenceEndpoint _endpoint;

        //internal 
		public AudioConferenceBridge(ConferenceEndpoint endpoint)
        {
            _endpoint = endpoint;
        }

        public string Name { get; set; }

        public string ConferenceCode
        {
            get
            {
                return Endpoint.ConferenceCode;
            }
            set
            {
                if (Endpoint.ConferenceCode != value)
                {
                    Endpoint.ConferenceCode = value;
                    NotifyPropertyChanged("ConferenceCode");
                }
            }
        }

        public string LeaderPin
        {
            get
            {
                return Endpoint.LeaderPin;
            }
            set
            {
                if (Endpoint.LeaderPin != value)
                {
                    Endpoint.LeaderPin = value;
                    NotifyPropertyChanged("LeaderPin");
                }
            }
        }

        //ZD 104292 start
        public string participantCode{ get; set; }
        //ZD 104292 End
        public override string ToString()
        {
            return Name + (Endpoint.Address != null ? " (" + Endpoint.Address + ")" : "");
        }

        //internal 
		public ConferenceEndpoint Endpoint
        {
            get { return _endpoint; }
        }

        public string Email
        {
            get
            {
                return Endpoint.ConferenceCode;
            }
            set
            {
                if (Endpoint.ConferenceCode != value)
                {
                    Endpoint.ConferenceCode = value;
                    NotifyPropertyChanged("ConferenceCode");
                }
            }
        }

        #region Implementation of INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        private void NotifyPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }
    }
}