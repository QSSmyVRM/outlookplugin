﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	internal class GetVNOCUserListRequest : ServiceRequestBase<GetVNOCUserListResponse>
	{
		public enum CrossSilo
		{
			No = 0,
			Yes = 1
		}

		internal CrossSilo _isCrossSilo;
		public GetVNOCUserListRequest(MyVrmService service)
			: base(service)
		{
			_isCrossSilo = CrossSilo.No;
		}

		#region Overrides of ServiceRequestBase

		internal override string GetXmlElementName()
		{
			return "login";
		}

		internal override string GetCommandName()
		{
			return "GetVNOCUserList";
		}

		internal override string GetResponseXmlElementName()
		{
			return "users";
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			UserId.WriteToXml(writer, "userID");
			writer.WriteElementValue(XmlNamespace.NotSpecified, "isCrossSilo", _isCrossSilo);
		}

		#endregion
	}
	//
	public class GetVNOCUserListResponse : ServiceResponse
	{
		private readonly List<VNOCUser> _VNOCUsers = new List<VNOCUser>();

		public /*ReadOnly*/Collection<VNOCUser> VNOCUsers
		{
			get
			{
				return new /*ReadOnly*/Collection<VNOCUser>(_VNOCUsers);
			}
		}

		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{
			base.ReadElementsFromXml(reader);
			_VNOCUsers.Clear();
			do
			{
				reader.Read();
				if (reader.IsStartElement(XmlNamespace.NotSpecified, "user"))
				{
					var user = new VNOCUser();
					user.LoadFromXml(reader, "user");
					_VNOCUsers.Add(user);
				}
				else
				{
					reader.SkipCurrentElement();
				}
			} while (!reader.IsEndElement(XmlNamespace.NotSpecified, "users"));
		}
	}
	public class VNOCUser : ComplexProperty
	{
		public int UserID { get; private set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string UserEmail { get; set; }
		public string OrganizationName { get; set; }
		public string LoginName { get; set; }
		public bool CheckedState { get; set; }

		public override object Clone()
		{
			VNOCUser copy = new VNOCUser();
			copy.UserID = UserID;
			copy.FirstName = string.Copy(FirstName);
			copy.LastName = string.Copy(LastName);
			copy.UserEmail = string.Copy(UserEmail);
			copy.OrganizationName = string.Copy(OrganizationName);
			copy.LoginName = string.Copy(LoginName);
			copy.CheckedState = CheckedState;
			return copy;
		}

		internal override void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
		{
			do
			{
				reader.Read();
				switch(reader.LocalName)
				{
					case "userID":
						{
							UserID = reader.ReadElementValue<int>(XmlNamespace.NotSpecified, "userID");
							break;
						}
					case "firstName":
						{
							FirstName = reader.ReadElementValue(XmlNamespace.NotSpecified, "firstName");
							break;
						}
					case "lastName":
						{
							LastName = reader.ReadElementValue(XmlNamespace.NotSpecified, "lastName");
							break;
						}
					case "userEmail":
						{
							UserEmail = reader.ReadElementValue(XmlNamespace.NotSpecified, "userEmail");
							break;
						}
					case "orgName":
						{
							OrganizationName = reader.ReadElementValue(XmlNamespace.NotSpecified, "orgName");
							break;
						}
					case "login":
						{
							LoginName = reader.ReadElementValue(XmlNamespace.NotSpecified, "login");
							break;
						}
					default:
						{
							reader.SkipCurrentElement();
							break;
						}
				}
			} while (!reader.IsEndElement(XmlNamespace.NotSpecified, xmlElementName));
		}
	}
}

