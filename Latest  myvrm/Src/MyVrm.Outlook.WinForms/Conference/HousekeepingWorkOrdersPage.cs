﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraTreeList.Nodes;
using MyVrm.Common.ComponentModel;
using MyVrm.WebServices.Data;
using System.Collections.Generic; //ZD 103771

namespace MyVrm.Outlook.WinForms.Conference
{
    public partial class HousekeepingWorkOrdersPage : ConferencePage
    {
        private class WorkOrderRecord
        {
            private Room _room;
            private WorkOrder _workOrder;

            internal WorkOrderRecord(Room room, WorkOrder workOrder)
            {
                _room = room;
                _workOrder = workOrder;
            }
            public RoomId RoomId
            {
                get
                {
                    return _room.Id;
                }
            }
            public string RoomName
            {
                get
                {
                    return _room.Name;
                }
            }
            public WorkOrderId Id
            {
                get
                {
                    return _workOrder != null ? _workOrder.Id : null;
                }
            }
            public string Name
            {
                get
                {
                    return _workOrder != null ? _workOrder.Name : null;
                }
            }
            //ZD 102957 start  //For transaltion
            //public WorkOrderStatus? Status
            //{
            //    get
            //    {
            //        return _workOrder != null ? new WorkOrderStatus?(_workOrder.Status) : null;
            //    }
            //}
            public string Status
            {
                get
                {
                    // return _workOrder != null ? new WorkOrderStatus?(_workOrder.Status) : null;  
                    if (_workOrder != null)
                    {
                        if (_workOrder.Status.ToString() == "Pending")
                            return Strings.Pending;
                        else
                            return Strings.NotAvailableText;
                    }
                    else
                        return null;
                }
            }
            //ZD 102957 End 
            internal WorkOrder WorkOrder
            {
                get
                {
                    return _workOrder;
                }
                set
                {
                    _workOrder = value;
                }
            }

            internal void DetachWorkOrder()
            {
                _workOrder = null;
            }
        }

        private readonly DataList<WorkOrderRecord> _workOrderRecords = new DataList<WorkOrderRecord>();

        public HousekeepingWorkOrdersPage()
        {
            InitializeComponent();
            Text = Strings.HousekeepingWorkOrdersPageText;
            workOrderList.DataSource = _workOrderRecords;
        	bar.Text = Strings.ToolsBarText;
			roomNameColumn.Caption = Strings.RoomLableText;
			workOrderNameColumn.Caption = Strings.WorkOrderLableText;
			workOrderStatusColumn.Caption = Strings.WorkOrderStatusColumnText;
            workOrderRepositoryItemTextEdit.NullText = Strings.NoAssignedWorkOrderText;
            workOrderStatusRepositoryItemTextEdit.NullText = Strings.NotAvailableText;
        	assignWorkOrderBarButtonItem.Caption = Strings.AssignMenuItemText;
			editWorkOrderBarButtonItem.Caption = Strings.EditMenuItemText;
			removeWorkOrderBarButtonItem.Caption = Strings.RemoveMenuItemText;
        }

        protected override void OnReadOnlyChanged(ReadOnlyChangedEventArgs e)
        {
            base.OnReadOnlyChanged(e);
            bar.Visible = !e.ReadOnly;
            workOrderList.Enabled = !e.ReadOnly;
        }

        void WorkOrderRecordsListChanged(object sender, ListChangedEventArgs e)
        {
            switch (e.ListChangedType)
            {
                case ListChangedType.Reset:
                    break;
                case ListChangedType.ItemAdded:
                    break;
                case ListChangedType.ItemDeleted:
                    break;
                case ListChangedType.ItemMoved:
                    break;
                case ListChangedType.ItemChanged:
                    {
                        EnableBarButtons(_workOrderRecords[e.NewIndex].WorkOrder);
                        break;
                    }
                case ListChangedType.PropertyDescriptorAdded:
                    break;
                case ListChangedType.PropertyDescriptorDeleted:
                    break;
                case ListChangedType.PropertyDescriptorChanged:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void EnableBarButtons(WorkOrder workOrder)
        {
            var isWorkOrderAssigned = workOrder != null;
            //assignWorkOrderBarButtonItem.Enabled = !isWorkOrderAssigned; //ZD 103771 
            editWorkOrderBarButtonItem.Enabled = isWorkOrderAssigned;
            //removeWorkOrderBarButtonItem.Enabled = isWorkOrderAssigned;
        }

        private void EnableBarButtons(TreeListNode node)
        {
            if (node != null)
            {
                EnableBarButtons(_workOrderRecords[node.Id].WorkOrder);
            }
            else
            {
                assignWorkOrderBarButtonItem.Enabled = false;
                editWorkOrderBarButtonItem.Enabled = false;
                removeWorkOrderBarButtonItem.Enabled = false;
            }
        }
        void ConferenceBindingSource_DataSourceChanged(object sender, EventArgs e)
        {
            ResetData();
        }

        private void ResetData()
        {
            if (Conference != null)
            {
                Conference.LocationIds.ListChanged += LocationIds_ListChanged;
                ResetWorkOrderRecords();
            }
        }

        private void HousekeepingWorkOrdersPage_Load(object sender, EventArgs e)
        {
            ConferenceBindingSource.DataSourceChanged += ConferenceBindingSource_DataSourceChanged;
            _workOrderRecords.ListChanged += WorkOrderRecordsListChanged;
            ResetData();
        }

        void LocationIds_ListChanged(object sender, ListChangedEventArgs e)
        {
            switch (e.ListChangedType)
            {
                case ListChangedType.Reset:
                    {
                        ResetWorkOrderRecords();
                        break;
                    }
                case ListChangedType.ItemAdded:
                    {
                        var room = MyVrmAddin.Instance.GetRoomFromCache(new RoomId(Conference.LocationIds[e.NewIndex].ConfEndPt.Id.Id));
                        WorkOrder workorder = Conference.Conference.HouseKeepingWorkOrders.FirstOrDefault(order => order.RoomId.Equals(room.Id));
                        _workOrderRecords.Add(new WorkOrderRecord(room, workorder));
                        break;
                    }
                case ListChangedType.ItemDeleted:
                    {
                        //var room = MyVrmAddin.Instance.GetRoomFromCache(new RoomId(Conference.LocationIds[e.NewIndex].ConfEndPt.Id.Id));//ALLBUGS-123
                        //Conference.Conference.HouseKeepingWorkOrders.Remove(Conference.Conference.HouseKeepingWorkOrders.FirstOrDefault(order => order.RoomId == room.Id));//ALLBUGS-123
                        _workOrderRecords.RemoveAt(e.NewIndex);
                        break;
                    }
                case ListChangedType.ItemMoved:
                    {
                        var workOrderRecord = _workOrderRecords[e.OldIndex];
                        _workOrderRecords.Move(workOrderRecord, e.NewIndex);
                        break;
                    }
                case ListChangedType.ItemChanged:
                    break;
                case ListChangedType.PropertyDescriptorAdded:
                    break;
                case ListChangedType.PropertyDescriptorDeleted:
                    break;
                case ListChangedType.PropertyDescriptorChanged:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            workOrderList.RefreshDataSource();
        }

        private void ResetWorkOrderRecords()
        {
            _workOrderRecords.Clear();
            DataList<RoomId> roomID1;
            roomID1 = new DataList<RoomId>();
            List<string> Roomlist = new List<string>(); string Roomid = "";//ALLBUGS-123

            for (int i = 0; i < Conference.LocationIds.Count; i++)
            {
                if(new RoomId("0")!=new RoomId(Conference.LocationIds[i].ConfEndPt.Id.Id))
                roomID1.Add(new RoomId(Conference.LocationIds[i].ConfEndPt.Id.Id));
                var roomid = MyVrmAddin.Instance.GetRoomFromCache(new RoomId(Conference.LocationIds[i].ConfEndPt.Id.Id));//ALLBUGS-123
                Roomlist.Add(roomid.Id.ToString());
            }
            var query = from roomId in roomID1
                        join workOrder in Conference.Conference.HouseKeepingWorkOrders on roomId equals
                            workOrder.RoomId into workOrders
                        from wo in workOrders.DefaultIfEmpty()
                        select new WorkOrderRecord(MyVrmAddin.Instance.GetRoomFromCache(roomId), wo);

            //ALLBUGS-123 start
            for (int i = 0; i < Conference.Conference.HouseKeepingWorkOrders.Count; i++)
            {
                Roomid = Conference.Conference.HouseKeepingWorkOrders[i].RoomId.ToString();
                if (!Roomlist.Contains(Roomid))
                {
                    Conference.Conference.HouseKeepingWorkOrders.Remove(Conference.Conference.HouseKeepingWorkOrders.FirstOrDefault(order => order.RoomId.ToString() == Roomid));
                }
            }
            //ALLBUGS-123 End

            _workOrderRecords.AddRange(query);
        }

        private void editWorkOrderBarButtonItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EditWorkOrder();
        }

        private void EditWorkOrder()
        {
            if (workOrderList.FocusedNode == null)
            {
                return;
            }
            using (var dialog = new HousekeepingWorkOrderDialog())
            {
                var workOrderRecord = _workOrderRecords[workOrderList.FocusedNode.Id];
                dialog.WorkOrder = workOrderRecord.WorkOrder;
                if( dialog.ShowDialog(this) == DialogResult.OK)
                {
					if (Conference != null)
						Conference.Appointment.SetNonOutlookProperty(true);
                }
            }
        }

        private void assignWorkOrderBarButtonItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            AssignWorkOrder();
        }

        private void AssignWorkOrder()
        {
           //HousekeepingWorkOrderDialog Admin = new HousekeepingWorkOrderDialog();
            bool newWorkorder = false; //103771 start
            if (workOrderList.FocusedNode == null)
            {
                return;
            }
            using (var dialog = new HousekeepingWorkOrderDialog())
            {
                var workOrderRecord = _workOrderRecords[workOrderList.FocusedNode.Id];
                dialog.WorkOrder = new WorkOrder(MyVrmService.Service, workOrderRecord.RoomId)
                {
                    Name = string.Format("{0}_HK", Conference.Conference.Name),
                    SetId = 0,
                    Status = WorkOrderStatus.Pending,
                    Type = WorkOrderType.Housekeeping,
                    //AdministratorId = MyVrmService.Service.UserId, 
                    //WOAdministratorId = IncWO,//103364 
                    Start = Conference.StartDate,
                    Complete = Conference.EndDate,
                    TimeZone = Conference.TimeZone,
                    DeliveryCost = 0,
                    DeliveryType = WorkOrderDeliveryType.Custom,
                    ServiceCharge = 0,
                    TotalCost = 0,
                    Notify = false, 
                    Reminder = false
                };

                if (_workOrderRecords[workOrderList.FocusedNode.Id].WorkOrder == null) //103771
                    newWorkorder = true;//103771

                if (dialog.ShowDialog(this) == DialogResult.OK)
                {
                    Conference.Conference.HouseKeepingWorkOrders.Add(dialog.WorkOrder);
                    var record = _workOrderRecords.FirstOrDefault(rec => rec.RoomId == dialog.WorkOrder.RoomId);
                    record.WorkOrder = dialog.WorkOrder;
                    if (newWorkorder) //103771 start
                    {
                        _workOrderRecords.ResetItem(workOrderList.FocusedNode.Id);
                    }
                    else
                    {
                        var room = MyVrmAddin.Instance.GetRoomFromCache(record.RoomId);
                        WorkOrder workorder = Conference.Conference.HouseKeepingWorkOrders.FirstOrDefault(order => order.RoomId.Equals(room.Id));
                        _workOrderRecords.Add(new WorkOrderRecord(room, workorder));
                    }
                    //103771 End

					if (Conference != null)
						Conference.Appointment.SetNonOutlookProperty(true);
                }
            }
        }
         //ZD 103771 start
        private void removeWorkOrderBarButtonItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            List<string> Roomlist = new List<string>();
            string Roomid = "";
            int count = 0; 
            if (workOrderList.FocusedNode != null)
            {
                for (int i = 0; i < Conference.Conference.HouseKeepingWorkOrders.Count; i++)
                {
                    Roomid = Conference.Conference.HouseKeepingWorkOrders[i].RoomId.ToString();
                    Roomlist.Add(Roomid);
                }
                var workOrderRecord = _workOrderRecords[workOrderList.FocusedNode.Id];
                for (int i = 0; i < Roomlist.Count; i++)
                {
                    if (Roomlist.Contains(workOrderRecord.RoomId.ToString()))
                    {
                        count = count + 1;
                    }
                }
                if (count > 1)
                {
                    Conference.Conference.HouseKeepingWorkOrders.Remove(workOrderRecord.WorkOrder);
                    _workOrderRecords.Remove(workOrderRecord);
                    workOrderRecord.DetachWorkOrder();
                    _workOrderRecords.ResetItem(workOrderList.FocusedNode.Id);
                }
                else
                {
                    Conference.Conference.HouseKeepingWorkOrders.Remove(workOrderRecord.WorkOrder);
                    workOrderRecord.DetachWorkOrder();
                    _workOrderRecords.ResetItem(workOrderList.FocusedNode.Id);
                }
                //var workOrderRecord = _workOrderRecords[workOrderList.FocusedNode.Id];
                //Conference.Conference.HouseKeepingWorkOrders.Remove(workOrderRecord.WorkOrder);
                //workOrderRecord.DetachWorkOrder();
                //_workOrderRecords.ResetItem(workOrderList.FocusedNode.Id);  //ZD 103771 End
				if (Conference != null)
					Conference.Appointment.SetNonOutlookProperty(true);
            }
        }

        private void workOrderList_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            EnableBarButtons(e.Node);
        }

        private void workOrderList_DoubleClick(object sender, EventArgs e)
        {
            if (workOrderList.FocusedNode != null)
            {
                if (_workOrderRecords[workOrderList.FocusedNode.Id].WorkOrder == null)
                {
                    AssignWorkOrder();
                }
                else
                {
                    EditWorkOrder();
                }
            }
        }
    }
}
