﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/* NOTE!
 * 
 * DeleteTemplate can delete only one (first in list if any) template. That's why it does not have sense to make a list of IDs to delete.
 * On _any_ int value it receives <success>1</success> - no matter does this template ID exists or not.
 * Invalid format of ID provokes error 422.
 * 
 */

namespace MyVrm.WebServices.Data
{
	//Request
	internal class ConferenceDeleteTemplateRequest : ServiceRequestBase<ConferenceDeleteTemplateResponse>
	{
		public int TemplateID;
		internal ConferenceDeleteTemplateRequest(MyVrmService service)
			: base(service)
		{
		}

		#region Overrides of ServiceRequestBase<ConferenceDeleteTemplateResponse>

		internal override string GetXmlElementName()
		{
			return "login";
		}

		internal override string GetCommandName()
		{
			return Constants.DeleteTemplateCommandName;
		}

		internal override string GetResponseXmlElementName()
		{
			return "success";
		}

		internal override ConferenceDeleteTemplateResponse ParseResponse(MyVrmServiceXmlReader reader)
		{
			var response = new ConferenceDeleteTemplateResponse();
			response.ReadElementsFromXml(reader);
			return response;
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			if (UserId != null)
				UserId.WriteToXml(writer, "userID");
			writer.WriteStartElement(XmlNamespace.NotSpecified, "templates");
			writer.WriteStartElement(XmlNamespace.NotSpecified, "template");
			writer.WriteElementValue(XmlNamespace.NotSpecified, "templateID", TemplateID);
			writer.WriteEndElement();
			writer.WriteEndElement();
		}

		#endregion
	}

	//Response
	public class ConferenceDeleteTemplateResponse : ServiceResponse
	{
		internal bool Success { get; set; }

		//Read template info and template conference instance 
		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{
			Success = Utilities.BoolStringToBool(reader.ReadValue());
		}
	}
}
