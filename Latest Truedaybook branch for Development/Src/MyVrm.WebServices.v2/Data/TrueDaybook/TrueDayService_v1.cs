﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.Text;
using CallWebApi.Classes;
using Microsoft.Office.Interop.Outlook;
using MyVrm.Common;
using MyVrm.WebServices.Data;
using MyVrm.WebServices.Data.TrueDaybook;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using Exception = System.Exception;

//Manual realization
namespace MyVrm.WebServices.Data
{
	public class TrueDayServices_v1
	{
		private string _sessionId; //per organization
		private TrueDaybookResponse _trueDaybookResponse;
		private int _maxAvailSeats = -1; //-1 - not obtained yet

		public enum UserState
		{
			Undefined = -1,
			RegularUser,
			PowerUser
		}

		public UserState LoggedUserState;

		private bool _isLogged;
		public bool IsLogged
		{
			get { return _isLogged; }
			set { _isLogged = value; }
		}
		private int _settingsVersion = 0;
		public int SettingsVersion
		{
			get { return _settingsVersion; }
			set { _settingsVersion = value; }
		}

		public static object CallWebApi(object data, Uri webApiUrl, Type type, string method)
		{
			// Create a WebClient to POST the request
			WebClient client = new WebClient();

			// Set the header so it knows we are sending JSON
			client.Headers[HttpRequestHeader.ContentType] = "application/json";

			// Serialise the data we are sending in to JSON
			string serialisedData = JsonConvert.SerializeObject(data, new IsoDateTimeConverter());

			string response;
			// Make the request
			if (method == "GET")
				response = client.DownloadString(webApiUrl);
			else
				response = client.UploadString(webApiUrl, method, serialisedData);

			// Deserialise the response into a GUID
			return JsonConvert.DeserializeObject(response, type);
		}

		//GetAvailableSeats
		public int GetMaxAvail()
		{
			////Authenticate();
			//if (_maxAvailSeats == -1 || _trueDaybookDefaults == null)
			//{
			//    GetDefaultSettings();
			//}
			Authenticate();

			if (_maxAvailSeats != -1)
				return _maxAvailSeats;

			var webClient = new WebClient();
			webClient.Headers.Add("Accept", "application/json");
			string resURL = string.Format("{0}/api/Settings/GetMaximumAvailableSeats?authenticationTicket={1}", MyVrmService.Service.Url, _sessionId);
			string response = webClient.DownloadString(resURL);
			_trueDaybookResponse = new TrueDaybookResponse(response);
			TrueDaybookErrCode code = _trueDaybookResponse.Code;
			if (code.ErrorCodeInt == (int)WebApiCodes.GetMaximumAvailableSeatsCode.Success)
			{
				_maxAvailSeats = _trueDaybookResponse.GetValue<int>("MaximumAvailableSeats");
			}
			else
			{
				if(code.ErrorCodeInt == (int)WebApiCodes.GetMaximumAvailableSeatsCode.ExpiredTicket)
				{
					ClearLoggedState();
					GetMaxAvail();
				}
				MyVrmService.TraceSource.TraceError(code.ErrorCodeString + code.ErrorMsg);
			}
		
			return _maxAvailSeats;
		}

		string DateTimeToString(DateTime dt)
		{
			return dt.ToString("yyyy-MM-dd HH:mm");
		}
		//public TrueDayServices()
		//{
		//    //Authenticate();
		//}

		void ClearLoggedState()
		{
			_isLogged = false;
		}
		
		public bool Authenticate()//string login, string pwd)
		{
			if (_isLogged)
				return true;

			//AuthRequest request = new AuthRequest
			//{
			//    Name = MyVrmService.Service.Credential.UserName,
			//    Password = MyVrmService.Service.Credential.Password
			//};

			bool bRet = false;
			string resURL = MyVrmService.Service.FullUrl;

			var webClient = new WebClient();
			webClient.Headers.Add("Accept", "application/json");
			//webClient.Headers.Add("HttpMethod", "POST");
			resURL = MyVrmService.Service.Url + "/api/User/Authenticate";
			//webClient.Credentials = new System.Net.NetworkCredential(creds.Name, creds.Password);
			string response = string.Empty;
			NameValueCollection param = new NameValueCollection();
			param.Add("name", MyVrmService.Service.Credential.UserName);
			param.Add("password", MyVrmService.Service.Credential.Password);
			var resp = webClient.UploadValues(resURL, null, param);
			char[] respArr = Encoding.UTF8.GetChars(resp);
			response = new string(respArr);
			_trueDaybookResponse = new TrueDaybookResponse(response);
			TrueDaybookErrCode code = _trueDaybookResponse.Code;

			if (code.ErrorCodeInt == (int)WebApiCodes.AuthenticateCode.Success)
			{
				_sessionId = _trueDaybookResponse.GetValue("AuthenticationTicket").ToString();
				//Guid guid = new Guid(_sessionId);
				int state = 0;
				int.TryParse(_trueDaybookResponse.GetValue("UserType").ToString(), out state);
				LoggedUserState = (UserState) state;
				bRet = true;
			}
			else
			{
				MyVrmService.TraceSource.TraceError(code.ErrorCodeString + code.ErrorMsg);
			}
			
			_isLogged = bRet;

			return bRet;
		}

		//
		private TrueDaybookDefaults _trueDaybookDefaults = null;
		public TrueDaybookDefaults GetDefaultSettings()
		{
			try
			{
				Authenticate();

				if (_trueDaybookDefaults != null)
					return _trueDaybookDefaults;

				var webClient = new WebClient();
				webClient.Headers.Add("Accept", "application/json");
				string resURL = string.Format("{0}/api/Settings/GetDefaultSettings?authenticationTicket={1}", MyVrmService.Service.Url, _sessionId);
				string response = webClient.DownloadString(resURL);
				_trueDaybookResponse = new TrueDaybookResponse(response);
				TrueDaybookErrCode code = _trueDaybookResponse.Code;
				if (code.ErrorCodeInt == (int)WebApiCodes.GetDefaultSettingsCode.Success)
				{
					_maxAvailSeats = _trueDaybookResponse.GetValue<int>("MaximumAvailableSeats");
					string defaultBody = _trueDaybookResponse.GetValue<string>("DefaultAppointmentInvitation");
					string defaultSubj = _trueDaybookResponse.GetValue<string>("DefaultAppointmentSubject");
					TrueDaybookDefaults.TxtTypeEnum fmt = TrueDaybookDefaults.TxtTypeEnum.Txt; //_trueDaybookResponse.GetValue<TrueDaybookDefaults.TxtTypeEnum>("defBodyFormat");
					_trueDaybookDefaults = new TrueDaybookDefaults(defaultSubj, defaultBody, fmt);
				}
				else
				{
					MyVrmService.TraceSource.TraceError(code.ErrorCodeString + code.ErrorMsg);
				}
			}
			catch (Exception exception)
			{
				MyVrmService.TraceSource.TraceError(exception.Message);
			}

			return _trueDaybookDefaults;
		}

		public AvailSeatsCollection GetAvailSeats(DateTime start, DateTime end)
		{
			//_sessionId
			Authenticate();

			AvailSeatsCollection ret = new AvailSeatsCollection();
			DateTime startUtc = start.ToUniversalTime(); 
			DateTime endUtc = end.ToUniversalTime();

			var webClient = new WebClient();
			webClient.Headers.Add("Accept", "application/json");
			string resURL = string.Format("{0}/api/Settings/GetAvailableSeats?authenticationTicket={1}&startDate={2}&endDate={3}",
				MyVrmService.Service.Url, _sessionId, DateTimeToString(startUtc), DateTimeToString(endUtc));
			string response = webClient.DownloadString(resURL);
			_trueDaybookResponse = new TrueDaybookResponse(response);
			TrueDaybookErrCode code = _trueDaybookResponse.Code;
			if (code.ErrorCodeInt == (int)WebApiCodes.GetAvailableSeatsCode.Success)
			{
				JArray reservations = _trueDaybookResponse.GetJArray("Reservations");

				if (reservations != null)
				{
					DateTime lastDate = new DateTime();
					int lastSeatsNum = 0;
					foreach (var record in reservations)
					{
						int seats = record["Seats"].Value<int>();
						DateTime dt = record["Date"].Value<DateTime>();
						TimeSpan interval = dt.ToLocalTime() - start;
						lastDate = dt.ToLocalTime();
						lastSeatsNum = seats;
						ret.Add(new AvailSeats() { Seats = seats, TimeSpan = interval });
					}
					if(lastDate < end)
					{
						int i = ret.Count;
						ret.Add(new AvailSeats() { Seats = lastSeatsNum, TimeSpan = end - start });
					}
				}
			}
			else
			{
				MyVrmService.TraceSource.TraceError(code.ErrorCodeString + code.ErrorMsg);
			}
						 

			//ret.Add(new AvailSeats() { Seats = 0, TimeSpan = new TimeSpan(8, 0, 0) });
			//ret.Add(new AvailSeats() { Seats = 3, TimeSpan = new TimeSpan(0, 10, 0)});
			//ret.Add(new AvailSeats() { Seats = 0, TimeSpan = new TimeSpan(0, 50, 0) });
			//ret.Add(new AvailSeats() { Seats = 5, TimeSpan = new TimeSpan(0, 30, 0) });
			//ret.Add(new AvailSeats() { Seats = 2, TimeSpan = new TimeSpan(0, 30, 0) });
			//ret.Add(new AvailSeats() { Seats = 0, TimeSpan = new TimeSpan(2, 0, 0) });
			//ret.Add(new AvailSeats() { Seats = 7, TimeSpan = new TimeSpan(0, 15, 0) });
			//ret.Add(new AvailSeats() { Seats = 4, TimeSpan = new TimeSpan(0, 15, 0) });
			//ret.Add(new AvailSeats() { Seats = 0, TimeSpan = new TimeSpan(0, 0, 1) });
			return ret;
		}
		public const string VirtualConfIdPropName = "VConfId";

		public static List<ConferenceRequest.Participant> GetParticipantsList(AppointmentItem appItem)
		{
			List<ConferenceRequest.Participant> uniqueRecipients = new List<ConferenceRequest.Participant>();

			string smtpAddress = string.Empty;
			foreach (Recipient recipient in appItem.Recipients)
			{
				if (recipient.Resolved) //found in Exchange
				{
					var exchangeUser = recipient.AddressEntry.GetExchangeUser();
					smtpAddress = exchangeUser != null ? exchangeUser.PrimarySmtpAddress : recipient.Address;
				}
				else
				{
					smtpAddress = recipient.Name; //not found - take it's name as an e-mail address
				}

				if (uniqueRecipients.Find( item => item.Email == smtpAddress) == null) //( IndexOf(smtpAddress) == -1))
				{
					uniqueRecipients.Add(new ConferenceRequest.Participant() {Email = smtpAddress, Name = recipient.Name});
				}
			}
			return uniqueRecipients;
		}

		public void ScheduleNewVConf(AppointmentItem appItem)//ref VConf vConf)//object data) 
		{
			//_sessionId
			Authenticate();
			DateTime start = appItem.Start.ToUniversalTime(); //DateTime.SpecifyKind(appItem.Start, DateTimeKind.Utc);//vConf.Start, DateTimeKind.Utc);
			DateTime end = appItem.End.ToUniversalTime(); //DateTime.SpecifyKind(appItem.End, DateTimeKind.Utc);

			foreach (Recipient recipient in appItem.Recipients)
			{
				var exchangeUser = recipient.AddressEntry.GetExchangeUser();
				var smtpAddress = exchangeUser != null ? exchangeUser.PrimarySmtpAddress : recipient.Address;
				//vConf.Envitees.Add(smtpAddress);//recipient.Name);
			}

			if(appItem.IsRecurring && appItem.RecurrenceState == OlRecurrenceState.olApptMaster)
			{
				//Microsoft.Office.Interop.Outlook.RecurrencePattern rp = appItem.GetRecurrencePattern();
				//????????? 
				//switch( rp.RecurrenceType)
				//{
				//    case RecurrenceType.
				//}
			}
			var webClient = new WebClient();
			webClient.Headers.Add("Accept", "application/json");
			webClient.Headers.Add("ContentType", "application/json");
			webClient.Headers.Add("Content-Type", "application/json");
			string resURL = string.Format("{0}/api/Conference?authenticationTicket={1}", MyVrmService.Service.Url, _sessionId); ///PostConference
			string response = string.Empty;
			NameValueCollection param = new NameValueCollection();
			param.Add("Subject", appItem.Subject);
			param.Add("InvitationBody", appItem.Body);
			param.Add("StartDate", DateTimeToString(start));
			param.Add("EndDate", DateTimeToString(end));

			//NameValueCollection participants = new NameValueCollection();
			
			ConferenceRequest req = new ConferenceRequest
										{

											Subject = appItem.Subject,
											StartDate = start,
											EndDate = end,
											InvitationBody = appItem.Body,
											Recurrence = "",
											Participants = GetParticipantsList(appItem)
											//Participants = new List<ConferenceRequest.Participant>
											//{
											//    new ConferenceRequest.Participant
											//    {
											//        Email = "email1@test.net",
											//        Name = "name1@test.net",
											//    },
											//    new ConferenceRequest.Participant
											//    {
											//        Email = "email1@test.net",
											//        Name = "name1@test.net",
											//    }
											//}
										};
			string stt = JsonConvert.SerializeObject(req.Participants);
			param.Add("Participants", stt);//participants);
			string serialisedData = JsonConvert.SerializeObject(req, new IsoDateTimeConverter());

			try
			{
				response = webClient.UploadString(resURL, serialisedData);
					//UploadValues(resURL, null, param);
				//char[] respArr = Encoding.UTF8.GetChars(resp);
				//response = new string(respArr);
			}
			catch (Exception exception)
			{
				MyVrmService.TraceSource.TraceError(exception.Message);
				throw;
			}

			_trueDaybookResponse = new TrueDaybookResponse(response);
			TrueDaybookErrCode code = _trueDaybookResponse.Code;
			if (code.ErrorCodeInt == (int)WebApiCodes.PostConferenceCode.Success)
			{
				if (appItem.UserProperties.Find(VirtualConfIdPropName, true) == null)
					appItem.UserProperties.Add(VirtualConfIdPropName, OlUserPropertyType.olText/*olInteger*/, false, OlFormatText.olFormatTextText);//olFormatNumberRaw);
				appItem.UserProperties[VirtualConfIdPropName].Value = (int)_trueDaybookResponse.GetValue<int>("Id");
			}
			else
			{
				MyVrmService.TraceSource.TraceError(code.ErrorCodeString + code.ErrorMsg);
			}
		}

		public void DeleteVConf(int conferenceId)
		{
			Authenticate();
			var webClient = new WebClient();
			webClient.Headers.Add("Accept", "application/json");
			string resURL = string.Format("{0}/api/Conference?authenticationTicket={1}&conferenceId={2}", MyVrmService.Service.Url, _sessionId, conferenceId); ///DeleteConference
			try
			{
				string serialisedData = JsonConvert.SerializeObject(null, new IsoDateTimeConverter());

				string response = webClient.UploadString(resURL, "DELETE", serialisedData);
				_trueDaybookResponse = new TrueDaybookResponse(response);
			}
			catch (Exception exception)
			{
				MyVrmService.TraceSource.TraceError(exception.Message);
				throw;
			}
			TrueDaybookErrCode code = _trueDaybookResponse.Code;
			if (code.ErrorCodeInt != (int)WebApiCodes.GetDefaultSettingsCode.Success)
			{
				MyVrmService.TraceSource.TraceError(code.ErrorCodeString + code.ErrorMsg);
			}
		}
	
		/// <summary>
		/// Update existing virtual conference data
		/// </summary>
		/// <param name="data"></param>
		public void UpdateVConf(int conferenceId, AppointmentItem appItem)//VConf vConf) //object data)
		{
			//_sessionId ?????
			Authenticate();
			DateTime start = appItem.Start.ToUniversalTime(); //DateTime.SpecifyKind(appItem.Start, DateTimeKind.Utc);
			DateTime end = appItem.End.ToUniversalTime();//DateTime.SpecifyKind(appItem.End, DateTimeKind.Utc);

			var webClient = new WebClient();
			webClient.Headers.Add("Accept", "application/json");
			webClient.Headers.Add("ContentType", "application/json");
			webClient.Headers.Add("Content-Type", "application/json");
			string resURL = string.Format("{0}/api/Conference?authenticationTicket={1}", MyVrmService.Service.Url, _sessionId);///PutConference
			string response = string.Empty;

			ConferenceRequest req = new ConferenceRequest
			{
				Id = conferenceId,
				Subject = appItem.Subject,
				StartDate = start,
				EndDate = end,
				InvitationBody = appItem.Body,
				Recurrence = "",
				Participants = GetParticipantsList(appItem)
				//new List<ConferenceRequest.Participant>
				//                {
				//                    new ConferenceRequest.Participant
				//                    {
				//                        Email = "email1@test.net",
				//                        Name = "name1@test.net",
				//                    },
				//                    new ConferenceRequest.Participant
				//                    {
				//                        Email = "email1@test.net",
				//                        Name = "name1@test.net",
				//                    }
				//                }
			};
			string serialisedData = JsonConvert.SerializeObject(req, new IsoDateTimeConverter());

			try
			{
				response = webClient.UploadString(resURL, "PUT", serialisedData);
			}
			catch (Exception exception)
			{
				MyVrmService.TraceSource.TraceError(exception.Message);
				throw;
			}

			_trueDaybookResponse = new TrueDaybookResponse(response);
			TrueDaybookErrCode code = _trueDaybookResponse.Code;
			if (code.ErrorCodeInt == (int)WebApiCodes.PutConferenceCode.Success)
			{
				//if (appItem.UserProperties.Find(VirtualConfIdPropName, true) == null)
				//    appItem.UserProperties.Add(VirtualConfIdPropName, OlUserPropertyType.olInteger, false, OlFormatNumber.olFormatNumberRaw);
				//appItem.UserProperties[VirtualConfIdPropName].Value = (int)_trueDaybookResponse.GetValue<int>("Id");
			}
			else
			{
				MyVrmService.TraceSource.TraceError(code.ErrorCodeString + code.ErrorMsg);
			}
		}
	}
}