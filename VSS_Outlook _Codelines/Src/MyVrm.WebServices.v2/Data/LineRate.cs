﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    public class LineRate
    {
        internal LineRate()
        {
        }

        public int Id { get; internal set; }
        public string Name { get; internal set; }

        internal void LoadFromXml(MyVrmServiceXmlReader reader)
        {
            Id = reader.ReadElementValue<int>(XmlNamespace.NotSpecified, "LineRateID");
            Name = reader.ReadElementValue(XmlNamespace.NotSpecified, "LineRateName");
        }
    }
}
