﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System.Collections.Generic;
namespace MyVrm.WebServices.Data
{
    public class WorkOrderItem : ComplexProperty
    {
		public override object Clone()//CopyTo(WorkOrderItem item)
		{
			//if (item == null)
			WorkOrderItem item = new WorkOrderItem();

			item.DeliveryCost = DeliveryCost;
			item.DeliveryType = DeliveryType;
			item.Id = Id;
			item.Quantity = Quantity;
			item.ServiceCharge = ServiceCharge;
			item.Uid = Uid;
			return item;
		}



		public override bool Equals(object obj)
		{
			if (ReferenceEquals(this, obj))
				return true;

			WorkOrderItem item = obj as WorkOrderItem;
			if(item == null)
				return false;
			if (item.DeliveryCost != DeliveryCost)
				return false;
			if (item.DeliveryType != DeliveryType)
				return false;
			if (item.Id != Id)
				return false;
			if (item.Quantity != Quantity)
				return false;
			if (item.ServiceCharge != ServiceCharge)
				return false;
			if (item.Uid != Uid)
				return false;
			return true;
		}
        public WorkOrderItem()
        {
            Uid = 0;
        }


        private List<InventoryCharges> inventoryCharges;

        public List<InventoryCharges> InventoryCharges
        {
            get { return inventoryCharges; }
            set { inventoryCharges = value; }
        }

        private int uid;
        public int Id { get; set; }
        public int Uid
        {
            get
            {
                return uid;
            }

            private set
            {
                uid = value;
            }
        }
        public uint Quantity { get; set; }
        public decimal ServiceCharge { get; set; }
        public int DeliveryType { get; set; }
        public decimal DeliveryCost { get; set; }

        public decimal Price { get; set; }

        

        internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
        {
            switch (reader.LocalName)
            {
                case "ID":
                    {
                        Id = reader.ReadElementValue<int>();
                        return true;
                    }
                case "UID":
                    {
                        Uid = reader.ReadElementValue<int>();
                        return true;
                    }
                case "QuantityRequested":
                    {
                        Quantity = reader.ReadElementValue<uint>();
                        return true;
                    }
                case "ServiceCharge":
                    {
                        ServiceCharge = reader.ReadElementValue<decimal>();
                        return true;
                    }
                case "DeliveryType":
                    {
                        DeliveryType = reader.ReadElementValue<int>();
                        return true;
                    }
                case "DeliveryCost":
                    {
                        DeliveryCost = reader.ReadElementValue<decimal>();
                        return true;
                    }
                case "Price":
                    {
                        Price = reader.ReadElementValue<decimal>();
                        return true;
                    }
            }
            return false;
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            writer.WriteElementValue(XmlNamespace.NotSpecified, "ID", Id);
            // UID is always "0"
            writer.WriteElementValue(XmlNamespace.NotSpecified, "UID", Uid);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "Quantity", Quantity);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "ServiceCharge", ServiceCharge);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "DeliveryType", DeliveryType);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "DeliveryCost", DeliveryCost);
            
            
            
        }

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
    }
}
