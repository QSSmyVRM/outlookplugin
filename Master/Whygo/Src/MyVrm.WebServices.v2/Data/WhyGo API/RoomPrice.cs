﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	public class RoomPrice : ComplexProperty
	{
		private const string const_LocationID = "LocationID";
		private const string const_AfterHoursPrice = "afterHoursPrice";
		private const string const_CrazyHoursPrice = "crazyHoursPrice";
		private const string const_EarlyHoursPrice = "earlyHoursPrice";
		private const string const_OfficeHoursPrice = "officeHoursPrice";
		private const string const_CurrencyType = "CurrencyType";

		private RoomId _locationID;
		private string _afterHoursPrice;
		private string _crazyHoursPrice;
		private string _earlyHoursPrice;
		private string _officeHoursPrice;
		private CurrencyAbbr.CurrencyType _currencyType;

		public override object Clone()
		{
			RoomPrice copy = new RoomPrice();
			copy._locationID = new RoomId(_locationID.Id);
			copy._afterHoursPrice = string.Copy(_afterHoursPrice);
			copy._crazyHoursPrice = string.Copy(_crazyHoursPrice);
			copy._earlyHoursPrice = string.Copy(_earlyHoursPrice);
			copy._officeHoursPrice = string.Copy(_officeHoursPrice);
			copy._currencyType = _currencyType;

			return copy;
		}

		internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
		{
			bool bRet = false;
			switch (reader.LocalName)
			{
				case const_LocationID:
					{
						_locationID = new RoomId( reader.ReadElementValue().Trim());
						bRet = true;
						break;
					}
				case const_AfterHoursPrice:
					{
						_afterHoursPrice = reader.ReadElementValue();
						bRet = true;
						break;
					}
				case const_CrazyHoursPrice:
					{
						_crazyHoursPrice = reader.ReadElementValue();
						bRet = true;
						break;
					}
				case const_EarlyHoursPrice:
					{
						_earlyHoursPrice = reader.ReadElementValue();
						bRet = true;
						break;
					}
				case const_OfficeHoursPrice:
					{
						_officeHoursPrice = reader.ReadElementValue();
						bRet = true;
						break;
					}
				case const_CurrencyType:
					{
						_currencyType = CurrencyAbbr.GetCurrencyByAbbr(reader.ReadElementValue().Trim());
						bRet = true;
						break;
					}
				default:
					break;
			}
			reader.Read();
			return bRet;
		}

		//internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		//{
		//    writer.WriteStartElement(Namespace, const_TemplateInfo);
		//    if (ID != null)
		//        writer.WriteElementValue(Namespace, const_ID, ID);
		//    if (Name != null)
		//        writer.WriteElementValue(Namespace, const_Name, Name);
		//    writer.WriteElementValue(Namespace, const_IsPublic, IsPublic.ToString());
		//    writer.WriteStartElement(Namespace, const_Owner);
		//    if (OwnerFirstName != null)
		//        writer.WriteElementValue(Namespace, const_OwnerFirstName, OwnerFirstName);
		//    if (OwnerLastName != null)
		//        writer.WriteElementValue(Namespace, const_OwnerLastName, OwnerLastName);
		//    writer.WriteEndElement();
		//    writer.WriteElementValue(Namespace, const_SetDefault, SetDefault.ToString().ToLower());
		//    if (Description != null)
		//        writer.WriteElementValue(Namespace, const_Description, Description);
		//    writer.WriteEndElement();
		//}
	}
}
