﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace MyVrm.Common.Caching
{
    /// <summary>
    /// Utility class for serializing and deserializing objects to and from byte streams
    /// </summary>
    public static class SerializationUtility
    {
        /// <summary>
        /// Converts an object into an array of bytes. Object must be serializable.
        /// </summary>
        /// <param name="value">Object to serialize. May be null.</param>
        /// <returns>Serialized object, or null if input was null.</returns>
        public static byte[] ToBytes(object value)
        {
            if (value == null)
            {
                return null;
            }

            byte[] inMemoryBytes;
            using (var inMemoryData = new MemoryStream())
            {
                new BinaryFormatter().Serialize(inMemoryData, value);
                inMemoryBytes = inMemoryData.ToArray();
            }

            return inMemoryBytes;
        }

        /// <summary>
        /// Converts a byte array into an object. 
        /// </summary>
        /// <param name="serializedObject">Object to deserialize. May be null.</param>
        /// <returns>Deserialized object, or null if input was null.</returns>
        public static object ToObject(byte[] serializedObject)
        {
            if (serializedObject == null)
            {
                return null;
            }

            using (var dataInMemory = new MemoryStream(serializedObject))
            {
                return new BinaryFormatter().Deserialize(dataInMemory);
            }
        }
    }
}
