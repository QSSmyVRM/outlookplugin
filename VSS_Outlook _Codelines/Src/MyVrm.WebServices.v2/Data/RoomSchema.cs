﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    public class RoomSchema : ServiceObjectSchema
    {
        public static readonly PropertyDefinition Id;
        public static readonly PropertyDefinition Name;
        public static readonly PropertyDefinition Phone;
        public static readonly PropertyDefinition Capacity;
        public static readonly PropertyDefinition PhoneLinesCount;
        public static readonly PropertyDefinition SetupTime;
        public static readonly PropertyDefinition TeardownTime;
        public static readonly PropertyDefinition AssistantId;
        public static readonly PropertyDefinition AssistantName;
        public static readonly PropertyDefinition MultipleAssistantEmails;
        public static readonly PropertyDefinition TopTierId;
        public static readonly PropertyDefinition TopTierName;
        public static readonly PropertyDefinition MiddleTierId;
        public static readonly PropertyDefinition MiddleTierName;
        public static readonly PropertyDefinition Floor;
        public static readonly PropertyDefinition Number;
        public static readonly PropertyDefinition StreetAddress1;
        public static readonly PropertyDefinition StreetAddress2;
        public static readonly PropertyDefinition City;
        public static readonly PropertyDefinition StateId;
        public static readonly PropertyDefinition State;
        public static readonly PropertyDefinition ZipCode;
        public static readonly PropertyDefinition CountryId;
        public static readonly PropertyDefinition Country;
        public static readonly PropertyDefinition HandicappedAccess;
        public static readonly PropertyDefinition MapLink;
        public static readonly PropertyDefinition ParkingDirections;
        public static readonly PropertyDefinition AdditionalComments;
        public static readonly PropertyDefinition TimeZone;
        public static readonly PropertyDefinition Longitude;
        public static readonly PropertyDefinition Latitude;
        public static readonly PropertyDefinition Approvers;
        public static readonly PropertyDefinition EndpointId;
        public static readonly PropertyDefinition Custom1;
        public static readonly PropertyDefinition Custom2;
        public static readonly PropertyDefinition Custom3;
        public static readonly PropertyDefinition Custom4;
        public static readonly PropertyDefinition Custom5;
        public static readonly PropertyDefinition Custom6;
        public static readonly PropertyDefinition Custom7;
        public static readonly PropertyDefinition Custom8;
        public static readonly PropertyDefinition Custom9;
        public static readonly PropertyDefinition Custom10;
        public static readonly PropertyDefinition Projector;
        public static readonly PropertyDefinition Media; 
        public static readonly PropertyDefinition DynamicLayout;
        public static readonly PropertyDefinition CateringFacility;
        public static readonly PropertyDefinition Departments;
        public static readonly PropertyDefinition RoomImages;
        public static readonly PropertyDefinition RoomImageName;
        public static readonly PropertyDefinition Images;
		public static readonly PropertyDefinition RoomCategory;
        public static readonly PropertyDefinition isVMR;
        public static readonly PropertyDefinition InternalNumber;
        public static readonly PropertyDefinition ExternalNumber;
        public static readonly PropertyDefinition AssistantInchargeEmail;

        internal static readonly RoomSchema Instance;

        static RoomSchema()
        {
            Id = new ComplexPropertyDefinition<RoomId>("RoomID", () => new RoomId());
            Name = new StringPropertyDefinition("RoomName", PropertyDefinitionFlags.CanSet);
            Phone = new StringPropertyDefinition("RoomPhoneNumber", PropertyDefinitionFlags.CanSet);
            Capacity = new IntPropertyDefinition("MaximumCapacity", PropertyDefinitionFlags.CanSet);
            PhoneLinesCount = new IntPropertyDefinition("MaximumConcurrentPhoneCalls", PropertyDefinitionFlags.CanSet);
            SetupTime = new IntPropertyDefinition("SetupTime", PropertyDefinitionFlags.CanSet);
            TeardownTime = new IntPropertyDefinition("TeardownTime", PropertyDefinitionFlags.CanSet);
            AssistantId = new ComplexPropertyDefinition<UserId>("AssistantInchargeID", PropertyDefinitionFlags.CanSet, () => new UserId());
            AssistantName = new StringPropertyDefinition("AssistantInchargeName", PropertyDefinitionFlags.None);
            MultipleAssistantEmails = new StringPropertyDefinition("MultipleAssistantEmails", PropertyDefinitionFlags.CanSet);
            AssistantInchargeEmail = new StringPropertyDefinition("AssistantInchargeEmail", PropertyDefinitionFlags.CanSet);
            TopTierId = new ComplexPropertyDefinition<TierId>("Tier1ID", PropertyDefinitionFlags.CanSet, () => new TierId());
            TopTierName = new StringPropertyDefinition("Tier1Name", PropertyDefinitionFlags.None);
            MiddleTierId = new ComplexPropertyDefinition<TierId>("Tier2ID", PropertyDefinitionFlags.CanSet, () => new TierId());
            MiddleTierName = new StringPropertyDefinition("Tier2Name", PropertyDefinitionFlags.None);
            Floor = new StringPropertyDefinition("Floor", PropertyDefinitionFlags.CanSet);
            Number = new StringPropertyDefinition("RoomNumber", PropertyDefinitionFlags.CanSet);
            StreetAddress1 = new StringPropertyDefinition("StreetAddress1", PropertyDefinitionFlags.CanSet);
            StreetAddress2 = new StringPropertyDefinition("StreetAddress2", PropertyDefinitionFlags.CanSet);
            City = new StringPropertyDefinition("City", PropertyDefinitionFlags.CanSet);
            StateId = new StringPropertyDefinition("State", PropertyDefinitionFlags.CanSet);
            State = new StringPropertyDefinition("StateName", PropertyDefinitionFlags.None);
            ZipCode = new StringPropertyDefinition("ZipCode", PropertyDefinitionFlags.CanSet);
            CountryId = new StringPropertyDefinition("Country", PropertyDefinitionFlags.CanSet);
            Country = new StringPropertyDefinition("CountryName", PropertyDefinitionFlags.CanSet);
            HandicappedAccess = new BoolPropertyDefinition("Handicappedaccess", PropertyDefinitionFlags.CanSet);
            MapLink = new StringPropertyDefinition("MapLink", PropertyDefinitionFlags.CanSet);
            ParkingDirections = new StringPropertyDefinition("ParkingDirections", PropertyDefinitionFlags.CanSet);
            AdditionalComments = new StringPropertyDefinition("AdditionalComments", PropertyDefinitionFlags.CanSet);
            TimeZone = new TimeZonePropertyDefinition("TimezoneID", PropertyDefinitionFlags.CanSet);
            Longitude = new StringPropertyDefinition("Longitude", PropertyDefinitionFlags.CanSet);
            Latitude = new StringPropertyDefinition("Latitude", PropertyDefinitionFlags.CanSet);
            Approvers = new ComplexPropertyDefinition<RoomApprovers>("Approvers",
                                                                     PropertyDefinitionFlags.AutoInstantiateOnRead |
                                                                     PropertyDefinitionFlags.CanSet,
                                                                     () => new RoomApprovers());
            EndpointId = new ComplexPropertyDefinition<EndpointId>("EndpointID", PropertyDefinitionFlags.CanSet,
                                                                   () => new EndpointId());
            Custom1 = new StringPropertyDefinition("Custom1", PropertyDefinitionFlags.CanSet);
            Custom2 = new StringPropertyDefinition("Custom2", PropertyDefinitionFlags.CanSet);
            Custom3 = new StringPropertyDefinition("Custom3", PropertyDefinitionFlags.CanSet);
            Custom4 = new StringPropertyDefinition("Custom4", PropertyDefinitionFlags.CanSet);
            Custom5 = new StringPropertyDefinition("Custom5", PropertyDefinitionFlags.CanSet);
            Custom6 = new StringPropertyDefinition("Custom6", PropertyDefinitionFlags.CanSet);
            Custom7 = new StringPropertyDefinition("Custom7", PropertyDefinitionFlags.CanSet);
            Custom8 = new StringPropertyDefinition("Custom8", PropertyDefinitionFlags.CanSet);
            Custom9 = new StringPropertyDefinition("Custom9", PropertyDefinitionFlags.CanSet);
            Custom10 = new StringPropertyDefinition("Custom10", PropertyDefinitionFlags.CanSet);
            Projector = new BoolPropertyDefinition("Projector", PropertyDefinitionFlags.CanSet);
            isVMR = new BoolPropertyDefinition("IsVMR", PropertyDefinitionFlags.CanSet);
            InternalNumber = new StringPropertyDefinition("InternalNumber", PropertyDefinitionFlags.CanSet);
            ExternalNumber = new StringPropertyDefinition("ExternalNumber", PropertyDefinitionFlags.CanSet);
            Media = new GenericPropertyDefinition<MediaType>("Video", PropertyDefinitionFlags.CanSet);
            DynamicLayout = new BoolPropertyDefinition("DynamicRoomLayout", PropertyDefinitionFlags.CanSet);
            CateringFacility = new BoolPropertyDefinition("CatererFacility", PropertyDefinitionFlags.CanSet);
            Departments = new ComplexPropertyDefinition<DepartmentCollection>("Departments",
                                                                              PropertyDefinitionFlags.
                                                                                  AutoInstantiateOnRead,
                                                                              () => new DepartmentCollection());
            RoomImages = new ComplexPropertyDefinition<RoomImageCollection>("RoomImages",
                                                                            PropertyDefinitionFlags.
                                                                                AutoInstantiateOnRead,
                                                                            () => new RoomImageCollection());
            RoomImageName = new StringPropertyDefinition("RoomImageName", PropertyDefinitionFlags.CanSet);
            Images = new ComplexPropertyDefinition<RoomImages>("Images",
                                                               PropertyDefinitionFlags.AutoInstantiateOnRead |
                                                               PropertyDefinitionFlags.CanSet, () => new RoomImages());
			RoomCategory = new GenericPropertyDefinition<RoomCategoryType>("RoomCategory", PropertyDefinitionFlags.CanSet);  

            Instance = new RoomSchema();
        }

        internal override void RegisterProperties()
        {
            base.RegisterProperties();
            RegisterProperty(Id);
            RegisterProperty(Name);
            RegisterProperty(Phone); 
            RegisterProperty(Capacity);
            RegisterProperty(PhoneLinesCount);
            RegisterProperty(SetupTime);
            RegisterProperty(TeardownTime);
            RegisterProperty(AssistantId);
            RegisterProperty(AssistantName);
            RegisterProperty(MultipleAssistantEmails);
            RegisterProperty(AssistantInchargeEmail);
            RegisterProperty(TopTierId);
            RegisterProperty(TopTierName);
            RegisterProperty(MiddleTierId);
            RegisterProperty(MiddleTierName);
            RegisterProperty(Floor);
            RegisterProperty(Number);
            RegisterProperty(StreetAddress1);
            RegisterProperty(StreetAddress2);
            RegisterProperty(City);
            RegisterProperty(StateId);
            RegisterProperty(State);
            RegisterProperty(ZipCode);
            RegisterProperty(CountryId);
            RegisterProperty(Country);
            RegisterProperty(HandicappedAccess);
            RegisterProperty(MapLink);
            RegisterProperty(ParkingDirections);
            RegisterProperty(AdditionalComments);
            RegisterProperty(TimeZone);
            RegisterProperty(Longitude);
            RegisterProperty(Latitude);
            RegisterProperty(Approvers);
            RegisterProperty(EndpointId);
            RegisterProperty(Custom1);
            RegisterProperty(Custom2);
            RegisterProperty(Custom3);
            RegisterProperty(Custom4);
            RegisterProperty(Custom5);
            RegisterProperty(Custom6);
            RegisterProperty(Custom7);
            RegisterProperty(Custom8);
            RegisterProperty(Custom9);
            RegisterProperty(Custom10);
            RegisterProperty(Projector);
            RegisterProperty(Media); 
            RegisterProperty(DynamicLayout);
            RegisterProperty(CateringFacility);
            RegisterProperty(Departments);
            RegisterProperty(RoomImages);
            RegisterProperty(RoomImageName);
            RegisterProperty(Images);
			RegisterProperty(RoomCategory);
            RegisterProperty(isVMR);
            RegisterProperty(InternalNumber);
            RegisterProperty(ExternalNumber);
        }
    }
}
