﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.Outlook.WinForms.Conference
{
	partial class ExternalRoomsControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && _conferenceBindingSource != null)
				_conferenceBindingSource.DataSourceChanged -= ConferenceBindingSourceDataSourceChanged;

			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
			this.bnEditRoom = new DevExpress.XtraEditors.SimpleButton();
			this.extUsersTreeList = new DevExpress.XtraTreeList.TreeList();
			this.emailCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.addressCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.protocolCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.mcuCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.callerColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.bnAddRoom = new DevExpress.XtraEditors.SimpleButton();
			this.bnRemoveRoom = new DevExpress.XtraEditors.SimpleButton();
			this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
			this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
			this.layoutControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.extUsersTreeList)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
			this.SuspendLayout();
			// 
			// layoutControl1
			// 
			this.layoutControl1.Controls.Add(this.bnEditRoom);
			this.layoutControl1.Controls.Add(this.extUsersTreeList);
			this.layoutControl1.Controls.Add(this.bnAddRoom);
			this.layoutControl1.Controls.Add(this.bnRemoveRoom);
			this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.layoutControl1.Location = new System.Drawing.Point(0, 0);
			this.layoutControl1.Name = "layoutControl1";
			this.layoutControl1.Root = this.layoutControlGroup1;
			this.layoutControl1.Size = new System.Drawing.Size(722, 238);
			this.layoutControl1.TabIndex = 1;
			this.layoutControl1.Text = "layoutControl1";
			// 
			// bnEditRoom
			// 
			this.bnEditRoom.Location = new System.Drawing.Point(591, 61);
			this.bnEditRoom.Name = "bnEditRoom";
			this.bnEditRoom.Size = new System.Drawing.Size(124, 28);
			this.bnEditRoom.StyleController = this.layoutControl1;
			this.bnEditRoom.TabIndex = 11;
			this.bnEditRoom.Text = "Edit Selected Room";
			this.bnEditRoom.Click += new System.EventHandler(this.bnEditRoom_Click);
			// 
			// extUsersTreeList
			// 
			this.extUsersTreeList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.emailCol,
            this.addressCol,
            this.protocolCol,
            this.mcuCol,
            this.callerColumn});
			this.extUsersTreeList.Location = new System.Drawing.Point(0, 0);
			this.extUsersTreeList.Name = "extUsersTreeList";
			this.extUsersTreeList.OptionsView.ShowIndicator = false;
			this.extUsersTreeList.OptionsView.ShowRoot = false;
			this.extUsersTreeList.Size = new System.Drawing.Size(584, 238);
			this.extUsersTreeList.TabIndex = 9;
			// 
			// emailCol
			// 
			this.emailCol.Caption = "External User E-mail";
			this.emailCol.FieldName = "UserEmail";
			this.emailCol.Name = "emailCol";
			this.emailCol.OptionsColumn.ReadOnly = true;
			this.emailCol.Visible = true;
			this.emailCol.VisibleIndex = 0;
			// 
			// addressCol
			// 
			this.addressCol.Caption = "Address/Phone";
			this.addressCol.FieldName = "AddressPhone";
			this.addressCol.Name = "addressCol";
			this.addressCol.OptionsColumn.ReadOnly = true;
			this.addressCol.Visible = true;
			this.addressCol.VisibleIndex = 1;
			// 
			// protocolCol
			// 
			this.protocolCol.Caption = "Protocol";
			this.protocolCol.FieldName = "ProtocolName";
			this.protocolCol.Name = "protocolCol";
			this.protocolCol.OptionsColumn.ReadOnly = true;
			this.protocolCol.Visible = true;
			this.protocolCol.VisibleIndex = 2;
			// 
			// mcuCol
			// 
			this.mcuCol.Caption = "MCU";
			this.mcuCol.FieldName = "MCUName";
			this.mcuCol.Name = "mcuCol";
			this.mcuCol.OptionsColumn.ReadOnly = true;
			this.mcuCol.Visible = true;
			this.mcuCol.VisibleIndex = 3;
			// 
			// callerColumn
			// 
			this.callerColumn.Caption = "Caller/Callee";
			this.callerColumn.FieldName = "Caller/Callee";
			this.callerColumn.Name = "callerColumn";
			this.callerColumn.OptionsColumn.ReadOnly = true;
			this.callerColumn.Visible = true;
			this.callerColumn.VisibleIndex = 4;
			// 
			// bnAddRoom
			// 
			this.bnAddRoom.Location = new System.Drawing.Point(591, 21);
			this.bnAddRoom.Name = "bnAddRoom";
			this.bnAddRoom.Size = new System.Drawing.Size(124, 28);
			this.bnAddRoom.StyleController = this.layoutControl1;
			this.bnAddRoom.TabIndex = 12;
			this.bnAddRoom.Text = "Add External Room";
			this.bnAddRoom.Click += new System.EventHandler(this.bnAddRoom_Click);
			// 
			// bnRemoveRoom
			// 
			this.bnRemoveRoom.Location = new System.Drawing.Point(591, 101);
			this.bnRemoveRoom.Name = "bnRemoveRoom";
			this.bnRemoveRoom.Size = new System.Drawing.Size(124, 28);
			this.bnRemoveRoom.StyleController = this.layoutControl1;
			this.bnRemoveRoom.TabIndex = 10;
			this.bnRemoveRoom.Text = "Remove Selected Room";
			this.bnRemoveRoom.Click += new System.EventHandler(this.bnRemoveRoom_Click);
			// 
			// layoutControlGroup1
			// 
			this.layoutControlGroup1.CustomizationFormText = "Root";
			this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
			this.layoutControlGroup1.GroupBordersVisible = false;
			this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem3,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.emptySpaceItem4,
            this.emptySpaceItem6,
            this.emptySpaceItem5});
			this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup1.Name = "Root";
			this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup1.Size = new System.Drawing.Size(722, 238);
			this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup1.Text = "Root";
			this.layoutControlGroup1.TextVisible = false;
			// 
			// emptySpaceItem3
			// 
			this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
			this.emptySpaceItem3.Location = new System.Drawing.Point(589, 0);
			this.emptySpaceItem3.MinSize = new System.Drawing.Size(1, 1);
			this.emptySpaceItem3.Name = "emptySpaceItem3";
			this.emptySpaceItem3.Size = new System.Drawing.Size(128, 19);
			this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.emptySpaceItem3.Text = "emptySpaceItem3";
			this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
			// 
			// emptySpaceItem1
			// 
			this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
			this.emptySpaceItem1.Location = new System.Drawing.Point(584, 0);
			this.emptySpaceItem1.MaxSize = new System.Drawing.Size(10, 0);
			this.emptySpaceItem1.MinSize = new System.Drawing.Size(5, 10);
			this.emptySpaceItem1.Name = "emptySpaceItem1";
			this.emptySpaceItem1.Size = new System.Drawing.Size(5, 238);
			this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.emptySpaceItem1.Text = "emptySpaceItem1";
			this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
			// 
			// emptySpaceItem2
			// 
			this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
			this.emptySpaceItem2.Location = new System.Drawing.Point(717, 0);
			this.emptySpaceItem2.MaxSize = new System.Drawing.Size(10, 0);
			this.emptySpaceItem2.MinSize = new System.Drawing.Size(5, 10);
			this.emptySpaceItem2.Name = "emptySpaceItem2";
			this.emptySpaceItem2.Size = new System.Drawing.Size(5, 238);
			this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.emptySpaceItem2.Text = "emptySpaceItem2";
			this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
			// 
			// layoutControlItem4
			// 
			this.layoutControlItem4.Control = this.extUsersTreeList;
			this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
			this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem4.MinSize = new System.Drawing.Size(201, 24);
			this.layoutControlItem4.Name = "layoutControlItem4";
			this.layoutControlItem4.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlItem4.Size = new System.Drawing.Size(584, 238);
			this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem4.Text = "layoutControlItem4";
			this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem4.TextToControlDistance = 0;
			this.layoutControlItem4.TextVisible = false;
			// 
			// layoutControlItem5
			// 
			this.layoutControlItem5.Control = this.bnRemoveRoom;
			this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
			this.layoutControlItem5.Location = new System.Drawing.Point(589, 99);
			this.layoutControlItem5.MaxSize = new System.Drawing.Size(150, 32);
			this.layoutControlItem5.MinSize = new System.Drawing.Size(107, 32);
			this.layoutControlItem5.Name = "layoutControlItem5";
			this.layoutControlItem5.Size = new System.Drawing.Size(128, 32);
			this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem5.Text = "layoutControlItem5";
			this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem5.TextToControlDistance = 0;
			this.layoutControlItem5.TextVisible = false;
			// 
			// layoutControlItem6
			// 
			this.layoutControlItem6.Control = this.bnEditRoom;
			this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
			this.layoutControlItem6.Location = new System.Drawing.Point(589, 59);
			this.layoutControlItem6.MaxSize = new System.Drawing.Size(150, 32);
			this.layoutControlItem6.MinSize = new System.Drawing.Size(107, 32);
			this.layoutControlItem6.Name = "layoutControlItem6";
			this.layoutControlItem6.Size = new System.Drawing.Size(128, 32);
			this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem6.Text = "layoutControlItem6";
			this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem6.TextToControlDistance = 0;
			this.layoutControlItem6.TextVisible = false;
			// 
			// layoutControlItem7
			// 
			this.layoutControlItem7.Control = this.bnAddRoom;
			this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
			this.layoutControlItem7.Location = new System.Drawing.Point(589, 19);
			this.layoutControlItem7.MaxSize = new System.Drawing.Size(150, 32);
			this.layoutControlItem7.MinSize = new System.Drawing.Size(128, 32);
			this.layoutControlItem7.Name = "layoutControlItem7";
			this.layoutControlItem7.Size = new System.Drawing.Size(128, 32);
			this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem7.Text = "layoutControlItem7";
			this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem7.TextToControlDistance = 0;
			this.layoutControlItem7.TextVisible = false;
			// 
			// emptySpaceItem4
			// 
			this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
			this.emptySpaceItem4.Location = new System.Drawing.Point(589, 131);
			this.emptySpaceItem4.MaxSize = new System.Drawing.Size(160, 0);
			this.emptySpaceItem4.MinSize = new System.Drawing.Size(104, 24);
			this.emptySpaceItem4.Name = "emptySpaceItem4";
			this.emptySpaceItem4.Size = new System.Drawing.Size(128, 107);
			this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.emptySpaceItem4.Text = "emptySpaceItem4";
			this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
			// 
			// emptySpaceItem6
			// 
			this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
			this.emptySpaceItem6.Location = new System.Drawing.Point(589, 91);
			this.emptySpaceItem6.MinSize = new System.Drawing.Size(1, 1);
			this.emptySpaceItem6.Name = "emptySpaceItem6";
			this.emptySpaceItem6.Size = new System.Drawing.Size(128, 8);
			this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.emptySpaceItem6.Text = "emptySpaceItem6";
			this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
			// 
			// emptySpaceItem5
			// 
			this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
			this.emptySpaceItem5.Location = new System.Drawing.Point(589, 51);
			this.emptySpaceItem5.MinSize = new System.Drawing.Size(1, 1);
			this.emptySpaceItem5.Name = "emptySpaceItem5";
			this.emptySpaceItem5.Size = new System.Drawing.Size(128, 8);
			this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.emptySpaceItem5.Text = "emptySpaceItem5";
			this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
			// 
			// ExternalRoomsControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.layoutControl1);
			this.Margin = new System.Windows.Forms.Padding(0);
			this.Name = "ExternalRoomsControl";
			this.Size = new System.Drawing.Size(722, 238);
			this.Load += new System.EventHandler(this.ExternalRoomsControl_Load);
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
			this.layoutControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.extUsersTreeList)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private DevExpress.XtraLayout.LayoutControl layoutControl1;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
		private DevExpress.XtraTreeList.TreeList extUsersTreeList;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
		private DevExpress.XtraTreeList.Columns.TreeListColumn emailCol;
		private DevExpress.XtraTreeList.Columns.TreeListColumn addressCol;
		private DevExpress.XtraTreeList.Columns.TreeListColumn protocolCol;
		private DevExpress.XtraTreeList.Columns.TreeListColumn mcuCol;
		private DevExpress.XtraEditors.SimpleButton bnRemoveRoom;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
		private DevExpress.XtraEditors.SimpleButton bnEditRoom;
		private DevExpress.XtraEditors.SimpleButton bnAddRoom;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
		private DevExpress.XtraTreeList.Columns.TreeListColumn callerColumn;
	}
}
