using System;
using System.ComponentModel;

namespace MyVrm.DataImport.Excel.WinForms
{
    /// <summary>
    /// Displays a single column header in a ListView control.
    /// </summary>
    [DefaultProperty("Text")]
    public class ColumnHeader : System.Windows.Forms.ColumnHeader
    {
        private string displayMember = null;
        private string format;
        private IFormatProvider formatInfo;
        private string nullText;
        private bool isSortable = true;

        /// <summary>
        /// Initializes a new instance of the ColumnHeader class.
        /// </summary>
        public ColumnHeader()
        {
            nullText = Strings.ListViewNullText;
        }

        /// <summary>
        /// Creates an identical copy of the current ColumnHeader that is not attached to any list view control.
        /// </summary>
        /// <returns>An object representing a copy of this ColumnHeader object.</returns>
        public new object Clone()
        {
            ColumnHeader clone = (ColumnHeader) base.Clone();
            clone.DisplayMember = DisplayMember;
            clone.Format = Format;
            clone.FormatInfo = FormatInfo;
            return clone;
        }

        /// <summary>
        /// Gets or sets a string that specifies the property of the data source whose contents you want to display.
        /// </summary>
        [Category("Data")]
        public string DisplayMember
        {
            get { return displayMember; }
            set
            {
                displayMember = value;
                // notify ListView that DisplayMember has been changed
                if (ListView != null)
                {
                }
            }
        }

        /// <summary>
        /// Gets the ListView control the ColumnHeader is located in.
        /// </summary>
        /// <value>A ListView control that represents the control that contains the ColumnHeader.</value>
        [Browsable(false)]
        public new DataListView ListView
        {
            get
            {
                if (base.ListView != null)
                {
                    if (base.ListView is DataListView)
                    {
                        return (DataListView) base.ListView;
                    }
                    else
                    {
                        throw new InvalidCastException();
                    }
                }
                return null;
            }
        }

        /// <summary>
        /// Gets or sets the character(s) that specify how text is formatted.
        /// </summary>
        /// <value>The character or characters that specify how text is formatted.</value>
        public string Format
        {
            get { return format; }
            set { format = value; }
        }

        /// <summary>
        /// Gets or sets the culture specific information used to determine how values are formatted.
        /// </summary>
        /// <value>An object that implements the IFormatProvider interface, such as the CultureInfo class.</value>
        [Browsable(false)]
        public IFormatProvider FormatInfo
        {
            get { return formatInfo; }
            set
            {
                if (formatInfo == null || !(formatInfo.Equals(value)))
                {
                    formatInfo = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the text that is displayed when the column contains a null reference (Nothing in Visual Basic).
        /// </summary>
        /// <value>A string displayed in a column containing a DBNull.Value.</value>
        [Browsable(false)]
        public string NullText
        {
            get { return nullText; }
            set
            {
                if (nullText != null && nullText == value)
                {
                    return;
                }
                nullText = value;
            }
        }
        [DefaultValue(true)]
        public bool IsSortable
        {
            get { return isSortable; }
            set { isSortable = value; }
        }

        internal string GetDisplayText(object value)
        {
            return GetText(value);
        }

        private string GetText(object value)
        {
            string text;
            if (value == null)
            {
                return String.Empty;
            }
            if (value as DBNull != null)
            {
                return nullText;
            }
            if (format != null && format.Length > 0 && value as IFormattable != null)
            {
                try
                {
                    text = ((IFormattable) value).ToString(format, formatInfo);
                    return text;
                }
                catch (Exception)
                {
                }
            }
            return value.ToString();
        }
    }
}