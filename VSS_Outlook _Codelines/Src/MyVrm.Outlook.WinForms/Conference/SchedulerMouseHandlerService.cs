﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using DevExpress.Services;

namespace MyVrm.Outlook.WinForms.Conference
{
    internal class SchedulerMouseHandlerService : MouseHandlerServiceWrapper
    {
        public SchedulerMouseHandlerService(IMouseHandlerService service) : base(service)
        {
        }

        public override void OnMouseDown(System.Windows.Forms.MouseEventArgs e)
        {
            base.OnMouseDown(e);
        }
    }
}
