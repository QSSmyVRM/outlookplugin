﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Services.Description;

namespace MyVrm.WebServices.Data
{
	public class ConfInfo : ComplexProperty
	{
		public string ConferenceId; //594,1

		private int _conferenceUniqueId; //8572
		public int ConferenceUniqueId { get { return _conferenceUniqueId; } set { _conferenceUniqueId = value; } }

		private string _conferenceName;
		public string ConferenceName { get { return _conferenceName; } set { _conferenceName = value; } }

		private ConfType _type;
		public ConfType Type { get { return _type; } set { _type = value; } }

		private DateTime _conferenceDateTime; //6/29/2011 8:00 AM
		public DateTime ConferenceDateTime { get { return _conferenceDateTime; } set { _conferenceDateTime = value; } }

		private TimeSpan _conferenceDuration; //60
		public TimeSpan ConferenceDuration { get { return _conferenceDuration; } set { _conferenceDuration = value; } }

		public TConferenceStatus ConferenceStatus; //1
		public TConferenceStatus ConferenceActualStatus; //1
		public bool OpenForRegistration; //0 //Register Open (Public)
		//public DateTime SetupTime; //6/29/2011 8:00 AM
		//public DateTime TearDownTime; //6/29/2011 9:00 AM
		//public SetupDur>-241</SetupDur>
		//public TearDownDur>241</TearDownDur>
		//public string ConferenceHost; //VRM Administrator
		//public ConferenceHostEmail>admin@myvrm.com</ConferenceHostEmail>
		//public ConfDescription>N/A</ConfDescription>
		//public WebsiteURL>http://localhost/myvrm</WebsiteURL>
		//public IsHost>1</IsHost>
		//public IsParticipant>0</IsParticipant>
		public bool IsRecur; //0
		public List<SelectedRoom> Rooms = new List<SelectedRoom>(); //???

		/* <Selected>
		   public ID>12</ID>
		   public Name>Dans Conference Room</Name>
		 public /Selected>*/
		private ApprovedEntry _approvedEntity = new ApprovedEntry();
		public ApprovedEntry ApprovedEntity { get { return _approvedEntity; } set { _approvedEntity = value; } }

		public override object Clone()
		{
			ConfInfo copy = new ConfInfo();
			copy.ConferenceId = string.Copy(ConferenceId);
			copy._conferenceName = string.Copy(_conferenceName);
			copy._approvedEntity = (ApprovedEntry)_approvedEntity.Clone();
			copy._conferenceDateTime = new DateTime(_conferenceDateTime.Ticks);
			copy._conferenceDuration = new TimeSpan(_conferenceDuration.Ticks);
			copy._conferenceUniqueId = _conferenceUniqueId;
			copy._type = _type;
			copy.ConferenceStatus = ConferenceStatus;
			copy.IsRecur = IsRecur;
			copy.OpenForRegistration = OpenForRegistration;
			copy.Rooms = new List<SelectedRoom>(Rooms);

			return copy;
		}

		//private List<ApprovedEntry> _approvedEntries = new List<ApprovedEntry>();
		//public Level ApproveLevel; //assume there can be only one approve level at a time
		//public List<ApprovedEntry> _approvedEntries = new List<ApprovedEntry>();
		//- <ApprovalPending>
		//    - <Entities>
		//        <Level>1</Level> 
		//        - <Entity>
		//            <Name>Room 294</Name> 
		//            <ID>16</ID> 
		//            <Message /> 
		//        </Entity>
		//        - <Entity>
		//            <Name>Room 268</Name> 
		//            <ID>17</ID> 
		//            <Message /> 
		//        </Entity>
		//    </Entities>
		//</ApprovalPending>

		//public LastRunDate>

		internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
		{
			bool bRet = false;
			switch (reader.LocalName)
			{
				case "ConferenceID":
					{
						ConferenceId = reader.ReadElementValue();
						bRet = true;
						break;
					}
				case "ConferenceUniqueID":
					{
						_conferenceUniqueId = reader.ReadElementValue<int>();
						bRet = true;
						break;
					}
				case "ConferenceName":
					{
						_conferenceName = reader.ReadElementValue();
						bRet = true;
						break;
					}
				case "ConferenceType":
					{
						_type = (ConfType)(reader.ReadElementValue<int>());
						bRet = true;
						break;
					}
				case "ConferenceDateTime":
					{
						_conferenceDateTime = Utilities.StringToDateTime(reader.ReadElementValue());//, MyVrmService.Service.IsUtcEnabled);
						//DateTime.TryParseExact( reader.ReadElementValue(), ;
						bRet = true;
						break;
					}
				case "ConferenceDuration":
					{
						_conferenceDuration = new TimeSpan(0, reader.ReadElementValue<int>(), 0);
						bRet = true;
						break;
					}
				case "ConferenceStatus":
					{
						ConferenceStatus = (TConferenceStatus)reader.ReadElementValue<int>();
						bRet = true;
						break;
					}
				case "ConferenceActualStatus":
					{
						ConferenceActualStatus = (TConferenceStatus)reader.ReadElementValue<int>();
						bRet = true;
						break;
					}
				case "OpenForRegistration":
					{
						OpenForRegistration = reader.ReadElementValue<bool>();
						bRet = true;
						break;
					}
				case "IsRecur":
					{
						IsRecur = reader.ReadElementValue<bool>();
						bRet = true;
						break;
					}
				case "Location"://"":
					{
						while (!reader.IsEndElement(XmlNamespace.NotSpecified, "Location"))
						{
							reader.Read();
							while (reader.LocalName == "Selected" && !reader.IsEndElement(XmlNamespace.NotSpecified, "Selected"))
							{
								int id = 0;
								string name = string.Empty;

								reader.Read();
								if (reader.LocalName == "ID")
								{
									id = reader.ReadElementValue<int>();
								}

								reader.Read();
								if (reader.LocalName == "Name")
									name = reader.ReadElementValue();

								if (id != 0)
								{
									Rooms.Add(new SelectedRoom() { Id = id, Name = name });
								}
								reader.Read();
							}
						}
						bRet = true;
						break;
					}
				case "ApprovalPending":
					while (!reader.IsEndElement(XmlNamespace.NotSpecified, "ApprovalPending"))
					{
						reader.Read();
						if (reader.IsStartElement(XmlNamespace.NotSpecified, "Entities"))
						{
							_approvedEntity.LoadFromXml(reader);
						}
					}
					bRet = true;
					break;
				default:
					break;
			}
			reader.Read();
			return bRet;
		}
	}

	public enum InstanceType
	{
		Regular,
		RecurrenceMain,
		RecurrenceMainExpandable,
		RecurrenceOccurence
	}
	public struct TransformedConfInfo
	{
		public ApprovalUnit AppUnit { get; set; }
		public ConfInfo ConfInfo { get; set; }
		public string ConfTypeName { get; set; }
		public int ParentID { get; set; }
		public int KeyID { get; set; }
		public InstanceType InstanceType { get; set; }
	}
}
