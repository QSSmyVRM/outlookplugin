﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;

namespace MyVrm.Outlook.WinForms.Conference
{
    class CustomDevExpressTreeList : TreeList
    {
        public CustomDevExpressTreeList() : base() { }
        public CustomDevExpressTreeList(object ignore) : base(ignore) { }
        private bool allowMakeNodeVisible = true;
        protected override void OnStartSorting()
        {
            base.OnStartSorting();
            allowMakeNodeVisible = false;
        }
        protected override void OnEndSorting()
        {
            base.OnEndSorting();
            allowMakeNodeVisible = true;
        }
        public override int MakeNodeVisible(TreeListNode node)
        {
            if (!allowMakeNodeVisible) return -1;
            return base.MakeNodeVisible(node);
        }
    }
}
