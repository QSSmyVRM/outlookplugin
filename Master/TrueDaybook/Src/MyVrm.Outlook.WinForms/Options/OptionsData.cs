﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.Outlook.WinForms.Options
{
    class ConferenceRoomsList
    {
        private string id;

        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        public virtual string DisplayMember {get; set; }

        public override string ToString()
        {
            return this.DisplayMember;
        }

    }

    class ConferenceParticipants
    {

        public ConferenceParticipants(string firstName, string lastName, string email, int externalAttendee, int roomAttendee, int cc, bool bNotify, int video, int audio)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.email = email;
            this.ExternalAttendee = externalAttendee;
            this.RoomAttendee = roomAttendee;
            this.CC = cc;
            this.Notify = bNotify;
            this.Video = video;
            this.Audio = audio;
        }
        private string firstName;

        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }

        private string lastName;

        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        private string email;

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        private int externalAttendee =1;

        public int ExternalAttendee
        {
            get { return externalAttendee; }
            set { externalAttendee = value; }
        }
        private int roomAttendee=0;

        public int RoomAttendee
        {
            get { return roomAttendee; }
            set { roomAttendee = value; }
        }

        private int _cc = 0;

        public int CC
        {
            get { return _cc; }
            set { _cc = value; }
        }

        private bool _notify = false;

        public bool Notify
        {
            get { return _notify; }
            set { _notify = value; }
        }

        private int _video = 1;

        public int Video
        {
            get { return _video; }
            set { _video = value; }
        }

        private int audio = 0;

        public int Audio
        {
            get { return audio; }
            set { audio = value; }
        }

        private bool readOnly= false;

        public bool ReadOnly
        {
            get { return readOnly; }
            set { readOnly = value; }
        }


    }
}
