﻿using System;
using System.Collections.Generic;
using Microsoft.Office.Interop.Outlook;
using MyVrm.Common.Extensions;
using MyVrm.MAPI;

namespace MyVrm.Outlook
{
    public partial class OutlookAppointmentImpl
    {
        public override TimeZoneInfo StartTimeZone
        {
            get
            {
                return TimeZoneInfo.Local;
            }
            set
            {
            }
        }

        public override TimeZoneInfo EndTimeZone
        {
            get
            {
                return TimeZoneInfo.Local;
            }
            set
            {
            }
        }

        public override DateTime StartInStartTimeZone
        {
            get { return Item.Start; }
            set
            {
                if (value != Item.Start)
                {
                    Item.Start = value;
                }
            }
        }

        public override DateTime EndInEndTimeZone
        {
            get { return Item.End; }
            set
            {
                if (value != Item.End)
                {
                    Item.End = value;
                }
            }
        }

        public override string GlobalAppointmentId
        {
            get
            {
                using(var mapiObject = new MapiObject(Item.MAPIOBJECT))
                {
                    var globalObjectId = (byte[]) mapiObject.GetProperty(NamedProperty.PidLidGlobalObjectId);
                    return globalObjectId == null ? null : globalObjectId.ToHexString();
                }
            }
        }

        public override void RemoveRecipient(string email)
        {
            var idx = 0;
            for (var i = 1; i <= Item.Recipients.Count; i++)
            {
                var recipient = Item.Recipients[i];
                string smtpAddress = null;
                using (var mapiObject = new MapiObject(recipient.AddressEntry.MAPIOBJECT))
                {
                    var propValue = mapiObject.GetProperty(MapiTags.PidTagPrimarySmtpAddress);
                    if (propValue != null)
                        smtpAddress = propValue.ToString();
                }
                smtpAddress = smtpAddress ??  recipient.Address;
                if (email.Equals(smtpAddress, StringComparison.InvariantCultureIgnoreCase))
                {
                    idx = i;
                    break;
                }
            }
            if (idx > 0)
            {
                Item.Recipients.Remove(idx);
            }
        }

        partial void ItemPropertyChanged(string name)
        {
            if (name == "Resources")
            {
                NotifyPropertyChanged("Recipients");
            }
            else if (name == "Start")
            {
                NotifyPropertyChanged("StartInStartTimeZone");
            }
            else if (name == "End")
            {
                NotifyPropertyChanged("EndInEndTimeZone");
            }
            else
            {
                NotifyPropertyChanged(name);
            }
        }

        /// <summary>
        /// Removes custom form definition (one-off). See http://support.microsoft.com/kb/928908
        /// </summary>
        /// <param name="removePropDef"></param>
        partial void RemoveOneOff(bool removePropDef)
        {
            var namedProperties = new List<NamedProperty>
                                      {
                                                          NamedProperty.PidLidFormStorage,
                                                          NamedProperty.PidLidPageDirStream,
                                                          NamedProperty.PidLidFormPropStream,
                                                      };
            if (removePropDef)
                namedProperties.Add(NamedProperty.PidLidPropDefStream);

            using (var mapiObject = new MapiObject(Item.MAPIOBJECT))
            {
                mapiObject.DeleteProps(mapiObject.GetIDsFromNames(namedProperties.ToArray()));
                var customFlagTagId = mapiObject.GetIDFromName(NamedProperty.PidLidCustomFlag);
                var flagsProperty = mapiObject.GetProperty((MapiTags)customFlagTagId);
                if (flagsProperty != null)
                {
                    var flags = (CustomFlagPropertyFlags)flagsProperty;
                    flags &= ~CustomFlagPropertyFlags.OneOff;
                    if (removePropDef)
                    {
                        flags &= ~CustomFlagPropertyFlags.PropertyDefinition;
                    }
                    mapiObject.SetProps(new[] { new MapiValue((MapiTags)customFlagTagId, flags) });
                }
                mapiObject.SaveChanges();
            }
        }
    }
}