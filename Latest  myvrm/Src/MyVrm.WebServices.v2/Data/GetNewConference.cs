﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using MyVrm.Common.ComponentModel;

namespace MyVrm.WebServices.Data
{
	internal class GetNewConferenceRequest : ServiceRequestBase<GetNewConferenceResponse>
	{
		public GetNewConferenceRequest(MyVrmService service)
			: base(service)
		{
		}

		#region Overrides of ServiceRequestBase

		internal override string GetXmlElementName()
		{
			return "login";
		}

		internal override string GetCommandName()
		{
			return "GetNewConference";
		}

		internal override string GetResponseXmlElementName()
		{
			return "conference";
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			UserId.WriteToXml(writer, "userID");
			writer.WriteElementValue(XmlNamespace.NotSpecified, "utcEnabled", MyVrmService.Service.UtcEnabled);
		}

		#endregion
	}
	//
	public class GetNewConferenceResponse : ServiceResponse
	{
		public bool Secured { get; set; }
        public string internalBridge { get; set; }
        public string externalBridge { get; set; }
        public string PrivateVMR { get; set; }
		private readonly ConfMessageCollection _confMsgs = new ConfMessageCollection();
        public int GetGuestUserLiscense { get; set; } //101343
		public ConfMessageCollection ConfMessages
		{
			get
			{
				//ConfMessageCollection ret = new ConfMessageCollection();
				//_confMsgs = (ConfMessageCollection) ret.Clone(); 
				//return ret;
				return _confMsgs;
			}
		}

		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{
			reader.Read();
			while (!reader.IsStartElement(XmlNamespace.NotSpecified, "userInfo"))
				reader.SkipCurrentElement();
			if (reader.IsStartElement(XmlNamespace.NotSpecified, "userInfo"))
			{
				do
				{
					reader.Read();
					if (reader.LocalName == "Secured")
					{
						Secured = reader.ReadElementValue<bool>();
					}
                    else if (reader.LocalName == "internalBridge")
                    {
                        internalBridge = reader.ReadElementValue<string>();
                    }
                    else if (reader.LocalName == "externalBridge")
                    {
                        externalBridge = reader.ReadElementValue<string>();
                    }
                    else if (reader.LocalName == "PrivateVMR")
                    {
                       PrivateVMR = reader.ReadElementValue();
                       //Z XD 103387 start
                        //if (!String.IsNullOrEmpty(PrivateVMR) && (PrivateVMR.Contains("õ") || 
                        //    PrivateVMR.Contains("æ") || PrivateVMR.Contains("σ")))
                        //    PrivateVMR = PrivateVMR.Replace("õ", "<").Replace("æ", ">").Replace("σ", "&");    
                       PrivateVMR = Utilities.ReplaceOutXMLSpecialCharacters(PrivateVMR); //ZD 104771
                        //ZD 103387 End
                    }

                    else if (reader.LocalName == "GuestRoomPerUser")//zd 101343
                    {
                        GetGuestUserLiscense = reader.ReadElementValue<int>();
                    }
                    else
                        reader.SkipCurrentElement();
				} while (!reader.IsEndElement(XmlNamespace.NotSpecified, "userInfo"));
			}

			while (!reader.IsStartElement(XmlNamespace.NotSpecified, "confInfo"))
				reader.Read(); 

			if (reader.IsStartElement(XmlNamespace.NotSpecified, "confInfo"))
			{
				while (!reader.IsStartElement(XmlNamespace.NotSpecified, "ConfMessageList"))
				{
					reader.Read();
				}

				_confMsgs.Items.Clear();
				do
				{
					reader.Read();
					if (reader.IsStartElement(XmlNamespace.NotSpecified, "ConfMessage"))
					{
						var msg = new ConfMessage();
						msg.LoadFromXml(reader, "ConfMessage");
						_confMsgs.Add(msg);
					}
					else
					{
						reader.SkipCurrentElement();
					}
				} while (!reader.IsEndElement(XmlNamespace.NotSpecified, "ConfMessageList"));
			}

			while (!reader.IsEndElement(XmlNamespace.NotSpecified, "conference"))
			{
				reader.Read();
			}
		}
	}
}
