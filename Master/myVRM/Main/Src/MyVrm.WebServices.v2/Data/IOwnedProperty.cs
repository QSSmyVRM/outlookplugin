﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    /// <summary>
    /// Complex properties that implement that interface are owned by an instance of <see cref="ServiceObject"/>.
    /// </summary>
    internal interface IOwnedProperty
    {
        /// <summary>
        /// Gets or sets the owner.
        /// </summary>
        ServiceObject Owner { get; set; }
    }
}
