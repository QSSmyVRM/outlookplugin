/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace MyVrm.Common.Collections.Generic
{
    /// <summary>
    /// Provides a generic _property comparer
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class PropertyComparer<T> : Comparer<T>
    {
        private readonly PropertyDescriptor _property;
        private readonly IComparer _propertyComparer;
        private readonly ListSortDirection _sortDirection;

        /// <summary>
        /// Initializes a new instance of the PropertyComparer
        /// </summary>
        /// <param name="property">The <see cref="PropertyDescriptor"/> to compare</param>
        /// <param name="direction">One of the <see cref="ListSortDirection"/> values</param>
        public PropertyComparer(PropertyDescriptor property, ListSortDirection direction)
            : this(property, direction, StringComparer.OrdinalIgnoreCase)
        {
        }

        /// <summary>
        /// Initializes a new instance of the PropertyComparer
        /// </summary>
        /// <param name="property">The <see cref="PropertyDescriptor"/> to compare</param>
        /// <param name="direction">One of the <see cref="ListSortDirection"/> values</param>
        /// <param name="propertyComparer">Property comparer</param>
        public PropertyComparer(PropertyDescriptor property, ListSortDirection direction, IComparer propertyComparer)
        {
            if (property == null)
            {
                throw new ArgumentNullException("property");
            }
            if (propertyComparer == null)
            {
                throw new ArgumentNullException("propertyComparer");
            }
            _property = property;
            _sortDirection = direction;
            _propertyComparer = propertyComparer;
        }

        /// <summary>
        /// Performs comparison of two objects of the same type and returns a value indicating whether one is less than, equal to, or greater than the other
        /// </summary>
        /// <param name="x">The first object to compare.</param>
        /// <param name="y">The second object to compare. </param>
        /// <returns></returns>
        public override int Compare(T x, T y)
        {
            object obj2 = _property.GetValue(x);
            object obj3 = _property.GetValue(y);
            int num = _propertyComparer.Compare(obj2, obj3);
            if (_sortDirection == ListSortDirection.Descending)
            {
                num = -num;
            }
            return num;
        }
    }
}