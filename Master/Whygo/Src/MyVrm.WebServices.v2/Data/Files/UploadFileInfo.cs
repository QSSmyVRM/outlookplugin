﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.WebServices.Data.Files
{
    /// <summary>
    /// Contains information about file to be uploaded
    /// </summary>
    public class UploadFileInfo
    {
        public UploadFileInfo(string name, byte[] data)
        {
            Name = name;
            Data = data;
        }
        /// <summary>
        /// Gets or sets file 
        /// </summary>
        public string Name { get; private set; }
        public string RemotePath { get; internal set; }
        public byte[] Data { get; private set; }
    }
}