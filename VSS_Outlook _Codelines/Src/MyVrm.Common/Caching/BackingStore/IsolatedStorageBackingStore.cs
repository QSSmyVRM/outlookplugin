﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace MyVrm.Common.Caching.BackingStore
{
    public class IsolatedStorageBackingStore : IBackingStore, IDisposable
    {
        private readonly Dictionary<string, object> _items = new Dictionary<string, object>(100);

        #region Implementation of IBackingStore

        public void Add(CacheItem cacheItem)
        {
            _items.Add(cacheItem.Key, cacheItem.Value);
        }

        public void Flush()
        {
            throw new NotImplementedException();
        }

        public Dictionary<string, object> Load()
        {
            _items.Clear();
            Dictionary<string, object> itemsToLoad = null;
            using(var storageFile = IsolatedStorageFile.GetUserStoreForAssembly())
            {
                storageFile.CreateDirectory("myVRM");
                IFormatter formatter = new BinaryFormatter();
                using (var stream = new IsolatedStorageFileStream("myVRM/cache", FileMode.OpenOrCreate, FileAccess.Read, storageFile))
                {
                    try
                    {
                        itemsToLoad = (Dictionary<string, object>)formatter.Deserialize(stream);
                    }
                    catch (Exception)
                    {
                    
                    }
                }
                
            }
            if (itemsToLoad != null)
            {
                foreach (var cacheItem in itemsToLoad)
                {
                    _items.Add(cacheItem.Key, cacheItem.Value);
                }

            }
            return itemsToLoad;
        }

        public void Remove(string key)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Implementation of IDisposable

        public void Dispose()
        {
            using (var storageFile = IsolatedStorageFile.GetUserStoreForAssembly())
            {
                storageFile.CreateDirectory("myVRM");
                IFormatter formatter = new BinaryFormatter();
                using (
                    var stream = new IsolatedStorageFileStream("myVRM/cache", FileMode.OpenOrCreate, FileAccess.Write,
                                                               storageFile))
                {
                    formatter.Serialize(stream, _items);
                }
                _items.Clear();
            }
        }

        #endregion
    }
}
