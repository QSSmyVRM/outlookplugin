﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using MyVrm.Common.ComponentModel;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook.WinForms.Calendar
{
	public partial class CalendarForSelectedRooms : RoomsCalendarDialog
	{
		private readonly List<RoomId> _roomList = new List<RoomId>();

		public CalendarForSelectedRooms(List<RoomId> roomList)
		{
			InitializeComponent();
			_roomList = roomList;
		}

		private void CalendarForSelectedRooms_Load(object sender, EventArgs e)
		{
			List<ManagedRoom> allRooms = new List<ManagedRoom>();
			allRooms.AddRange(Rooms);
			
			Rooms.Clear();

			foreach (var room in _roomList)
			{
				Rooms.Add(allRooms.Find(_a_room => _a_room.Id.Id == room.Id));
			}

			RoomListControl.NodesIterator.DoOperation(node => { node.Checked = true ; });
			ExternalCalendarUpdate();
		}
	}
}
