﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	public class VConf
	{
		private string _id;
		public string Id 
		{ 
			get { return _id; }  
			set { _id = value; }
		}

		private DateTime _start;
		public DateTime Start
		{
			set { _start = value; }
			get { return _start; }  
		}
		private DateTime _end;
		public DateTime End
		{
			set { _end = value; }
			get { return _end; }
		}
		private string _body;
		public  string Body
		{
			set { _body = value; }
			get { return _body; }
		}
		private string _subject;
		public string Subject
		{
			set { _subject = value; }
			get { return _subject; }
		}
		private List<string> _envitees;
		public List<string> Envitees
		{
			set { _envitees = value; }
			get { return _envitees; }
		}
	}
}
