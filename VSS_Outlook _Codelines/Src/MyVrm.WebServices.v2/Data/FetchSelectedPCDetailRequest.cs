﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;


namespace MyVrm.WebServices.Data
{
    internal class FetchSelectedPCDetailsRequest : ServiceRequestBase<FetchSelectedPCDetailsResponse>
    {
        public FetchSelectedPCDetailsRequest(MyVrmService service)
            : base(service)
        {
        }

        public ConferenceId contentid { get; set; }
        // public string ICalId { get; set; }

        public string subject { get; set; }
        public string placeholders { get; set; }
        public string emailtype { get; set; }
        public string emaillanguage { get; set; }
        public string body { get; set; }
        public string emailcontent { get; set; }
        public string PCType { get; set; }
        public int PCID { get; set; }

        //public string  { get; set; }

        #region Overrides of ServiceRequestBase<GetEmailContentResponse>

        internal override string GetXmlElementName()
        {
            return "FetchSelectedPCDetails";
        }

        internal override string GetCommandName()
        {
            return Constants.FetchSelectedPCDetails;
        }

        internal override string GetResponseXmlElementName()
        {
            return "PCDetails";
        }

        internal override FetchSelectedPCDetailsResponse ParseResponse(MyVrmServiceXmlReader reader)
        {
            var response = new FetchSelectedPCDetailsResponse();
            response.LoadFromXml(reader, GetResponseXmlElementName());
            return response;
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            writer.WriteStartElement(XmlNamespace.NotSpecified, "FetchPCDetails");
            UserId.WriteToXml(writer, "userID");
            MyVrmService.Service.OrganizationId.WriteToXml(writer, "organizationID");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "PCType", PCID);
        }

        #endregion
    }

    public class FetchSelectedPCDetailsResponse : ServiceResponse
    {
        public string PCDetail { get; internal set; }
        public string PCId { get; internal set; }
        public string Description { get; internal set; }
        public string SkypeURL { get; internal set; }
        public string VCDialinIP { get; internal set; }
        public string VCMeetingID { get; internal set; }
        public string VCDialinSIP { get; internal set; }
        public string VCDialinH323 { get; internal set; }
        public string VCPin { get; internal set; }
        public string PCDialinNum { get; internal set; }
        public string PCDialinFreeNum { get; internal set; }
        public string PCMeetingID { get; internal set; }
        public string PCPin { get; internal set; }
        public string Intructions { get; internal set; }
       


        //Read template info and template conference instance 
        internal void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
        {
            try
            {
                do
                {
                    reader.Read();
                    if (reader.LocalName != "" && reader.LocalName != null)
                    {
                        switch (reader.LocalName)
                        {

                            case "PCDetail":
                                PCDetail = reader.ReadElementValue();
                                break;
                           // case "PCId":
                              //  PCId = reader.ReadElementValue();
                               // break;
                            case "Description":
                                Description = reader.ReadElementValue();
                                break;
                            case "SkypeURL":
                                SkypeURL = reader.ReadElementValue();
                                break;
                            case "VCDialinIP":
                                VCDialinIP = reader.ReadElementValue();
                                break;
                            case "VCMeetingID":
                                VCMeetingID = reader.ReadElementValue();
                                break;
                            case "VCDialinSIP":
                                VCDialinSIP = reader.ReadElementValue();
                                break;
                            case "VCDialinH323":
                                VCDialinH323 = reader.ReadElementValue();
                                break;
                            case "VCPin":
                                VCPin = reader.ReadElementValue();
                                break;
                            case "PCDialinNum":
                                PCDialinNum = reader.ReadElementValue();
                                break;
                            case "PCDialinFreeNum":
                                PCDialinFreeNum = reader.ReadElementValue();
                                break;
                            case "PCMeetingID":
                                PCMeetingID = reader.ReadElementValue();
                                break;
                            case "PCPin":
                                PCPin = reader.ReadElementValue();
                                break;
                            case "Intructions":
                                Intructions = reader.ReadElementValue();
                                break;
                                
                            default:
                                reader.SkipCurrentElement();
                                break;
                        }
                    }
                } while (!reader.IsEndElement(XmlNamespace.NotSpecified, xmlElementName));
            }
            catch (Exception ex)
            {
                MyVrmService.TraceSource.TraceError(reader.LocalName + ": " + ex.Message, null);
                //reader.Read();
                //throw;
            }
        }

    }




}