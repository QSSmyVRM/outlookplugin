﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using DevExpress.XtraEditors.DXErrorProvider;
using MyVrm.Common.ComponentModel;
using MyVrm.Outlook.WinForms.Options;
using MyVrm.Outlook.WinForms.RecurrentConferences;
using MyVrm.WebServices.Data;
using Exception = System.Exception;
using RecurrencePattern = MyVrm.WebServices.Data.RecurrencePattern;

namespace MyVrm.Outlook.WinForms.Conference
{
	public delegate void AfterSaveEventHandler(object sender, EventArgs e);
	public delegate void ExternalUpdateEventHandler(object sender, EventArgs e);

    public class ConferenceWrapper : INotifyPropertyChanged, IDXDataErrorInfo
    {
        private readonly OutlookAppointment _appointment;

        private DataList<RoomId> _rooms;
        private DataList<Participant> _participants;

		//From York
		private ReadOnlyCollection<AudioUser> _audioUsers;
        private Collection<AudioConferenceBridge> _audioConferenceBridges;
        // Room endpoints
        private readonly DataList<ConferenceEndpoint> _roomEndpoints = new DataList<ConferenceEndpoint>();
        private CustomAttributeId _specialInstructionsAttrId;
        public TimeZoneInfo SeatsTimezone;
    	private string _initialBodyTxt;
    	public string InitialBodyTxt
    	{
			set { _initialBodyTxt = value; }
			get { return _initialBodyTxt; }
    	}
        public ConferenceWrapper(OutlookAppointment appointment, MyVrmService service)
        {
			InitialBodyTxt = string.Empty;
            if (appointment == null) 
                throw new ArgumentNullException("appointment");
            if (service == null)
                throw new ArgumentNullException("service");
            
            Conference = BindToConference(appointment, service);
            _appointment = appointment;
            _appointment.PropertyChanged += AppointmentPropertyChanged;
            _appointment.Saving += AppointmentSaving;
            _appointment.BeforeDelete += AppointmentBeforeDelete;
            _appointment.Sending += AppointmentSending;
            Initialize();
			
			//AudioConferenceBridges.Clear();
			//RoomEndpoints.Clear();
			//if (Conference.AdvancedAudioVideoSettings != null && Conference.AdvancedAudioVideoSettings.Endpoints != null)
			//{
			//    foreach (var conferenceEndpoint in Conference.AdvancedAudioVideoSettings.Endpoints)
			//    {
			//        switch (conferenceEndpoint.Type)
			//        {
			//            case ConferenceEndpointType.User:
			//                AudioConferenceBridges.Add(new AudioConferenceBridge(conferenceEndpoint));
			//                Participants.Remove(Participants.FirstOrDefault(party => party.UserId == conferenceEndpoint.Id));
			//                break;
			//            case ConferenceEndpointType.Room:
			//                RoomEndpoints.Add(conferenceEndpoint);
			//                break;
			//        }
			//    }
			//}
            //Initialize();
        }

        public ConferenceWrapper(MyVrmService service)
        {
            InitialBodyTxt = string.Empty;
            Conference = new MyVrm.WebServices.Data.Conference(service);
            
        }

		public IEnumerable<AudioUser> AudioUsers
		{
			get
			{
				return _audioUsers ?? (_audioUsers = MyVrmService.Service.GetAudioUsers());
			}
		}

    	
    	public Collection<AudioConferenceBridge> AudioConferenceBridges
    	{
    		get { return _audioConferenceBridges ?? (_audioConferenceBridges = new Collection<AudioConferenceBridge>()); }
    	}

		public static WebServices.Data.Conference BindToConference(OutlookAppointment appointment, MyVrmService service)
        {
			WebServices.Data.Conference conference;
            if (appointment.LinkedWithConference)
            {
                switch(appointment.RecurrenceState)
                {   
                    case RecurrenceState.NotRecurring:
                    case RecurrenceState.Master:
                    {
                        conference = WebServices.Data.Conference.Bind(MyVrmService.Service,
                                                                      appointment.ConferenceId);
                        break;
                    }
                    case RecurrenceState.Occurrence:
                    case RecurrenceState.Exception:
                    {
                        if (appointment.ConferenceId.IsRecurringId)
                        {
                            conference = WebServices.Data.Conference.Bind(MyVrmService.Service,
                                                                          appointment.ConferenceId,
																		  appointment.Start.Date, 
																		  appointment.StartInStartTimeZone,
																		  appointment.EndInEndTimeZone);
                        }
                        else
                        {
                            conference = WebServices.Data.Conference.Bind(MyVrmService.Service,
                                                                          appointment.ConferenceId);
                        }
                        break;
                    }
                    default:
                        throw new ArgumentOutOfRangeException("appointment",
                                                              string.Format("Unknown appointment recurrence state {0}",
                                                                            appointment.RecurrenceState));
                }
				conference.StartDate = appointment.StartInStartTimeZone;
				conference.EndDate = appointment.StartInStartTimeZone + appointment.Duration;
               
				if (conference.AppointmentTime != null)
				{
                    
					conference.AppointmentTime.StartTime = appointment.StartInStartTimeZone.TimeOfDay;
					conference.AppointmentTime.EndTime = conference.AppointmentTime.StartTime + appointment.Duration;
				}
				if (conference.RecurrencePattern != null)
				{
					conference.RecurrencePattern.StartTime = conference.AppointmentTime.StartTime;
					conference.RecurrencePattern.EndTime = conference.AppointmentTime.EndTime;
				}
				conference.ClearChangeLog();
            }
            else
            {
                //Probably here we need to bring up that instant conf dialog

                conference = new WebServices.Data.Conference(service);
                
               
            }
            return conference;
        }

        public OutlookAppointment Appointment
        {
            get
            {
                return _appointment;
            }
        }

		public WebServices.Data.Conference Conference { get; private set; }

        public string Name
        {
            get
            {
                return Conference.Name;
            }
            set
            {
                if (value != Conference.Name)
                {
                    Conference.Name = value;
                    NotifyPropertyChanged("Name");
                }
            }
        }

        public string Description
        {
            get
            {
                return Conference.Description;
            }
            set
            {
                Conference.Description = value;
            }
        }

        public ConferenceType Type
        {
            get
            {
                return Conference.Type;
            }
            set
            {
                if (value != Conference.Type)
                {
                    Conference.Type = value;
                    if (Type == ConferenceType.RoomConference)
                    {
                        ClearEndpoints();
                    }
                    NotifyPropertyChanged("Type");
                }
            }
        }

        public DateTime StartDate
        {
            get
            {
                return Conference.StartDate;
            }
            set
            {
                if (value != Conference.StartDate)
                {
                    Conference.StartDate = value;
                    NotifyPropertyChanged("StartDate");
                }
            }
        }

        public DateTime EndDate
        {
            get
            {
                return Conference.EndDate;
            }
            set
            {
                if (value != Conference.EndDate)
                {
                    Conference.EndDate = value;
                    NotifyPropertyChanged("EndDate");
                }
            }
        }

        public TimeZoneInfo TimeZone
        {
            get
            {
                return Conference.TimeZone;
            }
            set
             {
                if (!Conference.TimeZone.Equals(value))// != value))
                {
                    Conference.TimeZone = value;
                    NotifyPropertyChanged("TimeZone");
                }
            }
        }

        public bool IsRecurring
        {
            get
            {
                return Conference.IsRecurring;
            }
            set
            {
                if (Conference.IsRecurring != value)
                {
                    Conference.IsRecurring = value;
                    NotifyPropertyChanged("IsRecurring");
                }
            }
        }

        public RecurrencePattern RecurrencePattern
        {
            get
            {
                return Conference.RecurrencePattern;
            }
            set
            {
                if (Conference.RecurrencePattern != value)
                {
                    Conference.RecurrencePattern = value;
                    NotifyPropertyChanged("RecurrencePattern");
                }
            }
        }

        public DataList<Participant> Participants
        {
            get
            {
                if (_participants == null)
                {
                    _participants = new DataList<Participant>();
                    _participants.CopyFrom(Conference.Participants);
                    _participants.ListChanged += ParticipantsListChanged;
                }
                return _participants;
            }
        }

        public DataList<RoomId> LocationIds
        {
            get
            {
                if (_rooms == null)
                {
                    _rooms = new DataList<RoomId>();
                    _rooms.CopyFrom(Conference.Rooms);
                    _rooms.ListChanged += RoomsListChanged;
                }
                return _rooms;
            }
        }

        public UserId Host
        {
            get
            {
                return Conference.HostId;
            }
            set
            {
                Conference.HostId = value;
            }
        }

        public bool IsNew
        {
            get
            {
                return Conference.IsNew;
            }
        }

        public ConferenceId Id
        {
            get
            {
                return Conference.Id;
            }
        }

        public TimeSpan Duration
        {
            get
            {
                return Conference.Duration;        
            }
            set
            {
                Conference.Duration = value;
                NotifyPropertyChanged("Duration");
            }
        }

        public string Password
        {
            get
            {
                return Conference.Password;
            }
            set
            {
                if (value == Conference.Password) 
                    return;
                Conference.Password = value;
                NotifyPropertyChanged("Password");
            }
        }

        public bool IsPublic
        {
            get
            {
                return Conference.IsPublic;
            }
            set
            {
                Conference.IsPublic = value;
            }
        }

        public bool HostPresent
        {
            get
            {
                return Appointment != null && Conference.Participants.FirstOrDefault(
                    participant =>
                    string.Equals(Appointment.CurrentUser.SmtpAddress, participant.Email,
                                                               StringComparison.InvariantCultureIgnoreCase)) != null;
            }
            set
            {
                if (Appointment == null) 
                    return;
                var participant = Conference.Participants.FirstOrDefault(
                    item =>
                    string.Equals(Appointment.CurrentUser.SmtpAddress, item.Email,
                                                               StringComparison.InvariantCultureIgnoreCase));
                if (participant != null && !value)
                {
                    Participants.Remove(participant);
                }
                else if (participant == null && value)
                {
                    Participants.Add(new Participant(Appointment.CurrentUser.Name, "", Appointment.CurrentUser.SmtpAddress)
                                     {
                                         InvitationMode = ParticipantInvitationMode.CcOnly,
                                         Notify = false
                                     });
                }
            }
        }
        /// <summary>
        /// Indicates that the conference is available for read-only mode.
        /// </summary>
        public bool IsAllowedToModify
        {
            get { return Conference.IsAllowedToModify; }
        }

        public DataList<ConferenceEndpoint> RoomEndpoints
        {
            get { return _roomEndpoints; }
        }

        public string SpecialInstructions
        {
            get
            {
                var attribute = GetSpecialInstructionsAttribute();
                return attribute.Value;
            }
            set
            {
                var attribute = GetSpecialInstructionsAttribute();
                attribute.Value = value;
            }
        }

        private CustomAttribute GetSpecialInstructionsAttribute()
        {
            if (_specialInstructionsAttrId == null)
            {
                var attributeDefinition =
                    MyVrmService.Service.GetCustomAttributeDefinitions().FirstOrDefault(
                        attrDef =>
                        attrDef.CreateType == CustomAttributeCreateType.System &&
                        string.Equals(attrDef.Title, "Special Instructions"));
                if (attributeDefinition != null)
                {
                    _specialInstructionsAttrId = attributeDefinition.Id;
                }
            }
            var attribute = Conference.CustomAttributes.FirstOrDefault(attr => attr.Id == _specialInstructionsAttrId);
            if (attribute == null)
            {
                attribute = new CustomAttribute {Id = _specialInstructionsAttrId, Type = CustomAttributeType.MultilineText};
                Conference.CustomAttributes.Add(attribute);
            }
            return attribute;
        }

        public event ExternalUpdateEventHandler ExternalUpdate;

		public virtual void OnExternalUpdate(EventArgs e)
		{
			ExternalUpdateEventHandler handler = ExternalUpdate;
			if (handler != null) handler(this, e);
		}

        public event CancelEventHandler BeforeSave;

 		private void InvokeBeforeSave(CancelEventArgs e)
        {
            var handler = BeforeSave;
            if (handler != null) handler(this, e);
        }

		public event AfterSaveEventHandler AfterSave;

		private void OnAfterSave(EventArgs e)
		{
			//Clear non-outlook property
			Appointment.SetNonOutlookProperty(false);

			var handler = AfterSave;
			if (handler != null) handler(this, e);
		}

        public void FillFromTemplate(WebServices.Data.Conference template)
        {
            Conference.Name = template.Name;
            //+ Outlook assign 
            Appointment.Subject = template.Name;

            Conference.Password = template.Password;

            //Fake assign indeed
            Conference.Description = template.Description;
            //Real assign
            Appointment.Body = template.Description;

            Conference.Duration = template.Duration;
            //+ Outlook assign 
            Appointment.Duration = template.Duration;

            //conferenceWrapper.template.Type = template.Type;

            //Fake assign indeed
            Conference.Rooms.Clear();
            Conference.Rooms.AddRange(template.Rooms);
            //Real assign
            LocationIds.Clear();
            LocationIds.AddRange(template.Rooms);

            //Assign participants
            Participants.Clear();
            //Outlook assign - for mail notification sending
            var participantsEmialList = template.Participants.Select(participant => participant.Email).ToList();
            Appointment.SetRecipients(participantsEmialList);

            Conference.IsPublic = template.IsPublic;
            OnExternalUpdate(new EventArgs());
        }

		public virtual bool Save()
		{
			bool bCancelSave = false;
            try
            {
                // ignore any changes
                if (!IsAllowedToModify) return true;
                var e = new CancelEventArgs(false);
                InvokeBeforeSave(e);
                if (!e.Cancel)
                {
                    // Outlook doesn't fire PropertyChanged event for Body property so we always set conference description to appointment's body.
                    Conference.Description = Appointment.Body;
                    //Force to update properties (While moving calendar item Outlook doesn't fire events for property changes 
                    Conference.Name = Appointment.Subject;
                    Conference.StartDate = Appointment.StartInStartTimeZone;
                    Conference.EndDate = Appointment.EndInEndTimeZone; 
					Conference.ModifiedOccurrences.Clear();

                    List<OutlookAppointmentWithMark> outLookAppointmentList;
                    while (true)
                    {
                        // Resolve recurring conflicts
                        if (!ResolveRecurringConflicts(out outLookAppointmentList))
                            return false;
                        SaveEndpoints();
                        //Save to myVRM
                        try
                        {
                        	bCancelSave = false;
							if(_conferenceCopy == null || !_conferenceCopy.Equals(Conference))
								Conference.Save();
							else
							{
								bCancelSave = true;
							}
                            break;
                        }
                        catch (RoomConflictException exception)
                        {
                            UIHelper.ShowError(exception.Message);
                            if (!Conference.IsRecurring)
                                return false;
                        }
                        catch (OutOfSystemAvailabilityException exception)
                        {
                            UIHelper.ShowError(exception.Message);
							if (!Conference.IsRecurring || Conference.IsRecurring && Conference.Rooms.Count == 0)
                                return false;
                        }
                    }
					if (Conference.IsRecurring && !bCancelSave )
                    {
                        if (outLookAppointmentList != null && outLookAppointmentList.Count > 0)
                        {
                            // Save customized conference instances to Outlook (as exceptions)
                            Appointment.Save();

							List<OutlookAppointmentWithMark> outLookDeletedAppointmentList = new List<OutlookAppointmentWithMark>();
							List<OutlookAppointmentWithMark> outLookChangedAppointmentList = new List<OutlookAppointmentWithMark>();

							foreach (var entry in outLookAppointmentList)
							{
								if (entry.IsDeleted)
									outLookDeletedAppointmentList.Add(entry);
								else
									outLookChangedAppointmentList.Add(entry);
							}
							//Remove all deleted occurences
							foreach (var entry in outLookDeletedAppointmentList)
							{
								try
								{
									using (var outlookAppointment = Appointment.GetOccurrence(entry.OriginalStart))
									{
										if (outlookAppointment != null )
											outlookAppointment.Delete();
									}
								}
								catch (Exception exception)
								{
									UIHelper.ShowError(exception.Message);
								}
							}
							//Assign the rest occurences
							foreach (var entry in outLookChangedAppointmentList)
                            {
								try
								{
									using (OutlookAppointment outlookAppointment = Appointment.GetOccurrence(entry.OriginalStart))
									{
                                        if (outlookAppointment != null)
										{
											outlookAppointment.Start = entry.AppointmentStartTime;
											outlookAppointment.End = entry.AppointmentEndTime;
											outlookAppointment.Save();
										}
									}
								}
								catch (Exception exception)
								{
									UIHelper.ShowError(exception.Message);
								}
                            }
                        }
                    }
                    Conference.UpdateCalendarUniqueId(Appointment.GlobalAppointmentId);
                    Appointment.ConferenceId = Conference.Id;
                }
				if (!e.Cancel)
					OnAfterSave(e);
            	e.Cancel = bCancelSave;
                return !e.Cancel;
			}
			catch (SaveConferenceCancelledException)
			{
				return false;
			}
        }

        private bool ResolveRecurringConflicts(out List<OutlookAppointmentWithMark> outLookAppointmentList)
        {
            outLookAppointmentList = new List<OutlookAppointmentWithMark>();
            if (Conference.IsRecurring && Conference.Rooms.Count > 0)
            {
                var cursor = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                RecurrenceConferencesResponse retResponse;
                var id = Conference.Id ?? ConferenceId.NewConferenceId;

                try
                {
                    retResponse = MyVrmService.Service.GetConcurrentConfsList(Conference);
                }
                finally
                {
                    Cursor.Current = cursor;
                }
                if (!retResponse.HasConflicts)
                    return true;
                if (DialogResult.No ==
                           UIHelper.ShowMessage(Strings.ResolveRecConfConflictMsg, MessageBoxButtons.YesNo,
                                                MessageBoxIcon.Question))
                    return false;
                
                var roomTimetable = new List<RoomTimetable>();
                var minDate = DateTime.MaxValue;
                var maxDate = DateTime.MinValue;
				var workingTime = new OrganizationOptions.SystemAvailableTime();

                var retOccurs = retResponse.Occurences;
                foreach (var occur in retOccurs)
                {
					if (minDate > occur.StartDate)
						minDate = occur.StartDate;
					if (maxDate < occur.StartDate)
						maxDate = occur.StartDate;
                }

                cursor = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                try
                {
                    foreach (var roomId in Conference.Rooms)
                    {
						for (DateTime monDate = minDate; monDate.Year <= maxDate.Year && monDate.Month <= maxDate.Month; monDate = monDate.AddMonths(1))
                        {
                            var res = MyVrmService.Service.GetRoomMonthlyCalendar(roomId, monDate);
                            roomTimetable.Add(new RoomTimetable(MyVrmAddin.Instance.GetRoomFromCache(roomId), res));
                        }
                    }
					OrganizationOptions orgOptions = MyVrmService.Service.GetOrganizationOptions();
					if (orgOptions != null)
						workingTime = orgOptions.AvailableTime;
                }
                finally
                {
                    Cursor.Current = cursor;
                }

            	DateTime rightBorder = ConflictResolveDialog.SchedulerMaxDateTime;
				if( Conference.RecurrencePattern.HasEnd )
				{
					rightBorder = Conference.RecurrencePattern.GetLastOccurrence().End;
					rightBorder = rightBorder.Date.AddDays(1);
				}
            	using (var dlg = new ConflictResolveDialog(roomTimetable, retOccurs.ToList(), id, Name, workingTime,
					Conference.StartDate, rightBorder))
                {
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
						OccurrenceInfoExtendCollection resultAppointments = dlg.ResultAppointments;
                        // get dates
                        var occurrences = Conference.RecurrencePattern.GetOccurrenceInfoList(minDate,
                                                                                             maxDate.Date.AddDays(1));
                        Conference.RecurrencePattern.RecurrenceType = RecurrenceType.Custom;
                        // fill custom dates
                        Conference.RecurrencePattern.CustomDates.Clear();
                        Conference.ModifiedOccurrences.Clear();

                        foreach (var occurrenceInfo in occurrences)
                        {
                            var added = false;
                            var deleted = false;

                            foreach (var occ in //indeed only one element
								resultAppointments.Where(
                                    occ => occurrenceInfo.Start.Date == occ.OrigStart.Date))
                            {
                                var outlookApp = new OutlookAppointmentWithMark
                                                     {
                                                         AppointmentStartTime = occ.Start,
                                                         AppointmentEndTime = occ.End,
                                                         OriginalStart = occurrenceInfo.Start,
                                                         IsDeleted = occ.IsDeleted
                                                     };

                                if (occ.IsDeleted)
                                {
                                    deleted = true;
                                }
										
                                outLookAppointmentList.Add(outlookApp);

                                if (!deleted)
                                {
									Conference.RecurrencePattern.CustomDates.Add(occ.Start);
                                    Conference.ModifiedOccurrences.Add(occ);
                                    added = true;
                                }
                                break;
                            }
                            if (!added && !deleted)
                            {
                                Conference.RecurrencePattern.CustomDates.Add(occurrenceInfo.Start);
                                Conference.ModifiedOccurrences.Add(occurrenceInfo);
                            }
                        }
						//Sort CustomDates and ModifiedOccurrences in calendar order
						Conference.RecurrencePattern.CustomDates.Sort();
						OccurrenceInfoCollection orderedModifiedOccurrences = new OccurrenceInfoCollection();
						foreach( DateTime dt in Conference.RecurrencePattern.CustomDates)
						{
							orderedModifiedOccurrences.Add(Conference.ModifiedOccurrences.FirstOrDefault(occInfo => occInfo.Start == dt));
						}
						Conference.ModifiedOccurrences.Clear();
                    	foreach (var orderedModifiedOccurrence in orderedModifiedOccurrences)
                    	{
							Conference.ModifiedOccurrences.Add(orderedModifiedOccurrence);
                    	}
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private void SaveEndpoints()
        {
            Conference.AdvancedAudioVideoSettings.Endpoints.Clear();
            // Add room endpoints
            foreach (var conferenceEndpoint in RoomEndpoints)
            {
                Conference.AdvancedAudioVideoSettings.Endpoints.Add(conferenceEndpoint);
            }
            // Add audio conference bridges
            foreach (var audioConferenceBridge in AudioConferenceBridges)
            {
                var participant = Conference.Participants.FirstOrDefault(
                    part => part.UserId == audioConferenceBridge.Endpoint.Id as UserId);
                if (participant == null)
                {
                    Conference.Participants.Add(new Participant(audioConferenceBridge.Endpoint.Id as UserId)
                                                    {
                                                        AudioVideoMode =
                                                            MediaType.AudioOnly,
                                                        InvitationMode =
                                                            ParticipantInvitationMode.External
                                                    });
                }

                Conference.AdvancedAudioVideoSettings.Endpoints.Add(audioConferenceBridge.Endpoint);
            }
        }

        #region Implementation of IDXDataErrorInfo

        public void GetPropertyError(string propertyName, ErrorInfo info)
        {
            switch (propertyName)
            {
                case "Name":
                    if (string.IsNullOrEmpty(Name))
                    {
                        info.ErrorText = Strings.ConferenceNameRequired;
                    }
                    else if (Regex.IsMatch(Name, Strings.ConferenceNameRegexPattern))
                    {
                        info.ErrorText = Strings.ConferenceNameContainsInvalidCharacters;
                    }
                    break;
                case "Password":
                    if (!string.IsNullOrEmpty(Password) && !Regex.IsMatch(Password, Strings.ConferencePasswordRegexPattern))
                    {
                        info.ErrorText = Strings.ConferencePasswordContainsInvalidCharacters;
                    }
                    break;
            }
        }

        public void GetError(ErrorInfo info)
        {
        }

        #endregion

        private void Initialize()
        {

            if (!IsNew)
                return;
            Name = Appointment.Subject;
            Description = Appointment.Body;
            StartDate = Appointment.StartInStartTimeZone;
            EndDate = Appointment.EndInEndTimeZone;
            TimeZone = Appointment.StartTimeZone;
            //Duration = TimeSpan.FromMinutes(MyVrmService.Service.OrganizationOptions.DefaultConfDuration);   104749
            var currentUser = Appointment.CurrentUser;
            IsRecurring = Appointment.IsRecurring;
            if (IsRecurring)
            {
                RecurrencePattern = Appointment.GetRecurrencePattern(Conference.TimeZone);
            }
            var email = "";
            Func<Participant, bool> findParticipantByEmailPredicate =
                match => string.Equals(match.Email, email, StringComparison.InvariantCultureIgnoreCase);
            email = currentUser.SmtpAddress;
            if (Participants.FirstOrDefault(findParticipantByEmailPredicate) == null)
            {
                Participants.Add(new Participant(currentUser.SmtpAddress)
                                     {
                                         FirstName = currentUser.Name,
                                         InvitationMode = ParticipantInvitationMode.Room
                                     });
            }

            foreach (var recipient in Appointment.Recipients)
            {
                email = recipient.SmtpAddress;
                if (Participants.FirstOrDefault(findParticipantByEmailPredicate) == null)
                {
                    Participants.Add(new Participant(recipient.SmtpAddress)
                                         {
                                             FirstName = recipient.Name,
                                             InvitationMode = ParticipantInvitationMode.Room
                                         });
                }
            }
        }

        private void ClearEndpoints()
        {
            AudioConferenceBridges.Clear();
            RoomEndpoints.Clear();
        }

        #region Implementation of INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;


        private void NotifyPropertyChanged(String info)
        {
            UpdateAppointment(info);
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        #endregion

        void UpdateAppointment(string conferencePropertyName)
        {
            switch(conferencePropertyName)
            {
                case "StartDate":
                {
                    Appointment.StartInStartTimeZone = StartDate;
                    Appointment.StartTimeZone = TimeZone;
                    break;
                }
                case "EndDate":
                {
                    Appointment.EndInEndTimeZone = EndDate;
                    Appointment.EndTimeZone = TimeZone;
                    break;
                }
                case "Duration":
                {
                    Appointment.Duration = Duration;
                    break;
                }
                case "Name":
                {
                    Appointment.Subject = Name;
                    break;
                }
            }
        }

    	private static WebServices.Data.Conference _conferenceCopy = null;
		private static TrueDaybookFields _appCopy = null;
		public void ClearConfCopy()
		{
			_conferenceCopy = null;
			_appCopy = null;
		}

    	public bool IsConfChanged()
		{
			if (_conferenceCopy != null)
				return !_conferenceCopy.Equals(Conference);
			return true;
		}

    	void AppointmentSending(object sender, CancelEventArgs e)
        {
			AppointmentSaving(sender, e);
        }
		//static bool bInternalChange = false;

        private void AppointmentSaving(object sender, CancelEventArgs e)
        {
            try
            {
                
				if (Appointment.GetIsVConfDirtyProperty())// || (InitialBodyTxt != Appointment.Body && Appointment.Body != null))//&& !Appointment.GetVirtualConfIsReverted())
				{
					if (_appCopy == null || _appCopy.IsDifferent(Appointment))
					{
						bool Cancel = false;
                        
						Appointment.VC_CheckAndSave(ref Cancel,this.Conference); //create, update or delete
						if (!Cancel)
						{
							Appointment.ClearIsVConfDirtyProperty();
							if (Appointment.GetIsVirtualConf())
							{
								UIHelper.ShowConferenceIsSuccessfullyCreatedPopup(null);
								
								InitialBodyTxt = Appointment.Body;
								if (_appCopy == null)
									_appCopy = new TrueDaybookFields(Appointment);
								else
									_appCopy.Copy(Appointment);
								//_conferenceCopy = (WebServices.Data.Conference)Conference.Clone();
							}
						}
						e.Cancel = Cancel;
					}
				}
				//bool nonOutlookPropsChanged = Appointment.GetNonOutlookProperty();
				
				//if (Conference.IsDirty || nonOutlookPropsChanged )
				//{
				//    e.Cancel = !Save();
				//    if (!e.Cancel)
				//    {
				//        UIHelper.ShowConferenceIsSuccessfullyCreatedPopup(null);
				//        _conferenceCopy = (WebServices.Data.Conference) Conference.Clone();
				//    }
				//}
            }
			catch(NoCredentialsException ex)
			{
				UIHelper.ShowError(ex.Message);
				ConnectionOptionsDialog.ExternalCall();
				e.Cancel = true;
			}
            catch (Exception exception)
            {
                UIHelper.ShowError(exception.Message);
                e.Cancel = true;
            }
        }

        void AppointmentPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            try
            {
				//Trace.WriteLine("** !!! ** PropertyName = " + e.PropertyName);
                switch (e.PropertyName)
                {
                    case "Subject":
                    {
                        Name = Appointment.Subject;
                        break;
                    }
                    case "StartInStartTimeZone":
                    {
                        StartDate = Appointment.StartInStartTimeZone;
                        EndDate = Appointment.EndInEndTimeZone; 
                        TimeZone = Appointment.StartTimeZone;
                        SeatsTimezone = TimeZone;
                        break;
                    }
                    case "EndInEndTimeZone":
                    {
                        StartDate = Appointment.StartInStartTimeZone;
                        EndDate = Appointment.EndInEndTimeZone; 
                        break;
                    }
                    case "IsRecurring":
                    {
                        IsRecurring = Appointment.IsRecurring;
                        if (Appointment.IsRecurring)
                        {
                            RecurrencePattern = Appointment.GetRecurrencePattern(Conference.TimeZone);
                        }
                        break;
                    }
                    case "Recipients":
                    {
                        foreach (var recipient in from recipient in Appointment.Recipients
                                                  let participant = Participants.FirstOrDefault(item => string.Equals(recipient.SmtpAddress, item.Email, StringComparison.InvariantCultureIgnoreCase))
                                                  where participant == null
                                                  select recipient)
                        {
                            Participants.Add(new Participant(recipient.Name, "", recipient.SmtpAddress)
                                             {
                                                 InvitationMode = ParticipantInvitationMode.Room
                                             });
                        }
                        var participantsToDelete =
                            Participants.Where(participant =>
                                               !Appointment.Recipients.Any(
                                                   recipient => string.Equals(recipient.SmtpAddress, participant.Email, StringComparison.InvariantCultureIgnoreCase)) &&
                                               !string.Equals(Appointment.CurrentUser.SmtpAddress, participant.Email, StringComparison.InvariantCultureIgnoreCase)).ToList();
                        foreach (var participant in participantsToDelete)
                        {
                            Participants.Remove(participant);
                        }
                        break;
                    }
                }
            }
            catch (Exception exception)
            {
                MyVrmAddin.TraceSource.TraceException(exception);
                UIHelper.ShowError(exception.Message);
            }
        }

        void AppointmentBeforeDelete(object sender, CancelEventArgs e)
        {
        }

        private void RoomsListChanged(object sender, ListChangedEventArgs e)
        {
            switch(e.ListChangedType)
            {
                case ListChangedType.Reset:
                {
                    Conference.Rooms.Clear();
                    Conference.Rooms.AddRange(_rooms);
                    break;
                }
                case ListChangedType.ItemAdded:
                {
                    Conference.Rooms.Insert(e.NewIndex, _rooms[e.NewIndex]);
                    break;
                }
                case ListChangedType.ItemDeleted:
                {
                    Conference.Rooms.RemoveAt(e.NewIndex);
                    break;
                }
				case ListChangedType.ItemMoved:
        			RoomId id = Conference.Rooms[e.OldIndex];
        			Conference.Rooms.RemoveAt(e.OldIndex);
        			Conference.Rooms.Insert(e.NewIndex, id);
            		break;
                case ListChangedType.ItemChanged:
                {
                    Conference.Rooms[e.NewIndex] = _rooms[e.NewIndex];
                    break;
                }
                case ListChangedType.PropertyDescriptorAdded:
                    break;
                case ListChangedType.PropertyDescriptorDeleted:
                    break;
                case ListChangedType.PropertyDescriptorChanged:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        void ParticipantsListChanged(object sender, ListChangedEventArgs e)
        {
            switch(e.ListChangedType)
            {
                case ListChangedType.Reset:
                {
                    Conference.Participants.Clear();
                    Conference.Participants.AddRange(_participants);
                    break;
                }
                case ListChangedType.ItemAdded:
                {
                    Conference.Participants.Insert(e.NewIndex, _participants[e.NewIndex]);
                    break;
                }
                case ListChangedType.ItemDeleted:
                {
                    var email = Conference.Participants[e.NewIndex].Email;
                    Conference.Participants.RemoveAt(e.NewIndex);
                    Appointment.RemoveRecipient(email);
                    break;
                }
                case ListChangedType.ItemMoved:
                {
                    break;
                }
                case ListChangedType.ItemChanged:
                    break;
                case ListChangedType.PropertyDescriptorAdded:
                    break;
                case ListChangedType.PropertyDescriptorDeleted:
                    break;
                case ListChangedType.PropertyDescriptorChanged:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
