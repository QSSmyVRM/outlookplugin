﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System.Configuration;

namespace MyVrm.Outlook.WinForms.Properties
{
    partial class Settings
    {
        public void UpdateProvider(SettingsProvider provider)
        {
            foreach (SettingsProperty property in Properties)
            {
                property.Provider = provider;
            }
            Providers.Clear();
            Providers.Add(provider);
        }
    }
}