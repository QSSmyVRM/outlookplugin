﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
    public class ConfRoom:ComplexProperty 
     {
		public override object Clone()//CopyTo(Participant participant)
		{
			//if (participant == null)
          
            if (ConfEndPt.Id.Id != "0")
            {
                ConfRoom CRoom = new ConfRoom();
                CRoom.Id = Id;
                CRoom.ConfEndPt = ConfEndPt;
                return CRoom;
            }
            return new ConfRoom(new RoomId ("0"));
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(this, obj))
				return true;

            ConfRoom participant = obj as ConfRoom;
			if (participant == null)
				return false;
			if (participant.Id != Id)
				return false;
            if (participant.ConfEndPt != ConfEndPt)
				return false;
		
			
			return true;
		}
        

        public ConfRoom()
        {
            Id = new RoomId("0");
            ConfEndPt = new ConferenceEndpoint();
            ConfEndPt.Id = Id;
        }
        public ConfRoom(ConferenceEndpoint ConfEndpt, RoomId id)
        {
            if (id.Id != "0")
            {
                Id = id;
                ConfEndPt = new ConferenceEndpoint();
                ConfEndPt = ConfEndpt;
            }
        }
        public ConfRoom(RoomId id)
        {
            if (id.Id != "0")
            {
                Id = id;
                ConfEndPt = new ConferenceEndpoint();
                ConfEndPt.Id = Id;
            }
        }
        
        public RoomId Id { get; set; }

       

        public ConferenceEndpoint ConfEndPt { get; set; }

      
            

     

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
           
            ConfEndPt.WriteElementsToXml(writer);// 103900
        }
        internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
        {
           //ConfEndPt.TryReadElementFromXml(reader);
            switch (reader.LocalName)
            {
                case "level1ID":
                   // CRoom = new ConfRoom();
                    ConfEndPt = new ConferenceEndpoint();
                    ConfEndPt.Id = new RoomId();
                    ConfEndPt.Id.LoadFromXml(reader, "level1ID");
                    Id =new RoomId (ConfEndPt.Id.Id);
                    return true;
            }
            return false; 
        }
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

    }
}
