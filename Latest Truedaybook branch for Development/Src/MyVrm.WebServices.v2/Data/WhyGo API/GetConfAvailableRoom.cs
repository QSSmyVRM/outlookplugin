﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;

namespace MyVrm.WebServices.Data
{
	internal class GetConfAvailableRoomRequest : ServiceRequestBase<GetConfAvailableRoomResponse>
	{
		public enum RoomTypeToSearchFor
		{
			Both = 0,
			Public = 1,
			Private = -1
		}

		public enum DataRequestMode
		{
			None = 0,
			AllData = 1,
			IdTs = 2
		}

		internal int _page;
		internal string _filter;
		internal SearchSortParam _searchBy;
		internal SearchSortParam _sortBy;
		internal ConferenceId ConferenceId;
		internal bool Immediate { get; set; }
		internal bool Recurring { get; set; }
		internal DateTime Start { get; set; }
		internal TimeZoneInfo TimeZone { get; set; }
		internal TimeSpan Duration { get; set; }
		internal MediaTypeFilter MediaType { get; set; }
		internal ConferenceType ConferenceType { get; set; }
		internal RoomTypeToSearchFor _roomTypeToSearchFor;
		internal DataRequestMode _requestMode;

		internal GetConfAvailableRoomRequest(MyVrmService service)
			: base(service)
		{
			MediaType = MediaTypeFilter.All;
			ConferenceType = ConferenceType.AudioVideo;
			Start = DateTime.MinValue;
			TimeZone = TimeZoneInfo.Local;
			Duration = TimeSpan.Zero;
			_requestMode = DataRequestMode.IdTs;//AllData;//IdTs;
		}

		internal RoomId RoomId { get; set; }

		public DataRequestMode RequestMode
		{
			set { _requestMode = value; }
			get { return _requestMode; }
		}

		#region Overrides of ServiceRequestBase

		internal override string GetXmlElementName()
		{
			return "GetConfAvailableRoom";
		}

		internal override string GetCommandName()
		{
			return Constants.GetConfAvailableRoomCommandName;
		}

		internal override string GetResponseXmlElementName()
		{
			return "GetConfAvailableRoom";
		}

		internal override GetConfAvailableRoomResponse ParseResponse(MyVrmServiceXmlReader reader)
		{
			var response = new GetConfAvailableRoomResponse(); //{ Room = new Room(Service) };
			response.LoadFromXml(reader, "GetConfAvailableRoom");// GetResponseXmlElementName());
			return response;
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			UserId.WriteToXml(writer, "userID");
			ConferenceId.WriteToXml(writer);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "confType", ConferenceType);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "immediate", Immediate);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "recurring", Recurring);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "startDate", Utilities.DateToString(Start.Date));
			writer.WriteElementValue(XmlNamespace.NotSpecified, "startHour", Utilities.HourToString(Start.Hour));
			writer.WriteElementValue(XmlNamespace.NotSpecified, "startMin", Start.Minute);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "startSet", Utilities.GetNoonAbbr(Start.TimeOfDay));
			writer.WriteElementValue(XmlNamespace.NotSpecified, "mediaType", MediaType);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "timeZone", TimeZoneConvertion.ConvertToTimeZoneId(TimeZone));
			writer.WriteElementValue(XmlNamespace.NotSpecified, "serviceType", -1);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "selected", "");
			writer.WriteElementValue(XmlNamespace.NotSpecified, "dirationMin", Duration.TotalMinutes);

			writer.WriteElementValue(XmlNamespace.NotSpecified, "searchBy", _searchBy);
			if (_page != 0)
				writer.WriteElementValue(XmlNamespace.NotSpecified, "pageNo", _page);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "searchFor", _filter);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "Public", _roomTypeToSearchFor);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "outXMLType", _requestMode);
		}

		#endregion
	}

	public class GetConfAvailableRoomResponse : ServiceResponse
	{
		public List<Room> Rooms { get; internal set; }
		public int PageNo { get; internal set; }
		public int TotalPages { get; internal set; }
		public int TotalNumber { get; internal set; }

		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{
			Rooms = new List<Room>();
			while (!reader.IsEndElement(XmlNamespace.NotSpecified, "GetConfAvailableRoom"))
			{
				switch (reader.LocalName)
				{
					case "pageNo":
						PageNo = reader.ReadElementValue<int>();
						break;
					case "totalPages":
						TotalPages = reader.ReadElementValue<int>();
						break;
					case "totalNumber":
						TotalNumber = reader.ReadElementValue<int>();
						break;
					case "Room":
						Room aRoom = new Room(MyVrmService.Service);
						aRoom.LoadFromXml(reader, true, "Room");
						Rooms.Add(aRoom);
						break;
				}
				reader.Read();
			}
		}
	}
}