﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using MyVrm.WebServices.Data;
using Image=System.Drawing.Image;

namespace MyVrm.Outlook.WinForms.Conference
{
    internal enum ConferenceResourceType
    {
        User,
        Room
    }

    internal class ConferenceResource
    {
        public object Id { get; set; }
        public string Caption { get; set; }
        public ConferenceResourceType Type { get; set; }
        public Image Image { get; set; }
        public ParticipantInvitationMode ParticipantInvitationMode { get; set; }
    }

    internal class ConferenceFakeResource : ConferenceResource
    {
        internal ConferenceFakeResource()
        {
            Id = -1;
        }
    }
}