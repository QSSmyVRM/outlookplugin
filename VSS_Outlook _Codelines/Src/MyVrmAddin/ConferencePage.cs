﻿using System;
using DevExpress.XtraEditors;
using Microsoft.Office.Interop.Outlook;
using MyVrm.Outlook;
using MyVrm.Outlook.WinForms;
using MyVrm.Outlook.WinForms.Conference;
using Exception = System.Exception;

namespace MyVrmAddin2003
{
    public partial class ConferencePage : CustomFormControl
    {
        public ConferencePage()
        {
            InitializeComponent();
			Text = Strings.ConferencePageCaption;
        }

        private void ConferencePage_CustomFormShowing(object sender, EventArgs e)
        {
            try
            {
                //OutlookAppointmentImpl appointmentImpl;
                //conferenceControl.Conference =
                //    Globals.ThisAddIn.GetConferenceWrapperForAppointment(OutlookItem as AppointmentItem,
                //                                                         out appointmentImpl);
            }
            catch (Exception exception)
            {
                MyVrmAddin.TraceSource.TraceException(exception);
                UIHelper.ShowError(exception.Message);
            }
        }

        public ConferenceWrapper Conference
        {
            get { return conferenceControl.Conference; }
            set { conferenceControl.Conference = value; }
        }
    }

    internal partial class WindowCustomFormCollection
    {
        public ConferencePage ConferencePage
        {
            get { return FindFirst<ConferencePage>(); }
        }
    }
}
