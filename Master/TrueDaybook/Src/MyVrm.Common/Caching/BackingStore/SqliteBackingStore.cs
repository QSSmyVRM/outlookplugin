﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Runtime.Serialization;
using System.Threading;

namespace MyVrm.Common.Caching.BackingStore
{
    public class SqliteBackingStore : IBackingStore, IDisposable
    {
        private readonly SQLiteConnectionStringBuilder _connectionString;
        private readonly ReaderWriterLock _lockObject = new ReaderWriterLock();
        const long SchemaVersion = 2;
        private bool _initialized = false;

        public SqliteBackingStore(string databaseName)
        {
            _connectionString = new SQLiteConnectionStringBuilder {DataSource = databaseName};
        }

        #region Implementation of IBackingStore

        public void Add(CacheItem cacheItem)
        {
            EnsureInitialized();

            byte[] valueBytes = SerializationUtility.ToBytes(cacheItem.Value);
            
            using(var connection = GetConnection())
            {
                var command = new SQLiteCommand("INSERT OR REPLACE INTO [CacheData] ([Key], [Value]) VALUES(@Key, @Value)", connection);
                command.Parameters.Add("@Key", DbType.AnsiString).Value = cacheItem.Key;
                command.Parameters.Add("@Value", DbType.Binary, valueBytes.Length).Value = valueBytes;
                connection.Open();

                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        public void Flush()
        {
            EnsureInitialized();
            using (var connection = GetConnection())
            {
                var command = new SQLiteCommand("DELETE FROM [CacheData];VACUUM", connection);
                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        public Dictionary<string, object> Load()
        {
            EnsureInitialized();

            var items = new Dictionary<string, object>();

            using (var connection = GetConnection())
            {
                var command = new SQLiteCommand("SELECT [Key], [Value] FROM [CacheData]", connection);
                connection.Open();
                using(var reader = command.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while(reader.Read())
                    {
                        var key = reader.GetString(0);
                        byte[] valueBytes = null;
                        var bufferSize = reader.GetBytes(1, 0, valueBytes, 0, 0);
                        valueBytes = new byte[bufferSize];
                        reader.GetBytes(1, 0, valueBytes, 0, valueBytes.Length);
                        try
                        {
                            items.Add(key, SerializationUtility.ToObject(valueBytes));
                        }
                        catch (SerializationException /*e*/)
                        {
                        }
                    }
                }
            }
            return items;
        }

        public void Remove(string key)
        {
            EnsureInitialized();
            using (var connection = GetConnection())
            {
                var command = new SQLiteCommand("DELETE FROM [CacheData] WHERE [Key] = @Key", connection);
                command.Parameters.Add("@Key", DbType.AnsiString).Value = key;
                connection.Open();

                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        #endregion

        private SQLiteConnection GetConnection()
        {
            return new SQLiteConnection(_connectionString.ToString());
        }

        #region Implementation of IDisposable

        public void Dispose()
        {
            
        }

        #endregion

        private long GetSchemaVersion()
        {
            lock (_lockObject)
            {
                using (var connection = GetConnection())
                {
                    var command = new SQLiteCommand(connection) { CommandText = "PRAGMA user_version" };
                    connection.Open();
                    var ver = (long)command.ExecuteScalar();
                    connection.Close();
                    return ver;
                }

            }
        }

        private void EnsureInitialized()
        {
            if (_initialized)
                return;
            if (File.Exists(_connectionString.DataSource))
            {
                var ver = GetSchemaVersion();
                if (ver != SchemaVersion)
                {
                    File.Delete(_connectionString.DataSource);                    
                }
                else
                {
                    _initialized = true;
                    return;
                }
            }
            using (var connection = GetConnection())
            {
                var command = new SQLiteCommand(Strings.SQLiteCreateCachingDb, connection);
                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
            }
            _initialized = true;
        }
    }
}
