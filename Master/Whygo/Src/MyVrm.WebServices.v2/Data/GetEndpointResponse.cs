﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System.Linq;

namespace MyVrm.WebServices.Data
{
    internal class GetEndpointResponse : ServiceResponse
    {
        internal Endpoint Endpoint { get; private set; }

        internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
        {
            do
            {
                reader.Read();
                if (reader.LocalName == "Endpoint" && !reader.IsEmptyElement)
                {
                    Endpoint = new Endpoint(reader.Service);
                    Endpoint.LoadFromXml(reader, true);
                    var profile = Endpoint.Profiles.FirstOrDefault(match => match.Id == Endpoint.DefaultProfileId);
                    if (profile != null)
                    {
                        profile.IsDefault = true;
                    }
                }
                else
                {
                    reader.SkipCurrentElement();
                }

            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "EndpointDetails"));
        }
    }
}
