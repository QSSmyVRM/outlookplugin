﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.Outlook.WinForms.Conference
{
    partial class AudioVideoConferenceAVSettingsPage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AudioVideoConferenceAVSettingsPage));
            this.repositoryBridgeComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.roomAudioVideoSettingsListControl = new MyVrm.Outlook.WinForms.Conference.RoomAudioVideoSettingsListControl();
            this.maxAudioPortsEdit = new DevExpress.XtraEditors.SpinEdit();
            this.maxVideoPortsEdit = new DevExpress.XtraEditors.SpinEdit();
            this.conferenceOnPortEdit = new DevExpress.XtraEditors.CheckEdit();
            this.lectureModeEdit = new DevExpress.XtraEditors.CheckEdit();
            this.videoModeEdit = new MyVrm.Outlook.WinForms.EnumComboBoxEdit();
            this.videoDisplayEdit = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.videoDisplayImages = new DevExpress.Utils.ImageCollection(this.components);
            this.singleDialinEdit = new DevExpress.XtraEditors.CheckEdit();
            this.audioCodecEdit = new MyVrm.Outlook.WinForms.EnumComboBoxEdit();
            this.audioVideoTypeEdit = new MyVrm.Outlook.WinForms.EnumComboBoxEdit();
            this.maxLineRateEdit = new MyVrm.Outlook.WinForms.EnumComboBoxEdit();
            this.encryptedEdit = new DevExpress.XtraEditors.CheckEdit();
            this.dualStreamModeEdit = new DevExpress.XtraEditors.CheckEdit();
            this.videoCodecEdit = new MyVrm.Outlook.WinForms.EnumComboBoxEdit();
            this.audioVideoProtocolEdit = new MyVrm.Outlook.WinForms.EnumComboBoxEdit();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.commonAVSettingsGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.polycomSettingsGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryBridgeComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.maxAudioPortsEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxVideoPortsEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.conferenceOnPortEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lectureModeEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.videoModeEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.videoDisplayEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.videoDisplayImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.singleDialinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.audioCodecEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.audioVideoTypeEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxLineRateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.encryptedEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dualStreamModeEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.videoCodecEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.audioVideoProtocolEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.commonAVSettingsGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.polycomSettingsGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            this.SuspendLayout();
            // 
            // repositoryBridgeComboBox
            // 
            this.repositoryBridgeComboBox.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryBridgeComboBox.AutoHeight = false;
            this.repositoryBridgeComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryBridgeComboBox.Name = "repositoryBridgeComboBox";
            this.repositoryBridgeComboBox.NullText = "<Select bridge>";
            this.repositoryBridgeComboBox.NullValuePrompt = "<Select bridge>";
            this.repositoryBridgeComboBox.NullValuePromptShowForEmptyValue = true;
            this.repositoryBridgeComboBox.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // layoutControl3
            // 
            this.layoutControl3.AllowCustomizationMenu = false;
            this.layoutControl3.Controls.Add(this.roomAudioVideoSettingsListControl);
            this.layoutControl3.Controls.Add(this.maxAudioPortsEdit);
            this.layoutControl3.Controls.Add(this.maxVideoPortsEdit);
            this.layoutControl3.Controls.Add(this.conferenceOnPortEdit);
            this.layoutControl3.Controls.Add(this.lectureModeEdit);
            this.layoutControl3.Controls.Add(this.videoModeEdit);
            this.layoutControl3.Controls.Add(this.videoDisplayEdit);
            this.layoutControl3.Controls.Add(this.singleDialinEdit);
            this.layoutControl3.Controls.Add(this.audioCodecEdit);
            this.layoutControl3.Controls.Add(this.audioVideoTypeEdit);
            this.layoutControl3.Controls.Add(this.maxLineRateEdit);
            this.layoutControl3.Controls.Add(this.encryptedEdit);
            this.layoutControl3.Controls.Add(this.dualStreamModeEdit);
            this.layoutControl3.Controls.Add(this.videoCodecEdit);
            this.layoutControl3.Controls.Add(this.audioVideoProtocolEdit);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 0);
            this.layoutControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup3;
            this.layoutControl3.Size = new System.Drawing.Size(1030, 748);
            this.layoutControl3.TabIndex = 0;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // roomAudioVideoSettingsListControl
            // 
            this.roomAudioVideoSettingsListControl._repositoryBridgeComboBox = this.repositoryBridgeComboBox;
            this.roomAudioVideoSettingsListControl.BridgeNameColumnVisibility = true;
            this.roomAudioVideoSettingsListControl.Location = new System.Drawing.Point(6, 38);
            this.roomAudioVideoSettingsListControl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.roomAudioVideoSettingsListControl.Name = "roomAudioVideoSettingsListControl";
            this.roomAudioVideoSettingsListControl.Size = new System.Drawing.Size(1018, 701);
            this.roomAudioVideoSettingsListControl.TabIndex = 20;
            this.roomAudioVideoSettingsListControl.useDefaultColumnVisibility = true;
            // 
            // maxAudioPortsEdit
            // 
            this.maxAudioPortsEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.maxAudioPortsEdit.Location = new System.Drawing.Point(609, 34);
            this.maxAudioPortsEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.maxAudioPortsEdit.Name = "maxAudioPortsEdit";
            this.maxAudioPortsEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.maxAudioPortsEdit.Properties.IsFloatValue = false;
            this.maxAudioPortsEdit.Properties.Mask.EditMask = "N00";
            this.maxAudioPortsEdit.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.maxAudioPortsEdit.Size = new System.Drawing.Size(53, 22);
            this.maxAudioPortsEdit.StyleController = this.layoutControl3;
            this.maxAudioPortsEdit.TabIndex = 19;
            // 
            // maxVideoPortsEdit
            // 
            this.maxVideoPortsEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.maxVideoPortsEdit.Location = new System.Drawing.Point(173, 34);
            this.maxVideoPortsEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.maxVideoPortsEdit.Name = "maxVideoPortsEdit";
            this.maxVideoPortsEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.maxVideoPortsEdit.Properties.IsFloatValue = false;
            this.maxVideoPortsEdit.Properties.Mask.EditMask = "N00";
            this.maxVideoPortsEdit.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.maxVideoPortsEdit.Size = new System.Drawing.Size(53, 22);
            this.maxVideoPortsEdit.StyleController = this.layoutControl3;
            this.maxVideoPortsEdit.TabIndex = 18;
            // 
            // conferenceOnPortEdit
            // 
            this.conferenceOnPortEdit.Location = new System.Drawing.Point(3, 67);
            this.conferenceOnPortEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.conferenceOnPortEdit.Name = "conferenceOnPortEdit";
            this.conferenceOnPortEdit.Properties.Caption = "Conference on Port";
            this.conferenceOnPortEdit.Size = new System.Drawing.Size(436, 21);
            this.conferenceOnPortEdit.StyleController = this.layoutControl3;
            this.conferenceOnPortEdit.TabIndex = 17;
            // 
            // lectureModeEdit
            // 
            this.lectureModeEdit.Location = new System.Drawing.Point(3, 37);
            this.lectureModeEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lectureModeEdit.Name = "lectureModeEdit";
            this.lectureModeEdit.Properties.Caption = "Lecture Mode";
            this.lectureModeEdit.Size = new System.Drawing.Size(436, 21);
            this.lectureModeEdit.StyleController = this.layoutControl3;
            this.lectureModeEdit.TabIndex = 16;
            // 
            // videoModeEdit
            // 
            this.videoModeEdit.EditValue = "";
            this.videoModeEdit.Location = new System.Drawing.Point(173, 6);
            this.videoModeEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.videoModeEdit.Name = "videoModeEdit";
            this.videoModeEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.videoModeEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.videoModeEdit.Size = new System.Drawing.Size(266, 22);
            this.videoModeEdit.StyleController = this.layoutControl3;
            this.videoModeEdit.TabIndex = 15;
            // 
            // videoDisplayEdit
            // 
            this.videoDisplayEdit.EditValue = 0;
            this.videoDisplayEdit.Location = new System.Drawing.Point(609, 96);
            this.videoDisplayEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.videoDisplayEdit.Name = "videoDisplayEdit";
            this.videoDisplayEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.videoDisplayEdit.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 0, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 1, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 2, 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 3, 3),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 4, 4),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 5, 5)});
            this.videoDisplayEdit.Properties.LargeImages = this.videoDisplayImages;
            this.videoDisplayEdit.Size = new System.Drawing.Size(53, 45);
            this.videoDisplayEdit.StyleController = this.layoutControl3;
            this.videoDisplayEdit.TabIndex = 14;
            // 
            // videoDisplayImages
            // 
            this.videoDisplayImages.ImageSize = new System.Drawing.Size(57, 43);
            this.videoDisplayImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("videoDisplayImages.ImageStream")));
            this.videoDisplayImages.Images.SetKeyName(0, "01.gif");
            this.videoDisplayImages.Images.SetKeyName(1, "02.gif");
            this.videoDisplayImages.Images.SetKeyName(2, "03.gif");
            this.videoDisplayImages.Images.SetKeyName(3, "04.gif");
            this.videoDisplayImages.Images.SetKeyName(4, "05.gif");
            this.videoDisplayImages.Images.SetKeyName(5, "06.gif");
            // 
            // singleDialinEdit
            // 
            this.singleDialinEdit.Location = new System.Drawing.Point(439, 156);
            this.singleDialinEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.singleDialinEdit.Name = "singleDialinEdit";
            this.singleDialinEdit.Properties.Caption = "Single Dial-in Number";
            this.singleDialinEdit.Size = new System.Drawing.Size(435, 21);
            this.singleDialinEdit.StyleController = this.layoutControl3;
            this.singleDialinEdit.TabIndex = 13;
            // 
            // audioCodecEdit
            // 
            this.audioCodecEdit.EditValue = "";
            this.audioCodecEdit.Location = new System.Drawing.Point(609, 65);
            this.audioCodecEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.audioCodecEdit.Name = "audioCodecEdit";
            this.audioCodecEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.audioCodecEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.audioCodecEdit.Size = new System.Drawing.Size(265, 22);
            this.audioCodecEdit.StyleController = this.layoutControl3;
            this.audioCodecEdit.TabIndex = 12;
            // 
            // audioVideoTypeEdit
            // 
            this.audioVideoTypeEdit.EditValue = "";
            this.audioVideoTypeEdit.Location = new System.Drawing.Point(609, 3);
            this.audioVideoTypeEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.audioVideoTypeEdit.Name = "audioVideoTypeEdit";
            this.audioVideoTypeEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.audioVideoTypeEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.audioVideoTypeEdit.Size = new System.Drawing.Size(265, 22);
            this.audioVideoTypeEdit.StyleController = this.layoutControl3;
            this.audioVideoTypeEdit.TabIndex = 10;
            // 
            // maxLineRateEdit
            // 
            this.maxLineRateEdit.EditValue = "";
            this.maxLineRateEdit.Location = new System.Drawing.Point(173, 156);
            this.maxLineRateEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.maxLineRateEdit.Name = "maxLineRateEdit";
            this.maxLineRateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.maxLineRateEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.maxLineRateEdit.Size = new System.Drawing.Size(262, 22);
            this.maxLineRateEdit.StyleController = this.layoutControl3;
            this.maxLineRateEdit.TabIndex = 9;
            // 
            // encryptedEdit
            // 
            this.encryptedEdit.Location = new System.Drawing.Point(3, 126);
            this.encryptedEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.encryptedEdit.Name = "encryptedEdit";
            this.encryptedEdit.Properties.Caption = "Encryption";
            this.encryptedEdit.Size = new System.Drawing.Size(432, 21);
            this.encryptedEdit.StyleController = this.layoutControl3;
            this.encryptedEdit.TabIndex = 8;
            // 
            // dualStreamModeEdit
            // 
            this.dualStreamModeEdit.Location = new System.Drawing.Point(3, 96);
            this.dualStreamModeEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dualStreamModeEdit.Name = "dualStreamModeEdit";
            this.dualStreamModeEdit.Properties.Caption = "Dual Stream Mode";
            this.dualStreamModeEdit.Size = new System.Drawing.Size(432, 21);
            this.dualStreamModeEdit.StyleController = this.layoutControl3;
            this.dualStreamModeEdit.TabIndex = 7;
            // 
            // videoCodecEdit
            // 
            this.videoCodecEdit.EditValue = "";
            this.videoCodecEdit.Location = new System.Drawing.Point(173, 65);
            this.videoCodecEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.videoCodecEdit.Name = "videoCodecEdit";
            this.videoCodecEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.videoCodecEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.videoCodecEdit.Size = new System.Drawing.Size(262, 22);
            this.videoCodecEdit.StyleController = this.layoutControl3;
            this.videoCodecEdit.TabIndex = 6;
            // 
            // audioVideoProtocolEdit
            // 
            this.audioVideoProtocolEdit.EditValue = "";
            this.audioVideoProtocolEdit.Location = new System.Drawing.Point(173, 3);
            this.audioVideoProtocolEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.audioVideoProtocolEdit.Name = "audioVideoProtocolEdit";
            this.audioVideoProtocolEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.audioVideoProtocolEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.audioVideoProtocolEdit.Size = new System.Drawing.Size(262, 22);
            this.audioVideoProtocolEdit.StyleController = this.layoutControl3;
            this.audioVideoProtocolEdit.TabIndex = 4;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.commonAVSettingsGroup,
            this.polycomSettingsGroup,
            this.layoutControlGroup4,
            this.layoutControlGroup5});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "Root";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(1030, 748);
            this.layoutControlGroup3.Text = "Root";
            this.layoutControlGroup3.TextVisible = false;
            // 
            // commonAVSettingsGroup
            // 
            this.commonAVSettingsGroup.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.commonAVSettingsGroup.AppearanceGroup.Options.UseFont = true;
            this.commonAVSettingsGroup.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.commonAVSettingsGroup.AppearanceItemCaption.Options.UseFont = true;
            this.commonAVSettingsGroup.CustomizationFormText = "Common Settings (vendor-neutral)";
            this.commonAVSettingsGroup.ExpandButtonVisible = true;
            this.commonAVSettingsGroup.Expanded = false;
            this.commonAVSettingsGroup.GroupBordersVisible = false;
            this.commonAVSettingsGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem22,
            this.layoutControlItem21,
            this.layoutControlItem20,
            this.layoutControlItem19,
            this.layoutControlItem17,
            this.layoutControlItem23,
            this.layoutControlItem25,
            this.layoutControlItem26,
            this.layoutControlItem27,
            this.emptySpaceItem6,
            this.emptySpaceItem1,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.emptySpaceItem7,
            this.emptySpaceItem8});
            this.commonAVSettingsGroup.Location = new System.Drawing.Point(0, 0);
            this.commonAVSettingsGroup.Name = "commonAVSettingsGroup";
            this.commonAVSettingsGroup.Size = new System.Drawing.Size(1028, 3);
            this.commonAVSettingsGroup.Text = "Common Settings (vendor-neutral)";
            this.commonAVSettingsGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.maxLineRateEdit;
            this.layoutControlItem22.CustomizationFormText = "Maximum Line Rate";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 153);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(436, 31);
            this.layoutControlItem22.Text = "Maximum Line Rate";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(166, 17);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.encryptedEdit;
            this.layoutControlItem21.CustomizationFormText = "layoutControlItem21";
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 123);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(436, 30);
            this.layoutControlItem21.Text = "layoutControlItem21";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem21.TextToControlDistance = 0;
            this.layoutControlItem21.TextVisible = false;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.dualStreamModeEdit;
            this.layoutControlItem20.CustomizationFormText = "layoutControlItem20";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 93);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(436, 30);
            this.layoutControlItem20.Text = "layoutControlItem20";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem20.TextToControlDistance = 0;
            this.layoutControlItem20.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.videoCodecEdit;
            this.layoutControlItem19.CustomizationFormText = "Video Codecs";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 62);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(436, 31);
            this.layoutControlItem19.Text = "Video Codecs";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(166, 17);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.audioVideoProtocolEdit;
            this.layoutControlItem17.CustomizationFormText = "Restrict Network Access To";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(436, 31);
            this.layoutControlItem17.Text = "Restrict Network Access To";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(166, 17);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.audioVideoTypeEdit;
            this.layoutControlItem23.CustomizationFormText = "Restrict Usage To";
            this.layoutControlItem23.Location = new System.Drawing.Point(436, 0);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(439, 31);
            this.layoutControlItem23.Text = "Restrict Usage To";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(166, 17);
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.audioCodecEdit;
            this.layoutControlItem25.CustomizationFormText = "layoutControlItem25";
            this.layoutControlItem25.Location = new System.Drawing.Point(436, 62);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(439, 31);
            this.layoutControlItem25.Text = "Audio Codecs";
            this.layoutControlItem25.TextSize = new System.Drawing.Size(166, 17);
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.singleDialinEdit;
            this.layoutControlItem26.CustomizationFormText = "layoutControlItem26";
            this.layoutControlItem26.Location = new System.Drawing.Point(436, 153);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(439, 31);
            this.layoutControlItem26.Text = "layoutControlItem26";
            this.layoutControlItem26.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem26.TextToControlDistance = 0;
            this.layoutControlItem26.TextVisible = false;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.videoDisplayEdit;
            this.layoutControlItem27.CustomizationFormText = "Video Display";
            this.layoutControlItem27.Location = new System.Drawing.Point(436, 93);
            this.layoutControlItem27.MaxSize = new System.Drawing.Size(227, 56);
            this.layoutControlItem27.MinSize = new System.Drawing.Size(227, 56);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(227, 60);
            this.layoutControlItem27.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem27.Text = "Video Display";
            this.layoutControlItem27.TextSize = new System.Drawing.Size(166, 17);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(663, 93);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(212, 60);
            this.emptySpaceItem6.Text = "emptySpaceItem6";
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 184);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(875, 221);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.maxVideoPortsEdit;
            this.layoutControlItem11.CustomizationFormText = "Maximum Video Ports";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 31);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(227, 31);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(227, 31);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(227, 31);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "Maximum Video Ports";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(166, 17);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.maxAudioPortsEdit;
            this.layoutControlItem12.CustomizationFormText = "Maximum Audio Ports";
            this.layoutControlItem12.Location = new System.Drawing.Point(436, 31);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(227, 31);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(227, 31);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(227, 31);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "Maximum Audio Ports";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(166, 17);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(227, 31);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(209, 31);
            this.emptySpaceItem7.Text = "emptySpaceItem7";
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(663, 31);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(212, 31);
            this.emptySpaceItem8.Text = "emptySpaceItem8";
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // polycomSettingsGroup
            // 
            this.polycomSettingsGroup.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.polycomSettingsGroup.AppearanceGroup.Options.UseFont = true;
            this.polycomSettingsGroup.CustomizationFormText = "Polycom Specific Settings";
            this.polycomSettingsGroup.ExpandButtonVisible = true;
            this.polycomSettingsGroup.Expanded = false;
            this.polycomSettingsGroup.GroupBordersVisible = false;
            this.polycomSettingsGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem28,
            this.layoutControlItem29,
            this.layoutControlItem30,
            this.emptySpaceItem5});
            this.polycomSettingsGroup.Location = new System.Drawing.Point(0, 3);
            this.polycomSettingsGroup.Name = "polycomSettingsGroup";
            this.polycomSettingsGroup.Size = new System.Drawing.Size(1028, 3);
            this.polycomSettingsGroup.Text = "Polycom Specific Settings";
            this.polycomSettingsGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.videoModeEdit;
            this.layoutControlItem28.CustomizationFormText = "layoutControlItem28";
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(440, 31);
            this.layoutControlItem28.Text = "Video Mode";
            this.layoutControlItem28.TextSize = new System.Drawing.Size(166, 16);
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.lectureModeEdit;
            this.layoutControlItem29.CustomizationFormText = "layoutControlItem29";
            this.layoutControlItem29.Location = new System.Drawing.Point(0, 31);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(440, 30);
            this.layoutControlItem29.Text = "layoutControlItem29";
            this.layoutControlItem29.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem29.TextToControlDistance = 0;
            this.layoutControlItem29.TextVisible = false;
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.Control = this.conferenceOnPortEdit;
            this.layoutControlItem30.CustomizationFormText = "layoutControlItem30";
            this.layoutControlItem30.Location = new System.Drawing.Point(0, 61);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(440, 30);
            this.layoutControlItem30.Text = "layoutControlItem30";
            this.layoutControlItem30.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem30.TextToControlDistance = 0;
            this.layoutControlItem30.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(440, 0);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(435, 91);
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.layoutControlGroup4.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup4.CustomizationFormText = "Rooms";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 6);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup4.Size = new System.Drawing.Size(1028, 737);
            this.layoutControlGroup4.Text = "Rooms";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.roomAudioVideoSettingsListControl;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1022, 705);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.layoutControlGroup5.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup5.CustomizationFormText = "Users";
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.Expanded = false;
            this.layoutControlGroup5.GroupBordersVisible = false;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem3});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 743);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(1028, 3);
            this.layoutControlGroup5.Text = "Users";
            this.layoutControlGroup5.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(861, 10);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 182);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(701, 258);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // AudioVideoConferenceAVSettingsPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.layoutControl3);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "AudioVideoConferenceAVSettingsPage";
            this.Size = new System.Drawing.Size(1030, 748);
            this.Load += new System.EventHandler(this.AudioVideoConferenceAVSettingsPage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryBridgeComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.maxAudioPortsEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxVideoPortsEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.conferenceOnPortEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lectureModeEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.videoModeEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.videoDisplayEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.videoDisplayImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.singleDialinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.audioCodecEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.audioVideoTypeEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxLineRateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.encryptedEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dualStreamModeEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.videoCodecEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.audioVideoProtocolEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.commonAVSettingsGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.polycomSettingsGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private MyVrm.Outlook.WinForms.EnumComboBoxEdit videoCodecEdit;
        private MyVrm.Outlook.WinForms.EnumComboBoxEdit audioVideoProtocolEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraEditors.CheckEdit encryptedEdit;
        private DevExpress.XtraEditors.CheckEdit dualStreamModeEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private MyVrm.Outlook.WinForms.EnumComboBoxEdit maxLineRateEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlGroup commonAVSettingsGroup;
        private MyVrm.Outlook.WinForms.EnumComboBoxEdit audioCodecEdit;
        private MyVrm.Outlook.WinForms.EnumComboBoxEdit audioVideoTypeEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraEditors.CheckEdit singleDialinEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.ImageComboBoxEdit videoDisplayEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private MyVrm.Outlook.WinForms.EnumComboBoxEdit videoModeEdit;
        private DevExpress.XtraLayout.LayoutControlGroup polycomSettingsGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraEditors.CheckEdit conferenceOnPortEdit;
        private DevExpress.XtraEditors.CheckEdit lectureModeEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.Utils.ImageCollection videoDisplayImages;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.SpinEdit maxVideoPortsEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.SpinEdit maxAudioPortsEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private RoomAudioVideoSettingsListControl roomAudioVideoSettingsListControl;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryBridgeComboBox;

    }
}