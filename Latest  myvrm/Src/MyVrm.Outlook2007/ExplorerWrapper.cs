﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using Microsoft.Office.Interop.Outlook;

namespace MyVrm.Outlook
{
    public class ExplorerWrapper
    {
        public ExplorerWrapper(Explorer explorer)
        {
            try
            {
                if (explorer == null)
                    throw new ArgumentNullException("explorer");
                Explorer = explorer;
                Id = Guid.NewGuid();
                ((ExplorerEvents_10_Event)Explorer).Close += OnExplorerClose;
                Explorer.Deactivate += OnDeactivate;
                Initialize();
            }
            catch (System.Exception exception)
            {
                MyVrmAddin.TraceSource.TraceException(exception);
            }
        }

        public Guid Id { get; private set; }

        public Explorer Explorer { get; private set; }

        protected virtual void Initialize()
        {
        }

        public event EventHandler Close;

        protected virtual void OnClose()
        {
            if (Close!= null)
            {
                Close(this, EventArgs.Empty);
            }
        }

        protected virtual void OnDeactivate()
        {

        }

        protected void OnExplorerClose()
        {
            OnClose();
            Explorer.Deactivate -= OnDeactivate;
            ((ExplorerEvents_10_Event)Explorer).Close -= OnExplorerClose;
            Explorer = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

    }
}
