﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    public class UserRoleSchema : ServiceObjectSchema
    {
        public static readonly PropertyDefinition Id;
        public static readonly PropertyDefinition Name;
        public static readonly PropertyDefinition Locked;
        public static readonly PropertyDefinition IsActive;

        internal static readonly UserRoleSchema Instance;

        static UserRoleSchema()
        {
            Id = new ComplexPropertyDefinition<UserRoleId>("ID", () => new UserRoleId());
            Name = new StringPropertyDefinition("name", PropertyDefinitionFlags.CanSet);
            Locked = new BoolPropertyDefinition("locked", PropertyDefinitionFlags.CanSet);
            IsActive = new BoolPropertyDefinition("active", PropertyDefinitionFlags.CanSet);

            Instance = new UserRoleSchema();
        }

        internal override void RegisterProperties()
        {
            base.RegisterProperties();
            RegisterProperty(Id);
            RegisterProperty(Name);
            RegisterProperty(Locked);
            RegisterProperty(IsActive);
        }
    }
}
