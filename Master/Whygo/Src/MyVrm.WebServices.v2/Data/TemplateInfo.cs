﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.WebServices.Data
{
	public class TemplateInfo : ComplexProperty
	{
		private const string const_TemplateInfo = "templateInfo";
		private const string const_ID = "ID";
		private const string const_Name = "name";
		private const string const_IsPublic = "public";
		private const string const_Owner = "owner";
		private const string const_OwnerFirstName = "firstName";
		private const string const_OwnerLastName = "lastName";
		private const string const_SetDefault = "setDefault";
		private const string const_Description = "description";

		public string ID;
		public string Name;
		public int IsPublic ;
		public string OwnerFirstName;
		public string OwnerLastName;
		public bool SetDefault ;
		public string Description;

		public override object Clone()
		{
			TemplateInfo copy = new TemplateInfo();
			copy.ID = string.Copy(ID);
			copy.Name = string.Copy(Name);
			copy.OwnerFirstName = string.Copy(OwnerFirstName);
			copy.OwnerLastName = string.Copy(OwnerLastName);
			copy.Description = string.Copy(Description);
			copy.IsPublic = IsPublic;
			copy.SetDefault = SetDefault;

			return copy;
		}

		internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
		{
			bool bRet = false;
			switch (reader.LocalName)
			{
				case const_ID:
					{
						ID = reader.ReadElementValue();
						bRet = true;
						break;
					}
				case const_Name:
					{
						Name = reader.ReadElementValue();
						bRet = true;
						break;
					}
				case const_IsPublic:
					{
						IsPublic = reader.ReadElementValue<int>();
						bRet = true;
						break;
					}
				case const_Owner: //skip, section name
					{
						bRet = true;
						break;
					}
				case const_OwnerFirstName:
					{
						OwnerFirstName = reader.ReadElementValue();
						bRet = true;
						break;
					}
				case const_OwnerLastName:
					{
						OwnerLastName = reader.ReadElementValue();
						bRet = true;
						break;
					}
				case const_SetDefault:
					{
						SetDefault = reader.ReadElementValue<bool>();
						bRet = true;
						break;
					}
				case const_Description:
					{
						Description = reader.ReadElementValue();
						bRet = true;
						break;
					}
				default:
					break;
			}
			reader.Read();
			return bRet;
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			writer.WriteStartElement(Namespace, const_TemplateInfo); 
			if (ID != null)
				writer.WriteElementValue(Namespace, const_ID, ID);
			if (Name != null)
				writer.WriteElementValue(Namespace, const_Name, Name);
			writer.WriteElementValue(Namespace, const_IsPublic, IsPublic.ToString());
			writer.WriteStartElement(Namespace, const_Owner);
			if (OwnerFirstName != null)
				writer.WriteElementValue(Namespace, const_OwnerFirstName, OwnerFirstName);
			if (OwnerLastName != null)
				writer.WriteElementValue(Namespace, const_OwnerLastName, OwnerLastName);
			writer.WriteEndElement(); 
			writer.WriteElementValue(Namespace, const_SetDefault, SetDefault.ToString().ToLower());
			if (Description != null)
				writer.WriteElementValue(Namespace, const_Description, Description);
			writer.WriteEndElement();
		}
	}
}
