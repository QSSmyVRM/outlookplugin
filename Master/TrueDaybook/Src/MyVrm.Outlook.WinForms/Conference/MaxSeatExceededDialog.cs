﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyVrm.Outlook.WinForms.Conference
{
    public partial class MaxSeatExceededDialog : Form
    {

        private DateTime _startDateTime = DateTime.Now;

        private bool _bShowSeatsDialog = false;

        public bool ShowSeatsDialog
        {
            get { return _bShowSeatsDialog; }
            set { _bShowSeatsDialog = value; }
        }

        public DateTime StartDateTime
        {
            get { return _startDateTime; }
            set { _startDateTime = value; }
        }
        private DateTime _endDateTime = DateTime.Now.AddHours(1.0);

        public DateTime EndDateTime
        {
            get { return _endDateTime; }
            set { _endDateTime = value; }
        }


        public MaxSeatExceededDialog()
        {
            InitializeComponent();
            this.Text = Strings.MaxSeatsExceededDialogHeading;
            btnCancel.Text = Strings.CancelButtonText;
            btnCheckAvailableSeats.Text = Strings.CheckAvailabilityText;
            labelControl1.Text = Strings.MaxSearchExceededMessage;


        }

        private void MaxSeatExceededDialog_Load(object sender, EventArgs e)
        {

        }

        private void btnCheckAvailableSeats_Click(object sender, EventArgs e)
        {
            
            DialogResult = DialogResult.OK;
            this.Close();
            
            
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void MaxSeatExceededDialog_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
