﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	public class RoomTimetable
	{
		public Room _room;
		public ReadOnlyCollection<ConferenceOccurrence> _occurences;
		public RoomTimetable(Room room, ReadOnlyCollection<ConferenceOccurrence> occurences)
		{
			_room = room;
			_occurences = occurences;
		}
	}
}
