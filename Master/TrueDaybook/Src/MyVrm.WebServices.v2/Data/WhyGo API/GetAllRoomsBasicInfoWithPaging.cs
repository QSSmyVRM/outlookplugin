﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;

namespace MyVrm.WebServices.Data
{
	public enum SearchSortParam
	{
		NoType,
		Capacity = 1,
		City = 2,   
		Name = 3,
		Phone = 4,
		Site = 5,
		State = 6,
		ZipCode = 7
	}

	internal class GetAllRoomsBasicInfoWithPagingRequest : ServiceRequestBase<GetAllRoomsBasicInfoWithPagingResponse>
	{
		internal int _page;
		internal string _filter;
		internal SearchSortParam _searchBy;
		internal SearchSortParam _sortBy;

		public static SearchSortParam GetSearchSortParamByName(string name)
		{
			switch (name)
			{
				case "Capacity":
					return SearchSortParam.Capacity;
				case "City":
					return SearchSortParam.City;
				case "Name":
					return SearchSortParam.Name;
				case "Phone":
					return SearchSortParam.Phone;
				case "Site":
					return SearchSortParam.Site;
				case "State":
					return SearchSortParam.State;
				case "ZipCode":
				case "Zip Code":
					return SearchSortParam.ZipCode;
			}
			return SearchSortParam.NoType;
		}

		internal GetAllRoomsBasicInfoWithPagingRequest(MyVrmService service)
			: base(service)
		{
		}

		internal RoomId RoomId { get; set; }

		#region Overrides of ServiceRequestBase

		internal override string GetXmlElementName()
		{
			return "GetAllRoomsBasicInfo";
		}

		internal override string GetCommandName()
		{
			return Constants.GetAllRoomsBasicInfoCommandName;
		}

		internal override string GetResponseXmlElementName()
		{
			return "Rooms";
		}

		internal override GetAllRoomsBasicInfoWithPagingResponse ParseResponse(MyVrmServiceXmlReader reader)
		{
			var response = new GetAllRoomsBasicInfoWithPagingResponse(); //{ Room = new Room(Service) };
			response.LoadFromXml(reader, "Rooms");// GetResponseXmlElementName());
			return response;
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			UserId.WriteToXml(writer, "UserID");
			//writer.WriteElementValue(XmlNamespace.NotSpecified, "roomName", _filter);
			//writer.WriteElementValue(XmlNamespace.NotSpecified, "SortBy", _sortBy);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "SearchBy", _searchBy);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "SearchFor", _filter);
			if (_page != 0)
				writer.WriteElementValue(XmlNamespace.NotSpecified, "pageNo", _page);
		}

		#endregion
	}

	public class GetAllRoomsBasicInfoWithPagingResponse : ServiceResponse
	{
		public List<Room> Rooms { get; internal set; }
		public int PageNo { get; internal set; }
		public int TotalPages { get; internal set; }
		public int TotalNumber { get; internal set; }

		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{
			Rooms = new List<Room>();
			while (!reader.IsEndElement(XmlNamespace.NotSpecified, "Rooms"))
			{
				switch (reader.LocalName)
				{
					case "pageNo":
						PageNo = reader.ReadElementValue<int>();
						break;
					case "totalPages":
						TotalPages = reader.ReadElementValue<int>();
						break;
					case "totalNumber":
						TotalNumber = reader.ReadElementValue<int>();
						break;
					case "Room":
						Room aRoom = new Room(MyVrmService.Service);
						aRoom.LoadFromXml(reader, true, "Room");
						Rooms.Add(aRoom);
						break;
				}
				reader.Read();
			}
		}
	}
}