/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.Outlook.WinForms.Options
{
    partial class ConnectionOptionsDialog
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.layoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.windowsUserPasswordTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.windowsUserAccountNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.windowsUserDomainNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.customUserCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.differentWindowsUserCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.defaultWindowsUserCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.webServiceUrlEdit = new DevExpress.XtraEditors.TextEdit();
            this.testConnectionButton = new DevExpress.XtraEditors.SimpleButton();
            this.userPasswordEdit = new DevExpress.XtraEditors.TextEdit();
            this.userNameEdit = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.differentWindowsUserCredentialControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.customUserCredentialControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.errorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).BeginInit();
            this.ContentPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).BeginInit();
            this.layoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.windowsUserPasswordTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowsUserAccountNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowsUserDomainNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.customUserCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.differentWindowsUserCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.defaultWindowsUserCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.webServiceUrlEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userPasswordEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userNameEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.differentWindowsUserCredentialControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.customUserCredentialControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // ContentPanel
            // 
            this.ContentPanel.Controls.Add(this.layoutControl);
            this.ContentPanel.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.ContentPanel.Size = new System.Drawing.Size(424, 429);
            // 
            // layoutControl
            // 
            this.layoutControl.Controls.Add(this.windowsUserPasswordTextEdit);
            this.layoutControl.Controls.Add(this.windowsUserAccountNameTextEdit);
            this.layoutControl.Controls.Add(this.windowsUserDomainNameTextEdit);
            this.layoutControl.Controls.Add(this.customUserCheckEdit);
            this.layoutControl.Controls.Add(this.differentWindowsUserCheckEdit);
            this.layoutControl.Controls.Add(this.defaultWindowsUserCheckEdit);
            this.layoutControl.Controls.Add(this.labelControl1);
            this.layoutControl.Controls.Add(this.webServiceUrlEdit);
            this.layoutControl.Controls.Add(this.testConnectionButton);
            this.layoutControl.Controls.Add(this.userPasswordEdit);
            this.layoutControl.Controls.Add(this.userNameEdit);
            this.layoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl.Location = new System.Drawing.Point(0, 0);
            this.layoutControl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layoutControl.Name = "layoutControl";
            this.layoutControl.Root = this.layoutControlGroup1;
            this.layoutControl.Size = new System.Drawing.Size(424, 429);
            this.layoutControl.TabIndex = 0;
            this.layoutControl.Text = "layoutControl1";
            // 
            // windowsUserPasswordTextEdit
            // 
            this.windowsUserPasswordTextEdit.Location = new System.Drawing.Point(109, 150);
            this.windowsUserPasswordTextEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.windowsUserPasswordTextEdit.Name = "windowsUserPasswordTextEdit";
            this.windowsUserPasswordTextEdit.Properties.PasswordChar = '*';
            this.windowsUserPasswordTextEdit.Size = new System.Drawing.Size(313, 22);
            this.windowsUserPasswordTextEdit.StyleController = this.layoutControl;
            this.windowsUserPasswordTextEdit.TabIndex = 6;
            this.windowsUserPasswordTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.windowsUserPasswordTextEdit_Validating);
            // 
            // windowsUserAccountNameTextEdit
            // 
            this.windowsUserAccountNameTextEdit.Location = new System.Drawing.Point(109, 124);
            this.windowsUserAccountNameTextEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.windowsUserAccountNameTextEdit.Name = "windowsUserAccountNameTextEdit";
            this.windowsUserAccountNameTextEdit.Size = new System.Drawing.Size(313, 22);
            this.windowsUserAccountNameTextEdit.StyleController = this.layoutControl;
            this.windowsUserAccountNameTextEdit.TabIndex = 5;
            this.windowsUserAccountNameTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.windowsUserAccountNameTextEdit_Validating);
            // 
            // windowsUserDomainNameTextEdit
            // 
            this.windowsUserDomainNameTextEdit.Location = new System.Drawing.Point(109, 98);
            this.windowsUserDomainNameTextEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.windowsUserDomainNameTextEdit.Name = "windowsUserDomainNameTextEdit";
            this.windowsUserDomainNameTextEdit.Size = new System.Drawing.Size(313, 22);
            this.windowsUserDomainNameTextEdit.StyleController = this.layoutControl;
            this.windowsUserDomainNameTextEdit.TabIndex = 4;
            this.windowsUserDomainNameTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.windowsUserDomainNameTextEdit_Validating);
            // 
            // customUserCheckEdit
            // 
            this.customUserCheckEdit.Location = new System.Drawing.Point(2, 176);
            this.customUserCheckEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.customUserCheckEdit.Name = "customUserCheckEdit";
            this.customUserCheckEdit.Properties.Caption = "Custom User";
            this.customUserCheckEdit.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.customUserCheckEdit.Properties.RadioGroupIndex = 0;
            this.customUserCheckEdit.Size = new System.Drawing.Size(420, 21);
            this.customUserCheckEdit.StyleController = this.layoutControl;
            this.customUserCheckEdit.TabIndex = 7;
            this.customUserCheckEdit.TabStop = false;
            this.customUserCheckEdit.CheckedChanged += new System.EventHandler(this.customUserCheckEdit_CheckedChanged);
            // 
            // differentWindowsUserCheckEdit
            // 
            this.differentWindowsUserCheckEdit.Location = new System.Drawing.Point(2, 73);
            this.differentWindowsUserCheckEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.differentWindowsUserCheckEdit.Name = "differentWindowsUserCheckEdit";
            this.differentWindowsUserCheckEdit.Properties.Caption = "Different Windows User";
            this.differentWindowsUserCheckEdit.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.differentWindowsUserCheckEdit.Properties.RadioGroupIndex = 0;
            this.differentWindowsUserCheckEdit.Size = new System.Drawing.Size(420, 21);
            this.differentWindowsUserCheckEdit.StyleController = this.layoutControl;
            this.differentWindowsUserCheckEdit.TabIndex = 3;
            this.differentWindowsUserCheckEdit.TabStop = false;
            this.differentWindowsUserCheckEdit.CheckedChanged += new System.EventHandler(this.differentWindowsUserCheckEdit_CheckedChanged);
            // 
            // defaultWindowsUserCheckEdit
            // 
            this.defaultWindowsUserCheckEdit.Location = new System.Drawing.Point(2, 48);
            this.defaultWindowsUserCheckEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.defaultWindowsUserCheckEdit.Name = "defaultWindowsUserCheckEdit";
            this.defaultWindowsUserCheckEdit.Properties.Caption = "Current Windows User ({0})";
            this.defaultWindowsUserCheckEdit.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.defaultWindowsUserCheckEdit.Properties.RadioGroupIndex = 0;
            this.defaultWindowsUserCheckEdit.Size = new System.Drawing.Size(420, 21);
            this.defaultWindowsUserCheckEdit.StyleController = this.layoutControl;
            this.defaultWindowsUserCheckEdit.TabIndex = 2;
            this.defaultWindowsUserCheckEdit.TabStop = false;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(2, 28);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(89, 16);
            this.labelControl1.StyleController = this.layoutControl;
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Authenticate As";
            // 
            // webServiceUrlEdit
            // 
            this.webServiceUrlEdit.Location = new System.Drawing.Point(109, 2);
            this.webServiceUrlEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.webServiceUrlEdit.Name = "webServiceUrlEdit";
            this.webServiceUrlEdit.Size = new System.Drawing.Size(313, 22);
            this.webServiceUrlEdit.StyleController = this.layoutControl;
            this.webServiceUrlEdit.TabIndex = 0;
            this.webServiceUrlEdit.Validating += new System.ComponentModel.CancelEventHandler(this.webServiceUrlEdit_Validating);
            // 
            // testConnectionButton
            // 
            this.testConnectionButton.Location = new System.Drawing.Point(316, 253);
            this.testConnectionButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.testConnectionButton.Name = "testConnectionButton";
            this.testConnectionButton.Size = new System.Drawing.Size(106, 29);
            this.testConnectionButton.StyleController = this.layoutControl;
            this.testConnectionButton.TabIndex = 10;
            this.testConnectionButton.Text = "Test connection";
            this.testConnectionButton.Click += new System.EventHandler(this.testConnectionButton_Click);
            // 
            // userPasswordEdit
            // 
            this.userPasswordEdit.Location = new System.Drawing.Point(109, 227);
            this.userPasswordEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.userPasswordEdit.Name = "userPasswordEdit";
            this.userPasswordEdit.Properties.PasswordChar = '*';
            this.userPasswordEdit.Size = new System.Drawing.Size(313, 22);
            this.userPasswordEdit.StyleController = this.layoutControl;
            this.userPasswordEdit.TabIndex = 9;
            this.userPasswordEdit.Validating += new System.ComponentModel.CancelEventHandler(this.userPasswordEdit_Validating);
            // 
            // userNameEdit
            // 
            this.userNameEdit.Location = new System.Drawing.Point(109, 201);
            this.userNameEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.userNameEdit.Name = "userNameEdit";
            this.userNameEdit.Size = new System.Drawing.Size(313, 22);
            this.userNameEdit.StyleController = this.layoutControl;
            this.userNameEdit.TabIndex = 8;
            this.userNameEdit.Validating += new System.ComponentModel.CancelEventHandler(this.userNameEdit_Validating);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.layoutControlItem4,
            this.layoutControlItem1,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.differentWindowsUserCredentialControlGroup,
            this.customUserCredentialControlGroup});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(424, 429);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 251);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(314, 178);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.testConnectionButton;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(314, 251);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(110, 33);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(110, 33);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(110, 178);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.webServiceUrlEdit;
            this.layoutControlItem1.CustomizationFormText = "Web Service URL:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(424, 26);
            this.layoutControlItem1.Text = "Web Service URL:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(103, 16);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.labelControl1;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(424, 20);
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.defaultWindowsUserCheckEdit;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 46);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(424, 25);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.differentWindowsUserCheckEdit;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 71);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(424, 25);
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.customUserCheckEdit;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 174);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(424, 25);
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // differentWindowsUserCredentialControlGroup
            // 
            this.differentWindowsUserCredentialControlGroup.CustomizationFormText = "layoutControlGroup2";
            this.differentWindowsUserCredentialControlGroup.GroupBordersVisible = false;
            this.differentWindowsUserCredentialControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem12});
            this.differentWindowsUserCredentialControlGroup.Location = new System.Drawing.Point(0, 96);
            this.differentWindowsUserCredentialControlGroup.Name = "differentWindowsUserCredentialControlGroup";
            this.differentWindowsUserCredentialControlGroup.Size = new System.Drawing.Size(424, 78);
            this.differentWindowsUserCredentialControlGroup.Text = "differentWindowsUserCredentialControlGroup";
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.windowsUserDomainNameTextEdit;
            this.layoutControlItem10.CustomizationFormText = "Domain:";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(424, 26);
            this.layoutControlItem10.Text = "Domain:";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(103, 16);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.windowsUserAccountNameTextEdit;
            this.layoutControlItem11.CustomizationFormText = "User:";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(424, 26);
            this.layoutControlItem11.Text = "User:";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(103, 16);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.windowsUserPasswordTextEdit;
            this.layoutControlItem12.CustomizationFormText = "Password:";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 52);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(424, 26);
            this.layoutControlItem12.Text = "Password:";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(103, 16);
            // 
            // customUserCredentialControlGroup
            // 
            this.customUserCredentialControlGroup.CustomizationFormText = "customUserCredentialControlGroup";
            this.customUserCredentialControlGroup.GroupBordersVisible = false;
            this.customUserCredentialControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3});
            this.customUserCredentialControlGroup.Location = new System.Drawing.Point(0, 199);
            this.customUserCredentialControlGroup.Name = "customUserCredentialControlGroup";
            this.customUserCredentialControlGroup.Size = new System.Drawing.Size(424, 52);
            this.customUserCredentialControlGroup.Text = "customUserCredentialControlGroup";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.userNameEdit;
            this.layoutControlItem2.CustomizationFormText = "Login:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(424, 26);
            this.layoutControlItem2.Text = "Login:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(103, 16);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.userPasswordEdit;
            this.layoutControlItem3.CustomizationFormText = "Password:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(424, 26);
            this.layoutControlItem3.Text = "Password:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(103, 16);
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // ConnectionOptionsDialog
            // 
            this.ApplyEnabled = true;
            this.ApplyVisible = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(448, 485);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "ConnectionOptionsDialog";
            this.Text = "Connection Options";
            this.Load += new System.EventHandler(this.OptionsControl_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ConnectionOptionsDialog_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).EndInit();
            this.ContentPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).EndInit();
            this.layoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.windowsUserPasswordTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowsUserAccountNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowsUserDomainNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.customUserCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.differentWindowsUserCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.defaultWindowsUserCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.webServiceUrlEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userPasswordEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userNameEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.differentWindowsUserCredentialControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.customUserCredentialControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit userNameEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.TextEdit userPasswordEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.SimpleButton testConnectionButton;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.TextEdit webServiceUrlEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.CheckEdit defaultWindowsUserCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.CheckEdit customUserCheckEdit;
        private DevExpress.XtraEditors.CheckEdit differentWindowsUserCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.TextEdit windowsUserAccountNameTextEdit;
        private DevExpress.XtraEditors.TextEdit windowsUserDomainNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.TextEdit windowsUserPasswordTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlGroup differentWindowsUserCredentialControlGroup;
        private DevExpress.XtraLayout.LayoutControlGroup customUserCredentialControlGroup;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider errorProvider;
    }
}