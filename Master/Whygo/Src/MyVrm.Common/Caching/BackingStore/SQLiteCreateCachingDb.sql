﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
PRAGMA page_size = 4096;

CREATE TABLE [CacheData]
(
	[Key] varchar (128),
	[Value] BLOB,
	CONSTRAINT PK_CacheData PRIMARY KEY ([Key])
);


PRAGMA user_version = 2;