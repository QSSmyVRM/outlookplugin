﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using MyVrm.WebServices.Data;
using Image = MyVrm.WebServices.Data.Image;

namespace MyVrm.Outlook.WinForms.Conference
{
    public partial class HousekeepingWorkOrderDialog : Dialog
    {
        private class WorkOrderItemRecord
        {
            private readonly WorkOrderItem _workOrderItem;
            private readonly InventorySetItem _inventorySetItem;
            private System.Drawing.Image _itemImage;

            internal WorkOrderItemRecord(WorkOrderItem workOrderItem, InventorySetItem inventorySetItem)
            {
                _workOrderItem = workOrderItem;
                _inventorySetItem = inventorySetItem;
                RequestedQuantity = _workOrderItem.Quantity;
            }

            public int InventoryItemId
            {
                get
                {
                    return _inventorySetItem.Id;
                }
            }
            public int Id
            {
                get
                {
                    return WorkOrderItem.Id;
                }
            }
            public string Name
            {
                get
                {
                    return _inventorySetItem.Name;
                }
            }
            public System.Drawing.Image Image
            {
                get {
                    return _itemImage ??
                           (_itemImage = System.Drawing.Image.FromStream(_inventorySetItem.Image.AsStream()));
                }
            }
            public string Comments
            {
                get
                {
                    return _inventorySetItem.Comments;
                }
            }
            public string Description
            {
                get
                {
                    return _inventorySetItem.Description;
                }
            }
            public uint Quantity
            {
                get
                {
                    return _inventorySetItem.Quantity;
                }
            }

            public uint RequestedQuantity { get; set; }

            internal WorkOrderItem WorkOrderItem
            {
                get { return _workOrderItem; }
            }
        }

        private class RoomLayout
        {
            private readonly Image _image;
            private System.Drawing.Image _itemImage;

            internal RoomLayout(Image image)
            {
                _image = image;
            }

            public string Name { get { return _image.Name; } set{} }

            public System.Drawing.Image Image
            {
                get
                {
                    return _itemImage ??
                        (_itemImage = System.Drawing.Image.FromStream(_image.AsStream()));
                }
            }
        }

        public HousekeepingWorkOrderDialog()
        {
            WorkOrderItemRecords = new List<WorkOrderItemRecord>();
            InitializeComponent();
        	layoutControlItem1.Text = Strings.RoomLableText;
        	layoutControlItem3.Text = Strings.SetNameLableText;
			layoutControlItem1.CustomizationFormText = Strings.RoomLableText;
			layoutControlItem3.CustomizationFormText = Strings.SetNameLableText;
			layoutControlItem2.Text = Strings.RoomLayoutLableText;
			layoutControlItem2.CustomizationFormText = Strings.RoomLayoutLableText;
			Text = Strings.WorkOrderLableText;
        	roomSetComboBox.Properties.NullText = Strings.SelectOneText;
			itemNameColumn.Caption = Strings.NameColumnText;
			serialNumberColumn.Caption = Strings.SerialColumnText;
			itemImageColumn.Caption = Strings.ImageColumnText;
			commentsColumn.Caption = Strings.CommentsColumnText;
			requestQuantityColumn.Caption = Strings.RequestedQuantityColumnText;
			roomLayoutEdit.Properties.NullText = Strings.SelectLayoutText;
			ApplyEnabled = false;
			ApplyVisible = false;
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
        Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        public WorkOrder WorkOrder { get; set; }

        private List<WorkOrderItemRecord> WorkOrderItemRecords { get; set; }

        private void HousekeepingWorkOrderDialog_Load(object sender, EventArgs e)
        {
            var cursor = Cursor;
            Cursor = Cursors.WaitCursor;
            try
            {
                var room = MyVrmAddin.Instance.GetRoomFromCache(WorkOrder.RoomId);
                Text = string.Format("{0} - {1}", Text, WorkOrder.Name);
                roomNameLabel.Text = room.Name;
                var roomSets = MyVrmService.Service.GetRoomSets(WorkOrder.RoomId, RoomSetType.Housekeeping);
                roomSetComboBox.Properties.Items.BeginUpdate();
                try
                {
                    foreach (var roomSet in roomSets)
                    {
                        roomSetComboBox.Properties.Items.Add(roomSet);
                    }
                    var workOrderRoomSet = roomSets.FirstOrDefault(s => s.Id == WorkOrder.SetId);
                    roomSetComboBox.SelectedItem = workOrderRoomSet;
                }
                finally
                {
                    roomSetComboBox.Properties.Items.EndUpdate();
                }
                roomLayoutEdit.Enabled = room.DynamicLayout;
                if (room.DynamicLayout)
                {
                    roomLayoutList.DataSource = room.RoomImages.Select((img, layout) => new RoomLayout(img)).ToList();
                    roomLayoutEdit.EditValue = WorkOrder.RoomLayout;
                }
            }
            finally
            {
                Cursor = cursor;
            }
        }

        private void roomSetComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var cursor = Cursor;
            Cursor = Cursors.WaitCursor;
            try
            {
                var roomSet = roomSetComboBox.SelectedItem as RoomSet;
                if (roomSet != null)
                {
                    var inventorySet = MyVrmService.Service.GetInventoryDetails(roomSet.Id, RoomSetType.Housekeeping);
                    var query = from inventoryItem in inventorySet.Items
                                join workOrderItem in WorkOrder.Items on inventoryItem.Id equals workOrderItem.Id into
                                    woItems
                                from woItem in woItems.DefaultIfEmpty(new WorkOrderItem { Id = inventoryItem.Id })
                                select
                                    new WorkOrderItemRecord(woItem, inventoryItem);
                    WorkOrderItemRecords = query.ToList();
                    workOrderItemsList.DataSource = WorkOrderItemRecords;
                }
            }
            finally
            {
                Cursor = cursor;
            }
        }

        private void quantityRepositoryItemSpinEdit_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            //var newValue = Convert.ToInt32(e.NewValue);
			int newValue = 0;
			Int32.TryParse(e.NewValue.ToString(), out newValue);
            if (newValue < 0)
            {
                e.Cancel = true;
            }
        }

        private void HousekeepingWorkOrderDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult != DialogResult.OK) return;
            WorkOrderItemRecords.ForEach(rec => rec.WorkOrderItem.Quantity = rec.RequestedQuantity);
            var workOrderItemsWithNonZeroQuantity = from itemRecord in WorkOrderItemRecords
                                                    where itemRecord.RequestedQuantity > 0
                                                    select itemRecord.WorkOrderItem;
            if (roomSetComboBox.EditValue == null)
            {
                ShowMessage(Strings.SelectHousekeepingSetName);
                e.Cancel = true;
                return;
            }
            if (workOrderItemsWithNonZeroQuantity.Count() == 0)
            {
                ShowMessage(Strings.SpecifyWorkOrderItemRequestQuantity);
                e.Cancel = true;
                return;
            }
            WorkOrder.RoomLayout = (string)roomLayoutEdit.EditValue;
            WorkOrder.SetId = ((RoomSet)roomSetComboBox.SelectedItem).Id;
            WorkOrder.Items.Clear();
            WorkOrder.Items.AddRange(workOrderItemsWithNonZeroQuantity);
        }

        private TreeListNode RoomLayoutListHotTrackNode
        {
            get
            {
                return roomLayoutList.FocusedNode;
            }
            set
            {
                if (roomLayoutList.FocusedNode != value)
                {
                    var prevHotTrackNode = roomLayoutList.FocusedNode;
                    roomLayoutList.FocusedNode = value;
                    if (roomLayoutList.ActiveEditor != null)
                        roomLayoutList.PostEditor();
                    roomLayoutList.InvalidateNode(prevHotTrackNode);
                    roomLayoutList.InvalidateNode(roomLayoutList.FocusedNode);
                }
            }
        }
        private void roomLayoutEdit_QueryResultValue(object sender, DevExpress.XtraEditors.Controls.QueryResultValueEventArgs e)
        {
            e.Value = RoomLayoutListHotTrackNode != null ? RoomLayoutListHotTrackNode[roomLayoutNameColumn] : null;
        }

        private void roomLayoutEdit_QueryPopUp(object sender, CancelEventArgs e)
        {
            //roomLayoutList.ForceInitialize();
            var name = roomLayoutEdit.EditValue as string;
            RoomLayoutListHotTrackNode = roomLayoutList.FindNodeByFieldValue("Name", name);
        }

        private void roomLayoutList_MouseMove(object sender, MouseEventArgs e)
        {
            var treelist = sender as TreeList;
            var info = treelist.CalcHitInfo(new Point(e.X, e.Y));

            if (info.HitInfoType == HitInfoType.Cell)
                RoomLayoutListHotTrackNode = info.Node;
        }

        private void roomLayoutList_Click(object sender, EventArgs e)
        {
            //if (RoomLayoutListHotTrackNode != null)
            //{
            //    roomLayoutEdit.EditValue = RoomLayoutListHotTrackNode[roomLayoutNameColumn];
            //}
            //roomLayoutEdit.ClosePopup();
        }

        private void roomLayoutList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                roomLayoutList_Click(sender, EventArgs.Empty);
            }
        }

        private void roomLayoutList_MouseClick(object sender, MouseEventArgs e)
        {
            var treelist = sender as TreeList;
            var info = treelist.CalcHitInfo(new Point(e.X, e.Y));
            if (info.HitInfoType == HitInfoType.Cell)
            {
                if (RoomLayoutListHotTrackNode != null)
                {
                    roomLayoutEdit.EditValue = RoomLayoutListHotTrackNode[roomLayoutNameColumn];
                }
                roomLayoutEdit.ClosePopup();
            }
        }
    }
}
