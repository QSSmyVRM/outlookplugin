﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System.Collections.Generic;
using MyVrm.Common.ComponentModel;
namespace MyVrm.WebServices.Data
{
    public class RoomIdCollection : ComplexPropertyCollection<ConfRoom> //ComplexPropertyCollection<RoomId>
    {
        private readonly List<ConfRoom> _rooms = new List<ConfRoom>();
        public override object Clone()
        {
            RoomIdCollection copy = new RoomIdCollection();
            foreach (var participant in Items)
            {
                ConfRoom copyPart = (ConfRoom)participant.Clone();
                copy.Add(copyPart);
            }
            return copy;
        }

        public void Add(ConfRoom participant)
        {
            InternalAdd(participant);
        }

        public void AddRange(IEnumerable<ConfRoom> participants)
        {
            InternalAddRange(participants);
        }

        public void Insert(int index, ConfRoom participant)
        {
            //Items.Insert(index, participant);
            InternalInsert(index, participant);
        }

        public void Remove(ConfRoom participant)
        {
            InternalRemove(participant);
        }
        public void Remove(DataList<ConfRoom> COnfRoom)
        {
            foreach (var participant in COnfRoom)
            {
                InternalRemove(participant);
            }
        }
        public void RemoveAt(int index)
       {
            var participant = Items[index];
            Remove(participant);
        }
        public void RemoveR(Conference conference)
        {
         // public void RemoVeURooms(this)
        
            DataList<ConfRoom> cfroom = new DataList<ConfRoom>();
            foreach (var cRoom in conference.Rooms)
            {
                if (cRoom.ConfEndPt.Id.Id == "0")
                {
                    cfroom.Add(cRoom);
                }
            }
            conference.Rooms.Remove(cfroom);
        
        }
        public void Clear()
        {
            InternalClear();
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public void CopyTo(ConfRoom[] array, int arrayIndex)
        {
            _rooms.CopyTo(array, arrayIndex);
        }

        #region Overrides of ComplexPropertyCollection<ConferenceRoom>

        internal override ConfRoom CreateComplexProperty(string xmlElementName)
        {
            return new ConfRoom();
        }
        internal override void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
        {
            reader.EnsureCurrentNodeIsStartElement(XmlNamespace.NotSpecified, xmlElementName);
            do
            {
                reader.Read();
                if (reader.IsStartElement(XmlNamespace.NotSpecified, "selected"))
                {
                    do
                    {
                        //reader.ReadElementValue();
                        //reader = reader.Read();
                        reader.Read();
                        switch (reader.LocalName)
                        {
                            case "level1ID":
                                // CRoom = new ConfRoom();
                                var ConfRoom = new ConfRoom();
                                ConfRoom.ConfEndPt = new ConferenceEndpoint();
                                ConfRoom.ConfEndPt.Id = new RoomId();
                                ConfRoom.ConfEndPt.Id.LoadFromXml(reader, "level1ID");
                                ConfRoom.Id = new RoomId(ConfRoom.ConfEndPt.Id.Id);
                                _rooms.Add(ConfRoom);
                                Items.Add(ConfRoom);
                                break;
                        }
                       
                       // ConfRoom.LoadFromXml(reader, XmlNamespace.NotSpecified, "selected");
                       
                    } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "selected"));

                    //base.LoadFromXml(reader, "selected");
                }
                else
                {
                    reader.SkipCurrentElement();
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, xmlElementName));
        }

        internal override string GetCollectionItemXmlElementName(ConfRoom complexProperty)
        {
            return "Location";
        }
        #endregion 
    }
}
