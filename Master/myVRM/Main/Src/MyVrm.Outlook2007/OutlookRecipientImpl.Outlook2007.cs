﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Outlook;
using Microsoft.Office.Interop.Outlook.Extensions;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook
{
    internal partial class OutlookRecipientImpl
    {
        public override string SmtpAddress
        {
            get
            {
                const string PR_SMTP_ADDRESS = "http://schemas.microsoft.com/mapi/proptag/0x39FE001E";
                if (_smtpAddress == null)
                {
                    if (!_recipient.Resolved)
                    {
                        return null;
                    }
                    var propertyAccessor = _recipient.PropertyAccessor;
                    try
                    {
                        _smtpAddress = propertyAccessor.GetProperty(PR_SMTP_ADDRESS).ToString();
                    }
                    catch
                    {

                        return null;
                    }
                }
                return _smtpAddress;
            }
        }
        protected override List<FreeBusyInfo> GetFreeBusyFromDefaultCalendarFolder(DateTime start, DateTime end)
        {
            var freeBusyInfos = new List<FreeBusyInfo>();
            if (_calendarFolder == null)
            {
                if (_recipient.Resolved)
                {
                    MyVrmAddin.TraceSource.TraceInformation("GetFreeBusyFromDefaultCalendarFolder: GetNamespace");
                    var nameSpace = _recipient.Application.GetNamespace("MAPI");
                    try
                    {
                        if (_recipient.Application.Session.CurrentUser.EntryID == _recipient.EntryID)
                        {
                            MyVrmAddin.TraceSource.TraceInformation("GetFreeBusyFromDefaultCalendarFolder: GetDefaultFolder");
                            _calendarFolder =
                                nameSpace.GetDefaultFolder(
                                    OlDefaultFolders.olFolderCalendar);
                        }
                        else
                        {
                            var addressEntry = _recipient.AddressEntry;
                            if (addressEntry.AddressEntryUserType == OlAddressEntryUserType.olExchangeUserAddressEntry)
                            {
                                MyVrmAddin.TraceSource.TraceInformation("GetFreeBusyFromDefaultCalendarFolder: GetSharedDefaultFolder");
                                _calendarFolder =
                                    nameSpace.GetSharedDefaultFolder(_recipient,
                                                                     OlDefaultFolders.
                                                                         olFolderCalendar);
                            }
                        }
                    }
                    finally
                    {
                        Marshal.ReleaseComObject(nameSpace);
                    }
                }
            }
            if (_calendarFolder != null)
            {
                MyVrmAddin.TraceSource.TraceInformation("GetFreeBusyFromDefaultCalendarFolder: foreach AppointmentItems");

                var calendarFolder = ((Folder)_calendarFolder);
                var appointments = from item in calendarFolder.Items.AsQueryable<ExtendedAppointment>()
                                   where item.Start >= start && item.End <= end
                                   select item;
                foreach (var appointment in appointments)
                {
                    using (var outlookAppointmentImpl = new OutlookAppointmentImpl(appointment.Item))
                    {
                        if (outlookAppointmentImpl.IsRecurring)
                        {
                            var recurrencePattern =
                                outlookAppointmentImpl.GetRecurrencePattern(outlookAppointmentImpl.StartTimeZone);
                            IEnumerable<OccurrenceInfo> occurrenceInfos =
                                recurrencePattern.GetOccurrenceInfoList(start, end);
                            freeBusyInfos.AddRange(
                                occurrenceInfos.Select(
                                    occurrenceInfo =>
                                    new FreeBusyInfo(outlookAppointmentImpl.BusyStatus, occurrenceInfo.Start,
                                                     occurrenceInfo.End, outlookAppointmentImpl.Subject)));
                        }
                        else
                        {
                            if (outlookAppointmentImpl.Start >= start && outlookAppointmentImpl.End <= end)
                            {
                                freeBusyInfos.Add(new FreeBusyInfo(outlookAppointmentImpl.BusyStatus,
                                                                   outlookAppointmentImpl.Start,
                                                                   outlookAppointmentImpl.End,
                                                                   outlookAppointmentImpl.Subject));
                            }
                        }
                    }
                }
            }
            return freeBusyInfos;
        }
    }
}