﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace CallWebApi.Classes
{
    /// <summary>
    /// Base class for all responses
    /// </summary>
    public class BaseResponse
    {
        /// <summary>
        /// Indicate, if auth request was success
        /// </summary>
        public bool Success { set; get; }

        /// <summary>
        /// Message from auth action
        /// </summary>
        public string Message { set; get; }

        /// <summary>
        /// Code
        /// </summary>
        public int Code { set; get; }

        /// <summary>
        /// Tenant's settings version
        /// </summary>
        public long SettingsVersion { set; get; }
    }
}