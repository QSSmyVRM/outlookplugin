﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	//Request
	class SetPrivatePublicRoomProfileRequest : ServiceRequestBase<SetPrivatePublicRoomProfileResponse>
	{
		internal WhyGoManagedRoom ManagedRoom;
		internal SetPrivatePublicRoomProfileRequest(MyVrmService service)
			: base(service)
		{
		}

		#region Overrides of ServiceRequestBase<SetPrivatePublicRoomProfileResponse>

		internal override string GetXmlElementName()
		{
			return Constants.SetPrivatePublicRoomProfileCommandName;
		}

		internal override string GetCommandName()
		{
			return Constants.SetPrivatePublicRoomProfileCommandName;
		}

		internal override string GetResponseXmlElementName()
		{
			return "success";
		}

		internal override SetPrivatePublicRoomProfileResponse ParseResponse(MyVrmServiceXmlReader reader)
		{
			var response = new SetPrivatePublicRoomProfileResponse();
			response.ReadElementsFromXml(reader);
			return response;
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			if (UserId != null)
				UserId.WriteToXml(writer, "userID");
			if( ManagedRoom != null)
				ManagedRoom.WriteElementsToXml(writer);
		}

		#endregion
	}

	//Response
	public class SetPrivatePublicRoomProfileResponse : ServiceResponse
	{
		public bool Success { get; set; }

		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{
			Success = Utilities.BoolStringToBool(reader.ReadValue());
		}
	}
}