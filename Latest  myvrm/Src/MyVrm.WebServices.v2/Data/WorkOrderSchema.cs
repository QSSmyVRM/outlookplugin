﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    internal class WorkOrderSchema : WorkOrderBaseSchema
    {
        public static readonly PropertyDefinition Type;
        public static readonly PropertyDefinition RoomLayout;
        public static readonly PropertyDefinition SetId;
        public static readonly PropertyDefinition AdministratorId;
        public static readonly PropertyDefinition StartDate;
        public static readonly PropertyDefinition StartTime;
        public static readonly PropertyDefinition CompleteDate;
        public static readonly PropertyDefinition CompleteTime;
        public static readonly PropertyDefinition Timezone;
        public static readonly PropertyDefinition Status;
        public static readonly PropertyDefinition DeliveryType;
        public static readonly PropertyDefinition DeliveryCost;
        public static readonly PropertyDefinition ServiceCharge;
        public static readonly PropertyDefinition Comments;
        public static readonly PropertyDefinition Description;
        public static readonly PropertyDefinition TotalCost;
        public static readonly PropertyDefinition Notify;
        public static readonly PropertyDefinition Reminder;
        public static readonly PropertyDefinition Items;
        public static readonly PropertyDefinition WOAdministratorId; //ZD 103364 

        internal static WorkOrderSchema Instance;

        static WorkOrderSchema()
        {
            Type = new GenericPropertyDefinition<WorkOrderType>("Type", PropertyDefinitionFlags.CanSet);
            RoomLayout = new StringPropertyDefinition("RoomLayout", PropertyDefinitionFlags.CanSet);
            SetId = new IntPropertyDefinition("SetID", PropertyDefinitionFlags.CanSet);
            AdministratorId = new ComplexPropertyDefinition<UserId>("AssignedToID", "AdminID", PropertyDefinitionFlags.CanSet,
                                                                    () => new UserId());
            WOAdministratorId = new IntPropertyDefinition("AdminID", PropertyDefinitionFlags.CanSet); ////ZD 103364 
            StartDate = new DatePropertyDefinition("StartByDate", PropertyDefinitionFlags.CanSet);
			StartTime = new TimePropertyDefinition("StartByTime", PropertyDefinitionFlags.CanSet, true);
            CompleteDate = new DatePropertyDefinition("CompletedByDate", PropertyDefinitionFlags.CanSet);
            CompleteTime = new TimePropertyDefinition("CompletedByTime", PropertyDefinitionFlags.CanSet, true);
            Timezone = new TimeZonePropertyDefinition("Timezone", PropertyDefinitionFlags.CanSet);
            Status = new GenericPropertyDefinition<WorkOrderStatus>("Status", PropertyDefinitionFlags.CanSet);
            DeliveryType = new GenericPropertyDefinition<WorkOrderDeliveryType>("DeliveryType",
                                                                                PropertyDefinitionFlags.CanSet);
            DeliveryCost = new DecimalPropertyDefinition("DeliveryCost", PropertyDefinitionFlags.CanSet);
            ServiceCharge = new DecimalPropertyDefinition("ServiceCharge", PropertyDefinitionFlags.CanSet);
            Comments = new StringPropertyDefinition("Comments", PropertyDefinitionFlags.CanSet);
            Description = new StringPropertyDefinition("Description", PropertyDefinitionFlags.CanSet);
            TotalCost = new DecimalPropertyDefinition("TotalCost", PropertyDefinitionFlags.CanSet);
            Notify = new BoolPropertyDefinition("Notify", PropertyDefinitionFlags.CanSet);
            Reminder = new BoolPropertyDefinition("Reminder", PropertyDefinitionFlags.CanSet);
            Items = new ComplexPropertyDefinition<WorkOrderItemCollection>("ItemList",
                                                                           PropertyDefinitionFlags.AutoInstantiateOnRead |
                                                                           PropertyDefinitionFlags.CanSet,
                                                                           () => new WorkOrderItemCollection());
            Instance = new WorkOrderSchema();
        }

        internal override void RegisterProperties()
        {
            base.RegisterProperties();
            RegisterProperty(Type);
            RegisterProperty(RoomLayout);
            RegisterProperty(SetId);
            RegisterProperty(AdministratorId);
            RegisterProperty(StartDate);
            RegisterProperty(StartTime);
            RegisterProperty(CompleteDate);
            RegisterProperty(CompleteTime);
            RegisterProperty(Timezone);
            RegisterProperty(Status);
            RegisterProperty(DeliveryType);
            RegisterProperty(DeliveryCost);
            RegisterProperty(ServiceCharge);
            RegisterProperty(Comments);
            RegisterProperty(Description);
            RegisterProperty(TotalCost);
            RegisterProperty(Notify);
            RegisterProperty(Reminder);
            RegisterProperty(Items);
            RegisterProperty(WOAdministratorId); //ZD 103364 
            
        }
    }
}

