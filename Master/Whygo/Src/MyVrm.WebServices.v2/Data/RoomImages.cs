﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;

namespace MyVrm.WebServices.Data
{
    [Serializable]
    public class RoomImages : ComplexProperty
    {
        private Image _map1;
        public Image Map1
        {
            get
            {
                if (_map1 == null)
                {
                    _map1 = new Image();
                }
                return _map1;
            }
            set { _map1 = value; }
        }

        private Image _map2;
        public Image Map2
        {
            get
            {
                if (_map2 == null)
                {
                    _map2 = new Image();
                }
                return _map2;
            }
            set { _map2 = value; }
        }

        private Image _security1;
        public Image Security1
        {
            get
            {
                if (_security1 == null)
                {
                    _security1 = new Image();
                }
                return _security1;
            }
            set { _security1 = value; }
        }

        private Image _security2;
        public Image Security2
        {
            get
            {
                if (_security2 == null)
                {
                    _security2 = new Image();
                }
                return _security2;
            }
            set { _security2 = value; }
        }

        private Image _misc1;
        public Image Misc1
        {
            get
            {
                if (_misc1 == null)
                {
                    _misc1 = new Image();
                }
                return _misc1;
            }
            set { _misc1 = value; }
        }

        private Image _misc2;
        public Image Misc2
        {
            get
            {
                if (_misc2 == null)
                {
                    _misc2 = new Image();
                }
                return _misc2;
            }
            set { _misc2 = value; }
        }

		public override object Clone()
		{
			RoomImages copy = new RoomImages();
			copy._map1 = new Image(_map1.Name, _map1.Data);
			copy._map2 = new Image(_map2.Name, _map2.Data);
			copy._misc1 = new Image(_misc1.Name, _misc1.Data);
			copy._misc2 = new Image(_misc2.Name, _misc2.Data);
			copy._security1 = new Image(_security1.Name, _security1.Data);
			copy._security2 = new Image(_security2.Name, _security2.Data);

			return copy;
		}

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            base.WriteElementsToXml(writer);
            WriteImage(writer, "Map1", "Map1Image", Map1);
            WriteImage(writer, "Map2", "Map2Image", Map2);
            WriteImage(writer, "Security1", "Security1Image", Security1);
            WriteImage(writer, "Security2", "Security2Image", Security2);
            WriteImage(writer, "Misc1", "Misc1Image", Misc1);
            WriteImage(writer, "Misc2", "Misc2Image", Misc2);
        }

        internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
        {
            switch(reader.LocalName)
            {
                case "Map1":
                {
                    Map1.Name = reader.ReadValue();
                    return true;
                }
                case "Map1Image":
                {
                    Map1.Data = reader.ReadBase64ElementValue();
                    return true;
                }
                case "Map2":
                {
                    Map2.Name = reader.ReadValue();
                    return true;
                }
                case "Map2Image":
                {
                    Map2.Data = reader.ReadBase64ElementValue();
                    return true;
                }
                case "Security1":
                {
                    Security1.Name = reader.ReadValue();
                    return true;
                }
                case "Security1Image":
                {
                    Security1.Data = reader.ReadBase64ElementValue();
                    return true;
                }
                case "Security2":
                {
                    Security2.Name = reader.ReadValue();
                    return true;
                }
                case "Security2Image":
                {
                    Security2.Data = reader.ReadBase64ElementValue();
                    return true;
                }
                case "Misc1":
                {
                    Misc1.Name = reader.ReadValue();
                    return true;
                }
                case "Misc1Image":
                {
                    Misc1.Data = reader.ReadBase64ElementValue();
                    return true;
                }
                case "Misc2":
                {
                    Misc2.Name = reader.ReadValue(); //ZD 102051
                    return true;
                }
                case "Misc2Image":
                {
                    Misc2.Data = reader.ReadBase64ElementValue(); //ZD 102051
                    return true;
                }
            }
            return false;
        }

        private static void WriteImage(MyVrmXmlWriter writer, string nameElementName, string imageElementName, Image image)
        {
            if (image != null)
            {
                writer.WriteElementValue(XmlNamespace.NotSpecified, nameElementName, image.Name);
                writer.WriteElementValue(XmlNamespace.NotSpecified, imageElementName, image.Data);
            }
            else
            {
                writer.WriteElementValue(XmlNamespace.NotSpecified, nameElementName, string.Empty);
                writer.WriteElementValue(XmlNamespace.NotSpecified, imageElementName, string.Empty);
            }
        }
    }
}
