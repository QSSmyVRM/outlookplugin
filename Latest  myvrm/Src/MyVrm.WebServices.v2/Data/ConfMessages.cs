﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	public class ConfMessage :ComplexProperty
	{
  //      <ConfMessage>
  //  <controlID>1</controlID>
  //  <textMessage>The conference will end in 5 minutes</textMessage>
  //  <msgduartion>5M</msgduartion>
  //</ConfMessage>

		public bool Checked { get; set; }
		public int ControlID { get; set; }
		public string TextMessage { get; set; }
		public string Duartion { get; set; } 

		public override object Clone()
		{
			ConfMessage copy = new ConfMessage();
			copy.ControlID = ControlID;
			copy.TextMessage = string.Copy(TextMessage);
			copy.Duartion = string.Copy(Duartion);
			return copy;
		}

		internal override void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
		{
			do
			{
				reader.Read();
				switch (reader.LocalName)
				{
					case "controlID":
						{
							ControlID = reader.ReadElementValue<int>(XmlNamespace.NotSpecified, "controlID");
							Checked = true; //????????
							break;
						}
					case "textMessage":
						{
							TextMessage = reader.ReadElementValue(XmlNamespace.NotSpecified, "textMessage");
							break;
						}
					case "msgduartion":
						{
							Duartion = reader.ReadElementValue(XmlNamespace.NotSpecified, "msgduartion");
							break;
						}
					default:
						{
							reader.SkipCurrentElement();
							break;
						}
				}
			} while (!reader.IsEndElement(XmlNamespace.NotSpecified, xmlElementName));
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			writer.WriteStartElement(XmlNamespace.NotSpecified, "ConfMessage");
			writer.WriteElementValue(XmlNamespace.NotSpecified, "controlID", ControlID);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "textMessage", TextMessage);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "msgduartion", Duartion);
			writer.WriteEndElement();
		}
	}
}