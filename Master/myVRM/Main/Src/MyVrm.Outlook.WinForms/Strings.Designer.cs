﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MyVrm.Outlook.WinForms {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Strings {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Strings() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("MyVrm.Outlook.WinForms.Strings", typeof(Strings).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Additional Information:.
        /// </summary>
        internal static string AboutDialogInfoText {
            get {
                return ResourceManager.GetString("AboutDialogInfoText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tech Support Contact:.
        /// </summary>
        internal static string AboutDialogSupportContactText {
            get {
                return ResourceManager.GetString("AboutDialogSupportContactText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tech Support Email:.
        /// </summary>
        internal static string AboutDialogSupportEmailText {
            get {
                return ResourceManager.GetString("AboutDialogSupportEmailText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tech Support Phone:.
        /// </summary>
        internal static string AboutDialogSupportPhoneText {
            get {
                return ResourceManager.GetString("AboutDialogSupportPhoneText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Version.
        /// </summary>
        internal static string AboutDialogVersionText {
            get {
                return ResourceManager.GetString("AboutDialogVersionText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to About.
        /// </summary>
        internal static string AboutLableText {
            get {
                return ResourceManager.GetString("AboutLableText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Accept.
        /// </summary>
        internal static string AcceptLicenseAgreementText {
            get {
                return ResourceManager.GetString("AcceptLicenseAgreementText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Alternate.
        /// </summary>
        internal static string Alternate {
            get {
                return ResourceManager.GetString("Alternate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The alternate selection has been enabled only for Rooms..
        /// </summary>
        internal static string Alternateselection {
            get {
                return ResourceManager.GetString("Alternateselection", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Append conference details to a meeting request.
        /// </summary>
        internal static string AppendConferenceDetailsToAMeetingRequestCaptionText {
            get {
                return ResourceManager.GetString("AppendConferenceDetailsToAMeetingRequestCaptionText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Apply.
        /// </summary>
        internal static string ApplyBtnText {
            get {
                return ResourceManager.GetString("ApplyBtnText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Approve.
        /// </summary>
        internal static string ApprovedTxt {
            get {
                return ResourceManager.GetString("ApprovedTxt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Audio Only.
        /// </summary>
        internal static string AudioOnlyTxt {
            get {
                return ResourceManager.GetString("AudioOnlyTxt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Audio/Video.
        /// </summary>
        internal static string AudioVideoTxt {
            get {
                return ResourceManager.GetString("AudioVideoTxt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Authenticate As.
        /// </summary>
        internal static string AuthenticateAsLableText {
            get {
                return ResourceManager.GetString("AuthenticateAsLableText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Change conference options.
        /// </summary>
        internal static string ChangeConferenceOptionsLableText {
            get {
                return ResourceManager.GetString("ChangeConferenceOptionsLableText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Clear Cache.
        /// </summary>
        internal static string ClearCacheLableText {
            get {
                return ResourceManager.GetString("ClearCacheLableText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Clear room&apos;s local cache.
        /// </summary>
        internal static string ClearRoomLocalCacheLableText {
            get {
                return ResourceManager.GetString("ClearRoomLocalCacheLableText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Comment.
        /// </summary>
        internal static string CommentHeaderTxt {
            get {
                return ResourceManager.GetString("CommentHeaderTxt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Conference Approve.
        /// </summary>
        internal static string ConferenceApproveCaptionTxt {
            get {
                return ResourceManager.GetString("ConferenceApproveCaptionTxt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Conference Data/Time.
        /// </summary>
        internal static string ConferenceDataTimeHeaderTxt {
            get {
                return ResourceManager.GetString("ConferenceDataTimeHeaderTxt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your conference has been scheduled successfully..
        /// </summary>
        internal static string ConferenceIsScheduledSuccessfully {
            get {
                return ResourceManager.GetString("ConferenceIsScheduledSuccessfully", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Conference.
        /// </summary>
        internal static string ConferenceLableText {
            get {
                return ResourceManager.GetString("ConferenceLableText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Conference Name.
        /// </summary>
        internal static string ConferenceNameHeaderTxt {
            get {
                return ResourceManager.GetString("ConferenceNameHeaderTxt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Conference name is required..
        /// </summary>
        internal static string ConferenceNameRequired {
            get {
                return ResourceManager.GetString("ConferenceNameRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Conference Options.
        /// </summary>
        internal static string ConferenceOptionsLableText {
            get {
                return ResourceManager.GetString("ConferenceOptionsLableText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Conference Type.
        /// </summary>
        internal static string ConferenceTypeHeaderTxt {
            get {
                return ResourceManager.GetString("ConferenceTypeHeaderTxt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Need confirmation of deletion from server.
        /// </summary>
        internal static string ConfirmDeletionCaptionText {
            get {
                return ResourceManager.GetString("ConfirmDeletionCaptionText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Conflict Resolution Dialog.
        /// </summary>
        internal static string ConflictResolutionDialogLableText {
            get {
                return ResourceManager.GetString("ConflictResolutionDialogLableText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Conflict type:.
        /// </summary>
        internal static string ConflictType {
            get {
                return ResourceManager.GetString("ConflictType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Conference name:.
        /// </summary>
        internal static string ConfName {
            get {
                return ResourceManager.GetString("ConfName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Connection Options.
        /// </summary>
        internal static string ConnectionOptionsLableText {
            get {
                return ResourceManager.GetString("ConnectionOptionsLableText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dial-in to MCU.
        /// </summary>
        internal static string ConnectionTypeDialinToMCU {
            get {
                return ResourceManager.GetString("ConnectionTypeDialinToMCU", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dial-out from MCU.
        /// </summary>
        internal static string ConnectionTypeDialoutFromMCU {
            get {
                return ResourceManager.GetString("ConnectionTypeDialoutFromMCU", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Direct to MCU.
        /// </summary>
        internal static string ConnectionTypeDirectToMCU {
            get {
                return ResourceManager.GetString("ConnectionTypeDirectToMCU", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Current Windows User ({0}).
        /// </summary>
        internal static string CurrentWindowsUserCaptionText {
            get {
                return ResourceManager.GetString("CurrentWindowsUserCaptionText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Custom User.
        /// </summary>
        internal static string CustomUserCaptionText {
            get {
                return ResourceManager.GetString("CustomUserCaptionText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Decision.
        /// </summary>
        internal static string DecisionHeaderTxt {
            get {
                return ResourceManager.GetString("DecisionHeaderTxt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Decline.
        /// </summary>
        internal static string DeclineLicenseAgreementText {
            get {
                return ResourceManager.GetString("DeclineLicenseAgreementText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Template name field cannot be empty..
        /// </summary>
        internal static string DefineTemplateName {
            get {
                return ResourceManager.GetString("DefineTemplateName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Delete it from server by default without confirmation.
        /// </summary>
        internal static string DeleteNoConfirmCaptionText {
            get {
                return ResourceManager.GetString("DeleteNoConfirmCaptionText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to When deleting conference from Outlook.
        /// </summary>
        internal static string DelitionOptionsCaptionText {
            get {
                return ResourceManager.GetString("DelitionOptionsCaptionText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Deny.
        /// </summary>
        internal static string DeniedTxt {
            get {
                return ResourceManager.GetString("DeniedTxt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Description:.
        /// </summary>
        internal static string Description {
            get {
                return ResourceManager.GetString("Description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        internal static string DialogCancelButtonText {
            get {
                return ResourceManager.GetString("DialogCancelButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to OK.
        /// </summary>
        internal static string DialogOkButtonText {
            get {
                return ResourceManager.GetString("DialogOkButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Different Windows User.
        /// </summary>
        internal static string DifferentWindowsUserCaptionText {
            get {
                return ResourceManager.GetString("DifferentWindowsUserCaptionText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Domain:.
        /// </summary>
        internal static string DomainLableText {
            get {
                return ResourceManager.GetString("DomainLableText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Duration.
        /// </summary>
        internal static string DurationHeaderTxt {
            get {
                return ResourceManager.GetString("DurationHeaderTxt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit Template.
        /// </summary>
        internal static string EditTemplate {
            get {
                return ResourceManager.GetString("EditTemplate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Template Name:.
        /// </summary>
        internal static string EditTemplateName {
            get {
                return ResourceManager.GetString("EditTemplateName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to End:.
        /// </summary>
        internal static string End {
            get {
                return ResourceManager.GetString("End", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Endpoint Name.
        /// </summary>
        internal static string EndpointName {
            get {
                return ResourceManager.GetString("EndpointName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hotdesking.
        /// </summary>
        internal static string Hotdesking {
            get {
                return ResourceManager.GetString("Hotdesking", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Instances with conflicts:.
        /// </summary>
        internal static string InstancesWithConflictsLableText {
            get {
                return ResourceManager.GetString("InstancesWithConflictsLableText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Instance Type.
        /// </summary>
        internal static string InstanceTypeHeaderTxt {
            get {
                return ResourceManager.GetString("InstanceTypeHeaderTxt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Local Cache.
        /// </summary>
        internal static string LocalCacheCustomizationFormText {
            get {
                return ResourceManager.GetString("LocalCacheCustomizationFormText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Login:.
        /// </summary>
        internal static string LoginLableText {
            get {
                return ResourceManager.GetString("LoginLableText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to MCU approval.
        /// </summary>
        internal static string MCUApprovalTooltipTxt {
            get {
                return ResourceManager.GetString("MCUApprovalTooltipTxt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Name:.
        /// </summary>
        internal static string Name {
            get {
                return ResourceManager.GetString("Name", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Need Approve For.
        /// </summary>
        internal static string NeedApproveForHeaderTxt {
            get {
                return ResourceManager.GetString("NeedApproveForHeaderTxt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to no conflict.
        /// </summary>
        internal static string NoConflict {
            get {
                return ResourceManager.GetString("NoConflict", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No more conferences to approve..
        /// </summary>
        internal static string NoMoreConferencesToApproveMsg {
            get {
                return ResourceManager.GetString("NoMoreConferencesToApproveMsg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Normal conference.
        /// </summary>
        internal static string NormalConferenceTooltipTxt {
            get {
                return ResourceManager.GetString("NormalConferenceTooltipTxt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Do not delete it from server by default.
        /// </summary>
        internal static string NotDeleteNoConfirmCaptionText {
            get {
                return ResourceManager.GetString("NotDeleteNoConfirmCaptionText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Options.
        /// </summary>
        internal static string OptionsLableText {
            get {
                return ResourceManager.GetString("OptionsLableText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password:.
        /// </summary>
        internal static string PasswordLableText {
            get {
                return ResourceManager.GetString("PasswordLableText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Point-To-Point.
        /// </summary>
        internal static string PointToPointTxt {
            get {
                return ResourceManager.GetString("PointToPointTxt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please Wait! Work in progress....
        /// </summary>
        internal static string ProgressBarMsgTxt {
            get {
                return ResourceManager.GetString("ProgressBarMsgTxt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Public.
        /// </summary>
        internal static string Public {
            get {
                return ResourceManager.GetString("Public", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Makes template available for use by anyone in the organization..
        /// </summary>
        internal static string PublicTemplate {
            get {
                return ResourceManager.GetString("PublicTemplate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Recurrent conference occurence.
        /// </summary>
        internal static string RecurrentConferenceOccurenceTooltipTxt {
            get {
                return ResourceManager.GetString("RecurrentConferenceOccurenceTooltipTxt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Recurrent conference.
        /// </summary>
        internal static string RecurrentConferenceTooltipTxt {
            get {
                return ResourceManager.GetString("RecurrentConferenceTooltipTxt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Only one Room can be selected for Alternate Approval process..
        /// </summary>
        internal static string RoomAlternateselection {
            get {
                return ResourceManager.GetString("RoomAlternateselection", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Room approval.
        /// </summary>
        internal static string RoomApprovalTooltipTxt {
            get {
                return ResourceManager.GetString("RoomApprovalTooltipTxt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Room conflict.
        /// </summary>
        internal static string RoomConflict {
            get {
                return ResourceManager.GetString("RoomConflict", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Room Only.
        /// </summary>
        internal static string RoomOnlyTxt {
            get {
                return ResourceManager.GetString("RoomOnlyTxt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Room name.
        /// </summary>
        internal static string RSDRoomNameLableText {
            get {
                return ResourceManager.GetString("RSDRoomNameLableText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Set As Default.
        /// </summary>
        internal static string SetAsDefault {
            get {
                return ResourceManager.GetString("SetAsDefault", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Set up connection settings.
        /// </summary>
        internal static string SetUpConnectionSettingsLableText {
            get {
                return ResourceManager.GetString("SetUpConnectionSettingsLableText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Show popup when a conference is successfully scheduled.
        /// </summary>
        internal static string ShowPopupWhenAConferenceIsSuccessfullyScheduledCaptionText {
            get {
                return ResourceManager.GetString("ShowPopupWhenAConferenceIsSuccessfullyScheduledCaptionText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Start:.
        /// </summary>
        internal static string Start {
            get {
                return ResourceManager.GetString("Start", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to There are conflicts with previously set up conferences..
        /// </summary>
        internal static string StillHasConflictsMsg {
            get {
                return ResourceManager.GetString("StillHasConflictsMsg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to System approval.
        /// </summary>
        internal static string SystemApprovalTooltipTxt {
            get {
                return ResourceManager.GetString("SystemApprovalTooltipTxt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to System.
        /// </summary>
        internal static string SystemTxt {
            get {
                return ResourceManager.GetString("SystemTxt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Template with the same name is already exist. 
        ///The template name must be unique..
        /// </summary>
        internal static string TemplateAlreadyExists {
            get {
                return ResourceManager.GetString("TemplateAlreadyExists", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Test connection failed.
        ///Reason:
        ///{0}.
        /// </summary>
        internal static string TestConnectionFailed {
            get {
                return ResourceManager.GetString("TestConnectionFailed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Test connection.
        /// </summary>
        internal static string TestConnectionLableText {
            get {
                return ResourceManager.GetString("TestConnectionLableText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Test connection succeeded..
        /// </summary>
        internal static string TestConnectionSucceeded {
            get {
                return ResourceManager.GetString("TestConnectionSucceeded", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Time Zone conflict.
        /// </summary>
        internal static string TimeZoneConflict {
            get {
                return ResourceManager.GetString("TimeZoneConflict", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Blue Jeans.
        /// </summary>
        internal static string TxtBlueJeans {
            get {
                return ResourceManager.GetString("TxtBlueJeans", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to External.
        /// </summary>
        internal static string TxtExternal {
            get {
                return ResourceManager.GetString("TxtExternal", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Jabber.
        /// </summary>
        internal static string TxtJabber {
            get {
                return ResourceManager.GetString("TxtJabber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Lync.
        /// </summary>
        internal static string TxtLinc {
            get {
                return ResourceManager.GetString("TxtLinc", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No items.
        /// </summary>
        internal static string TxtNoItems {
            get {
                return ResourceManager.GetString("TxtNoItems", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to None.
        /// </summary>
        internal static string TxtNone {
            get {
                return ResourceManager.GetString("TxtNone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Personal.
        /// </summary>
        internal static string TxtPersonal {
            get {
                return ResourceManager.GetString("TxtPersonal", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Room.
        /// </summary>
        internal static string TxtRoom {
            get {
                return ResourceManager.GetString("TxtRoom", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Vidtel.
        /// </summary>
        internal static string TxtVidtel {
            get {
                return ResourceManager.GetString("TxtVidtel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Undecided.
        /// </summary>
        internal static string UndecidedTxt {
            get {
                return ResourceManager.GetString("UndecidedTxt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unique ID.
        /// </summary>
        internal static string UniqueIDHeaderTxt {
            get {
                return ResourceManager.GetString("UniqueIDHeaderTxt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User:.
        /// </summary>
        internal static string UserLableText {
            get {
                return ResourceManager.GetString("UserLableText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Virtual Meeting Room.
        /// </summary>
        internal static string VMRTxt {
            get {
                return ResourceManager.GetString("VMRTxt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Web Services.
        /// </summary>
        internal static string WebServicesLableText {
            get {
                return ResourceManager.GetString("WebServicesLableText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Web Service URL:.
        /// </summary>
        internal static string WebServiceURLLableText {
            get {
                return ResourceManager.GetString("WebServiceURLLableText", resourceCulture);
            }
        }
    }
}
