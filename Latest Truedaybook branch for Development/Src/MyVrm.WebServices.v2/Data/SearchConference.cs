﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	public enum ConfType
	{
		AudioOnly = 6,//1,
		AudioVideo = 2,
		RoomConference = 7,//3,
		PointToPoint = 4,
		VMR = 10
	}

	public enum TConferenceStatus
	{
		undefined = -1,
		Scheduled = 0,
		Pending = 1,
		Terminated = 3,
		Ongoing = 5,
		OnMCU = 6,
		Completed = 7,
		Deleted = 9
	}

	public enum TConferenceSearchType
	{
		undefined = -1,
		Ongoing = 1,
		Today = 2,
		ThisWeek = 3,
		ThisMonth = 4,
		Custom = 5,
		Yesterday = 6,
		Tomorrow = 7
	}
	public enum TPublic
	{
		undefined = -1,
		Private = 0,
		Public = 1
	}

	public enum TApprovalPending
	{
		//undefined = -1,
		NoApproval = 0,
		Approval = 1
	}
	public enum TRecurrenceStyle
	{
		//undefined = -1,
		Single = 0,
		Recurrent = 1
	}


	public enum TSelectionType
	{
		undefined = -1,
		None = 0,
		Any = 1,
		Selected = 2
	}

	public enum TSortBy
	{
		//undefined = -1,
		UniqueID = 1,
		ConferenceName = 2,
		ConferenceStartDateTime = 3
	}

	public struct SelectedRoom
	{
		public int Id;
		public string Name;
	}

	internal class SearchConferenceRequest : ServiceRequestBase<SearchConferenceResponse>
	{
		public string ConferenceId;
		public int ConfUniqueId;
		public TConferenceStatus ConferenceStatus;
		public TConferenceSearchType ConferenceSearchType;
		public string ConferenceName;
		public DateTime SearchingDateFrom;
		public DateTime SearchingDateTo;
		public string ConferenceHost;
		public int ConferenceParticipant; //code
		public TPublic Public; //NONE - Any
		public TApprovalPending ApprovalPending;
		public TRecurrenceStyle RecurrenceStyle;
		public TSelectionType SelectionType;
		public string SelectedRooms;
		public int PageNo;
		public TSortBy SortBy;
		public CustomAttributeCollection CustomAttributes;

		public SearchConferenceRequest(MyVrmService service)
			: base(service)
		{
			ConferenceId = string.Empty;
			ConfUniqueId = 0;
			ConferenceStatus = TConferenceStatus.Pending;
			ConferenceSearchType = TConferenceSearchType.undefined;
			ConferenceName = string.Empty;
			SearchingDateFrom = DateTime.MaxValue;
			SearchingDateTo = DateTime.MinValue;
			ConferenceHost = string.Empty;
			ConferenceParticipant = 0;
			Public = TPublic.undefined;
			ApprovalPending = TApprovalPending.Approval;
			RecurrenceStyle = TRecurrenceStyle.Single;
			PageNo = 1;
			SortBy = TSortBy.ConferenceStartDateTime;
			SelectionType = TSelectionType.Any;
			SelectedRooms = string.Empty;
			CustomAttributes = null;
		}

		#region Overrides of ServiceRequestBase<SearchConferenceResponse>

		internal override string GetXmlElementName()
		{
			return Constants.SearchConferenceCommandName;
		}

		internal override string GetCommandName()
		{
			return Constants.SearchConferenceCommandName; 
		}

		internal override string GetResponseXmlElementName()
		{
			return Constants.SearchConferenceCommandName; 
		}

		internal override SearchConferenceResponse ParseResponse(MyVrmServiceXmlReader reader)
		{
			var response = new SearchConferenceResponse();
			response.ReadElementsFromXml(reader);
			return response;
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			if (OrganizationId != null)
				OrganizationId.WriteToXml(writer, "organizationID");
			if (UserId != null)
				UserId.WriteToXml(writer, "UserID");

			writer.WriteElementValue(XmlNamespace.NotSpecified, "ConferenceID", ConferenceId);

			if (ConfUniqueId > 0)
				writer.WriteElementValue(XmlNamespace.NotSpecified, "ConferenceUniqueID", ConfUniqueId);
			else
				writer.WriteElementValue(XmlNamespace.NotSpecified, "ConferenceUniqueID", null);
			
			writer.WriteStartElement(XmlNamespace.NotSpecified, "StatusFilter");
			writer.WriteElementValue(XmlNamespace.NotSpecified, "ConferenceStatus", ConferenceStatus);
            writer.WriteEndElement();

			writer.WriteElementValue(XmlNamespace.NotSpecified, "ConferenceName", ConferenceName );

			if (ConferenceSearchType != TConferenceSearchType.undefined)
				writer.WriteElementValue(XmlNamespace.NotSpecified, "ConferenceSearchType", ConferenceSearchType);
			else
				writer.WriteElementValue(XmlNamespace.NotSpecified, "ConferenceSearchType", null);

			if (SearchingDateFrom != DateTime.MaxValue)
				writer.WriteElementValue(XmlNamespace.NotSpecified, "DateFrom", SearchingDateFrom);
			else
				writer.WriteElementValue(XmlNamespace.NotSpecified, "DateFrom", null);

			if (SearchingDateTo != DateTime.MinValue)
				writer.WriteElementValue(XmlNamespace.NotSpecified, "DateTo", SearchingDateTo);
			else
				writer.WriteElementValue(XmlNamespace.NotSpecified, "DateTo", null);

			writer.WriteElementValue(XmlNamespace.NotSpecified, "ConferenceHost", ConferenceHost);
			if (ConferenceParticipant > 0)
				writer.WriteElementValue(XmlNamespace.NotSpecified, "ConferenceParticipant", ConferenceParticipant);
			else
				writer.WriteElementValue(XmlNamespace.NotSpecified, "ConferenceParticipant", null);

			if (Public != TPublic.undefined)
				writer.WriteElementValue(XmlNamespace.NotSpecified, "Public", Public);
			else 
				writer.WriteElementValue(XmlNamespace.NotSpecified, "Public", null);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "ApprovalPending", ApprovalPending);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "RecurrenceStyle", RecurrenceStyle);

			writer.WriteStartElement(XmlNamespace.NotSpecified, "Location");
			writer.WriteElementValue(XmlNamespace.NotSpecified, "SelectionType", SelectionType);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "SelectedRooms", SelectedRooms);
			writer.WriteEndElement();
			
			writer.WriteElementValue(XmlNamespace.NotSpecified, "PageNo", PageNo);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "SortBy", SortBy);
			if (CustomAttributes != null && CustomAttributes.Count > 0 )
			{
				CustomAttributes.WriteAttributesToXml(writer);
			}
		}

		#endregion
	}

	//Response
	public class SearchConferenceResponse : ServiceResponse
	{
		public List<ConfInfo> foundConfs = new List<ConfInfo>();
		//Read info 
		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{
			bool isEmplty = false;
			do
			{
				//Forward till "Conferences" element 
				while (!reader.IsStartElement(XmlNamespace.NotSpecified, "Conferences"))
					reader.Read();

				while (!reader.IsEndElement(XmlNamespace.NotSpecified, "Conferences"))
				{
					while (!reader.IsStartElement(XmlNamespace.NotSpecified, "Conference"))
					{
						reader.Read();
						if (reader.IsEndElement(XmlNamespace.NotSpecified, "Conferences")) //If there is no approving conferences
						{
							isEmplty = true;
							break;
						}
					}
					if (isEmplty) //If there is no approving conferences
						break; 

					if (reader.LocalName == "Conference")
					{
						var foundConfInfo = new ConfInfo();


						while (!reader.IsEndElement(XmlNamespace.NotSpecified, "Conference"))
						{
							foundConfInfo.TryReadElementFromXml(reader);
						}
						foundConfs.Add(foundConfInfo);
					}
					reader.Read();
				}
			} while (!reader.IsEndElement(XmlNamespace.NotSpecified, "Conferences"));

			while (!reader.IsEndElement(XmlNamespace.NotSpecified, Constants.SearchConferenceCommandName))
			{
				reader.Read();
			}
		}
	}
}
