﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyVrm.Outlook.WinForms.Conference
{
    
    public partial class InstantConference : Form
    {
        private BindingList<MyVrm.WebServices.Data.UsersDatasource> _outlookUsers = new BindingList<MyVrm.WebServices.Data.UsersDatasource>();
        public InstantConference(BindingList<MyVrm.WebServices.Data.UsersDatasource> outlookUsers)
        {
            InitializeComponent();
            _outlookUsers = outlookUsers;
            comboIds.Properties.Items.Add(Strings.Dynamic);
            comboIds.Properties.Items.Add(Strings.Static);

            comboIds.SelectedIndex = 0;

            //txtInvitees.BackColor = this.BackColor;

            this.Text = Strings.InstantConferenceCaption;

            txtInvitees.BackColor = this.BackColor;
        }

        private bool _hideRecipients = false;

        public bool HideRecipients
        {
            get { return _hideRecipients; }
            set { _hideRecipients = value; if (_hideRecipients) { txtInvitees.Visible = false; btnAddressBook.Visible = false; lblBottomText.Visible = false; lblinvitepartycipant.Visible = false; } } //ZD 101226
        }

        private bool  _isStatic;

        public bool IsStatic
        {
            get { return _isStatic; }
            set { _isStatic = value; }
        }

        //ZD 101226 Starts
        private bool _isimmediateconf;

        public bool immediateconf
        {
            get { return _isimmediateconf; }
            set { _isimmediateconf = value; }
        }
        //ZD 101226 Ends
        
        

        private void InstantConference_Load(object sender, EventArgs e)
        {
            
        }

        private void btnAddressBook_Click(object sender, EventArgs e)
        {
            using(Options.UsersDialog uDialog = new MyVrm.Outlook.WinForms.Options.UsersDialog(_outlookUsers))
            {
                if (uDialog.ShowDialog() == DialogResult.OK)
                {
                    List<MyVrm.WebServices.Data.UsersDatasource> selectedUsers =  uDialog.SelectedUsers;
                    
                    if (txtInvitees != null && txtInvitees.Text != null && txtInvitees.Text.Trim().Length > 0)
                        txtInvitees.Text += ";";
                    else
                        txtInvitees.Text = string.Empty;
                    foreach (MyVrm.WebServices.Data.UsersDatasource user in selectedUsers)
                    {
                        txtInvitees.Text += user.Email;
                        txtInvitees.Text += ";";
                    }
                }
            }
        }

        public string Recipients
        {
            get
            {
                return txtInvitees.Text;
            }
        }

        public bool EnableStatic
        {
            get
            {
                return comboIds.Properties.Items.Count == 2;
            }
            set
            {
                if (!value) comboIds.Properties.Items.RemoveAt(1);
            }
        }
        public delegate void AcceptedHandler(object sender, EventArgs e);
        public event AcceptedHandler Accepted;
        
        private void simpleButton2_Click(object sender, EventArgs e)
        {
            //ZD 101226 Starts
            if(!lblinvitepartycipant.Visible)
                _isimmediateconf = false;
            else
                _isimmediateconf = true; 
            //ZD 101226 Ends
            Cursor currentCursor = Cursors.WaitCursor;
            this.Cursor = Cursors.WaitCursor;
            if (comboIds.SelectedIndex == 0)
                _isStatic = false;
            else
                _isStatic = true;
            try
            {
                if (Accepted != null)
                {
                    EventArgs args = new EventArgs();
                    Accepted(this, args);
                }
                else //ZD 101226
                {
                   this.DialogResult = DialogResult.OK;
                   this.Close();
                }
            }
            catch(Exception ex)
            {
            }
            this.Cursor = currentCursor;
         
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
