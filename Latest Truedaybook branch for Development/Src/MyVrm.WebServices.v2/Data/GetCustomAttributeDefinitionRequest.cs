﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    internal class GetCustomAttributeDefinitionRequest : ServiceRequestBase<GetCustomAttributeDefinitionResponse>
    {
        public GetCustomAttributeDefinitionRequest(MyVrmService service) : base(service)
        {
        }

        public CustomAttributeId Id { get; set; }

        #region Overrides of ServiceRequestBase<GetCustomAttributeDefinitionResponse>

        internal override string GetXmlElementName()
        {
            return "CustomAttribute";
        }

        internal override string GetCommandName()
        {
            return "GetCustomAttributeByID";
        }

        internal override string GetResponseXmlElementName()
        {
            return "CustomAttribute";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            Id.WriteToXml(writer, "ID");
            OrganizationId.WriteToXml(writer);
        }

        #endregion
    }
}
