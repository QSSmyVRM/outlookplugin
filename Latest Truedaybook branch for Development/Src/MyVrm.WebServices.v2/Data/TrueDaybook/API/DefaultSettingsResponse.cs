﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace CallWebApi.Classes
{
    /// <summary>
    /// Request with default setting of the company
    /// </summary>
    public class DefaultSettingsResponse : BaseResponse
    {
        /// <summary>
        /// Maximum available seats for this company
        /// </summary>
        public int MaximumAvailableSeats { set; get; }

        /// <summary>
        /// Default appointment subject text for this company
        /// </summary>
        public string DefaultAppointmentSubject { set; get; }

        /// <summary>
        /// Default appointment invitation text for this company
        /// </summary>
        public string DefaultAppointmentInvitation { set; get; }
    }
}