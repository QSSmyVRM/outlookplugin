﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.Outlook.WinForms.ApprovePendings
{
	partial class ConferenceApprovalDialog
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.treeList1 = new DevExpress.XtraTreeList.TreeList();
            this.uniqueIDColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.instanceTypeColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.conferenceNameColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.dataTimeColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.conferenceTypeColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.durationColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.approvalTypeColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.decisionColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryDecisionComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.commentColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryCommentTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).BeginInit();
            this.ContentPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryDecisionComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCommentTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // ContentPanel
            // 
            this.ContentPanel.Controls.Add(this.layoutControl1);
            this.ContentPanel.Size = new System.Drawing.Size(807, 322);
            // 
            // treeList1
            // 
            this.treeList1.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.uniqueIDColumn,
            this.instanceTypeColumn,
            this.conferenceNameColumn,
            this.dataTimeColumn,
            this.conferenceTypeColumn,
            this.durationColumn,
            this.approvalTypeColumn,
            this.decisionColumn,
            this.commentColumn});
            this.treeList1.Location = new System.Drawing.Point(2, 2);
            this.treeList1.Name = "treeList1";
            this.treeList1.OptionsView.ShowIndicator = false;
            this.treeList1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryDecisionComboBox,
            this.repositoryCommentTextEdit});
            this.treeList1.Size = new System.Drawing.Size(803, 318);
            this.treeList1.TabIndex = 0;
            this.treeList1.BeforeExpand += new DevExpress.XtraTreeList.BeforeExpandEventHandler(this.OnBeforeExpand);
            this.treeList1.AfterExpand += new DevExpress.XtraTreeList.NodeEventHandler(this.OnAfterExpand);
            this.treeList1.CustomDrawNodeCell += new DevExpress.XtraTreeList.CustomDrawNodeCellEventHandler(this.OnCustomDrawNodeCell);
            this.treeList1.CellValueChanging += new DevExpress.XtraTreeList.CellValueChangedEventHandler(this.treeList1_CellValueChanging);
            // 
            // uniqueIDColumn
            // 
            this.uniqueIDColumn.Caption = "Unique ID";
            this.uniqueIDColumn.FieldName = "ConfInfo.ConferenceUniqueId";
            this.uniqueIDColumn.Name = "uniqueIDColumn";
            this.uniqueIDColumn.OptionsColumn.AllowEdit = false;
            this.uniqueIDColumn.OptionsColumn.AllowFocus = false;
            this.uniqueIDColumn.OptionsColumn.ReadOnly = true;
            this.uniqueIDColumn.Visible = true;
            this.uniqueIDColumn.VisibleIndex = 0;
            this.uniqueIDColumn.Width = 93;
            // 
            // instanceTypeColumn
            // 
            this.instanceTypeColumn.Caption = "Instance Type";
            this.instanceTypeColumn.FieldName = "InstanceType";
            this.instanceTypeColumn.Name = "instanceTypeColumn";
            this.instanceTypeColumn.OptionsColumn.AllowEdit = false;
            this.instanceTypeColumn.OptionsColumn.AllowSort = false;
            this.instanceTypeColumn.OptionsColumn.ReadOnly = true;
            this.instanceTypeColumn.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // conferenceNameColumn
            // 
            this.conferenceNameColumn.Caption = "Conference Name";
            this.conferenceNameColumn.FieldName = "ConfInfo.ConferenceName";
            this.conferenceNameColumn.Name = "conferenceNameColumn";
            this.conferenceNameColumn.OptionsColumn.AllowEdit = false;
            this.conferenceNameColumn.OptionsColumn.AllowFocus = false;
            this.conferenceNameColumn.OptionsColumn.ReadOnly = true;
            this.conferenceNameColumn.Visible = true;
            this.conferenceNameColumn.VisibleIndex = 1;
            this.conferenceNameColumn.Width = 119;
            // 
            // dataTimeColumn
            // 
            this.dataTimeColumn.Caption = "Conference Data/Time";
            this.dataTimeColumn.FieldName = "ConfInfo.ConferenceDateTime";
            this.dataTimeColumn.Format.FormatString = "d";
            this.dataTimeColumn.Format.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dataTimeColumn.Name = "dataTimeColumn";
            this.dataTimeColumn.OptionsColumn.AllowEdit = false;
            this.dataTimeColumn.OptionsColumn.AllowFocus = false;
            this.dataTimeColumn.OptionsColumn.ReadOnly = true;
            this.dataTimeColumn.Visible = true;
            this.dataTimeColumn.VisibleIndex = 2;
            this.dataTimeColumn.Width = 152;
            // 
            // conferenceTypeColumn
            // 
            this.conferenceTypeColumn.Caption = "Conference Type";
            this.conferenceTypeColumn.FieldName = "ConfTypeName";
            this.conferenceTypeColumn.Name = "conferenceTypeColumn";
            this.conferenceTypeColumn.OptionsColumn.AllowEdit = false;
            this.conferenceTypeColumn.OptionsColumn.AllowFocus = false;
            this.conferenceTypeColumn.OptionsColumn.AllowSort = false;
            this.conferenceTypeColumn.OptionsColumn.ReadOnly = true;
            this.conferenceTypeColumn.Visible = true;
            this.conferenceTypeColumn.VisibleIndex = 3;
            this.conferenceTypeColumn.Width = 133;
            // 
            // durationColumn
            // 
            this.durationColumn.Caption = "Duration";
            this.durationColumn.FieldName = "ConfInfo.ConferenceDuration";
            this.durationColumn.Name = "durationColumn";
            this.durationColumn.OptionsColumn.AllowEdit = false;
            this.durationColumn.OptionsColumn.AllowFocus = false;
            this.durationColumn.OptionsColumn.AllowSort = false;
            this.durationColumn.OptionsColumn.ReadOnly = true;
            this.durationColumn.Visible = true;
            this.durationColumn.VisibleIndex = 4;
            this.durationColumn.Width = 71;
            // 
            // approvalTypeColumn
            // 
            this.approvalTypeColumn.Caption = "Need Approve For";
            this.approvalTypeColumn.FieldName = "AppUnit.Name";
            this.approvalTypeColumn.Name = "approvalTypeColumn";
            this.approvalTypeColumn.OptionsColumn.AllowEdit = false;
            this.approvalTypeColumn.OptionsColumn.AllowFocus = false;
            this.approvalTypeColumn.OptionsColumn.AllowSort = false;
            this.approvalTypeColumn.OptionsColumn.ReadOnly = true;
            this.approvalTypeColumn.Visible = true;
            this.approvalTypeColumn.VisibleIndex = 5;
            this.approvalTypeColumn.Width = 116;
            // 
            // decisionColumn
            // 
            this.decisionColumn.Caption = "Decision";
            this.decisionColumn.ColumnEdit = this.repositoryDecisionComboBox;
            this.decisionColumn.FieldName = "AppUnit.Decision";
            this.decisionColumn.Name = "decisionColumn";
            this.decisionColumn.OptionsColumn.AllowSort = false;
            this.decisionColumn.Visible = true;
            this.decisionColumn.VisibleIndex = 6;
            this.decisionColumn.Width = 117;
            // 
            // repositoryDecisionComboBox
            // 
            this.repositoryDecisionComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryDecisionComboBox.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Name1")});
            this.repositoryDecisionComboBox.Name = "repositoryDecisionComboBox";
            this.repositoryDecisionComboBox.ShowHeader = false;
            // 
            // commentColumn
            // 
            this.commentColumn.Caption = "Comment";
            this.commentColumn.ColumnEdit = this.repositoryCommentTextEdit;
            this.commentColumn.FieldName = "AppUnit.Comment";
            this.commentColumn.Fixed = DevExpress.XtraTreeList.Columns.FixedStyle.Right;
            this.commentColumn.Name = "commentColumn";
            this.commentColumn.OptionsColumn.AllowSort = false;
            this.commentColumn.Visible = true;
            this.commentColumn.VisibleIndex = 7;
            this.commentColumn.Width = 116;
            // 
            // repositoryCommentTextEdit
            // 
            this.repositoryCommentTextEdit.Name = "repositoryCommentTextEdit";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.treeList1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(807, 322);
            this.layoutControl1.TabIndex = 1;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(807, 322);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.treeList1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(104, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(807, 322);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // ConferenceApprovalDialog
            // 
            this.ApplyEnabled = true;
            this.ApplyVisible = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(831, 378);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "ConferenceApprovalDialog";
            this.Text = "Conference Approval";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnClosing);
            this.Load += new System.EventHandler(this.OnLoad);
            ((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).EndInit();
            this.ContentPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryDecisionComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryCommentTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);

		}

     

		#endregion

		private DevExpress.XtraTreeList.TreeList treeList1;
		private DevExpress.XtraTreeList.Columns.TreeListColumn uniqueIDColumn;
		private DevExpress.XtraTreeList.Columns.TreeListColumn conferenceNameColumn;
		private DevExpress.XtraTreeList.Columns.TreeListColumn dataTimeColumn;
		private DevExpress.XtraTreeList.Columns.TreeListColumn conferenceTypeColumn;
		private DevExpress.XtraTreeList.Columns.TreeListColumn durationColumn;
		private DevExpress.XtraTreeList.Columns.TreeListColumn approvalTypeColumn;
        //private DevExpress.XtraTreeList.Columns.TreeListColumn AlternateSelection;//ZD 101270
        //private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryalternateselection;  
		private DevExpress.XtraTreeList.Columns.TreeListColumn decisionColumn;

       
		private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryDecisionComboBox;
		private DevExpress.XtraTreeList.Columns.TreeListColumn commentColumn; 
		private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryCommentTextEdit;
		//private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
		//private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
		private DevExpress.XtraLayout.LayoutControl layoutControl1;


		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
		private DevExpress.XtraTreeList.Columns.TreeListColumn instanceTypeColumn;
	}
}
