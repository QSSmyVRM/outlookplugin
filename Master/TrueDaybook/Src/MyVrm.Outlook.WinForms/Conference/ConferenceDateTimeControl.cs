﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System.ComponentModel;
using System.Windows.Forms;

namespace MyVrm.Outlook.WinForms.Conference
{
    public partial class ConferenceDateTimeControl : ConferenceAwareControl
    {
        public ConferenceDateTimeControl()
        {
            InitializeComponent();
        	layoutControlItem14.Text = Strings.StartTimeLableText;
			layoutControlItem15.Text = Strings.EndTimeLableText;
        }


        protected override void OnReadOnlyChanged(ReadOnlyChangedEventArgs e)
        {
            startDateEdit.Enabled = startTimeEdit.Enabled = !e.ReadOnly;
            endDateEdit.Enabled = endTimeEdit.Enabled = !e.ReadOnly;
        }

        protected override void OnConferenceBindingSourceDataSourceChanged(System.EventArgs e)
        {
            Conference.PropertyChanged +=ConferencePropertyChanged;

            startDateEdit.Enabled = startTimeEdit.Enabled = !Conference.IsRecurring;
            endDateEdit.Enabled = endTimeEdit.Enabled = !Conference.IsRecurring;
        }

        private void ConferencePropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch(e.PropertyName)
            {
                case "IsRecurring":
                {
                    startDateEdit.Enabled = startTimeEdit.Enabled = !Conference.IsRecurring;
                    endDateEdit.Enabled = endTimeEdit.Enabled = !Conference.IsRecurring;
                    break;
                }
            }
        }

        void endTimeBinding_BindingComplete(object sender, BindingCompleteEventArgs e)
        {
            if (e.BindingCompleteState != BindingCompleteState.Success && e.BindingCompleteContext == BindingCompleteContext.DataSourceUpdate)
            {
                ShowError(e.ErrorText);
                e.Binding.ReadValue();
                e.Cancel = true;
            }
        }

        private void ConferenceDateTimeControl_Load(object sender, System.EventArgs e)
        {
            if (!DesignMode)
            {
                startDateEdit.DataBindings.Add("EditValue", ConferenceBindingSource, "StartDate", true,
                                               DataSourceUpdateMode.OnPropertyChanged);
                startTimeEdit.DataBindings.Add("EditValue", ConferenceBindingSource, "StartDate", true,
                                               DataSourceUpdateMode.OnPropertyChanged);
                Binding endDateBinding = endDateEdit.DataBindings.Add("EditValue", ConferenceBindingSource, "EndDate",
                                                                      true, DataSourceUpdateMode.OnPropertyChanged);
                endDateBinding.BindingComplete += endTimeBinding_BindingComplete;
                Binding endTimeBinding = endTimeEdit.DataBindings.Add("EditValue", ConferenceBindingSource, "EndDate",
                                                                      true, DataSourceUpdateMode.OnPropertyChanged);
                endTimeBinding.BindingComplete += endTimeBinding_BindingComplete;
            }
        }
    }
}