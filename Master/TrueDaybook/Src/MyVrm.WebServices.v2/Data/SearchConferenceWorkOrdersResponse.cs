﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace MyVrm.WebServices.Data
{
    internal class SearchConferenceWorkOrdersResponse : ServiceResponse
    {
        readonly List<WorkOrderId> _workOrderIds = new List<WorkOrderId>();

        internal ReadOnlyCollection<WorkOrderId> WorkOrderIds
        {
            get
            {
                return new ReadOnlyCollection<WorkOrderId>(_workOrderIds);
            }
        }

        internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
        {
            do
            {
                reader.Read();
                if (reader.LocalName == "WorkOrder")
                {
                    do
                    {
                        reader.Read();
                        if (reader.LocalName == "ID")
                        {
                            var workOrderId = new WorkOrderId();
                            workOrderId.LoadFromXml(reader, "ID");
                            _workOrderIds.Add(workOrderId);
                        }
                        else
                        {
                            reader.SkipCurrentElement();
                        }
                    } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "WorkOrder"));
                }
                else
                {
                    reader.SkipCurrentElement();
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "WorkOrderList"));
        }
    }
}
