﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using MyVrm.Common;

namespace MyVrm.WebServices.Data
{
    /// <summary>
    /// Specifies status of work order.
    /// </summary>
    public enum WorkOrderStatus
    {
        [LocalizedDescription("WorkOrderStatusPending", typeof(Strings))]
        Pending = 0,
        [LocalizedDescription("WorkOrderStatusCompleted", typeof(Strings))]
        Completed = 1
    }
    /// <summary>
    /// Specifies delivery type of work order.
    /// </summary>
    public enum WorkOrderDeliveryType
    {
        [LocalizedDescription("WorkOrderDeliverTypeCustom", typeof(Strings))]
        Custom = 0,
        [LocalizedDescription("WorkOrderDeliverTypeDeliveryAndPickup", typeof(Strings))]
        DeliveryAndPickup = 1,
        [LocalizedDescription("WorkOrderDeliverTypeDeliveryOnly", typeof(Strings))]
        DeliveryOnly = 2,
        [LocalizedDescription("WorkOrderDeliverTypePickupOnly", typeof(Strings))]
        PickupOnly = 3,
        [LocalizedDescription("WorkOrderDeliverTypeEquipmentPickup", typeof(Strings))]
        EquipmentPickup = 4
    }
    /// <summary>
    /// Specifies type of work order.
    /// </summary>
    public enum WorkOrderType
    {
        Inventory = 1,
        Catering = 2,
        Housekeeping = 3
    }
    /// <summary>
    /// Specifies search filter for MyVrmService.SearchConferenceWorkOrders method.
    /// </summary>
    public enum WorkOrderSearchFilter
    {
        /// <summary>
        /// View all work orders.
        /// </summary>
        All = -1,
        /// <summary>
        /// View the conference work orders.
        /// </summary>
        Conference = 0,
        /// <summary>
        /// View pending work orders.
        /// </summary>
        Pending = 1,
        /// <summary>
        /// View incomplete work orders.
        /// </summary>
        Incomplete = 2
    }

    /// <summary>
    /// Represents a audio-visual or housekeeping work order.
    /// </summary>
    [ServiceObjectDefinition("WorkOrder")]
    public class WorkOrder : WorkOrderBase
    {
		public new /*override*/ object Clone()//CopyTo(WorkOrder workOrder)
		{
			//if (workOrder == null)
			WorkOrder workOrder = new WorkOrder(Service);

			workOrder.Comments = Comments;
			workOrder.Complete = Complete;
			workOrder.CompleteDate = CompleteDate;
			workOrder.CompleteTime = CompleteTime;
			workOrder.DeliveryCost = DeliveryCost;
			workOrder.DeliveryType = DeliveryType;
			workOrder.Description = Description;

			if (Id != null)
			{
				WorkOrderId copyId = new WorkOrderId(Id.Id);
				//Id.SetFieldValue(ref copyId, Id);
				workOrder.PropertyBag[WorkOrderBaseSchema.Id] = copyId;
			}
			int i = 0;
			for (i = 0; i < Items.Count; i++)
			{
				WorkOrderItem item = //new WorkOrderItem();
				(WorkOrderItem)Items[i].Clone();//CopyTo(item);
				workOrder.Items.Add(item);
			}

			workOrder.Name = Name;
			workOrder.Notify = Notify;
			workOrder.Reminder = Reminder;
			workOrder.RoomId = RoomId;
			workOrder.RoomLayout = RoomLayout;
			workOrder.ServiceCharge = ServiceCharge;
			workOrder.SetId = SetId;
			workOrder.Start = Start;
			workOrder.StartDate = StartDate;
			workOrder.StartTime = StartTime;
			workOrder.Status = Status;
			workOrder.TimeZone = TimeZoneInfo.FindSystemTimeZoneById(TimeZone.Id);
			workOrder.TotalCost = TotalCost;
			workOrder.Type = Type;
			return workOrder;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(this, obj))
				return true;

			WorkOrder workOrder = obj as WorkOrder;
			if (workOrder == null)
				return false;
			if (workOrder.Comments != Comments)
				return false;
			if (workOrder.Complete != Complete)
				return false;
			if (workOrder.CompleteDate != CompleteDate)
				return false;
			if (workOrder.CompleteTime != CompleteTime)
				return false;
			if (workOrder.DeliveryCost != DeliveryCost)
				return false;
			if (workOrder.DeliveryType != DeliveryType)
				return false;
			if (workOrder.Description != Description)
				return false;
			if (workOrder.Id != Id)
				return false;
			//if (workOrder.IsDirty != IsDirty)
			//    return false;
			//if (workOrder.IsNew != IsNew)
			//    return false;

			if (workOrder.Items != null && Items == null || workOrder.Items == null && Items != null )
				return false;

			if (workOrder.Items != null && Items != null && workOrder.Items.Count != Items.Count)
				return false;
			for (int i = 0; workOrder.Items != null && Items != null /*extra condition*/ && i < workOrder.Items.Count; i++)
			{
				if (!workOrder.Items[i].Equals(Items[i]))
					return false;
			}
			
			if (workOrder.Name != Name)
				return false;
			if (workOrder.Notify != Notify)
				return false;
			if (workOrder.Reminder != Reminder)
				return false;
			if (workOrder.RoomId != RoomId)
				return false;
			if (workOrder.RoomLayout != RoomLayout)
				return false;
			if (workOrder.ServiceCharge != ServiceCharge)
				return false;
			if (workOrder.SetId != SetId)
				return false;
			if (workOrder.Start != Start)
				return false;
			if (workOrder.StartDate != StartDate)
				return false;
			if (workOrder.StartTime != StartTime)
				return false;
			if (workOrder.Status != Status)
				return false;
			if (!workOrder.TimeZone.Equals(TimeZone))
				return false;
			if (workOrder.TotalCost != TotalCost)
				return false;
			if (workOrder.Type != Type)
				return false;
			return true;
		}

        public WorkOrder(MyVrmService service) : base(service)
        {
        }

        public WorkOrder(MyVrmService service, RoomId roomId)
            : base(service)
        {
            RoomId = roomId;
        }

        public WorkOrderType Type
        {
            get
            {
                return (WorkOrderType)PropertyBag[WorkOrderSchema.Type];
            }
            set
            {
                PropertyBag[WorkOrderSchema.Type] = value; 
            }
        }
        /// <summary>
        /// Gets or sets name of room image or null for rooms with no dynamic layout.
        /// </summary>
        public string RoomLayout
        {
            get
            {
                return (string)PropertyBag[WorkOrderSchema.RoomLayout];
            }
            set
            {
                PropertyBag[WorkOrderSchema.RoomLayout] = value;
            }
        }

        public int SetId
        {
            get
            {
                int iRet = 0;
				if (PropertyBag[WorkOrderSchema.SetId] != null)
					int.TryParse(PropertyBag[WorkOrderSchema.SetId].ToString(), out iRet);
				return iRet; //(int)PropertyBag[WorkOrderSchema.SetId];
            }
            set
            {
                PropertyBag[WorkOrderSchema.SetId] = value;
            }
        }

        public UserId AdministratorId
        {
            get
            {
                return (UserId)PropertyBag[WorkOrderSchema.AdministratorId];
            }
            set
            {
                PropertyBag[WorkOrderSchema.AdministratorId] = value;
            }
        }

        public DateTime Start
        {
            get
            {
                return (StartDate + StartTime);
            }
            set
            {
                StartDate = value.Date;
                StartTime = value.TimeOfDay;
            }
        }

        public DateTime Complete
        {
            get
            {
                return CompleteDate + CompleteTime;
            }
            set
            {
                CompleteDate = value.Date;
                CompleteTime = value.TimeOfDay;
            }
        }

        public TimeZoneInfo TimeZone
        {
            get
            {
                return (TimeZoneInfo) PropertyBag[WorkOrderSchema.Timezone];
            }
            set
            {
                PropertyBag[WorkOrderSchema.Timezone] = value;
            }
        }

        public WorkOrderStatus Status
        {
            get
            {
                return (WorkOrderStatus)PropertyBag[WorkOrderSchema.Status];
            }
            set
            {
                PropertyBag[WorkOrderSchema.Status] = value;
            }
        }


        public WorkOrderDeliveryType DeliveryType
        {
            get
            {
                return (WorkOrderDeliveryType)PropertyBag[WorkOrderSchema.DeliveryType];
            }
            set
            {
                PropertyBag[WorkOrderSchema.DeliveryType] = value;
            }
        }

        public decimal DeliveryCost
        {
            get
            {
                return (decimal)PropertyBag[WorkOrderSchema.DeliveryCost];
            }
            set
            {
                PropertyBag[WorkOrderSchema.DeliveryCost] = value;
            }
        }

        public decimal ServiceCharge
        {
            get
            {
                return (decimal)PropertyBag[WorkOrderSchema.ServiceCharge];
            }
            set
            {
                PropertyBag[WorkOrderSchema.ServiceCharge] = value;
            }
        }

        public string Comments
        {
            get
            {
                return (string)PropertyBag[WorkOrderSchema.Comments];
            }
            set
            {
                PropertyBag[WorkOrderSchema.Comments] = value;
            }
        }

        public string Description
        {
            get
            {
                return (string)PropertyBag[WorkOrderSchema.Description];
            }
            set
            {
                PropertyBag[WorkOrderSchema.Description] = value;
            }
        }

        public decimal TotalCost
        {
            get
            {
                return (decimal)PropertyBag[WorkOrderSchema.TotalCost];
            }
            set
            {
                PropertyBag[WorkOrderSchema.TotalCost] = value;
            }
        }

        public bool Notify
        {
            get
            {
				bool bRet = false;
				if (PropertyBag[WorkOrderSchema.Notify] != null)
					bool.TryParse(PropertyBag[WorkOrderSchema.Notify].ToString(), out bRet);
				return bRet;//(bool)PropertyBag[WorkOrderSchema.Notify];
            }
            set
            {
                PropertyBag[WorkOrderSchema.Notify] = value;
            }
        }

        public bool Reminder
        {
            get
            {
				bool bRet = false;
				if (PropertyBag[WorkOrderSchema.Reminder] != null)
					bool.TryParse(PropertyBag[WorkOrderSchema.Reminder].ToString(), out bRet);
				return bRet;//(bool)PropertyBag[WorkOrderSchema.Reminder];
            }
            set
            {
                PropertyBag[WorkOrderSchema.Reminder] = value;
            }
        }

        public WorkOrderItemCollection Items
        {
            get
            {
                return (WorkOrderItemCollection)PropertyBag[WorkOrderSchema.Items];
            }
        }

        #region Overrides of ServiceObject

        internal override ServiceObjectSchema GetSchema()
        {
            return WorkOrderSchema.Instance;
        }

        #endregion

        private DateTime StartDate
        {
            get
            {
                return (DateTime)PropertyBag[WorkOrderSchema.StartDate];
            }
            set
            {
                PropertyBag[WorkOrderSchema.StartDate] = value;
            }
        }

        private TimeSpan StartTime
        {
            get
            {
                return (TimeSpan)PropertyBag[WorkOrderSchema.StartTime];
            }
            set
            {
                PropertyBag[WorkOrderSchema.StartTime] = value;
            }
        }

        private DateTime CompleteDate
        {
            get
            {
                return (DateTime)PropertyBag[WorkOrderSchema.CompleteDate];
            }
            set
            {
                PropertyBag[WorkOrderSchema.CompleteDate] = value;
            }
        }

        private TimeSpan CompleteTime
        {
            get
            {
                return (TimeSpan)PropertyBag[WorkOrderSchema.CompleteTime];
            }
            set
            {
                PropertyBag[WorkOrderSchema.CompleteTime] = value;
            }
        }

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
    }
}
