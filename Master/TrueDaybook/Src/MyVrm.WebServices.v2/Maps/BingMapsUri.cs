﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Text;

namespace MyVrm.WebServices.Maps
{
    public class BingMapsUri : MapsUri
    {
        private const string BaseAddress = "http://bing.com/maps/default.aspx";

        public override string ToString()
        {
            // Build search url to Bing Maps. 
            // See http://help.live.com/Help.aspx?market=en-US&project=WL_Local&querytype=topic&query=WL_LOCAL_PROC_BuildURL.htm
            var uriBuilder = new UriBuilder(BaseAddress);
            var queryBuilder = new StringBuilder();
            // Bing API version
            queryBuilder.Append("&v=2");
            // Center point
            if (!string.IsNullOrEmpty(Longitude) && !string.IsNullOrEmpty(Latitude))
            {
                queryBuilder.AppendFormat("&cp={0}~{1}", Latitude, Longitude);
            }
            //Location
            if (!string.IsNullOrEmpty(Location))
            {
                queryBuilder.AppendFormat("&where1={0}", Location);
            }
            // Zoom level
            queryBuilder.Append("&lvl=15");
            // Style 
            queryBuilder.Append("&style=r"); // Display a road view of the map.
            uriBuilder.Query = queryBuilder.ToString();
            return uriBuilder.ToString();
        }
    }
}