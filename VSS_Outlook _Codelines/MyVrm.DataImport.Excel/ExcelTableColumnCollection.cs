﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Excel;

namespace MyVrm.DataImport.Excel
{
    public class ExcelTableColumnCollection : ICollection<ExcelTableColumn>
    {
        private readonly List<ExcelTableColumn> _columns = new List<ExcelTableColumn>();

        public ExcelTableColumn Add(ListColumn listColumn)
        {
            var tableColumn = new ExcelTableColumn(listColumn);
            Add(tableColumn);
            return tableColumn;
        }

        public ExcelTableColumn Add(ListColumn listColumn, Type type)
        {
            var tableColumn = new ExcelTableColumn(listColumn, type);
            Add(tableColumn);
            return tableColumn;
        }

        public void AddRange(IEnumerable<ExcelTableColumn> columns)
        {
            _columns.AddRange(columns);
        }

        public ExcelTableColumn FindByListColumnIndex(int index)
        {
            return _columns.Find(match => match.Index == index);
        }

        public ExcelTableColumn this[ListColumn listColumn]
        {
            get
            {
                return FindByListColumnIndex(listColumn.Index);
            }
        }

        #region Implementation of IEnumerable

        public IEnumerator<ExcelTableColumn> GetEnumerator()
        {
            return _columns.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region Implementation of ICollection<ExcelTableColumn>

        public void Add(ExcelTableColumn item)
        {
            _columns.Add(item);
        }

        public void Clear()
        {
            _columns.Clear();
        }

        public bool Contains(ExcelTableColumn item)
        {
            return _columns.Contains(item);
        }

        public void CopyTo(ExcelTableColumn[] array, int arrayIndex)
        {
            _columns.CopyTo(array, arrayIndex);
        }

        public bool Remove(ExcelTableColumn item)
        {
            return _columns.Remove(item);
        }

        public int Count
        {
            get
            {
                return _columns.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        #endregion
    }
}
