﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    public sealed class UserName : ComplexProperty
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

		public override object Clone()
		{
			UserName copy = new UserName();
			copy.FirstName = string.Copy(FirstName);
			copy.LastName = string.Copy(LastName);

			return copy;
		}

        public override string ToString()
        {
            return string.Join(" ", new[] {FirstName, LastName});
        }

        internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
        {
            switch(reader.LocalName)
            {
                case "firstName":
                {
                    FirstName = reader.ReadValue();
                    return true;
                }
                case "lastName":
                {
                    LastName = reader.ReadValue();
                    return true;
                }
            }
            return false;
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            writer.WriteElementValue(XmlNamespace.NotSpecified, "firstName", FirstName);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "lastName", LastName);
        }
    }
}
