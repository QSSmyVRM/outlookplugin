﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;

namespace MyVrm.WebServices.Data
{
    /// <summary>
    /// Represents room approvers.
    /// </summary>
    [Serializable]
    public class RoomApprovers : ComplexProperty
    {
        private Approver _primaryApprover;

		public override object Clone()
		{
			RoomApprovers copy = new RoomApprovers();
			copy.PrimaryApprover = (Approver)PrimaryApprover.Clone();
			copy.SecondaryApprover1 = (Approver)SecondaryApprover1.Clone();
			copy.SecondaryApprover2 = (Approver)SecondaryApprover2.Clone();

			return copy;
		}
        /// <summary>
        /// Gets or sets the primary approver for the room.
        /// </summary>
        public Approver PrimaryApprover
        {
            get { return _primaryApprover ?? (_primaryApprover = new Approver()); }
            set { _primaryApprover = value; }
        }

        private Approver _secondaryApprover1;

        /// <summary>
        /// Gets or sets the secondary approver #1 for the room.
        /// </summary>
        /// <remarks>Approves use of room for a conference, if the primary approver not available.</remarks>
        public Approver SecondaryApprover1
        {
            get { return _secondaryApprover1 ?? (_secondaryApprover1 = new Approver()); }
            set { _secondaryApprover1 = value; }
        }

        private Approver _secondaryApprover2;

        /// <summary>
        /// Gets or sets the secondary approver #2 ID for the room.
        /// </summary>
        /// <remarks>Approves use of room for a conference, if the Primary Approver and Secondary Approver #1 are not available.</remarks>
        public Approver SecondaryApprover2
        {
            get { return _secondaryApprover2 ?? (_secondaryApprover2 = new Approver()); }
            set { _secondaryApprover2 = value; }
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            base.WriteElementsToXml(writer);
            if (PrimaryApprover.UserId == null)
            {
                writer.WriteElementValue(XmlNamespace.NotSpecified, "Approver1ID", null);
            }
            else
            {
                PrimaryApprover.UserId.WriteToXml(writer, "Approver1ID");
            }
            writer.WriteElementValue(XmlNamespace.NotSpecified, "Approver1Name", PrimaryApprover.Name);
            if (SecondaryApprover1.UserId == null)
            {
                writer.WriteElementValue(XmlNamespace.NotSpecified, "Approver2ID", null);
            }
            else
            {
                SecondaryApprover1.UserId.WriteToXml(writer, "Approver2ID");
            }
            writer.WriteElementValue(XmlNamespace.NotSpecified, "Approver2Name", SecondaryApprover1.Name);
            if (SecondaryApprover2.UserId == null)
            {
                writer.WriteElementValue(XmlNamespace.NotSpecified, "Approver3ID", null);
            }
            else
            {
                SecondaryApprover2.UserId.WriteToXml(writer, "Approver3ID");
            }
            writer.WriteElementValue(XmlNamespace.NotSpecified, "Approver3Name", SecondaryApprover2.Name);
        }

        internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
        {
            switch(reader.LocalName)
            {
                case "Approver1ID":
                {
                    PrimaryApprover.UserId = new UserId();
                    PrimaryApprover.UserId.LoadFromXml(reader, reader.LocalName);
                    return true;
                }
                case "Approver1Name":
                {
                    PrimaryApprover.Name = reader.ReadValue();
                    return true;
                }
                case "Approver2ID":
                {
                    SecondaryApprover1.UserId = new UserId();
                    SecondaryApprover1.UserId.LoadFromXml(reader, reader.LocalName);
                    return true;
                }
                case "Approver2Name":
                {
                    SecondaryApprover1.Name = reader.ReadValue();
                    return true;
                }
                case "Approver3ID":
                {
                    SecondaryApprover2.UserId = new UserId();
                    SecondaryApprover2.UserId.LoadFromXml(reader, reader.LocalName);
                    return true;
                }
                case "Approver3Name":
                {
                    SecondaryApprover2.Name = reader.ReadValue();
                    return true;
                }
            }
            return false;
        }

        public override string ToString()
        {
            var list = new List<string>();
            var str = PrimaryApprover.ToString();
            if (!string.IsNullOrEmpty(str))
            {
                list.Add(str);
            }
            str = SecondaryApprover1.ToString();
            if (!string.IsNullOrEmpty(str))
            {
                list.Add(str);
            }
            str = SecondaryApprover2.ToString();
            if (!string.IsNullOrEmpty(str))
            {
                list.Add(str);
            }

            return string.Join(", ", list.ToArray());
        }
    }
}
