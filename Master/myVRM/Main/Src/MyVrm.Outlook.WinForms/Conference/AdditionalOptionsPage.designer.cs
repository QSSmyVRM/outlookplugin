﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.Outlook.WinForms.Conference
{
    partial class AdditionalOptionsPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
			if (disposing && ConferenceBindingSource != null )
				ConferenceBindingSource.DataSourceChanged -= ConferenceBindingSource_DataSourceChanged;

            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdditionalOptionsPage));
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.textEntitycode = new DevExpress.XtraEditors.TextEdit();
            this.bnRemove = new DevExpress.XtraEditors.SimpleButton();
            this.bnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.listBoxVNOCUsers = new DevExpress.XtraEditors.ListBoxControl();
            this.treeListMessages = new DevExpress.XtraTreeList.TreeList();
            this.colState = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colMessages = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemComboBoxMessages = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.colTime = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.confMessageBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemComboBox_1_4_7_5Min = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox_2_5_8_2Min = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox_3_6_9_30Sec = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.checkEditDVNOC = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditConciergeMonitoring = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditMeetGreet = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditAVSupport = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lblEntitycode = new DevExpress.XtraEditors.LabelControl();
            this.specialInstructionsEdit = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItemEntitycode = new DevExpress.XtraLayout.LayoutControlItem();
            this.errorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);//ZD 103265
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEntitycode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxVNOCUsers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListMessages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxMessages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.confMessageBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox_1_4_7_5Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox_2_5_8_2Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox_3_6_9_30Sec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditDVNOC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditConciergeMonitoring.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditMeetGreet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAVSupport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.specialInstructionsEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemEntitycode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();//ZD 103265
            this.SuspendLayout();
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.AllowCustomizationMenu = false;
            this.dataLayoutControl1.Controls.Add(this.labelControl2);
            this.dataLayoutControl1.Controls.Add(this.textEntitycode);
            this.dataLayoutControl1.Controls.Add(this.bnRemove);
            this.dataLayoutControl1.Controls.Add(this.bnAdd);
            this.dataLayoutControl1.Controls.Add(this.listBoxVNOCUsers);
            this.dataLayoutControl1.Controls.Add(this.treeListMessages);
            this.dataLayoutControl1.Controls.Add(this.checkEditDVNOC);
            this.dataLayoutControl1.Controls.Add(this.checkEditConciergeMonitoring);
            this.dataLayoutControl1.Controls.Add(this.checkEditMeetGreet);
            this.dataLayoutControl1.Controls.Add(this.checkEditAVSupport);
            this.dataLayoutControl1.Controls.Add(this.labelControl1);
            this.dataLayoutControl1.Controls.Add(this.lblEntitycode);
            this.dataLayoutControl1.Controls.Add(this.specialInstructionsEdit);
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(601, 455);
            this.dataLayoutControl1.TabIndex = 0;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // labelControl2
            // 
            //this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl2.Location = new System.Drawing.Point(240, 160);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(331, 16);
            this.labelControl2.StyleController = this.dataLayoutControl1;
            this.labelControl2.TabIndex = 15;
            this.labelControl2.Text = "Message Overlay";
            // 
            // textEntitycode
            // 
            this.textEntitycode.Location = new System.Drawing.Point(112, 135);
            this.textEntitycode.MaximumSize = new System.Drawing.Size(150, 20);
            this.textEntitycode.MinimumSize = new System.Drawing.Size(150, 20);
            this.textEntitycode.Name = "textEntitycode";
            this.textEntitycode.Size = new System.Drawing.Size(150, 20);
            this.textEntitycode.StyleController = this.dataLayoutControl1;
            this.textEntitycode.TabIndex = 8;
            this.textEntitycode.TextChanged += new System.EventHandler(this.textEntitycode_EditValueChanged);
            //this.textEntitycode.LostFocus += textEntitycode_OnDefocus; 
            // 
            // bnRemove
            // 
            this.bnRemove.Appearance.Options.UseImage = true;
            this.bnRemove.Image = ((System.Drawing.Image)(resources.GetObject("bnRemove.Image")));
            this.bnRemove.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnRemove.Location = new System.Drawing.Point(200, 280);
            this.bnRemove.Name = "bnRemove";
            this.bnRemove.Size = new System.Drawing.Size(26, 23);
            this.bnRemove.StyleController = this.dataLayoutControl1;
            this.bnRemove.TabIndex = 14;
            this.bnRemove.Text = "simpleButton2";
            this.bnRemove.Click += new System.EventHandler(this.bnRemove_Click);
            // 
            // bnAdd
            // 
            this.bnAdd.Appearance.Options.UseImage = true;
            this.bnAdd.Image = ((System.Drawing.Image)(resources.GetObject("bnAdd.Image")));
            this.bnAdd.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.bnAdd.Location = new System.Drawing.Point(170, 280);
            this.bnAdd.Name = "bnAdd";
            this.bnAdd.Size = new System.Drawing.Size(26, 23);
            this.bnAdd.StyleController = this.dataLayoutControl1;
            this.bnAdd.TabIndex = 13;
            this.bnAdd.Text = "simpleButton1";
            this.bnAdd.Click += new System.EventHandler(this.bnAdd_Click);
            // 
            // listBoxVNOCUsers
            // 
            this.listBoxVNOCUsers.Location = new System.Drawing.Point(2, 280);
            this.listBoxVNOCUsers.Name = "listBoxVNOCUsers";
            this.listBoxVNOCUsers.Size = new System.Drawing.Size(164, 135);
            this.listBoxVNOCUsers.StyleController = this.dataLayoutControl1;
            this.listBoxVNOCUsers.TabIndex = 12;
            // 
            // treeListMessages
            // 
            this.treeListMessages.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colState,
            this.colMessages,
            this.colTime});
            this.treeListMessages.DataSource = this.confMessageBindingSource;
            this.treeListMessages.Location = new System.Drawing.Point(240, 180);
            this.treeListMessages.Name = "treeListMessages";
            this.treeListMessages.OptionsView.ShowIndicator = false;
            this.treeListMessages.OptionsView.ShowRoot = false;
            this.treeListMessages.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemComboBoxMessages,
            this.repositoryItemComboBox_1_4_7_5Min,
            this.repositoryItemComboBox_2_5_8_2Min,
            this.repositoryItemComboBox_3_6_9_30Sec,
            this.repositoryItemComboBox1});
            this.treeListMessages.Size = new System.Drawing.Size(331, 235);
            this.treeListMessages.TabIndex = 16;
            this.treeListMessages.CustomNodeCellEdit += new DevExpress.XtraTreeList.GetCustomNodeCellEditEventHandler(this.treeListMessages_CustomNodeCellEdit);
            this.treeListMessages.CellValueChanging += new DevExpress.XtraTreeList.CellValueChangedEventHandler(this.treeListMessages_CellValueChanging);
            // 
            // colState
            // 
            this.colState.AppearanceHeader.Options.UseBackColor = true;
            this.colState.Caption = " ";
            this.colState.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colState.FieldName = "Checked";
            this.colState.Name = "colState";
            this.colState.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.colState.OptionsColumn.FixedWidth = true;
            this.colState.Visible = true;
            this.colState.VisibleIndex = 0;
            this.colState.Width = 34;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // colMessages
            // 
            this.colMessages.AppearanceHeader.Options.UseTextOptions = true;
            this.colMessages.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMessages.Caption = "Message";
            this.colMessages.ColumnEdit = this.repositoryItemComboBoxMessages;
            this.colMessages.FieldName = "TextMessage";
            this.colMessages.Name = "colMessages";
            this.colMessages.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.colMessages.Visible = true;
            this.colMessages.VisibleIndex = 1;
            this.colMessages.Width = 213;
            // 
            // repositoryItemComboBoxMessages
            // 
            this.repositoryItemComboBoxMessages.AutoHeight = false;
            this.repositoryItemComboBoxMessages.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBoxMessages.Name = "repositoryItemComboBoxMessages";
            this.repositoryItemComboBoxMessages.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // colTime
            // 
            this.colTime.AppearanceHeader.Options.UseTextOptions = true;
            this.colTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTime.Caption = "Before End";
            this.colTime.ColumnEdit = this.repositoryItemComboBox1;
            this.colTime.FieldName = "Duartion";
            this.colTime.Name = "colTime";
            this.colTime.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.colTime.OptionsColumn.FixedWidth = true;
            this.colTime.Visible = true;
            this.colTime.VisibleIndex = 2;
            this.colTime.Width = 80;
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // confMessageBindingSource
            // 
            this.confMessageBindingSource.DataSource = typeof(MyVrm.WebServices.Data.ConfMessage);
            // 
            // repositoryItemComboBox_1_4_7_5Min
            // 
            this.repositoryItemComboBox_1_4_7_5Min.AutoHeight = false;
            this.repositoryItemComboBox_1_4_7_5Min.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox_1_4_7_5Min.Name = "repositoryItemComboBox_1_4_7_5Min";
            this.repositoryItemComboBox_1_4_7_5Min.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // repositoryItemComboBox_2_5_8_2Min
            // 
            this.repositoryItemComboBox_2_5_8_2Min.AutoHeight = false;
            this.repositoryItemComboBox_2_5_8_2Min.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox_2_5_8_2Min.Name = "repositoryItemComboBox_2_5_8_2Min";
            this.repositoryItemComboBox_2_5_8_2Min.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // repositoryItemComboBox_3_6_9_30Sec
            // 
            this.repositoryItemComboBox_3_6_9_30Sec.AutoHeight = false;
            this.repositoryItemComboBox_3_6_9_30Sec.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox_3_6_9_30Sec.Name = "repositoryItemComboBox_3_6_9_30Sec";
            this.repositoryItemComboBox_3_6_9_30Sec.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // checkEditDVNOC
            // 
            this.checkEditDVNOC.Location = new System.Drawing.Point(2, 255);
            this.checkEditDVNOC.Name = "checkEditDVNOC";
            this.checkEditDVNOC.Properties.Caption = "Dedicated VNOC Operator";
            this.checkEditDVNOC.Size = new System.Drawing.Size(224, 19);
            this.checkEditDVNOC.StyleController = this.dataLayoutControl1;
            this.checkEditDVNOC.TabIndex = 11;
            this.checkEditDVNOC.CheckStateChanged += new System.EventHandler(this.CheckStateChanged);
            // 
            // checkEditConciergeMonitoring
            // 
            this.checkEditConciergeMonitoring.Location = new System.Drawing.Point(2, 230);
            this.checkEditConciergeMonitoring.Name = "checkEditConciergeMonitoring";
            this.checkEditConciergeMonitoring.Properties.Caption = "Call Monitoring ";
            this.checkEditConciergeMonitoring.Size = new System.Drawing.Size(224, 19);
            this.checkEditConciergeMonitoring.StyleController = this.dataLayoutControl1;
            this.checkEditConciergeMonitoring.TabIndex = 10;
            this.checkEditConciergeMonitoring.CheckStateChanged += new System.EventHandler(this.CheckStateChanged);
            // 
            // checkEditMeetGreet
            // 
            this.checkEditMeetGreet.Location = new System.Drawing.Point(2, 205);
            this.checkEditMeetGreet.Name = "checkEditMeetGreet";
            this.checkEditMeetGreet.Properties.Caption = "Meet and Greet ";
            this.checkEditMeetGreet.Size = new System.Drawing.Size(224, 19);
            this.checkEditMeetGreet.StyleController = this.dataLayoutControl1;
            this.checkEditMeetGreet.TabIndex = 9;
            this.checkEditMeetGreet.CheckStateChanged += new System.EventHandler(this.CheckStateChanged);
            // 
            // checkEditAVSupport
            // 
            this.checkEditAVSupport.Location = new System.Drawing.Point(2, 180);
            this.checkEditAVSupport.Name = "checkEditAVSupport";
            this.checkEditAVSupport.Properties.Caption = "On-Site A/V Support ";
            this.checkEditAVSupport.Size = new System.Drawing.Size(224, 19);
            this.checkEditAVSupport.StyleController = this.dataLayoutControl1;
            this.checkEditAVSupport.TabIndex = 8;
            this.checkEditAVSupport.CheckStateChanged += new System.EventHandler(this.CheckStateChanged);

            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // labelControl1
            // 
            //this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl1.Location = new System.Drawing.Point(2, 160);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(224, 16);
            this.labelControl1.StyleController = this.dataLayoutControl1;
            this.labelControl1.TabIndex = 6;
            this.labelControl1.Text = "Conference Support";
            // 
            // specialInstructionsEdit
            // 
            this.specialInstructionsEdit.Location = new System.Drawing.Point(2, 42);
            this.specialInstructionsEdit.Name = "specialInstructionsEdit";
            this.specialInstructionsEdit.Size = new System.Drawing.Size(597, 89);
            this.specialInstructionsEdit.StyleController = this.dataLayoutControl1;
            this.specialInstructionsEdit.TabIndex = 5;
            this.specialInstructionsEdit.UseOptimizedRendering = true;
            this.specialInstructionsEdit.EditValueChanged += new System.EventHandler(this.specialInstructionsEdit_EditValueChanged);
            this.specialInstructionsEdit.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.specialInstructionsEdit_EditValueChanging);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem3,
            this.emptySpaceItem3,
            this.layoutControlItem11,
            this.emptySpaceItem4,
            this.emptySpaceItem5,
            this.layoutControlItemEntitycode});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(601, 455);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.specialInstructionsEdit;
            this.layoutControlItem2.CustomizationFormText = "Type instructions here";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(601, 109);
            this.layoutControlItem2.Text = "Type instructions here";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(107, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.emptySpaceItem1.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem1.CustomizationFormText = "Special Instructions";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(601, 24);
            this.emptySpaceItem1.Text = "Special Instructions";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(107, 0);
            this.emptySpaceItem1.TextVisible = true;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(168, 305);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(1, 1);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(60, 112);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.labelControl1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 158);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(0, 20);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(228, 20);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.checkEditAVSupport;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 178);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(0, 25);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(148, 25);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(228, 25);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.checkEditMeetGreet;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 203);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(0, 25);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(119, 25);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(228, 25);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.checkEditConciergeMonitoring;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 228);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(0, 25);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(152, 25);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(228, 25);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.checkEditDVNOC;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 253);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(0, 25);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(175, 25);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(228, 25);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.listBoxVNOCUsers;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 278);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(54, 20);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(168, 139);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.bnAdd;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(168, 278);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(30, 27);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(30, 27);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(30, 27);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.bnRemove;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(198, 278);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(30, 27);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(30, 27);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(30, 27);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "layoutControlItem10";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.labelControl2;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(238, 158);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 20);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(79, 20);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(335, 20);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 417);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(1, 1);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(601, 38);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.treeListMessages;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(238, 178);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(215, 24);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(335, 239);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "layoutControlItem11";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextToControlDistance = 0;
            this.layoutControlItem11.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(228, 158);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(1, 1);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(10, 259);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(573, 158);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(1, 1);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(28, 259);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);

            // 
            // lblEntitycode
            // 
            //this.lblEntitycode.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblEntitycode.Location = new System.Drawing.Point(280, 155);
            this.lblEntitycode.Name = "labelControl1";
            this.lblEntitycode.Size = new System.Drawing.Size(150, 20);
            this.lblEntitycode.StyleController = this.dataLayoutControl1;
            this.lblEntitycode.TabIndex = 6;
            this.lblEntitycode.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            //this.lblEntitycode.Appearance.Font =
            this.lblEntitycode.ForeColor = System.Drawing.Color.Red;
            //this.lblEntitycode.Text = "inavlid Entity code";
         
            // 
            // layoutControlItemEntitycode
            // 
            this.layoutControlItemEntitycode.Control = this.textEntitycode;
            this.layoutControlItemEntitycode.CustomizationFormText = "Entity Code";
            this.layoutControlItemEntitycode.Location = new System.Drawing.Point(0, 133);
            this.layoutControlItemEntitycode.MaxSize = new System.Drawing.Size(50, 25);
            this.layoutControlItemEntitycode.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItemEntitycode.Name = "layoutControlItemEntitycode";
            this.layoutControlItemEntitycode.Size = new System.Drawing.Size(601, 25);
            this.layoutControlItemEntitycode.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemEntitycode.Text = "Entity Code";
            this.layoutControlItemEntitycode.TextSize = new System.Drawing.Size(107, 13);
            this.layoutControlItemEntitycode.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            // 
            // AdditionalOptionsPage
            // 
            this.Controls.Add(this.dataLayoutControl1);
            this.Name = "AdditionalOptionsPage";
            this.Size = new System.Drawing.Size(601, 455);
            this.Load += new System.EventHandler(this.AdditionalInfoPage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEntitycode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxVNOCUsers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListMessages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxMessages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.confMessageBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox_1_4_7_5Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox_2_5_8_2Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox_3_6_9_30Sec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditDVNOC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditConciergeMonitoring.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditMeetGreet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAVSupport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.specialInstructionsEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemEntitycode)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
		private DevExpress.XtraEditors.CheckEdit checkEditDVNOC;
		private DevExpress.XtraEditors.CheckEdit checkEditConciergeMonitoring;
		private DevExpress.XtraEditors.CheckEdit checkEditMeetGreet;
		private DevExpress.XtraEditors.CheckEdit checkEditAVSupport;
		private DevExpress.XtraEditors.LabelControl labelControl1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
		private DevExpress.XtraEditors.ListBoxControl listBoxVNOCUsers;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
		private DevExpress.XtraEditors.SimpleButton bnRemove;
		private DevExpress.XtraEditors.SimpleButton bnAdd;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
		private DevExpress.XtraEditors.LabelControl labelControl2;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
		private DevExpress.XtraTreeList.TreeList treeListMessages;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
		private DevExpress.XtraTreeList.Columns.TreeListColumn colState;
		private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
		private DevExpress.XtraTreeList.Columns.TreeListColumn colMessages;
		private DevExpress.XtraTreeList.Columns.TreeListColumn colTime;
		private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBoxMessages;
		private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox_1_4_7_5Min;
		private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox_2_5_8_2Min;
		private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox_3_6_9_30Sec;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
		private System.Windows.Forms.BindingSource confMessageBindingSource;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
		private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraEditors.MemoEdit specialInstructionsEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemEntitycode;
        private DevExpress.XtraEditors.LabelControl lblEntitycode;
        //private DevExpress.XtraEditors.ComboBoxEdit EntitycodeComboBox;
        private DevExpress.XtraEditors.TextEdit textEntitycode;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider errorProvider;
    }
}
