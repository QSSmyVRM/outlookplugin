﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.Outlook.WinForms
{
    partial class AboutDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
			this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
			this.extPhoneLabelControl = new DevExpress.XtraEditors.LabelControl();
			this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
			this.copyrightEdit = new DevExpress.XtraEditors.LabelControl();
			this.additionInfoEdit = new DevExpress.XtraEditors.MemoEdit();
			this.productLabel = new DevExpress.XtraEditors.LabelControl();
			this.contactPhoneEdit = new DevExpress.XtraEditors.LabelControl();
			this.contactEmailEdit = new DevExpress.XtraEditors.HyperLinkEdit();
			this.contactNameEdit = new DevExpress.XtraEditors.LabelControl();
			this.versionLabel = new DevExpress.XtraEditors.LabelControl();
			this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.versionLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
			this.supportEmailLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
			this.supportPhoneLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
			this.supportContactLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
			this.infoLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
			this.extSupportPhoneLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
			((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).BeginInit();
			this.ContentPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
			this.layoutControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
			this.layoutControl2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.additionInfoEdit.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.contactEmailEdit.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.versionLayoutControlItem)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.supportEmailLayoutControlItem)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.supportPhoneLayoutControlItem)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.supportContactLayoutControlItem)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.infoLayoutControlItem)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.extSupportPhoneLayoutControlItem)).BeginInit();
			this.SuspendLayout();
			// 
			// ContentPanel
			// 
			this.ContentPanel.Controls.Add(this.layoutControl1);
			this.ContentPanel.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
			this.ContentPanel.Size = new System.Drawing.Size(387, 358);
			// 
			// layoutControl1
			// 
			this.layoutControl1.AllowCustomizationMenu = false;
			this.layoutControl1.Controls.Add(this.layoutControl2);
			this.layoutControl1.Controls.Add(this.copyrightEdit);
			this.layoutControl1.Controls.Add(this.additionInfoEdit);
			this.layoutControl1.Controls.Add(this.productLabel);
			this.layoutControl1.Controls.Add(this.contactPhoneEdit);
			this.layoutControl1.Controls.Add(this.contactEmailEdit);
			this.layoutControl1.Controls.Add(this.contactNameEdit);
			this.layoutControl1.Controls.Add(this.versionLabel);
			this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.layoutControl1.Location = new System.Drawing.Point(0, 0);
			this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.layoutControl1.Name = "layoutControl1";
			this.layoutControl1.Root = this.layoutControlGroup1;
			this.layoutControl1.Size = new System.Drawing.Size(387, 358);
			this.layoutControl1.TabIndex = 0;
			this.layoutControl1.Text = "layoutControl1";
			// 
			// layoutControl2
			// 
			this.layoutControl2.AutoScroll = false;
			this.layoutControl2.Controls.Add(this.extPhoneLabelControl);
			this.layoutControl2.Location = new System.Drawing.Point(212, 128);
			this.layoutControl2.Name = "layoutControl2";
			this.layoutControl2.Root = this.layoutControlGroup2;
			this.layoutControl2.Size = new System.Drawing.Size(173, 22);
			this.layoutControl2.TabIndex = 15;
			this.layoutControl2.Text = "layoutControl2";
			// 
			// extPhoneLabelControl
			// 
			this.extPhoneLabelControl.Location = new System.Drawing.Point(2, 2);
			this.extPhoneLabelControl.Name = "extPhoneLabelControl";
			this.extPhoneLabelControl.Size = new System.Drawing.Size(169, 18);
			this.extPhoneLabelControl.StyleController = this.layoutControl2;
			this.extPhoneLabelControl.TabIndex = 16;
			this.extPhoneLabelControl.Text = "+1 516-935-0877, ext. 151";
			// 
			// layoutControlGroup2
			// 
			this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
			this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
			this.layoutControlGroup2.GroupBordersVisible = false;
			this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
			this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup2.Name = "layoutControlGroup2";
			this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup2.Size = new System.Drawing.Size(173, 22);
			this.layoutControlGroup2.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup2.Text = "layoutControlGroup2";
			this.layoutControlGroup2.TextVisible = false;
			// 
			// layoutControlItem1
			// 
			this.layoutControlItem1.Control = this.extPhoneLabelControl;
			this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
			this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem1.MinSize = new System.Drawing.Size(157, 20);
			this.layoutControlItem1.Name = "layoutControlItem1";
			this.layoutControlItem1.Size = new System.Drawing.Size(173, 22);
			this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem1.Text = "layoutControlItem1";
			this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem1.TextToControlDistance = 0;
			this.layoutControlItem1.TextVisible = false;
			// 
			// copyrightEdit
			// 
			this.copyrightEdit.Location = new System.Drawing.Point(2, 44);
			this.copyrightEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.copyrightEdit.Name = "copyrightEdit";
			this.copyrightEdit.Size = new System.Drawing.Size(383, 16);
			this.copyrightEdit.StyleController = this.layoutControl1;
			this.copyrightEdit.TabIndex = 14;
			// 
			// additionInfoEdit
			// 
			this.additionInfoEdit.Location = new System.Drawing.Point(2, 173);
			this.additionInfoEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.additionInfoEdit.Name = "additionInfoEdit";
			this.additionInfoEdit.Properties.ReadOnly = true;
			this.additionInfoEdit.Size = new System.Drawing.Size(383, 183);
			this.additionInfoEdit.StyleController = this.layoutControl1;
			this.additionInfoEdit.TabIndex = 13;
			// 
			// productLabel
			// 
			this.productLabel.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.productLabel.Appearance.Options.UseFont = true;
			this.productLabel.Location = new System.Drawing.Point(2, 2);
			this.productLabel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.productLabel.Name = "productLabel";
			this.productLabel.Size = new System.Drawing.Size(383, 18);
			this.productLabel.StyleController = this.layoutControl1;
			this.productLabel.TabIndex = 12;
			// 
			// contactPhoneEdit
			// 
			this.contactPhoneEdit.Location = new System.Drawing.Point(212, 108);
			this.contactPhoneEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.contactPhoneEdit.Name = "contactPhoneEdit";
			this.contactPhoneEdit.Size = new System.Drawing.Size(173, 16);
			this.contactPhoneEdit.StyleController = this.layoutControl1;
			this.contactPhoneEdit.TabIndex = 9;
			// 
			// contactEmailEdit
			// 
			this.contactEmailEdit.EditValue = "";
			this.contactEmailEdit.Location = new System.Drawing.Point(212, 84);
			this.contactEmailEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.contactEmailEdit.Name = "contactEmailEdit";
			this.contactEmailEdit.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
			this.contactEmailEdit.Size = new System.Drawing.Size(173, 20);
			this.contactEmailEdit.StyleController = this.layoutControl1;
			this.contactEmailEdit.TabIndex = 8;
			// 
			// contactNameEdit
			// 
			this.contactNameEdit.Location = new System.Drawing.Point(212, 64);
			this.contactNameEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.contactNameEdit.Name = "contactNameEdit";
			this.contactNameEdit.Size = new System.Drawing.Size(173, 16);
			this.contactNameEdit.StyleController = this.layoutControl1;
			this.contactNameEdit.TabIndex = 7;
			// 
			// versionLabel
			// 
			this.versionLabel.Location = new System.Drawing.Point(60, 24);
			this.versionLabel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.versionLabel.Name = "versionLabel";
			this.versionLabel.Size = new System.Drawing.Size(325, 16);
			this.versionLabel.StyleController = this.layoutControl1;
			this.versionLabel.TabIndex = 4;
			// 
			// layoutControlGroup1
			// 
			this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
			this.layoutControlGroup1.GroupBordersVisible = false;
			this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.versionLayoutControlItem,
            this.supportEmailLayoutControlItem,
            this.supportPhoneLayoutControlItem,
            this.supportContactLayoutControlItem,
            this.layoutControlItem9,
            this.infoLayoutControlItem,
            this.layoutControlItem3,
            this.extSupportPhoneLayoutControlItem});
			this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup1.Name = "layoutControlGroup1";
			this.layoutControlGroup1.Size = new System.Drawing.Size(387, 358);
			this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup1.Text = "layoutControlGroup1";
			this.layoutControlGroup1.TextVisible = false;
			// 
			// versionLayoutControlItem
			// 
			this.versionLayoutControlItem.Control = this.versionLabel;
			this.versionLayoutControlItem.CustomizationFormText = "layoutControlItem1";
			this.versionLayoutControlItem.Location = new System.Drawing.Point(0, 22);
			this.versionLayoutControlItem.Name = "versionLayoutControlItem";
			this.versionLayoutControlItem.Size = new System.Drawing.Size(387, 20);
			this.versionLayoutControlItem.Text = "[Version]";
			this.versionLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
			this.versionLayoutControlItem.TextSize = new System.Drawing.Size(53, 16);
			this.versionLayoutControlItem.TextToControlDistance = 5;
			// 
			// supportEmailLayoutControlItem
			// 
			this.supportEmailLayoutControlItem.Control = this.contactEmailEdit;
			this.supportEmailLayoutControlItem.CustomizationFormText = "Tech Support Email:";
			this.supportEmailLayoutControlItem.Location = new System.Drawing.Point(0, 82);
			this.supportEmailLayoutControlItem.Name = "supportEmailLayoutControlItem";
			this.supportEmailLayoutControlItem.Size = new System.Drawing.Size(387, 24);
			this.supportEmailLayoutControlItem.Text = "[Tech Support Email:]";
			this.supportEmailLayoutControlItem.TextSize = new System.Drawing.Size(206, 16);
			// 
			// supportPhoneLayoutControlItem
			// 
			this.supportPhoneLayoutControlItem.Control = this.contactPhoneEdit;
			this.supportPhoneLayoutControlItem.CustomizationFormText = "Tech Support Phone:";
			this.supportPhoneLayoutControlItem.Location = new System.Drawing.Point(0, 106);
			this.supportPhoneLayoutControlItem.Name = "supportPhoneLayoutControlItem";
			this.supportPhoneLayoutControlItem.Size = new System.Drawing.Size(387, 20);
			this.supportPhoneLayoutControlItem.Text = "[Tech Support Phone:]";
			this.supportPhoneLayoutControlItem.TextSize = new System.Drawing.Size(206, 16);
			// 
			// supportContactLayoutControlItem
			// 
			this.supportContactLayoutControlItem.Control = this.contactNameEdit;
			this.supportContactLayoutControlItem.CustomizationFormText = "Tech Support Contact:";
			this.supportContactLayoutControlItem.Location = new System.Drawing.Point(0, 62);
			this.supportContactLayoutControlItem.Name = "supportContactLayoutControlItem";
			this.supportContactLayoutControlItem.Size = new System.Drawing.Size(387, 20);
			this.supportContactLayoutControlItem.Text = "[Tech Support Contact:]";
			this.supportContactLayoutControlItem.TextSize = new System.Drawing.Size(206, 16);
			// 
			// layoutControlItem9
			// 
			this.layoutControlItem9.Control = this.productLabel;
			this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
			this.layoutControlItem9.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem9.Name = "layoutControlItem9";
			this.layoutControlItem9.Size = new System.Drawing.Size(387, 22);
			this.layoutControlItem9.Text = "layoutControlItem9";
			this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem9.TextToControlDistance = 0;
			this.layoutControlItem9.TextVisible = false;
			// 
			// infoLayoutControlItem
			// 
			this.infoLayoutControlItem.Control = this.additionInfoEdit;
			this.infoLayoutControlItem.CustomizationFormText = "Additional Information:";
			this.infoLayoutControlItem.Location = new System.Drawing.Point(0, 152);
			this.infoLayoutControlItem.Name = "infoLayoutControlItem";
			this.infoLayoutControlItem.Size = new System.Drawing.Size(387, 206);
			this.infoLayoutControlItem.Text = "[Additional Information:]";
			this.infoLayoutControlItem.TextLocation = DevExpress.Utils.Locations.Top;
			this.infoLayoutControlItem.TextSize = new System.Drawing.Size(206, 16);
			// 
			// layoutControlItem3
			// 
			this.layoutControlItem3.Control = this.copyrightEdit;
			this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
			this.layoutControlItem3.Location = new System.Drawing.Point(0, 42);
			this.layoutControlItem3.Name = "layoutControlItem3";
			this.layoutControlItem3.Size = new System.Drawing.Size(387, 20);
			this.layoutControlItem3.Text = "layoutControlItem3";
			this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem3.TextToControlDistance = 0;
			this.layoutControlItem3.TextVisible = false;
			// 
			// extSupportPhoneLayoutControlItem
			// 
			this.extSupportPhoneLayoutControlItem.Control = this.layoutControl2;
			this.extSupportPhoneLayoutControlItem.CustomizationFormText = "[External Support Phone:]";
			this.extSupportPhoneLayoutControlItem.Location = new System.Drawing.Point(0, 126);
			this.extSupportPhoneLayoutControlItem.MinSize = new System.Drawing.Size(1, 1);
			this.extSupportPhoneLayoutControlItem.Name = "extSupportPhoneLayoutControlItem";
			this.extSupportPhoneLayoutControlItem.Size = new System.Drawing.Size(387, 26);
			this.extSupportPhoneLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.extSupportPhoneLayoutControlItem.Text = "Tech Support Phone - International:";
			this.extSupportPhoneLayoutControlItem.TextSize = new System.Drawing.Size(206, 16);
			// 
			// AboutDialog
			// 
			this.ApplyEnabled = true;
			this.ApplyVisible = true;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
			this.ClientSize = new System.Drawing.Size(411, 414);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
			this.Name = "AboutDialog";
			this.Text = "About";
			this.Load += new System.EventHandler(this.AboutDialog_Load);
			((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).EndInit();
			this.ContentPanel.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
			this.layoutControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
			this.layoutControl2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.additionInfoEdit.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.contactEmailEdit.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.versionLayoutControlItem)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.supportEmailLayoutControlItem)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.supportPhoneLayoutControlItem)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.supportContactLayoutControlItem)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.infoLayoutControlItem)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.extSupportPhoneLayoutControlItem)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.LabelControl versionLabel;
        private DevExpress.XtraLayout.LayoutControlItem versionLayoutControlItem;
        private DevExpress.XtraEditors.LabelControl contactNameEdit;
        private DevExpress.XtraLayout.LayoutControlItem supportContactLayoutControlItem;
        private DevExpress.XtraEditors.HyperLinkEdit contactEmailEdit;
        private DevExpress.XtraLayout.LayoutControlItem supportEmailLayoutControlItem;
        private DevExpress.XtraEditors.LabelControl contactPhoneEdit;
        private DevExpress.XtraLayout.LayoutControlItem supportPhoneLayoutControlItem;
        private DevExpress.XtraEditors.LabelControl productLabel;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.MemoEdit additionInfoEdit;
        private DevExpress.XtraLayout.LayoutControlItem infoLayoutControlItem;
        private DevExpress.XtraEditors.LabelControl copyrightEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
		private DevExpress.XtraLayout.LayoutControl layoutControl2;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
		private DevExpress.XtraLayout.LayoutControlItem extSupportPhoneLayoutControlItem;
		private DevExpress.XtraEditors.LabelControl extPhoneLabelControl;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;


    }
}
