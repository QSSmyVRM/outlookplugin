﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.ComponentModel;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook.WinForms.Conference
{
    public partial class ConferenceResourcesPage : ConferenceResourcesPageBase
    {

        public ConferenceResourcesPage()
        {
            InitializeComponent();

            conferenceNameEdit.DataBindings.Add("EditValue", ConferenceBindingSource, "Name", true,
                                                DataSourceUpdateMode.OnPropertyChanged);
            resourcesScheduleControl.ConferenceBindingSource = ConferenceBindingSource;
            
            conferenceDateTimeControl.ConferenceBindingSource = ConferenceBindingSource;
            conferenceGeneralControl.ConferenceBindingSource = ConferenceBindingSource;
			nameLayoutControlItem.Text = Strings.ConferenceNameLableText + ":";
			nameLayoutControlItem.CustomizationFormText = Strings.ConferenceNameLableText + ":";
			conferenceNameEdit.Properties.NullText = "<" + Strings.ConferenceNameNullText + ">";
        }


        protected override void OnApplying(CancelEventArgs e)
        {
            base.OnApplying(e);
            if (e.Cancel)
            {
                return;
            }
			if (Conference != null && resourcesScheduleControl.IsConfTypeMatchRoomTypes(Conference.Type) == false || Conference == null)
			{
				e.Cancel = true;
				return;
			}
			//if (Conference != null)
			//{
			//    if (Conference.Type == ConferenceType.PointToPoint && 
			//        Conference.LocationIds.Count + Conference.ExternalRoomsAVSettings.Count != 2)
			//    {
			//        throw new Exception(Strings.Point2PointConferenceMaxRoomCountReached);
			//    }
			//}
        }

        protected override void OnReadOnlyChanged(ReadOnlyChangedEventArgs e)
        {
            conferenceNameEdit.Enabled = !e.ReadOnly;
            resourcesScheduleControl.ReadOnly = e.ReadOnly;
            conferenceDateTimeControl.ReadOnly = e.ReadOnly;
            conferenceGeneralControl.ReadOnly = e.ReadOnly;
        }
    }
}