﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace MyVrm.WebServices.Data
{
    /// <summary>
    /// Represents conference room approver.
    /// </summary>
    [Serializable]
    public class Approver : ICloneable
    {
		public object Clone()
		{
			Approver copy = new Approver();

			copy.Name = string.Copy(Name);
			copy.UserId = new UserId(UserId.Id);
			return copy;
		}

    	internal Approver()
        {
        }

        public Approver(UserId userId)
        {
            UserId = userId;
        }
        /// <summary>
        /// Gets approver's ID.
        /// </summary>
        public UserId UserId { get; internal set; }
        /// <summary>
        /// Gets approver's name.
        /// </summary>
        public string Name { get; internal set; }

        public override string ToString()
        {
            return Name ?? "";
        }
    }
}
