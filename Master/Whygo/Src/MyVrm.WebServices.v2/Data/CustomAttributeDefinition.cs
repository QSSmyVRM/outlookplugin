﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.WebServices.Data
{
    [ServiceObjectDefinition("CustomAttribute")]
    public class CustomAttributeDefinition : ServiceObject
    {
        public CustomAttributeDefinition(MyVrmService service) : base(service)
        {
        }

        public CustomAttributeId Id
        {
            get
            {
                return (CustomAttributeId) PropertyBag[CustomAttributeDefinitionSchema.Id];
            }
            set
            {
                PropertyBag[CustomAttributeDefinitionSchema.Id] = value;
            }
        }

        public string Title
        {
            get
            {
                return (string)PropertyBag[CustomAttributeDefinitionSchema.Title];
            }
            set
            {
                PropertyBag[CustomAttributeDefinitionSchema.Title] = value;
            }
        }

        public bool Mandatory
        {
            get
            {
				bool bRet = false;
				if (PropertyBag[CustomAttributeDefinitionSchema.Mandatory] != null)
					bool.TryParse(PropertyBag[CustomAttributeDefinitionSchema.Mandatory].ToString(), out bRet);
				return bRet;//(bool)PropertyBag[CustomAttributeDefinitionSchema.Mandatory];
            }
            set
            {
                PropertyBag[CustomAttributeDefinitionSchema.Mandatory] = value;
            }
        }

        public string Description
        {
            get
            {
                return (string)PropertyBag[CustomAttributeDefinitionSchema.Description];
            }
            set
            {
                PropertyBag[CustomAttributeDefinitionSchema.Description] = value;
            }
        }

        public CustomAttributeType Type
        {
            get
            {
                return (CustomAttributeType)PropertyBag[CustomAttributeDefinitionSchema.Type];
            }
            set
            {
                PropertyBag[CustomAttributeDefinitionSchema.Type] = value;
            }
        }

        public int Status
        {
            get
            {
                int iRet = 0;
				if (PropertyBag[CustomAttributeDefinitionSchema.Status] != null)
					int.TryParse(PropertyBag[CustomAttributeDefinitionSchema.Status].ToString(), out iRet);
				return iRet; //(int)PropertyBag[CustomAttributeDefinitionSchema.Status];
            }
            set
            {
                PropertyBag[CustomAttributeDefinitionSchema.Status] = value;
            }
        }

        public bool IncludeInEmail
        {
            get
            {
				bool bRet = false;
				if (PropertyBag[CustomAttributeDefinitionSchema.IncludeInEmail] != null)
					bool.TryParse(PropertyBag[CustomAttributeDefinitionSchema.IncludeInEmail].ToString(), out bRet);
				return bRet;//(bool)PropertyBag[CustomAttributeDefinitionSchema.IncludeInEmail];
            }
            set
            {
                PropertyBag[CustomAttributeDefinitionSchema.IncludeInEmail] = value;
            }
        }

        public CustomAttributeCreateType CreateType
        {
            get
            {
                return (CustomAttributeCreateType)PropertyBag[CustomAttributeDefinitionSchema.CreateType];
            }
            set
            {
                PropertyBag[CustomAttributeDefinitionSchema.CreateType] = value;
            }
        }

        #region Overrides of ServiceObject

        internal override ServiceObjectSchema GetSchema()
        {
            return CustomAttributeDefinitionSchema.Instance;
        }

        #endregion
    }
}
