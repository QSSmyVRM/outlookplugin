﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System.Collections.Generic;

namespace MyVrm.Common.Caching
{
    public static class CacheFactory
    {
        private static ICacheManager DefaultCacheManager;

        private static readonly object LockObject = new object();

        public static ICacheManager GetCacheManager(string cachePath)
        {
            lock (LockObject)
            {
                if (DefaultCacheManager == null)
                {
                    DefaultCacheManager = new CacheManager(cachePath);
                }
                return DefaultCacheManager;
            }
        }

        public static void ResetCache()
        {
            lock (LockObject)
            {
                DefaultCacheManager = null;
            }
        }
    }
}
