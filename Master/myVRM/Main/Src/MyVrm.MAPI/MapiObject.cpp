#include "StdAfx.h"
#include "MapiObject.h"
#include "MapiException.h"

using namespace System::Runtime::InteropServices;
using namespace System::ComponentModel;
using namespace System::Collections::Generic;
using namespace MyVrm::MAPI;

MapiObject::MapiObject(void)
{
}

MapiObject::MapiObject(System::Object^ mapiObject)
{
	if (mapiObject == nullptr)
	{
		throw gcnew ArgumentNullException("mapiObject");
	}
	System::IntPtr mapiItf = Marshal::GetIUnknownForObject(mapiObject);
	try
	{
		if (mapiItf == IntPtr::Zero)
		{
			throw gcnew ArgumentException("Argument does not implement IUnknown", "mapiObject");
		}
		IMAPIProp* prop;
		HRESULT hr = ((LPUNKNOWN)mapiItf.ToPointer())->QueryInterface(IID_IMAPIProp, (void**)&prop);
		if (FAILED(hr))
		{
			throw gcnew Win32Exception(hr);
		}
		mapiProp = prop;
	}
	finally
	{
		Marshal::Release(mapiItf);
	}
}

MapiObject::~MapiObject()
{
	this->!MapiObject();
}

MapiObject::!MapiObject()
{
	if (mapiProp)
	{
		mapiProp->Release();
		mapiProp = NULL;
	}
}

System::Object^ MapiObject::GetProperty(MapiTags tag)
{
	array<int>^ tags = gcnew array<int>(1) {(int)tag};
	List<MapiValue>^ tagValues = GetProps(tags);
	if (tagValues->Count > 0 && !tagValues[0].IsError)
	{
		return tagValues[0].Value;
	}
	return nullptr;
}

System::Object^ MapiObject::GetProperty(NamedProperty^ namedProperty)
{
	int propId = GetIDFromName(namedProperty);
	return GetProperty((MapiTags)propId);
}

List<MapiValue>^ MapiObject::GetProps(array<int> ^tags)
{
	ULONG *pPropArray = new ULONG[tags->Length+1];
	List<MapiValue>^ values = gcnew List<MapiValue>();
	try
	{
		pPropArray[0] = tags->Length;
		IntPtr ptrPropArray(&pPropArray[1]);
		Marshal::Copy(tags, 0, ptrPropArray, tags->Length);
		
		ULONG ulOutValues;
		LPSPropValue lppPropArray;
		HRESULT hr = mapiProp->GetProps((LPSPropTagArray)pPropArray, 0, &ulOutValues, &lppPropArray);
		MapiException::ThrowIfError(hr, "MapiObject::GetProps", mapiProp);
		
		try
		{
			for (ULONG i = 0; i < ulOutValues; i++)
			{
				values->Add(MapiValue::Create(IntPtr(&lppPropArray[i])));
			}
		}
		finally
		{
			MAPIFreeBuffer(lppPropArray);
		}
		return values;
	}
	finally
	{
		delete[] pPropArray;
	};	
}

array<int>^ MapiObject::GetIDsFromNames(array<NamedProperty^>^ namedProperties)
{
	if (namedProperties == nullptr)
	{
		throw gcnew System::ArgumentNullException("namedProperties");
	}
	if (namedProperties->Length == 0)
	{
		throw gcnew System::ArgumentOutOfRangeException("namedProperties");
	}
	int size  = namedProperties->Length* sizeof(LPMAPINAMEID);
	for each(NamedProperty^ namedProperty in namedProperties)
	{
		size += namedProperty->GetBytesToMarshal();
	}
	BYTE* pData = NULL;
	LPSPropTagArray pPropTags = NULL;
	try
	{
		BYTE* pData = new BYTE[size];
		BYTE* pElem = pData + (sizeof(LPMAPINAMEID) * namedProperties->Length);
		BYTE* pExtra = pElem + (sizeof(MAPINAMEID) * namedProperties->Length);
		LPMAPINAMEID *lppPropNames = (LPMAPINAMEID*)pData;
		for (int i = 0; i < namedProperties->Length; i++)
		{
			LPMAPINAMEID pPropName = (LPMAPINAMEID)(pElem + (i*sizeof(MAPINAMEID)));
			namedProperties[i]->MarshalToNative(pPropName, &pExtra);
			lppPropNames[i]  = pPropName;
		}
		HRESULT hr = mapiProp->GetIDsFromNames(namedProperties->Length, lppPropNames, 0, &pPropTags);
		MapiException::ThrowIfError(hr, "MapiObject.GetIDsFromNames()", mapiProp);
		array<int>^ ids = gcnew array<int>(pPropTags->cValues);
		for (ULONG i = 0; i < pPropTags->cValues; i++)
		{
			ids[i] = CHANGE_PROP_TYPE(pPropTags->aulPropTag[i], ManagedTypeToMapiPropType(namedProperties[i]->Type));
		}
		return ids;
	}
	finally
	{
		if (pPropTags != NULL)
		{
			::MAPIFreeBuffer(pPropTags);
		}
		delete pData;
	}
}

int MapiObject::GetIDFromName(NamedProperty^ namedProperty)
{
	return GetIDsFromNames(gcnew array<NamedProperty^>(1){namedProperty})[0];
}

void MapiObject::SetProps(array<MapiValue> ^values)
{
	// Calc memory buffer size for SPropValue structures + databuffer
	int bufsize = values->Length*sizeof(SPropValue);
	for each (MapiValue^ value in values)
		bufsize += value->WriteNativeData(NULL, NULL);

	BYTE *pBuf = new BYTE[bufsize];
	LPSPropValue pPropValue = (LPSPropValue)pBuf;
	BYTE *pData = pBuf + values->Length*sizeof(SPropValue);
	try
	{
		for each (MapiValue^ value in values)
		{
			pData += value->WriteNativeData(pPropValue, pData);
			pPropValue++;
		}			

		HRESULT hr = mapiProp->SetProps(values->Length, (LPSPropValue)pBuf, NULL);
		MapiException::ThrowIfError(hr, "IMAPIProp.SetProps()", mapiProp);
	}
	finally
	{
		delete[] pBuf;
	};	
}

void MapiObject::DeleteProps(array<int> ^tags)
{
	ULONG *pPropArray = new ULONG[tags->Length+1];
	try
	{
		pPropArray[0] = tags->Length;
		IntPtr ptrPropArray(&pPropArray[1]);
		Marshal::Copy(tags, 0, ptrPropArray, tags->Length);
		HRESULT hr = mapiProp->DeleteProps((LPSPropTagArray)pPropArray, NULL);
		MapiException::ThrowIfError(hr, "IMAPIProp.DeleteProps()", mapiProp);
	}
	finally
	{
		delete[] pPropArray;
	};
}

void MapiObject::SaveChanges()
{
	HRESULT hr = mapiProp->SaveChanges(KEEP_OPEN_READWRITE);
	MapiException::ThrowIfError(hr, "IMAPIProp.SaveChanges()", mapiProp);
}

unsigned int MapiObject::ManagedTypeToMapiPropType(System::Type^ type)
{
	if (type == nullptr)
	{
		throw gcnew ArgumentNullException("type");
	}
	if (type == System::Int16::typeid)
	{
		return PT_I2;
	}
	if (System::Int32::typeid == type)
	{
		return PT_I4;
	}
	if (System::Single::typeid == type)
	{
		return PT_R4;
	}
	if (System::Double::typeid == type)
	{
		return PT_DOUBLE;
	}
	if (System::Boolean::typeid == type)
	{
		return PT_BOOLEAN;
	}
	if (System::Int64::typeid == type)
	{
		return PT_I8;
	}
	if (System::String::typeid == type)
	{
		return PT_UNICODE;
	}
	if (System::DateTime::typeid == type)
	{
		return PT_SYSTIME;
	}
	if (System::Guid::typeid == type)
	{
		return PT_CLSID;
	}
	if (array<Byte>::typeid == type )
	{
		return PT_BINARY;
	}
	throw gcnew NotSupportedException("type");
}