﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.Outlook.WinForms.Conference
{
    partial class RoomInformationDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			this.dataLayoutControl = new DevExpress.XtraDataLayout.DataLayoutControl();
			this.miscImagesControl = new MyVrm.Outlook.WinForms.ImageListControl();
			this.mapImagesControl = new MyVrm.Outlook.WinForms.ImageListControl();
			this.roomImagesControl = new MyVrm.Outlook.WinForms.ImageListControl();
			this.roomPictureEdit = new DevExpress.XtraEditors.PictureEdit();
			this.yahooMapsHyperLink = new DevExpress.XtraEditors.HyperLinkEdit();
			this.bingMapsHyperLink = new DevExpress.XtraEditors.HyperLinkEdit();
			this.googleMapsHyperLink = new DevExpress.XtraEditors.HyperLinkEdit();
			this.roomNumberLabelControl = new DevExpress.XtraEditors.LabelControl();
			this.capacityLabelControl = new DevExpress.XtraEditors.LabelControl();
			this.projectorLabelControl = new DevExpress.XtraEditors.LabelControl();
			this.cateringLabelControl = new DevExpress.XtraEditors.LabelControl();
			this.endpointLabelControl = new DevExpress.XtraEditors.LabelControl();
			this.approversLabelControl = new DevExpress.XtraEditors.LabelControl();
			this.assistantLabelControl = new DevExpress.XtraEditors.LabelControl();
			this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
			this.addressLabelControl = new DevExpress.XtraEditors.LabelControl();
			this.phoneLabelControl = new DevExpress.XtraEditors.LabelControl();
			this.timeZoneLabelControl = new DevExpress.XtraEditors.LabelControl();
			this.floorLabelControl = new DevExpress.XtraEditors.LabelControl();
			this.departmentsLabelControl = new DevExpress.XtraEditors.LabelControl();
			this.tiersLabelControl = new DevExpress.XtraEditors.LabelControl();
			this.roomNameLabelControl = new DevExpress.XtraEditors.LabelControl();
			this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.roomNameLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
			this.simpleSeparator1 = new DevExpress.XtraLayout.SimpleSeparator();
			this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
			this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
			this.roomBindingSource = new System.Windows.Forms.BindingSource(this.components);
			((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).BeginInit();
			this.ContentPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl)).BeginInit();
			this.dataLayoutControl.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.roomPictureEdit.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.yahooMapsHyperLink.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.bingMapsHyperLink.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.googleMapsHyperLink.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.roomNameLayoutControlItem)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.roomBindingSource)).BeginInit();
			this.SuspendLayout();
			// 
			// ContentPanel
			// 
			this.ContentPanel.Controls.Add(this.dataLayoutControl);
			this.ContentPanel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.ContentPanel.Size = new System.Drawing.Size(600, 394);
			// 
			// dataLayoutControl
			// 
			this.dataLayoutControl.AllowCustomizationMenu = false;
			this.dataLayoutControl.Controls.Add(this.miscImagesControl);
			this.dataLayoutControl.Controls.Add(this.mapImagesControl);
			this.dataLayoutControl.Controls.Add(this.roomImagesControl);
			this.dataLayoutControl.Controls.Add(this.roomPictureEdit);
			this.dataLayoutControl.Controls.Add(this.yahooMapsHyperLink);
			this.dataLayoutControl.Controls.Add(this.bingMapsHyperLink);
			this.dataLayoutControl.Controls.Add(this.googleMapsHyperLink);
			this.dataLayoutControl.Controls.Add(this.roomNumberLabelControl);
			this.dataLayoutControl.Controls.Add(this.capacityLabelControl);
			this.dataLayoutControl.Controls.Add(this.projectorLabelControl);
			this.dataLayoutControl.Controls.Add(this.cateringLabelControl);
			this.dataLayoutControl.Controls.Add(this.endpointLabelControl);
			this.dataLayoutControl.Controls.Add(this.approversLabelControl);
			this.dataLayoutControl.Controls.Add(this.assistantLabelControl);
			this.dataLayoutControl.Controls.Add(this.labelControl8);
			this.dataLayoutControl.Controls.Add(this.addressLabelControl);
			this.dataLayoutControl.Controls.Add(this.phoneLabelControl);
			this.dataLayoutControl.Controls.Add(this.timeZoneLabelControl);
			this.dataLayoutControl.Controls.Add(this.floorLabelControl);
			this.dataLayoutControl.Controls.Add(this.departmentsLabelControl);
			this.dataLayoutControl.Controls.Add(this.tiersLabelControl);
			this.dataLayoutControl.Controls.Add(this.roomNameLabelControl);
			this.dataLayoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataLayoutControl.Location = new System.Drawing.Point(0, 0);
			this.dataLayoutControl.Name = "dataLayoutControl";
			this.dataLayoutControl.Root = this.layoutControlGroup1;
			this.dataLayoutControl.Size = new System.Drawing.Size(600, 394);
			this.dataLayoutControl.TabIndex = 0;
			this.dataLayoutControl.Text = "dataLayoutControl1";
			// 
			// miscImagesControl
			// 
			this.miscImagesControl.Location = new System.Drawing.Point(280, 323);
			this.miscImagesControl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.miscImagesControl.Name = "miscImagesControl";
			this.miscImagesControl.Size = new System.Drawing.Size(318, 69);
			this.miscImagesControl.TabIndex = 31;
			// 
			// mapImagesControl
			// 
			this.mapImagesControl.Location = new System.Drawing.Point(282, 240);
			this.mapImagesControl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.mapImagesControl.Name = "mapImagesControl";
			this.mapImagesControl.Size = new System.Drawing.Size(316, 63);
			this.mapImagesControl.TabIndex = 29;
			// 
			// roomImagesControl
			// 
			this.roomImagesControl.Location = new System.Drawing.Point(282, 184);
			this.roomImagesControl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.roomImagesControl.Name = "roomImagesControl";
			this.roomImagesControl.Size = new System.Drawing.Size(316, 36);
			this.roomImagesControl.TabIndex = 28;
			// 
			// roomPictureEdit
			// 
			this.roomPictureEdit.Location = new System.Drawing.Point(2, 2);
			this.roomPictureEdit.Name = "roomPictureEdit";
			this.roomPictureEdit.Properties.ReadOnly = true;
			this.roomPictureEdit.Properties.ShowMenu = false;
			this.roomPictureEdit.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
			this.roomPictureEdit.Size = new System.Drawing.Size(87, 87);
			this.roomPictureEdit.StyleController = this.dataLayoutControl;
			this.roomPictureEdit.TabIndex = 26;
            this.roomPictureEdit.Properties.NullText = "No image data";
            //MyPictureEdit.Properties.NullText = "No current image to display";
			// 
			// yahooMapsHyperLink
			// 
			this.yahooMapsHyperLink.EditValue = "";
			this.yahooMapsHyperLink.Location = new System.Drawing.Point(73, 225);
			this.yahooMapsHyperLink.Name = "yahooMapsHyperLink";
			this.yahooMapsHyperLink.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
			this.yahooMapsHyperLink.Properties.Appearance.Options.UseBackColor = true;
			this.yahooMapsHyperLink.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
			this.yahooMapsHyperLink.Properties.Caption = "Yahoo Map";
			this.yahooMapsHyperLink.Size = new System.Drawing.Size(67, 18);
			this.yahooMapsHyperLink.StyleController = this.dataLayoutControl;
			this.yahooMapsHyperLink.TabIndex = 25;
			// 
			// bingMapsHyperLink
			// 
			this.bingMapsHyperLink.EditValue = "";
			this.bingMapsHyperLink.Location = new System.Drawing.Point(144, 225);
			this.bingMapsHyperLink.Name = "bingMapsHyperLink";
			this.bingMapsHyperLink.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
			this.bingMapsHyperLink.Properties.Appearance.Options.UseBackColor = true;
			this.bingMapsHyperLink.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
			this.bingMapsHyperLink.Properties.Caption = "Bing Map";
			this.bingMapsHyperLink.Size = new System.Drawing.Size(132, 18);
			this.bingMapsHyperLink.StyleController = this.dataLayoutControl;
			this.bingMapsHyperLink.TabIndex = 24;
			// 
			// googleMapsHyperLink
			// 
			this.googleMapsHyperLink.EditValue = "";
			this.googleMapsHyperLink.Location = new System.Drawing.Point(2, 225);
			this.googleMapsHyperLink.Name = "googleMapsHyperLink";
			this.googleMapsHyperLink.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
			this.googleMapsHyperLink.Properties.Appearance.Options.UseBackColor = true;
			this.googleMapsHyperLink.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
			this.googleMapsHyperLink.Properties.Caption = "Google Map";
			this.googleMapsHyperLink.Size = new System.Drawing.Size(67, 18);
			this.googleMapsHyperLink.StyleController = this.dataLayoutControl;
			this.googleMapsHyperLink.TabIndex = 23;
			// 
			// roomNumberLabelControl
			// 
			this.roomNumberLabelControl.Location = new System.Drawing.Point(119, 127);
			this.roomNumberLabelControl.Name = "roomNumberLabelControl";
			this.roomNumberLabelControl.Size = new System.Drawing.Size(157, 13);
			this.roomNumberLabelControl.StyleController = this.dataLayoutControl;
			this.roomNumberLabelControl.TabIndex = 22;
			// 
			// capacityLabelControl
			// 
			this.capacityLabelControl.Location = new System.Drawing.Point(399, 151);
			this.capacityLabelControl.Name = "capacityLabelControl";
			this.capacityLabelControl.Size = new System.Drawing.Size(199, 13);
			this.capacityLabelControl.StyleController = this.dataLayoutControl;
			this.capacityLabelControl.TabIndex = 20;
			// 
			// projectorLabelControl
			// 
			this.projectorLabelControl.Location = new System.Drawing.Point(399, 134);
			this.projectorLabelControl.Name = "projectorLabelControl";
			this.projectorLabelControl.Size = new System.Drawing.Size(199, 13);
			this.projectorLabelControl.StyleController = this.dataLayoutControl;
			this.projectorLabelControl.TabIndex = 17;
			// 
			// cateringLabelControl
			// 
			this.cateringLabelControl.Location = new System.Drawing.Point(399, 117);
			this.cateringLabelControl.Name = "cateringLabelControl";
			this.cateringLabelControl.Size = new System.Drawing.Size(199, 13);
			this.cateringLabelControl.StyleController = this.dataLayoutControl;
			this.cateringLabelControl.TabIndex = 16;
			// 
			// endpointLabelControl
			// 
			this.endpointLabelControl.Location = new System.Drawing.Point(119, 305);
			this.endpointLabelControl.Name = "endpointLabelControl";
			this.endpointLabelControl.Size = new System.Drawing.Size(157, 13);
			this.endpointLabelControl.StyleController = this.dataLayoutControl;
			this.endpointLabelControl.TabIndex = 15;
			// 
			// approversLabelControl
			// 
			this.approversLabelControl.Location = new System.Drawing.Point(119, 288);
			this.approversLabelControl.Name = "approversLabelControl";
			this.approversLabelControl.Size = new System.Drawing.Size(157, 13);
			this.approversLabelControl.StyleController = this.dataLayoutControl;
			this.approversLabelControl.TabIndex = 14;
			// 
			// assistantLabelControl
			// 
			this.assistantLabelControl.Location = new System.Drawing.Point(119, 271);
			this.assistantLabelControl.Name = "assistantLabelControl";
			this.assistantLabelControl.Size = new System.Drawing.Size(157, 13);
			this.assistantLabelControl.StyleController = this.dataLayoutControl;
			this.assistantLabelControl.TabIndex = 13;
			// 
			// labelControl8
			// 
			this.labelControl8.Location = new System.Drawing.Point(119, 254);
			this.labelControl8.Name = "labelControl8";
			this.labelControl8.Size = new System.Drawing.Size(157, 13);
			this.labelControl8.StyleController = this.dataLayoutControl;
			this.labelControl8.TabIndex = 12;
			// 
			// addressLabelControl
			// 
			this.addressLabelControl.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
			this.addressLabelControl.Location = new System.Drawing.Point(119, 178);
			this.addressLabelControl.Name = "addressLabelControl";
			this.addressLabelControl.Size = new System.Drawing.Size(157, 43);
			this.addressLabelControl.StyleController = this.dataLayoutControl;
			this.addressLabelControl.TabIndex = 11;
			// 
			// phoneLabelControl
			// 
			this.phoneLabelControl.Location = new System.Drawing.Point(119, 161);
			this.phoneLabelControl.Name = "phoneLabelControl";
			this.phoneLabelControl.Size = new System.Drawing.Size(157, 13);
			this.phoneLabelControl.StyleController = this.dataLayoutControl;
			this.phoneLabelControl.TabIndex = 10;
			// 
			// timeZoneLabelControl
			// 
			this.timeZoneLabelControl.Location = new System.Drawing.Point(119, 144);
			this.timeZoneLabelControl.Name = "timeZoneLabelControl";
			this.timeZoneLabelControl.Size = new System.Drawing.Size(157, 13);
			this.timeZoneLabelControl.StyleController = this.dataLayoutControl;
			this.timeZoneLabelControl.TabIndex = 9;
			// 
			// floorLabelControl
			// 
			this.floorLabelControl.Location = new System.Drawing.Point(119, 110);
			this.floorLabelControl.Name = "floorLabelControl";
			this.floorLabelControl.Size = new System.Drawing.Size(157, 13);
			this.floorLabelControl.StyleController = this.dataLayoutControl;
			this.floorLabelControl.TabIndex = 7;
			// 
			// departmentsLabelControl
			// 
			this.departmentsLabelControl.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
			this.departmentsLabelControl.Location = new System.Drawing.Point(399, 93);
			this.departmentsLabelControl.Name = "departmentsLabelControl";
			this.departmentsLabelControl.Size = new System.Drawing.Size(199, 20);
			this.departmentsLabelControl.StyleController = this.dataLayoutControl;
			this.departmentsLabelControl.TabIndex = 6;
			// 
			// tiersLabelControl
			// 
			this.tiersLabelControl.Location = new System.Drawing.Point(119, 93);
			this.tiersLabelControl.Name = "tiersLabelControl";
			this.tiersLabelControl.Size = new System.Drawing.Size(157, 13);
			this.tiersLabelControl.StyleController = this.dataLayoutControl;
			this.tiersLabelControl.TabIndex = 5;
			// 
			// roomNameLabelControl
			// 
			this.roomNameLabelControl.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.roomNameLabelControl.Appearance.Options.UseFont = true;
			this.roomNameLabelControl.Location = new System.Drawing.Point(210, 2);
			this.roomNameLabelControl.Name = "roomNameLabelControl";
			this.roomNameLabelControl.Size = new System.Drawing.Size(388, 16);
			this.roomNameLabelControl.StyleController = this.dataLayoutControl;
			this.roomNameLabelControl.TabIndex = 4;
			// 
			// layoutControlGroup1
			// 
			this.layoutControlGroup1.CustomizationFormText = "Root";
			this.layoutControlGroup1.GroupBordersVisible = false;
			this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.roomNameLayoutControlItem,
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem2,
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.simpleSeparator1,
            this.layoutControlItem4,
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem16,
            this.layoutControlItem23,
            this.layoutControlItem14,
            this.emptySpaceItem2,
            this.layoutControlItem17});
			this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup1.Name = "Root";
			this.layoutControlGroup1.Size = new System.Drawing.Size(600, 394);
			this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup1.Text = "Root";
			this.layoutControlGroup1.TextVisible = false;
			// 
			// roomNameLayoutControlItem
			// 
			this.roomNameLayoutControlItem.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.roomNameLayoutControlItem.AppearanceItemCaption.Options.UseFont = true;
			this.roomNameLayoutControlItem.Control = this.roomNameLabelControl;
			this.roomNameLayoutControlItem.CustomizationFormText = "Room Name:";
			this.roomNameLayoutControlItem.Location = new System.Drawing.Point(91, 0);
			this.roomNameLayoutControlItem.Name = "roomNameLayoutControlItem";
			this.roomNameLayoutControlItem.Size = new System.Drawing.Size(509, 91);
			this.roomNameLayoutControlItem.Text = "Room Name:";
			this.roomNameLayoutControlItem.TextSize = new System.Drawing.Size(113, 16);
			// 
			// layoutControlItem1
			// 
			this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
			this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
			this.layoutControlItem1.Control = this.tiersLabelControl;
			this.layoutControlItem1.CustomizationFormText = "Tier Information";
			this.layoutControlItem1.Location = new System.Drawing.Point(0, 91);
			this.layoutControlItem1.Name = "layoutControlItem1";
			this.layoutControlItem1.Size = new System.Drawing.Size(278, 17);
			this.layoutControlItem1.Text = "Tier Information";
			this.layoutControlItem1.TextSize = new System.Drawing.Size(113, 13);
			// 
			// layoutControlItem3
			// 
			this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
			this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
			this.layoutControlItem3.Control = this.floorLabelControl;
			this.layoutControlItem3.CustomizationFormText = "Floor";
			this.layoutControlItem3.Location = new System.Drawing.Point(0, 108);
			this.layoutControlItem3.Name = "layoutControlItem3";
			this.layoutControlItem3.Size = new System.Drawing.Size(278, 17);
			this.layoutControlItem3.Text = "Floor";
			this.layoutControlItem3.TextSize = new System.Drawing.Size(113, 13);
			// 
			// layoutControlItem5
			// 
			this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
			this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
			this.layoutControlItem5.Control = this.timeZoneLabelControl;
			this.layoutControlItem5.CustomizationFormText = "Time Zone";
			this.layoutControlItem5.Location = new System.Drawing.Point(0, 142);
			this.layoutControlItem5.Name = "layoutControlItem5";
			this.layoutControlItem5.Size = new System.Drawing.Size(278, 17);
			this.layoutControlItem5.Text = "Time Zone";
			this.layoutControlItem5.TextSize = new System.Drawing.Size(113, 13);
			// 
			// layoutControlItem6
			// 
			this.layoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
			this.layoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
			this.layoutControlItem6.Control = this.phoneLabelControl;
			this.layoutControlItem6.CustomizationFormText = "Phone Number";
			this.layoutControlItem6.Location = new System.Drawing.Point(0, 159);
			this.layoutControlItem6.Name = "layoutControlItem6";
			this.layoutControlItem6.Size = new System.Drawing.Size(278, 17);
			this.layoutControlItem6.Text = "Phone Number";
			this.layoutControlItem6.TextSize = new System.Drawing.Size(113, 13);
			// 
			// layoutControlItem7
			// 
			this.layoutControlItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
			this.layoutControlItem7.AppearanceItemCaption.Options.UseFont = true;
			this.layoutControlItem7.Control = this.addressLabelControl;
			this.layoutControlItem7.CustomizationFormText = "Address";
			this.layoutControlItem7.Location = new System.Drawing.Point(0, 176);
			this.layoutControlItem7.Name = "layoutControlItem7";
			this.layoutControlItem7.Size = new System.Drawing.Size(278, 47);
			this.layoutControlItem7.Text = "Address";
			this.layoutControlItem7.TextSize = new System.Drawing.Size(113, 13);
			// 
			// layoutControlItem8
			// 
			this.layoutControlItem8.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
			this.layoutControlItem8.AppearanceItemCaption.Options.UseFont = true;
			this.layoutControlItem8.Control = this.labelControl8;
			this.layoutControlItem8.CustomizationFormText = "Parking Directions";
			this.layoutControlItem8.Location = new System.Drawing.Point(0, 252);
			this.layoutControlItem8.Name = "layoutControlItem8";
			this.layoutControlItem8.Size = new System.Drawing.Size(278, 17);
			this.layoutControlItem8.Text = "Parking Directions";
			this.layoutControlItem8.TextSize = new System.Drawing.Size(113, 13);
			// 
			// layoutControlItem9
			// 
			this.layoutControlItem9.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
			this.layoutControlItem9.AppearanceItemCaption.Options.UseFont = true;
			this.layoutControlItem9.Control = this.assistantLabelControl;
			this.layoutControlItem9.CustomizationFormText = "Assistant-in-Charge";
			this.layoutControlItem9.Location = new System.Drawing.Point(0, 269);
			this.layoutControlItem9.Name = "layoutControlItem9";
			this.layoutControlItem9.Size = new System.Drawing.Size(278, 17);
			this.layoutControlItem9.Text = "Assistant-in-Charge";
			this.layoutControlItem9.TextSize = new System.Drawing.Size(113, 13);
			// 
			// layoutControlItem10
			// 
			this.layoutControlItem10.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
			this.layoutControlItem10.AppearanceItemCaption.Options.UseFont = true;
			this.layoutControlItem10.Control = this.approversLabelControl;
			this.layoutControlItem10.CustomizationFormText = "Approvers";
			this.layoutControlItem10.Location = new System.Drawing.Point(0, 286);
			this.layoutControlItem10.Name = "layoutControlItem10";
			this.layoutControlItem10.Size = new System.Drawing.Size(278, 17);
			this.layoutControlItem10.Text = "Approvers";
			this.layoutControlItem10.TextSize = new System.Drawing.Size(113, 13);
			// 
			// layoutControlItem11
			// 
			this.layoutControlItem11.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
			this.layoutControlItem11.AppearanceItemCaption.Options.UseFont = true;
			this.layoutControlItem11.Control = this.endpointLabelControl;
			this.layoutControlItem11.CustomizationFormText = "Endpoint";
			this.layoutControlItem11.Location = new System.Drawing.Point(0, 303);
			this.layoutControlItem11.Name = "layoutControlItem11";
			this.layoutControlItem11.Size = new System.Drawing.Size(278, 17);
			this.layoutControlItem11.Text = "Endpoint";
			this.layoutControlItem11.TextSize = new System.Drawing.Size(113, 13);
			// 
			// layoutControlItem2
			// 
			this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
			this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
			this.layoutControlItem2.Control = this.departmentsLabelControl;
			this.layoutControlItem2.CustomizationFormText = "Departments";
			this.layoutControlItem2.Location = new System.Drawing.Point(280, 91);
			this.layoutControlItem2.Name = "layoutControlItem2";
			this.layoutControlItem2.Size = new System.Drawing.Size(320, 24);
			this.layoutControlItem2.Text = "Departments";
			this.layoutControlItem2.TextSize = new System.Drawing.Size(113, 13);
			// 
			// layoutControlItem12
			// 
			this.layoutControlItem12.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
			this.layoutControlItem12.AppearanceItemCaption.Options.UseFont = true;
			this.layoutControlItem12.Control = this.cateringLabelControl;
			this.layoutControlItem12.CustomizationFormText = "Catering Facility";
			this.layoutControlItem12.Location = new System.Drawing.Point(280, 115);
			this.layoutControlItem12.Name = "layoutControlItem12";
			this.layoutControlItem12.Size = new System.Drawing.Size(320, 17);
			this.layoutControlItem12.Text = "Catering Facility";
			this.layoutControlItem12.TextSize = new System.Drawing.Size(113, 13);
			// 
			// layoutControlItem13
			// 
			this.layoutControlItem13.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
			this.layoutControlItem13.AppearanceItemCaption.Options.UseFont = true;
			this.layoutControlItem13.Control = this.projectorLabelControl;
			this.layoutControlItem13.CustomizationFormText = "Projector Available";
			this.layoutControlItem13.Location = new System.Drawing.Point(280, 132);
			this.layoutControlItem13.Name = "layoutControlItem13";
			this.layoutControlItem13.Size = new System.Drawing.Size(320, 17);
			this.layoutControlItem13.Text = "Projector Available";
			this.layoutControlItem13.TextSize = new System.Drawing.Size(113, 13);
			// 
			// simpleSeparator1
			// 
			this.simpleSeparator1.CustomizationFormText = "simpleSeparator1";
			this.simpleSeparator1.Location = new System.Drawing.Point(278, 91);
			this.simpleSeparator1.Name = "simpleSeparator1";
			this.simpleSeparator1.Size = new System.Drawing.Size(2, 214);
			this.simpleSeparator1.Text = "simpleSeparator1";
			this.simpleSeparator1.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
			// 
			// layoutControlItem4
			// 
			this.layoutControlItem4.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
			this.layoutControlItem4.AppearanceItemCaption.Options.UseFont = true;
			this.layoutControlItem4.Control = this.roomNumberLabelControl;
			this.layoutControlItem4.CustomizationFormText = "Room Number";
			this.layoutControlItem4.Location = new System.Drawing.Point(0, 125);
			this.layoutControlItem4.Name = "layoutControlItem4";
			this.layoutControlItem4.Size = new System.Drawing.Size(278, 17);
			this.layoutControlItem4.Text = "Room Number";
			this.layoutControlItem4.TextSize = new System.Drawing.Size(113, 13);
			// 
			// layoutControlItem18
			// 
			this.layoutControlItem18.Control = this.googleMapsHyperLink;
			this.layoutControlItem18.CustomizationFormText = "layoutControlItem18";
			this.layoutControlItem18.Location = new System.Drawing.Point(0, 223);
			this.layoutControlItem18.MaxSize = new System.Drawing.Size(0, 29);
			this.layoutControlItem18.MinSize = new System.Drawing.Size(71, 29);
			this.layoutControlItem18.Name = "layoutControlItem18";
			this.layoutControlItem18.Size = new System.Drawing.Size(71, 29);
			this.layoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem18.Text = "layoutControlItem18";
			this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem18.TextToControlDistance = 0;
			this.layoutControlItem18.TextVisible = false;
			// 
			// layoutControlItem19
			// 
			this.layoutControlItem19.Control = this.bingMapsHyperLink;
			this.layoutControlItem19.CustomizationFormText = "layoutControlItem19";
			this.layoutControlItem19.Location = new System.Drawing.Point(142, 223);
			this.layoutControlItem19.MaxSize = new System.Drawing.Size(0, 29);
			this.layoutControlItem19.MinSize = new System.Drawing.Size(71, 29);
			this.layoutControlItem19.Name = "layoutControlItem19";
			this.layoutControlItem19.Size = new System.Drawing.Size(136, 29);
			this.layoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem19.Text = "layoutControlItem19";
			this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem19.TextToControlDistance = 0;
			this.layoutControlItem19.TextVisible = false;
			// 
			// layoutControlItem20
			// 
			this.layoutControlItem20.Control = this.yahooMapsHyperLink;
			this.layoutControlItem20.CustomizationFormText = "layoutControlItem20";
			this.layoutControlItem20.Location = new System.Drawing.Point(71, 223);
			this.layoutControlItem20.MaxSize = new System.Drawing.Size(0, 29);
			this.layoutControlItem20.MinSize = new System.Drawing.Size(71, 29);
			this.layoutControlItem20.Name = "layoutControlItem20";
			this.layoutControlItem20.Size = new System.Drawing.Size(71, 29);
			this.layoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem20.Text = "layoutControlItem20";
			this.layoutControlItem20.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem20.TextToControlDistance = 0;
			this.layoutControlItem20.TextVisible = false;
			// 
			// layoutControlItem21
			// 
			this.layoutControlItem21.Control = this.roomPictureEdit;
			this.layoutControlItem21.CustomizationFormText = "layoutControlItem21";
			this.layoutControlItem21.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem21.MaxSize = new System.Drawing.Size(91, 91);
			this.layoutControlItem21.MinSize = new System.Drawing.Size(91, 91);
			this.layoutControlItem21.Name = "layoutControlItem21";
			this.layoutControlItem21.Size = new System.Drawing.Size(91, 91);
			this.layoutControlItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem21.Text = "layoutControlItem21";
			this.layoutControlItem21.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem21.TextToControlDistance = 0;
			this.layoutControlItem21.TextVisible = false;
			// 
			// layoutControlItem16
			// 
			this.layoutControlItem16.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
			this.layoutControlItem16.AppearanceItemCaption.Options.UseFont = true;
			this.layoutControlItem16.Control = this.capacityLabelControl;
			this.layoutControlItem16.CustomizationFormText = "Capacity";
			this.layoutControlItem16.Location = new System.Drawing.Point(280, 149);
			this.layoutControlItem16.Name = "layoutControlItem16";
			this.layoutControlItem16.Size = new System.Drawing.Size(320, 17);
			this.layoutControlItem16.Text = "Capacity";
			this.layoutControlItem16.TextSize = new System.Drawing.Size(113, 13);
			// 
			// layoutControlItem23
			// 
			this.layoutControlItem23.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
			this.layoutControlItem23.AppearanceItemCaption.Options.UseFont = true;
			this.layoutControlItem23.Control = this.roomImagesControl;
			this.layoutControlItem23.CustomizationFormText = "Room Images";
			this.layoutControlItem23.Location = new System.Drawing.Point(280, 166);
			this.layoutControlItem23.MinSize = new System.Drawing.Size(211, 51);
			this.layoutControlItem23.Name = "layoutControlItem23";
			this.layoutControlItem23.Size = new System.Drawing.Size(320, 56);
			this.layoutControlItem23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem23.Text = "Room Images";
			this.layoutControlItem23.TextLocation = DevExpress.Utils.Locations.Top;
			this.layoutControlItem23.TextSize = new System.Drawing.Size(113, 13);
			// 
			// layoutControlItem14
			// 
			this.layoutControlItem14.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
			this.layoutControlItem14.AppearanceItemCaption.Options.UseFont = true;
			this.layoutControlItem14.Control = this.mapImagesControl;
			this.layoutControlItem14.CustomizationFormText = "Map Images";
			this.layoutControlItem14.Location = new System.Drawing.Point(280, 222);
			this.layoutControlItem14.MinSize = new System.Drawing.Size(211, 51);
			this.layoutControlItem14.Name = "layoutControlItem14";
			this.layoutControlItem14.Size = new System.Drawing.Size(320, 83);
			this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem14.Text = "Map Images";
			this.layoutControlItem14.TextLocation = DevExpress.Utils.Locations.Top;
			this.layoutControlItem14.TextSize = new System.Drawing.Size(113, 13);
			// 
			// emptySpaceItem2
			// 
			this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
			this.emptySpaceItem2.Location = new System.Drawing.Point(0, 320);
			this.emptySpaceItem2.Name = "emptySpaceItem2";
			this.emptySpaceItem2.Size = new System.Drawing.Size(278, 74);
			this.emptySpaceItem2.Text = "emptySpaceItem2";
			this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
			// 
			// layoutControlItem17
			// 
			this.layoutControlItem17.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
			this.layoutControlItem17.AppearanceItemCaption.Options.UseFont = true;
			this.layoutControlItem17.Control = this.miscImagesControl;
			this.layoutControlItem17.CustomizationFormText = "Misc Images";
			this.layoutControlItem17.Location = new System.Drawing.Point(278, 305);
			this.layoutControlItem17.MinSize = new System.Drawing.Size(211, 51);
			this.layoutControlItem17.Name = "layoutControlItem17";
			this.layoutControlItem17.Size = new System.Drawing.Size(322, 89);
			this.layoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem17.Text = "Misc Images";
			this.layoutControlItem17.TextLocation = DevExpress.Utils.Locations.Top;
			this.layoutControlItem17.TextSize = new System.Drawing.Size(113, 13);
			// 
			// roomBindingSource
			// 
			this.roomBindingSource.DataSourceChanged += new System.EventHandler(this.roomBindingSource_DataSourceChanged);
			// 
			// RoomInformationDialog
			// 
			this.ApplyEnabled = true;
			this.ApplyVisible = true;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.ClientSize = new System.Drawing.Size(724, 550);
			this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.MinimumSize = new System.Drawing.Size(852, 577);
            this.MaximumSize = new System.Drawing.Size(952, 577);
			this.Name = "RoomInformationDialog";
			this.Text = "Room Information";
			((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).EndInit();
			this.ContentPanel.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl)).EndInit();
			this.dataLayoutControl.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.roomPictureEdit.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.yahooMapsHyperLink.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.bingMapsHyperLink.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.googleMapsHyperLink.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.roomNameLayoutControlItem)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.roomBindingSource)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.LabelControl roomNameLabelControl;
        private DevExpress.XtraLayout.LayoutControlItem roomNameLayoutControlItem;
        private DevExpress.XtraEditors.LabelControl tiersLabelControl;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.LabelControl departmentsLabelControl;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.LabelControl floorLabelControl;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.LabelControl phoneLabelControl;
        private DevExpress.XtraEditors.LabelControl timeZoneLabelControl;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.LabelControl addressLabelControl;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.LabelControl assistantLabelControl;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.LabelControl approversLabelControl;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.LabelControl endpointLabelControl;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.LabelControl projectorLabelControl;
        private DevExpress.XtraEditors.LabelControl cateringLabelControl;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraEditors.LabelControl capacityLabelControl;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator1;
        private System.Windows.Forms.BindingSource roomBindingSource;
        private DevExpress.XtraEditors.LabelControl roomNumberLabelControl;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.HyperLinkEdit yahooMapsHyperLink;
        private DevExpress.XtraEditors.HyperLinkEdit bingMapsHyperLink;
        private DevExpress.XtraEditors.HyperLinkEdit googleMapsHyperLink;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraEditors.PictureEdit roomPictureEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private ImageListControl roomImagesControl;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private ImageListControl mapImagesControl;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private ImageListControl miscImagesControl;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
    }
}