﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using Microsoft.Win32;
using MyVrm.Common.Caching;
using MyVrm.Common.Diagnostics;
using MyVrm.Outlook.Configuration;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook
{
    public class MyVrmAddin
    {
        private static string _companyName = "myVRM";
        private static string _productName = "TrueDaybook Outlook Add-in";
        private static string _copyright = "";
        private static string _forgotPasswordURL = "";
        private static string webSettingsURL = "";
		private static string _buildType = "";
        private static string _productVersion = "1.0";
        private static string _productStorageVersion = "1.0";
		private static string _productDisplayName = "myVRM";
		private static object _bmpLogo16;
		private static object _bmpLogo65;

        private static readonly object SyncObject = new object();

        private Settings _settings;
        private const string CacheFileName = "roomcache.db3";
        private readonly object _cacheLock = new object();
        private ICacheManager _cacheManager;
        private string _currentCachedWebServiceUrl;

        private static MyVrmAddin _instance;
        private readonly List<Room> _roomCache = new List<Room>();
        private readonly List<User> _userCache = new List<User>();
        private List<AudioUser> _audioUsersCache;

        private static MyVrmTraceSource _traceSource;

        private string _defaultEulaText;
        private const string EulaFile = "EULA.rtf";
        private const string UserConfigFileName = "UserConfig.dat";

        private MyVrmAddin()
        {
        }

        public static MyVrmTraceSource TraceSource
        {
            get
            {
                if (_traceSource == null)
                {
                    lock(SyncObject)
                    {
                        Trace.AutoFlush = true;
                        _traceSource = new MyVrmTraceSource("myVRM")
                                           {
                                               Switch = new SourceSwitch("SourceSwitch", "Information")
                                           };
                        var traceFile = Path.Combine(TraceLogFolder, "trace.log");
                        if (File.Exists(traceFile))
                        {
                            try
                            {
                                File.Delete(traceFile);
                            }
                            catch (Exception /*e*/)
                            {
                                //TODO
                            }
                        }
                        var textListener = new DelimitedListTraceListener(traceFile)
                                               {
                                                   TraceOutputOptions =
                                                       TraceOptions.DateTime | TraceOptions.ThreadId |
                                                       TraceOptions.ProcessId,
                                                   Delimiter = "\x9"
                                               };
                        _traceSource.Listeners.Add(textListener);
                    }
                }
                return _traceSource;
            }
        }

        public static string LocalUserAppDataPath
        {
            get
            {
                return GetDataPath(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData));
            }
        }

        public static string LocalUserAppRegPath
        {
            get
            {
                return AppRegPath;
            }
        }

        private static string AppRegPath
        {
            get
            {
                return @"Software\" + CompanyName + @"\" + ProductName;
            }
        }

        private static string TraceLogFolder
        {
            get
            {
                var path = Path.Combine(LocalUserAppDataPath, "Logs");
                lock(SyncObject)
                {
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                }
                return path;
            }
        }

        private static string GetDataPath(string basePath)
        {
            string path = string.Format(@"{0}\{1}\{2}\{3}", new object[] {basePath, CompanyName, ProductName, ProductStorageVersion});
            lock (SyncObject)
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
            }
            return path;
        }

        public static MyVrmAddin Instance
        {
            get
            {
                lock (SyncObject)
                {
                    if (_instance == null)
                    {
                        _instance = new MyVrmAddin();
                    }
                }

                return _instance;
            }
        }
        public Settings Settings
        {
            get
            {
                if (_settings == null)
                {
                    _settings = GetSettings();
                    _settings.SettingsLoaded += SettingsLoaded;
                    _settings.SettingsSaving += SettingsSaving;
                    ApplySettings();
                }
                return _settings;
            }
        }

        public static string CompanyName
        {
            get { return _companyName; }
            set { _companyName = value; }
        }

        public static string ProductName
        {
            get { return _productName; }
            set { _productName = value; }
        }

        public static string ProductStorageVersion
        {
            get { return _productStorageVersion; }
            set { _productStorageVersion = value; }
        }

        public static string ProductVersion
        {
            get { return _productVersion; }
            set { _productVersion = value; }
        }

        public static string Copyright
        {
            get { return _copyright; }
            set { _copyright = value; }
        }
		public static string BuildType
        {
			get { return _buildType; }
			set { _buildType = value; }
        }

        public static string ForgotPasswordURL
        {
            get { return _forgotPasswordURL; }
            set { _forgotPasswordURL = value; }
        }
        public static string WebSettingsURL
        {
            get { return webSettingsURL; }
            set { webSettingsURL = value; }
        }
		public static string ProductDisplayName
        {
			get { return _productDisplayName; }
			set { _productDisplayName = value; }
        }
		public static object Logo16
        {
			get { return _bmpLogo16; }
			set { _bmpLogo16 = value; }
        }
		public static object Logo65
		{
			get { return _bmpLogo65; }
			set { _bmpLogo65 = value; }
		}

        private static object _bmpInstanceConference16;
        public static object InstantConference16
        {
            get { return _bmpInstanceConference16; }
            set { _bmpInstanceConference16 = value; }
        }
		
        private ICacheManager CacheManager
        {
            get
            {
                lock (_cacheLock)
                {
                    if (!MyVrmService.Service.Url.Equals(_currentCachedWebServiceUrl, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (_cacheManager != null)
                        {
                            CacheFactory.ResetCache();
                            _cacheManager = null;
                        }
                    }
                    if (_cacheManager == null)
                    {
                        _cacheManager = CacheFactory.GetCacheManager(MakeCachePath(MyVrmService.Service.Url));
                        _currentCachedWebServiceUrl = MyVrmService.Service.Url;
                    }
                    return _cacheManager;
                }
            }
        }

        public void ClearRoomCache()
        {
            CacheManager.Flush();
        }

        public Room GetRoomFromCache(RoomId roomId)
        {
            return GetRoomFromCache(roomId, false);
        }

        public Room GetRoomFromCache(RoomId roomId, bool ignoreCache)
        {
            Room room = null;
            var key = roomId.ToString();
            if (!ignoreCache)
            {
            	try
            	{
					if (CacheManager.Contains(key))
					{
						room = (Room)CacheManager[key];
					}
            	}
				catch (Exception ex) //Can be provoked by deserialization error because of incorrect assemble versions
            	{
					MyVrmAddin.TraceSource.TraceException(ex);
            	}
            }
            if (room == null)
            {
                room = MyVrmService.Service.GetRoom(roomId);
                if (CacheManager.Contains(key))
                {
                    CacheManager.Remove(key);
                }
                CacheManager.Add(room.Id.ToString(), room);
            }
            return room;
        }

		/**/
		public Room GetRoomFromCache_BasicInfo(RoomId roomId)
		{
			return GetRoomFromCache_BasicInfo(roomId, false);
		}

		public Room GetRoomFromCache_BasicInfo(RoomId roomId, bool ignoreCache)
		{
			Room room = null;
			var key = roomId.ToString();
			if (!ignoreCache)
			{
				try
				{
					if (CacheManager.Contains(key))
					{
						room = (Room)CacheManager[key];
					}
				}
				catch (Exception ex) //Can be provoked by deserialization error because of incorrect assemble versions
				{
					MyVrmAddin.TraceSource.TraceException(ex);
				}
			}
			if (room == null)
			{
				room = MyVrmService.Service.GetAllRoomsBasicInfo(roomId);
				if (CacheManager.Contains(key))
				{
					CacheManager.Remove(key);
				}
				CacheManager.Add(room.Id.ToString(), room);
			}
			return room;
		}

		public void UpdateRoomCache(Room room)
		{
			if (room != null)
			{
				var key = room.Id.ToString();
				if (CacheManager.Contains(key))
				{
					var roomExist = (Room)CacheManager[key];
					//if (roomExist.PublicRoomData != null && room.PublicRoomData != null &&
					//        roomExist.PublicRoomData.PublicRoomLastModified != room.PublicRoomData.PublicRoomLastModified) 
					if (roomExist.LastModifiedDate != room.LastModifiedDate) //time stamp differs
					{
						CacheManager.Remove(key);
						CacheManager.Add(room.Id.ToString(), room);
					}
				}
				else //not exist
				{
					CacheManager.Add(room.Id.ToString(), room);
				}
			}
		}
		//
		public Room GetRoomOnlyFromCache(RoomId roomId)
		{
			Room room = null;
			var key = roomId.ToString();

			try
			{
				if (CacheManager.Contains(key))
				{
					room = (Room) CacheManager[key];
				}
			}
			catch (Exception ex) //Can be provoked by deserialization error because of incorrect assemble versions
			{
				TraceSource.TraceException(ex);
			}
			return room;
		}

    	/**/

        public User GetUserFromCache(UserId userId)
        {
            var user = _userCache.Find(match => match.Id == userId);
            if (user == null)
            {
                user = MyVrmService.Service.GetUser(userId);
                _userCache.Add(user);
            }
            return user;
        }

        public IEnumerable<AudioUser> GetAudioUsers()
        {
            return _audioUsersCache ?? (_audioUsersCache = new List<AudioUser>(MyVrmService.Service.GetAudioUsers()));
        }

        public SettingsProvider GetNewSettingsProvider()
        {
            var provider = new AddinUserSettingsProvider(Path.Combine(LocalUserAppDataPath, UserConfigFileName)) { ApplicationName = ProductName };
            return provider;
        }

        public string DefaultLicenseAgreement
        {
            get
            {
                if (_defaultEulaText != null) 
                    return _defaultEulaText;
                var eulaFile =
                    DefaultLicenseAgreementFile;
                if (File.Exists(eulaFile))
                {
                    _defaultEulaText = File.ReadAllText(eulaFile);
                }
                return _defaultEulaText;
            }
        }

        public static string DefaultLicenseAgreementFile
        {
            get
            {
                return Path.Combine(new Uri(Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase)).LocalPath,
                                    EulaFile);
            }
        }

        public bool IsLicenseAgreementAccepted
        {
            get
            {
                var eulaFile = Path.Combine(LocalUserAppDataPath, EulaFile);
                if (File.Exists(eulaFile))
                {
                    var savedEula = File.ReadAllText(eulaFile);
                    return savedEula == DefaultLicenseAgreement;
                    
                }
                return false;
            }
        }

        public void AcceptLicenseAgreement(string srcEulaFile)
        {
            var eulaFile = Path.Combine(LocalUserAppDataPath, EulaFile);
            File.Copy(srcEulaFile, eulaFile, true);
        }

        private void SettingsSaving(object sender, CancelEventArgs e)
        {
            SaveWebServiceUrlToLocalUserRegistry(Settings.MyVrmServiceUrl);
            SaveWebServiceAuthModeToLocalUserRegistry(Settings.AuthenticationMode);
            ApplySettings();
        }

        private void SettingsLoaded(object sender, SettingsLoadedEventArgs e)
        {
            bool needToSave = false;
            string webServiceUrl = GetWebServiceUrlFromLocalUserRegistry();
            AuthenticationMode authenticationMode = GetWebServiceAuthModeFromLocalUserRegistry();
            if (!string.IsNullOrEmpty(webServiceUrl) && !webServiceUrl.Equals(Settings.MyVrmServiceUrl, StringComparison.InvariantCultureIgnoreCase))
            {
                Settings.MyVrmServiceUrl = webServiceUrl;
                needToSave = true;
            }
            if (authenticationMode != AuthenticationMode.Unknown && authenticationMode != Settings.AuthenticationMode)
            {
                Settings.AuthenticationMode = authenticationMode;
                if (authenticationMode == AuthenticationMode.Windows)
                {
                    Settings.UseDefaultCredentials = true;
                    Settings.UserName = string.Empty;
                    Settings.UserDomain = string.Empty;
                    Settings.UserPassword = string.Empty;
                    needToSave = true;
                }
            }
            if (needToSave)
                Settings.Save();
            ApplySettings();
        }

        private void ApplySettings()
        {
			//Settings.MyVrmServiceUrl = "http://daybook.myvrm.com";
            if (!string.IsNullOrEmpty(Settings.MyVrmServiceUrl))
            {
                MyVrmService.Service.Url = Settings.MyVrmServiceUrl;
                MyVrmService.Service.AuthenticationMode = Settings.AuthenticationMode;
                MyVrmService.Service.UseDefaultCredential = Settings.UseDefaultCredentials;
                MyVrmService.Service.Credential = new NetworkCredential(Settings.UserName, Settings.UserPassword, Settings.UserDomain);
                CacheFactory.ResetCache();
                //MyVrmService.Service.CacheManager = CacheFactory.GetCacheManager(MakeCachePath(MyVrmService.Service.Url));
            }
        }

        private Settings GetSettings()
        {
            var settings = new Settings();
            var provider = GetNewSettingsProvider();
            provider.Initialize(null, null);
            settings.UpdateProvider(provider);
            settings.Reload();
            if (!settings.IsNewSettings)
            {
                var videoMeetingSettingsProvider = GetVideoMeetingAddinSettingsProvider();
                if (videoMeetingSettingsProvider != null)
                {
                    // Migrate settings from Video Meeting add-in configuration file
                    videoMeetingSettingsProvider.Initialize(null, null);
                    settings.UpdateProvider(videoMeetingSettingsProvider);
                    settings.Reload();
                    settings.UpdateProvider(provider);
                    settings.IsNewSettings = true;
                    settings.Save();
                }
                else
                {
                    // Migrate settings from old formatted (XML) configuration file
                    var oldSettings = new Settings();
                    oldSettings.Reload();
                    var url = oldSettings.MyVrmServiceUrl;
                    oldSettings.MyVrmServiceUrl = url;
                    oldSettings.UpdateProvider(provider);
                    oldSettings.IsNewSettings = true;
                    oldSettings.Save();
                }
            }
            var newSettings = new Settings();
            newSettings.UpdateProvider(provider);
            return newSettings;
        }
        /// <summary>
        /// Returns <see cref="SettingsProvider"/> for York's Video Meeting add-in. This method is used for migrating Video Meeting settings to generic add-in.
        /// </summary>
        /// <returns><see cref="SettingsProvider"/> for York's Video Meeting add-in if exists otherwise null.</returns>
        private SettingsProvider GetVideoMeetingAddinSettingsProvider()
        {
            var configPath = Path.Combine(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                                          @"York Telecom\Video Meeting\1.0"), UserConfigFileName);
            return File.Exists(configPath) ? new AddinUserSettingsProvider(configPath) : null;
        }

        private static string MakeCachePath(string url)
        {
            if (string.IsNullOrEmpty(url)) 
                throw new ArgumentNullException("url");
            var path = Path.Combine(Path.Combine(LocalUserAppDataPath, "Cache"), new Uri(url).Host);
            lock(SyncObject)
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
            }
            return Path.Combine(path,
                                CacheFileName);
        }

        private static string GetWebServiceUrlFromLocalUserRegistry()
        {
            const string regValue = "WebServiceUrl";
            try
            {
                var key = Registry.CurrentUser.OpenSubKey(LocalUserAppRegPath);
                return (string)key.GetValue(regValue, string.Empty);
            }
            catch (Exception e)
            {
                TraceSource.TraceWarning(Strings.FailedToReadRegistryValue, regValue, e.Message);
                return string.Empty;
            }
        }

        private static void SaveWebServiceUrlToLocalUserRegistry(string url)
        {
            const string regValue = "WebServiceUrl";
            try
            {
                var key = Registry.CurrentUser.CreateSubKey(LocalUserAppRegPath);
                key.SetValue(regValue, url, RegistryValueKind.String);
            }
            catch (Exception e)
            {
                TraceSource.TraceWarning(Strings.FailedToSaveRegistryValue, regValue, e.Message);
            }
        }

        private static AuthenticationMode GetWebServiceAuthModeFromLocalUserRegistry()
        {
            const string regValue = "WebServiceAuthMode";
            try
            {
                var key = Registry.CurrentUser.CreateSubKey(LocalUserAppRegPath);
                return (AuthenticationMode)Enum.Parse(typeof(AuthenticationMode), (string)key.GetValue(regValue, string.Empty));
            }
            catch (Exception e)
            {
                TraceSource.TraceWarning(Strings.FailedToReadRegistryValue, regValue, e.Message);
                return AuthenticationMode.Unknown;
            }
        }

        private static void SaveWebServiceAuthModeToLocalUserRegistry(AuthenticationMode mode)
        {
            const string regValue = "WebServiceAuthMode";
            try
            {
                var key = Registry.CurrentUser.CreateSubKey(LocalUserAppRegPath);
                key.SetValue(regValue, Enum.GetName(typeof(AuthenticationMode), mode), RegistryValueKind.String);
            }
            catch (Exception e)
            {
                TraceSource.TraceWarning(Strings.FailedToSaveRegistryValue, regValue, e.Message);
            }
        }
    }
}
