﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace MyVrm.WebServices.Data
{
    internal class GetVideoEquipmentRequest : ServiceRequestBase<GetVideoEquipmentResponse>
    {
        public GetVideoEquipmentRequest(MyVrmService service) : base(service)
        {
        }

        #region Overrides of ServiceRequestBase

        internal override string GetXmlElementName()
        {
            return "GetVideoEquipment";
        }

        internal override string GetCommandName()
        {
            return "GetVideoEquipment";
        }

        internal override string GetResponseXmlElementName()
        {
            return "VideoEquipment";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            writer.WriteElementValue(XmlNamespace.NotSpecified, "UserID", UserId.Id);
        }

        #endregion

    }
}
