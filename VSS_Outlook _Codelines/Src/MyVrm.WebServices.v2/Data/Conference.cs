﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using MyVrm.Common;

namespace MyVrm.WebServices.Data
{
    /// <summary>
    /// Represents a conference in myVRM Web Service.
    /// </summary>
    [ServiceObjectDefinition("confInfo")]
	public class Conference : ConferenceBase 
    {
		private DateTime? _occurrenceDate;

		private ConferenceAdvancedAudioVideoSettings _advancedAudioVideoSettings;
		private WorkOrderCollection _inventoryWorkOrders;
		private WorkOrderCollection _houseKeepingWorkOrders;
		private CateringWorkOrderCollection _cateringWorkOrders;

    	/// <summary>
        /// Creates a new instance of the <see cref="Conference"/> class.
        /// </summary>
        public Conference(MyVrmService service) : base(service)
        {
        	Type = Service.OrganizationOptions.DefaultConferenceType;
        }

		internal Conference(MyVrmService service, PropertyBag propertyBag)
			: base(service, propertyBag)
		{
		}

        /// <summary>
        /// Binds to the existing conference.
        /// </summary>
        /// <param name="service">The instance of <see cref="MyVrmService"/>.</param>
        /// <param name="id">The identifier of the conference.</param>
        /// <returns>The instance of <see cref="Conference"/>.</returns>
		public new static Conference Bind(MyVrmService service, ConferenceId id)
		{
			var conference = service.GetConference(id);
			try
			{
				if (conference.IsRecurring)
				{
					if (conference.RecurrenceRange != null)
						conference.RecurrenceRange.SetupRecurrence(conference.RecurrencePattern);
					conference.RecurrencePattern.StartTime = conference.AppointmentTime.StartTime;
					conference.RecurrencePattern.EndTime = conference.AppointmentTime.EndTime;
					conference.StartDate = conference.RecurrencePattern.StartDate +
					                       conference.RecurrencePattern.StartTime;
					conference.Duration = conference.AppointmentTime.Duration;
				}
				return conference;
			}
			finally
			{
				conference.ClearChangeLog();
			}
		}

    	/// <summary>
    	/// Binds to the occurence of the recurring conference.
    	/// </summary>
    	/// <param name="service">The instance of <see cref="MyVrmService"/>.</param>
    	/// <param name="id">The identifier of the conference.</param>
    	/// <param name="occurrenceDate">The date of the occurence.</param>
    	/// <param name="startTimeInTimeZone"></param>
    	/// <param name="endTimeInTimeZone"></param>
    	/// <returns>The instance of <see cref="Conference"/>.</returns>
    	public static Conference Bind(MyVrmService service, ConferenceId id, DateTime occurrenceDate, 
			DateTime startTimeInTimeZone, DateTime endTimeInTimeZone)
		{
			var conference = service.GetConference(id);
			try
			{
				if (conference.IsRecurring)
				{
					if (conference.RecurrenceRange != null)
						conference.RecurrenceRange.SetupRecurrence(conference.RecurrencePattern);
					//conference.RecurrencePattern.StartTime = startTimeInTimeZone.TimeOfDay;
					//conference.RecurrencePattern.EndTime = endTimeInTimeZone.TimeOfDay;
					conference.StartDate = occurrenceDate.Date + startTimeInTimeZone.TimeOfDay; // conference.RecurrencePattern.StartTime;
					conference.Duration = endTimeInTimeZone.TimeOfDay - startTimeInTimeZone.TimeOfDay;
					conference.IsRecurring = false;
					conference.RecurrencePattern = null;  
				}
				conference._occurrenceDate = occurrenceDate;
				return conference;
			}
			finally
			{
				conference.ClearChangeLog();
			}
		}

        /// <summary>
        /// Gets or sets conference type.
        /// </summary>
        public override ConferenceType Type
        {
            get
            {
                return (ConferenceType)PropertyBag[ConferenceSchema.Type];
            }
            set
            {
                PropertyBag[ConferenceSchema.Type] = value;
            }
        }

        public override ConferenceType confType
        {
            get
            {
                return (ConferenceType)PropertyBag[ConferenceSchema.confType];
            }
            set
            {
                PropertyBag[ConferenceSchema.confType] = value;
            }
        }
		
		/// <summary>
		/// Gets advanced audio-video settings
		/// </summary>
		public ConferenceAdvancedAudioVideoSettings AdvancedAudioVideoSettings
		{
			get
			{
				if (_advancedAudioVideoSettings == null)
				{
					// Do not get AV settings for a room conference and for VMR conference. It always fail.
					_advancedAudioVideoSettings = !IsNew && Type != ConferenceType.RoomConference &&
						 (VMRType == VMRConferenceType.None || VMRType == VMRConferenceType.Room ) //ZD 104170
						? Service.GetConferenceAdvancedAudioVideoSettings(Id)
						: new ConferenceAdvancedAudioVideoSettings();
					((IOwnedProperty)_advancedAudioVideoSettings).Owner = this;
				}
				return _advancedAudioVideoSettings;
			}
		}
		/// <summary>
		/// Gets a list of audio/video work orders for this conference.
		/// </summary>
		public WorkOrderCollection InventoryWorkOrders
		{
			get
			{
				if (_inventoryWorkOrders == null)
				{
					if (!IsNew)
					{
						_inventoryWorkOrders = new WorkOrderCollection();

						foreach (var workOrderId in Service.SearchConferenceWorkOrders(Id, WorkOrderType.Inventory))
						{
							_inventoryWorkOrders.WorkOrders.Add(Service.GetWorkOrderDetails(Id, workOrderId));
						}
						_inventoryWorkOrders.ClearChangeLog();
					}
					else
					{
						_inventoryWorkOrders = new WorkOrderCollection();
					}
					((IOwnedProperty)_inventoryWorkOrders).Owner = this;
				}

				return _inventoryWorkOrders;
			}
		}
		/// <summary>
		/// Gets a list of housekeeping work orders for this conference.
		/// </summary>
		public WorkOrderCollection HouseKeepingWorkOrders
		{
			get
			{
				if (_houseKeepingWorkOrders == null)
				{
					if (!IsNew)
					{
						_houseKeepingWorkOrders = new WorkOrderCollection();
						foreach (var workOrderId in Service.SearchConferenceWorkOrders(Id, WorkOrderType.Housekeeping))
						{
							_houseKeepingWorkOrders.WorkOrders.Add(Service.GetWorkOrderDetails(Id, workOrderId));
						}
						_houseKeepingWorkOrders.ClearChangeLog();
					}
					else
					{
						_houseKeepingWorkOrders = new WorkOrderCollection();
					}
					((IOwnedProperty)_houseKeepingWorkOrders).Owner = this;
				}
				return _houseKeepingWorkOrders;
			}
		}

		public CateringWorkOrderCollection CateringWorkOrders
		{
			get
			{
				if (_cateringWorkOrders == null)
				{
					if (!IsNew)
					{
						_cateringWorkOrders = new CateringWorkOrderCollection();
						foreach (var workOrderId in Service.SearchConferenceWorkOrders(Id, WorkOrderType.Catering))
						{
							_cateringWorkOrders.WorkOrders.Add(Service.GetCateringWorkOrderDetails(Id, workOrderId));
						}
						_cateringWorkOrders.ClearChangeLog();
					}
					else
					{
						_cateringWorkOrders = new CateringWorkOrderCollection();
					}
					((IOwnedProperty)_cateringWorkOrders).Owner = this;
				}
				return _cateringWorkOrders;
			}
		}
		/**/
        /// <summary>
        /// The collection of file paths linked with the conference.
        /// </summary>
        public ConferenceFileCollection Files
        {
            get { return (ConferenceFileCollection) PropertyBag[ConferenceSchema.FilesToUpload]; }
        }
        /// <summary>
        /// Gets Boolean value that specifies whether the conference can be modified. 
        /// </summary>
        public bool IsAllowedToModify
        {
            get { return IsNew || HostId == Service.UserId; }
        }
		/// <summary>
		/// Saves the conference or if this is a new conference then creates this.
		/// </summary>
      
		public void Save()
		{

            
			// Ignore if there are no changes in the conference
			if (!IsDirty) 	
				return;
			Origin = ConferenceOrigin.Api;
			// Force load advanced AV settings
			if (AdvancedAudioVideoSettings == null)
				return;

            
			if (IsRecurring)
			{
				// Fill appointment time for recurring conference
				AppointmentTime = GetAppointmentTime();
				//Save as recurring conference
				Service.SaveConference(this);
			}
			else
			{
				if (_occurrenceDate.HasValue)
				{
					// Save as occurrence of recurring conference
					Service.SaveConferenceOccurrence(this, _occurrenceDate.Value);
				}
				else
				{
					// Save as normal conference

					Service.SaveConference(this);
				}
			}
			//AdvancedAudioVideoSettings.Save();
			if (_inventoryWorkOrders != null)
			{
				InventoryWorkOrders.Save();
			}
			if (_houseKeepingWorkOrders != null)
			{
				HouseKeepingWorkOrders.Save();
			}
			if (_cateringWorkOrders != null)
			{
				CateringWorkOrders.Save();
			}
			ClearChangeLog();
		}

		public override void Delete()
		{
            //Service.CheckModifyOperation(this);
			if (IsAllowedToModify)
			{
				InventoryWorkOrders.Clear();
				InventoryWorkOrders.Save();
				CateringWorkOrders.Clear();
				CateringWorkOrders.Save();
				HouseKeepingWorkOrders.Clear();
				HouseKeepingWorkOrders.Save();

				base.Delete();
			}
		}

        /// <summary>
        /// Deletes the occurrance of the recurring conference.
        /// </summary>
        /// <param name="instanceDate">Instance date.</param>
        public void DeleteOccurrance(DateTime instanceDate)
        {
            //Service.CheckModifyOperation(this);
			if (IsAllowedToModify)
				Service.DeleteRecurringConferenceInstance(Id, instanceDate);
        }

        private RecurrenceRange RecurrenceRange
		{
			get { return (RecurrenceRange)PropertyBag[ConferenceSchema.RecurrenceRange]; }
			set { PropertyBag[ConferenceSchema.RecurrenceRange] = value; }
		}

		public override object Clone()
		{
			Conference _conf = null;
			try
			{
				_conf = (Conference)base.Clone();
				ConferenceAdvancedAudioVideoSettings ad = AdvancedAudioVideoSettings;
				if (_advancedAudioVideoSettings != null)
					_conf._advancedAudioVideoSettings = (ConferenceAdvancedAudioVideoSettings) _advancedAudioVideoSettings.Clone();
				
				if (_cateringWorkOrders != null)
				{
					_conf._cateringWorkOrders = (CateringWorkOrderCollection) _cateringWorkOrders.Clone();
				}

				if (_houseKeepingWorkOrders != null)
				{
					_conf._houseKeepingWorkOrders = (WorkOrderCollection) _houseKeepingWorkOrders.Clone();
				}

				if (_inventoryWorkOrders != null)
				{
					_conf._inventoryWorkOrders = (WorkOrderCollection) _inventoryWorkOrders.Clone();
				}

				if (ModifiedOccurrences != null)
				{
					_conf.ModifiedOccurrences.Clear();
					foreach (OccurrenceInfo modifiedOccurrence in ModifiedOccurrences)
					{
						OccurrenceInfo ocCopy = (OccurrenceInfo) modifiedOccurrence.Clone();//new OccurrenceInfo();
						_conf.ModifiedOccurrences.Add(ocCopy);
					}
				}
				
				if (_occurrenceDate != null)
					_conf._occurrenceDate = new DateTime(_occurrenceDate.Value.Ticks) ;
			}
			catch (Exception exception)
			{
				MyVrmService.TraceSource.TraceException(exception);
			}

			return _conf;
		}

		public override bool Equals(object savingConference)
		{
			if (ReferenceEquals(this, savingConference))
				return true;

			Conference confToCompare = savingConference as Conference;
			if (confToCompare == null)
				return false;

			int i = 0;

			if (AdvancedAudioVideoSettings != null && confToCompare.AdvancedAudioVideoSettings == null ||
				AdvancedAudioVideoSettings == null && confToCompare.AdvancedAudioVideoSettings != null )
				return false;

			if (AdvancedAudioVideoSettings != null && AdvancedAudioVideoSettings.Endpoints != null && 
				confToCompare.AdvancedAudioVideoSettings != null && confToCompare.AdvancedAudioVideoSettings.Endpoints == null )
				return false;

			if (AdvancedAudioVideoSettings != null && AdvancedAudioVideoSettings.Endpoints == null &&
				confToCompare.AdvancedAudioVideoSettings != null && confToCompare.AdvancedAudioVideoSettings.Endpoints != null )
				return false;

			if (AdvancedAudioVideoSettings != null && AdvancedAudioVideoSettings.Endpoints != null && AdvancedAudioVideoSettings.Endpoints.Items != null &&
				confToCompare.AdvancedAudioVideoSettings != null && confToCompare.AdvancedAudioVideoSettings.Endpoints != null && confToCompare.AdvancedAudioVideoSettings.Endpoints.Items == null)
				return false;

			if (AdvancedAudioVideoSettings != null && AdvancedAudioVideoSettings.Endpoints != null && AdvancedAudioVideoSettings.Endpoints.Items == null &&
					confToCompare.AdvancedAudioVideoSettings != null && confToCompare.AdvancedAudioVideoSettings.Endpoints != null && confToCompare.AdvancedAudioVideoSettings.Endpoints.Items != null)
				return false;

			if (AdvancedAudioVideoSettings != null && AdvancedAudioVideoSettings.Endpoints != null &&
				confToCompare.AdvancedAudioVideoSettings != null && confToCompare.AdvancedAudioVideoSettings.Endpoints != null &&
				confToCompare.AdvancedAudioVideoSettings.Endpoints.Count != AdvancedAudioVideoSettings.Endpoints.Count)
				return false;

			if (AdvancedAudioVideoSettings != null && AdvancedAudioVideoSettings.Endpoints != null &&
				AdvancedAudioVideoSettings.Endpoints.Items != null &&
				confToCompare.AdvancedAudioVideoSettings != null && confToCompare.AdvancedAudioVideoSettings.Endpoints != null &&
				confToCompare.AdvancedAudioVideoSettings.Endpoints.Items != null &&
				confToCompare.AdvancedAudioVideoSettings.Endpoints.Items.Count != AdvancedAudioVideoSettings.Endpoints.Items.Count)
				return false;

			for (i = 0; confToCompare.AdvancedAudioVideoSettings != null && confToCompare.AdvancedAudioVideoSettings.Endpoints != null &&
				confToCompare.AdvancedAudioVideoSettings.Endpoints.Items != null && AdvancedAudioVideoSettings.Endpoints != null &&
				i < confToCompare.AdvancedAudioVideoSettings.Endpoints.Items.Count; i++)
			{
				if (!confToCompare.AdvancedAudioVideoSettings.Endpoints.Items[i].Equals(AdvancedAudioVideoSettings.Endpoints[i]))
					return false;
			}

			if (AdvancedAudioVideoSettings != null && confToCompare.AdvancedAudioVideoSettings.SingleDialin != AdvancedAudioVideoSettings.SingleDialin)
				return false;

			if (AppointmentTime != null && confToCompare.AppointmentTime == null ||
				AppointmentTime == null && confToCompare.AppointmentTime != null)
				return false;

			if (AppointmentTime != null && !AppointmentTime.Equals(confToCompare.AppointmentTime))
				return false;

			if (AudioVideoParameters != null && confToCompare.AudioVideoParameters == null ||
				AudioVideoParameters == null && confToCompare.AudioVideoParameters != null)
				return false;

			if (AudioVideoParameters != null && !AudioVideoParameters.Equals(confToCompare.AudioVideoParameters))
				return false;

			if (CateringWorkOrders != null && confToCompare.CateringWorkOrders == null ||
				CateringWorkOrders == null && confToCompare.CateringWorkOrders != null )
				return false;

			if (CateringWorkOrders != null && confToCompare.CateringWorkOrders.Count != CateringWorkOrders.Count)
				return false;
			for (i = 0; confToCompare.CateringWorkOrders != null && i < confToCompare.CateringWorkOrders.Count; i++)
			{
				if (!confToCompare.CateringWorkOrders[i].Equals(CateringWorkOrders[i]))
					return false;
			}
			if (CustomAttributes != null && confToCompare.CustomAttributes == null ||
				CustomAttributes == null && confToCompare.CustomAttributes != null ||
				CustomAttributes != null && confToCompare.CustomAttributes != null && CustomAttributes.Count != confToCompare.CustomAttributes.Count)
				return false;
			for (i = 0; confToCompare.CustomAttributes != null && i < confToCompare.CustomAttributes.Count; i++)
			{
				if (!confToCompare.CustomAttributes[i].Equals(CustomAttributes[i]))
					return false;
			}
			if( confToCompare.Description != Description )
				return false;
			if( confToCompare.StartDate != StartDate )
				return false;
			if( confToCompare.Duration != Duration )
				return false;
			if( confToCompare.EndDate != EndDate )
				return false;

			if (Files != null && confToCompare.Files == null ||
				Files == null && confToCompare.Files != null)
				return false;
			if (Files != null && confToCompare.Files != null)
			{
				if (Files.Count() != confToCompare.Files.Count())
					return false;
			}
			if (Files != null)
			{
				IEnumerator<string> enumF = Files.GetEnumerator();
				IEnumerator<string> enumCopyF = confToCompare.Files.GetEnumerator();
				while ((enumF.MoveNext()) && (enumCopyF.MoveNext())  )
				{
					if( enumF.Current != enumCopyF.Current)
						return false;
				}
			}
			if( HostEmail != confToCompare.HostEmail)
				return false;
			if( confToCompare.HostId != HostId )
				return false;

			if (HostName != confToCompare.HostName)
				return false;

			if (HouseKeepingWorkOrders != null && confToCompare.HouseKeepingWorkOrders == null ||
				HouseKeepingWorkOrders == null && confToCompare.HouseKeepingWorkOrders != null)
				return false;
			if (HouseKeepingWorkOrders != null && confToCompare.HouseKeepingWorkOrders.Count != HouseKeepingWorkOrders.Count)
				return false;
			for (i = 0; confToCompare.HouseKeepingWorkOrders != null && i < confToCompare.HouseKeepingWorkOrders.Count; i++)
			{
				if (!confToCompare.HouseKeepingWorkOrders[i].Equals(HouseKeepingWorkOrders[i]))
					return false;
			}

			if (confToCompare.Id != null && Id == null || confToCompare.Id == null && Id != null )
			return false;

			if (confToCompare.Id != null && Id != null && confToCompare.Id.Id != Id.Id)
				return false;

			if (InventoryWorkOrders != null && confToCompare.InventoryWorkOrders == null ||
				InventoryWorkOrders == null && confToCompare.InventoryWorkOrders != null)
				return false;

			if (InventoryWorkOrders != null && confToCompare.InventoryWorkOrders.Count != InventoryWorkOrders.Count)
				return false;
			for (i = 0; confToCompare.InventoryWorkOrders != null && i < confToCompare.InventoryWorkOrders.Count; i++)
			{
				if (!confToCompare.InventoryWorkOrders[i].Equals(InventoryWorkOrders[i]))
					return false;
			}
			
			if( confToCompare.IsImmediate != IsImmediate )
				return false;
			if( confToCompare.IsOpenForPublicRegistration != IsOpenForPublicRegistration )
				return false;
			if( confToCompare.IsPublic != IsPublic )
				return false;
			if( confToCompare.IsRecurring != IsRecurring )
				return false;

			if (ModifiedOccurrences != null && confToCompare.ModifiedOccurrences == null ||
				ModifiedOccurrences == null && confToCompare.ModifiedOccurrences != null)
				return false;

			if (ModifiedOccurrences != null && confToCompare.ModifiedOccurrences.Count != ModifiedOccurrences.Count)
				return false;
			for (i = 0; confToCompare.ModifiedOccurrences != null && i < confToCompare.ModifiedOccurrences.Count; i++)
			{
				if (!confToCompare.ModifiedOccurrences[i].Equals(ModifiedOccurrences[i]))
					return false;
			}

			if( confToCompare.ModifyType != ModifyType )
				return false;
			if( confToCompare.Name != Name )
				return false;
			/*
			 * Can be omitted - because for Outlook conf it is always ConferenceOrigin.Api
			 * if( confToCompare.Origin != Origin )
				return false;
			 * */

			if (Participants != null && confToCompare.Participants == null ||
				Participants == null && confToCompare.Participants != null)
				return false;

			if (Participants != null && confToCompare.Participants.Count != Participants.Count)
				return false;
			for (i = 0; confToCompare.Participants != null && i < confToCompare.Participants.Count; i++)
			{
				if (!confToCompare.Participants[i].Equals(Participants[i]))
					return false;
			}

			if( confToCompare.Password != Password )
				return false;

			if (RecurrencePattern != null && confToCompare.RecurrencePattern == null ||
				RecurrencePattern == null && confToCompare.RecurrencePattern != null)
				return false;

			if (RecurrencePattern != null && !RecurrencePattern.Equals(confToCompare.RecurrencePattern))
				return false;

			if (confToCompare.RecurrenceRange != null && RecurrenceRange == null ||
				confToCompare.RecurrenceRange == null && RecurrenceRange != null )
				return false;
			if (RecurrenceRange != null && confToCompare.RecurrenceRange.StartDate != RecurrenceRange.StartDate)
				return false;

			if( confToCompare.RecurringText != RecurringText )
				return false;

			if(Rooms != null && confToCompare.Rooms == null ||
				Rooms == null && confToCompare.Rooms != null ||
				Rooms.Items != null && confToCompare.Rooms.Items == null ||
				Rooms.Items == null && confToCompare.Rooms.Items != null )
				return false;
			if (Rooms != null && Rooms.Items != null && confToCompare.Rooms != null && confToCompare.Rooms.Items != null &&  
				Rooms.Items.Count != confToCompare.Rooms.Items.Count)
				return false;
			
			for (i = 0; Rooms != null && Rooms.Items != null && i < Rooms.Items.Count ; i++)
			{
				if( Rooms.Items[i].Id != confToCompare.Rooms.Items[i].Id)
					return false;
			}

			if (!confToCompare.TimeZone.Equals(TimeZone))
				return false;

			if( confToCompare.Type != Type )
				return false;

			if (confToCompare.UniqueId != null && UniqueId == null || confToCompare.UniqueId == null && UniqueId != null)
				return false;
			if (confToCompare.UniqueId != null && UniqueId != null && confToCompare.UniqueId != null && confToCompare.UniqueId.Id != UniqueId.Id)
				return false;
			if( confToCompare._occurrenceDate != _occurrenceDate )
				return false;

			if( confToCompare.IsCloudConferencing !=  IsCloudConferencing)
				return false;
			if( confToCompare.IsPCconference != IsPCconference)
				return false;
            //ZD 102651 start
            if( confToCompare.MeetandGreetBuffer != MeetandGreetBuffer)
				return false;
            //ZD 102651 End
			if (confToCompare.VMRType != VMRType)
				return false;
			if( confToCompare.PCVendorId != PCVendorId)
				return false;

			if (confToCompare.IsSecured != IsSecured)
				return false;
			if (confToCompare.ConciergeSupportParams != ConciergeSupportParams)
				return false;
			if (confToCompare.ConfMessageCollection != ConfMessageCollection)
				return false;
			return true;
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
    }
}
