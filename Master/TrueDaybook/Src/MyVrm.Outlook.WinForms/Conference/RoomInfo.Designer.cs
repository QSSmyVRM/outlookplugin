﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.Outlook.WinForms.Conference
{
	partial class RoomInfo
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
			this.memoNotes = new DevExpress.XtraEditors.MemoEdit();
			this.labelNoMap = new DevExpress.XtraEditors.LabelControl();
			this.labelNoImage = new DevExpress.XtraEditors.LabelControl();
			this.labelNotes = new DevExpress.XtraEditors.LabelControl();
			this.labelDescription = new DevExpress.XtraEditors.LabelControl();
			this.textDescription = new DevExpress.XtraEditors.LabelControl();
			this.textInternetAvailable = new DevExpress.XtraEditors.LabelControl();
			this.labelInternetAvailable = new DevExpress.XtraEditors.LabelControl();
			this.textOpenHours = new DevExpress.XtraEditors.LabelControl();
			this.labelOpenHours = new DevExpress.XtraEditors.LabelControl();
			this.textRoomCapacity = new DevExpress.XtraEditors.LabelControl();
			this.textSDHD = new DevExpress.XtraEditors.LabelControl();
			this.textAfterHrsPrice = new DevExpress.XtraEditors.LabelControl();
			this.textOfficeHrsPrice = new DevExpress.XtraEditors.LabelControl();
			this.textEarlyHrsPrice = new DevExpress.XtraEditors.LabelControl();
			this.textLateHrsPrice = new DevExpress.XtraEditors.LabelControl();
			this.textLateHrsTime = new DevExpress.XtraEditors.LabelControl();
			this.textOfficeHrsTime = new DevExpress.XtraEditors.LabelControl();
			this.labelAfterHrs = new DevExpress.XtraEditors.LabelControl();
			this.labelOfficeHrs = new DevExpress.XtraEditors.LabelControl();
			this.textAfterHrsTime = new DevExpress.XtraEditors.LabelControl();
			this.labelEarlyHrs = new DevExpress.XtraEditors.LabelControl();
			this.textPhoneNumber = new DevExpress.XtraEditors.LabelControl();
			this.textLocationName = new DevExpress.XtraEditors.LabelControl();
			this.textCountrySateSuburb = new DevExpress.XtraEditors.LabelControl();
			this.textEquipmentType = new DevExpress.XtraEditors.LabelControl();
			this.textISDN = new DevExpress.XtraEditors.LabelControl();
			this.labelEquipmentType = new DevExpress.XtraEditors.LabelControl();
			this.labelSDHD = new DevExpress.XtraEditors.LabelControl();
			this.labelISDN = new DevExpress.XtraEditors.LabelControl();
			this.labelRoomCapacity = new DevExpress.XtraEditors.LabelControl();
			this.labelLateHrs = new DevExpress.XtraEditors.LabelControl();
			this.webBrowser1 = new System.Windows.Forms.WebBrowser();
			this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
			this.textEarlyHrsTime = new DevExpress.XtraEditors.LabelControl();
			this.labelLocationName = new DevExpress.XtraEditors.LabelControl();
			this.labelCountrySateSuburb = new DevExpress.XtraEditors.LabelControl();
			this.labelPhoneNumber = new DevExpress.XtraEditors.LabelControl();
			this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.groupMap = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
			this.groupPicture = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlNoImage = new DevExpress.XtraLayout.LayoutControlItem();
			this.groupSiteDescription = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlGroup15 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlGroup18 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
			this.groupSiteLocation = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
			this.groupHoursAndPrice = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutEarlyHoursGroup = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutOfficeHoursGroup = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutAfterHoursGroup = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutLateHoursGroup = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
			this.groupTechnicalDetails = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlGroup14 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlGroup17 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlGroup13 = new DevExpress.XtraLayout.LayoutControlGroup();
			((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).BeginInit();
			this.ContentPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
			this.layoutControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.memoNotes.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.groupMap)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.groupPicture)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlNoImage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.groupSiteDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.groupSiteLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.groupHoursAndPrice)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutEarlyHoursGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutOfficeHoursGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutAfterHoursGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutLateHoursGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.groupTechnicalDetails)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).BeginInit();
			this.SuspendLayout();
			// 
			// ContentPanel
			// 
			this.ContentPanel.Controls.Add(this.layoutControl1);
			this.ContentPanel.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
			this.ContentPanel.Size = new System.Drawing.Size(987, 604);
			// 
			// layoutControl1
			// 
			this.layoutControl1.Controls.Add(this.memoNotes);
			this.layoutControl1.Controls.Add(this.labelNoMap);
			this.layoutControl1.Controls.Add(this.labelNoImage);
			this.layoutControl1.Controls.Add(this.labelNotes);
			this.layoutControl1.Controls.Add(this.labelDescription);
			this.layoutControl1.Controls.Add(this.textDescription);
			this.layoutControl1.Controls.Add(this.textInternetAvailable);
			this.layoutControl1.Controls.Add(this.labelInternetAvailable);
			this.layoutControl1.Controls.Add(this.textOpenHours);
			this.layoutControl1.Controls.Add(this.labelOpenHours);
			this.layoutControl1.Controls.Add(this.textRoomCapacity);
			this.layoutControl1.Controls.Add(this.textSDHD);
			this.layoutControl1.Controls.Add(this.textAfterHrsPrice);
			this.layoutControl1.Controls.Add(this.textOfficeHrsPrice);
			this.layoutControl1.Controls.Add(this.textEarlyHrsPrice);
			this.layoutControl1.Controls.Add(this.textLateHrsPrice);
			this.layoutControl1.Controls.Add(this.textLateHrsTime);
			this.layoutControl1.Controls.Add(this.textOfficeHrsTime);
			this.layoutControl1.Controls.Add(this.labelAfterHrs);
			this.layoutControl1.Controls.Add(this.labelOfficeHrs);
			this.layoutControl1.Controls.Add(this.textAfterHrsTime);
			this.layoutControl1.Controls.Add(this.labelEarlyHrs);
			this.layoutControl1.Controls.Add(this.textPhoneNumber);
			this.layoutControl1.Controls.Add(this.textLocationName);
			this.layoutControl1.Controls.Add(this.textCountrySateSuburb);
			this.layoutControl1.Controls.Add(this.textEquipmentType);
			this.layoutControl1.Controls.Add(this.textISDN);
			this.layoutControl1.Controls.Add(this.labelEquipmentType);
			this.layoutControl1.Controls.Add(this.labelSDHD);
			this.layoutControl1.Controls.Add(this.labelISDN);
			this.layoutControl1.Controls.Add(this.labelRoomCapacity);
			this.layoutControl1.Controls.Add(this.labelLateHrs);
			this.layoutControl1.Controls.Add(this.webBrowser1);
			this.layoutControl1.Controls.Add(this.pictureEdit1);
			this.layoutControl1.Controls.Add(this.textEarlyHrsTime);
			this.layoutControl1.Controls.Add(this.labelLocationName);
			this.layoutControl1.Controls.Add(this.labelCountrySateSuburb);
			this.layoutControl1.Controls.Add(this.labelPhoneNumber);
			this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.layoutControl1.Location = new System.Drawing.Point(0, 0);
			this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.layoutControl1.Name = "layoutControl1";
			this.layoutControl1.Root = this.layoutControlGroup1;
			this.layoutControl1.Size = new System.Drawing.Size(987, 604);
			this.layoutControl1.TabIndex = 0;
			this.layoutControl1.Text = "layoutControl1";
			// 
			// memoNotes
			// 
			this.memoNotes.EditValue = "Half Day Rate Listed (min 2 hours up to 4 hours).";
			this.memoNotes.Location = new System.Drawing.Point(527, 538);
			this.memoNotes.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.memoNotes.Name = "memoNotes";
			this.memoNotes.Properties.Appearance.BackColor = System.Drawing.Color.White;
			this.memoNotes.Properties.Appearance.Options.UseBackColor = true;
			this.memoNotes.Properties.Appearance.Options.UseTextOptions = true;
			this.memoNotes.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
			this.memoNotes.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
			this.memoNotes.Size = new System.Drawing.Size(446, 52);
			this.memoNotes.StyleController = this.layoutControl1;
			this.memoNotes.TabIndex = 56;
			// 
			// labelNoMap
			// 
			this.labelNoMap.Appearance.Options.UseTextOptions = true;
			this.labelNoMap.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.labelNoMap.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
			this.labelNoMap.Location = new System.Drawing.Point(14, 298);
			this.labelNoMap.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.labelNoMap.Name = "labelNoMap";
			this.labelNoMap.Size = new System.Drawing.Size(103, 292);
			this.labelNoMap.StyleController = this.layoutControl1;
			this.labelNoMap.TabIndex = 55;
			this.labelNoMap.Text = "No Link";
			this.labelNoMap.Visible = false;
			// 
			// labelNoImage
			// 
			this.labelNoImage.Appearance.Options.UseTextOptions = true;
			this.labelNoImage.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.labelNoImage.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
			this.labelNoImage.Location = new System.Drawing.Point(14, 36);
			this.labelNoImage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.labelNoImage.Name = "labelNoImage";
			this.labelNoImage.Size = new System.Drawing.Size(103, 212);
			this.labelNoImage.StyleController = this.layoutControl1;
			this.labelNoImage.TabIndex = 54;
			this.labelNoImage.Text = "No Image";
			this.labelNoImage.Visible = false;
			// 
			// labelNotes
			// 
			this.labelNotes.Appearance.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
			this.labelNotes.Appearance.Options.UseFont = true;
			this.labelNotes.Appearance.Options.UseTextOptions = true;
			this.labelNotes.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.labelNotes.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
			this.labelNotes.Location = new System.Drawing.Point(377, 538);
			this.labelNotes.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.labelNotes.Name = "labelNotes";
			this.labelNotes.Size = new System.Drawing.Size(146, 52);
			this.labelNotes.StyleController = this.layoutControl1;
			this.labelNotes.TabIndex = 53;
			this.labelNotes.Text = "Notes:";
			// 
			// labelDescription
			// 
			this.labelDescription.Appearance.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
			this.labelDescription.Appearance.Options.UseFont = true;
			this.labelDescription.Appearance.Options.UseTextOptions = true;
			this.labelDescription.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.labelDescription.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
			this.labelDescription.Location = new System.Drawing.Point(377, 512);
			this.labelDescription.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.labelDescription.Name = "labelDescription";
			this.labelDescription.Size = new System.Drawing.Size(146, 22);
			this.labelDescription.StyleController = this.layoutControl1;
			this.labelDescription.TabIndex = 52;
			this.labelDescription.Text = "Description:";
			// 
			// textDescription
			// 
			this.textDescription.Appearance.BackColor = System.Drawing.Color.White;
			this.textDescription.Appearance.Options.UseBackColor = true;
			this.textDescription.Location = new System.Drawing.Point(527, 512);
			this.textDescription.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.textDescription.Name = "textDescription";
			this.textDescription.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
			this.textDescription.Size = new System.Drawing.Size(446, 22);
			this.textDescription.StyleController = this.layoutControl1;
			this.textDescription.TabIndex = 50;
			this.textDescription.Text = "Fully equiped office";
			// 
			// textInternetAvailable
			// 
			this.textInternetAvailable.Appearance.BackColor = System.Drawing.Color.White;
			this.textInternetAvailable.Appearance.Options.UseBackColor = true;
			this.textInternetAvailable.Location = new System.Drawing.Point(527, 485);
			this.textInternetAvailable.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.textInternetAvailable.Name = "textInternetAvailable";
			this.textInternetAvailable.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
			this.textInternetAvailable.Size = new System.Drawing.Size(446, 23);
			this.textInternetAvailable.StyleController = this.layoutControl1;
			this.textInternetAvailable.TabIndex = 49;
			this.textInternetAvailable.Text = "Yes - FREE of charge ";
			// 
			// labelInternetAvailable
			// 
			this.labelInternetAvailable.Appearance.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
			this.labelInternetAvailable.Appearance.Options.UseFont = true;
			this.labelInternetAvailable.Appearance.Options.UseTextOptions = true;
			this.labelInternetAvailable.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.labelInternetAvailable.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
			this.labelInternetAvailable.Location = new System.Drawing.Point(377, 485);
			this.labelInternetAvailable.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.labelInternetAvailable.Name = "labelInternetAvailable";
			this.labelInternetAvailable.Size = new System.Drawing.Size(146, 23);
			this.labelInternetAvailable.StyleController = this.layoutControl1;
			this.labelInternetAvailable.TabIndex = 48;
			this.labelInternetAvailable.Text = "Internet Available:";
			// 
			// textOpenHours
			// 
			this.textOpenHours.Appearance.BackColor = System.Drawing.Color.White;
			this.textOpenHours.Appearance.Options.UseBackColor = true;
			this.textOpenHours.Location = new System.Drawing.Point(527, 459);
			this.textOpenHours.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.textOpenHours.Name = "textOpenHours";
			this.textOpenHours.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
			this.textOpenHours.Size = new System.Drawing.Size(446, 22);
			this.textOpenHours.StyleController = this.layoutControl1;
			this.textOpenHours.TabIndex = 47;
			this.textOpenHours.Text = "Available 24 hours";
			// 
			// labelOpenHours
			// 
			this.labelOpenHours.Appearance.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
			this.labelOpenHours.Appearance.Options.UseFont = true;
			this.labelOpenHours.Appearance.Options.UseTextOptions = true;
			this.labelOpenHours.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.labelOpenHours.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
			this.labelOpenHours.Location = new System.Drawing.Point(377, 459);
			this.labelOpenHours.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.labelOpenHours.Name = "labelOpenHours";
			this.labelOpenHours.Size = new System.Drawing.Size(146, 22);
			this.labelOpenHours.StyleController = this.layoutControl1;
			this.labelOpenHours.TabIndex = 46;
			this.labelOpenHours.Text = "Open Hours:";
			// 
			// textRoomCapacity
			// 
			this.textRoomCapacity.Appearance.BackColor = System.Drawing.Color.White;
			this.textRoomCapacity.Appearance.Options.UseBackColor = true;
			this.textRoomCapacity.Location = new System.Drawing.Point(527, 433);
			this.textRoomCapacity.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.textRoomCapacity.Name = "textRoomCapacity";
			this.textRoomCapacity.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
			this.textRoomCapacity.Size = new System.Drawing.Size(446, 22);
			this.textRoomCapacity.StyleController = this.layoutControl1;
			this.textRoomCapacity.TabIndex = 45;
			this.textRoomCapacity.Text = "10";
			// 
			// textSDHD
			// 
			this.textSDHD.Appearance.BackColor = System.Drawing.Color.White;
			this.textSDHD.Appearance.Options.UseBackColor = true;
			this.textSDHD.Location = new System.Drawing.Point(527, 359);
			this.textSDHD.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.textSDHD.Name = "textSDHD";
			this.textSDHD.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
			this.textSDHD.Size = new System.Drawing.Size(446, 24);
			this.textSDHD.StyleController = this.layoutControl1;
			this.textSDHD.TabIndex = 44;
			this.textSDHD.Text = "Standard && High Definition";
			// 
			// textAfterHrsPrice
			// 
			this.textAfterHrsPrice.Appearance.BackColor = System.Drawing.Color.White;
			this.textAfterHrsPrice.Appearance.Options.UseBackColor = true;
			this.textAfterHrsPrice.Appearance.Options.UseTextOptions = true;
			this.textAfterHrsPrice.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.textAfterHrsPrice.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
			this.textAfterHrsPrice.Location = new System.Drawing.Point(689, 225);
			this.textAfterHrsPrice.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.textAfterHrsPrice.Name = "textAfterHrsPrice";
			this.textAfterHrsPrice.Size = new System.Drawing.Size(138, 25);
			this.textAfterHrsPrice.StyleController = this.layoutControl1;
			this.textAfterHrsPrice.TabIndex = 43;
			this.textAfterHrsPrice.Text = "200.00";
			// 
			// textOfficeHrsPrice
			// 
			this.textOfficeHrsPrice.Appearance.BackColor = System.Drawing.Color.White;
			this.textOfficeHrsPrice.Appearance.BorderColor = System.Drawing.Color.Transparent;
			this.textOfficeHrsPrice.Appearance.Options.UseBackColor = true;
			this.textOfficeHrsPrice.Appearance.Options.UseBorderColor = true;
			this.textOfficeHrsPrice.Appearance.Options.UseTextOptions = true;
			this.textOfficeHrsPrice.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.textOfficeHrsPrice.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
			this.textOfficeHrsPrice.Location = new System.Drawing.Point(538, 225);
			this.textOfficeHrsPrice.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.textOfficeHrsPrice.Name = "textOfficeHrsPrice";
			this.textOfficeHrsPrice.Size = new System.Drawing.Size(141, 25);
			this.textOfficeHrsPrice.StyleController = this.layoutControl1;
			this.textOfficeHrsPrice.TabIndex = 42;
			this.textOfficeHrsPrice.Text = "180.00";
			// 
			// textEarlyHrsPrice
			// 
			this.textEarlyHrsPrice.Appearance.BackColor = System.Drawing.Color.White;
			this.textEarlyHrsPrice.Appearance.Options.UseBackColor = true;
			this.textEarlyHrsPrice.Appearance.Options.UseTextOptions = true;
			this.textEarlyHrsPrice.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.textEarlyHrsPrice.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
			this.textEarlyHrsPrice.Location = new System.Drawing.Point(380, 225);
			this.textEarlyHrsPrice.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.textEarlyHrsPrice.Name = "textEarlyHrsPrice";
			this.textEarlyHrsPrice.Size = new System.Drawing.Size(148, 25);
			this.textEarlyHrsPrice.StyleController = this.layoutControl1;
			this.textEarlyHrsPrice.TabIndex = 41;
			this.textEarlyHrsPrice.Text = "230.00";
			// 
			// textLateHrsPrice
			// 
			this.textLateHrsPrice.Appearance.BackColor = System.Drawing.Color.White;
			this.textLateHrsPrice.Appearance.Options.UseBackColor = true;
			this.textLateHrsPrice.Appearance.Options.UseTextOptions = true;
			this.textLateHrsPrice.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.textLateHrsPrice.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
			this.textLateHrsPrice.Location = new System.Drawing.Point(837, 225);
			this.textLateHrsPrice.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.textLateHrsPrice.Name = "textLateHrsPrice";
			this.textLateHrsPrice.Size = new System.Drawing.Size(133, 25);
			this.textLateHrsPrice.StyleController = this.layoutControl1;
			this.textLateHrsPrice.TabIndex = 40;
			this.textLateHrsPrice.Text = "230.00";
			// 
			// textLateHrsTime
			// 
			this.textLateHrsTime.Appearance.BackColor = System.Drawing.Color.White;
			this.textLateHrsTime.Appearance.Options.UseBackColor = true;
			this.textLateHrsTime.Appearance.Options.UseTextOptions = true;
			this.textLateHrsTime.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.textLateHrsTime.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
			this.textLateHrsTime.Location = new System.Drawing.Point(837, 197);
			this.textLateHrsTime.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.textLateHrsTime.Name = "textLateHrsTime";
			this.textLateHrsTime.Size = new System.Drawing.Size(133, 24);
			this.textLateHrsTime.StyleController = this.layoutControl1;
			this.textLateHrsTime.TabIndex = 37;
			this.textLateHrsTime.Text = "9:00PM-6:30AM";
			// 
			// textOfficeHrsTime
			// 
			this.textOfficeHrsTime.Appearance.BackColor = System.Drawing.Color.White;
			this.textOfficeHrsTime.Appearance.Options.UseBackColor = true;
			this.textOfficeHrsTime.Appearance.Options.UseTextOptions = true;
			this.textOfficeHrsTime.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.textOfficeHrsTime.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
			this.textOfficeHrsTime.Location = new System.Drawing.Point(538, 197);
			this.textOfficeHrsTime.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.textOfficeHrsTime.Name = "textOfficeHrsTime";
			this.textOfficeHrsTime.Size = new System.Drawing.Size(141, 24);
			this.textOfficeHrsTime.StyleController = this.layoutControl1;
			this.textOfficeHrsTime.TabIndex = 36;
			this.textOfficeHrsTime.Text = "8:30AM-5:00PM";
			// 
			// labelAfterHrs
			// 
			this.labelAfterHrs.Appearance.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
			this.labelAfterHrs.Appearance.Options.UseFont = true;
			this.labelAfterHrs.Appearance.Options.UseTextOptions = true;
			this.labelAfterHrs.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.labelAfterHrs.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
			this.labelAfterHrs.Location = new System.Drawing.Point(689, 169);
			this.labelAfterHrs.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.labelAfterHrs.Name = "labelAfterHrs";
			this.labelAfterHrs.Size = new System.Drawing.Size(138, 24);
			this.labelAfterHrs.StyleController = this.layoutControl1;
			this.labelAfterHrs.TabIndex = 35;
			this.labelAfterHrs.Text = "After Hrs";
			// 
			// labelOfficeHrs
			// 
			this.labelOfficeHrs.Appearance.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
			this.labelOfficeHrs.Appearance.Options.UseFont = true;
			this.labelOfficeHrs.Appearance.Options.UseTextOptions = true;
			this.labelOfficeHrs.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.labelOfficeHrs.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
			this.labelOfficeHrs.Location = new System.Drawing.Point(538, 169);
			this.labelOfficeHrs.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.labelOfficeHrs.Name = "labelOfficeHrs";
			this.labelOfficeHrs.Size = new System.Drawing.Size(141, 24);
			this.labelOfficeHrs.StyleController = this.layoutControl1;
			this.labelOfficeHrs.TabIndex = 34;
			this.labelOfficeHrs.Text = "Office Hrs";
			// 
			// textAfterHrsTime
			// 
			this.textAfterHrsTime.Appearance.BackColor = System.Drawing.Color.White;
			this.textAfterHrsTime.Appearance.Options.UseBackColor = true;
			this.textAfterHrsTime.Appearance.Options.UseTextOptions = true;
			this.textAfterHrsTime.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.textAfterHrsTime.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
			this.textAfterHrsTime.Location = new System.Drawing.Point(689, 197);
			this.textAfterHrsTime.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.textAfterHrsTime.Name = "textAfterHrsTime";
			this.textAfterHrsTime.Size = new System.Drawing.Size(138, 24);
			this.textAfterHrsTime.StyleController = this.layoutControl1;
			this.textAfterHrsTime.TabIndex = 38;
			this.textAfterHrsTime.Text = "5:00PM-9:00PM ";
			// 
			// labelEarlyHrs
			// 
			this.labelEarlyHrs.Appearance.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
			this.labelEarlyHrs.Appearance.Options.UseFont = true;
			this.labelEarlyHrs.Appearance.Options.UseTextOptions = true;
			this.labelEarlyHrs.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.labelEarlyHrs.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
			this.labelEarlyHrs.Location = new System.Drawing.Point(380, 169);
			this.labelEarlyHrs.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.labelEarlyHrs.Name = "labelEarlyHrs";
			this.labelEarlyHrs.Size = new System.Drawing.Size(148, 24);
			this.labelEarlyHrs.StyleController = this.layoutControl1;
			this.labelEarlyHrs.TabIndex = 33;
			this.labelEarlyHrs.Text = "Early Hrs";
			// 
			// textPhoneNumber
			// 
			this.textPhoneNumber.Appearance.BackColor = System.Drawing.Color.White;
			this.textPhoneNumber.Appearance.Options.UseBackColor = true;
			this.textPhoneNumber.Location = new System.Drawing.Point(560, 92);
			this.textPhoneNumber.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.textPhoneNumber.Name = "textPhoneNumber";
			this.textPhoneNumber.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
			this.textPhoneNumber.Size = new System.Drawing.Size(413, 24);
			this.textPhoneNumber.StyleController = this.layoutControl1;
			this.textPhoneNumber.TabIndex = 31;
			this.textPhoneNumber.Text = "+44 (0) 207 183 0460";
			// 
			// textLocationName
			// 
			this.textLocationName.Appearance.BackColor = System.Drawing.Color.White;
			this.textLocationName.Appearance.Options.UseBackColor = true;
			this.textLocationName.Location = new System.Drawing.Point(560, 64);
			this.textLocationName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.textLocationName.Name = "textLocationName";
			this.textLocationName.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
			this.textLocationName.Size = new System.Drawing.Size(413, 24);
			this.textLocationName.StyleController = this.layoutControl1;
			this.textLocationName.TabIndex = 30;
			this.textLocationName.Text = "WEST PALM BEACH, Philips Point";
			// 
			// textCountrySateSuburb
			// 
			this.textCountrySateSuburb.Appearance.BackColor = System.Drawing.Color.White;
			this.textCountrySateSuburb.Appearance.Options.UseBackColor = true;
			this.textCountrySateSuburb.Location = new System.Drawing.Point(560, 36);
			this.textCountrySateSuburb.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.textCountrySateSuburb.Name = "textCountrySateSuburb";
			this.textCountrySateSuburb.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
			this.textCountrySateSuburb.Size = new System.Drawing.Size(413, 24);
			this.textCountrySateSuburb.StyleController = this.layoutControl1;
			this.textCountrySateSuburb.TabIndex = 29;
			this.textCountrySateSuburb.Text = "USA / Florida / West Palm Beach";
			// 
			// textEquipmentType
			// 
			this.textEquipmentType.Appearance.BackColor = System.Drawing.Color.White;
			this.textEquipmentType.Appearance.Options.UseBackColor = true;
			this.textEquipmentType.Location = new System.Drawing.Point(527, 331);
			this.textEquipmentType.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.textEquipmentType.Name = "textEquipmentType";
			this.textEquipmentType.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
			this.textEquipmentType.Size = new System.Drawing.Size(446, 24);
			this.textEquipmentType.StyleController = this.layoutControl1;
			this.textEquipmentType.TabIndex = 28;
			this.textEquipmentType.Text = "LifeSize Express 220";
			// 
			// textISDN
			// 
			this.textISDN.Appearance.BackColor = System.Drawing.Color.White;
			this.textISDN.Appearance.Options.UseBackColor = true;
			this.textISDN.LineColor = System.Drawing.Color.Blue;
			this.textISDN.Location = new System.Drawing.Point(527, 303);
			this.textISDN.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.textISDN.Name = "textISDN";
			this.textISDN.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
			this.textISDN.Size = new System.Drawing.Size(446, 24);
			this.textISDN.StyleController = this.layoutControl1;
			this.textISDN.TabIndex = 27;
			this.textISDN.Text = "ISDN at 384Kbps & IP at 1Mbps ";
			// 
			// labelEquipmentType
			// 
			this.labelEquipmentType.Appearance.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
			this.labelEquipmentType.Appearance.Options.UseFont = true;
			this.labelEquipmentType.Appearance.Options.UseTextOptions = true;
			this.labelEquipmentType.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.labelEquipmentType.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
			this.labelEquipmentType.Location = new System.Drawing.Point(377, 331);
			this.labelEquipmentType.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.labelEquipmentType.Name = "labelEquipmentType";
			this.labelEquipmentType.Size = new System.Drawing.Size(146, 24);
			this.labelEquipmentType.StyleController = this.layoutControl1;
			this.labelEquipmentType.TabIndex = 26;
			this.labelEquipmentType.Text = "Equipment Type:";
			// 
			// labelSDHD
			// 
			this.labelSDHD.Appearance.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
			this.labelSDHD.Appearance.Options.UseFont = true;
			this.labelSDHD.Appearance.Options.UseTextOptions = true;
			this.labelSDHD.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.labelSDHD.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
			this.labelSDHD.Location = new System.Drawing.Point(377, 359);
			this.labelSDHD.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.labelSDHD.Name = "labelSDHD";
			this.labelSDHD.Size = new System.Drawing.Size(146, 24);
			this.labelSDHD.StyleController = this.layoutControl1;
			this.labelSDHD.TabIndex = 25;
			this.labelSDHD.Text = "SD / HD Capable:";
			// 
			// labelISDN
			// 
			this.labelISDN.Appearance.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
			this.labelISDN.Appearance.Options.UseFont = true;
			this.labelISDN.Appearance.Options.UseTextOptions = true;
			this.labelISDN.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.labelISDN.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
			this.labelISDN.Location = new System.Drawing.Point(377, 303);
			this.labelISDN.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.labelISDN.Name = "labelISDN";
			this.labelISDN.Size = new System.Drawing.Size(146, 24);
			this.labelISDN.StyleController = this.layoutControl1;
			this.labelISDN.TabIndex = 21;
			this.labelISDN.Text = "ISDN / IP Capable:";
			// 
			// labelRoomCapacity
			// 
			this.labelRoomCapacity.Appearance.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
			this.labelRoomCapacity.Appearance.Options.UseFont = true;
			this.labelRoomCapacity.Appearance.Options.UseTextOptions = true;
			this.labelRoomCapacity.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.labelRoomCapacity.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
			this.labelRoomCapacity.Location = new System.Drawing.Point(377, 433);
			this.labelRoomCapacity.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.labelRoomCapacity.Name = "labelRoomCapacity";
			this.labelRoomCapacity.Size = new System.Drawing.Size(146, 22);
			this.labelRoomCapacity.StyleController = this.layoutControl1;
			this.labelRoomCapacity.TabIndex = 20;
			this.labelRoomCapacity.Text = "Room Capacity:";
			// 
			// labelLateHrs
			// 
			this.labelLateHrs.Appearance.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
			this.labelLateHrs.Appearance.Options.UseFont = true;
			this.labelLateHrs.Appearance.Options.UseTextOptions = true;
			this.labelLateHrs.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.labelLateHrs.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
			this.labelLateHrs.Location = new System.Drawing.Point(837, 169);
			this.labelLateHrs.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.labelLateHrs.Name = "labelLateHrs";
			this.labelLateHrs.Size = new System.Drawing.Size(133, 24);
			this.labelLateHrs.StyleController = this.layoutControl1;
			this.labelLateHrs.TabIndex = 18;
			this.labelLateHrs.Text = "Late Hrs";
			// 
			// webBrowser1
			// 
			this.webBrowser1.IsWebBrowserContextMenuEnabled = false;
			this.webBrowser1.Location = new System.Drawing.Point(121, 298);
			this.webBrowser1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
			this.webBrowser1.Name = "webBrowser1";
			this.webBrowser1.Size = new System.Drawing.Size(228, 292);
			this.webBrowser1.TabIndex = 17;
			this.webBrowser1.Url = new System.Uri("", System.UriKind.Relative);
			// 
			// pictureEdit1
			// 
			this.pictureEdit1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.pictureEdit1.EditValue = global::MyVrm.Outlook.WinForms.Properties.Resources.room23;
			this.pictureEdit1.Location = new System.Drawing.Point(121, 36);
			this.pictureEdit1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.pictureEdit1.Name = "pictureEdit1";
			this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
			this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
			this.pictureEdit1.Size = new System.Drawing.Size(228, 212);
			this.pictureEdit1.StyleController = this.layoutControl1;
			this.pictureEdit1.TabIndex = 4;
			this.pictureEdit1.EditValueChanged += new System.EventHandler(this.pictureEdit1_EditValueChanged);
			// 
			// textEarlyHrsTime
			// 
			this.textEarlyHrsTime.Appearance.BackColor = System.Drawing.Color.White;
			this.textEarlyHrsTime.Appearance.BorderColor = System.Drawing.Color.Black;
			this.textEarlyHrsTime.Appearance.Options.UseBackColor = true;
			this.textEarlyHrsTime.Appearance.Options.UseBorderColor = true;
			this.textEarlyHrsTime.Appearance.Options.UseTextOptions = true;
			this.textEarlyHrsTime.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.textEarlyHrsTime.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
			this.textEarlyHrsTime.Location = new System.Drawing.Point(380, 197);
			this.textEarlyHrsTime.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.textEarlyHrsTime.Name = "textEarlyHrsTime";
			this.textEarlyHrsTime.Size = new System.Drawing.Size(148, 24);
			this.textEarlyHrsTime.StyleController = this.layoutControl1;
			this.textEarlyHrsTime.TabIndex = 19;
			this.textEarlyHrsTime.Text = "6:30AM-8:30AM";
			// 
			// labelLocationName
			// 
			this.labelLocationName.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.labelLocationName.Appearance.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
			this.labelLocationName.Appearance.ForeColor = System.Drawing.Color.Black;
			this.labelLocationName.Appearance.Options.UseFont = true;
			this.labelLocationName.Appearance.Options.UseForeColor = true;
			this.labelLocationName.Appearance.Options.UseTextOptions = true;
			this.labelLocationName.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.labelLocationName.LineLocation = DevExpress.XtraEditors.LineLocation.Center;
			this.labelLocationName.Location = new System.Drawing.Point(377, 64);
			this.labelLocationName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.labelLocationName.Name = "labelLocationName";
			this.labelLocationName.Size = new System.Drawing.Size(179, 24);
			this.labelLocationName.StyleController = this.layoutControl1;
			this.labelLocationName.TabIndex = 8;
			this.labelLocationName.Text = "Location Name:";
			// 
			// labelCountrySateSuburb
			// 
			this.labelCountrySateSuburb.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.labelCountrySateSuburb.Appearance.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
			this.labelCountrySateSuburb.Appearance.ForeColor = System.Drawing.Color.Black;
			this.labelCountrySateSuburb.Appearance.Options.UseFont = true;
			this.labelCountrySateSuburb.Appearance.Options.UseForeColor = true;
			this.labelCountrySateSuburb.Appearance.Options.UseTextOptions = true;
			this.labelCountrySateSuburb.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.labelCountrySateSuburb.LineLocation = DevExpress.XtraEditors.LineLocation.Right;
			this.labelCountrySateSuburb.Location = new System.Drawing.Point(377, 36);
			this.labelCountrySateSuburb.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.labelCountrySateSuburb.Name = "labelCountrySateSuburb";
			this.labelCountrySateSuburb.Size = new System.Drawing.Size(179, 24);
			this.labelCountrySateSuburb.StyleController = this.layoutControl1;
			this.labelCountrySateSuburb.TabIndex = 6;
			this.labelCountrySateSuburb.Text = "Country/State/Suburb:";
			// 
			// labelPhoneNumber
			// 
			this.labelPhoneNumber.Appearance.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
			this.labelPhoneNumber.Appearance.ForeColor = System.Drawing.Color.Black;
			this.labelPhoneNumber.Appearance.Options.UseFont = true;
			this.labelPhoneNumber.Appearance.Options.UseForeColor = true;
			this.labelPhoneNumber.Appearance.Options.UseTextOptions = true;
			this.labelPhoneNumber.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.labelPhoneNumber.Location = new System.Drawing.Point(377, 92);
			this.labelPhoneNumber.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.labelPhoneNumber.Name = "labelPhoneNumber";
			this.labelPhoneNumber.Size = new System.Drawing.Size(179, 24);
			this.labelPhoneNumber.StyleController = this.layoutControl1;
			this.labelPhoneNumber.TabIndex = 10;
			this.labelPhoneNumber.Text = "Phone Number:";
			// 
			// layoutControlGroup1
			// 
			this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
			this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
			this.layoutControlGroup1.GroupBordersVisible = false;
			this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.groupMap,
            this.groupPicture,
            this.groupSiteDescription,
            this.groupSiteLocation,
            this.groupHoursAndPrice,
            this.groupTechnicalDetails});
			this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup1.Name = "Root";
			this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup1.Size = new System.Drawing.Size(987, 604);
			this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup1.Text = "Root";
			this.layoutControlGroup1.TextVisible = false;
			// 
			// groupMap
			// 
			this.groupMap.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
			this.groupMap.AppearanceGroup.Options.UseFont = true;
			this.groupMap.AppearanceGroup.Options.UseTextOptions = true;
			this.groupMap.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.groupMap.AppearanceGroup.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
			this.groupMap.AppearanceItemCaption.Options.UseTextOptions = true;
			this.groupMap.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.groupMap.AppearanceTabPage.Header.ForeColor = System.Drawing.Color.Green;
			this.groupMap.AppearanceTabPage.Header.Options.UseForeColor = true;
			this.groupMap.CustomizationFormText = "LOCATION MAP";
			this.groupMap.ExpandButtonVisible = true;
			this.groupMap.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem14,
            this.layoutControlItem39});
			this.groupMap.Location = new System.Drawing.Point(0, 262);
			this.groupMap.Name = "groupMap";
			this.groupMap.Size = new System.Drawing.Size(363, 342);
			this.groupMap.Text = "LOCATION MAP";
			this.groupMap.Click += new System.EventHandler(this.layoutControlGroup4_Click);
			// 
			// layoutControlItem14
			// 
			this.layoutControlItem14.Control = this.webBrowser1;
			this.layoutControlItem14.CustomizationFormText = "layoutControlItem14";
			this.layoutControlItem14.Location = new System.Drawing.Point(107, 0);
			this.layoutControlItem14.Name = "layoutControlItem14";
			this.layoutControlItem14.Size = new System.Drawing.Size(232, 296);
			this.layoutControlItem14.Text = "layoutControlItem14";
			this.layoutControlItem14.TextLocation = DevExpress.Utils.Locations.Top;
			this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem14.TextToControlDistance = 0;
			this.layoutControlItem14.TextVisible = false;
			this.layoutControlItem14.DoubleClick += new System.EventHandler(this.layoutControlItem14_DoubleClick);
			this.layoutControlItem14.MouseDown += new System.Windows.Forms.MouseEventHandler(this.layoutControlItem14_MouseDown);
			this.layoutControlItem14.Click += new System.EventHandler(this.layoutControlItem14_Click);
			// 
			// layoutControlItem39
			// 
			this.layoutControlItem39.Control = this.labelNoMap;
			this.layoutControlItem39.CustomizationFormText = "layoutControlItem39";
			this.layoutControlItem39.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem39.MinSize = new System.Drawing.Size(79, 20);
			this.layoutControlItem39.Name = "layoutControlItem39";
			this.layoutControlItem39.Size = new System.Drawing.Size(107, 296);
			this.layoutControlItem39.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem39.Text = "layoutControlItem39";
			this.layoutControlItem39.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem39.TextToControlDistance = 0;
			this.layoutControlItem39.TextVisible = false;
			// 
			// groupPicture
			// 
			this.groupPicture.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
			this.groupPicture.AppearanceGroup.Options.UseFont = true;
			this.groupPicture.AppearanceGroup.Options.UseTextOptions = true;
			this.groupPicture.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.groupPicture.CustomizationFormText = "_ROOM NAME PLACEHOLDER_";
			this.groupPicture.ExpandButtonVisible = true;
			this.groupPicture.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlNoImage});
			this.groupPicture.Location = new System.Drawing.Point(0, 0);
			this.groupPicture.Name = "groupPicture";
			this.groupPicture.Size = new System.Drawing.Size(363, 262);
			this.groupPicture.Text = "WEST PALM BEACH, Philips Point";
			// 
			// layoutControlItem1
			// 
			this.layoutControlItem1.AppearanceItemCaption.BorderColor = System.Drawing.Color.Black;
			this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
			this.layoutControlItem1.AppearanceItemCaption.ForeColor = System.Drawing.Color.Green;
			this.layoutControlItem1.AppearanceItemCaption.Options.UseBorderColor = true;
			this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
			this.layoutControlItem1.AppearanceItemCaption.Options.UseForeColor = true;
			this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
			this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.layoutControlItem1.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
			this.layoutControlItem1.Control = this.pictureEdit1;
			this.layoutControlItem1.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
			this.layoutControlItem1.CustomizationFormText = "layoutImageControlItem";
			this.layoutControlItem1.Location = new System.Drawing.Point(107, 0);
			this.layoutControlItem1.MinSize = new System.Drawing.Size(24, 24);
			this.layoutControlItem1.Name = "layoutControlItem1";
			this.layoutControlItem1.Size = new System.Drawing.Size(232, 216);
			this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem1.Text = "layoutControlItem1";
			this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Top;
			this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem1.TextToControlDistance = 0;
			this.layoutControlItem1.TextVisible = false;
			// 
			// layoutControlNoImage
			// 
			this.layoutControlNoImage.Control = this.labelNoImage;
			this.layoutControlNoImage.CustomizationFormText = "No Image";
			this.layoutControlNoImage.Location = new System.Drawing.Point(0, 0);
			this.layoutControlNoImage.MinSize = new System.Drawing.Size(79, 20);
			this.layoutControlNoImage.Name = "layoutControlNoImage";
			this.layoutControlNoImage.Size = new System.Drawing.Size(107, 216);
			this.layoutControlNoImage.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlNoImage.Text = "No Image";
			this.layoutControlNoImage.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlNoImage.TextToControlDistance = 0;
			this.layoutControlNoImage.TextVisible = false;
			// 
			// groupSiteDescription
			// 
			this.groupSiteDescription.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
			this.groupSiteDescription.AppearanceGroup.Options.UseFont = true;
			this.groupSiteDescription.AppearanceGroup.Options.UseTextOptions = true;
			this.groupSiteDescription.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.groupSiteDescription.CustomizationFormText = "SITE DESCRIPTION:";
			this.groupSiteDescription.ExpandButtonVisible = true;
			this.groupSiteDescription.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup15,
            this.layoutControlGroup18});
			this.groupSiteDescription.Location = new System.Drawing.Point(363, 397);
			this.groupSiteDescription.Name = "groupSiteDescription";
			this.groupSiteDescription.Size = new System.Drawing.Size(624, 207);
			this.groupSiteDescription.Text = "SITE DESCRIPTION:";
			// 
			// layoutControlGroup15
			// 
			this.layoutControlGroup15.CustomizationFormText = "layoutControlGroup15";
			this.layoutControlGroup15.GroupBordersVisible = false;
			this.layoutControlGroup15.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem38,
            this.layoutControlItem37,
            this.layoutControlItem34,
            this.layoutControlItem17,
            this.layoutControlItem12});
			this.layoutControlGroup15.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup15.Name = "layoutControlGroup15";
			this.layoutControlGroup15.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup15.Size = new System.Drawing.Size(150, 161);
			this.layoutControlGroup15.Text = "layoutControlGroup15";
			this.layoutControlGroup15.TextVisible = false;
			// 
			// layoutControlItem38
			// 
			this.layoutControlItem38.Control = this.labelNotes;
			this.layoutControlItem38.CustomizationFormText = "layoutControlItem38";
			this.layoutControlItem38.Location = new System.Drawing.Point(0, 105);
			this.layoutControlItem38.MinSize = new System.Drawing.Size(86, 20);
			this.layoutControlItem38.Name = "layoutControlItem38";
			this.layoutControlItem38.Size = new System.Drawing.Size(150, 56);
			this.layoutControlItem38.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem38.Text = "layoutControlItem38";
			this.layoutControlItem38.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem38.TextToControlDistance = 0;
			this.layoutControlItem38.TextVisible = false;
			// 
			// layoutControlItem37
			// 
			this.layoutControlItem37.Control = this.labelDescription;
			this.layoutControlItem37.CustomizationFormText = "layoutControlItem37";
			this.layoutControlItem37.Location = new System.Drawing.Point(0, 79);
			this.layoutControlItem37.MinSize = new System.Drawing.Size(86, 20);
			this.layoutControlItem37.Name = "layoutControlItem37";
			this.layoutControlItem37.Size = new System.Drawing.Size(150, 26);
			this.layoutControlItem37.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem37.Text = "layoutControlItem37";
			this.layoutControlItem37.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem37.TextToControlDistance = 0;
			this.layoutControlItem37.TextVisible = false;
			// 
			// layoutControlItem34
			// 
			this.layoutControlItem34.Control = this.labelInternetAvailable;
			this.layoutControlItem34.CustomizationFormText = "layoutControlItem34";
			this.layoutControlItem34.Location = new System.Drawing.Point(0, 52);
			this.layoutControlItem34.MinSize = new System.Drawing.Size(109, 20);
			this.layoutControlItem34.Name = "layoutControlItem34";
			this.layoutControlItem34.Size = new System.Drawing.Size(150, 27);
			this.layoutControlItem34.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem34.Text = "layoutControlItem34";
			this.layoutControlItem34.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem34.TextToControlDistance = 0;
			this.layoutControlItem34.TextVisible = false;
			// 
			// layoutControlItem17
			// 
			this.layoutControlItem17.Control = this.labelOpenHours;
			this.layoutControlItem17.CustomizationFormText = "layoutControlItem17";
			this.layoutControlItem17.Location = new System.Drawing.Point(0, 26);
			this.layoutControlItem17.MinSize = new System.Drawing.Size(76, 20);
			this.layoutControlItem17.Name = "layoutControlItem17";
			this.layoutControlItem17.Size = new System.Drawing.Size(150, 26);
			this.layoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem17.Text = "layoutControlItem17";
			this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem17.TextToControlDistance = 0;
			this.layoutControlItem17.TextVisible = false;
			// 
			// layoutControlItem12
			// 
			this.layoutControlItem12.Control = this.labelRoomCapacity;
			this.layoutControlItem12.CustomizationFormText = "layoutControlItem12";
			this.layoutControlItem12.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem12.MinSize = new System.Drawing.Size(94, 20);
			this.layoutControlItem12.Name = "layoutControlItem12";
			this.layoutControlItem12.Size = new System.Drawing.Size(150, 26);
			this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem12.Text = " ";
			this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem12.TextToControlDistance = 0;
			this.layoutControlItem12.TextVisible = false;
			// 
			// layoutControlGroup18
			// 
			this.layoutControlGroup18.CustomizationFormText = "layoutControlGroup18";
			this.layoutControlGroup18.GroupBordersVisible = false;
			this.layoutControlGroup18.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem16,
            this.layoutControlItem33,
            this.layoutControlItem35,
            this.layoutControlItem28,
            this.layoutControlItem9});
			this.layoutControlGroup18.Location = new System.Drawing.Point(150, 0);
			this.layoutControlGroup18.Name = "layoutControlGroup18";
			this.layoutControlGroup18.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup18.Size = new System.Drawing.Size(450, 161);
			this.layoutControlGroup18.Text = "layoutControlGroup18";
			this.layoutControlGroup18.TextVisible = false;
			// 
			// layoutControlItem16
			// 
			this.layoutControlItem16.Control = this.textRoomCapacity;
			this.layoutControlItem16.CustomizationFormText = "layoutControlItem16";
			this.layoutControlItem16.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem16.MinSize = new System.Drawing.Size(18, 20);
			this.layoutControlItem16.Name = "layoutControlItem16";
			this.layoutControlItem16.Size = new System.Drawing.Size(450, 26);
			this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem16.Text = "layoutControlItem16";
			this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem16.TextToControlDistance = 0;
			this.layoutControlItem16.TextVisible = false;
			// 
			// layoutControlItem33
			// 
			this.layoutControlItem33.Control = this.textOpenHours;
			this.layoutControlItem33.CustomizationFormText = "layoutControlItem33";
			this.layoutControlItem33.Location = new System.Drawing.Point(0, 26);
			this.layoutControlItem33.MinSize = new System.Drawing.Size(109, 20);
			this.layoutControlItem33.Name = "layoutControlItem33";
			this.layoutControlItem33.Size = new System.Drawing.Size(450, 26);
			this.layoutControlItem33.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem33.Text = "layoutControlItem33";
			this.layoutControlItem33.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem33.TextToControlDistance = 0;
			this.layoutControlItem33.TextVisible = false;
			// 
			// layoutControlItem35
			// 
			this.layoutControlItem35.Control = this.textInternetAvailable;
			this.layoutControlItem35.CustomizationFormText = "layoutControlItem35";
			this.layoutControlItem35.Location = new System.Drawing.Point(0, 52);
			this.layoutControlItem35.MinSize = new System.Drawing.Size(128, 20);
			this.layoutControlItem35.Name = "layoutControlItem35";
			this.layoutControlItem35.Size = new System.Drawing.Size(450, 27);
			this.layoutControlItem35.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem35.Text = "layoutControlItem35";
			this.layoutControlItem35.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem35.TextToControlDistance = 0;
			this.layoutControlItem35.TextVisible = false;
			// 
			// layoutControlItem28
			// 
			this.layoutControlItem28.Control = this.textDescription;
			this.layoutControlItem28.CustomizationFormText = "layoutControlItem28";
			this.layoutControlItem28.Location = new System.Drawing.Point(0, 79);
			this.layoutControlItem28.MinSize = new System.Drawing.Size(86, 20);
			this.layoutControlItem28.Name = "layoutControlItem28";
			this.layoutControlItem28.Size = new System.Drawing.Size(450, 26);
			this.layoutControlItem28.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem28.Text = "layoutControlItem28";
			this.layoutControlItem28.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem28.TextToControlDistance = 0;
			this.layoutControlItem28.TextVisible = false;
			// 
			// layoutControlItem9
			// 
			this.layoutControlItem9.Control = this.memoNotes;
			this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
			this.layoutControlItem9.Location = new System.Drawing.Point(0, 105);
			this.layoutControlItem9.MinSize = new System.Drawing.Size(14, 22);
			this.layoutControlItem9.Name = "layoutControlItem9";
			this.layoutControlItem9.Size = new System.Drawing.Size(450, 56);
			this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem9.Text = "layoutControlItem9";
			this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem9.TextToControlDistance = 0;
			this.layoutControlItem9.TextVisible = false;
			// 
			// groupSiteLocation
			// 
			this.groupSiteLocation.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
			this.groupSiteLocation.AppearanceGroup.Options.UseFont = true;
			this.groupSiteLocation.AppearanceGroup.Options.UseTextOptions = true;
			this.groupSiteLocation.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.groupSiteLocation.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
			this.groupSiteLocation.AppearanceItemCaption.Options.UseFont = true;
			this.groupSiteLocation.AppearanceItemCaption.Options.UseTextOptions = true;
			this.groupSiteLocation.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.groupSiteLocation.CustomizationFormText = "SITE LOCATION AND CONTACT DETAILS:";
			this.groupSiteLocation.ExpandButtonVisible = true;
			this.groupSiteLocation.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3});
			this.groupSiteLocation.Location = new System.Drawing.Point(363, 0);
			this.groupSiteLocation.Name = "groupSiteLocation";
			this.groupSiteLocation.Size = new System.Drawing.Size(624, 130);
			this.groupSiteLocation.Text = "SITE LOCATION AND CONTACT DETAILS:";
			// 
			// layoutControlGroup2
			// 
			this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
			this.layoutControlGroup2.GroupBordersVisible = false;
			this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem5,
            this.layoutControlItem7});
			this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup2.Name = "layoutControlGroup2";
			this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup2.Size = new System.Drawing.Size(183, 84);
			this.layoutControlGroup2.Text = "layoutControlGroup2";
			this.layoutControlGroup2.TextVisible = false;
			// 
			// layoutControlItem3
			// 
			this.layoutControlItem3.Control = this.labelCountrySateSuburb;
			this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
			this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem3.MinSize = new System.Drawing.Size(134, 20);
			this.layoutControlItem3.Name = "layoutControlItem3";
			this.layoutControlItem3.Size = new System.Drawing.Size(183, 28);
			this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem3.Text = "layoutControlItem3";
			this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem3.TextToControlDistance = 0;
			this.layoutControlItem3.TextVisible = false;
			// 
			// layoutControlItem5
			// 
			this.layoutControlItem5.Control = this.labelLocationName;
			this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
			this.layoutControlItem5.Location = new System.Drawing.Point(0, 28);
			this.layoutControlItem5.MinSize = new System.Drawing.Size(93, 20);
			this.layoutControlItem5.Name = "layoutControlItem5";
			this.layoutControlItem5.Size = new System.Drawing.Size(183, 28);
			this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem5.Text = "layoutControlItem5";
			this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem5.TextToControlDistance = 0;
			this.layoutControlItem5.TextVisible = false;
			// 
			// layoutControlItem7
			// 
			this.layoutControlItem7.Control = this.labelPhoneNumber;
			this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
			this.layoutControlItem7.Location = new System.Drawing.Point(0, 56);
			this.layoutControlItem7.MinSize = new System.Drawing.Size(93, 20);
			this.layoutControlItem7.Name = "layoutControlItem7";
			this.layoutControlItem7.Size = new System.Drawing.Size(183, 28);
			this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem7.Text = "layoutControlItem7";
			this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem7.TextToControlDistance = 0;
			this.layoutControlItem7.TextVisible = false;
			// 
			// layoutControlGroup3
			// 
			this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
			this.layoutControlGroup3.GroupBordersVisible = false;
			this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem6,
            this.layoutControlItem8});
			this.layoutControlGroup3.Location = new System.Drawing.Point(183, 0);
			this.layoutControlGroup3.Name = "layoutControlGroup3";
			this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup3.Size = new System.Drawing.Size(417, 84);
			this.layoutControlGroup3.Text = "layoutControlGroup3";
			this.layoutControlGroup3.TextVisible = false;
			// 
			// layoutControlItem4
			// 
			this.layoutControlItem4.Control = this.textCountrySateSuburb;
			this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
			this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem4.MinSize = new System.Drawing.Size(192, 20);
			this.layoutControlItem4.Name = "layoutControlItem4";
			this.layoutControlItem4.Size = new System.Drawing.Size(417, 28);
			this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem4.Text = "layoutControlItem4";
			this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem4.TextToControlDistance = 0;
			this.layoutControlItem4.TextVisible = false;
			// 
			// layoutControlItem6
			// 
			this.layoutControlItem6.Control = this.textLocationName;
			this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
			this.layoutControlItem6.Location = new System.Drawing.Point(0, 28);
			this.layoutControlItem6.MinSize = new System.Drawing.Size(192, 20);
			this.layoutControlItem6.Name = "layoutControlItem6";
			this.layoutControlItem6.Size = new System.Drawing.Size(417, 28);
			this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem6.Text = "layoutControlItem6";
			this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem6.TextToControlDistance = 0;
			this.layoutControlItem6.TextVisible = false;
			// 
			// layoutControlItem8
			// 
			this.layoutControlItem8.Control = this.textPhoneNumber;
			this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
			this.layoutControlItem8.Location = new System.Drawing.Point(0, 56);
			this.layoutControlItem8.MinSize = new System.Drawing.Size(129, 20);
			this.layoutControlItem8.Name = "layoutControlItem8";
			this.layoutControlItem8.Size = new System.Drawing.Size(417, 28);
			this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem8.Text = "layoutControlItem8";
			this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem8.TextToControlDistance = 0;
			this.layoutControlItem8.TextVisible = false;
			// 
			// groupHoursAndPrice
			// 
			this.groupHoursAndPrice.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
			this.groupHoursAndPrice.AppearanceGroup.Options.UseFont = true;
			this.groupHoursAndPrice.AppearanceGroup.Options.UseTextOptions = true;
			this.groupHoursAndPrice.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.groupHoursAndPrice.CustomizationFormText = "HOURS OF OPERATION AND PER HOUR PRICING, EURO:";
			this.groupHoursAndPrice.ExpandButtonVisible = true;
			this.groupHoursAndPrice.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutEarlyHoursGroup,
            this.layoutOfficeHoursGroup,
            this.layoutAfterHoursGroup,
            this.layoutLateHoursGroup});
			this.groupHoursAndPrice.Location = new System.Drawing.Point(363, 130);
			this.groupHoursAndPrice.Name = "groupHoursAndPrice";
			this.groupHoursAndPrice.Size = new System.Drawing.Size(624, 137);
			this.groupHoursAndPrice.Text = "HOURS OF OPERATION AND PER HOUR PRICING, EURO:";
			// 
			// layoutEarlyHoursGroup
			// 
			this.layoutEarlyHoursGroup.CustomizationFormText = "layoutControlGroup8";
			this.layoutEarlyHoursGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem30,
            this.layoutControlItem11,
            this.layoutControlItem22});
			this.layoutEarlyHoursGroup.Location = new System.Drawing.Point(0, 0);
			this.layoutEarlyHoursGroup.Name = "layoutEarlyHoursGroup";
			this.layoutEarlyHoursGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutEarlyHoursGroup.Size = new System.Drawing.Size(158, 91);
			this.layoutEarlyHoursGroup.Text = "layoutEarlyHoursGroup";
			this.layoutEarlyHoursGroup.TextVisible = false;
			// 
			// layoutControlItem30
			// 
			this.layoutControlItem30.Control = this.textEarlyHrsPrice;
			this.layoutControlItem30.CustomizationFormText = "layoutControlItem30";
			this.layoutControlItem30.Location = new System.Drawing.Point(0, 56);
			this.layoutControlItem30.MinSize = new System.Drawing.Size(43, 20);
			this.layoutControlItem30.Name = "layoutControlItem30";
			this.layoutControlItem30.Size = new System.Drawing.Size(152, 29);
			this.layoutControlItem30.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem30.Text = "layoutControlItem30";
			this.layoutControlItem30.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem30.TextToControlDistance = 0;
			this.layoutControlItem30.TextVisible = false;
			// 
			// layoutControlItem11
			// 
			this.layoutControlItem11.Control = this.textEarlyHrsTime;
			this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
			this.layoutControlItem11.Location = new System.Drawing.Point(0, 28);
			this.layoutControlItem11.MinSize = new System.Drawing.Size(97, 20);
			this.layoutControlItem11.Name = "layoutControlItem11";
			this.layoutControlItem11.Size = new System.Drawing.Size(152, 28);
			this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem11.Text = "layoutControlItem11";
			this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem11.TextToControlDistance = 0;
			this.layoutControlItem11.TextVisible = false;
			// 
			// layoutControlItem22
			// 
			this.layoutControlItem22.Control = this.labelEarlyHrs;
			this.layoutControlItem22.CustomizationFormText = "layoutControlItem22";
			this.layoutControlItem22.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem22.MinSize = new System.Drawing.Size(63, 20);
			this.layoutControlItem22.Name = "layoutControlItem22";
			this.layoutControlItem22.Size = new System.Drawing.Size(152, 28);
			this.layoutControlItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem22.Text = "layoutControlItem22";
			this.layoutControlItem22.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem22.TextToControlDistance = 0;
			this.layoutControlItem22.TextVisible = false;
			// 
			// layoutOfficeHoursGroup
			// 
			this.layoutOfficeHoursGroup.CustomizationFormText = "layoutControlGroup9";
			this.layoutOfficeHoursGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem31,
            this.layoutControlItem25,
            this.layoutControlItem23});
			this.layoutOfficeHoursGroup.Location = new System.Drawing.Point(158, 0);
			this.layoutOfficeHoursGroup.Name = "layoutOfficeHoursGroup";
			this.layoutOfficeHoursGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutOfficeHoursGroup.Size = new System.Drawing.Size(151, 91);
			this.layoutOfficeHoursGroup.Text = "layoutOfficeHoursGroup";
			this.layoutOfficeHoursGroup.TextVisible = false;
			// 
			// layoutControlItem31
			// 
			this.layoutControlItem31.AppearanceItemCaption.BorderColor = System.Drawing.Color.Transparent;
			this.layoutControlItem31.AppearanceItemCaption.Options.UseBorderColor = true;
			this.layoutControlItem31.Control = this.textOfficeHrsPrice;
			this.layoutControlItem31.CustomizationFormText = "layoutControlItem31";
			this.layoutControlItem31.Location = new System.Drawing.Point(0, 56);
			this.layoutControlItem31.MinSize = new System.Drawing.Size(43, 20);
			this.layoutControlItem31.Name = "layoutControlItem31";
			this.layoutControlItem31.Size = new System.Drawing.Size(145, 29);
			this.layoutControlItem31.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem31.Text = "layoutControlItem31";
			this.layoutControlItem31.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem31.TextToControlDistance = 0;
			this.layoutControlItem31.TextVisible = false;
			// 
			// layoutControlItem25
			// 
			this.layoutControlItem25.Control = this.textOfficeHrsTime;
			this.layoutControlItem25.CustomizationFormText = "layoutControlItem25";
			this.layoutControlItem25.Location = new System.Drawing.Point(0, 28);
			this.layoutControlItem25.MinSize = new System.Drawing.Size(96, 20);
			this.layoutControlItem25.Name = "layoutControlItem25";
			this.layoutControlItem25.Size = new System.Drawing.Size(145, 28);
			this.layoutControlItem25.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem25.Text = "layoutControlItem25";
			this.layoutControlItem25.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem25.TextToControlDistance = 0;
			this.layoutControlItem25.TextVisible = false;
			// 
			// layoutControlItem23
			// 
			this.layoutControlItem23.Control = this.labelOfficeHrs;
			this.layoutControlItem23.CustomizationFormText = "layoutControlItem23";
			this.layoutControlItem23.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem23.MinSize = new System.Drawing.Size(60, 20);
			this.layoutControlItem23.Name = "layoutControlItem23";
			this.layoutControlItem23.Size = new System.Drawing.Size(145, 28);
			this.layoutControlItem23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem23.Text = "layoutControlItem23";
			this.layoutControlItem23.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem23.TextToControlDistance = 0;
			this.layoutControlItem23.TextVisible = false;
			// 
			// layoutAfterHoursGroup
			// 
			this.layoutAfterHoursGroup.CustomizationFormText = "layoutControlGroup10";
			this.layoutAfterHoursGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem32,
            this.layoutControlItem27,
            this.layoutControlItem24});
			this.layoutAfterHoursGroup.Location = new System.Drawing.Point(309, 0);
			this.layoutAfterHoursGroup.Name = "layoutAfterHoursGroup";
			this.layoutAfterHoursGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutAfterHoursGroup.Size = new System.Drawing.Size(148, 91);
			this.layoutAfterHoursGroup.Text = "layoutAfterHoursGroup";
			this.layoutAfterHoursGroup.TextVisible = false;
			// 
			// layoutControlItem32
			// 
			this.layoutControlItem32.Control = this.textAfterHrsPrice;
			this.layoutControlItem32.CustomizationFormText = "layoutControlItem32";
			this.layoutControlItem32.Location = new System.Drawing.Point(0, 56);
			this.layoutControlItem32.MinSize = new System.Drawing.Size(43, 20);
			this.layoutControlItem32.Name = "layoutControlItem32";
			this.layoutControlItem32.Size = new System.Drawing.Size(142, 29);
			this.layoutControlItem32.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem32.Text = "layoutControlItem32";
			this.layoutControlItem32.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem32.TextToControlDistance = 0;
			this.layoutControlItem32.TextVisible = false;
			// 
			// layoutControlItem27
			// 
			this.layoutControlItem27.Control = this.textAfterHrsTime;
			this.layoutControlItem27.CustomizationFormText = "layoutControlItem27";
			this.layoutControlItem27.Location = new System.Drawing.Point(0, 28);
			this.layoutControlItem27.MinSize = new System.Drawing.Size(99, 20);
			this.layoutControlItem27.Name = "layoutControlItem27";
			this.layoutControlItem27.Size = new System.Drawing.Size(142, 28);
			this.layoutControlItem27.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem27.Text = "layoutControlItem27";
			this.layoutControlItem27.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem27.TextToControlDistance = 0;
			this.layoutControlItem27.TextVisible = false;
			// 
			// layoutControlItem24
			// 
			this.layoutControlItem24.Control = this.labelAfterHrs;
			this.layoutControlItem24.CustomizationFormText = "layoutControlItem24";
			this.layoutControlItem24.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem24.MinSize = new System.Drawing.Size(55, 20);
			this.layoutControlItem24.Name = "layoutControlItem24";
			this.layoutControlItem24.Size = new System.Drawing.Size(142, 28);
			this.layoutControlItem24.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem24.Text = "layoutControlItem24";
			this.layoutControlItem24.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem24.TextToControlDistance = 0;
			this.layoutControlItem24.TextVisible = false;
			// 
			// layoutLateHoursGroup
			// 
			this.layoutLateHoursGroup.CustomizationFormText = "layoutControlGroup11";
			this.layoutLateHoursGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem26,
            this.layoutControlItem29});
			this.layoutLateHoursGroup.Location = new System.Drawing.Point(457, 0);
			this.layoutLateHoursGroup.Name = "layoutLateHoursGroup";
			this.layoutLateHoursGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutLateHoursGroup.Size = new System.Drawing.Size(143, 91);
			this.layoutLateHoursGroup.Text = "layoutLateHoursGroup";
			this.layoutLateHoursGroup.TextVisible = false;
			// 
			// layoutControlItem2
			// 
			this.layoutControlItem2.Control = this.labelLateHrs;
			this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
			this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem2.MinSize = new System.Drawing.Size(51, 20);
			this.layoutControlItem2.Name = "layoutControlItem2";
			this.layoutControlItem2.Size = new System.Drawing.Size(137, 28);
			this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem2.Text = "layoutControlItem2";
			this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem2.TextToControlDistance = 0;
			this.layoutControlItem2.TextVisible = false;
			// 
			// layoutControlItem26
			// 
			this.layoutControlItem26.Control = this.textLateHrsTime;
			this.layoutControlItem26.CustomizationFormText = "layoutControlItem26";
			this.layoutControlItem26.Location = new System.Drawing.Point(0, 28);
			this.layoutControlItem26.MinSize = new System.Drawing.Size(96, 20);
			this.layoutControlItem26.Name = "layoutControlItem26";
			this.layoutControlItem26.Size = new System.Drawing.Size(137, 28);
			this.layoutControlItem26.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem26.Text = "layoutControlItem26";
			this.layoutControlItem26.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem26.TextToControlDistance = 0;
			this.layoutControlItem26.TextVisible = false;
			// 
			// layoutControlItem29
			// 
			this.layoutControlItem29.Control = this.textLateHrsPrice;
			this.layoutControlItem29.CustomizationFormText = "layoutControlItem29";
			this.layoutControlItem29.Location = new System.Drawing.Point(0, 56);
			this.layoutControlItem29.MinSize = new System.Drawing.Size(43, 20);
			this.layoutControlItem29.Name = "layoutControlItem29";
			this.layoutControlItem29.Size = new System.Drawing.Size(137, 29);
			this.layoutControlItem29.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem29.Text = "layoutControlItem29";
			this.layoutControlItem29.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem29.TextToControlDistance = 0;
			this.layoutControlItem29.TextVisible = false;
			// 
			// groupTechnicalDetails
			// 
			this.groupTechnicalDetails.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
			this.groupTechnicalDetails.AppearanceGroup.Options.UseFont = true;
			this.groupTechnicalDetails.AppearanceGroup.Options.UseTextOptions = true;
			this.groupTechnicalDetails.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.groupTechnicalDetails.CustomizationFormText = "TECHNICAL DETAILS:";
			this.groupTechnicalDetails.ExpandButtonVisible = true;
			this.groupTechnicalDetails.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup14,
            this.layoutControlGroup17});
			this.groupTechnicalDetails.Location = new System.Drawing.Point(363, 267);
			this.groupTechnicalDetails.Name = "groupTechnicalDetails";
			this.groupTechnicalDetails.Size = new System.Drawing.Size(624, 130);
			this.groupTechnicalDetails.Text = "TECHNICAL DETAILS:";
			// 
			// layoutControlGroup14
			// 
			this.layoutControlGroup14.CustomizationFormText = "layoutControlGroup14";
			this.layoutControlGroup14.GroupBordersVisible = false;
			this.layoutControlGroup14.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.layoutControlItem13});
			this.layoutControlGroup14.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup14.Name = "layoutControlGroup14";
			this.layoutControlGroup14.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup14.Size = new System.Drawing.Size(150, 84);
			this.layoutControlGroup14.Text = "layoutControlGroup14";
			this.layoutControlGroup14.TextVisible = false;
			// 
			// layoutControlItem18
			// 
			this.layoutControlItem18.Control = this.labelSDHD;
			this.layoutControlItem18.CustomizationFormText = "layoutControlItem18";
			this.layoutControlItem18.Location = new System.Drawing.Point(0, 56);
			this.layoutControlItem18.MinSize = new System.Drawing.Size(104, 20);
			this.layoutControlItem18.Name = "layoutControlItem18";
			this.layoutControlItem18.Size = new System.Drawing.Size(150, 28);
			this.layoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem18.Text = "layoutControlItem18";
			this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem18.TextToControlDistance = 0;
			this.layoutControlItem18.TextVisible = false;
			// 
			// layoutControlItem19
			// 
			this.layoutControlItem19.Control = this.labelEquipmentType;
			this.layoutControlItem19.CustomizationFormText = "layoutControlItem19";
			this.layoutControlItem19.Location = new System.Drawing.Point(0, 28);
			this.layoutControlItem19.MinSize = new System.Drawing.Size(101, 20);
			this.layoutControlItem19.Name = "layoutControlItem19";
			this.layoutControlItem19.Size = new System.Drawing.Size(150, 28);
			this.layoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem19.Text = "layoutControlItem19";
			this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem19.TextToControlDistance = 0;
			this.layoutControlItem19.TextVisible = false;
			// 
			// layoutControlItem13
			// 
			this.layoutControlItem13.Control = this.labelISDN;
			this.layoutControlItem13.CustomizationFormText = "layoutControlItem13";
			this.layoutControlItem13.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem13.MinSize = new System.Drawing.Size(111, 20);
			this.layoutControlItem13.Name = "layoutControlItem13";
			this.layoutControlItem13.Size = new System.Drawing.Size(150, 28);
			this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem13.Text = "layoutControlItem13";
			this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem13.TextToControlDistance = 0;
			this.layoutControlItem13.TextVisible = false;
			// 
			// layoutControlGroup17
			// 
			this.layoutControlGroup17.CustomizationFormText = "layoutControlGroup17";
			this.layoutControlGroup17.GroupBordersVisible = false;
			this.layoutControlGroup17.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem15});
			this.layoutControlGroup17.Location = new System.Drawing.Point(150, 0);
			this.layoutControlGroup17.Name = "layoutControlGroup17";
			this.layoutControlGroup17.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup17.Size = new System.Drawing.Size(450, 84);
			this.layoutControlGroup17.Text = "layoutControlGroup17";
			this.layoutControlGroup17.TextVisible = false;
			// 
			// layoutControlItem20
			// 
			this.layoutControlItem20.Control = this.textISDN;
			this.layoutControlItem20.CustomizationFormText = "layoutControlItem20";
			this.layoutControlItem20.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem20.MinSize = new System.Drawing.Size(178, 20);
			this.layoutControlItem20.Name = "layoutControlItem20";
			this.layoutControlItem20.Size = new System.Drawing.Size(450, 28);
			this.layoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem20.Text = "layout";
			this.layoutControlItem20.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem20.TextToControlDistance = 0;
			this.layoutControlItem20.TextVisible = false;
			// 
			// layoutControlItem21
			// 
			this.layoutControlItem21.Control = this.textEquipmentType;
			this.layoutControlItem21.CustomizationFormText = "layoutControlItem21";
			this.layoutControlItem21.Location = new System.Drawing.Point(0, 28);
			this.layoutControlItem21.MinSize = new System.Drawing.Size(121, 20);
			this.layoutControlItem21.Name = "layoutControlItem21";
			this.layoutControlItem21.Size = new System.Drawing.Size(450, 28);
			this.layoutControlItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem21.Text = "layoutControlItem21";
			this.layoutControlItem21.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem21.TextToControlDistance = 0;
			this.layoutControlItem21.TextVisible = false;
			// 
			// layoutControlItem15
			// 
			this.layoutControlItem15.Control = this.textSDHD;
			this.layoutControlItem15.CustomizationFormText = "layoutControlItem15";
			this.layoutControlItem15.Location = new System.Drawing.Point(0, 56);
			this.layoutControlItem15.MinSize = new System.Drawing.Size(155, 20);
			this.layoutControlItem15.Name = "layoutControlItem15";
			this.layoutControlItem15.Size = new System.Drawing.Size(450, 28);
			this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem15.Text = "layoutControlItem15";
			this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem15.TextToControlDistance = 0;
			this.layoutControlItem15.TextVisible = false;
			// 
			// layoutControlGroup13
			// 
			this.layoutControlGroup13.CustomizationFormText = "layoutControlGroup13";
			this.layoutControlGroup13.Location = new System.Drawing.Point(134, 89);
			this.layoutControlGroup13.Name = "layoutControlGroup13";
			this.layoutControlGroup13.OptionsItemText.TextToControlDistance = 5;
			this.layoutControlGroup13.Size = new System.Drawing.Size(92, 26);
			this.layoutControlGroup13.Text = "layoutControlGroup13";
			this.layoutControlGroup13.TextVisible = false;
			// 
			// RoomInfo
			// 
			this.ApplyEnabled = true;
			this.ApplyVisible = true;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
			this.ClientSize = new System.Drawing.Size(1011, 660);
			this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.Name = "RoomInfo";
			this.Text = "Public Room Information";
			this.Load += new System.EventHandler(this.RoomInfo_Load);
			this.Shown += new System.EventHandler(this.RoomInfo_Shown);
			((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).EndInit();
			this.ContentPanel.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
			this.layoutControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.memoNotes.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.groupMap)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.groupPicture)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlNoImage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.groupSiteDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.groupSiteLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.groupHoursAndPrice)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutEarlyHoursGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutOfficeHoursGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutAfterHoursGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutLateHoursGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.groupTechnicalDetails)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).EndInit();
			this.ResumeLayout(false);

		}

		

		

		
		

		#endregion

		private DevExpress.XtraLayout.LayoutControl layoutControl1;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private DevExpress.XtraEditors.PictureEdit pictureEdit1;
		private DevExpress.XtraEditors.LabelControl labelCountrySateSuburb;
		private DevExpress.XtraEditors.LabelControl labelLocationName;
		private DevExpress.XtraEditors.LabelControl labelPhoneNumber;
		private System.Windows.Forms.WebBrowser webBrowser1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
		private DevExpress.XtraLayout.LayoutControlGroup groupMap;
		private DevExpress.XtraLayout.LayoutControlGroup groupPicture;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
		private DevExpress.XtraLayout.LayoutControlGroup groupSiteLocation;
		private DevExpress.XtraEditors.LabelControl labelLateHrs;
		private DevExpress.XtraLayout.LayoutControlGroup groupHoursAndPrice;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
		private DevExpress.XtraEditors.LabelControl labelISDN;
		private DevExpress.XtraEditors.LabelControl labelRoomCapacity;
		private DevExpress.XtraEditors.LabelControl textEarlyHrsTime;
		private DevExpress.XtraLayout.LayoutControlGroup groupSiteDescription;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
		private DevExpress.XtraLayout.LayoutControlGroup groupTechnicalDetails;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
		private DevExpress.XtraEditors.LabelControl labelEquipmentType;
		private DevExpress.XtraEditors.LabelControl labelSDHD;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
		private DevExpress.XtraEditors.LabelControl textEquipmentType;
		private DevExpress.XtraEditors.LabelControl textISDN;
		private DevExpress.XtraEditors.LabelControl textPhoneNumber;
		private DevExpress.XtraEditors.LabelControl textLocationName;
		private DevExpress.XtraEditors.LabelControl textCountrySateSuburb;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup13;
		private DevExpress.XtraEditors.LabelControl labelAfterHrs;
		private DevExpress.XtraEditors.LabelControl labelOfficeHrs;
		private DevExpress.XtraEditors.LabelControl labelEarlyHrs;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
		private DevExpress.XtraEditors.LabelControl textAfterHrsTime;
		private DevExpress.XtraEditors.LabelControl textLateHrsTime;
		private DevExpress.XtraEditors.LabelControl textOfficeHrsTime;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
		private DevExpress.XtraEditors.LabelControl textAfterHrsPrice;
		private DevExpress.XtraEditors.LabelControl textOfficeHrsPrice;
		private DevExpress.XtraEditors.LabelControl textEarlyHrsPrice;
		private DevExpress.XtraEditors.LabelControl textLateHrsPrice;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
		private DevExpress.XtraEditors.LabelControl textSDHD;
		private DevExpress.XtraEditors.LabelControl textInternetAvailable;
		private DevExpress.XtraEditors.LabelControl labelInternetAvailable;
		private DevExpress.XtraEditors.LabelControl textOpenHours;
		private DevExpress.XtraEditors.LabelControl labelOpenHours;
		private DevExpress.XtraEditors.LabelControl textRoomCapacity;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
		private DevExpress.XtraEditors.LabelControl labelNotes;
		private DevExpress.XtraEditors.LabelControl labelDescription;
		private DevExpress.XtraEditors.LabelControl textDescription;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
		private DevExpress.XtraLayout.LayoutControlGroup layoutEarlyHoursGroup;
		private DevExpress.XtraLayout.LayoutControlGroup layoutOfficeHoursGroup;
		private DevExpress.XtraLayout.LayoutControlGroup layoutAfterHoursGroup;
		private DevExpress.XtraLayout.LayoutControlGroup layoutLateHoursGroup;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup15;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup14;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup18;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup17;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
		private DevExpress.XtraEditors.LabelControl labelNoImage;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlNoImage;
		private DevExpress.XtraEditors.LabelControl labelNoMap;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
		private DevExpress.XtraEditors.MemoEdit memoNotes;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;

	}
}
