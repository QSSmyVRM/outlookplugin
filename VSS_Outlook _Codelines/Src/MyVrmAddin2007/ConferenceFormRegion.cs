﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System.Drawing;
    using System.Globalization;
    using System.Threading;
    using Microsoft.Office.Interop.Outlook;
    using MyVrm.Outlook;
    using MyVrm.Outlook.WinForms;
    using Exception = System.Exception;

    namespace MyVrmAddin2007
    {
        partial class ConferenceFormRegion
        {
            #region Form Region Factory

            [Microsoft.Office.Tools.Outlook.FormRegionMessageClass(Microsoft.Office.Tools.Outlook.FormRegionMessageClassAttribute.Appointment)]
            [Microsoft.Office.Tools.Outlook.FormRegionName("MyVrmAddin2007.ConferenceFormRegion")]
            public partial class ConferenceFormRegionFactory
            {
                // Occurs before the form region is initialized.
                // To prevent the form region from appearing, set e.Cancel to true.
                // Use e.OutlookItem to get a reference to the current Outlook item.
                private void ConferenceFormRegionFactory_FormRegionInitializing(object sender, Microsoft.Office.Tools.Outlook.FormRegionInitializingEventArgs e)
                {
                    e.Cancel = !MyVrmAddin.Instance.IsLicenseAgreementAccepted;
                }
            }

            #endregion

            // Occurs before the form region is displayed.
            // Use this.OutlookItem to get a reference to the current Outlook item.
            // Use this.OutlookFormRegion to get a reference to the form region.
            private void ConferenceFormRegion_FormRegionShowing(object sender, System.EventArgs e)
            {
                try
                {
                    /**/
                    MyVrmAddin.TraceSource.TraceInformation("\n ConferenceFormRegion.....");
                    var appointmentItem = OutlookItem as AppointmentItem;
					if (appointmentItem != null)
					{
						var appointmentImpl = Globals.ThisAddIn.Appointments[appointmentItem] ??
						                      new OutlookAppointmentImpl(appointmentItem);
						if (appointmentImpl.ConferenceId == null)// &&
						    //Globals.ThisAddIn.IsItLiveMeetingAppointment(appointmentItem))
						{
							if (appointmentImpl.GlobalAppointmentId != null && 
								Globals.ThisAddIn.ConvertedConfs.ContainsKey(appointmentImpl.GlobalAppointmentId))
							{
								appointmentImpl.ConferenceId = Globals.ThisAddIn.ConvertedConfs[appointmentImpl.GlobalAppointmentId];
							}
						}
					}
                	/**/
                    conferenceControl.Conference =
                        Globals.ThisAddIn.InitConferenceWrapper(OutlookFormRegion.Inspector, appointmentItem);
                }
                catch (Exception exception)
                {
                    MyVrmAddin.TraceSource.TraceException(exception);
                    UIHelper.ShowError(exception.Message);
                }

            }

            // Occurs when the form region is closed.
            // Use this.OutlookItem to get a reference to the current Outlook item.
            // Use this.OutlookFormRegion to get a reference to the form region.
            private void ConferenceFormRegion_FormRegionClosed(object sender, System.EventArgs e)
            {
            }
        }
    }
