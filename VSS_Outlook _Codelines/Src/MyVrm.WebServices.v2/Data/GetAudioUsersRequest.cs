﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;

namespace MyVrm.WebServices.Data
{
    internal class GetAudioUsersRequest : ServiceRequestBase<GetAudioUsersResponse>
    {
        internal string ConferenceId { get; set; }  //ZD 104699 start
        internal bool Immediate { get; set; }
        internal DateTime Start { get; set; }
        internal TimeSpan Duration { get; set; }
        internal TimeZoneInfo TimeZone { get; set; }
        public ConferenceId Id;   //ZD 104699 End

        internal GetAudioUsersRequest(MyVrmService service) : base(service)
        {
            
        }

        internal GetAudioUsersRequest(MyVrmService service, ConferenceId confID)
            : base(service)
        {
            Id = confID;
        }
        #region Overrides of ServiceRequestBase<GetAudioUsersResponse>

        internal override string GetXmlElementName()  
        {
            return "login";
        }

        internal override string GetCommandName()
        {
            return "GetAudioUserList";
        }

        internal override string GetResponseXmlElementName()
        {
            return "users";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            OrganizationId.WriteToXml(writer);
            UserId.WriteToXml(writer);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "confID", ConferenceId); //ZD 104699 start
            writer.WriteElementValue(XmlNamespace.NotSpecified, "immediate", Immediate);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "startDate", Utilities.DateToString(Start.Date));
            writer.WriteElementValue(XmlNamespace.NotSpecified, "startHour", Utilities.HourToString(Start.Hour));
            writer.WriteElementValue(XmlNamespace.NotSpecified, "startMin", Start.Minute);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "startSet", Utilities.GetNoonAbbr(Start.TimeOfDay));
            writer.WriteElementValue(XmlNamespace.NotSpecified, "timeZone", TimeZoneConvertion.ConvertToTimeZoneId(TimeZone));
            writer.WriteElementValue(XmlNamespace.NotSpecified, "durationMin", Duration.TotalMinutes); //ZD 104699 End
            writer.WriteElementValue(XmlNamespace.NotSpecified, "FetchFrom", "Express");
            //<FetchFrom>Express</FetchFrom>";
        }

        #endregion
    }
}
