﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    internal class FindUsersRequest : ServiceRequestBase<FindUsersResponse>
    {
        public FindUsersRequest(MyVrmService service) : base(service)
        {
        }

        #region Overrides of ServiceRequestBase

        internal override string GetXmlElementName()
        {
            return "login";
        }

        internal override string GetCommandName()
        {
            return "GetManageUser";
        }

        internal override string GetResponseXmlElementName()
        {
            return "users";
        }

        public bool AudioUsers { get; set; }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            UserId.WriteToXml(writer);
            OrganizationId.WriteToXml(writer);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "sortBy", "1");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "alphabet", "all");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "pageNo", "1");
            if(AudioUsers == true)
                writer.WriteElementValue(XmlNamespace.NotSpecified, "audioaddon", "1");
            else
                writer.WriteElementValue(XmlNamespace.NotSpecified, "audioaddon", "0");
                

        }

        #endregion
    }
}
