﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace MyVrm.WebServices.Data
{
	public class RoomIdCollectionBase : ComplexPropertyCollection<RoomId>
	{
		private string _itemXmlElementTagName;

		public override object Clone()
		{
			Type type = GetType();
			object ret;
			if (type.IsSubclassOf(typeof(RoomIdCollectionBase))) //for RoomIdCollection and RoomIDsForRecurrenceCollection
			{
				ConstructorInfo ctor = type.GetConstructor(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance,
						null, Type.EmptyTypes, null);
				ret = ctor != null ? ctor.Invoke(null) : null;
			}
			else
			{
				ConstructorInfo ctor = type.GetConstructor( BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance,
						null, new Type[] { typeof(string) }, null);
				ret = ctor != null ? ctor.Invoke(new object[] { _itemXmlElementTagName }) : null;
			}
			RoomIdCollectionBase copy = ret as RoomIdCollectionBase;
			if (copy != null)
			{
				foreach (var item in Items)
				{
					copy.Add((RoomId) item.Clone());
				}
			}
			return copy;
		}

		public RoomIdCollectionBase(string itemXmlElementTagName)
		{
			_itemXmlElementTagName = itemXmlElementTagName;
		}

        public new RoomId this[int index]
        {
            get
            {
                return base[index];
            }
            set
            {
                base[index] = value;
            }
        }

        public void Add(RoomId roomId)
        {
            InternalAdd(roomId);
        }

        public void AddRange(IEnumerable<RoomId> roomIds )
        {
            InternalAddRange(roomIds);
        }

        public void Remove(RoomId roomId)
        {
            InternalRemove(roomId);
        }

        public void RemoveAt(int index)
        {
            InternalRemoveAt(index);
        }

        public void Clear()
        {
            InternalClear();
        }

        public void Insert(int index, RoomId roomId)
        {
			//Items.Insert(index, roomId);
			InternalInsert(index, roomId);
        }

        #region Overrides of ComplexPropertyCollection<RoomId>

        internal override RoomId CreateComplexProperty(string xmlElementName)
        {
            return new RoomId();
        }

        internal override string GetCollectionItemXmlElementName(RoomId complexProperty)
        {
			return _itemXmlElementTagName;
		}
		#endregion
	}
}
