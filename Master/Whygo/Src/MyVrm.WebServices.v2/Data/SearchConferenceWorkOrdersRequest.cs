﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.WebServices.Data
{
    internal class SearchConferenceWorkOrdersRequest : ServiceRequestBase<SearchConferenceWorkOrdersResponse>
    {
        internal SearchConferenceWorkOrdersRequest(MyVrmService service) : base(service)
        {
        }

        internal ConferenceId ConferenceId { get; set; }
        internal WorkOrderType Type { get; set; }
        internal WorkOrderSearchFilter SearchFilter { get; set; }

        #region Overrides of ServiceRequestBase

        internal override string GetXmlElementName()
        {
            return "login";
        }

        internal override string GetCommandName()
        {
            return "SearchConferenceWorkOrders";
        }

        internal override string GetResponseXmlElementName()
        {
            return "WorkOrderList";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            writer.WriteStartElement(XmlNamespace.NotSpecified, "user");
            UserId.WriteToXml(writer);
            writer.WriteEndElement();
            writer.WriteElementValue(XmlNamespace.NotSpecified, "userID", null);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "Filter", SearchFilter);
            ConferenceId.WriteToXml(writer, "ConfID");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "Name", null);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "Rooms", null);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "DateFrom", null);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "DateTo", null);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "TimeFrom", null);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "TimeTo", null);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "Status", null);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "Type", Type);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "MaxRecords", null);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "pageEnable", false);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "pageNo", null);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "SortBy", 3);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "utcEnabled", Service.IsUtcEnabled);
        }

        #endregion
    }
}
