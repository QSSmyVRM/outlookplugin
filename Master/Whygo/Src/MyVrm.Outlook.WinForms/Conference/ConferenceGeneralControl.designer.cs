﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.Outlook.WinForms.Conference
{
    partial class ConferenceGeneralControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
			DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
			DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
			DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
			DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
			DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
			DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
			DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
			DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
			this.layoutControl2 = new DevExpress.XtraDataLayout.DataLayoutControl();
			this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
			this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.conferencePasswordEdit = new DevExpress.XtraEditors.TextEdit();
			this.publicConferenceCheckEdit = new DevExpress.XtraEditors.CheckEdit();
			this.conferenceTypeEdit = new MyVrm.Outlook.WinForms.EnumComboBoxEdit();
			this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
			this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
			this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
			((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
			this.layoutControl2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.conferencePasswordEdit.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.publicConferenceCheckEdit.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.conferenceTypeEdit.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
			this.SuspendLayout();
			// 
			// layoutControl2
			// 
			this.layoutControl2.AllowCustomizationMenu = false;
			this.layoutControl2.Controls.Add(this.layoutControl1);
			this.layoutControl2.Controls.Add(this.conferencePasswordEdit);
			this.layoutControl2.Controls.Add(this.publicConferenceCheckEdit);
			this.layoutControl2.Controls.Add(this.conferenceTypeEdit);
			this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.layoutControl2.Location = new System.Drawing.Point(0, 0);
			this.layoutControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.layoutControl2.Name = "layoutControl2";
			this.layoutControl2.Root = this.layoutControlGroup2;
			this.layoutControl2.Size = new System.Drawing.Size(1136, 62);
			this.layoutControl2.TabIndex = 0;
			this.layoutControl2.Text = "layoutControl2";
			// 
			// layoutControl1
			// 
			this.layoutControl1.Location = new System.Drawing.Point(515, 0);
			this.layoutControl1.Margin = new System.Windows.Forms.Padding(0);
			this.layoutControl1.Name = "layoutControl1";
			this.layoutControl1.Root = this.layoutControlGroup1;
			this.layoutControl1.Size = new System.Drawing.Size(621, 62);
			this.layoutControl1.TabIndex = 13;
			this.layoutControl1.Text = "layoutControl1";
			// 
			// layoutControlGroup1
			// 
			this.layoutControlGroup1.BackgroundImage = global::MyVrm.Outlook.WinForms.Properties.Resources.whygo_tr1;
			this.layoutControlGroup1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.layoutControlGroup1.BackgroundImageVisible = true;
			this.layoutControlGroup1.CaptionImagePadding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
			this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
			this.layoutControlGroup1.GroupBordersVisible = false;
			this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup1.Name = "layoutControlGroup1";
			this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup1.Size = new System.Drawing.Size(621, 62);
			this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup1.Text = "layoutControlGroup1";
			this.layoutControlGroup1.TextVisible = false;
			// 
			// conferencePasswordEdit
			// 
			this.conferencePasswordEdit.Location = new System.Drawing.Point(135, 27);
			this.conferencePasswordEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.conferencePasswordEdit.Name = "conferencePasswordEdit";
			this.conferencePasswordEdit.Properties.PasswordChar = '*';
			this.conferencePasswordEdit.Properties.ValidateOnEnterKey = true;
			this.conferencePasswordEdit.Size = new System.Drawing.Size(175, 22);
			this.conferencePasswordEdit.StyleController = this.layoutControl2;
			toolTipTitleItem1.Text = "Conference Password";
			toolTipItem1.LeftIndent = 6;
			superToolTip1.Items.Add(toolTipTitleItem1);
			superToolTip1.Items.Add(toolTipItem1);
			this.conferencePasswordEdit.SuperTip = superToolTip1;
			this.conferencePasswordEdit.TabIndex = 12;
			// 
			// publicConferenceCheckEdit
			// 
			this.publicConferenceCheckEdit.Location = new System.Drawing.Point(314, 2);
			this.publicConferenceCheckEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.publicConferenceCheckEdit.Name = "publicConferenceCheckEdit";
			this.publicConferenceCheckEdit.Properties.Caption = "Public Conference";
			this.publicConferenceCheckEdit.Size = new System.Drawing.Size(199, 21);
			this.publicConferenceCheckEdit.StyleController = this.layoutControl2;
			toolTipTitleItem2.Text = "Public Conference";
			toolTipItem2.LeftIndent = 6;
			toolTipItem2.Text = "If checked, the conference is listed as a public conference";
			superToolTip2.Items.Add(toolTipTitleItem2);
			superToolTip2.Items.Add(toolTipItem2);
			this.publicConferenceCheckEdit.SuperTip = superToolTip2;
			this.publicConferenceCheckEdit.TabIndex = 11;
			// 
			// conferenceTypeEdit
			// 
			this.conferenceTypeEdit.Location = new System.Drawing.Point(135, 2);
			this.conferenceTypeEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.conferenceTypeEdit.Name = "conferenceTypeEdit";
			this.conferenceTypeEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.conferenceTypeEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
			this.conferenceTypeEdit.Size = new System.Drawing.Size(175, 22);
			this.conferenceTypeEdit.StyleController = this.layoutControl2;
			toolTipTitleItem3.Text = "Conference Type";
			toolTipItem3.LeftIndent = 6;
			toolTipItem3.Text = "Select Audio/Video (default), Audio Only, Point-to-Point, or Room Conference.";
			superToolTip3.Items.Add(toolTipTitleItem3);
			superToolTip3.Items.Add(toolTipItem3);
			this.conferenceTypeEdit.SuperTip = superToolTip3;
			this.conferenceTypeEdit.TabIndex = 10;
			// 
			// layoutControlGroup2
			// 
			this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
			this.layoutControlGroup2.GroupBordersVisible = false;
			this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem6,
            this.layoutControlItem16,
            this.emptySpaceItem1,
            this.layoutControlItem2});
			this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup2.Name = "Root";
			this.layoutControlGroup2.Size = new System.Drawing.Size(1136, 62);
			this.layoutControlGroup2.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup2.Text = "Root";
			this.layoutControlGroup2.TextVisible = false;
			// 
			// layoutControlItem1
			// 
			this.layoutControlItem1.Control = this.conferenceTypeEdit;
			this.layoutControlItem1.CustomizationFormText = "Conference Type:";
			this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem1.MaxSize = new System.Drawing.Size(312, 24);
			this.layoutControlItem1.MinSize = new System.Drawing.Size(312, 24);
			this.layoutControlItem1.Name = "layoutControlItem1";
			this.layoutControlItem1.Size = new System.Drawing.Size(312, 25);
			this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem1.Text = "Conference Type:";
			this.layoutControlItem1.TextSize = new System.Drawing.Size(129, 16);
			// 
			// layoutControlItem6
			// 
			this.layoutControlItem6.Control = this.publicConferenceCheckEdit;
			this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
			this.layoutControlItem6.Location = new System.Drawing.Point(312, 0);
			this.layoutControlItem6.Name = "layoutControlItem6";
			this.layoutControlItem6.Size = new System.Drawing.Size(203, 25);
			this.layoutControlItem6.Text = "layoutControlItem6";
			this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem6.TextToControlDistance = 0;
			this.layoutControlItem6.TextVisible = false;
			// 
			// layoutControlItem16
			// 
			this.layoutControlItem16.Control = this.conferencePasswordEdit;
			this.layoutControlItem16.CustomizationFormText = "Conference Password:";
			this.layoutControlItem16.Location = new System.Drawing.Point(0, 25);
			this.layoutControlItem16.MaxSize = new System.Drawing.Size(312, 26);
			this.layoutControlItem16.MinSize = new System.Drawing.Size(312, 26);
			this.layoutControlItem16.Name = "layoutControlItem16";
			this.layoutControlItem16.Size = new System.Drawing.Size(312, 37);
			this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem16.Text = "Conference Password:";
			this.layoutControlItem16.TextSize = new System.Drawing.Size(129, 16);
			// 
			// emptySpaceItem1
			// 
			this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
			this.emptySpaceItem1.Location = new System.Drawing.Point(312, 25);
			this.emptySpaceItem1.Name = "emptySpaceItem1";
			this.emptySpaceItem1.Size = new System.Drawing.Size(203, 37);
			this.emptySpaceItem1.Text = "emptySpaceItem1";
			this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
			// 
			// layoutControlItem2
			// 
			this.layoutControlItem2.Control = this.layoutControl1;
			this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
			this.layoutControlItem2.Location = new System.Drawing.Point(515, 0);
			this.layoutControlItem2.MinSize = new System.Drawing.Size(1, 1);
			this.layoutControlItem2.Name = "layoutControlItem2";
			this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlItem2.Size = new System.Drawing.Size(621, 62);
			this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem2.Text = "layoutControlItem2";
			this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem2.TextToControlDistance = 0;
			this.layoutControlItem2.TextVisible = false;
			// 
			// emptySpaceItem4
			// 
			this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
			this.emptySpaceItem4.Location = new System.Drawing.Point(0, 182);
			this.emptySpaceItem4.Name = "emptySpaceItem4";
			this.emptySpaceItem4.Size = new System.Drawing.Size(701, 258);
			this.emptySpaceItem4.Text = "emptySpaceItem4";
			this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
			// 
			// ConferenceGeneralControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSize = true;
			this.Controls.Add(this.layoutControl2);
			this.DoubleBuffered = true;
			this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.Name = "ConferenceGeneralControl";
			this.Size = new System.Drawing.Size(1136, 62);
			this.Load += new System.EventHandler(this.ConferenceGeneralControl_Load);
			((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
			this.layoutControl2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.conferencePasswordEdit.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.publicConferenceCheckEdit.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.conferenceTypeEdit.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.TextEdit conferencePasswordEdit;
        private DevExpress.XtraEditors.CheckEdit publicConferenceCheckEdit;
        private EnumComboBoxEdit conferenceTypeEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
		private DevExpress.XtraLayout.LayoutControl layoutControl1;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;

    }
}