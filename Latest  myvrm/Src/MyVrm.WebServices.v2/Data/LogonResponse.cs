﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    internal class LogonResponse : ServiceResponse
    {
        internal UserId UserId { get; private set; }
        internal OrganizationId OrganizationId { get; private set; }
		internal bool IsAVEnabled { get; set; }
//101456 starts
        internal bool enableDesktopVideo { get; set; }
        internal bool enableBlueJeans { get; set; }
        internal bool enableJabber { get; set; }
        internal bool enableLync { get; set; }
        internal bool enableVidtel { get; set; }
        //101456 Ends
        public bool EnableAdditionalOption { get; set; }
        public bool EnableAVWorkOrder { get; set; }
        public bool EnableCateringWO { get; set; }
        public bool EnableFacilityWO { get; set; } //ZD 101838
        public string StartMode { get; set; } //102638
        internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
        {
            do
            {
                reader.Read();
                if (reader.LocalName == "globalInfo")
                {
                    do
                    {
                        reader.Read();
                        if (reader.LocalName == "userID")
                        {
                            UserId = new UserId(reader.ReadValue());
                        }
                        else if (reader.LocalName == "organizationID")
                        {
                            OrganizationId = OrganizationId.Invalid;
                            OrganizationId.LoadFromXml(reader, reader.LocalName);
                        }
						else if (reader.LocalName == "enableAV")
						{
							IsAVEnabled = reader.ReadElementValue<bool>();
						}//101456 starts
                        else if (reader.LocalName == "enableDesktopVideo")
                        {
                            enableDesktopVideo = reader.ReadElementValue<bool>();
                        }
                        else if (reader.LocalName == "enableBlueJeans")
                        {
                            enableBlueJeans = reader.ReadElementValue<bool>();
                        }
                        else if (reader.LocalName == "enableJabber")
                        {
                            enableJabber = reader.ReadElementValue<bool>();
                        }
                        else if (reader.LocalName == "enableLync")
                        {
                            enableLync = reader.ReadElementValue<bool>();
                        }
                        else if (reader.LocalName == "enableVidtel")
                        {
                            enableVidtel = reader.ReadElementValue<bool>();
                        }
                        else if (reader.LocalName == "enableAdditionalOption")
                        {
                            EnableAdditionalOption = reader.ReadElementValue<bool>();
                        }
                        else if (reader.LocalName == "enableAVWorkOrder")
                        {
                            EnableAVWorkOrder = reader.ReadElementValue<bool>();
                        }
                        else if (reader.LocalName == "enableCateringWO")
                        {
                            EnableCateringWO = reader.ReadElementValue<bool>();
                        }
                        else if (reader.LocalName == "enableFacilityWO")
                        {
                            EnableFacilityWO = reader.ReadElementValue<bool>();
                        }
                        else if (reader.LocalName == "StartMode")
                        {
                            StartMode = reader.ReadValue();
                        }
                        else
                        {
                            reader.SkipCurrentElement();
                        }
//101456 ends
                    } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "globalInfo"));
                }
                else
                {
                    reader.SkipCurrentElement();
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "user"));
        }
    }
}
