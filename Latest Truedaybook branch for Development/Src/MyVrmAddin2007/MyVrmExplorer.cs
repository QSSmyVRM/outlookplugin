﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Outlook;
using Microsoft.Win32;
using MyVrm.Outlook;
using MyVrm.Outlook.WinForms;
using MyVrm.Outlook.WinForms.CheckSeats;
using MyVrm.Outlook.WinForms.Conference;
using MyVrm.Outlook.WinForms.Options;
using MyVrm.WebServices.Data;
using MyVrmAddin2007.Properties;
using Exception = System.Exception;
using Image = System.Drawing.Image;

namespace MyVrmAddin2007
{
    public class MyVrmExplorer : ExplorerWrapper
    {
        private CommandBar _myVrmCommandBar;
        private CommandBar _myVrmVCCommandBar;

        private CommandBarComboBox _templateCombo;
        private CommandBarButton _fillTemplateCombo;
        private /*List<TemplateInfo>*/TDBTemplateCollection _templateList = new TDBTemplateCollection();// List<TemplateInfo>();

        private CommandBarButton _favoritesButton;
        private CommandBarButton _roomPrivateCalendarButton;
        private CommandBarButton _roomPublicCalendarButton;
        private CommandBarButton _conferenceButton;
        private CommandBarButton _instantConferenceButton;
        private CommandBarButton _approvalPendingButton;
        private CommandBarButton _optionsButton;

        private CommandBarButton _vConferenceButton;
        private CommandBarButton _vcOptionsButton;
        private CommandBarButton _seatsButton;

        private const string myVRMToolbarVal = "myVRMToolbarEnabled";
        public bool immediateconf = false;

        public MyVrmExplorer(Explorer explorer)
            : base(explorer)
        {
        }

        protected override void OnClose()
        {
            base.OnClose();
            try
            {
                //Do nothing for Outlook 2010 and 2013
                if (Explorer.Application.Version.StartsWith("14.") || Explorer.Application.Version.StartsWith("15."))
                    return;
                if (MyVrmAddin.BuildType != "04")
                {
                    _conferenceButton.Click -= _conferenceButton_Click;
                    _conferenceButton.Delete(true);
                    _conferenceButton = null;

                    _fillTemplateCombo.Click -= _fillTemplateCombo_Click;
                    _fillTemplateCombo.Delete(true);
                    _fillTemplateCombo = null;

                    _approvalPendingButton.Click -= _approvalPendingButton_Click;
                    _approvalPendingButton.Delete(true);
                    _approvalPendingButton = null;

                    _roomPrivateCalendarButton.Click -= _roomCalendarButton_Click;
                    _roomPrivateCalendarButton.Delete(true);
                    _roomPrivateCalendarButton = null;

                    _roomPublicCalendarButton.Click -= _roomCalendarButton_Click;
                    _roomPublicCalendarButton.Delete(true);
                    _roomPublicCalendarButton = null;

                    _favoritesButton.Click -= _favoriteRoomsButton_Click;
                    _favoritesButton.Delete(true);
                    _favoritesButton = null;

                    _optionsButton.Click -= OptionsButtonClick;
                    _optionsButton.Delete(true);
                    _optionsButton = null;

                    _instantConferenceButton.Click -= new _CommandBarButtonEvents_ClickEventHandler(_instantConferenceButton_Click);
                    _instantConferenceButton.Delete(true);
                    _instantConferenceButton = null;
                    bool bVisible = _myVrmCommandBar.Visible;
                    using (var key = Registry.CurrentUser.OpenSubKey(MyVrmAddin.LocalUserAppRegPath, true))
                    {
                        key.SetValue(myVRMToolbarVal, bVisible ? 1 : 0, RegistryValueKind.DWord);
                    }
                    _myVrmCommandBar.Delete();
                    Marshal.ReleaseComObject(_myVrmCommandBar);
                    _myVrmCommandBar = null;
                }
                else
                {
                    //********
                    _vConferenceButton.Click -= createVirtualConferenceButton_Click;
                    _vConferenceButton.Delete(true);
                    _vConferenceButton = null;

                    _seatsButton.Click -= CheckSeatsButtonClick;
                    _seatsButton.Delete(true);
                    _seatsButton = null;

                    _vcOptionsButton.Click -= OptionsButtonClick;
                    _vcOptionsButton.Delete(true);
                    _vcOptionsButton = null;

                    _instantConferenceButton.Click -= new _CommandBarButtonEvents_ClickEventHandler(_instantConferenceButton_Click);
                    _instantConferenceButton.Delete(true);
                    _instantConferenceButton = null;

                    _fillTemplateCombo.Click -= _fillTemplateCombo_Click;
                    _fillTemplateCombo.Delete(true);
                    _fillTemplateCombo = null;

                    _myVrmVCCommandBar.Delete();
                    Marshal.ReleaseComObject(_myVrmVCCommandBar);
                    _myVrmVCCommandBar = null;
                }
            }
            catch (Exception e)
            {
                MyVrmAddin.TraceSource.TraceException(e);
                UIHelper.ShowError(e.Message);
            }
        }

        protected override void Initialize()
        {
            base.Initialize();
            try
            {
                int _isVisible = 0;
                bool bValueIsExist = false;

                //Do nothing for Outlook 2010 and 2013
                if (Explorer.Application.Version.StartsWith("14.") || Explorer.Application.Version.StartsWith("15."))
                    return;

                RegistryKey myVRMKey = Registry.CurrentUser.OpenSubKey(MyVrmAddin.LocalUserAppRegPath);
                if (myVRMKey != null)
                {
                    string[] values = myVRMKey.GetValueNames();
                    bValueIsExist = values.Contains(myVRMToolbarVal);
                }
                if (bValueIsExist) //if key and value exist use them
                {
                    object val = myVRMKey.GetValue(myVRMToolbarVal, 0);
                    int.TryParse(val.ToString(), out _isVisible);
                }
                else //otherwice there is the first run - show toolbar
                {
                    _isVisible = 1;
                }

                if (myVRMKey != null)
                    myVRMKey.Close();


                //Create toolbar
                CommandBars cmdBars = Explorer.CommandBars;
                if (MyVrmAddin.BuildType != "04")
                {

                    _myVrmCommandBar = cmdBars.Add( /*"myVRM"*/MyVrmAddin.ProductDisplayName, MsoBarPosition.msoBarTop, false, true);
                    _myVrmCommandBar.Enabled = true;

                    //Create Conference button
                    _conferenceButton = (CommandBarButton)_myVrmCommandBar.Controls.Add(
                        MsoControlType.msoControlButton, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    _conferenceButton.Style = MsoButtonStyle.msoButtonIconAndCaption;
                    _conferenceButton.Caption = Strings.NewConference;
                    _conferenceButton.Picture = MyVrm.Outlook.WinForms.AxHost.GetIPictureDispFromPicture((Image)MyVrmAddin.Logo16);

                    //Resources.Logo);
                    _conferenceButton.Mask = MyVrm.Outlook.WinForms.AxHost.GetMaskImage((Image)MyVrmAddin.Logo16); //Resources.Logo);
                    _conferenceButton.Click += _conferenceButton_Click;

                    //Instant Conference Button
                    _instantConferenceButton = (CommandBarButton)_myVrmCommandBar.Controls.Add(
                        MsoControlType.msoControlButton, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    _instantConferenceButton.Style = MsoButtonStyle.msoButtonIconAndCaption;
                    _instantConferenceButton.Caption = Strings.NewConference;
                    _instantConferenceButton.Picture = MyVrm.Outlook.WinForms.AxHost.GetIPictureDispFromPicture((Image)MyVrmAddin.InstantConference16);
                    _instantConferenceButton.Mask = MyVrm.Outlook.WinForms.AxHost.GetMaskImage((Image)MyVrmAddin.InstantConference16); //Resources.Logo);
                    _instantConferenceButton.Click += new _CommandBarButtonEvents_ClickEventHandler(_instantConferenceButton_Click);

                    /*exp templates ->*/
                    _fillTemplateCombo = (CommandBarButton)_myVrmCommandBar.Controls.Add(
                        MsoControlType.msoControlButton, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    _fillTemplateCombo.Style = MsoButtonStyle.msoButtonCaption;
                    _fillTemplateCombo.Caption = Strings.UpdateTemplateListMenuPoint;
                    _fillTemplateCombo.Click += _fillTemplateCombo_Click;
                    _fillTemplateCombo.BeginGroup = true;
                    _fillTemplateCombo.TooltipText = Strings.UpdateTemplateListMenuPointToolTip;

                    _templateCombo = (CommandBarComboBox)_myVrmCommandBar.Controls.Add(
                        MsoControlType.msoControlComboBox, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    _templateCombo.Visible = true;
                    _templateCombo.Change += _templateCombo_Change;
                    _templateCombo.Width = 200;
                    _templateCombo.Enabled = false;
                    _templateCombo.Caption = Strings.TemplateListMenuPointToolTip;

                    //Create Approve Conference button
                    _approvalPendingButton = (CommandBarButton)_myVrmCommandBar.Controls.Add(
                        MsoControlType.msoControlButton, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    _approvalPendingButton.Style = MsoButtonStyle.msoButtonIconAndCaption;
                    _approvalPendingButton.Caption = Strings.ConferenceApproval;
                    _approvalPendingButton.Picture = MyVrm.Outlook.WinForms.AxHost.GetIPictureDispFromPicture(Resources.approveConf);
                    //Logo);
                    _approvalPendingButton.Mask = MyVrm.Outlook.WinForms.AxHost.GetMaskImage(Resources.approveConf); //Logo);
                    _approvalPendingButton.Click += _approvalPendingButton_Click;
                    _approvalPendingButton.BeginGroup = true;
                    //Create Private Room Calendar button
                    _roomPrivateCalendarButton = (CommandBarButton)_myVrmCommandBar.Controls.Add(
                        MsoControlType.msoControlButton, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    _roomPrivateCalendarButton.Style = MsoButtonStyle.msoButtonIconAndCaption;
                    _roomPrivateCalendarButton.Caption = Strings.RoomsPrivateCalendar;
                    _roomPrivateCalendarButton.Picture =
                        MyVrm.Outlook.WinForms.AxHost.GetIPictureDispFromPicture(Resources.RoomsCalendar);
                    _roomPrivateCalendarButton.Mask = MyVrm.Outlook.WinForms.AxHost.GetMaskImage(Resources.RoomsCalendar);
                    _roomPrivateCalendarButton.Click += _roomCalendarButton_Click;

                    //Create Public Room Calendar button
                    _roomPublicCalendarButton = (CommandBarButton)_myVrmCommandBar.Controls.Add(
                        MsoControlType.msoControlButton, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    _roomPublicCalendarButton.Style = MsoButtonStyle.msoButtonIconAndCaption;
                    _roomPublicCalendarButton.Caption = Strings.RoomsPublicCalendar;
                    _roomPublicCalendarButton.Picture =
                        MyVrm.Outlook.WinForms.AxHost.GetIPictureDispFromPicture(Resources.RoomsCalendar);
                    _roomPublicCalendarButton.Mask = MyVrm.Outlook.WinForms.AxHost.GetMaskImage(Resources.RoomsCalendar);
                    _roomPublicCalendarButton.Click += _roomCalendarButton_Click;

                    //Create Room Favorites button
                    _favoritesButton = (CommandBarButton)_myVrmCommandBar.Controls.Add(
                        MsoControlType.msoControlButton, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    _favoritesButton.Style = MsoButtonStyle.msoButtonIconAndCaption;
                    _favoritesButton.Caption = Strings.FavoriteRooms;
                    _favoritesButton.Picture = MyVrm.Outlook.WinForms.AxHost.GetIPictureDispFromPicture(Resources.FavoriteRooms);
                    _favoritesButton.Mask = MyVrm.Outlook.WinForms.AxHost.GetMaskImage(Resources.FavoriteRooms);
                    _favoritesButton.Click += _favoriteRoomsButton_Click;
                    //Create Options button
                    _optionsButton = (CommandBarButton)_myVrmCommandBar.Controls.Add(
                        MsoControlType.msoControlButton, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    _optionsButton.Style = MsoButtonStyle.msoButtonIconAndCaption;
                    _optionsButton.Caption = Strings.OptionsMenuPoint;
                    _optionsButton.Picture = MyVrm.Outlook.WinForms.AxHost.GetIPictureDispFromPicture(Resources.Options);
                    _optionsButton.Mask = MyVrm.Outlook.WinForms.AxHost.GetMaskImage(Resources.Options);
                    _optionsButton.Click += OptionsButtonClick;

                    //For York version - hide all points except "New Conference" and "Options"
                    if (MyVrmAddin.BuildType == "03")
                    {
                        _fillTemplateCombo.Visible = false;
                        _templateCombo.Visible = false;
                        _approvalPendingButton.Visible = false;
                        _roomPrivateCalendarButton.Visible = false;
                        _roomPublicCalendarButton.Visible = false;
                        _favoritesButton.Visible = false;
                    }
                    _myVrmCommandBar.Visible = _isVisible > 0;
                }
                //******************************
                else
                {
                    _myVrmVCCommandBar = cmdBars.Add(" myVRM Virtual Conference ", MsoBarPosition.msoBarTop, false, true);
                    _myVrmVCCommandBar.Enabled = true;

                    _vConferenceButton =
                        (CommandBarButton)
                        _myVrmVCCommandBar.Controls.Add(MsoControlType.msoControlButton, Type.Missing, Type.Missing, Type.Missing,
                                                        Type.Missing);
                    _vConferenceButton.Style = MsoButtonStyle.msoButtonIconAndCaption;
                    _vConferenceButton.Caption = Strings.NewConference;
                    _vConferenceButton.Picture = MyVrm.Outlook.WinForms.AxHost.GetIPictureDispFromPicture(Resources.virtualConf16x16);
                    _vConferenceButton.Mask = MyVrm.Outlook.WinForms.AxHost.GetMaskImage(Resources.virtualConf16x16); //Resources.Logo);
                    _vConferenceButton.Click += createVirtualConferenceButton_Click;

                    //Instant Conference Button

                    _instantConferenceButton = (CommandBarButton)_myVrmVCCommandBar.Controls.Add(
                        MsoControlType.msoControlButton, Type.Missing, Type.Missing, Type.Missing, Type.Missing);

                    _instantConferenceButton.Style = MsoButtonStyle.msoButtonIconAndCaption;

                    _instantConferenceButton.Caption = Strings.InstantConference;

                    _instantConferenceButton.Picture = MyVrm.Outlook.WinForms.AxHost.GetIPictureDispFromPicture(Resources.instant_conference_icon_16x16);

                    _instantConferenceButton.Mask = MyVrm.Outlook.WinForms.AxHost.GetMaskImage(Resources.instant_conference_icon_16x16); //Resources.Logo);

                    _instantConferenceButton.Click += new _CommandBarButtonEvents_ClickEventHandler(_instantConferenceButton_Click);



                    _fillTemplateCombo = (CommandBarButton)_myVrmVCCommandBar.Controls.Add(
                        MsoControlType.msoControlButton, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    _fillTemplateCombo.Style = MsoButtonStyle.msoButtonCaption;
                    _fillTemplateCombo.Caption = Strings.UpdateTemplateListMenuPoint;
                    _fillTemplateCombo.Click += _fillTemplateCombo_Click;
                    _fillTemplateCombo.BeginGroup = true;
                    _fillTemplateCombo.TooltipText = Strings.UpdateTemplateListMenuPointToolTip;

                    _templateCombo = (CommandBarComboBox)_myVrmVCCommandBar.Controls.Add(
                        MsoControlType.msoControlComboBox, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    _templateCombo.Visible = true;
                    _templateCombo.Change += _templateCombo_Change;
                    _templateCombo.Width = 200;
                    _templateCombo.Enabled = false;
                    _templateCombo.Caption = Strings.TemplateListMenuPointToolTip;

                    _seatsButton = (CommandBarButton)_myVrmVCCommandBar.Controls.Add(
                        MsoControlType.msoControlButton, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    _seatsButton.Style = MsoButtonStyle.msoButtonIconAndCaption;
                    _seatsButton.Caption = Strings.CheckAvailableSeatsMenuItemTxt;// "Check Available Seats";
                    _seatsButton.Picture = MyVrm.Outlook.WinForms.AxHost.GetIPictureDispFromPicture(Resources.checkSeats16x16);
                    _seatsButton.Mask = MyVrm.Outlook.WinForms.AxHost.GetMaskImage(Resources.checkSeats16x16);
                    _seatsButton.BeginGroup = true;
                    _seatsButton.Click += CheckSeatsButtonClick;

                    _vcOptionsButton = (CommandBarButton)_myVrmVCCommandBar.Controls.Add(
                        MsoControlType.msoControlButton, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    _vcOptionsButton.Style = MsoButtonStyle.msoButtonIconAndCaption;
                    _vcOptionsButton.Caption = Strings.OptionsMenuPoint;
                    _vcOptionsButton.Picture = MyVrm.Outlook.WinForms.AxHost.GetIPictureDispFromPicture(Resources.Options16x16);
                    _vcOptionsButton.Mask = MyVrm.Outlook.WinForms.AxHost.GetMaskImage(Resources.Options16x16);
                    _vcOptionsButton.Click += OptionsButtonClick;
                    _myVrmVCCommandBar.Visible = true;
                }
            }
            catch (Exception exception)
            {
                MyVrmAddin.TraceSource.TraceException(exception);
            }
        }

        void _instantConferenceButton_Click(CommandBarButton Ctrl, ref bool CancelDefault)
        {
            try
            {
                BindingList<UsersDatasource> lstOutlookContacts = new BindingList<UsersDatasource>();
                MyVrm.Outlook.WinForms.Conference.InstantConference iConference = new InstantConference(lstOutlookContacts);
                iConference.EnableStatic = UserHasStaticID();
                iConference.Accepted += iConference_Accepted;
                DialogResult dlgResult = iConference.ShowDialog();
            }
            catch (Exception ex)//101677
            {
                MyVrmAddin.TraceSource.TraceException(ex);
                UIHelper.ShowError(ex.Message);
            }



        }

        void iConference_Accepted(object sender, EventArgs e)
        {
            InstantConference iConference = sender as InstantConference;
            if (iConference == null)
                return;
            try
            {
                immediateconf = iConference.immediateconf; //ZD 101226
               
                bool isStatic = iConference.IsStatic;
                string recipients = iConference.Recipients;

                AppointmentItem appItem = (AppointmentItem)Explorer.Application.CreateItem(OlItemType.olAppointmentItem);


                //ZD 101226 Starts
                //appItem.Start = DateTime.Now; 
                
                //appItem.End = appItem.Start.AddMinutes(30);
                //ZD 101226 Ends

                ConferenceRibbon.ConvertAppointment2Virtual(appItem, isStatic, immediateconf);

                OutlookAppointment outlookAppt = Globals.ThisAddIn.Appointments[appItem] as OutlookAppointment;
                if (outlookAppt != null)
                    outlookAppt.RaiseSavingEvent = true;

                char[] splitChar = { ';' };
                string[] splitParticipants = recipients.Split(splitChar, StringSplitOptions.RemoveEmptyEntries);
                if (splitParticipants.Length > 0)
                {
                    appItem.MeetingStatus = OlMeetingStatus.olMeeting;
                    foreach (string strParticipant in splitParticipants)
                        appItem.Recipients.Add(strParticipant);


                    appItem.Send();


                }
                else
                {
                    appItem.MeetingStatus = OlMeetingStatus.olNonMeeting;
                    appItem.Save();
                    appItem.Close(OlInspectorClose.olSave);

                }
            }
            catch
            {
            }
            iConference.Close();
        }



        private void _favoriteRoomsButton_Click(CommandBarButton ctrl, ref bool cancel)
        {
            ConferenceRibbon.favoriteRoomsButton_OnClick();
        }

        private void OptionsButtonClick(CommandBarButton ctrl, ref bool cancel)
        {
            /*using (var optDlg = new OptionsAsDlg())
             {
                 CancelEventArgs e = new CancelEventArgs();
                 if (optDlg.ShowDialog() == DialogResult.OK)
                     optDlg.optionsControl.External_OnApplying(e);
             }*/
            try
            {
                BindingList<UsersDatasource> lstOutlookContacts = new BindingList<UsersDatasource>();//Utils.GetOutlookContacts(Globals.ThisAddIn.Application);
                NewOptionsDlg optDlg = new NewOptionsDlg(lstOutlookContacts);
                optDlg.ShowDialog();
            }
            catch (Exception ex)//101677
            {
                UIHelper.ShowError (ex.Message);
                MyVrmAddin.TraceSource.TraceException(ex);

            }


        }

        private void _fillTemplateCombo_Click(CommandBarButton ctrl, ref bool cancel)
        {
            _templateCombo.Clear();
            _templateCombo.Enabled = false;
            //GetTemplateListResponse ret = null;
            TDGetTemplateListResponse ret = null;
            _templateList.Clear();

            var cursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            //Get data
            try
            {
                ret = MyVrmService.Service.TDGetTemplateList(TDGetTemplateListResponse.SortBy.ByName);
                //GetTemplateList(GetTemplateListResponse.SortByType.ByName);
            }
            catch (Exception exception)
            {
                MyVrmAddin.TraceSource.TraceException(exception, true);
                UIHelper.ShowMessage(exception.Message, MessageBoxButtons.OK,
                                     MessageBoxIcon.Exclamation);
            }
            finally
            {
                Cursor.Current = cursor;
            }
            if (ret != null && ret.TemplateCollection != null)
            {
                _templateList = ret.TemplateCollection;
                foreach (TDBTemplate templateInfo in ret.TemplateCollection)
                {
                    _templateCombo.AddItem(templateInfo.Name, Type.Missing);
                }
                _templateCombo.Enabled = true;
                //_templateCombo.Execute();

                _templateCombo.SetFocus();
                SendKeys.Send("+({DOWN})");
            }
        }

        void SetAppointmentParamsFromTemplate(int templateID)
        {
            TDGetTemplateResponse response = null;
            var cursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                response = MyVrmService.Service.TDGetTemplateInfo(templateID);
                //GetTemplate(templateID);
            }
            catch (Exception exception)
            {
                MyVrmAddin.TraceSource.TraceException(exception, true);
                UIHelper.ShowMessage(exception.Message, MessageBoxButtons.OK,
                                         MessageBoxIcon.Exclamation);
            }
            finally
            {
                Cursor.Current = cursor;
            }

            if (response != null)
            {
                //OutlookAppointmentImpl apptImpl;

                AppointmentItem appItem = (AppointmentItem)Explorer.Application.CreateItem(OlItemType.olAppointmentItem);
                Convert2TDBWrap(appItem, false);

                appItem.Body = string.IsNullOrEmpty(response.tdbTemplateInfo.Body) ? appItem.Body : response.tdbTemplateInfo.Body;
                appItem.Subject = string.IsNullOrEmpty(response.tdbTemplateInfo.Subj) ? appItem.Subject : response.tdbTemplateInfo.Subj;
                appItem.Duration = response.tdbTemplateInfo.Duration > 0 ? response.tdbTemplateInfo.Duration : appItem.Duration;
                appItem.Start = response.tdbTemplateInfo.StartTime.TimeOfDay.Ticks > 0 ?
                    appItem.Start.Date.AddTicks(response.tdbTemplateInfo.StartTime.TimeOfDay.Ticks) : appItem.Start;

                if (response.tdbTemplateInfo.Participans != null)
                {
                    var participantsEmialList = response.tdbTemplateInfo.Participans.Select(participant => participant.Email).ToList();

                    foreach (string outlookRecipientMail in participantsEmialList)
                    {
                        Recipient recipient = appItem.Recipients.Add(outlookRecipientMail);
                        bool bret = recipient.Resolve();
                        if (!recipient.Resolved)
                        {
                            appItem.Recipients.Remove(recipient.Index);
                        }
                    }
                }
                appItem.Display(Type.Missing);
                //appItem.GetInspector.SetCurrentFormPage("TrueDayBookAddin2007.ConferenceFormRegion");
                //ThisAddIn.Sub_SetAppointmentParamsFromTemplate(appItem, response.Conference, Type.Missing);
            }
        }

        void _templateCombo_Change(CommandBarComboBox Ctrl)
        {
            TDBTemplate found = _templateList.FirstOrDefault(a => a.Name == Ctrl.Text);
            int selected = found != null ? found.Id : -1;
            //FindIndex(0, tempalte => tempalte.Name == Ctrl.Text);
            if (selected >= 0)
            {
                SetAppointmentParamsFromTemplate(selected);//int.Parse(_templateList[selected].ID));
            }
        }

        private void _roomCalendarButton_Click(CommandBarButton ctrl, ref bool cancel)
        {
            ConferenceRibbon.roomsCalendarButton_OnClick(ctrl.Index == _roomPrivateCalendarButton.Index);
        }

        private void _approvalPendingButton_Click(CommandBarButton ctrl, ref bool cancel)
        {
            ConferenceRibbon.approvalConferencesButton_OnClick();
        }

        private void _conferenceButton_Click(CommandBarButton ctrl, ref bool cancel)
        {
            var cursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                AppointmentItem appItem = (AppointmentItem)Explorer.Application.CreateItem(OlItemType.olAppointmentItem);
                appItem.Display(Type.Missing);
                //appItem.GetInspector.SetCurrentFormPage("TrueDayBookAddin2007.ConferenceFormRegion");
            }
            catch (Exception exception)
            {
                MyVrmAddin.TraceSource.TraceException(exception);
                UIHelper.ShowError(exception.Message);
            }
            finally
            {
                Cursor.Current = cursor;
            }
        }

        private static bool UserHasStaticID()
        {
            User user = MyVrmService.Service.GetUser();
            return user.IsStaticIDEnabled;
        }
        public static void Convert2TDBWrap(AppointmentItem item, bool isStaticId)
        {
            try
            {


                AppointmentItem appItem = item;// (AppointmentItem)Explorer.Application.CreateItem(OlItemType.olAppointmentItem);
                OutlookAppointment outlookAppt = Globals.ThisAddIn.Appointments[appItem] as OutlookAppointment;
                if (outlookAppt != null)
                    outlookAppt.RaiseSavingEvent = false;



                ConferenceRibbon.ConvertAppointment2Virtual(appItem, isStaticId, false);//ZD 101226
                //appItem.End = appItem.Start.AddMinutes(MyVrmService.Service.OrganizationOptions.DefaultConfDuration); //102545 //ZD 104749
                //100337 - Why are we doing this?
                /*DateTime dt = appItem.Start.AddMinutes(appItem.ReminderMinutesBeforeStart + 5);
                int minutes = dt.Minute;
                minutes = ((minutes + 10) / 10) * 10;
                dt = new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, 0, 0);
                appItem.Start = dt + new TimeSpan(0, minutes, 0);*/
                appItem.Display(Type.Missing);
                outlookAppt = Globals.ThisAddIn.Appointments[appItem] as OutlookAppointment;
                if (outlookAppt != null)
                    outlookAppt.RaiseSavingEvent = true;
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        private void createVirtualConferenceButton_Click(CommandBarButton ctrl, ref bool cancel)
        {
            var cursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                //using (NewConfTypeDlg dlg = new NewConfTypeDlg())
                {
                    //	DialogResult dr = dlg.ShowDialog();
                    //	if (dr == DialogResult.OK)
                    {
                        //dlg.ConfId;
                        AppointmentItem appItem = (AppointmentItem)Explorer.Application.CreateItem(OlItemType.olAppointmentItem);
                        bool IsStaticId = false;
                        if (UserHasStaticID())
                        {
                            BindingList<UsersDatasource> lstOutlookContacts = new BindingList<UsersDatasource>();//Utils.GetOutlookContacts(Globals.ThisAddIn.Application);
                            MyVrm.Outlook.WinForms.Conference.InstantConference conferenceDialog = new InstantConference(lstOutlookContacts);
                            conferenceDialog.HideRecipients = true;
                            DialogResult dlgResult = conferenceDialog.ShowDialog();
                            if (dlgResult == DialogResult.OK)
                            {
                                IsStaticId = conferenceDialog.IsStatic;
                            }
                            else
                                return;


                        }
                        Convert2TDBWrap(appItem, IsStaticId);
                        //Comments from Rob
                        //appItem.GetInspector.SetCurrentFormPage("TrueDayBookAddin2007.ConferenceFormRegion");
                        /*
                        AppointmentItem appItem = (AppointmentItem) Explorer.Application.CreateItem(OlItemType.olAppointmentItem);

                        ConferenceRibbon.ConvertAppointment2Virtual(appItem);
                        DateTime dt = DateTime.Now.AddMinutes(appItem.ReminderMinutesBeforeStart + 5);
                        int minutes = dt.Minute;
                        minutes = ((minutes + 10)/10)*10;
                        dt = new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, 0, 0);
                        appItem.Start = dt + new TimeSpan(0, minutes, 0);
                        appItem.Display(Type.Missing);

                        */


                        //appItem.Body = "[Template invitation body text obtained from the server]";
                        //DateTime dt = DateTime.Now;
                        //DateTime dtRet = new DateTime(dt.Year, dt.Month, dt.Day + 1, dt.Hour, 0, 0);
                        //appItem.Start = dtRet;
                        //appItem.Duration = 30;
                        //appItem.Subject = "[Template invitation subject text obtained from the server]";
                        //if (appItem.UserProperties.Find("IsVConf", true) == null)
                        //    appItem.UserProperties.Add("IsVConf", OlUserPropertyType.olYesNo, false, OlFormatYesNo.olFormatYesNoTrueFalse);
                        //appItem.UserProperties["IsVConf"].Value = true;
                        //UserProperty pr = appItem.UserProperties.Find("IsVConf", true);
                        //appItem.Location = "[virtual room]";
                        //appItem.GetInspector.SetCurrentFormPage("TrueDayBookAddin2007.ConferenceFormRegion");
                    }
                }
            }
            catch (Exception exception)
            {
                MyVrmAddin.TraceSource.TraceException(exception);
                //UIHelper.ShowError(exception.Message);
            }
            finally
            {
                Cursor.Current = cursor;
            }
        }

        public static void CheckSeatsButtonClick(CommandBarButton ctrl, ref bool cancel)
        {
            var cursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                using (var checkXmlSeatsDlg = new XmlSeatsDlg())
                {
                    checkXmlSeatsDlg.ConfStart = DateTime.Now;
                    checkXmlSeatsDlg.ConfEnd = checkXmlSeatsDlg.ConfStart.AddHours(1);
                    checkXmlSeatsDlg.ConfId = string.Empty;
                    checkXmlSeatsDlg.ShowDialog();
                    if (checkXmlSeatsDlg.OpenOptionsDlg)
                    {
                        ConnectionOptionsDialog.ExternalCall();
                    }
                }
                /* unused
                using (var checkSeatsDlg = new CheckSeatsDlg())
                {
                    checkSeatsDlg.ConfStart = DateTime.Now.Date;
                    checkSeatsDlg.ShowDialog();
                    if(checkSeatsDlg.OpenOptionsDlg)
                    {
                        ConnectionOptionsDialog.ExternalCall();
                    }
                }
                 * */
            }
            catch (Exception exception)
            {
                MyVrmAddin.TraceSource.TraceException(exception);
                UIHelper.ShowError(exception.Message);
            }
            finally
            {
                Cursor.Current = cursor;
            }
        }

    }
}
