﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.WebServices.Data
{
    internal class GetWorkOrderDetailsRequest : ServiceRequestBase<GetWorkOrderDetailsResponse>
    {
        internal GetWorkOrderDetailsRequest(MyVrmService service) : base(service)
        {
        }

        internal ConferenceId ConferenceId { get; set; }
        internal WorkOrderId WorkOrderId { get; set; }


        #region Overrides of ServiceRequestBase

        internal override string GetXmlElementName()
        {
            return "login";
        }

        internal override string GetCommandName()
        {
            return "GetWorkOrderDetails";
        }

        internal override string GetResponseXmlElementName()
        {
            return "WorkOrder";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            UserId.WriteToXml(writer);
            ConferenceId.WriteToXml(writer, "ConfID");
            WorkOrderId.WriteToXml(writer, "WorkorderID");
			writer.WriteElementValue(XmlNamespace.NotSpecified, "utcEnabled", Service.IsUtcEnabled);
        }

        #endregion
    }
}
