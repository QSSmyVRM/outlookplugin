﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using Microsoft.VisualStudio.Tools.Applications.Runtime;
using MyVrm.DataImport.Excel;
using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;

namespace MyVrmExcelImportTemplate
{
    public partial class LDAPSheet
    {
        private LdapTable _ldapTable;

        private void LDAPSheet_Startup(object sender, System.EventArgs e)
        {
            _ldapTable = new LdapTable(ldapTable, Globals.ThisWorkbook.Service,
                                       Globals.ThisWorkbook.ValidationListManager);
            Globals.ThisWorkbook.RegisterTable(ldapTable, _ldapTable);
        }

        private void LDAPSheet_Shutdown(object sender, System.EventArgs e)
        {
        }

        #region VSTO Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(LDAPSheet_Startup);
            this.Shutdown += new System.EventHandler(LDAPSheet_Shutdown);
        }

        #endregion

    }
}
