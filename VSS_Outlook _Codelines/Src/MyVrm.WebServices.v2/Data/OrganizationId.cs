﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    /// <summary>
    /// Represents an organization ID.
    /// </summary>
    public class OrganizationId : ServiceId
    {
        internal static OrganizationId Invalid = new OrganizationId();

        internal OrganizationId()
        {
        }

        public OrganizationId(string id) : base(id)
        {
        }

        #region Overrides of ServiceId

        internal override string GetXmlElementName()
        {
            return "organizationID";
        }

        #endregion
    }
}
