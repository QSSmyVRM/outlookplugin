﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    public class LoggedOnUserSchema : ServiceObjectSchema
    {
        public static PropertyDefinition IsNew;
        public static PropertyDefinition GlobalInfo;
        internal static readonly LoggedOnUserSchema Instance;
        
        static LoggedOnUserSchema()
        {
            IsNew = new BoolPropertyDefinition("newUser");
            GlobalInfo = new ComplexPropertyDefinition<LoggedOnUserGlobalInfo>("globalInfo", () => new LoggedOnUserGlobalInfo());
            Instance = new LoggedOnUserSchema();
        }

        internal LoggedOnUserSchema()
        {
        }

        internal override void RegisterProperties()
        {
            base.RegisterProperties();
            RegisterProperty(IsNew);
            RegisterProperty(GlobalInfo);
        }
    }
}
