/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Text;
using System.Threading;

namespace MyVrm.Common
{
    /// <summary>
    /// Specifies whether a property, enum value, etc have localized description.
    /// </summary>
    [Serializable, AttributeUsage(AttributeTargets.All, AllowMultiple = false, Inherited = true)]
    public class LocalizedDescriptionAttribute : DescriptionAttribute
    {
        private readonly string _description;
        private static Dictionary<object, string> _enumStringTable;

        /// <summary>
        /// Initializes a new instance of <see cref="LocalizedDescriptionAttribute"/> class.
        /// </summary>
        public LocalizedDescriptionAttribute()
        {
            
        }
        /// <summary>
        /// Initializes a new instance of <see cref="LocalizedDescriptionAttribute"/> class.
        /// </summary>
        /// <param name="description">Description of a property, enum value, etc.</param>
        public LocalizedDescriptionAttribute(string description)
        {
            _description = description;
        }
        /// <summary>
        /// Initializes a new instance of <see cref="LocalizedDescriptionAttribute"/> class.
        /// </summary>
        /// <param name="propertyName">Name of property within Resources class.</param>
        /// <param name="resourcesType">Type of Resources class.</param>
        public LocalizedDescriptionAttribute(string propertyName, Type resourcesType)
        {
            if (string.IsNullOrEmpty(propertyName))
            {
                throw new ArgumentNullException("propertyName");
            }
            if (resourcesType == null)
            {
                throw new ArgumentNullException("resourcesType");
            }
            PropertyInfo property = resourcesType.GetProperty(propertyName, BindingFlags.Static | BindingFlags.NonPublic);
            _description = (string)property.GetValue(null, null);
        }
        /// <summary>
        /// Returns description for specified enum value.
        /// </summary>
        /// <param name="enumType">Type of enumeration.</param>
        /// <param name="value">Enum value.</param>
        /// <returns></returns>
        public static string FromEnum(Type enumType, object value)
        {
            string text;

            if (enumType == null)
            {
                throw new ArgumentNullException("enumType");
            }
            if (!enumType.IsEnum)
            {
                throw new ArgumentException("enumType must be an enum", "enumType");
            }
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }
            object key = Enum.ToObject(enumType, value);
            if (_enumStringTable == null)
            {
                var dict = new Dictionary<object, string>();
                Interlocked.CompareExchange(ref _enumStringTable, dict, null);
            }
            lock(_enumStringTable)
            {
                if (_enumStringTable.TryGetValue(key, out text))
                {
                    return text;
                }
                string[] enumValues = key.ToString().Split(new[] {','});
                var builder = new StringBuilder();
                for (int i = 0; i < enumValues.Length; i++)
                {
                    string name = enumValues[i];
                    string description = name;
                    FieldInfo field = enumType.GetField(name);
                    if (field != null)
                    {
                        object[] customAttributes = field.GetCustomAttributes(typeof (DescriptionAttribute), false);
                        if (customAttributes != null && customAttributes.Length > 0)
                        {
                            description = ((DescriptionAttribute) customAttributes[0]).Description;
                        }
                    }
                    if (i != 0)
                    {
                        builder.Append(", ");
                    }
                    builder.Append(description);
                }
                text = builder.ToString();
                _enumStringTable.Add(key, text);
            }
            return text;
        }
        /// <summary>
        /// Gets description.
        /// </summary>
        public sealed override string Description
        {
            get
            {
                return _description;
            }
        }
    }
}
