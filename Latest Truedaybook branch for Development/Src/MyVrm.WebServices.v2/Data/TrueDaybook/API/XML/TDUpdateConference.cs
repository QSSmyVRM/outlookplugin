﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using CallWebApi.Classes;
using System.Text.RegularExpressions;

namespace MyVrm.WebServices.Data
{
	internal class TDUpdateConferenceRequest : ServiceRequestBase<TDUpdateConferenceResponse>
	{
		internal string ConferenceId;
		internal string Subject;
		internal string Body;
		internal DateTime StartDate;
		internal DateTime EndDate;
		internal List<ConferenceRequest.Participant> Participants;
		internal bool Over;

		internal TDUpdateConferenceRequest(MyVrmService service)
			: base(service)
		{
		}

		#region Overrides of ServiceRequestBase

		internal override string GetXmlElementName()
		{
			return "TDUpdateConference";
		}

		internal override string GetCommandName()
		{
			return Constants.TDUpdateConferenceCommandName;
		}

		internal override string GetResponseXmlElementName()
		{
			return "TDUpdateConference";
		}

		internal override TDUpdateConferenceResponse ParseResponse(MyVrmServiceXmlReader reader)
		{
			var response = new TDUpdateConferenceResponse(); 
			response.LoadFromXml(reader, GetResponseXmlElementName());
			return response;
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			UserId.WriteToXml(writer, "userID");
			writer.WriteElementValue(XmlNamespace.NotSpecified, "conferenceId", ConferenceId);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "subject", Subject);
            
            //ZD 102545 start
            string Itembody = "";
            string[] Description; 
            if(Body.Contains("DO NOT DELETE OR CHANGE ANY OF THE TEXT BELOW THIS LINE"))
            {
                Description = Regex.Split(Body, "DO NOT DELETE OR CHANGE ANY OF THE TEXT BELOW THIS LINE");
                Itembody = Description[0].TrimEnd('*');
                writer.WriteElementValue(XmlNamespace.NotSpecified, "invitationBody", Itembody);
            }
            else  if(Body.Contains("You have been invited to attend a SCOPIA Meeting"))
            {
                Description = Regex.Split(Body, "You have been invited to attend a SCOPIA Meeting.");
                Itembody = Description[0].TrimEnd('\r');
                Itembody = Description[0].TrimEnd('\n');
                writer.WriteElementValue(XmlNamespace.NotSpecified, "invitationBody", Itembody);
            }
           else
			    writer.WriteElementValue(XmlNamespace.NotSpecified, "invitationBody", Body);
            //ZD 102545 End
			writer.WriteElementValue(XmlNamespace.NotSpecified, "startDate", StartDate.ToString("s", DateTimeFormatInfo.InvariantInfo));
			writer.WriteElementValue(XmlNamespace.NotSpecified, "endDate", EndDate.ToString("s", DateTimeFormatInfo.InvariantInfo));
			writer.WriteElementValue(XmlNamespace.NotSpecified, "overBookConf", Over ? 1 : 0);
			//<recurrence>????</recurrence>
			writer.WriteStartElement(XmlNamespace.NotSpecified, "participantsList");
			foreach (var participant in Participants)
			{
				writer.WriteStartElement(XmlNamespace.NotSpecified, "participant");
				writer.WriteElementValue(XmlNamespace.NotSpecified, "name", participant.Name);
				writer.WriteElementValue(XmlNamespace.NotSpecified, "email", participant.Email);
				writer.WriteEndElement();
			}
			writer.WriteEndElement();
		}
		#endregion
	}

	public class TDUpdateConferenceResponse : ServiceResponse
	{
		public string ConferenceId { get; internal set; }
		public long ExternalId { get; internal set; }
		public string ConferenceUrl{ get; internal set; }
		
		public string RetMessage { set; get; }
        public int RetCode { set; get; }
		public long SettingsVersion { set; get; }
		public XMLError XMLError { set; get; }

		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{
			while (!reader.IsEndElement(XmlNamespace.NotSpecified, "retState"))
			{
				try
				{
					switch (reader.LocalName)
					{
						case "conferenceId":
							ConferenceId = reader.ReadElementValue();
							break;
                        case "externalId":
                            ExternalId = reader.ReadElementValue<long>();
                            break;
                        case "conferenceUrl":
                           ConferenceUrl = reader.ReadElementValue();
                           break;
						case "retCode":
							RetCode = reader.ReadElementValue<int>();
							break;
						case "retMessage":
							RetMessage = reader.ReadElementValue();
							break;
						case "settingsVersion":
							SettingsVersion = reader.ReadElementValue<long>();
							break;
						case "error":
							XMLError = new XMLError();
							XMLError.LoadFromXml(reader, XmlNamespace.NotSpecified, "error");
							break;
					}
					reader.Read();
				}
				catch (Exception ex)
				{
					MyVrmService.TraceSource.TraceError(reader.LocalName + ": " + ex.Message, null);
					reader.Read();
					//throw;
				}
			}
		}
	}
}
