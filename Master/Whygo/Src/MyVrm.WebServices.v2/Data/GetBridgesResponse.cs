﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace MyVrm.WebServices.Data
{
    internal class GetBridgesResponse : ServiceResponse
    {
        private readonly List<BridgeName> _bridgeNames = new List<BridgeName>();

        public ReadOnlyCollection<BridgeName> BridgeNames
        {
            get
            {
                return new ReadOnlyCollection<BridgeName>(_bridgeNames);
            }
        }

        internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
        {
            base.ReadElementsFromXml(reader);
            _bridgeNames.Clear();
            do
            {
                reader.Read();
                if (reader.IsStartElement(XmlNamespace.NotSpecified, "Bridge"))
                {
                    var bridgeName = new BridgeName();
                    bridgeName.LoadFromXml(reader, "Bridge");
                    _bridgeNames.Add(bridgeName);
                }
                else
                {
                    reader.SkipCurrentElement();
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "Bridges"));
        }
    }
}
