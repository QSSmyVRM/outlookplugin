﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System.Windows.Forms;
using DevExpress.XtraEditors;
using Microsoft.Win32;
using MyVrm.Outlook.WinForms.Options;

namespace MyVrm.Outlook.WinForms
{
    public sealed class UIHelper
    {
        private static UIHelper _instance;
        private Properties.Settings _settings;
        private static readonly object LockObject = new object();

        private UIHelper()
        {
            
        }

        public static UIHelper Instance
        {
            get
            {
                lock (LockObject)
                {
                    return _instance ?? (_instance = new UIHelper());
                }
            }
        }

        internal Properties.Settings Settings
        {
            get
            {
                if (_settings == null)
                {
                    _settings = new Properties.Settings();
                    var provider = MyVrmAddin.Instance.GetNewSettingsProvider();
                    provider.Initialize(null, null);
                    _settings.UpdateProvider(provider);
                    _settings.Reload();
                }
                return _settings;
            }
        }

        public static void ShowConferenceIsSuccessfullyCreatedPopup(IWin32Window owner)
        {
            if (Instance.Settings.ShowPopupWhenConferenceIsScheduled)
            {
                ShowMessage(Strings.ConferenceIsScheduledSuccessfully);
            }
        }

        public static void ShowError(IWin32Window owner, string text)
        {
            ShowMessage(owner, text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        public static void ShowError(string text)
        {
            ShowError(null, text);
        }

        public static void ShowMessage(IWin32Window owner, string text)
        {
            ShowMessage(owner, text, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static void ShowMessage(string text)
        {
            ShowMessage(null, text);
        }

        public static DialogResult ShowMessage(string text, MessageBoxButtons buttons, MessageBoxIcon icon)
        {
            return ShowMessage(null, text, buttons, icon);
        }

        public static DialogResult ShowMessage(IWin32Window owner, string text, MessageBoxButtons buttons, MessageBoxIcon icon)
        {
            return MessageBox.Show(owner, text, MyVrmAddin.ProductDisplayName/*ProductName*/, buttons, icon);
        }

        public static bool AppendConferenceDetailsToMeeting
        {
            get
            {
                return Instance.Settings.AppendConferenceDetailsToMeeting;
            }
        }

		public static ConferenceOptionsDialog.DeletionOption DeletionOptionValue
		{
			get
			{
				return Instance.Settings.DeletionOptionValue;
			}
		}

        public static string GetCurrentSkinName(string version)
        {
            const int themeBlue = 1;
            const int themeSilver = 2;
            const int themeBlack = 3;
            if(string.IsNullOrEmpty(version)) // Unspecified Outlook version
            {
                return string.Empty;
            }
            if (version.StartsWith("11.")) // Outlook 2003
            {
                return "Office 2003";
            }
            if (version.StartsWith("12.")) // Outlook 2007
            {
                switch (GetOfficeColorScheme("12.0"))
                {
                    case themeBlue:
                        {
                            return "Office 2007 Blue";
                        }
                    case themeSilver:
                        {
                            return "Office 2007 Silver";
                        }
                    case themeBlack:
                        {
                            return "Office 2007 Black";
                        }
                    default:
                        {
                            return "Office 2007 Blue";
                        }
                }
            }
            if(version.StartsWith("14.")) // Outlook 2010
            {
                switch (GetOfficeColorScheme("14.0"))
                {
                    case themeBlue:
                        {
                            return "Office 2010 Blue";
                        }
                    case themeSilver:
                        {
                            return "Office 2010 Silver";
                        }
                    case themeBlack:
                        {
                            return "Office 2010 Black";
                        }
                    default:
                        {
                            return "Office 2010 Silver";
                        }
                }
            }
            return string.Empty; // Unknown Outlook version
        }

        private static int GetOfficeColorScheme(string version)
        {
            var officeCommonKey = string.Format(@"Software\Microsoft\Office\{0}\Common", version);
            const string officeThemeValueName = "Theme";

            using (var key = Registry.CurrentUser.OpenSubKey(officeCommonKey, false))
            {
                return (int) (key != null ? key.GetValue(officeThemeValueName, 0) : 0);
            }
        }
    }
}
