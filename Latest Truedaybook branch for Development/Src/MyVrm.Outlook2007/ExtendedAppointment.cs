﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using Microsoft.Office.Interop.Outlook.Extensions.Linq;

namespace MyVrm.Outlook
{
    internal class ExtendedAppointment : Appointment
    {
        [OutlookItemProperty("urn:schemas:calendar:dtstart")]
        public DateTime Start { get { return Item.Start; } }

        [OutlookItemProperty("urn:schemas:calendar:dtend")]
        public DateTime End { get { return Item.End; } }
    }
}
