﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
	//Request
	internal class ConferenceSetTemplateRequest : ServiceRequestBase<ConferenceSetTemplateResponse>
	{
		enum TemplateType
		{
			Private = 0,
			Public = 1
		}

		internal string Name;
		internal string Descr;
		internal bool IsPublic;
		internal bool IsDefault;
		internal Conference conference;

		internal ConferenceSetTemplateRequest(MyVrmService service)
			: base(service)
        {
		}

		#region Overrides of ServiceRequestBase<ConferenceSetTemplateResponse>

		internal override string GetXmlElementName()
		{
			return "template";  
		}

		internal override string GetCommandName()
		{
			return Constants.SetTemplateCommandName;
		}

		internal override string GetResponseXmlElementName()
		{
			return "template"; 
		}

		internal override ConferenceSetTemplateResponse ParseResponse(MyVrmServiceXmlReader reader)
		{
			var response = new ConferenceSetTemplateResponse();
			response.ReadElementsFromXml(reader);
			return response;
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			writer.WriteStartElement(XmlNamespace.NotSpecified, "userInfo");
			if (UserId != null)
				UserId.WriteToXml(writer, "userID");
			writer.WriteEndElement();

			TemplateInfo ti = new TemplateInfo()
			                  	{
			                  		ID = "new",
			                  		Name = Name,
			                  		IsPublic = IsPublic ? (int) TemplateType.Public : (int) TemplateType.Private,
			                  		OwnerFirstName = "",
			                  		OwnerLastName = "",
			                  		SetDefault = IsDefault,
									Description = Descr
			                  	};
			ti.WriteElementsToXml(writer);
			Template template = new Template(MyVrmService.Service);
			template.Name = conference.Name;
			template.Password = conference.Password;
			template.Description = conference.Description;
			template.Duration = conference.Duration;
			template.Type = conference.Type;
			template.Rooms.AddRange(conference.Rooms);
			template.Participants.AddRange(conference.Participants);
			template.IsPublic = conference.IsPublic;
			template.WriteToXml(writer);
		}

		#endregion
	}

	//Response
	public class ConferenceSetTemplateResponse : ServiceResponse 
	{
		//Tags
		private const string template = "template";
        private const string templateInfo = "templateInfo";
		private const string confInfo = "confInfo";
		public TemplateInfo TemplateInfo;
		public Conference Conference;

		//Read template info and template conference instance 
		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
        {
			do
			{
			    //Forward till "templateInfo" element 
			    while (!reader.IsEndElement(XmlNamespace.NotSpecified, templateInfo))
			    {
			    	while (!reader.IsStartElement(XmlNamespace.NotSpecified, templateInfo))
			    		reader.Read();

			    	if (reader.LocalName == templateInfo)
			    	{
			    		TemplateInfo = new TemplateInfo();
			    	}

			    	while (!reader.IsEndElement(XmlNamespace.NotSpecified, templateInfo))
			    	{
			    		TemplateInfo.TryReadElementFromXml(reader);
			    	}
			    }

				//Actually do not need to get it at all
				while (!reader.IsEndElement(XmlNamespace.NotSpecified, confInfo))
				{
					while (!reader.IsStartElement(XmlNamespace.NotSpecified, confInfo))
						reader.Read();

					if (reader.LocalName == confInfo)
					{
						Conference = new Conference(MyVrmService.Service);
					}

					while (!reader.IsEndElement(XmlNamespace.NotSpecified, confInfo))
					{
						Conference.LoadFromXml(reader,true);
					}
			    }
				
				reader.Read();

			} while (!reader.IsEndElement(XmlNamespace.NotSpecified, template)); //read till the "template" node end 
        }
	}
}
