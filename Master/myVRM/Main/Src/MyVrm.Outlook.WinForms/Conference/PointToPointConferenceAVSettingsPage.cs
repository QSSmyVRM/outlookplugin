﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using MyVrm.Common.ComponentModel;
using MyVrm.Common.EventBroker;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook.WinForms.Conference
{
    public partial class PointToPointConferenceAVSettingsPage : ConferencePage
    {
        private readonly DataList<RoomEndpoint> _roomEndpoints = new DataList<RoomEndpoint>();

        public PointToPointConferenceAVSettingsPage()
        {
            InitializeComponent();
            Text = Strings.PointToPointConferenceAVSettingsPageText;
            ConferenceBindingSource.DataSourceChanged += ConferenceBindingSource_DataSourceChanged;
            p2pRoomAudioVideoSettingsListControl.DataSource = _roomEndpoints;
        	layoutControlGroup10.Text = Strings.RoomsLableText;
			layoutControlGroup10.CustomizationFormText = Strings.RoomsLableText;
        	p2pRoomAudioVideoSettingsListControl.useDefaultColumnVisibility = false;
        	p2pRoomAudioVideoSettingsListControl.BridgeNameColumnVisibility = false;
        }

		EventBroker _broker;
		public void SetEventBroker(EventBroker broker)
		{
			_broker = broker;
			p2pRoomAudioVideoSettingsListControl.SetEventBroker(_broker);//roomAudioVideoSettingsListControl
		}

        internal ReadOnlyCollection<RoomEndpoint> Endpoints
        {
            get
            {
                return new ReadOnlyCollection<RoomEndpoint>(_roomEndpoints);
            }
        }

        protected override void OnReadOnlyChanged(ReadOnlyChangedEventArgs e)
        {
            p2pRoomAudioVideoSettingsListControl.Enabled = !e.ReadOnly;
			if (_broker != null)
				_broker.OnEvent("event_nonOutlookPropChanged", null); //?????
        }

        private void ConferenceBindingSource_DataSourceChanged(object sender, EventArgs e)
        {
            Conference.LocationIds.ListChanged += LocationIds_ListChanged;
            ResetRoomEndpoints();
        }

        private void ResetRoomEndpoints()
        {
            _roomEndpoints.Clear();
            for (int i = 0; i < Conference.LocationIds.Count; i++)
            {
                var roomId = Conference.LocationIds[i];
                AddRoomEndpoint(i, roomId);
            }
        }

        void LocationIds_ListChanged(object sender, ListChangedEventArgs e)
        {
            switch(e.ListChangedType)
            {
                case ListChangedType.Reset:
                {
                    ResetRoomEndpoints();
                    break;
                }
                case ListChangedType.ItemAdded:
                {
                    var roomId = Conference.LocationIds[e.NewIndex];
                    AddRoomEndpoint(e.NewIndex, roomId);
					if (_broker != null)
						_broker.OnEvent("event_nonOutlookPropChanged", null); 
                    break;
                }
                case ListChangedType.ItemDeleted:
                {
                    if (e.NewIndex >= 0 && _roomEndpoints.Count > 0)
                    {
                        var roomEndpoint = _roomEndpoints[e.NewIndex];
                        Conference.RoomEndpoints.Remove(roomEndpoint.ConferenceEndpoint);
                        _roomEndpoints.RemoveAt(e.NewIndex);
						if (_broker != null)
							_broker.OnEvent("event_nonOutlookPropChanged", null); 
                    }
                    break;
                }
                case ListChangedType.ItemMoved:
                {
                    var roomEndpoint = _roomEndpoints[e.OldIndex];
                    _roomEndpoints.Move(roomEndpoint, e.NewIndex);
                    break;
                }
                case ListChangedType.ItemChanged:
					if (_broker != null)
						_broker.OnEvent("event_nonOutlookPropChanged", null); 
                    break;
            }
            // Always set first endpoint to caller
			if (_roomEndpoints.Count > 0) 
            {
				if (_roomEndpoints.Count >= 2 && _roomEndpoints[0].Caller != ConferenceEndpointCallMode.Caller &&
						_roomEndpoints[1].Caller != ConferenceEndpointCallMode.Caller || _roomEndpoints.Count == 1)
					_roomEndpoints[0].Caller = ConferenceEndpointCallMode.Caller;
            }
        }

        private void AddRoomEndpoint(int index, RoomId roomId)
        {
            if (roomId == null) return;
            Room roomDetails = MyVrmAddin.Instance.GetRoomFromCache(roomId);
            Endpoint endpoint = null;

			ConferenceEndpoint conferenceEndpoint = null ;
        	foreach (ConferenceEndpoint confEndp in Conference.Conference.AdvancedAudioVideoSettings.Endpoints)
        	{
				if (confEndp.Id == roomId)
				{
					conferenceEndpoint = confEndp;
					break;
				}
        	}
			
            if ((roomDetails.Media == MediaType.AudioVideo || roomDetails.Media == MediaType.AudioOnly) 
                && roomDetails.EndpointId != null)
            {
                endpoint = MyVrmService.Service.GetEndpoint(roomDetails.EndpointId);
            }

            var roomEndpoint = new RoomEndpoint(roomDetails, conferenceEndpoint, endpoint);
            _roomEndpoints.Insert(index, roomEndpoint);
        }

		protected override void OnApplying(CancelEventArgs e)
		{
			p2pRoomAudioVideoSettingsListControl.CloseEditor();
			base.OnApplying(e);
			if (e.Cancel || Conference == null)
			{
				return;
			}

			foreach (var roomEndpoint in _roomEndpoints)
			{
				//Find target
				var endpoint = Conference.RoomEndpoints.FirstOrDefault(endp => endp.Id == roomEndpoint.Id);
				if (endpoint == null) //Extra, unreal situation
				{
					throw new MyVrmUnexpectedWrongDataException();
				}

				endpoint.UseDefault = false; 
				//Set bridge
				if (roomEndpoint.BridgeName == Strings.UseDefault)
				{
					endpoint.UseDefault = true;
				}
				else
				{
					if (p2pRoomAudioVideoSettingsListControl.Bridges != null)
					{
						foreach (var brigeName in
							p2pRoomAudioVideoSettingsListControl.Bridges.Where(brigeName => brigeName.Name == roomEndpoint.BridgeName))
						{
							endpoint.BridgeId = brigeName.Id;
							break;
						}
					}
				}

				//Set Profile
				if (roomEndpoint.ProfileId == EndpointProfileId.Default)
				{
					endpoint.ProfileId = roomEndpoint.Endpoint.Profiles.DefaultProfile.Id;
				}
				else
				{
					EndpointProfile setProf = roomEndpoint.Endpoint.Profiles.FirstOrDefault(prof => prof.Id == endpoint.ProfileId);
					if (setProf != null)
					{
						setProf.BridgeId = endpoint.BridgeId;
					}
					roomEndpoint.Endpoint.Save();
				}
			}
		}
    }
}