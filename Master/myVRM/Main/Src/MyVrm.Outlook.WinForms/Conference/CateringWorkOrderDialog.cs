﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook.WinForms.Conference
{
    public partial class CateringWorkOrderDialog : Dialog
    {
        private class WorkOrderMenuRecord
        {
            private readonly CateringWorkOrderMenu _workOrderMenu;
            private readonly CateringProviderMenu _providerMenu;
            private uint _quantity;

            internal WorkOrderMenuRecord(CateringWorkOrderMenu workOrderMenu, CateringProviderMenu providerMenu)
            {
                _workOrderMenu = workOrderMenu;
                _workOrderMenu.Price = providerMenu.Price;
                _providerMenu = providerMenu;
                _quantity = _workOrderMenu.Quantity;
            }

            public int MenuId
            {
                get
                {
                    return _providerMenu.Id;
                }
            }

            public string Name
            {
                get
                {
                    return _providerMenu.Name;
                }
            }

            public uint Quantity
            {
                get
                {
                    return _quantity;   
                }
                set
                {
                    _quantity = value;
                }
            }

            internal CateringWorkOrderMenu WorkOrderMenu
            {
                get
                {
                    return _workOrderMenu;
                }
            }
        }

        public CateringWorkOrderDialog()
        {
            WorkOrderMenuRecords = new List<WorkOrderMenuRecord>();
            InitializeComponent();
        	layoutControlItem1.Text = Strings.RoomLableText;
			layoutControlItem1.CustomizationFormText = Strings.RoomLableText;
			layoutControlItem2.Text = Strings.ServiceTypeLableText;
			layoutControlItem3.Text = Strings.MenuItemsLableText;
			layoutControlItem4.Text = Strings.CommentsLableText;
			layoutControlItem5.Text = Strings.DeliverByLableText;
			layoutControlItem2.CustomizationFormText = Strings.ServiceTypeLableText;
			layoutControlItem3.CustomizationFormText = Strings.MenuItemsLableText;
			layoutControlItem4.CustomizationFormText = Strings.CommentsLableText;
			layoutControlItem5.CustomizationFormText = Strings.DeliverByLableText;
        	layoutControlItem7.CustomizationFormText = Strings.WorkOrderTimeLableText;
			layoutControlItem7.Text = Strings.WorkOrderTimeLableText;
			Text = Strings.CateringWorkOrderLableText;
        	serviceTypeEdit.Properties.NullText = Strings.SelectOneText;
			menuNameColumn.Caption = Strings.NameColumnText;
			menuQuantityColumn.Caption = Strings.QuantityColumnText;
			ApplyEnabled = false;
			ApplyVisible = false;
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
         Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        internal CateringWorkOrder WorkOrder { get; set; }

        private List<WorkOrderMenuRecord> WorkOrderMenuRecords { get; set; }

        private void CateringWorkOrderDialog_Load(object sender, EventArgs e)
        {
            var services = MyVrmService.Service.GetCateringServices();
            //ZD 103364 start
            foreach (var service in services)
            {
                
                if (service.Name == "Breakfast")
                    service.Name = Strings.Breakfast;
                if (service.Name == "Lunch")
                    service.Name = Strings.Lunch;
                if (service.Name == "Dinner")
                    service.Name =Strings.Dinner;
                if (service.Name == "Snack")
                    service.Name = Strings.Snack;
                if (service.Name == "Coffee Service")
                    service.Name = Strings.CoffeeService;
                if (service.Name == "Vegetarian")
                    service.Name = Strings.Vegetarian;
                if (service.Name == "Kosher")
                    service.Name = Strings.Kosher;
                if (service.Name == "Low-Cal.")
                    service.Name = Strings.Low_Cal;
                if (service.Name == "Halal")
                    service.Name = Strings.Halal;

                serviceTypeEdit.Properties.Items.Add(service);
            }
            //ZD 103364 End
            //foreach (var service in services)
            //{
            //    string serviceitems = "";

            //    if (service.Name == "Breakfast")
            //        serviceitems = Strings.Breakfast;
            //    if (service.Name == "Lunch")
            //        serviceitems = Strings.Lunch;
            //    if (service.Name == "Dinner")
            //        serviceitems = Strings.Dinner;
            //    if (service.Name == "Snack")
            //        serviceitems = Strings.Snack;
            //    if (service.Name == "Coffee Service")
            //        serviceitems = Strings.CoffeeService;
            //    if (service.Name == "Vegetarian")
            //        serviceitems = Strings.Vegetarian;
            //    if (service.Name == "Kosher")
            //        serviceitems = Strings.Kosher;
            //    if (service.Name == "Low-Cal.")
            //        serviceitems = Strings.Low_Cal;
            //    if (service.Name == "Halal")
            //        serviceitems = Strings.Halal;

            //    serviceTypeEdit.Properties.Items.Add(serviceitems);
                
            //}
            //if (serviceTypeEdit.Properties.Items[0].ToString() == "Breakfast")
            //    serviceTypeEdit.Properties.a

            //if (serviceTypeEdit.Properties.Items[1].ToString() == "Lunch")
            //    serviceTypeEdit.Properties.Items[1].ToString() = Strings.Lunch;
            //if (serviceTypeEdit.Properties.Items[2].ToString() == "Dinner")
            //    serviceTypeEdit.Properties.Items[2].ToString() = Strings.Dinner;
            //if (serviceTypeEdit.Properties.Items[3].ToString() == "Snack")
            //    serviceTypeEdit.Properties.Items[3].ToString() = Strings.Snack;
            //if (serviceTypeEdit.Properties.Items[4].ToString() == "Coffee Service")
            //    serviceTypeEdit.Properties.Items[4].ToString() = Strings.CoffeeService;
            //if (serviceTypeEdit.Properties.Items[5].ToString() == "Vegetarian")
            //    serviceTypeEdit.Properties.Items[5].ToString() = Strings.Vegetarian;
            //if (serviceTypeEdit.Properties.Items[6].ToString() == "Kosher")
            //    serviceTypeEdit.Properties.Items[6].ToString() = Strings.Kosher;
            //if (serviceTypeEdit.Properties.Items[7].ToString() == "Low-Cal.")
            //    serviceTypeEdit.Properties.Items[7].ToString() = Strings.Low_Cal;
            //if (serviceTypeEdit.Properties.Items[8].ToString() == "Halal")
            //    serviceTypeEdit.Properties.Items[8].ToString() = Strings.Halal;
            //if (serviceTypeEdit.Properties.Items[0].ToString() == "")
            //    serviceTypeEdit.Properties.Items[0].ToString() = Strings.Breakfast;
            //if (serviceTypeEdit.Properties.Items[0].ToString() == "")
            //    serviceTypeEdit.Properties.Items[0].ToString() = Strings.Breakfast;

            CateringService catService = services.FirstOrDefault(service => service.Id == WorkOrder.CateringServiceId);
            bool bAssigned = false;
            if (WorkOrder.Menus.Count > 0)
            {
                if (catService != null)
                {
                    serviceTypeEdit.SelectedItem = catService;
                    bAssigned = true;
                }
                else
                {
                    if (services.Count == 1)
                    {
                        serviceTypeEdit.SelectedIndex = 0;
                        serviceTypeEdit.Refresh();
                    }
                }
            }
            if (!bAssigned)
            {
                if (services.Count == 1)
                {
                    serviceTypeEdit.SelectedIndex = 0;
                    serviceTypeEdit.Refresh();
                }
            }

            roomNameLabel.Text = MyVrmAddin.Instance.GetRoomFromCache(WorkOrder.RoomId).Name;
            commentsEdit.DataBindings.Add("EditValue", WorkOrder, "Comments");
            workOrderDateEdit.DataBindings.Add("EditValue", WorkOrder, "DeliveryTime", true,
                                               DataSourceUpdateMode.OnPropertyChanged);
            workOrderTimeEdit.DataBindings.Add("EditValue", WorkOrder, "DeliveryTime", true,
                                               DataSourceUpdateMode.OnPropertyChanged);
        }

        private void serviceTypeEdit_SelectedIndexChanged(object sender, EventArgs e)
        {
            var cursor = Cursor;
            Cursor = Cursors.WaitCursor;
            try
            {
                var service = serviceTypeEdit.SelectedItem as CateringService; //ZD 103364
                menusList.Nodes.Clear();
                if (service != null)
                {
                    var providerMenus = MyVrmService.Service.SearchProviderMenus(WorkOrder.RoomId, service.Id);
                    var query = from providerMenu in providerMenus
                                join workOrderMenu in WorkOrder.Menus on providerMenu.Id equals workOrderMenu.ProviderMenuId
                                    into
                                    menuItems
                                from menuItem in
                                    menuItems.DefaultIfEmpty(new CateringWorkOrderMenu() {ProviderMenuId = providerMenu.Id})
                                select new WorkOrderMenuRecord(menuItem, providerMenu);
                    WorkOrderMenuRecords = query.ToList();
                    menusList.DataSource = WorkOrderMenuRecords;
                }
            }
            finally
            {
                Cursor = cursor;
            }
        }

        private void quantityRepositoryItemSpinEdit_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
			//var newValue = Convert.ToInt32(e.NewValue);)
			int newValue = 0;
			Int32.TryParse(e.NewValue.ToString(), out newValue);
            if (newValue < 0)
            {
                e.Cancel = true;
            }
        }

        private void CateringWorkOrderDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult != DialogResult.OK) return;
            WorkOrderMenuRecords.ForEach(rec => rec.WorkOrderMenu.Quantity = rec.Quantity);
            var workOrderMenus = from record in WorkOrderMenuRecords
                                 where record.Quantity > 0
                                 select record.WorkOrderMenu;
            if (serviceTypeEdit.SelectedItem == null)
            {
                ShowMessage(Strings.SelectCateringServiceType);
                e.Cancel = true;
                return;
            }
            if (workOrderMenus.Count() == 0)
            {
                ShowMessage(Strings.SpecifyWorkOrderItemRequestQuantity);
                e.Cancel = true;
                return;
            }
            WorkOrder.CateringServiceId = ((CateringService) serviceTypeEdit.SelectedItem).Id;

            decimal totalCost = 0;
            

            foreach (CateringWorkOrderMenu menu in workOrderMenus)
            {
                

                totalCost += menu.Quantity * menu.Price;
               

            }
            WorkOrder.Menus.Clear();
            WorkOrder.Menus.AddRange(workOrderMenus);
            WorkOrder.Price = totalCost;
        }
    }
}
