﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Linq;
using DevExpress.XtraTab;
using MyVrm.Common.EventBroker;
using MyVrm.WebServices.Data;
using System.Windows.Forms;

namespace MyVrm.Outlook.WinForms.Conference
{
    public partial class ConferenceControl : ConferenceControlBase
    {
        private readonly ConferenceResourcesPage _resourcesPage;
        //private readonly PointToPointConferenceAVSettingsPage _pointToPointConferenceAVSettingsPage;
        private readonly AudioVideoConferenceAVSettingsPage _audioVideoConferenceAVSettingsPage;
        private readonly InventoryWorkOrdersPage _inventoryWorkOrdersPage;
        private readonly CateringWorkOrdersPage _cateringWorkOrdersPage;
        private readonly HousekeepingWorkOrdersPage _housekeepingWorkOrdersPage;
		private readonly AudioAddInPage _audioAddIn;
        private readonly FilesPage _filesPage;
        private readonly AdditionalOptionsPage _additionalOptionsPage;

		EventBroker _broker = new EventBroker();
		public void SetEventBroker(EventBroker broker)
		{
			_broker = broker;
			_broker.Register("event_nonOutlookPropChanged", new MethodInvoker(OnBroker));
			_audioVideoConferenceAVSettingsPage.SetEventBroker(_broker);
			//_pointToPointConferenceAVSettingsPage.SetEventBroker(_broker);
		}

		private void OnBroker()
		{
			if (Conference != null)
				Conference.Appointment.SetNonOutlookProperty(true);
		}

        public ConferenceControl()
        {
            InitializeComponent();

            _resourcesPage = new ConferenceResourcesPage();
            //_pointToPointConferenceAVSettingsPage = new PointToPointConferenceAVSettingsPage();
            _audioVideoConferenceAVSettingsPage = new AudioVideoConferenceAVSettingsPage();
            _inventoryWorkOrdersPage = new InventoryWorkOrdersPage();
            _cateringWorkOrdersPage = new CateringWorkOrdersPage();
            _housekeepingWorkOrdersPage = new HousekeepingWorkOrdersPage();
			_audioAddIn = new AudioAddInPage();
            _filesPage= new FilesPage();
			//For York version - hide "Files" page
			if (MyVrmAddin.BuildType == "03")
				_filesPage.Visible = false;
            _additionalOptionsPage = new AdditionalOptionsPage();
        	SetEventBroker(_broker);
        }

		public override void SelectedPageChanging(object sender, TabPageChangingEventArgs e)
		{
			if (e.PrevPage != null && e.PrevPage.TabIndex == _resourcesPage.TabIndex && Conference.Type == ConferenceType.PointToPoint)
			{
				if (Conference.LocationIds.Count != 2)
				{
					UIHelper.ShowError(Strings.Point2PointConferenceMaxRoomCountReached);
					e.Cancel = true;
				}
			}
		}

        protected override void ResetPages()
        {
            if (Pages.Count == 0)
            {
                AddPage(_resourcesPage);
            }

            Pages.Where(page => page != _resourcesPage).ToList().ForEach(RemovePage);
            /*var preference = MyVrmService.Service.OrganizationSettings.Preference;

            switch(Conference.Type)
            {
                case ConferenceType.AudioOnly:
            	{
					_audioVideoConferenceAVSettingsPage.SetPageType(AudioVideoConferenceAVSettingsPage.AVPageType.AudioVideo);
					if (MyVrmService.Service.IsAVEnabled)//preference.IsAVTabEnabled)
						AddPage(_audioVideoConferenceAVSettingsPage);
                    if (preference.IsFacilitiesEnabled)
                        AddPage(_inventoryWorkOrdersPage);
                    if (preference.IsCateringEnabled)
                        AddPage(_cateringWorkOrdersPage);
                    if (preference.IsHouseKeepingEnabled)
                        AddPage(_housekeepingWorkOrdersPage);
                    AddPage(_audioAddIn);
					//For York version - hide "Files" page
					if (MyVrmAddin.BuildType != "03")
						AddPage(_filesPage);
                    AddPage(_additionalOptionsPage);
                    break;
                }
                case ConferenceType.AudioVideo:
				case ConferenceType.VMR:
                {
					_audioVideoConferenceAVSettingsPage.SetPageType(AudioVideoConferenceAVSettingsPage.AVPageType.AudioVideo);
					if (MyVrmService.Service.IsAVEnabled)//preference.IsAVTabEnabled)
						AddPage(_audioVideoConferenceAVSettingsPage);
                    if (preference.IsFacilitiesEnabled)
                        AddPage(_inventoryWorkOrdersPage);
                    if (preference.IsCateringEnabled)
                        AddPage(_cateringWorkOrdersPage);
                    if (preference.IsHouseKeepingEnabled)
                        AddPage(_housekeepingWorkOrdersPage);
                    AddPage(_audioAddIn);
					//For York version - hide "Files" page
					if (MyVrmAddin.BuildType != "03")
						AddPage(_filesPage);
                    AddPage(_additionalOptionsPage); 
                    break;
                }
                case ConferenceType.PointToPoint:
        		{
        			_audioVideoConferenceAVSettingsPage.SetPageType(AudioVideoConferenceAVSettingsPage.AVPageType.PointToPoint);
					if (MyVrmService.Service.IsAVEnabled)//preference.IsAVTabEnabled) 
						AddPage(_audioVideoConferenceAVSettingsPage);
                    //AddPage(_pointToPointConferenceAVSettingsPage);
                    if (preference.IsFacilitiesEnabled)
                        AddPage(_inventoryWorkOrdersPage);
                    if (preference.IsCateringEnabled)
                        AddPage(_cateringWorkOrdersPage);
                    if (preference.IsHouseKeepingEnabled)
                        AddPage(_housekeepingWorkOrdersPage);
                    AddPage(_audioAddIn);
					//For York version - hide "Files" page
					if (MyVrmAddin.BuildType != "03")
						AddPage(_filesPage);
                    AddPage(_additionalOptionsPage); 
                    break;
                }
                case ConferenceType.RoomConference:
                {
                    if (preference.IsFacilitiesEnabled)
                        AddPage(_inventoryWorkOrdersPage);
                    if (preference.IsCateringEnabled)
                        AddPage(_cateringWorkOrdersPage);
                    if (preference.IsHouseKeepingEnabled)
                        AddPage(_housekeepingWorkOrdersPage);
					//For York version - hide "Files" page
					if (MyVrmAddin.BuildType != "03")
						AddPage(_filesPage);
                    AddPage(_additionalOptionsPage);
                    break;
                }
                default:
                    throw new ArgumentOutOfRangeException();
            }*/
        }
		protected override void ConferenceExternalUpdate(object sender, EventArgs e)
		{
			_resourcesPage.ConferenceBindingSource.ResetBindings(false);
			base.ConferenceExternalUpdate(sender, e);
		}
    }
}
