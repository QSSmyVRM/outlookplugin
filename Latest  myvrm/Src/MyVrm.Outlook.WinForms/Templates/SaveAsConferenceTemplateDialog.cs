﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Windows.Forms;
using MyVrm.Outlook.WinForms.Conference;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook.WinForms.Templates
{
	public partial class SaveAsConferenceTemplateDialog : MyVrm.Outlook.WinForms.Dialog
	{
		private const int ErrCode_SameNameExists = 222;
		public string TemplateName { private set; get; }
		public string TemplateDescr { private set; get; }
		public bool IsPublic { private set; get; }
		public bool IsDefault { private set; get; }
		private ConferenceWrapper _conferenceWrapper { set; get; }
        private ConferenceResourcesPage _conferenceName { set; get; }
      

		public SaveAsConferenceTemplateDialog(ConferenceWrapper conferenceWrapper)
		{
			InitializeComponent();

			ApplyEnabled = false;
			ApplyVisible = false;

			//Texts localization
			Text = Strings.EditTemplate;
			labelControl1.Text = Strings.Name;
            labelControl2.Text = Strings.Description;
			checkIsPublic.Properties.Caption = Strings.Public;
			checkIsDefault.Properties.Caption = Strings.SetAsDefault;
			labelControl3.Text = Strings.PublicTemplate;

			TemplateName = string.Empty;
			TemplateDescr = string.Empty;
			IsPublic = false;
			IsDefault = false;
			_conferenceWrapper = conferenceWrapper;
			editTemplateName.Select();
		}

		public override sealed string Text
		{
			get { return base.Text; }
			set { base.Text = value; }
		}

		void SaveAsConferenceTemplateDialog_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
		{
			if (DialogResult == System.Windows.Forms.DialogResult.OK)
			{
				TemplateName = editTemplateName.Text;
				TemplateDescr = memoTemplateDescr.Text;
				IsPublic = checkIsPublic.Checked;
				IsDefault = checkIsDefault.Checked;

                ConferenceResourcesPage resourcesPage = new ConferenceResourcesPage();

                if (TemplateName.Trim().Length == 0 )
				{
					UIHelper.ShowMessage(Strings.DefineTemplateName, MessageBoxButtons.OK,
					                     MessageBoxIcon.Exclamation);
					e.Cancel = true;
					editTemplateName.Select();
				}


				if(e.Cancel == false)
				{
					var cursor = Cursor.Current;
					Cursor.Current = Cursors.WaitCursor;

					try
					{
                        if (_conferenceWrapper.Conference.Name == null || _conferenceWrapper.Conference.Name == "") //ConferenceNameRequired
                        {
                            UIHelper.ShowMessage(Strings.ConferenceNameRequired, MessageBoxButtons.OK,
                                                 MessageBoxIcon.Exclamation);
                            e.Cancel = true;
                            editTemplateName.Select();
                        }
                        if (e.Cancel == false)
                        {
                            var cursora = Cursor.Current;
                            Cursor.Current = Cursors.WaitCursor;

                            MyVrmService.Service.ConferenceSetTemplate(_conferenceWrapper.Conference, TemplateName,
                                                                            TemplateDescr, IsPublic, IsDefault);
                        }
					}
					catch(MyVrmServiceException exception)
					{
						if (exception.ErrorCode == ErrCode_SameNameExists) //Template with a such name is already exists
						{
							UIHelper.ShowMessage(Strings.TemplateAlreadyExists, MessageBoxButtons.OK,
										 MessageBoxIcon.Exclamation);
							e.Cancel = true;
							editTemplateName.Select();
						}
						else
						{
							MyVrmAddin.TraceSource.TraceException(exception, true);
							UIHelper.ShowMessage(exception.Message, MessageBoxButtons.OK,
										 MessageBoxIcon.Exclamation);
							e.Cancel = true;
						}
					}
					catch (Exception exception)
					{
						MyVrmAddin.TraceSource.TraceException(exception, true);
						UIHelper.ShowMessage(exception.Message, MessageBoxButtons.OK,
										 MessageBoxIcon.Exclamation);
						e.Cancel = true;
					}
					finally
					{
						Cursor.Current = cursor;
					}
				}
			}
		}
	}
}
