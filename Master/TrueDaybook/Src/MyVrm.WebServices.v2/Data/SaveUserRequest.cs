﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;

namespace MyVrm.WebServices.Data
{
    internal class SaveUserRequest : ServiceRequestBase<SaveUserResponse>
    {
        internal SaveUserRequest(MyVrmService service) : base(service)
        {
        }

        public User User { get; set; }

        #region Overrides of ServiceRequestBase

        internal override string GetXmlElementName()
        {
            return "saveUser";
        }

        internal override string GetCommandName()
        {
            return "SetUser";
        }

        internal override string GetResponseXmlElementName()
        {
            return "SetUser";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            writer.WriteStartElement(XmlNamespace.NotSpecified, "login");
            UserId.WriteToXml(writer);
            OrganizationId.WriteToXml(writer);
            writer.WriteEndElement();
            User.WriteToXml(writer);
        }

        #endregion
    }
}
