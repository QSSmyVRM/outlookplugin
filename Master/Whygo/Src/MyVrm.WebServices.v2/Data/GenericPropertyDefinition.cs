﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;

namespace MyVrm.WebServices.Data
{
    internal sealed class GenericPropertyDefinition<T> : TypedPropertyDefinition
    {
        internal GenericPropertyDefinition(string xmlElementName) : base(xmlElementName)
        {
            PropertyType = typeof (T);
        }

        internal GenericPropertyDefinition(string xmlElementName, PropertyDefinitionFlags flags)
            : base(xmlElementName, flags)
        {
            PropertyType = typeof(T);
        }

        #region Overrides of TypedPropertyDefinition

        internal override object Parse(string value)
        {
            return Utilities.Parse<T>(value);
        }

        internal override string ToString(object value)
        {
            if (value is Enum)
            {
                return Utilities.SerializeEnum((Enum)value);
            }
            return base.ToString(value);
        }
        #endregion
    }
}
