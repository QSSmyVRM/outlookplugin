﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    /// <summary>
    /// Represents a base class for work orders.
    /// </summary>
    public abstract class WorkOrderBase : ServiceObject
    {
        protected WorkOrderBase(MyVrmService service) : base(service)
        {
        }

        public WorkOrderId Id
        {
            get
            {
                return (WorkOrderId)PropertyBag[WorkOrderBaseSchema.Id];
            }
        }

        public string Name
        {
            get
            {
                return (string)PropertyBag[WorkOrderBaseSchema.Name];
            }
            set
            {
                PropertyBag[WorkOrderBaseSchema.Name] = value;
            }
        }

        public RoomId RoomId
        {
            get
            {
                return (RoomId)PropertyBag[WorkOrderBaseSchema.RoomId];
            }
            set
            {
                PropertyBag[WorkOrderBaseSchema.RoomId] = value;
            }
        }

        internal override PropertyDefinition GetIdPropertyDefinition()
        {
            return WorkOrderBaseSchema.Id;
        }
    }
}
