﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;

namespace MyVrm.WebServices.Data
{
	class GetPublicCountriesRequest: ServiceRequestBase<GetPublicCountriesResponse>
	{
		internal GetPublicCountriesRequest(MyVrmService service)
			: base(service)
		{
		}

		#region Overrides of ServiceRequestBase

		internal override string GetXmlElementName()
		{
			return "GetPublicCountries";
		}

		internal override string GetCommandName()
		{
			return Constants.GetPublicCountriesCommandName;
		}

		internal override string GetResponseXmlElementName()
		{
			return "GetPublicCountries";
		}

		internal override GetPublicCountriesResponse ParseResponse(MyVrmServiceXmlReader reader)
		{
			var response = new GetPublicCountriesResponse(); //{ Room = new Room(Service) };
			response.LoadFromXml(reader, "GetPublicCountries");// GetResponseXmlElementName());
			return response;
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			UserId.WriteToXml(writer, "userID");
		}

		#endregion
	}

	public class GetPublicCountriesResponse : ServiceResponse
	{
		public Dictionary<int, string> Countries { get; internal set; }

		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{
			Countries = new Dictionary<int, string>();
			int id = 0;
			string name = string.Empty;
			while (!reader.IsEndElement(XmlNamespace.NotSpecified, "GetPublicCountries"))
			{
				switch (reader.LocalName)
				{
					case "ID":
						id = reader.ReadElementValue<int>();
						break;
					case "Name":
						name = reader.ReadElementValue<string>();
						break;
				}
				if (reader.IsEndElement(XmlNamespace.NotSpecified, "Country"))
				{
					if (id > 0 && !string.IsNullOrEmpty(name))
					{
						Countries.Add(id, name);
						id = 0;
						name = string.Empty;
					}
				}
				reader.Read();
			}
		}
	}
}