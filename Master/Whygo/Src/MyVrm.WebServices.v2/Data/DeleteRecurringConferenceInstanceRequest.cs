﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;

namespace MyVrm.WebServices.Data
{
    internal class DeleteRecurringConferenceInstanceRequest : ServiceRequestBase<DeleteConferenceResponse>
    {
        internal DeleteRecurringConferenceInstanceRequest(MyVrmService service) : base(service)
        {
        }

        internal ConferenceId ConferenceId { get; set; }
        internal DateTime InstanceDate { get; set; }

        #region Overrides of ServiceRequestBase<DeleteConferenceResponse>

        internal override string GetXmlElementName()
        {
            return "login";
        }

        internal override string GetCommandName()
        {
            return "DeleteRecurInstance";
        }

        internal override string GetResponseXmlElementName()
        {
            return "conferences";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            UserId.WriteToXml(writer, "userid");
            writer.WriteStartElement(XmlNamespace.NotSpecified, "deleterecurinstance");
            ConferenceId.WriteToXml(writer, "confid");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "reason", null);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "confdate", Utilities.DateToString(InstanceDate));
            writer.WriteEndElement();
        }

        #endregion
    }
}
