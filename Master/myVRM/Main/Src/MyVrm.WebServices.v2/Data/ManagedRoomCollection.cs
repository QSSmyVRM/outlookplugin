﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    internal class ManagedRoomCollection : BaseCollection<ManagedRoom>
    {
        #region Overrides of BaseCollection<ManagedRoom>

        internal override void LoadFromXml(MyVrmServiceXmlReader reader)
        {
            do
            {
                reader.Read();
                if (reader.LocalName == "level1")
                {
                    var room = new ManagedRoom();
                    room.LoadFromXml(reader, "level1");
                    Items.Add(room);
                }
                else
                {
                    reader.SkipCurrentElement();
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "level1List"));
        }

        #endregion
    }
}
