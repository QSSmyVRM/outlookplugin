﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    internal class StringPropertyDefinition : TypedPropertyDefinition
    {
        internal StringPropertyDefinition(string xmlElementName) : base(xmlElementName)
        {
            PropertyType = typeof(string);
        }

        internal StringPropertyDefinition(string xmlElementName, PropertyDefinitionFlags flags)
            : base(xmlElementName, flags)
        {
            PropertyType = typeof(string);
        }

        internal StringPropertyDefinition(string xmlElementName, string xmlElementForWrite, PropertyDefinitionFlags flags)
            : base(xmlElementName, xmlElementForWrite, flags)
        {
            PropertyType = typeof(string);
        }

        #region Overrides of TypedPropertyDefinition

        internal override object Parse(string value)
        {
            return value;
        }

        internal override bool IsNullable
        {
            get
            {
                return true;
            }
        }
        #endregion
    }
}
