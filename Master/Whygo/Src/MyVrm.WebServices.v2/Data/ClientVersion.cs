/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Globalization;

namespace MyVrm.WebServices.Data
{
    /// <summary>
    /// Represents myVRM client version.
    /// </summary>
    public sealed class ClientVersion : IEquatable<ClientVersion>, IFormattable
    {
        public ClientVersion(int majorVersion, int minorVersion, int revision)
        {
            MajorVersion = majorVersion;
            MinorVersion = minorVersion;
            Revision = revision;
        }

        public static ClientVersion Parse(string version)
        {
            if (version == null) 
                throw new ArgumentNullException("version");
            var parts = version.Split(new[] {'.'});
            if (parts.Length < 3)
            {
                throw new ClientVersionException(string.Format(Strings.InvalidClientVersionFormat, version));
            }
            int majorVersion;
            if (!int.TryParse(parts[0], NumberStyles.Integer, CultureInfo.InvariantCulture, out majorVersion) || majorVersion < 1)
            {
                throw new ClientVersionException(string.Format(Strings.InvalidClientVersionFormat, version));
            }
            int minorVersion;
            if (!int.TryParse(parts[1], NumberStyles.Integer, CultureInfo.InvariantCulture, out minorVersion) || minorVersion < 0)
            {
                throw new ClientVersionException(string.Format(Strings.InvalidClientVersionFormat, version));
            }
            int revision;
            if (!int.TryParse(parts[2], NumberStyles.Integer, CultureInfo.InvariantCulture, out revision) || revision < 0)
            {
                throw new ClientVersionException(string.Format(Strings.InvalidClientVersionFormat, version));
            }
            return new ClientVersion(majorVersion, minorVersion, revision);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (ClientVersion)) return false;
            return Equals((ClientVersion) obj);
        }

        public string ToString(string format, IFormatProvider formatProvider)
        {
            return ToString();
        }

        public bool Equals(ClientVersion other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other.MajorVersion == MajorVersion && other.MinorVersion == MinorVersion && other.Revision == Revision;
        }

        public override string ToString()
        {
            return string.Format("{0:d}.{1:d}.{2:d}", MajorVersion, MinorVersion, Revision);
        }
        /// <summary>
        /// Gets or sets the major myVRM client version.
        /// </summary>
        public int MajorVersion { get; set; }
        /// <summary>
        /// Gets or sets the minor myVRM client version.
        /// </summary>
        public int MinorVersion { get; set; }
        /// <summary>
        /// Gets or sets the revision number of myVRM client version.
        /// </summary>
        public int Revision { get; set; }

        public override int GetHashCode()
        {
            unchecked
            {
                var result = MajorVersion;
                result = (result*397) ^ MinorVersion;
                result = (result*397) ^ Revision;
                return result;
            }
        }
    }
}