﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    [ServiceObjectDefinition("Organization")]
    public class Organization : ServiceObject
    {
        internal Organization(MyVrmService service) : base(service)
        {
        }

        public OrganizationId Id
        {
            get
            {
                return (OrganizationId) PropertyBag[OrganizationSchema.Id];
            }
        }

        public string Name
        {
            get
            {
                return (string) PropertyBag[OrganizationSchema.Name];
            }
        }

        #region Overrides of ServiceObject

        internal override ServiceObjectSchema GetSchema()
        {
            return OrganizationSchema.Instance;
        }
        #endregion
    }
}
