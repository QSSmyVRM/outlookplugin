#pragma once
#include "MapiValue.h"
#include "NamedProperty.h"

namespace MyVrm
{
	namespace MAPI
	{
		using namespace System::Collections::Generic;
		public ref class MapiObject
		{
		public:
			MapiObject(void);
			MapiObject(System::Object^ mapiObject);
			virtual ~MapiObject();
			!MapiObject();

			System::Object^ GetProperty(MapiTags tag);
			System::Object^ GetProperty(NamedProperty^ namedProperty);
			array<int>^ GetIDsFromNames(array<NamedProperty^>^ namedProperties);
			int GetIDFromName(NamedProperty^ namedProperty);
			void SetProps(array<MapiValue> ^values);
			void DeleteProps(array<int> ^tags);
			void SaveChanges();
		internal:
			List<MapiValue>^ GetProps(array<int> ^tags);
			unsigned int ManagedTypeToMapiPropType(System::Type^ type);
		private:
			IMAPIProp* mapiProp;
		};
	}
}
