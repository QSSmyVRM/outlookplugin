﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System.Collections.Generic;

namespace MyVrm.WebServices.Data
{
    public class CateringWorkOrderMenuCollection : ComplexPropertyCollection<CateringWorkOrderMenu>
    {
		public override object Clone()
		{
			CateringWorkOrderMenuCollection copy = new CateringWorkOrderMenuCollection();
			foreach (var item in Items)
			{
				copy.InternalAdd((CateringWorkOrderMenu)item.Clone()); //???????
			}

			return copy;
		}

        public void Clear()
        {
            InternalClear();
        }

        public void AddRange(IEnumerable<CateringWorkOrderMenu> workOrderMenus)
        {
            InternalAddRange(workOrderMenus);
        }

        #region Overrides of ComplexPropertyCollection<CateringWorkOrderMenu>

        internal override CateringWorkOrderMenu CreateComplexProperty(string xmlElementName)
        {
            return new CateringWorkOrderMenu();
        }

        internal override string GetCollectionItemXmlElementName(CateringWorkOrderMenu complexProperty)
        {
            return "Menu";
        }

        #endregion

    }
}
