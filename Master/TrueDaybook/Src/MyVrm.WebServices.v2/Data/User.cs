﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;

namespace MyVrm.WebServices.Data
{
    /// <summary>
    /// Represents an user in myVRM.
    /// </summary>
    [ServiceObjectDefinition("user")]
    public class User : ServiceObject
    {
        public User(MyVrmService service) : base(service)
        {
        }

        public UserId Id
        {
            get
            {
                return (UserId) PropertyBag[UserSchema.UserId];
            }
            private set
            {
                PropertyBag[UserSchema.UserId] = value;
            }
        }

        public UserName UserName
        {
            get
            {
                return (UserName) PropertyBag[UserSchema.UserName];
            }
            set
            {
                PropertyBag[UserSchema.UserName] = value;
            }
        }



        public string Login
        {
            get
            {
                return (string)PropertyBag[UserSchema.Login];
            }
            set
            {
                PropertyBag[UserSchema.Login] = value;
            }
        }

        public string Password
        {
            get
            {
                return (string)PropertyBag[UserSchema.Password];
            }
            set
            {
                PropertyBag[UserSchema.Password] = value;
            }
        }

        public string Email
        {
            get
            {
                return (string)PropertyBag[UserSchema.Email];
            }
            set
            {
                PropertyBag[UserSchema.Email] = value;
            }
        }
        public LanguageInfo Language
        {
            get
            {
                return (LanguageInfo)PropertyBag[UserSchema.languageID];
            }
            set
            {
                PropertyBag[UserSchema.languageID] = value;
            }
        }
        public TimeZoneInfo TimeZone
        {
            get
            {
                return (TimeZoneInfo)PropertyBag[UserSchema.TimeZone];
            }
            set
            {
                PropertyBag[UserSchema.TimeZone] = value;
            }
        }

        public UserRoleId Role
        {
            get
            {
                return (UserRoleId)PropertyBag[UserSchema.RoleId];
            }
            set
            {
                PropertyBag[UserSchema.RoleId] = value;
            }
        }

        public BridgeId BridgeId
        {
            get
            {
                return (BridgeId)PropertyBag[UserSchema.BridgeId];
            }
            set
            {
                PropertyBag[UserSchema.BridgeId] = value;
            }
        }

        public UserStatus Status
        {
            get
            {
                return (UserStatus) PropertyBag[UserSchema.Status];
            }
        }

        public FavoriteRoomCollection FavoriteRooms
        {
            get
            {
                return (FavoriteRoomCollection)PropertyBag[UserSchema.PreferredLocations];    
            }
        }

        public int SavedSearchesId
        {
            get
            {
				int iRet = 0;
				if (PropertyBag[UserSchema.SavedSearchesId] != null)
					int.TryParse(PropertyBag[UserSchema.SavedSearchesId].ToString(), out iRet);
            	return iRet;//(int) PropertyBag[UserSchema.SavedSearchesId];
            }
            set
            {
                PropertyBag[UserSchema.SavedSearchesId] = value;    
            }
        }

        public bool AudioAddon
        {
            get
            {
				bool bRet = false;
				if (PropertyBag[UserSchema.AudioAddon] != null)
					bool.TryParse(PropertyBag[UserSchema.AudioAddon].ToString(), out bRet);
				return bRet;//(bool)PropertyBag[UserSchema.AudioAddon];
            }
            set
            {
                PropertyBag[UserSchema.AudioAddon] = value;
            }
        }

        public string ConferenceCode
        {
            get
            {
                return (string)PropertyBag[UserSchema.ConferenceCode];
            }
            set
            {
                PropertyBag[UserSchema.ConferenceCode] = value;
            }
        }

        public string LeaderPin
        {
            get
            {
                return (string)PropertyBag[UserSchema.LeaderPin];
            }
            set
            {
                PropertyBag[UserSchema.LeaderPin] = value;
            }
        }

        public string IpOrISDNAddress
        {
            get
            {
                return (string)PropertyBag[UserSchema.IpOrISDNAddress];
            }
            set
            {
                PropertyBag[UserSchema.IpOrISDNAddress] = value;
            }
        }

        public string StaticID
        {
            get
            {
                return (string)PropertyBag[UserSchema.StaticID];
            }
            set
            {
                PropertyBag[UserSchema.StaticID] = value;
            }
        }

        public int ApiPortNumber
        {
            get
            {
                int iRet = 0;
				if (PropertyBag[UserSchema.ApiPortNumber] != null)
					int.TryParse(PropertyBag[UserSchema.ApiPortNumber].ToString(), out iRet);
				return iRet; //(int)PropertyBag[UserSchema.ApiPortNumber];
            }
            set
            {
                PropertyBag[UserSchema.ApiPortNumber] = value;
            }
        }

        public bool IsStaticIDEnabled
        {
            get
            {
                bool bRet = false;
                if (PropertyBag[UserSchema.IsOutside] != null)
                    bool.TryParse(PropertyBag[UserSchema.IsStaticIDEnabled].ToString(), out bRet);
                return bRet;//(bool)PropertyBag[UserSchema.IsOutside];
            }
            set
            {
                PropertyBag[UserSchema.IsStaticIDEnabled] = value;
            }
        }

        public int VideoEquipment
        {
            get
            {
                int iRet = 0;
				if (PropertyBag[UserSchema.VideoEquipmentId] != null)
					int.TryParse(PropertyBag[UserSchema.VideoEquipmentId].ToString(), out iRet);
				return iRet; //(int)PropertyBag[UserSchema.VideoEquipmentId];
            }
            set
            {
                PropertyBag[UserSchema.VideoEquipmentId] = value;
            }
        }

        public int LineRate
        {
            get
            {
                int iRet = 0;
				if (PropertyBag[UserSchema.LineRate] != null)
					int.TryParse(PropertyBag[UserSchema.LineRate].ToString(), out iRet);
				return iRet; //(int)PropertyBag[UserSchema.LineRate];
            }
            set
            {
                PropertyBag[UserSchema.LineRate] = value;
            }
        }

        public int ConnectionType
        {
            get
            {
                int iRet = 0;
				if (PropertyBag[UserSchema.ConnectionType] != null)
					int.TryParse(PropertyBag[UserSchema.ConnectionType].ToString(), out iRet);
				return iRet; //(int)PropertyBag[UserSchema.ConnectionType];
            }
            set
            {
                PropertyBag[UserSchema.ConnectionType] = value;
            }
        }

        public bool IsOutside
        {
            get
            {
				bool bRet = false;
				if (PropertyBag[UserSchema.IsOutside] != null)
					bool.TryParse(PropertyBag[UserSchema.IsOutside].ToString(), out bRet);
				return bRet;//(bool)PropertyBag[UserSchema.IsOutside];
            }
            set
            {
                PropertyBag[UserSchema.IsOutside] = value;
            }
        }

        public AddressType AddressType
        {
            get
            {
                return (AddressType)PropertyBag[UserSchema.AddressType];
            }
            set
            {
                PropertyBag[UserSchema.AddressType] = value;
            }
        }

        /// <summary>
        /// Saves this user.
        /// </summary>
        public void Save()
        {
            Id = Service.SaveUser(this);
        }

        #region Overrides of ServiceObject

        internal override ServiceObjectSchema GetSchema()
        {
            return UserSchema.Instance;
        }

        internal override PropertyDefinition GetIdPropertyDefinition()
        {
            return UserSchema.UserId;
        }
        #endregion
    }
}
