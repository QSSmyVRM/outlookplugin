﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace MyVrm.WebServices.Data
{
    internal class GetOrganizationListResponse : ServiceResponse
    {
        private readonly List<Organization> _organizations = new List<Organization>();

        public ReadOnlyCollection<Organization> Organizations
        {
            get { return new ReadOnlyCollection<Organization>(_organizations); }
        }

        internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
        {
            _organizations.Clear();
            base.ReadElementsFromXml(reader);
            do
            {
                reader.Read();
                if (reader.IsStartElement(XmlNamespace.NotSpecified, "Organization"))
                {
                    Organization organization = new Organization(reader.Service);
                    organization.LoadFromXml(reader, false);
                    _organizations.Add(organization);
                }
                else
                {
                    reader.SkipCurrentElement();
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "GetOrganizationList"));
        }
    }
}
