﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System.Drawing;
using stdole;

namespace MyVrm.Outlook.WinForms
{
    public class AxHost : System.Windows.Forms.AxHost
    {
        public AxHost()
            : base("F244BD31-C442-45da-8C3D-D80986080DF0")
        {
        }

        public static new IPictureDisp GetIPictureDispFromPicture(Image image )
        {
            return (IPictureDisp)System.Windows.Forms.AxHost.GetIPictureDispFromPicture(image);
        }

        public static IPictureDisp GetMaskImage(Image image)
        {
            var bitmap = new Bitmap(image);
            for(int x =0; x < bitmap.Width; x++)
            {
                for(int y=0; y < bitmap.Height; y++)
                {
                    // get the pixel alpha
                    var pa = bitmap.GetPixel(x, y).A;
                    // get the grayscale alpha equivalent of the pixel alpha
                    var pg = 255 - pa;
                    bitmap.SetPixel(x, y, Color.FromArgb(pg, pg, pg));
                }
            }
            return GetIPictureDispFromPicture(bitmap);
        }
    }
}
