using System;
using System.Runtime.InteropServices;
using System.Security;

namespace MyVrm.DataImport.Excel.WinForms
{
    [SuppressUnmanagedCodeSecurity]
    internal static class UnsafeNativeMethods
    {
        [DllImport("user32.dll")]
        public static extern bool InvalidateRect(HandleRef hWnd, IntPtr rect, bool erase);
        [DllImport("User32")]
        public static extern IntPtr SendMessage(HandleRef hWnd, int msg, int wParam, uint lparam);
        [DllImport("user32.dll")]
        public static extern IntPtr SendMessage(HandleRef hWnd, int msg, IntPtr wParam, IntPtr lParam);
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr SendMessage(HandleRef hWnd, int msg, int wParam, [In, Out] ref NativeMethods.LVITEM lParam);
        [DllImport("user32.dll")]
        public static extern IntPtr SendMessage(HandleRef hWnd, int msg, IntPtr wParam, [In, Out] ref NativeMethods.HDITEM lparam);
    }
}