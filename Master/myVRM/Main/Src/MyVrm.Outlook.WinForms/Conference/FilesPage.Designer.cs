﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.Outlook.WinForms.Conference
{
    partial class FilesPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
			if (disposing && ConferenceBindingSource != null)
				ConferenceBindingSource.DataSourceChanged -= ConferenceBindingSource_DataSourceChanged;

            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FilesPage));
            this.filesList = new DevExpress.XtraTreeList.TreeList();
            this.fileNameColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.uploadedColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.uploadedTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.addFileBarButton = new DevExpress.XtraBars.BarButtonItem();
            this.uploadBarButton = new DevExpress.XtraBars.BarButtonItem();
            this.barDelfilesEntry = new DevExpress.XtraBars.BarButtonItem(); //102223
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.filesList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uploadedTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            this.SuspendLayout();
            // 
            // filesList
            // 
            this.filesList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.fileNameColumn,
            this.uploadedColumn});
            this.filesList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.filesList.Location = new System.Drawing.Point(0, 30);
            this.filesList.Name = "filesList";
            this.filesList.OptionsView.ShowButtons = false;
            this.filesList.OptionsView.ShowHorzLines = false;
            this.filesList.OptionsView.ShowIndicator = false;
            this.filesList.OptionsView.ShowRoot = false;
            this.filesList.OptionsView.ShowVertLines = false;
            this.filesList.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.uploadedTextEdit});
            this.filesList.Size = new System.Drawing.Size(524, 242);
            this.filesList.TabIndex = 0;
            // 
            // fileNameColumn
            // 
            this.fileNameColumn.Caption = "[Name]";
            this.fileNameColumn.FieldName = "Name";
            this.fileNameColumn.Name = "fileNameColumn";
            this.fileNameColumn.OptionsColumn.AllowEdit = false;
            this.fileNameColumn.OptionsColumn.AllowFocus = false;
            this.fileNameColumn.OptionsColumn.AllowMove = false;
            this.fileNameColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.fileNameColumn.OptionsColumn.AllowSort = false;
            this.fileNameColumn.OptionsColumn.ReadOnly = true;
            this.fileNameColumn.OptionsColumn.ShowInCustomizationForm = false;
            this.fileNameColumn.Visible = true;
            this.fileNameColumn.VisibleIndex = 0;
            // 
            // uploadedColumn
            // 
            this.uploadedColumn.Caption = "[Uploaded]";
            this.uploadedColumn.ColumnEdit = this.uploadedTextEdit;
            this.uploadedColumn.FieldName = "IsUploaded";
            this.uploadedColumn.Name = "uploadedColumn";
            this.uploadedColumn.OptionsColumn.AllowEdit = false;
            this.uploadedColumn.OptionsColumn.AllowFocus = false;
            this.uploadedColumn.OptionsColumn.AllowMove = false;
            this.uploadedColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.uploadedColumn.OptionsColumn.AllowSize = false;
            this.uploadedColumn.OptionsColumn.AllowSort = false;
            this.uploadedColumn.OptionsColumn.FixedWidth = true;
            this.uploadedColumn.OptionsColumn.ReadOnly = true;
            this.uploadedColumn.OptionsColumn.ShowInCustomizationForm = false;
            this.uploadedColumn.Visible = true;
            this.uploadedColumn.VisibleIndex = 1;
            this.uploadedColumn.Width = 100;
            // 
            // uploadedTextEdit
            // 
            this.uploadedTextEdit.AutoHeight = false;
            this.uploadedTextEdit.Name = "uploadedTextEdit";
            this.uploadedTextEdit.ReadOnly = true;
            // 
            // barManager
            // 
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Images = this.imageList;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.addFileBarButton,
            this.uploadBarButton,
            this.barDelfilesEntry}); //102223
            this.barManager.MaxItemId = 4;
            // 
            // bar1
            // 
            this.bar1.BarName = "Tools";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.addFileBarButton),
            new DevExpress.XtraBars.LinkPersistInfo(this.uploadBarButton),
            new DevExpress.XtraBars.LinkPersistInfo(this.barDelfilesEntry)}); //102223
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.RotateWhenVertical = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Tools";
            // 
            // addFileBarButton
            // 
            this.addFileBarButton.Caption = "[Add]";
            this.addFileBarButton.Id = 0;
            this.addFileBarButton.ImageIndex = 1;
            this.addFileBarButton.Name = "addFileBarButton";
            this.addFileBarButton.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.addFileBarButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.addFileBarButton_ItemClick);
            // 
            // uploadBarButton
            // 
            this.uploadBarButton.Caption = "[Upload]";
            this.uploadBarButton.Id = 1;
            this.uploadBarButton.ImageIndex = 0;
            this.uploadBarButton.Name = "uploadBarButton";
            this.uploadBarButton.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.uploadBarButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.uploadFileBarButton_ItemClick);
            //102223 start
            // 
            // barDelEntry
            // 
            this.barDelfilesEntry.Caption = "[Remove]";
            this.barDelfilesEntry.Id = 2;
            this.barDelfilesEntry.ImageIndex = 2;
            this.barDelfilesEntry.Name = "barDelEntry";
            this.barDelfilesEntry.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barDelfilesEntry.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barDelEntry_ItemClick);
            //102223 End
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(524, 30);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 272);
            this.barDockControlBottom.Size = new System.Drawing.Size(524, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 30);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 242);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(524, 30);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 242);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "Upload.PNG");
            this.imageList.Images.SetKeyName(1, "Add.PNG");
            this.imageList.Images.SetKeyName(2, "Remove.PNG");
            // 
            // openFileDialog
            // 
            this.openFileDialog.Multiselect = true;
            // 
            // FilesPage
            // 
            this.Controls.Add(this.filesList);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "FilesPage";
            this.Size = new System.Drawing.Size(524, 272);
            this.Load += new System.EventHandler(this.FilesPage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.filesList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uploadedTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTreeList.TreeList filesList;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem addFileBarButton;
        private DevExpress.XtraBars.BarButtonItem uploadBarButton;
        private DevExpress.XtraBars.BarButtonItem barDelfilesEntry;//102223
        private DevExpress.XtraTreeList.Columns.TreeListColumn fileNameColumn;
        private DevExpress.XtraTreeList.Columns.TreeListColumn uploadedColumn;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.ImageList imageList;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit uploadedTextEdit;
    }
}
