﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.WebServices.Data
{
    internal class GetOrganizationSettingsRequest : ServiceRequestBase<GetOrganizationSettingsResponse>
    {
        public GetOrganizationSettingsRequest(MyVrmService service) : base(service)
        {
        }

        #region Overrides of ServiceRequestBase<GetOrganizationSettingsResponse>

        internal override string GetXmlElementName()
        {
            return "GetOrgSettings";
        }

        internal override string GetCommandName()
        {
            return "GetOrgSettings";
        }

        internal override string GetResponseXmlElementName()
        {
            return "GetOrgSettings";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            UserId.WriteToXml(writer);
            //OrganizationId.WriteToXml(writer);
        }

        #endregion
    }
}
