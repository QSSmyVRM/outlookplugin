﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System.Collections.Generic;
using System.Diagnostics;
using MyVrm.Common.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;
using System;

namespace MyVrm.Common.Tests
{
    
    
    /// <summary>
    ///This is a test class for AdvancedThreadPoolTest and is intended
    ///to contain all AdvancedThreadPoolTest Unit Tests
    ///</summary>
    [TestClass()]
    public class AdvancedThreadPoolTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        [TestMethod]
        public void CancelTest()
        {
            var threadPool = new AdvancedThreadPool();
            var cookies = new List<WorkItem>();

            for (var iters = 10; iters <= 50; iters += 10)
            {
                Trace.WriteLine("Iteration with " + iters + " items.");
                for (var i = 0; i < iters; i++)
                {
                    var j = i;
                    cookies.Add(threadPool.QueueUserWorkItem(delegate
                                                                 {
                        Thread.Sleep(5000);
                        Trace.WriteLine(j);
                    }));
                }

                var rand = new Random();
                while (cookies.Count > 0)
                {
                    int i = rand.Next(cookies.Count);
                    WorkItem item = cookies[i];
                    cookies.RemoveAt(i);
                    Trace.WriteLine(threadPool.Cancel(item, true).ToString());
                    Thread.Sleep(250);
                }

                Trace.WriteLine("... Done Iteration ...");
            }
        }
    }
}
