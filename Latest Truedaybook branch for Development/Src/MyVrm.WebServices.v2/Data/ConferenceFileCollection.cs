﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
    public class ConferenceFileCollection : ComplexProperty, IOwnedProperty, IEnumerable<string>
    {
        private const string FileElementName = "file";
        private Conference _owner;
        private readonly List<string> _files = new List<string>();

		public override object Clone()
		{
			ConferenceFileCollection copy = new ConferenceFileCollection();
			foreach (var item in this)
			{
				copy._files.Add(string.Copy(item));
			}
			copy.Owner = Owner;

			return copy;
		}

        public ServiceObject Owner
        {
            get { return _owner; }
            set { _owner = value as Conference; }
        }

        public IEnumerator<string> GetEnumerator()
        {
            return _files.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(string path)
        {
            _files.Add(path);
            Owner.Changed();
        }

        public void AddRange(IEnumerable<string> paths)
        {
            _files.AddRange(paths);
            Owner.Changed();
        }

        public void Remove(string path)
        {
            _files.Remove(path);
            Owner.Changed();
        }

        public void Clear()
        {
            _files.Clear();
            Owner.Changed();
        }

        internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
        {
            if (reader.LocalName == FileElementName)
            {
                _files.Add(reader.ReadElementValue());
                return true;
            }
            return false;
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            foreach (var file in _files)
            {
                writer.WriteElementValue(XmlNamespace.NotSpecified, FileElementName, file);
            }
        }
    }
}
