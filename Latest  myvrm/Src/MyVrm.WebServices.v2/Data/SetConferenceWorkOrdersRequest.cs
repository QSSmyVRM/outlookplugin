﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace MyVrm.WebServices.Data
{
    internal class SetConferenceWorkOrdersRequest : ServiceRequestBase<SetConferenceWorkOrdersResponse>
    {
        internal SetConferenceWorkOrdersRequest(MyVrmService service) : base(service)
        {
        }

        internal WorkOrderCollection WorkOrders { get; set; }
        
        internal ConferenceId ConferenceId { get; set; }

        #region Overrides of ServiceRequestBase

        internal override string GetXmlElementName()
        {
            return "SetConferenceWorkOrders";
        }

        internal override string GetCommandName()
        {
            return "SetConferenceWorkOrders";
        }

        internal override string GetResponseXmlElementName()
        {
            return "success";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            writer.WriteStartElement(XmlNamespace.NotSpecified, "user");
            UserId.WriteToXml(writer);
            writer.WriteEndElement();
            writer.WriteStartElement(XmlNamespace.NotSpecified, "confInfo");
            ConferenceId.WriteToXml(writer, "ConfID");
            WorkOrders.WriteToXml(writer, "WorkOrderList");
            writer.WriteEndElement();
        }

        #endregion
    }
}
