﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.Outlook.WinForms.Conference
{
    partial class InstantConference
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.lblBottomText = new DevExpress.XtraEditors.LabelControl();
            this.lblTopText = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.comboIds = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtInvitees = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddressBook = new DevExpress.XtraEditors.SimpleButton();
            this.lblinvitepartycipant = new DevExpress.XtraEditors.LabelControl(); //ZD 101226
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboIds.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvitees.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.lblBottomText);
            this.panelControl1.Controls.Add(this.lblTopText);
            this.panelControl1.Controls.Add(this.pictureEdit1);
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(620, 64);
            this.panelControl1.TabIndex = 0;
            // 
            // lblBottomText
            // 
            this.lblBottomText.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(97)))), ((int)(((byte)(123)))));
            this.lblBottomText.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblBottomText.Appearance.Options.UseBackColor = true;
            this.lblBottomText.Appearance.Options.UseForeColor = true;
            this.lblBottomText.Location = new System.Drawing.Point(50, 36);
            this.lblBottomText.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.lblBottomText.Name = "lblBottomText";
            this.lblBottomText.Size = new System.Drawing.Size(200, 13);
            this.lblBottomText.TabIndex = 2;
            this.lblBottomText.Text = "Select attendees to join your conference.";
            // 
            // lblTopText
            // 
            this.lblTopText.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(129)))), ((int)(((byte)(158)))));
            this.lblTopText.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblTopText.Appearance.Options.UseBackColor = true;
            this.lblTopText.Appearance.Options.UseForeColor = true;
            this.lblTopText.Location = new System.Drawing.Point(50, 13);
            this.lblTopText.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.lblTopText.Name = "lblTopText";
            this.lblTopText.Size = new System.Drawing.Size(212, 13);
            this.lblTopText.TabIndex = 1;
            this.lblTopText.Text = "Select where you want to host this meeting.";
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureEdit1.EditValue = global::MyVrm.Outlook.WinForms.Properties.Resources.clip_header_bg;
            this.pictureEdit1.Location = new System.Drawing.Point(2, 2);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Size = new System.Drawing.Size(616, 60);
            this.pictureEdit1.TabIndex = 0;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 86);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Virtual Room";
            // 
            // comboIds
            // 
            this.comboIds.Location = new System.Drawing.Point(122, 85);
            this.comboIds.Name = "comboIds";
            this.comboIds.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.comboIds.Properties.Appearance.Options.UseBackColor = true;
            this.comboIds.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboIds.Properties.LookAndFeel.SkinName = "Blue";
            this.comboIds.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboIds.Size = new System.Drawing.Size(425, 20);
            this.comboIds.TabIndex = 2;
            // 
            // txtInvitees
            // 
            this.txtInvitees.Location = new System.Drawing.Point(122, 127);
            this.txtInvitees.Name = "txtInvitees";
            this.txtInvitees.Properties.Appearance.BackColor = System.Drawing.Color.DarkGray;
            this.txtInvitees.Properties.Appearance.Options.UseBackColor = true;
            this.txtInvitees.Properties.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.txtInvitees.Size = new System.Drawing.Size(425, 20);
            this.txtInvitees.TabIndex = 3;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(451, 170);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 5;
            this.simpleButton2.Text = "OK";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(533, 170);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(75, 23);
            this.simpleButton3.TabIndex = 6;
            this.simpleButton3.Text = "Cancel";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // btnAddressBook
            // 
            this.btnAddressBook.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btnAddressBook.Appearance.Options.UseBackColor = true;
            this.btnAddressBook.Image = global::MyVrm.Outlook.WinForms.Properties.Resources.phonebook;
            this.btnAddressBook.Location = new System.Drawing.Point(553, 125); //ZD 101226
            this.btnAddressBook.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.btnAddressBook.Name = "btnAddressBook";
            this.btnAddressBook.Size = new System.Drawing.Size(22, 23);
            this.btnAddressBook.TabIndex = 4;
            this.btnAddressBook.Click += new System.EventHandler(this.btnAddressBook_Click);
            
            // 
            // ZD 101226 starts
            // lblinvitepartycipant
            // 
            this.lblinvitepartycipant.Location = new System.Drawing.Point(12, 130);
            this.lblinvitepartycipant.Name = "lblinvitepartycipant";
            this.lblinvitepartycipant.Size = new System.Drawing.Size(87, 13);
            this.lblinvitepartycipant.TabIndex = 7;
            this.lblinvitepartycipant.Text = "Invite Participants";
            // ZD 101226 ends
            
            // 
            // InstantConference
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(620, 205);
            this.Controls.Add(this.lblinvitepartycipant);//ZD 101226
            this.Controls.Add(this.simpleButton3);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.btnAddressBook);
            this.Controls.Add(this.txtInvitees);
            this.Controls.Add(this.comboIds);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.panelControl1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InstantConference";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "TrueDayBook New Conference"; //ZD 101226
            this.Load += new System.EventHandler(this.InstantConference_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboIds.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvitees.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.LabelControl lblBottomText;
        private DevExpress.XtraEditors.LabelControl lblTopText;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit comboIds;
        private DevExpress.XtraEditors.TextEdit txtInvitees;
        private DevExpress.XtraEditors.SimpleButton btnAddressBook;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.LabelControl lblinvitepartycipant; //ZD 101226
    }
}