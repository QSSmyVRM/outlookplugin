﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using MyVrm.Common.Security;


namespace MyVrm.WebServices.Data
{
    class GetTDBUserPasswordrequest : ServiceRequestBase<GetTDBUserPasswordResponse>
    {

        private readonly NetworkCredential _credential;
        private readonly string _userName;
        private readonly string _domainName;
        //private readonly int _UserID;

        public GetTDBUserPasswordrequest(MyVrmService service)
            : base(service)
        {
        }

         internal GetTDBUserPasswordrequest(MyVrmService service, NetworkCredential credential) : base(service)
        {
            _credential = credential;
        }

         internal GetTDBUserPasswordrequest(MyVrmService service, string domaiName, string userName)
            : base(service)
        {
            _domainName = domaiName;
            _userName = userName;
        }

        public string subject { get; set; }
        public string placeholders { get; set; }
        public string emailtype { get; set; }


        #region Overrides of ServiceRequestBase

        internal override string GetXmlElementName()
        {
            return "GetHome";
        }

        internal override string GetCommandName()
        {
            return "GetHome";
        }

        internal override string GetResponseXmlElementName()
        {
            return "UserTDBpassword";
        }

        internal override GetTDBUserPasswordResponse ParseResponse(MyVrmServiceXmlReader reader)
        {
            var response = new GetTDBUserPasswordResponse();
            response.LoadFromXml(reader, GetResponseXmlElementName());
            return response;
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            writer.WriteStartElement(XmlNamespace.NotSpecified, "login");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "TDBUserPassword", "1");
            if (_credential != null)
            {
                writer.WriteElementValue(XmlNamespace.NotSpecified, "emailID", _credential.UserName);
               // writer.WriteElementValue(XmlNamespace.NotSpecified, "userPassword", _credential.Password);
            }
            else
            {
                writer.WriteElementValue(XmlNamespace.NotSpecified, "userName", SecurityUtils.JoinNTUserName(_domainName, _userName));
                writer.WriteElementValue(XmlNamespace.NotSpecified, "userAuthenticated", "Yes");
            }
        }

        #endregion

    }

    public class GetTDBUserPasswordResponse : ServiceResponse
    {
        public string UserName { get; internal set; }
        public string TDBUserPassword { get; internal set; }
        public string ConferenceUrl { get; internal set; }

        internal UserId UserId { get; private set; }
        internal OrganizationId OrganizationId { get; private set; }
        internal bool IsAVEnabled { get; set; }
        internal TrueDaybookService_v2.UserState LoggedUserState { get; set; }
        internal string Userpassword { get; set; }

        internal void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
        {
            try
            {
                do
                {
                    reader.Read();
                    if (reader.LocalName != "" && reader.LocalName != null)
                    {
                        switch (reader.LocalName)
                        {
                            //case "emailcontent":
                            //    LoadFromXml(reader,"emailcontent");
                            //    break;
                            case "userID":
                                UserId = new UserId(reader.ReadValue());
                                break;
                            case "Userpassword":
                                Userpassword = reader.ReadElementValue();
                                break;
                            default:
                                reader.SkipCurrentElement();
                                break;
                        }
                    }
                } while (!reader.IsEndElement(XmlNamespace.NotSpecified, xmlElementName));
            }
            catch (Exception ex)
            {
                MyVrmService.TraceSource.TraceError(reader.LocalName + ": " + ex.Message, null);
                //reader.Read();
                //throw;
            }
        }

      
    }
            
}
