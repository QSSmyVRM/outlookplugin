﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Xml;

namespace MyVrm.WebServices.Data
{
    internal delegate void ComplexPropertyChangedDelegate(ComplexProperty complexProperty);

    [Serializable]
    public abstract class ComplexProperty : ICloneable
    {
    	public abstract object Clone();

        internal ComplexProperty()
        {
            Namespace = XmlNamespace.NotSpecified;
        }

        internal XmlNamespace Namespace { get; set; }

        internal virtual void Changed()
        {
            if (OnChange != null)
            {
                OnChange(this);
            }
        }

        internal virtual void ClearChangeLog()
        {
        }

        internal virtual void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
        {
            LoadFromXml(reader, Namespace, xmlElementName);
        }

        internal virtual void LoadFromXml(MyVrmServiceXmlReader reader, XmlNamespace xmlNamespace, string xmlElementName)
        {
            reader.EnsureCurrentNodeIsStartElement(xmlNamespace, xmlElementName);
            ReadAttributesFromXml(reader);
            if (!reader.IsEmptyElement)
            {
                do
                {
                    reader.Read();
                    switch(reader.NodeType)
                    {
                        case XmlNodeType.Element:
                        {
                            if (!TryReadElementFromXml(reader))
                            {
                                reader.SkipCurrentElement();
                            }
                            break;
                        }
                        case XmlNodeType.Text:
                        {
                            ReadTextValueFromXml(reader);
                            break;
                        }
                    }
                } while (!reader.IsEndElement(xmlNamespace, xmlElementName));
            }
        }
        internal event ComplexPropertyChangedDelegate OnChange;

        internal virtual void ReadAttributesFromXml(MyVrmServiceXmlReader reader)
        {
        }

        internal virtual void ReadTextValueFromXml(MyVrmServiceXmlReader reader)
        {
        }

        internal virtual bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
        {
            return false;
        }

        internal virtual void WriteToXml(MyVrmXmlWriter writer, string xmlElementName)
        {
            WriteToXml(writer, Namespace, xmlElementName);
        }

        internal virtual void WriteToXml(MyVrmXmlWriter writer, XmlNamespace xmlNamespace, string xmlElementName)
        {
            writer.WriteStartElement(xmlNamespace, xmlElementName);
            WriteAttributesToXml(writer);
            WriteElementsToXml(writer);
            writer.WriteEndElement();
        }

        internal virtual void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            
        }

        internal virtual void WriteAttributesToXml(MyVrmXmlWriter writer)
        {
        }

        internal virtual void SetFieldValue<T>(ref T field, T value)
        {
            bool flag;
            if (field == null)
            {
                flag = value != null;
            }
            else if (field is IComparable)
            {
                flag = (field as IComparable).CompareTo(value) != 0;
            }
            else
            {
                flag = true;
            }
            if (flag)
            {
                field = value;
                Changed();
            }
        }
    }
}
