﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;

namespace MyVrm.Outlook
{
    [ComVisible(true)]
    [Guid("E13FEA80-EE8D-44f5-A962-457048F0DD4A"), ProgId("MyVrm.OutlookCustomFormControlHost")]
    [ClassInterface(ClassInterfaceType.AutoDual)]
    public class CustomFormControlHost : UserControl, ICustomFormControlHost
    {
        private CustomFormControl _customFormControl;

        [ComRegisterFunction]
        public static void RegisterClass(string key)
        {
            // Strip off HKEY_CLASSES_ROOT\ from the passed key as I don't need it
            var sb = new StringBuilder(key);
            sb.Replace(@"HKEY_CLASSES_ROOT\", "");

            // Open the CLSID\{guid} key for write access
            using (var k = Registry.ClassesRoot.OpenSubKey(sb.ToString(), true))
            {
                if (k != null)
                {
                    // And create the 'Control' key - this allows it to show up in 
                    // the ActiveX control container 
                    var ctrl = k.CreateSubKey("Control");
                    ctrl.Close();

                    // Next create the CodeBase entry - needed if not strong named and GACced.
                    var inprocServer32 = k.OpenSubKey("InprocServer32", true);
                    inprocServer32.SetValue("CodeBase", Assembly.GetExecutingAssembly().CodeBase);
                    inprocServer32.Close();

                    // Finally close the main key
                    k.Close();
                }
            }
        }

        [ComUnregisterFunction()]
        public static void UnregisterClass(string key)
        {
            var sb = new StringBuilder(key);
            sb.Replace(@"HKEY_CLASSES_ROOT\", "");

            // Open HKCR\CLSID\{guid} for write access
            using (var k = Registry.ClassesRoot.OpenSubKey(sb.ToString(), true))
            {
                if (k != null)
                {
                    // Delete the 'Control' key, but don't throw an exception if it does not exist
                    k.DeleteSubKey("Control", false);

                    // Next open up InprocServer32
                    var inprocServer32 = k.OpenSubKey("InprocServer32", true);

                    // And delete the CodeBase key, again not throwing if missing 
                    k.DeleteSubKey("CodeBase", false);

                    // Finally close the main key 
                    k.Close();
                }
            }
        }


        #region ICustomFormControlHost Members

        object ICustomFormControlHost.CustomFormControl
        {
            get { return _customFormControl; }
            set
            {
                if (_customFormControl != null)
                {
                    Controls.Remove(_customFormControl);
                    _customFormControl = null;
                }
                if (!(value is UserControl))
                {
                    throw new ArgumentException(string.Empty, "value");
                }
                _customFormControl = (CustomFormControl)value;
                _customFormControl.Dock = DockStyle.Fill;
                Controls.Add(_customFormControl);
            }
        }

        void ICustomFormControlHost.OnStartup()
        {
            _customFormControl.OnShowing();
        }

        public void OnShutdown()
        {
            _customFormControl.OnClose();
        }

        #endregion
    }
}
