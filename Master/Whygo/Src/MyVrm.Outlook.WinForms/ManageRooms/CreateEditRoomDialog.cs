﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraLayout.Utils;
using DevExpress.XtraPrinting.Native;
using MyVrm.WebServices.Data;
using MyVrm.WebServices.Data.Files;
using Image = System.Drawing.Image;
using System.Text.RegularExpressions;

namespace MyVrm.Outlook.WinForms.ManageRooms
{
	public partial class CreateEditRoomDialog : MyVrm.Outlook.WinForms.Dialog
	{
		public enum RoomTypeInCombo
		{
			VCTP,
			MeetingOnly
		}

		enum RoomLayoutType
		{
			Boardroom = 1,
            Cabaret,
            Classroom,
            Theatre,
            UShaped
		}

		private static string NoItems = "[no items]";
		private int _roomId;
		public int RoomId
		{
			set { _roomId = value; }
			get { return _roomId; }
		}
		public WGRoomType RoomType { get; set; }

		WhyGoManagedRoom _managedRoom = new WhyGoManagedRoom();
		
		public CreateEditRoomDialog()
		{
			InitializeComponent();
			ApplyVisible = false;
			roomTabbedGroup.ShowTabHeader = DefaultBoolean.False;

			var res = ManageRoomStaticData.CateringServiceDict.OrderBy(a => a, new CompareIntStr());
			foreach (var item in res )//ManageRoomStaticData.CateringServiceDict)
			{
				caiteringCheckedListBox.Items.Add(item, item.Value);//new CheckedListBoxItem(item.Value));
			}

			res = ManageRoomStaticData.EquipmentDict.OrderBy(a => a, new CompareIntStr());
			foreach (var item in res)//ManageRoomStaticData.EquipmentDict)
			{
				equipCheckedListBox.Items.Add(item, item.Value);//new CheckedListBoxItem(item.Value));
			}

			foreach (var item in ManageRoomStaticData.CountriesDict)
			{
				countryCombo.Properties.Items.Add(item.Value);
			}

			caiteringCheckedListBox.SortOrder = SortOrder.None;
			caiteringCheckedListBox.SelectedIndex = -1;
			equipCheckedListBox.SelectedIndex = -1;
			equipCheckedListBox.SortOrder = SortOrder.None;
			countryCombo.SelectedIndex = -1;
			stateCombo.Properties.NullValuePromptShowForEmptyValue = false;
			stateCombo.SelectedIndex = -1;
			stateCombo.EditValue = string.Empty;
			for (int i = 1; i <= 50; i++)
			{
				meetingCapacityCombo.Properties.Items.Add(i);
			}
			vctpCapacityCombo.Properties.Items.AddRange(meetingCapacityCombo.Properties.Items);

			roomLayoutComboBox.Properties.Items.Clear();
			roomLayoutComboBox.Properties.Items.AddRange(new [] {
            new ImageComboBoxItem(Strings.BoardroomLayout, RoomLayoutType.Boardroom, 0),
            new ImageComboBoxItem(Strings.CabaretLayout, RoomLayoutType.Cabaret, 1),
            new ImageComboBoxItem(Strings.ClassroomLayout, RoomLayoutType.Classroom, 2),
            new ImageComboBoxItem(Strings.TheatreLayout, RoomLayoutType.Theatre, 3),
            new ImageComboBoxItem(Strings.UShapedLayout, RoomLayoutType.UShaped, 4)});
			roomLayoutComboBox.SelectedIndex = 0;
			vctpLocationInfoIcon.SuperTip = labelControl2.SuperTip;
		}

		private void CreateEditRoomDialog_Load(object sender, EventArgs e)
		{
			OkEnabled = false;
			Text = RoomId == -1 ? Strings.CreateRoomCaption : Strings.EditRoomCaption;
			//if (RoomId == -1)
			//{
			//    roomTypeCombo.SelectedIndex = (int)RoomTypeInCombo.MeetingOnly;
			//}
			//else
			{
				//Get info for an existing room
				if (RoomId != -1)
				{
					try
					{
						_managedRoom._room = MyVrmService.Service.GetRoom(new RoomId(RoomId.ToString()));
						//MyVrmAddin.Instance.GetRoomFromCache(new RoomId(RoomId.ToString()));
						RoomType = _managedRoom._room.PublicRoomData.Type == "Video Conference or Telepresence" ?
						           	 WGRoomType.VCTP : WGRoomType.MeetingOnly;
					}
					catch (Exception exception)
					{
						MyVrmAddin.TraceSource.TraceException(exception, true);
						UIHelper.ShowMessage(exception.Message, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
					}
				}
				else
				{
					_managedRoom._room = new Room(MyVrmService.Service)
					{
						Id = RoomId == -1 ? WebServices.Data.RoomId.NewRoomId : new RoomId(RoomId.ToString())
					};
				}
				//
				switch(RoomType)
				{
					case WGRoomType.MeetingOnly:
						roomTypeCombo.SelectedIndex = (int)RoomTypeInCombo.MeetingOnly;
						break;
					case WGRoomType.VCTP:
						roomTypeCombo.SelectedIndex = (int)RoomTypeInCombo.VCTP;
						break;
					default:
						roomTypeCombo.SelectedIndex = -1;
						meetingGroup.Enabled = false;
						vctpGroup.Enabled = false;
						break;
				}
				if (RoomId != -1)
				{
					GetData();
				}
			}
		}

		private void labelControl2_Click(object sender, EventArgs e)
		{
		}

		private void roomTypeCombo_SelectedIndexChanged(object sender, EventArgs e)
		{
			switch ((RoomTypeInCombo)roomTypeCombo.SelectedIndex)
			{
				case RoomTypeInCombo.MeetingOnly :
					meetingGroup.Enabled = true;
					vctpGroup.Enabled = false;
					roomTabbedGroup.SelectedTabPage = meetingGroup;
					OkEnabled = true;
					RoomType = WGRoomType.MeetingOnly;
					break;
				case RoomTypeInCombo.VCTP:
					vctpGroup.Enabled = true;
					meetingGroup.Enabled = false;
					roomTabbedGroup.SelectedTabPage = vctpGroup;
					OkEnabled = true;
					RoomType = WGRoomType.VCTP;
					break;
			}
		}

		private void imageButton_Click(object sender, EventArgs e)
		{
			//imagePictureEdit.LoadImage();

			using(OpenFileDialog openFileDialog = new OpenFileDialog())
			{
				openFileDialog.CheckFileExists = true;
				openFileDialog.CheckPathExists = true;
				openFileDialog.DefaultExt = "bmp";
				openFileDialog.Filter = "All Picture Files |*.bmp;*.gif;*.jpg;*.jpeg;*.ico;*.png;*.tif";
				openFileDialog.Multiselect = false;
				openFileDialog.Title = "Select a room image";
				if( openFileDialog.ShowDialog() ==  DialogResult.OK)
				{
					try
					{
						imagePictureEdit.Image = Image.FromStream(openFileDialog.OpenFile());
						roomImageFilePath.Text = openFileDialog.FileName;
					}
					catch (Exception )
					{
						UIHelper.ShowMessage("Wrong picture format", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
						//ex.Message;
					}
					
				}
			}
		}

		private string ListToString(List<int> list)
		{
			string ret = string.Empty;

			foreach (var entry in list)
			{
				if (ret.Length > 0)
					ret += ",";
				ret += entry.ToString().Trim();
			}
			return ret;
		}

		private void CreateEditRoomDialog_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (((CreateEditRoomDialog)sender).DialogResult != DialogResult.OK)
				return;

			//For Video Conference & Teleprecence
			if (RoomType == WGRoomType.VCTP)//vctpGroup.Visible)
			{
				//Check input
				if( vctpLocationName.Text.Trim().Length == 0 )
				{
					UIHelper.ShowMessage("Location name is required!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
					e.Cancel = true;
				}

				if (!CheckMailInput(technicalContactMail.Text.Trim(), "Technical Contact") && !e.Cancel)
					e.Cancel = true;
				if (!e.Cancel && vctpCapacityCombo.SelectedIndex == -1)
				{
					UIHelper.ShowMessage("Capacity is required!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
					e.Cancel = true;
				}
			}

			//For Meeting Conference
			if (!e.Cancel && RoomType == WGRoomType.MeetingOnly)//meetingGroup.Visible)
			{
				//Check input
				if (meetingOnlyLocationName.Text.Trim().Length == 0) //Required field
				{
					UIHelper.ShowMessage("Location name is required!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
					e.Cancel = true;
				}
				if (!e.Cancel && countryCombo.SelectedIndex == -1)
				{
					UIHelper.ShowMessage("Select the country!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
					e.Cancel = true;
				}
				//if (!e.Cancel && stateCombo.SelectedIndex == -1)
				//{
				//    UIHelper.ShowMessage("Select the state!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				//    e.Cancel = true;
				//}
				if (!e.Cancel && citySuburb.Text.Trim().Length == 0)
				{
					UIHelper.ShowMessage("City/Suburb name is required!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
					e.Cancel = true;
				}
				if (!e.Cancel && addressMemoEdit.Text.Trim().Length == 0)
				{
					UIHelper.ShowMessage("Address is required!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
					e.Cancel = true;
				}

				if (!e.Cancel && !CheckMailInput(siteCoordinatorMail.Text.Trim(), "Site Coordinator"))
					e.Cancel = true;
				if (!e.Cancel && !CheckMailInput(managerMail.Text.Trim(), "Manager"))
					e.Cancel = true;
				if (!e.Cancel && meetingCapacityCombo.SelectedIndex == -1)
				{
					UIHelper.ShowMessage("Capacity is required!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
					e.Cancel = true;
				}
			}

			if (!e.Cancel)
			{
				Save();
			}
		}

		internal void GetData()
		{
			int i = 0;
			if (RoomType == WGRoomType.VCTP)
			{
				vctpLocationName.Text = _managedRoom._room.Name;
				i = vctpCapacityCombo.Properties.Items.IndexOf(_managedRoom._room.Capacity);
				vctpCapacityCombo.SelectedIndex = i;
				//vctpCapacityCombo.EditValue = _managedRoom._room.Capacity;
			}
			else
			{
				meetingOnlyLocationName.Text = _managedRoom._room.Name;
				i = meetingCapacityCombo.Properties.Items.IndexOf(_managedRoom._room.Capacity);
				meetingCapacityCombo.SelectedIndex = i;
				//meetingCapacityCombo.EditValue = _managedRoom._room.Capacity;
			}
			/**/
			citySuburb.Text = _managedRoom._room.City ?? string.Empty;

			i =
				stateCombo.Properties.Items.IndexOf(
					ManageRoomStaticData.StatesDict.FirstOrDefault(a => a.Key.First == _managedRoom._room.PublicRoomData.State));
			stateCombo.SelectedIndex = i;

			if (_managedRoom._room.PublicRoomData.State != 0)
			{
				string found =
					ManageRoomStaticData.StatesDict.FirstOrDefault(a => a.Key.First == _managedRoom._room.PublicRoomData.State).Key.
						Second;
				stateCombo.SelectedIndex = stateCombo.Properties.Items.IndexOf(found);
			}
			//??? = _managedRoom._room.ZipCode ;
			if (_managedRoom._room.PublicRoomData.Country != 0)
			{
				string found =
					ManageRoomStaticData.CountriesDict.FirstOrDefault(a => a.Key == _managedRoom._room.PublicRoomData.Country).Value;
				countryCombo.SelectedIndex = countryCombo.Properties.Items.IndexOf(found);

			}

			longitudeVal.Text = _managedRoom._room.Longitude ?? string.Empty;
			latitudeVal.Text = _managedRoom._room.Latitude ?? string.Empty;
			//if (!string.IsNullOrEmpty(_managedRoom._room.PublicRoomData.RoomImageName))
			if(_managedRoom._room.RoomImages.Count > 0)
			{
				roomImageFilePath.Text = _managedRoom._room.RoomImages[0].Name;//_managedRoom._room.PublicRoomData.RoomImageName; //????????
				//Byte[] bytes = Encoding.ASCII.GetBytes(_managedRoom._room.PublicRoomData.RoomImages);
				MemoryStream st = new MemoryStream(_managedRoom._room.RoomImages[0].Data);//bytes);
				imagePictureEdit.Image = Image.FromStream(st);
				//OpenFileDialog openFileDialog = new OpenFileDialog();
				//openFileDialog.FileName = _managedRoom._room.RoomImageName;
				//if (openFileDialog.CheckFileExists)
				//    imagePictureEdit.Image = Image.FromStream(openFileDialog.OpenFile());
			}
			addressMemoEdit.Text = _managedRoom._room.PublicRoomData.Address ?? string.Empty;

			foreach (int ndx in _managedRoom._room.PublicRoomData.AUXEquipmentList)
			{
				var obj = ManageRoomStaticData.EquipmentDict.FirstOrDefault(a => a.Key == ndx);
				foreach (CheckedListBoxItem chItem in equipCheckedListBox.Items)
				{
					if (((KeyValuePair<int, string>) chItem.Value).Key == obj.Key)
					{
						int found = equipCheckedListBox.Items.IndexOf(chItem);
						equipCheckedListBox.SetItemChecked(found, true);
						break;
					}
				}
			}

			if (_managedRoom._room.PublicRoomData.CateringOptions != ManageRoomStaticData.NoCatering.ToString())
			{
				//var res = ManageRoomStaticData.CateringServiceDict.OrderBy(a => a, new CompareIntStr());
				foreach (int ndx in _managedRoom._room.PublicRoomData.CateringOptionsList)
				{
					var obj = ManageRoomStaticData.CateringServiceDict.FirstOrDefault(a => a.Key == ndx);

					foreach (CheckedListBoxItem chItem in caiteringCheckedListBox.Items)
					{
						if (((KeyValuePair<int, string>) chItem.Value).Key == obj.Key)
						{
							int found = caiteringCheckedListBox.Items.IndexOf(chItem);
							caiteringCheckedListBox.SetItemChecked(found, true);
							break;
						}
					}
				}
			}
			memoEdit3.Text = _managedRoom._room.PublicRoomData.DefaultEquipment ?? string.Empty;
			memoEdit1.Text = _managedRoom._room.PublicRoomData.Description ?? string.Empty;
			memoEdit2.Text = _managedRoom._room.PublicRoomData.ExtraNotes ?? string.Empty;
			ipNumber.Text = _managedRoom._room.PublicRoomData.ipAddress ?? string.Empty;
			ipSpeedCombo.Text = _managedRoom._room.PublicRoomData.ipSpeed ?? string.Empty;
			isdnNumber.Text = _managedRoom._room.PublicRoomData.isdnAddress ?? string.Empty;
			isdnSpeedCombo.Text = _managedRoom._room.PublicRoomData.isdnSpeed ?? string.Empty;

			var item = roomLayoutComboBox.Properties.Items.GetItem((RoomLayoutType) _managedRoom._room.PublicRoomData.Layout);
			roomLayoutComboBox.SelectedIndex = roomLayoutComboBox.Properties.Items.IndexOf(item);

			//roomLayoutComboBox.Properties.Items.IndexOf((RoomLayoutType)_managedRoom._room.PublicRoomData.Layout);

			managerMail.Text = _managedRoom._room.PublicRoomData.Manager.Email ?? string.Empty;
			managerName.Text = _managedRoom._room.PublicRoomData.Manager.Name ?? string.Empty;
			managerPhone.Text = _managedRoom._room.PublicRoomData.Manager.Phone ?? string.Empty;

			siteCoordinatorMail.Text = _managedRoom._room.PublicRoomData.SiteCordinator.Email ?? string.Empty;
			siteCoordinatorName.Text = _managedRoom._room.PublicRoomData.SiteCordinator.Name ?? string.Empty;
			siteCoordinatorPhone.Text = _managedRoom._room.PublicRoomData.SiteCordinator.Phone ?? string.Empty;

			technicalContactMail.Text = _managedRoom._room.PublicRoomData.TechnicalContact.Email ?? string.Empty;
			technicalContactName.Text = _managedRoom._room.PublicRoomData.TechnicalContact.Name ?? string.Empty;
			technicalContactPhone.Text = _managedRoom._room.PublicRoomData.TechnicalContact.Phone ?? string.Empty;
		}

		internal void Save()
		{
			_managedRoom._room.PublicRoomData.Type = RoomType == WGRoomType.VCTP ? "Video Conference or Telepresence" : "Meeting Only";

			_managedRoom._room.Name = RoomType == WGRoomType.VCTP ? vctpLocationName.Text : meetingOnlyLocationName.Text;
			_managedRoom._room.Capacity = RoomType == WGRoomType.VCTP ?
												vctpCapacityCombo.EditValue is int ? (int)vctpCapacityCombo.EditValue : 0 :
												meetingCapacityCombo.EditValue is int ? (int)meetingCapacityCombo.EditValue : 0;
			_managedRoom._room.Media = RoomType == WGRoomType.VCTP ? MediaType.AudioVideo : MediaType.NoAudioVideo;
			_managedRoom._room.City = citySuburb.Text;

			if (!string.IsNullOrEmpty((string)stateCombo.EditValue) && NoItems.CompareTo((string)stateCombo.EditValue) != 0)
				_managedRoom._room.PublicRoomData.State = ManageRoomStaticData.StatesDict.FirstOrDefault(a => a.Key.Second == (string)stateCombo.EditValue).Key.First;
			//??? _managedRoom._room.ZipCode = ;
			if (!string.IsNullOrEmpty((string) countryCombo.EditValue))
				_managedRoom._room.CountryId = ManageRoomStaticData.CountriesDict.FirstOrDefault(a => a.Value == (string) countryCombo.EditValue).Key.ToString();

			_managedRoom._room.TimeZone = TimeZoneInfo.Local; //EST= 26; CST= 19; MST= 42; PST= 51
			_managedRoom._room.Longitude = longitudeVal.Text;
			_managedRoom._room.Latitude = latitudeVal.Text;

			//upload image file
			string imageFilePath = roomImageFilePath.Text.Trim();
			if (!string.IsNullOrEmpty(imageFilePath) )
			{
				if( File.Exists(imageFilePath))
				{
					try
					{
						byte[] data;
						using (var fileStm = File.OpenRead(imageFilePath))
						{
							data = new byte[fileStm.Length];
							fileStm.Read(data, 0, data.Length);
						}
						_managedRoom._room.PublicRoomData.RoomImageName = Path.GetFileName(imageFilePath);
						_managedRoom._room.PublicRoomData.RoomImages = Convert.ToBase64String(data);
						//var fileInfo = new UploadFileInfo(_managedRoom._room.RoomImageName, data);
						//MyVrmService.Service.UploadFile(fileInfo);
					}
					catch (Exception exception)
					{
						MyVrmAddin.TraceSource.TraceException(exception);
						ShowError(exception.Message);
					}
				}
				else
				{
					if (_managedRoom._room.RoomImages.Count > 0)
					{
						_managedRoom._room.PublicRoomData.RoomImageName = _managedRoom._room.RoomImages[0].Name;
						_managedRoom._room.PublicRoomData.RoomImages = Convert.ToBase64String(_managedRoom._room.RoomImages[0].Data);
					}
				}
			}

			_managedRoom._room.PublicRoomData.Address = addressMemoEdit.Text;

			if (_managedRoom._room.PublicRoomData.AUXEquipmentList != null)
				_managedRoom._room.PublicRoomData.AUXEquipmentList.Clear();
			else
				_managedRoom._room.PublicRoomData.AUXEquipmentList = new List<int>();
			_managedRoom._room.PublicRoomData.AUXEquipment = string.Empty;
			if (equipCheckedListBox.CheckedIndices.Count > 0)
			{
				foreach (int ndx in equipCheckedListBox.CheckedIndices)
				{
					KeyValuePair<int, string> sel = (KeyValuePair<int, string>)equipCheckedListBox.Items[ndx].Value;
					_managedRoom._room.PublicRoomData.AUXEquipmentList.Add(sel.Key);
					if (_managedRoom._room.PublicRoomData.AUXEquipment.Length > 0)
						_managedRoom._room.PublicRoomData.AUXEquipment += ",";
					_managedRoom._room.PublicRoomData.AUXEquipment += sel.Key.ToString();
				}
			}

			if (_managedRoom._room.PublicRoomData.CateringOptionsList != null)
				_managedRoom._room.PublicRoomData.CateringOptionsList.Clear();
			else
				_managedRoom._room.PublicRoomData.CateringOptionsList = new List<int>();
			_managedRoom._room.PublicRoomData.CateringOptions = string.Empty;
			if (caiteringCheckedListBox.CheckedIndices.Count == 0)
			{
				//set as selected: 3, "none" 
				_managedRoom._room.PublicRoomData.CateringOptionsList.Add(ManageRoomStaticData.NoCatering);
			}
			else
			{
				foreach (int ndx in caiteringCheckedListBox.CheckedIndices)
				{
					KeyValuePair<int, string> sel = (KeyValuePair<int, string>)caiteringCheckedListBox.Items[ndx].Value;
					_managedRoom._room.PublicRoomData.CateringOptionsList.Add(sel.Key);
					if (_managedRoom._room.PublicRoomData.CateringOptions.Length > 0)
						_managedRoom._room.PublicRoomData.CateringOptions += ",";
					_managedRoom._room.PublicRoomData.CateringOptions += sel.Key.ToString();
				}
			}
			_managedRoom._room.PublicRoomData.DefaultEquipment = memoEdit3.Text;
			_managedRoom._room.PublicRoomData.Description = memoEdit1.Text;
			_managedRoom._room.PublicRoomData.ExtraNotes = memoEdit2.Text;
			_managedRoom._room.PublicRoomData.ipAddress = ipNumber.Text;
			_managedRoom._room.PublicRoomData.ipSpeed = ipSpeedCombo.Text;
			_managedRoom._room.PublicRoomData.isdnAddress = isdnNumber.Text;
			_managedRoom._room.PublicRoomData.isdnSpeed = isdnSpeedCombo.Text;
			if(roomLayoutComboBox.SelectedItem != null)
				_managedRoom._room.PublicRoomData.Layout = (int)((ImageComboBoxItem)roomLayoutComboBox.SelectedItem).Value;
			_managedRoom._room.PublicRoomData.Manager = new ResponsiblePerson(WhyGoManagedRoom.GetPerson(WhyGoManagedRoom.PersonType.Manager))
			{
				Email = managerMail.Text,
				Name = managerName.Text,
				Phone = managerPhone.Text
			};
			_managedRoom._room.PublicRoomData.SiteCordinator = new ResponsiblePerson(WhyGoManagedRoom.GetPerson(WhyGoManagedRoom.PersonType.SiteCordinator))
			{
				Email = siteCoordinatorMail.Text,
				Name = siteCoordinatorName.Text,
				Phone = siteCoordinatorPhone.Text
			};
			_managedRoom._room.PublicRoomData.TechnicalContact = new ResponsiblePerson(WhyGoManagedRoom.GetPerson(WhyGoManagedRoom.PersonType.TechnicalContact))
			{
				Email = technicalContactMail.Text,
				Name = technicalContactName.Text,
				Phone = technicalContactPhone.Text
			};
			try
			{
				var ret = MyVrmService.Service.SetPrivatePublicRoomProfile(_managedRoom);

				if (ret != null)
				{
					if (ret.Success)
					{
						UIHelper.ShowMessage(RoomId == -1 ? "The room is created successfuly." : "The room is updated successfuly."/*,
						                     MessageBoxButtons.OK, MessageBoxIcon.Information*/);
						MyVrmAddin.Instance.UpdateRoomCache(_managedRoom._room);
					}
				}
			}
			catch (Exception exception)
			{
				MyVrmAddin.TraceSource.TraceException(exception, true);
				UIHelper.ShowMessage(exception.Message, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}
		}

		//void CheckNameInput(ChangingEventArgs e)
		private void CheckNameInput_EditValueChanging(object sender, ChangingEventArgs e)
		{
			foreach (char ch in e.NewValue.ToString())
			{
				bool bIsValid = Char.IsLetter(ch) || Char.IsWhiteSpace(ch) || ch.CompareTo('-') == 0 || ch.CompareTo('.') == 0;
				if(!bIsValid)
				{
					//e.NewValue = e.OldValue;
					UIHelper.ShowMessage("Only letters, dots, spaces and dashs are permitted!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
					e.Cancel = true;
					break;
				}
			}
		}

		private void CheckPhoneInput_EditValueChanging(object sender, ChangingEventArgs e)
		{
			foreach (char ch in e.NewValue.ToString())
			{
				bool bIsValid = Char.IsDigit(ch) || Char.IsWhiteSpace(ch) || ch.CompareTo('-') == 0 || ch.CompareTo('(') == 0
					|| ch.CompareTo(')') == 0 || ch.CompareTo('#') == 0;
				if (!bIsValid)
				{
					UIHelper.ShowMessage("Only digits, dots, spaces, parenthesis and hash marks are permitted!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
					e.Cancel = true;
					break;
				}
			}
		}

		bool CheckMailInput(string addr, string msgOriginator)
		{
			//Field is not required, if not filled - just return OK
			if (string.IsNullOrEmpty(addr))
				return true;

			bool bIsValid = addr != null && addr.Length >= 5 ;
			if(bIsValid)
			{
				int iLyagaNdx = addr.LastIndexOf('@');
				int iLastDot = addr.LastIndexOf('.');
				//address has
				// - @ in it
				// - @ is not in the 1 position
				// - to the right from the @ there are at least 2 symbols, and a '.', and there are no spaces
				// - a '.' is not the last one.
				bIsValid = iLyagaNdx > 0 && iLastDot > iLyagaNdx + 1 && iLastDot < addr.Length - 1 && iLyagaNdx > addr.LastIndexOf(' ');
			}
			
			if (!bIsValid)
			{
				UIHelper.ShowMessage(msgOriginator + " E-mail address is invalid!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}
			return bIsValid;
		}

		bool AreDigits(string val)
		{
			bool bRet = true;
			foreach (char ch in val)
			{
				bool bIsValid = Char.IsDigit(ch);
				if (!bIsValid)
				{
					UIHelper.ShowMessage("Only digits are permitted!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
					bRet = false;
					break;
				}
			}
			return bRet;
		}

		//longitude: 23° 27′ 30" E | W
		private void longitudeGrad_EditValueChanging(object sender, ChangingEventArgs e)
		{
			e.Cancel = AreDigits(e.NewValue.ToString()) != true;
					
			
		}

		private void longitudeMinuteSec_EditValueChanging(object sender, ChangingEventArgs e)
		{
			e.Cancel = AreDigits(e.NewValue.ToString()) != true;

			if (e.Cancel == false)
			{
				int iVal = 0;
				int.TryParse(e.NewValue.ToString(), out iVal);
				if (iVal > 60)
				{
					UIHelper.ShowMessage("This value can't be more than 60!", MessageBoxButtons.OK,
										 MessageBoxIcon.Exclamation);
					e.Cancel = true;
				}
			}
		}

		private void countryCombo_SelectedIndexChanged(object sender, EventArgs e)
		{
			ComboBoxEdit combo = sender as ComboBoxEdit;
			if( combo != null)
			{
				KeyValuePair<int, string> found = ManageRoomStaticData.CountriesDict.FirstOrDefault(a => a.Value == (string) combo.EditValue);
				int iNdx = found.Key;//: -1?
				stateCombo.EditValue = string.Empty;
				stateCombo.Properties.Items.Clear();
				
				var extracted = ManageRoomStaticData.StatesDict.Where(a => a.Value == iNdx).ToList();
				foreach (var state in extracted)
				{
					stateCombo.Properties.Items.Add(state.Key.Second);
				}
				if (stateCombo.Properties.Items.Count == 0)
				{
					stateCombo.EditValue = NoItems;
					stateCombo.Properties.NullValuePromptShowForEmptyValue = false;
				}
				else
				{
					stateCombo.SelectedIndex = 0;//-1;
					stateCombo.Properties.NullValuePromptShowForEmptyValue = true;
					stateCombo.ShowPopup();
				}
			}
		}

		private void clearImageBn_Click(object sender, EventArgs e)
		{
			imagePictureEdit.Image = null;
			roomImageFilePath.Text = string.Empty;
		}
	}
	public class CompareIntStr : IComparer<KeyValuePair<int, string>>
	{
		// Because the class implements IComparer, it must define a 
		// Compare method. The method returns a signed integer that indicates 
		// whether s1 > s2 (return is greater than 0), s1 < s2 (return is negative),
		// or s1 equals s2 (return value is 0). This Compare method compares strings. 
		public int Compare(KeyValuePair<int, string> obj1, KeyValuePair<int, string> obj2)
		{
			return string.Compare(obj1.Value, obj2.Value, true);
		}
	}
}
