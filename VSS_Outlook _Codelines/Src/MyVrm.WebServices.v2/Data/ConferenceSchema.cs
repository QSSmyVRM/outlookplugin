﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace MyVrm.WebServices.Data
{
	internal class ConferenceSchema : ConferenceBaseSchema
    {
		internal static readonly PropertyDefinition Type;
        internal static readonly PropertyDefinition FilesToUpload;
        internal static readonly PropertyDefinition confType;
		public new static readonly ConferenceSchema Instance;
        
        static ConferenceSchema()
        {
			Type = new GenericPropertyDefinition<ConferenceType>("createBy", PropertyDefinitionFlags.CanSet | PropertyDefinitionFlags.CanUpdate);
            FilesToUpload = new ComplexPropertyDefinition<ConferenceFileCollection>("fileUpload", PropertyDefinitionFlags.CanSet | PropertyDefinitionFlags.AutoInstantiateOnRead, () => new ConferenceFileCollection());
            confType = new GenericPropertyDefinition<ConferenceType>("confType", PropertyDefinitionFlags.CanSet | PropertyDefinitionFlags.CanUpdate);

			Instance = new ConferenceSchema();
        }

		internal override void RegisterProperties()
		{
			base.RegisterProperties();
		    RegisterProperty(Type);
            RegisterProperty(confType);
            RegisterProperty(FilesToUpload);
		}
    }
}
