﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections;
using System.Collections.Generic;

namespace MyVrm.WebServices.Data
{
    public class FavoriteRoomCollection : ComplexProperty, IEnumerable<RoomId>
    {
        private List<RoomId> _roomIds = new List<RoomId>();

		public override object Clone()
		{
			FavoriteRoomCollection copy = new FavoriteRoomCollection();
			foreach (var item in _roomIds)
			{
				copy._roomIds.Add((RoomId)item.Clone()); //???????
			}

			return copy;
		}

        #region Implementation of IEnumerable

        public IEnumerator<RoomId> GetEnumerator()
        {
            return _roomIds.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        internal override void LoadFromXml(MyVrmServiceXmlReader reader, XmlNamespace xmlNamespace, string xmlElementName)
        {
            reader.EnsureCurrentNodeIsStartElement(xmlNamespace, xmlElementName);
            if (!reader.IsEmptyElement)
            {
                do
                {
                    reader.Read();
                    if (reader.LocalName == "selected")
                    {
                        do
                        {
                            reader.Read();
                            if (reader.LocalName == "level1ID")
                            {
                                var rooms = reader.ReadElementValue();
                                _roomIds.AddRange(Array.ConvertAll(rooms.Split(new[] { ',' }), input => new RoomId(input)));
                            }
                            else
                            {
                                reader.SkipCurrentElement();
                            }
                        } while (!reader.IsEndElement(xmlNamespace, "selected"));
                    }
                    else
                    {
                        reader.SkipCurrentElement();
                    }
                } while (!reader.IsEndElement(xmlNamespace, xmlElementName));
            }
        }
    }
}
