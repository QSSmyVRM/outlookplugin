﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
//using Microsoft.Office.Interop.Outlook;//ZD 102582
using System.Drawing;
using System.Globalization;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using DevExpress.XtraEditors;
using MyVrm.WebServices.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraLayout;
using MyVrm.Common;
using MyVrm.Common.ComponentModel;
using MyVrm.Outlook.WinForms.Calendar;
using MyVrm.Outlook.WinForms.Properties;
using MyVrm.WebServices.Data;
using Image = System.Drawing.Image;

namespace MyVrm.Outlook.WinForms
{
	public partial class XmlSeatsDlg : MyVrm.Outlook.WinForms.Dialog
	{
		internal Color Green = Color.FromArgb(101, 254, 101);
		internal Color Yellow = Color.FromArgb(248, 240, 116);
        //private readonly OutlookAppointment _appointment;
        //public ConferenceWrapper Conf;
        Conference.ConferenceWrapper conWrapper = null;
        //Conference.ConferenceWrapper conWrapper;
        //WebServices.Data.Conference conference;
		public DateTime ConfStart 
		{
			get { return _confStart; }
			set
			{
				_confStart = value;
			} 
		}
		public DateTime ConfEnd
		{
			get { return _confEnd; }
			set
			{
				_confEnd = value;
			}
		}

        

     
		private DateTime _confStart = DateTime.Now;
		public DateTime _confEnd = DateTime.Now;
        public TimeZoneInfo timeZTc;// ZD  102582
		private string _confId;
		public string ConfId
		{
			set { _confId = value; }
			get { return _confId; }
		}
		public bool OpenOptionsDlg;
		public XmlSeatsDlg()
		{
			InitializeComponent();
			ApplyEnabled = false;
			ApplyVisible = false;
			OkEnabled = false;
			OkVisible = false;
			CancelVisible = true;
			CancelEnabled = true;
			CancelBnText = Strings.CloseBtnTxt;
			OpenOptionsDlg = false;
			Text = "Conference Time Availability : Number of Seats";
			//repositoryItemMemoEdit1.CreateEditor().EditValue= memoEdit1;
			//repositoryItemMemoEdit1.
		}
        //public OutlookAppointment Appointment
        //{
        //    get
        //    {
        //        return _appointment;
        //    }
        //}
		private void SeatsXML_Load(object sender, EventArgs e)
		{
			List<TCData> data = new List<TCData>();
			data.Add(new TCData(){Text = memoText1, Color = labelColor1});
			data.Add(new TCData(){ Text = memoText2, Color = labelColor2 });
			data.Add(new TCData(){ Text = memoText3, Color = labelColor3 });
			data.Add(new TCData(){ Text = memoText4, Color = labelColor4 });
			data.Add(new TCData(){ Text = memoText5, Color = labelColor5 });

			try
			{

                //User us = MyVrmService.Service.GetUser();
                //TimeZoneInfo Timezone = us.TimeZone;
                //conWrapper = new MyVrm.Outlook.WinForms.Conference.ConferenceWrapper(MyVrmService.Service);
                //Timezone = conWrapper.TimeZone;
                //ConfStart = conWrapper.StartDate;
                //ConfEnd = conWrapper.EndDate;
                if (timeZTc == null)
                {
                    User us = MyVrmService.Service.GetUser();
                    timeZTc = us.TimeZone;
                }
                var ret = MyVrmService.Service.TDGetSlotAvailableSeats(ConfStart, ConfEnd, ConfId, timeZTc);
				//    /*manual ->*/
				//    new TDGetSlotAvailableSeatsResponse();
				//ret.RetCode = 0;
				//ret.seatsNumberTotal = 5;
				//ret.AvailSeatsCollection = new AvailSeatsCollection2();
				//ret.AvailSeatsCollection.Add(new AvailSeats2() { Start = DateTime.Now, End = DateTime.Now.AddHours(1), Seats = 1});
				//ret.AvailSeatsCollection.Add(new AvailSeats2() { Start = DateTime.Now.AddHours(2), End = DateTime.Now.AddHours(3), Seats = 3 });
				//ret.AvailSeatsCollection.Add(new AvailSeats2() { Start = DateTime.Now.AddHours(4), End = DateTime.Now.AddHours(5), Seats = 5 });
				//ret.AvailSeatsCollection.Add(new AvailSeats2() { Start = DateTime.Now.AddHours(6), End = DateTime.Now.AddHours(7), Seats = 2 });
				//ret.AvailSeatsCollection.Add(new AvailSeats2() { Start = DateTime.Now.AddHours(8), End = DateTime.Now.AddHours(9), Seats = 5 });
				///*<- manual*/
				int i = 0;
				if (ret != null)
				{
					foreach (var record in ret.AvailSeatsCollection)
					{
						data[i].Text.EditValue = "\r\nStart Date/Time: " + record.Start.ToString("g", new CultureInfo("en-us"));
						data[i].Text.EditValue += "\r\nEnd Date/Time: " + record.End.ToString("g", new CultureInfo("en-us"));
						data[i].Text.EditValue += "\r\nAvailable Seats: " + record.Seats;
						data[i].Text.BackColor = Color.White;
						data[i].Color.Appearance.BackColor = record.Seats == ret.seatsNumberTotal ? Green : Yellow;
						i++;
					}
				}
			}
			catch (NoCredentialsException ex)
			{
				UIHelper.ShowError(ex.Message);
				OpenOptionsDlg = true;
				Close();
			}
			catch (Exception ex)
			{
				UIHelper.ShowError(ex.Message);
				Close();
			}
		}
	}

	internal class TCData
	{
		public MemoEdit Text;
		public LabelControl Color;
	}
}
