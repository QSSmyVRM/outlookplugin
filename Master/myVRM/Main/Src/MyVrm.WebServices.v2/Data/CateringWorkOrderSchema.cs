﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    internal class CateringWorkOrderSchema : WorkOrderBaseSchema
    {
        public static readonly PropertyDefinition CateringServiceId;
        public static readonly PropertyDefinition Comments;
        public static readonly PropertyDefinition DeliverByDate;
        public static readonly PropertyDefinition DeliverByTime;
        public static readonly PropertyDefinition Price;
        public static readonly PropertyDefinition TotalCost;
        public static readonly PropertyDefinition Menus;

        internal static readonly CateringWorkOrderSchema Instance;

        static CateringWorkOrderSchema()
        {
            CateringServiceId = new IntPropertyDefinition("SelectedService", "CateringService",
                                                          PropertyDefinitionFlags.CanSet);
            Comments = new StringPropertyDefinition("Comments", PropertyDefinitionFlags.CanSet);
            DeliverByDate = new DatePropertyDefinition("DeliverByDate", PropertyDefinitionFlags.CanSet);
            DeliverByTime = new TimePropertyDefinition("DeliverByTime", PropertyDefinitionFlags.CanSet, true);
            Price = new DecimalPropertyDefinition("Price", PropertyDefinitionFlags.CanSet);
            Menus = new ComplexPropertyDefinition<CateringWorkOrderMenuCollection>("MenuList",
                                                                                   PropertyDefinitionFlags.CanSet |
                                                                                   PropertyDefinitionFlags.
                                                                                       AutoInstantiateOnRead,
                                                                                   () =>
                                                                                   new CateringWorkOrderMenuCollection());
            TotalCost = new DecimalPropertyDefinition("TotalCost", PropertyDefinitionFlags.CanSet);
            Instance = new CateringWorkOrderSchema();
        }

        internal override void RegisterProperties()
        {
            base.RegisterProperties();
            RegisterProperty(CateringServiceId);
            RegisterProperty(Comments);
            RegisterProperty(DeliverByDate);
            RegisterProperty(DeliverByTime);
            RegisterProperty(Price);
            RegisterProperty(Menus);
            RegisterProperty(TotalCost);
        }
    }
}
