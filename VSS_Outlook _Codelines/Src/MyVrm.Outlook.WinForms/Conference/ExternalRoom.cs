﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook.WinForms.Conference
{
	public class WrapPair<type1, type2>
	{
		private type1 obj1;
		private type2 obj2;
		public type1 FirstObj
		{
			set { obj1 = value; }
			get { return obj1; }
		}

		public type2 SecondObj
		{
			set { obj2 = value; }
			get { return obj2; }
		}

		public WrapPair()
		{
			obj1 = default(type1);
			obj2 = default(type2);
		}
		public WrapPair(type1 ob1, type2 ob2)
		{
			obj1 = ob1;
			obj2 = ob2;
		}
	}

	public class ExternalRoom
	{
       
        public int confLisNum; //ZD 101343
   
        public UserId UserId { get; set; } 
		public string UserFirstName { set; get; }
		public string UserLastName { set; get; }
		public string UserEmail { set; get; }
		public string AddressPhone { set; get; }
        public string ID { set; get; } //ZD 101343
		public WrapPair<int, string> AddressType { set; get; }
		public WrapPair<int, string> ConnectionType { set; get; }
		public WrapPair<int, string> ProtocolType { set; get; }
		public string ProtocolName
		{
			get { return ProtocolType != null ? ProtocolType.SecondObj : string.Empty; }
		}
		public WrapPair<int, string> LineRate { set; get; }
		public WrapPair<int, string> Equipment { set; get; }
		public WrapPair<BridgeId, string> MCU { set; get; }
		public string MCUName
		{
			get { return MCU != null ? MCU.SecondObj : string.Empty; }
		}
		public WrapPair<int, string> CallerCallee { set; get; }
		public string CallerCalleeName
		{
			get { return CallerCallee != null ? CallerCallee.SecondObj : string.Empty; }
		}
        public int Connection { get; set; }//ZD 104278
		public WrapPair<MCUProfileId, string> MCUProfile { set; get; }
		public string APIPort { set; get; }
		public string URL { set; get; }
//ZD 101343 starts
        public bool IsGuestLoc { set; get; }
        public string EndPtName { set; get; }
        public bool IsChanged { set; get; }
        public EndpointId EndPointID { get; set; }
        public string RoomID { get; set; }
//ZD 100343 Ends
	}
}
