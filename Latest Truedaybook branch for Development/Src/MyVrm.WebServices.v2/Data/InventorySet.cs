﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.WebServices.Data
{
    [ServiceObjectDefinition("Inventory")]
    public class InventorySet : ServiceObject
    {
        public InventorySet(MyVrmService service) : base(service)
        {
        }

        public int Id
        {
            get
            {
				int iRet = 0;
				if (PropertyBag[InventorySetSchema.Id] != null)
            		int.TryParse(PropertyBag[InventorySetSchema.Id].ToString(), out iRet);
            	return iRet; //(int)PropertyBag[InventorySetSchema.Id];
            }
            set
            {
                PropertyBag[InventorySetSchema.Id] = value; 
            }
        }

        public int Type
        {
            get
            {
                int iRet = 0;
				if (PropertyBag[InventorySetSchema.Type] != null)
					int.TryParse(PropertyBag[InventorySetSchema.Type].ToString(), out iRet);
				return iRet; //(int)PropertyBag[InventorySetSchema.Type];
            }
            set
            {
                PropertyBag[InventorySetSchema.Type] = value;
            }
        }

        public string Name
        {
            get
            {
                return (string)PropertyBag[InventorySetSchema.Name];
            }
            set
            {
                PropertyBag[InventorySetSchema.Name] = value;
            }
        }

        public string Comments
        {
            get
            {
                return (string)PropertyBag[InventorySetSchema.Comments];
            }
            set
            {
                PropertyBag[InventorySetSchema.Comments] = value;
            }
        }

        public bool Notify
        {
            get
            {
				bool bRet = false;
				if (PropertyBag[InventorySetSchema.Notify] != null)
					bool.TryParse(PropertyBag[InventorySetSchema.Notify].ToString(), out bRet);
				return bRet;//(bool)PropertyBag[InventorySetSchema.Notify];
            }
            set
            {
                PropertyBag[InventorySetSchema.Comments] = value;
            }
        }

        public InventorySetItemCollection Items
        {
            get
            {
                return (InventorySetItemCollection) PropertyBag[InventorySetSchema.Items];
            }
        }

        public RoomIdCollection Rooms
        {
            get
            {
                return (RoomIdCollection)PropertyBag[InventorySetSchema.Rooms];
            }
        }

        #region Overrides of ServiceObject

        internal override ServiceObjectSchema GetSchema()
        {
            return InventorySetSchema.Instance;
        }

        #endregion
    }
}
