﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    public sealed class LineRateCollection : BaseCollection<LineRate>
    {
        #region Overrides of BaseCollection<LineRate>

        internal override void LoadFromXml(MyVrmServiceXmlReader reader)
        {
            do
            {
                reader.Read();
                if (reader.IsStartElement(XmlNamespace.NotSpecified, "Rate"))
                {
                    var lineRate = new LineRate();
                    lineRate.LoadFromXml(reader);
                    Items.Add(lineRate);
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "LineRate"));
        }

        #endregion
    }
}
