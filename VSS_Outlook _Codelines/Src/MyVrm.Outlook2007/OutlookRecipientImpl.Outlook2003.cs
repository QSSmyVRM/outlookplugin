﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Outlook;
using MyVrm.MAPI;
using MyVrm.WebServices.Data;
using Exception = System.Exception;

namespace MyVrm.Outlook
{
    internal partial class OutlookRecipientImpl
    {
        public override string SmtpAddress
        {
            get
            {
                if (_smtpAddress == null)
                {
                    if (!_recipient.Resolved)
                    {
                        return null;
                    }
                    try
                    {
                        using (var mapiObject = new MapiObject(_recipient.AddressEntry.MAPIOBJECT))
                        {
                            var property = mapiObject.GetProperty(MapiTags.PidTagPrimarySmtpAddress);
                            if (property != null)
                            {
                                _smtpAddress = property.ToString();
                            }
                            else
                            {
                                _smtpAddress = _recipient.Address;
                            }
                        }
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
                return _smtpAddress;
            }
        }
        protected override List<FreeBusyInfo> GetFreeBusyFromDefaultCalendarFolder(DateTime start, DateTime end)
        {
            var freeBusyInfos = new List<FreeBusyInfo>();
            if (_calendarFolder == null)
            {
                if (_recipient.Resolved)
                {
                    MyVrmAddin.TraceSource.TraceInformation("GetFreeBusyFromDefaultCalendarFolder: GetNamespace");
                    var nameSpace = _recipient.Application.GetNamespace("MAPI");
                    try
                    {
                        if (_recipient.Application.Session.CurrentUser.EntryID == _recipient.EntryID)
                        {
                            MyVrmAddin.TraceSource.TraceInformation("GetFreeBusyFromDefaultCalendarFolder: GetDefaultFolder");
                            _calendarFolder =
                                nameSpace.GetDefaultFolder(
                                    OlDefaultFolders.olFolderCalendar);
                        }
                        else
                        {
                            object addressType;
                            using (var mapiobject = new MapiObject(_recipient.AddressEntry.MAPIOBJECT))
                            {
                                addressType = mapiobject.GetProperty(MapiTags.PidTagAddressType);
                            }
                            if (addressType != null && string.Equals(addressType.ToString(), "EX", StringComparison.InvariantCultureIgnoreCase))
                            {
                                MyVrmAddin.TraceSource.TraceInformation(
                                    "GetFreeBusyFromDefaultCalendarFolder: GetSharedDefaultFolder");
                                _calendarFolder =
                                    nameSpace.GetSharedDefaultFolder(_recipient,
                                                                     OlDefaultFolders.
                                                                         olFolderCalendar);
                            }
                        }
                    }
                    finally
                    {
                        Marshal.ReleaseComObject(nameSpace);
                    }
                }
            }
            if (_calendarFolder != null)
            {
                MyVrmAddin.TraceSource.TraceInformation("GetFreeBusyFromDefaultCalendarFolder: foreach AppointmentItems");

                var appointments =
                    _calendarFolder.Items.OfType<AppointmentItem>().Where(
                        apptItem => apptItem.Start >= start && apptItem.End <= end);
                foreach (var appointment in appointments)
                {
                    using (var outlookAppointmentImpl = new OutlookAppointmentImpl(appointment))
                    {
                        if (outlookAppointmentImpl.IsRecurring)
                        {
                            var recurrencePattern =
                                outlookAppointmentImpl.GetRecurrencePattern(outlookAppointmentImpl.StartTimeZone);
                            IEnumerable<OccurrenceInfo> occurrenceInfos =
                                recurrencePattern.GetOccurrenceInfoList(start, end);
                            freeBusyInfos.AddRange(
                                occurrenceInfos.Select(
                                    occurrenceInfo =>
                                    new FreeBusyInfo(outlookAppointmentImpl.BusyStatus, occurrenceInfo.Start,
                                                     occurrenceInfo.End, outlookAppointmentImpl.Subject)));
                        }
                        else
                        {
                            if (outlookAppointmentImpl.Start >= start && outlookAppointmentImpl.End <= end)
                            {
                                freeBusyInfos.Add(new FreeBusyInfo(outlookAppointmentImpl.BusyStatus,
                                                                   outlookAppointmentImpl.Start,
                                                                   outlookAppointmentImpl.End,
                                                                   outlookAppointmentImpl.Subject));
                            }
                        }
                    }
                }
            }
            return freeBusyInfos;
        }
    }
}