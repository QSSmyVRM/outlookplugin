﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using Microsoft.Office.Tools.Ribbon;

namespace MyVrmAddin2007
{
    partial class ConferenceRibbon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabMyVRM = this.Factory.CreateRibbonTab();
            this.groupShow = this.Factory.CreateRibbonGroup();
            this.templatesGroup = this.Factory.CreateRibbonGroup();
            this.separator1 = this.Factory.CreateRibbonSeparator();
            this.generalGroup = this.Factory.CreateRibbonGroup();
            this.preferencesGroup = this.Factory.CreateRibbonGroup();
            this.optionsGroup = this.Factory.CreateRibbonGroup();
            this.appointmentButton = this.Factory.CreateRibbonButton();
            this.conferenceButton = this.Factory.CreateRibbonButton();
            this.templateMenu = this.Factory.CreateRibbonMenu();
            this.saveAsTemplateBn = this.Factory.CreateRibbonButton();
            this.roomsCalendarButton = this.Factory.CreateRibbonButton();
            this.favoriteRoomsButton = this.Factory.CreateRibbonButton();
            this.optionsButton = this.Factory.CreateRibbonButton();
            this.tabMyVRM.SuspendLayout();
            this.groupShow.SuspendLayout();
            this.templatesGroup.SuspendLayout();
            this.generalGroup.SuspendLayout();
            this.preferencesGroup.SuspendLayout();
            this.optionsGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabMyVRM
            // 
            this.tabMyVRM.Groups.Add(this.groupShow);
            this.tabMyVRM.Groups.Add(this.templatesGroup);
            this.tabMyVRM.Groups.Add(this.generalGroup);
            this.tabMyVRM.Groups.Add(this.preferencesGroup);
            this.tabMyVRM.Groups.Add(this.optionsGroup);
            this.tabMyVRM.Label = "myVRM";
            this.tabMyVRM.Name = "tabMyVRM";
            // 
            // groupShow
            // 
            this.groupShow.Items.Add(this.appointmentButton);
            this.groupShow.Items.Add(this.conferenceButton);
            this.groupShow.Label = "Show";
            this.groupShow.Name = "groupShow";
            // 
            // templatesGroup
            // 
            this.templatesGroup.Items.Add(this.templateMenu);
            this.templatesGroup.Items.Add(this.separator1);
            this.templatesGroup.Items.Add(this.saveAsTemplateBn);
            this.templatesGroup.Label = "Templates";
            this.templatesGroup.Name = "templatesGroup";
            // 
            // separator1
            // 
            this.separator1.Name = "separator1";
            // 
            // generalGroup
            // 
            this.generalGroup.Items.Add(this.roomsCalendarButton);
            this.generalGroup.Label = "General";
            this.generalGroup.Name = "generalGroup";
            // 
            // preferencesGroup
            // 
            this.preferencesGroup.Items.Add(this.favoriteRoomsButton);
            this.preferencesGroup.Label = "Preferences";
            this.preferencesGroup.Name = "preferencesGroup";
            // 
            // optionsGroup
            // 
            this.optionsGroup.Items.Add(this.optionsButton);
            this.optionsGroup.Label = "Options";
            this.optionsGroup.Name = "optionsGroup";
            // 
            // appointmentButton
            // 
            this.appointmentButton.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.appointmentButton.Label = "Appointment";
            this.appointmentButton.Name = "appointmentButton";
            this.appointmentButton.OfficeImageId = "ShowAppointmentPage";
            this.appointmentButton.ShowImage = true;
            // 
            // conferenceButton
            // 
            this.conferenceButton.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.conferenceButton.Image = global::MyVrmAddin2007.Properties.Resources.Logo65x65;
            this.conferenceButton.Label = "Conference";
            this.conferenceButton.Name = "conferenceButton";
            this.conferenceButton.ShowImage = true;
            // 
            // templateMenu
            // 
            this.templateMenu.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.templateMenu.Dynamic = true;
            this.templateMenu.Label = "Create From Template";
            this.templateMenu.Name = "templateMenu";
            this.templateMenu.OfficeImageId = "CreateForm";
            this.templateMenu.ShowImage = true;
            // 
            // saveAsTemplateBn
            // 
            this.saveAsTemplateBn.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.saveAsTemplateBn.Label = "Save As Template";
            this.saveAsTemplateBn.Name = "saveAsTemplateBn";
            this.saveAsTemplateBn.OfficeImageId = "FileSaveAs";
            this.saveAsTemplateBn.ShowImage = true;
            // 
            // roomsCalendarButton
            // 
            this.roomsCalendarButton.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.roomsCalendarButton.Label = "Rooms Calendar";
            this.roomsCalendarButton.Name = "roomsCalendarButton";
            this.roomsCalendarButton.OfficeImageId = "OpenAttachedCalendar";
            this.roomsCalendarButton.ShowImage = true;
            // 
            // favoriteRoomsButton
            // 
            this.favoriteRoomsButton.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.favoriteRoomsButton.Image = global::MyVrmAddin2007.Properties.Resources.Favorites;
            this.favoriteRoomsButton.Label = "Favorite Rooms";
            this.favoriteRoomsButton.Name = "favoriteRoomsButton";
            this.favoriteRoomsButton.ShowImage = true;
            // 
            // optionsButton
            // 
            this.optionsButton.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.optionsButton.Label = "Options";
            this.optionsButton.Name = "optionsButton";
            this.optionsButton.OfficeImageId = "ControlsGallery";
            this.optionsButton.ShowImage = true;
            // 
            // ConferenceRibbon
            // 
            this.Name = "ConferenceRibbon";
            this.RibbonType = "Microsoft.Outlook.Appointment";
            this.Tabs.Add(this.tabMyVRM);
            this.tabMyVRM.ResumeLayout(false);
            this.tabMyVRM.PerformLayout();
            this.groupShow.ResumeLayout(false);
            this.groupShow.PerformLayout();
            this.templatesGroup.ResumeLayout(false);
            this.templatesGroup.PerformLayout();
            this.generalGroup.ResumeLayout(false);
            this.generalGroup.PerformLayout();
            this.preferencesGroup.ResumeLayout(false);
            this.preferencesGroup.PerformLayout();
            this.optionsGroup.ResumeLayout(false);
            this.optionsGroup.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tabMyVRM;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup generalGroup;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton roomsCalendarButton;
		internal Microsoft.Office.Tools.Ribbon.RibbonGroup templatesGroup;
		internal Microsoft.Office.Tools.Ribbon.RibbonButton saveAsTemplateBn;
		internal Microsoft.Office.Tools.Ribbon.RibbonSeparator separator1;
		internal Microsoft.Office.Tools.Ribbon.RibbonGroup preferencesGroup;
		internal Microsoft.Office.Tools.Ribbon.RibbonButton favoriteRoomsButton;
		internal RibbonMenu templateMenu;
		internal RibbonGroup groupShow;
		internal RibbonButton conferenceButton;
		internal RibbonButton appointmentButton;
		internal RibbonGroup optionsGroup;
		internal RibbonButton optionsButton;
    }

    partial class ThisRibbonCollection : Microsoft.Office.Tools.Ribbon.RibbonReadOnlyCollection
    {
        internal ConferenceRibbon ConferenceRibbon
        {
            get { return this.GetRibbon<ConferenceRibbon>(); }
        }
    }
}
