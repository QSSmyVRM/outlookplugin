﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System.Collections.Generic;

namespace MyVrm.WebServices.Data
{
	public class RoomIdCollection : RoomIdCollectionBase //ComplexPropertyCollection<RoomId>
    {
		internal RoomIdCollection()
			: base("level1ID")
        {
        }

		//public new RoomId this[int index]
		//{
		//    get
		//    {
		//        return base[index];
		//    }
		//    set
		//    {
		//        base[index] = value;
		//    }
		//}

		//public void Add(RoomId roomId)
		//{
		//    InternalAdd(roomId);
		//}

		//public void AddRange(IEnumerable<RoomId> roomIds )
		//{
		//    InternalAddRange(roomIds);
		//}

		//public void Remove(RoomId roomId)
		//{
		//    InternalRemove(roomId);
		//}

		//public void RemoveAt(int index)
		//{
		//    InternalRemoveAt(index);
		//}

		//public void Clear()
		//{
		//    InternalClear();
		//}

		//public void Insert(int index, RoomId roomId)
		//{
		//   Items.Insert(index, roomId);
		//}

		#region Overrides of ComplexPropertyCollection<RoomId>

		//internal override RoomId CreateComplexProperty(string xmlElementName)
		//{
		//    return new RoomId();
		//}

		//internal override string GetCollectionItemXmlElementName(RoomId complexProperty)
		//{
		//    return "level1ID";
		//}

        internal override void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
        {
            reader.EnsureCurrentNodeIsStartElement(XmlNamespace.NotSpecified, xmlElementName);
            do
            {
                reader.Read();
                if (reader.IsStartElement(XmlNamespace.NotSpecified, "selected"))
                {
                    base.LoadFromXml(reader, "selected");        
                }
                else
                {
                    reader.SkipCurrentElement();
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, xmlElementName));
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            writer.WriteStartElement(XmlNamespace.NotSpecified, "selected");
            base.WriteElementsToXml(writer);
            writer.WriteEndElement();
        }
        #endregion
    }
}
