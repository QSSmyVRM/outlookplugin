﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    public class InventorySetItemCollection : ComplexPropertyCollection<InventorySetItem>
    {
        #region Overrides of ComplexPropertyCollection<InventorySetItem>

		public override object Clone()
		{
			InventorySetItemCollection copy = new InventorySetItemCollection();
			foreach (var item in this)
			{
				copy.InternalAdd((InventorySetItem)item.Clone()); //???????
			}

			return copy;
		}

        internal override InventorySetItem CreateComplexProperty(string xmlElementName)
        {
            if (xmlElementName == "Item")
            {
                return new InventorySetItem();
            }
            return null;
        }

        internal override string GetCollectionItemXmlElementName(InventorySetItem complexProperty)
        {
            return "Item";
        }

        #endregion
    }
}
