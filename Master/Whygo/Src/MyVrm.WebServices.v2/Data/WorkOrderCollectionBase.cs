﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace MyVrm.WebServices.Data
{
    public abstract class WorkOrderCollectionBase<TWorkOrder> : ComplexProperty, IEnumerable<TWorkOrder>, IOwnedProperty where TWorkOrder : WorkOrderBase
    {
        private readonly List<TWorkOrder> _workOrders = new List<TWorkOrder>();
        private readonly List<TWorkOrder> _addedWorkOrders = new List<TWorkOrder>();
        private readonly List<TWorkOrder> _modifiedWorkOrders = new List<TWorkOrder>();
        private readonly List<TWorkOrder> _removedWorkOrders = new List<TWorkOrder>();

        private Conference _owner;

        public void Add(TWorkOrder workOrder)
        {
            InternalAdd(workOrder, false);
        }

        public bool Remove(TWorkOrder workOrder)
        {
            if (!WorkOrders.Remove(workOrder))
            {
                return false;
            }
            workOrder.OnChange -= WorkOrderChanged;
            if (!AddedWorkOrders.Contains(workOrder))
            {
                RemovedWorkOrders.Add(workOrder);
            }
            else
            {
                AddedWorkOrders.Remove(workOrder);
            }
            ModifiedWorkOrders.Remove(workOrder);
            Changed();
            return true;
        }

        public void RemoveAt(int index)
        {
            Remove(WorkOrders[index]);
        }

        public void Clear()
        {
            while(Count > 0)
            {
                RemoveAt(0);
            }
        }

        public bool Contains(TWorkOrder workOrder)
        {
            return _workOrders.Contains(workOrder);
        }

        public TWorkOrder this[int index]
        {
            get
            {
                return WorkOrders[index];
            }
            set
            {
                WorkOrders[index] = value;
                Changed();
            }
        }

        public TWorkOrder this[RoomId roomId]
        {
            get
            {
                return WorkOrders.FirstOrDefault(workorder => workorder.RoomId == roomId);
            }
        }

        public int Count
        {
            get
            {
                return WorkOrders.Count;
            }
        }

        internal List<TWorkOrder> WorkOrders
        {
            get { return _workOrders; }
        }

        internal List<TWorkOrder> AddedWorkOrders
        {
            get { return _addedWorkOrders; }
        }

        internal List<TWorkOrder> ModifiedWorkOrders
        {
            get { return _modifiedWorkOrders; }
        }

        internal List<TWorkOrder> RemovedWorkOrders
        {
            get { return _removedWorkOrders; }
        }

        #region Implementation of IEnumerable

        public IEnumerator<TWorkOrder> GetEnumerator()
        {
            return WorkOrders.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region Implementation of IOwnedProperty

        ServiceObject IOwnedProperty.Owner
        {
            get { return Conference; }
            set
            {
                var conf = value as Conference;
                if (conf == null)
                {
                    throw new ArgumentException("value is not an instance of Conference class");
                }
                _owner = value as Conference;
            }
        }

        protected Conference Conference
        {
            get { return _owner; }
        }

        #endregion

        internal override void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
        {
            reader.EnsureCurrentNodeIsStartElement(XmlNamespace.NotSpecified, xmlElementName);
            if (!reader.IsEmptyElement)
            {
                do
                {
                    reader.Read();
                    if (reader.LocalName == "WorkOrder")
                    {
                        var workOrder = (TWorkOrder)Activator.CreateInstance(typeof(TWorkOrder), reader.Service);
                        workOrder.LoadFromXml(reader, true);
                        InternalAdd(workOrder, true);
                    }
                    else
                    {
                        reader.SkipCurrentElement();
                    }
                } while (!reader.IsEndElement(XmlNamespace.NotSpecified, xmlElementName));
            }
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            foreach (var workOrder in WorkOrders)
            {
                workOrder.WriteToXml(writer);
            }            
        }

        internal override void ClearChangeLog()
        {
            base.ClearChangeLog();
            
            AddedWorkOrders.Clear();
            ModifiedWorkOrders.Clear();
            RemovedWorkOrders.Clear();
        }

        internal abstract void Save();

        private void InternalAdd(TWorkOrder workOrder, bool loading)
        {
            if (!WorkOrders.Contains(workOrder))
            {
                WorkOrders.Add(workOrder);
                if (!loading)
                {
                    RemovedWorkOrders.Remove(workOrder);
                    AddedWorkOrders.Add(workOrder);
                }
                workOrder.OnChange += WorkOrderChanged;
                Changed();
            }
        }

        private void WorkOrderChanged(object sender, EventArgs eventArgs)
        {
            var item = sender as TWorkOrder;
            if (!AddedWorkOrders.Contains(item) && !ModifiedWorkOrders.Contains(item))
            {
                ModifiedWorkOrders.Add(item);
                Changed();
            }
        }

    }
}
