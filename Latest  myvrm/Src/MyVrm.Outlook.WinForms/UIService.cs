/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace MyVrm.Outlook.WinForms
{
    /// <summary>
    /// Provides UI service.
    /// </summary>
    public class UIService : IUIService
    {
        private Hashtable _styles;
        private readonly IWin32Window _parentWindow;
        private static string _defaultCaption;

        static UIService()
        {
			DefaultCaption = MyVrmAddin.ProductDisplayName;//Strings.DefaultCaption;
        }

        /// <summary>
        /// Initializes a new instance of <see cref="UIService"/> class.
        /// </summary>
        /// <param name="parentWindow"></param>
        public UIService(IWin32Window parentWindow)
        {
            this._parentWindow = parentWindow;
        }

        /// <summary>
        /// Gets or sets default caption for forms, dialogs.
        /// </summary>
        public static string DefaultCaption
        {
            get { return _defaultCaption; }
            set
            {
                value = value ?? string.Empty;
                _defaultCaption = value;
            }
        }

        #region IUIService Members

        ///<summary>
        ///Indicates whether the component can display a <see cref="T:System.Windows.Forms.Design.ComponentEditorForm"></see>.
        ///</summary>
        ///
        ///<returns>
        ///true if the specified component can display a component editor form; otherwise, false.
        ///</returns>
        ///
        ///<param name="component">The component to check for support for displaying a <see cref="T:System.Windows.Forms.Design.ComponentEditorForm"></see>. </param>
        public bool CanShowComponentEditor(object component)
        {
            var editor = (ComponentEditor) TypeDescriptor.GetEditor(component, typeof (ComponentEditor));
            return (null != editor);
        }

        ///<summary>
        ///Gets the window that should be used as the owner when showing dialog boxes.
        ///</summary>
        ///
        ///<returns>
        ///An <see cref="T:System.Windows.Forms.IWin32Window"></see> that indicates the window to own any child dialog boxes.
        ///</returns>
        ///
        public IWin32Window GetDialogOwnerWindow()
        {
            return _parentWindow;
        }

        ///<summary>
        ///Sets a flag indicating the UI has changed.
        ///</summary>
        ///
        public void SetUIDirty()
        {
        }

        ///<summary>
        ///Attempts to display a <see cref="T:System.Windows.Forms.Design.ComponentEditorForm"></see> for a component.
        ///</summary>
        ///
        ///<returns>
        ///true if the attempt is successful; otherwise, false.
        ///</returns>
        ///
        ///<param name="component">The component for which to display a <see cref="T:System.Windows.Forms.Design.ComponentEditorForm"></see>. </param>
        ///<param name="parent">The <see cref="T:System.Windows.Forms.IWin32Window"></see> to parent any dialog boxes to. </param>
        ///<exception cref="T:System.ArgumentException">The component does not support component editors. </exception>
        public bool ShowComponentEditor(object component, IWin32Window parent)
        {
            var editor = (ComponentEditor) TypeDescriptor.GetEditor(component, typeof (ComponentEditor));
            if (editor == null)
            {
                return false;
            }
            var componentEditor = editor as WindowsFormsComponentEditor;
            if (componentEditor != null)
            {
                if (parent == null)
                {
                    parent = GetDialogOwnerWindow();
                }
                return componentEditor.EditComponent(component, parent);
            }
            return editor.EditComponent(component);
        }

        ///<summary>
        ///Attempts to display the specified form in a dialog box.
        ///</summary>
        ///
        ///<returns>
        ///One of the <see cref="T:System.Windows.Forms.DialogResult"></see> values indicating the result code returned by the dialog box.
        ///</returns>
        ///
        ///<param name="form">The <see cref="T:System.Windows.Forms.Form"></see> to display. </param>
        public DialogResult ShowDialog(System.Windows.Forms.Form form)
        {
            IContainer container = null;
            DialogResult result;
            var control = GetDialogOwnerWindow() as Control;
            if (control != null)
            {
                container = control.Container;
                if (container != null)
                {
                    container.Add(form, form.Name + form.GetHashCode());
                }
            }
            if (string.IsNullOrEmpty(form.Text))
            {
                form.Text = DefaultCaption;
            }
            Control focusedControlOnPropertyPage = GetFocusedControl();
            try
            {
                result = OnShowDialog(form);
            }
            finally
            {
                if (container != null)
                {
                    container.Remove(form);
                }
                if (focusedControlOnPropertyPage != null)
                {
                    focusedControlOnPropertyPage.Focus();
                }
            }
            return result;
        }

        ///<summary>
        ///Displays the specified error message in a message box.
        ///</summary>
        ///
        ///<param name="message">The error message to display. </param>
        public void ShowError(string message)
        {
            MessageBox(message, DefaultCaption, MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }

        ///<summary>
        ///Displays the specified exception and information about the exception in a message box.
        ///</summary>
        ///
        ///<param name="ex">The <see cref="T:System.Exception"></see> to display. </param>
        public void ShowError(Exception ex)
        {
            ShowError(ex, String.Empty);
        }

        ///<summary>
        ///Displays the specified exception and information about the exception in a message box.
        ///</summary>
        ///
        ///<param name="message">A message to display that provides information about the exception. </param>
        ///<param name="ex">The <see cref="T:System.Exception"></see> to display. </param>
        public void ShowError(Exception ex, string message)
        {
            ShowError(ex.Message);
        }

        ///<summary>
        ///Displays the specified message in a message box.
        ///</summary>
        ///
        ///<param name="message">The message to display </param>
        public void ShowMessage(string message)
        {
            MessageBox(message, DefaultCaption, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        ///<summary>
        ///Displays the specified message in a message box with the specified caption.
        ///</summary>
        ///
        ///<param name="message">The message to display. </param>
        ///<param name="caption">The caption for the message box. </param>
        public void ShowMessage(string message, string caption)
        {
            MessageBox(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        ///<summary>
        ///Displays the specified message in a message box with the specified caption and buttons to place on the dialog box.
        ///</summary>
        ///
        ///<returns>
        ///One of the <see cref="T:System.Windows.Forms.DialogResult"></see> values indicating the result code returned by the dialog box.
        ///</returns>
        ///
        ///<param name="message">The message to display. </param>
        ///<param name="caption">The caption for the dialog box. </param>
        ///<param name="buttons">One of the <see cref="T:System.Windows.Forms.MessageBoxButtons"></see> values: <see cref="F:System.Windows.Forms.MessageBoxButtons.OK"></see>, <see cref="F:System.Windows.Forms.MessageBoxButtons.OKCancel"></see>, <see cref="F:System.Windows.Forms.MessageBoxButtons.YesNo"></see>, or <see cref="F:System.Windows.Forms.MessageBoxButtons.YesNoCancel"></see>. </param>
        public DialogResult ShowMessage(string message, string caption, MessageBoxButtons buttons)
        {
            return MessageBox(message, caption, buttons, MessageBoxIcon.Asterisk);
        }

        ///<summary>
        ///Displays the specified tool window.
        ///</summary>
        ///
        ///<returns>
        ///true if the tool window was successfully shown; false if it could not be shown or found.
        ///</returns>
        ///
        ///<param name="toolWindow">A <see cref="T:System.Guid"></see> identifier for the tool window. This can be a custom <see cref="T:System.Guid"></see> or one of the predefined values from <see cref="T:System.ComponentModel.Design.StandardToolWindows"></see>. </param>
        public bool ShowToolWindow(Guid toolWindow)
        {
            return false;
        }

        ///<summary>
        ///Gets the collection of styles that are specific to the host's environment.
        ///</summary>
        ///
        ///<returns>
        ///An <see cref="T:System.Collections.IDictionary"></see> containing style settings.
        ///</returns>
        ///
        public IDictionary Styles
        {
            get
            {
                if (_styles == null)
                {
                    _styles = new Hashtable(2);
                    _styles["DialogFont"] = SystemFonts.DialogFont;
                    _styles["HighlightColor"] = SystemColors.Highlight;
                }
                return _styles;
            }
        }

        #endregion

        protected virtual DialogResult MessageBox(string message, string caption, MessageBoxButtons buttons,
                                                  MessageBoxIcon icon)
        {
            return System.Windows.Forms.MessageBox.Show(_parentWindow, message, caption, buttons, icon);
        }

        protected virtual DialogResult OnShowDialog(System.Windows.Forms.Form form)
        {
            return form.ShowDialog(GetDialogOwnerWindow());
        }

        private Control GetFocusedControl()
        {
            var parent = GetDialogOwnerWindow() as Control;
            if ((parent != null) && (parent.Parent != null))
            {
                while (parent.Parent.Parent != null)
                {
                    parent = parent.Parent;
                }
            }
            return null;
        }
    }
}