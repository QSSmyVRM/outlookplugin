﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace MyVrm.WebServices.Data
{
    internal class GetInventoryDetailsRequest : ServiceRequestBase<GetInventoryDetailsResponse>
    {
        internal GetInventoryDetailsRequest(MyVrmService service) : base(service)
        {
        }

        internal int SetId { get; set; }
        
        internal RoomSetType SetType { get; set; }

        internal string PreselectedItems { get; set; }

        internal DateTime WorkOrderStart { get; set; }
        internal DateTime WorkOrderComplete { get; set; }

        internal TimeZoneInfo WoTimeZone{get;set;}

        #region Overrides of ServiceRequestBase

        internal override string GetXmlElementName()
        {
            return "login";
        }

        internal override string GetCommandName()
        {
            return "GetInventoryDetails";
        }

        internal override string GetResponseXmlElementName()
        {
            return "Inventory";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            UserId.WriteToXml(writer);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "Type", SetType);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "ID", SetId);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "StartByDate", "");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "StartByTime", "");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "CompleteByDate", "");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "CompleteByTime", "");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "Timezone", "");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "WorkorderID", "");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "PreselectedItems", "");
            //writer.WriteStartElement(XmlNamespace.NotSpecified, "Item");
            //writer.WriteElementValue(XmlNamespace.NotSpecified, "ID", "");
            //writer.WriteElementValue(XmlNamespace.NotSpecified, "UID", "");
            //writer.WriteElementValue(XmlNamespace.NotSpecified, "Quantity", "");
            //writer.WriteEndElement();
            //writer.WriteEndElement();
        }

        //internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        //{
        //    UserId.WriteToXml(writer);
        //    writer.WriteElementValue(XmlNamespace.NotSpecified, "Type", SetType);
        //    writer.WriteElementValue(XmlNamespace.NotSpecified, "ID", SetId);
        //    writer.WriteElementValue(XmlNamespace.NotSpecified, "StartByDate", WorkOrderStart.ToString("MM/dd/yyyy"));
        //    writer.WriteElementValue(XmlNamespace.NotSpecified, "StartByTime", WorkOrderStart.ToString("HH:mm"));
        //    writer.WriteElementValue(XmlNamespace.NotSpecified, "CompleteByDate", WorkOrderComplete.ToString("MM/dd/yyyy"));
        //    writer.WriteElementValue(XmlNamespace.NotSpecified, "CompleteByTime", WorkOrderComplete.ToString("HH:mm"));
        //    writer.WriteElementValue(XmlNamespace.NotSpecified, "Timezone", 26);
        //    writer.WriteElementValue(XmlNamespace.NotSpecified, "WorkorderID", "");
        //    writer.WriteElementValue(XmlNamespace.NotSpecified, "PreselectedItems", PreselectedItems);//103771
        //}

        #endregion
    }

    internal class GetInventoryWOAdminRequest : ServiceRequestBase<GetInventoryWOAdminResponse>
    {
        internal GetInventoryWOAdminRequest(MyVrmService service)
            : base(service)
        {
        }

        internal int SetId { get; set; }

        internal RoomSetType SetType { get; set; }


        #region Overrides of ServiceRequestBase

        internal override string GetXmlElementName()
        {
            return "login";
        }

        internal override string GetCommandName()
        {
            return "GetInventoryDetails";
        }

        internal override string GetResponseXmlElementName()
        {
            return "Inventory";
        }

        internal override GetInventoryWOAdminResponse ParseResponse(MyVrmServiceXmlReader reader)
        {
            var response = new GetInventoryWOAdminResponse();
            response.LoadFromXml(reader, GetResponseXmlElementName());
            return response;
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            UserId.WriteToXml(writer);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "Type", SetType);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "ID", SetId);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "StartByDate", "");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "StartByTime", "");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "CompleteByDate", "");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "CompleteByTime", "");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "Timezone", "");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "WorkorderID", "");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "PreselectedItems", "");
        }

        #endregion
    }
    public class GetInventoryWOAdminResponse : ServiceResponse
    {
        public int ID { get; internal set; }
        public string AssignedToName { get; internal set; }
        public int Quantity { get; internal set; }

        internal void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
        {
            try
            {
                do
                {
                    reader.Read();
                    if (reader.LocalName != "" && reader.LocalName != null)
                    {
                        switch (reader.LocalName)
                        {
                            case "Admin":
                                LoadFromXml(reader, "Admin");
                                break;
                            case "ID":
                                ID = reader.ReadElementValue<int>();
                                break;
                            case "AssignedToName":
                                AssignedToName = reader.ReadElementValue();
                                break;
                            case "Quantity":
                                Quantity = reader.ReadElementValue<int>();
                                break;
                            default:
                                reader.SkipCurrentElement();
                                break;
                        }
                    }
                } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "Inventory"));
            }
            catch (Exception ex)
            {
                MyVrmService.TraceSource.TraceError(reader.LocalName + ": " + ex.Message, null);
            }
        }

    }


  
}
