﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.Common.Collections.Generic
{
    public class TreeNode<T>
    {
        private TreeNodeList<T> _nodes;
        private TreeNode<T> _parent;

        public TreeNode(T value)
        {
            Value = value;    
        }

        public T Value { get; set; }

        public TreeNodeList<T> Nodes
        {
            get
            {
                if (_nodes == null)
                {
                    _nodes = new TreeNodeList<T>(this);
                }
                return _nodes;
            }
        }

        public TreeNode<T> Parent
        {
            get { return _parent; }
            set
            {
                if (value == _parent)
                {
                    return;
                }
                if (_parent != null)
                {
                    _parent.Nodes.Remove(this);
                }
                _parent = value;
            }
        }
    }
}
