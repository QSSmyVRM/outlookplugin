﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.Outlook.WinForms.Conference
{
    public partial class AdditionalOptionsPage : ConferencePage
    {
        public AdditionalOptionsPage()
        {
            Text = Strings.AdditionalOptionsPageText;
            InitializeComponent();
            ConferenceBindingSource.DataSourceChanged += ConferenceBindingSource_DataSourceChanged;
        }

        protected override void OnApplying(System.ComponentModel.CancelEventArgs e)
        {
            base.OnApplying(e);
            Conference.SpecialInstructions = specialInstructionsEdit.Text;
        }

        protected override void OnReadOnlyChanged(ReadOnlyChangedEventArgs e)
        {
            base.OnReadOnlyChanged(e);
            specialInstructionsEdit.Enabled = !e.ReadOnly;
        }

        void ConferenceBindingSource_DataSourceChanged(object sender, System.EventArgs e)
        {
            specialInstructionsEdit.Text = Conference.SpecialInstructions;
        }

        private void AdditionalInfoPage_Load(object sender, System.EventArgs e)
        {
        }

        private void specialInstructionsEdit_EditValueChanged(object sender, System.EventArgs e)
        {
        }

		void specialInstructionsEdit_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
		{
			if (e.OldValue != e.NewValue && e.OldValue != null)
			{
				if (Conference != null)
					Conference.Appointment.SetNonOutlookProperty(true);
			}
		}
    }
}
