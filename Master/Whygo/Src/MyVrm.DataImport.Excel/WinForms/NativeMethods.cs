using System;
using System.Runtime.InteropServices;

namespace MyVrm.DataImport.Excel.WinForms
{
    internal abstract class NativeMethods
    {
        public const int HDI_WIDTH = 0x0001;
        public const int HDI_HEIGHT = HDI_WIDTH;
        public const int HDI_TEXT = 0x0002;
        public const int HDI_FORMAT = 0x0004;
        public const int HDI_LPARAM = 0x0008;
        public const int HDI_BITMAP = 0x0010;

        public const int LVM_GETHEADER = 0x101f;
        public const int LVM_SETITEMW = 0x104e;

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        public struct HDITEM
        {
            public uint mask;
            public int cxy;
            public string pszText;
            public IntPtr hbm;
            public int cchTextMax;
            public int fmt;
            public IntPtr lParam;
            public int iImage;
            public int iOrder;
            public uint type;
            public IntPtr pvFilter;
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        public struct LVITEM
        {
            public int mask;
            public int iItem;
            public int iSubItem;
            public int state;
            public int stateMask;
            public string pszText;
            public int cchTextMax;
            public int iImage;
            public IntPtr lParam;
            public int iIndent;
            public int iGroupId;
            public int cColumns;
            public IntPtr puColumns;
        }

        public static readonly int HDM_GETITEM;
        public static readonly int HDM_SETITEM;

        static NativeMethods()
        {
            if (Marshal.SystemDefaultCharSize == 1)
            {
                HDM_GETITEM = 0x1203;
                HDM_SETITEM = 0x1204;
            }
            else
            {
                HDM_GETITEM = 0x120b;
                HDM_SETITEM = 0x120c;
            }
        }

        protected NativeMethods()
        {
        }

        public static int HIWORD(int n)
        {
            return ((n >> 0x10) & 0xffff);
        }

        public static int HIWORD(IntPtr n)
        {
            return HIWORD((int) ((long) n));
        }

        public static int LOWORD(int n)
        {
            return (n & 0xffff);
        }

        public static int LOWORD(IntPtr n)
        {
            return LOWORD((int) ((long) n));
        }
    }
}