﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System.Windows.Forms;
using System.Windows.Forms.Design;
using DevExpress.XtraEditors;

namespace MyVrm.Outlook.WinForms
{
    public class BaseControl : XtraUserControl
    {
        /// <summary>
        /// Shows error message.
        /// </summary>
        /// <param name="message">Error text.</param>
        public void ShowError(string message)
        {
            ShellUI.ShowError(message);
        }

        /// <summary>
        /// Shows informational message.
        /// </summary>
        /// <param name="message">Message text.</param>
        public void ShowMessage(string message)
        {
            ShellUI.ShowMessage(message);
        }

        public void ShowMessage(string message, string caption)
        {
            ShellUI.ShowMessage(message, caption);
        }
        /// <summary>
        /// Shows informational message.
        /// </summary>
        /// <param name="message">Message text.</param>
        /// <param name="buttons">Buttons.</param>
        /// <returns><see cref="DialogResult"/></returns>
        public DialogResult ShowMessage(string message, MessageBoxButtons buttons)
        {
            return ShellUI.ShowMessage(message, UIService.DefaultCaption, buttons);
        }
        /// <summary>
        /// Returns instance of <see cref="UIService"/>.
        /// </summary>
        public IUIService ShellUI
        {
            get
            {
                var service = (IUIService)GetService(typeof(IUIService));
                if (service == null)
                {
                    service = CreateUIService();
                }
                return service;
            }
        }

        protected virtual IUIService CreateUIService()
        {
            return new UIService(this);
        }
    }
}
