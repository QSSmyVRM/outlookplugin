﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.WebServices.Data
{
    internal class SaveConferenceResponse : ServiceResponse
    {
        internal ConferenceId ConferenceId { get; private set; }

        internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
        {
            reader.ReadStartElement(XmlNamespace.NotSpecified, "conferences");
            reader.ReadStartElement(XmlNamespace.NotSpecified, "conference");
            do
            {
                reader.Read();
                if (reader.IsStartElement(XmlNamespace.NotSpecified, "confID"))
                {
                    var conferenceId = new ConferenceId();
                    conferenceId.LoadFromXml(reader, "confID");
                    ConferenceId = conferenceId;
                }
                else
                {
                    reader.SkipCurrentElement();
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "conference"));
            reader.ReadEndElement(XmlNamespace.NotSpecified, "conferences");
        }
    }
}
