﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    public class ConferenceEndpointCollection : ComplexPropertyCollection<ConferenceEndpoint>
    {
		public override object Clone()
		{
			ConferenceEndpointCollection copy = new ConferenceEndpointCollection();
			foreach (var item in this)
			{
				copy.Add((ConferenceEndpoint)item.Clone()); 
			}

			return copy;
		}

        internal ConferenceEndpointCollection()
        {
        }

        public void Add(ConferenceEndpoint endpoint)
        {
            InternalAdd(endpoint);
        }

        public void Clear()
        {
            InternalClear();
        }

        public bool Remove(ConferenceEndpoint endpoint)
        {
            return InternalRemove(endpoint);
        }

        #region Overrides of ComplexPropertyCollection<ConferenceEndpoint>

        internal override ConferenceEndpoint CreateComplexProperty(string xmlElementName)
        {
            return new ConferenceEndpoint();
        }

        internal override string GetCollectionItemXmlElementName(ConferenceEndpoint complexProperty)
        {
            return "Endpoint";
        }

        #endregion

    }
}
