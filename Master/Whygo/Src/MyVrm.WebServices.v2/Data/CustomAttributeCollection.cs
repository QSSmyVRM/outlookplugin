﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.WebServices.Data
{
    /// <summary>
    /// Represents a list of custom attributes.
    /// </summary>
    public class CustomAttributeCollection : ComplexPropertyCollection<CustomAttribute>
    {
		public override object Clone()
		{
			CustomAttributeCollection copy = new CustomAttributeCollection();
			foreach (var item in Items)
			{
				copy.InternalAdd((CustomAttribute)item.Clone()); //???????
			}

			return copy;
		}

        public void Add(CustomAttribute customAttribute)
        {
            InternalAdd(customAttribute);
        }

        #region Overrides of ComplexPropertyCollection<CustomAttribute>

        internal override CustomAttribute CreateComplexProperty(string xmlElementName)
        {
            return new CustomAttribute();
        }

        internal override string GetCollectionItemXmlElementName(CustomAttribute complexProperty)
        {
            return "CustomAttribute";
        }

        #endregion
    }
}
