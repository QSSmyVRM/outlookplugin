﻿
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraTreeList.Nodes;
using MyVrm.Common.ComponentModel;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook.WinForms.Conference
{
    public partial class ExternalRoomsControl : BaseControl
    {
        private Collection<ExternalRoom> _rooms = null;//= new Collection<ExternalRoom>();
        public Collection<ExternalRoom> Rooms
        {
            get
            {
                return _rooms == null || _rooms.Count == 0 && Conference.ExternalParticipants.Count > 0 ? MakeRooms() : _rooms;
            }
        }
        public AudioVideoConferenceAVSettingsPage.AVPageType PageType;
        //ZD 101343 starts
        public int aCheck;
        // public int confLisNum = gtConfResponse.GetGuestUserLiscense;
        //->
   //ZD 101343 Ends
        private BindingSource _conferenceBindingSource;

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false),
         EditorBrowsable(EditorBrowsableState.Never)]
        public ConferenceWrapper Conference
        {
            get
            {
                return _conferenceBindingSource != null ? (ConferenceWrapper)_conferenceBindingSource.DataSource : null;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false),
         EditorBrowsable(EditorBrowsableState.Never)]
        public BindingSource ConferenceBindingSource
        {
            get
            {
                return _conferenceBindingSource;
            }
            set
            {
                if (_conferenceBindingSource != value)
                {
                    if (_conferenceBindingSource != null)
                    {
                        _conferenceBindingSource.DataSourceChanged -= ConferenceBindingSourceDataSourceChanged;
                    }
                    _conferenceBindingSource = null;
                }
                if (value != null)
                {
                    _conferenceBindingSource = value;
                    _conferenceBindingSource.DataSourceChanged += ConferenceBindingSourceDataSourceChanged;
                }
            }
        }
        protected virtual void OnConferenceBindingSourceDataSourceChanged(EventArgs e)
        {
        }

        private void ConferenceBindingSourceDataSourceChanged(object sender, EventArgs e)
        {
            OnConferenceBindingSourceDataSourceChanged(e);
        }
        // <-


        public ExternalRoomsControl()
        {
            InitializeComponent();
            _delegateExternalSetMCUProfile = new delegateExternalUpdateMCUProfile(ExternalUpdateMCUProfile);
            bnAddRoom.Text = Strings.TxtAddExternalRoom;
            bnEditRoom.Text = Strings.TxtEditSelectedRoom;
            bnRemoveRoom.Text = Strings.TxtRemoveSelectedRoom;
            

            emailCol.Caption = Strings.TxtExternalUserEmailColumn;
            addressCol.Caption = Strings.TxtAddressPhoneColumn;
            protocolCol.Caption = Strings.TxtProtocolColumn;
            mcuCol.Caption = Strings.TxtMCUColumn;
            callerColumn.Caption = Strings.TxtCallerCalleeColumn;
        }

        private void bnAddRoom_Click(object sender, EventArgs e)
        {
            bool flag = false;
            using (var dlg = new ExternalRoomDlg(Conference.gtConfResponse) { DlgMode = ExternalRoomDlg.Mode.Add, PageType = PageType })//ZD 101343 
            {
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    //ZD 101343 starts
                    if (dlg.Room.IsGuestLoc)//ZD 101343 
                    {
                        foreach (ExternalRoom cfRoom in _rooms)
                        {
                            if (cfRoom.EndPtName == dlg.Room.EndPtName.Trim())
                            {
                                UIHelper.ShowError(Strings.EndpointnameExists);
                                flag = true;
                                break;
                            }
                        }
                    }
                    //ZD 101343 Ends
                    if (!flag)
                    {
                        extUsersTreeList.BeginUnboundLoad();
                        _rooms.Add(dlg.Room);
                        extUsersTreeList.AppendNode(new object[] { dlg.Room.UserEmail, dlg.Room.AddressPhone, dlg.Room.ProtocolName, 
						dlg.Room.MCUName, dlg.Room.CallerCalleeName}, -1);
                        extUsersTreeList.EndUnboundLoad();
                        if (dlg.Room.MCU != null && dlg.Room.MCUProfile != null) //for non-PTP conf
                        {
                            ExternalUpdateMCUProfile(dlg.Room.MCU.FirstObj, dlg.Room.MCUProfile.FirstObj, dlg.Room.MCUProfile.SecondObj);
                            RoomAudioVideoSettingsListControl._delegateExternalSetMCUProfile(dlg.Room.MCU.FirstObj, dlg.Room.MCUName,
                                                                                             dlg.Room.MCUProfile.SecondObj);
                        }
                        if (dlg.Room.IsGuestLoc)
                        {
                            Conference.gtConfResponse--;

                        }
                        Conference.Appointment.SetNonOutlookProperty(true);
                    }
                }
            }
        }

        private void bnEditRoom_Click(object sender, EventArgs e)
        {
            bool flag = false;
           
            if (extUsersTreeList.FocusedNode != null)
            {
                ExternalRoom room = _rooms[extUsersTreeList.FocusedNode.Id];
                String strEndName = room.EndPtName;
                //ZD 101343 starts
                if (room != null)
                {
                    if (room.IsGuestLoc)
                    {
                        aCheck = 10;
                        Conference.gtConfResponse++;
                    }
                    else
                    {
                        aCheck = 5;
                    }
                    //ZD 101343 Ends
                    using (var dlg = new ExternalRoomDlg(Conference.gtConfResponse) { DlgMode = ExternalRoomDlg.Mode.Edit, PageType = PageType, Room = room })
                    {
                        //ZD 101343 starts
                        if (dlg.ShowDialog() == DialogResult.OK)
                        {
                            if (aCheck == 5)
                            {
                                if (dlg.Room.IsGuestLoc)
                                {
                                    foreach (ExternalRoom cfRoom in _rooms)
                                    {
                                        if (cfRoom.EndPtName == dlg.Room.EndPtName)
                                        {
                                            UIHelper.ShowError(Strings.EndpointnameExists);
                                            flag = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (dlg.Room.IsGuestLoc)
                                {
                                    foreach (ExternalRoom cfRoom in _rooms)
                                    {
                                        if (cfRoom.EndPtName == dlg.Room.EndPtName)
                                        {
                                            if (strEndName != dlg.Room.EndPtName)
                                            {
                                                UIHelper.ShowError(Strings.EndpointnameExistsOrginal);
                                                flag = true;
                                                break;
                                            }
                                        }
                                    }
                                }

                            }
                            //ZD 101343 Ends
                            if (!flag)
                            {
                                extUsersTreeList.FocusedNode[emailCol] = dlg.Room.UserEmail;
                                extUsersTreeList.FocusedNode[addressCol] = dlg.Room.AddressPhone;
                                extUsersTreeList.FocusedNode[protocolCol] = dlg.Room.ProtocolName;
                                extUsersTreeList.FocusedNode[mcuCol] = dlg.Room.MCUName;
                                extUsersTreeList.FocusedNode[callerColumn] = dlg.Room.CallerCalleeName;
                                _rooms.Remove(room);
                                _rooms.Insert(extUsersTreeList.FocusedNode.Id, dlg.Room);

                                if (dlg.Room.MCU != null && dlg.Room.MCUProfile != null) //for non-PTP conf
                                {
                                    ExternalUpdateMCUProfile(dlg.Room.MCU.FirstObj, dlg.Room.MCUProfile.FirstObj, dlg.Room.MCUProfile.SecondObj);
                                    RoomAudioVideoSettingsListControl._delegateExternalSetMCUProfile(dlg.Room.MCU.FirstObj, dlg.Room.MCUName,
                                                                                                     dlg.Room.MCUProfile.SecondObj);
                                }
                                if (dlg.Room.IsGuestLoc)
                                {
                                    Conference.gtConfResponse--;
                                }
                                Conference.Appointment.SetNonOutlookProperty(true);
                            }
                           
                        }
                        else
                        {
                            if (dlg.Room.IsGuestLoc)
                            {
                                Conference.gtConfResponse--;
                            }
                        }

                    }
                }
            }
        }

        private void bnRemoveRoom_Click(object sender, EventArgs e)
        {
            if (extUsersTreeList.FocusedNode != null)
            {
                ExternalRoom room = _rooms[extUsersTreeList.FocusedNode.Id];

                if (room != null)
                {
                    if (room.IsGuestLoc)
                        Conference.gtConfResponse++;
                    _rooms.Remove(room);
                    extUsersTreeList.Nodes.RemoveAt(extUsersTreeList.FocusedNode.Id);
                    Conference.Appointment.SetNonOutlookProperty(true);
                }
            }
        }

        public void SetColumnVisibility()
        {
            callerColumn.Visible = PageType == AudioVideoConferenceAVSettingsPage.AVPageType.PointToPoint;
            mcuCol.Visible = PageType != AudioVideoConferenceAVSettingsPage.AVPageType.PointToPoint;
            for (int i = 0; _rooms != null && i < _rooms.Count; i++)
            {
                extUsersTreeList.Nodes[i][callerColumn] = _rooms[i].CallerCalleeName;
                extUsersTreeList.Nodes[i][mcuCol] = _rooms[i].MCUName;
            }
        }

        private void ExternalRoomsControl_Load(object sender, EventArgs e)
        {
            if (!DesignMode)
            {
                foreach (var room in Rooms)
                {
                    extUsersTreeList.AppendNode(new object[]
					                            	{
					                            		room.UserEmail, room.AddressPhone, room.ProtocolName,
					                            		room.MCUName, room.CallerCalleeName
					                            	}, -1);
                }
            }
        }

        private Collection<ExternalRoom> MakeRooms()
        {
            if (DesignMode)
                return null;

            if (_rooms != null)
                _rooms.Clear();
            else
                _rooms = new Collection<ExternalRoom>();

            // ZD 101343 starts

            Collection<AudioConferenceBridge> Urs = Conference.ExternalRoomsAVSettings;
            Collection<AudioConferenceBridge> endpoints = Conference.ExternalRoomsAVSettings;
            DataList<ConfGuestRoom> Urse = Conference.ConfGuestRooms;
            DataList<ConferenceEndpoint> DlConfENdpt = Conference.RoomEndpoints;
            foreach (var user in Urse)
            {

                ExternalRoom room = new ExternalRoom();

                room.UserFirstName = user.FirstName;
                room.UserLastName = user.LastName;
                room.UserEmail = user.Email;
                room.UserId = user.UserId;
                room.IsGuestLoc = true;
                room.EndPtName = user.EndpointName;

                //  room.AddressType = user.AddressType;
                foreach (var ur in DlConfENdpt)
                {
                    if (ur.Id.ToString() == user.RoomID)
                    {
                        room.AddressPhone = ur.Address;
                        if (ur.AddressType > 0)
                        {
                            var val = MyVrmService.Service.AddressTypes.FirstOrDefault(a => a.Key == (int)ur.AddressType);
                            room.AddressType = new WrapPair<int, string>(val.Key, val.Value);
                        }
                        if (ur.Bandwidth != null)
                        {
                            var val = MyVrmService.Service.LineRates.FirstOrDefault(a => a.Key == (int)ur.Bandwidth);
                            room.LineRate = new WrapPair<int, string>(val.Key, val.Value);
                        }
                        if (ur.VideoEquipment != null)
                        {
                            var val = MyVrmService.Service.VideoEquipment.FirstOrDefault(a => a.Id == (int)ur.VideoEquipment);
                            if (val != null)
                                room.Equipment = new WrapPair<int, string>(val.Id, val.Name);
                        }
                        if (ur.DefaultProtocol != null)
                        {
                            var val = MyVrmService.Service.VideoProtocols.FirstOrDefault(a => a.Key == ur.DefaultProtocol);
                            room.ProtocolType = new WrapPair<int, string>(val.Key, val.Value);
                        }
                        if (ur.BridgeId != null)
                        {
                            var val = MyVrmService.Service.Bridges.FirstOrDefault(a => a.Id == ur.BridgeId);
                            if (val != null)
                                room.MCU = new WrapPair<BridgeId, string>(val.Id, val.Name);
                        }

                        if (ur.BridgeProfileID != null && ur.BridgeId != null)
                        {
                            var profiles = MyVrmService.Service.GetMCUProfiles(ur.BridgeId.Id);
                            var val = profiles.FirstOrDefault(a => a.Id == ur.BridgeProfileID);
                            if (val != null)
                                room.MCUProfile = new WrapPair<MCUProfileId, string>(val.Id, val.Name);
                        }

                        string callerName = Common.LocalizedDescriptionAttribute.FromEnum(ur.Caller.GetType(), ur.Caller);
                        room.CallerCallee = new WrapPair<int, string>((int)ur.Caller, callerName);

                        room.APIPort = ur.ApiPortNumber.ToString();
                        room.URL = ur.Url;
                        room.EndPointID = ur.EndpointId;
                        room.ID = ur.Id.ToString();


                    }
                }
             
                // room.AddressPhone  = user.Address;

                //if (user.MaxLineRate != 0)
                //{
                //    var val = MyVrmService.Service.LineRates.FirstOrDefault(a => a.Key == (int)user.MaxLineRate);
                //    room.LineRate = new WrapPair<int, string>(val.Key, val.Value);
                //}
                room.RoomID = user.RoomID;
                if (user.Protocol != 0)
                {
                    var val = MyVrmService.Service.VideoProtocols.FirstOrDefault(a => a.Key == user.Protocol);
                    room.ProtocolType = new WrapPair<int, string>(val.Key, val.Value);
                }
                if (user.ConnectionType != 0)
                {
                    ConnectionTypes conType = (ConnectionTypes)user.ConnectionType;
                    string name = Common.LocalizedDescriptionAttribute.FromEnum(conType.GetType(), conType);
                    room.ConnectionType = new WrapPair<int, string>((int)conType, name);
                }
                //if (user.BridgeId != null)
                //{
                //    var val = MyVrmService.Service.Bridges.FirstOrDefault(a => a.Id == user.BridgeId);
                //    if (val != null)
                //        room.MCU = new WrapPair<BridgeId, string>(val.Id, val.Name);
                //}
                _rooms.Add(room);
                //ZD 101343 Ends
            }

            // Conference.AudioConferenceBridges.Clear();

            //   string callerName = Common.LocalizedDescriptionAttribute.FromEnum(endpoint.Caller.GetType(), user.Caller);
            //  room.CallerCallee = new WrapPair<int, string>((int)foundEndpoint.Caller, callerName);

            //  room.APIPort = user.ApiPortNumber.ToString();

            // ZD 101343 starts

            DataList<Participant> users = Conference.ExternalParticipants;

            foreach (var user in users)
            {
                AudioConferenceBridge found = endpoints.FirstOrDefault(a => a.Endpoint.Id == user.UserId);
                var foundEndpoint = found != null ? found.Endpoint : null;
                if (foundEndpoint != null)
                {
                    ExternalRoom room = new ExternalRoom();

                    room.UserFirstName = user.FirstName;
                    room.UserLastName = user.LastName;
                    room.UserEmail = user.Email;
                    room.UserId = user.UserId;

                    room.AddressPhone = foundEndpoint.Address;
                    if (foundEndpoint.AddressType > 0)
                    {
                        var val = MyVrmService.Service.AddressTypes.FirstOrDefault(a => a.Key == (int)foundEndpoint.AddressType);
                        room.AddressType = new WrapPair<int, string>(val.Key, val.Value);
                    }
                    if (foundEndpoint.ConnectionType > 0 && foundEndpoint.ConnectionType != null)
                    {
                        ConnectionTypes conType = (ConnectionTypes)foundEndpoint.ConnectionType;
                        string name = Common.LocalizedDescriptionAttribute.FromEnum(conType.GetType(), conType);
                        room.ConnectionType = new WrapPair<int, string>((int)conType, name);
                    }

                    if (foundEndpoint.DefaultProtocol != null)
                    {
                        var val = MyVrmService.Service.VideoProtocols.FirstOrDefault(a => a.Key == foundEndpoint.DefaultProtocol);
                        room.ProtocolType = new WrapPair<int, string>(val.Key, val.Value);
                    }
                    if (foundEndpoint.Bandwidth != null)
                    {
                        var val = MyVrmService.Service.LineRates.FirstOrDefault(a => a.Key == (int)foundEndpoint.Bandwidth);
                        room.LineRate = new WrapPair<int, string>(val.Key, val.Value);
                    }

                    if (foundEndpoint.VideoEquipment != null)
                    {
                        var val = MyVrmService.Service.VideoEquipment.FirstOrDefault(a => a.Id == (int)foundEndpoint.VideoEquipment);
                        if (val != null)
                            room.Equipment = new WrapPair<int, string>(val.Id, val.Name);
                    }

                    if (foundEndpoint.BridgeId != null)
                    {
                        var val = MyVrmService.Service.Bridges.FirstOrDefault(a => a.Id == foundEndpoint.BridgeId);
                        if (val != null)
                            room.MCU = new WrapPair<BridgeId, string>(val.Id, val.Name);
                    }

                    if (foundEndpoint.BridgeProfileID != null && foundEndpoint.BridgeId != null)
                    {
                        var profiles = MyVrmService.Service.GetMCUProfiles(foundEndpoint.BridgeId.Id);
                        var val = profiles.FirstOrDefault(a => a.Id == foundEndpoint.BridgeProfileID);
                        if (val != null)
                            room.MCUProfile = new WrapPair<MCUProfileId, string>(val.Id, val.Name);
                    }

                    string callerName = Common.LocalizedDescriptionAttribute.FromEnum(foundEndpoint.Caller.GetType(), foundEndpoint.Caller);
                    room.CallerCallee = new WrapPair<int, string>((int)foundEndpoint.Caller, callerName);

                    room.APIPort = foundEndpoint.ApiPortNumber.ToString();
                    room.URL = foundEndpoint.Url;
                    _rooms.Add(room);
                }
            }
            return _rooms;
        }

        public void MakeEndpoints() //on apply before save
        {
            Conference.ExternalParticipants.Clear();
            Conference.ExternalRoomsAVSettings.Clear();
            Conference.ConfGuestRooms.Clear();
            foreach (var externalRoom in Rooms)
            {
                // ZD 101343 starts

                if (externalRoom.IsGuestLoc)
                {
                    try
                    {
                        ConfGuestRoom ConfGuestRoom = new ConfGuestRoom(MyVrmService.Service.UserId);
                        ConfGuestRoom.FirstName = externalRoom.UserFirstName;
                        ConfGuestRoom.LastName = externalRoom.UserLastName;
                        ConfGuestRoom.Email = externalRoom.UserEmail;
                        ConfGuestRoom.Address = externalRoom.AddressPhone;
                        if (externalRoom.RoomID != null)
                            ConfGuestRoom.RoomID = externalRoom.RoomID;

                        if (externalRoom.AddressType != null)
                        {
                            ConfGuestRoom.AddressType = (AddressType)externalRoom.AddressType.FirstObj;
                        }
                        if (externalRoom.ConnectionType != null)
                        {
                            ConfGuestRoom.ConnectionType = externalRoom.ConnectionType.FirstObj;
                        }

                        if (externalRoom.LineRate != null)
                        {
                            ConfGuestRoom.MaxLineRate = externalRoom.LineRate.FirstObj;
                        }

                        if (externalRoom.MCU != null)
                        {
                            ConfGuestRoom.BridgeId = externalRoom.MCU.FirstObj;
                        }
                        else
                        {
                            ConfGuestRoom.BridgeId = BridgeId.Default;
                        }

                        ConfGuestRoom.EndpointName = externalRoom.EndPtName;
                        if (externalRoom.Equipment != null)
                        {
                            ConfGuestRoom.VideoEquipmentid = externalRoom.Equipment.FirstObj;
                        }
                   

                        Conference.ConfGuestRooms.Add(ConfGuestRoom);
                        // ZD 101343 Ends
                    }
                    catch (Exception e)
                    {

                        MessageBox.Show(e.Message);
                    }

                }
                else
                {
                    Participant user = new Participant(externalRoom.UserId);
                    user.FirstName = externalRoom.UserFirstName;
                    user.LastName = externalRoom.UserLastName;
                    user.Email = externalRoom.UserEmail;
                    user.InvitationMode = ParticipantInvitationMode.External;
                    user.AudioVideoMode = MediaType.AudioVideo;
                    user.Notify = false;
                    Conference.ExternalParticipants.Add(user);
                }
                ConferenceEndpoint endpoint = new ConferenceEndpoint();

                if (externalRoom.IsGuestLoc)
                    endpoint.Type = ConferenceEndpointType.Room;
                else
                    endpoint.Type = ConferenceEndpointType.User;

                endpoint.Address = externalRoom.AddressPhone;
                if (externalRoom.AddressType != null)
                {
                    endpoint.AddressType = (AddressType)externalRoom.AddressType.FirstObj;
                }
                if (externalRoom.ConnectionType != null)
                {
                    endpoint.ConnectionType = externalRoom.ConnectionType.FirstObj;
                }
                if (externalRoom.ProtocolType != null)
                {
                    endpoint.DefaultProtocol = externalRoom.ProtocolType.FirstObj;
                }

                if (externalRoom.LineRate != null)
                {
                    endpoint.Bandwidth = externalRoom.LineRate.FirstObj;
                }

                if (externalRoom.Equipment != null)
                {
                    endpoint.VideoEquipment = externalRoom.Equipment.FirstObj;
                }


                if (externalRoom.MCU != null)
                {
                    endpoint.BridgeId = externalRoom.MCU.FirstObj;
                }
                else
                {
                    endpoint.BridgeId = BridgeId.Default;
                }

                if (externalRoom.MCUProfile != null)
                {
                    endpoint.BridgeProfileID = externalRoom.MCUProfile.FirstObj;
                }
                else
                {
                    endpoint.BridgeProfileID = MCUProfile.MCUProfileNoItems.Id;
                }

                endpoint.Caller = externalRoom.CallerCallee != null ? (ConferenceEndpointCallMode)externalRoom.CallerCallee.FirstObj :
                                                                        ConferenceEndpointCallMode.Callee;//ConferenceEndpointCallMode.None;

                endpoint.Connection = MediaType.AudioVideo;
                int iVal = 0;
                int.TryParse(externalRoom.APIPort, out iVal);
                endpoint.ApiPortNumber = iVal > 0 ? iVal : 23;
                endpoint.Url = externalRoom.URL;
                if (!externalRoom.IsGuestLoc)
                {
                    endpoint.Type = ConferenceEndpointType.User;
                    endpoint.EndpointId = null;// EndpointId.Empty;
                }
                else
                {
                    if (externalRoom.EndPointID != null)
                        endpoint.EndpointId = externalRoom.EndPointID;
                    if (externalRoom.ID != null)
                        endpoint.ConfGRoomID = externalRoom.ID;
                    endpoint.EndPointName = externalRoom.EndPtName;
                    endpoint.Type = ConferenceEndpointType.Room;
                }
                endpoint.Id = externalRoom.UserId == null ? UserId.New : externalRoom.UserId;
                endpoint.ExternalUserEmail = externalRoom.UserEmail;
                //endpoint.ProfileId = null;
                // ZD 101343 starts

                if (!externalRoom.IsGuestLoc)
                {
                    AudioConferenceBridge audioConferenceBridge = new AudioConferenceBridge(endpoint);
                    Conference.ExternalRoomsAVSettings.Add(audioConferenceBridge);
                }
                else
                    Conference.RoomEndpoints.Add(endpoint);
                // ZD 101343 ends

            }

        }

        public delegate void delegateExternalUpdateMCUProfile(BridgeId mcuId, MCUProfileId newMCUProfileId, string newMCUProfileName);

        public static delegateExternalUpdateMCUProfile _delegateExternalSetMCUProfile;

        public void ExternalUpdateMCUProfile(BridgeId mcuId, MCUProfileId newMCUProfileId, string newMCUProfileName)
        {
            foreach (var room in Rooms)
            {
                if (room.MCU.FirstObj == mcuId)
                {
                    room.MCUProfile = new WrapPair<MCUProfileId, string>(newMCUProfileId, newMCUProfileName);
                }
            }
        }
    }
}
