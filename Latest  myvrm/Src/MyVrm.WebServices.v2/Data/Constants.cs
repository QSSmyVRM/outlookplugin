﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace MyVrm.WebServices.Data
{
    internal static class Constants
    {
        public const string MyVRMServiceVersion = "2.0";
        public const string MyVRMServiceDefaultPath = "/en/myvrmws.asmx";
        public const string MyVRMServiceDefaultServiceFile = "myvrmws.asmx";
        public const string MyVRMServiceDefaultExtension = ".asmx";
        public const string NewConferenceId = "new";
        public const string NewServiceId = "new";
		public const string UseDefaultId = "-1";
        public const string DateFormat = "M/d/yyyy";
        public const string DateTimeFormat = "MM/dd/yyyy hh:mm tt";
        public const string DateTimeSecFormat = "MM/dd/yyyy h:mm:ss tt";
        public const string TimeFormat = "h:mm tt";
        public const string BeforeNoonAbbr = "AM";
        public const string AfterNoonAbbr = "PM";
        public const DayOfWeek FirstDayOfWeek = DayOfWeek.Sunday;
       // public const string InvalidConferenceNameRegularExpression = "[&<>'\\+%\\\\/\\(\\);\\?|\\^=!`,\\[\\]\\{\\}:#\\$@~\"]";
        public const string InvalidConferenceNameRegularExpression = "[&<>]"; //ZD 103356
        public const string SetConferenceCommandName = "SetConferenceDetails";//103900
        public const string SetAdvancedAvSettingsCommandName = "SetAdvancedAVSettings";
        public const string DeleteConferenceCommandName = "DeleteConference";
        public const string GetOldConferenceCommandName = "GetOldConference";
        public const string GetLocationsCommandName = "GetLocations";
        public const string GetLocations2CommmandName = "GetLocations2";
        public const string ManageConferenceRoomCommandName = "ManageConfRoom";
        public const string GetManagerUserCommandName = "GetManageUser";
        //public const string GetRoomDailyViewCommandName = "GetRoomDailyView";
        public const string GetRoomWeeklyViewCommandName = "GetRoomWeeklyView";
        public const string GetRoomMonthlyViewCommandName = "GetRoomMonthlyView";
        public const string GetHomeCommandName = "GetHome";
        public const string GetBridgesCommandName = "GetBridges";
        public const string GetEndpointDetailsCommandName = "GetEndpointDetails";
        public const string GetOldRoomCommandName = "GetOldRoom";
        public const string GetRoomProfileCommandName = "GetRoomProfile";
        public const string SearchRoomsCommandName = "SearchRooms";
        public const string GetOldUserCommandName = "GetOldUser";
		public const string GetRecurDateListCommandName = "GetRecurDateList";

		public const string SetTemplateCommandName = "SetTemplate";
		public const string GetTemplateCommandName = "GetTemplate";
		public const string GetTemplateListCommandName = "GetTemplateList";
		public const string GetNewTemplateCommandName = "GetNewTemplate";
		public const string GetOldTemplateCommandName = "GetOldTemplate";
		public const string SearchTemplateCommandName = "SearchTemplate";
		public const string DeleteTemplateCommandName = "DeleteTemplate";
		public const string SetPreferedRoomCommandName = "SetPreferedRoom";
		public const string GetPreferedRoomCommandName = "GetPreferedRoom";
		public const string SearchConferenceCommandName = "SearchConference";
		public const string SetApproveConferenceCommandName = "SetApproveConference";
        public const string GetEmailContentCommandName = "GetEmailContent"; //ZD 102193
        public const string FetchSelectedPCDetails = "FetchSelectedPCDetails";
        public const string GetOldBridge = "GetOldBridge";
        public const string LessThan = "õ"; //Alt 0245 //ZD 104771 start
        public const string GreaterThan = "æ"; //Alt 145
        public const string ampersand = "σ"; //Alt 229
        public const string doubleQuotes = @""""; //ZD 104391 //ZD 104771 End 
    }
}
