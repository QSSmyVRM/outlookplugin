﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System.Collections.Generic;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook.WinForms.Conference
{
    internal class RoomEndpoint
    {
        private readonly Room _room;
        private readonly ConferenceEndpoint _conferenceEndpoint;
        private readonly Endpoint _endpoint;
    	private string _bridgeName;

        public RoomEndpoint(Room room, ConferenceEndpoint conferenceEndpoint, Endpoint endpoint)
        {
            _room = room;
            _conferenceEndpoint = conferenceEndpoint;
            _endpoint = endpoint;
            if (_conferenceEndpoint != null && Endpoint != null)
            {
                // Set initial values
                var defaultProfile = Endpoint.Profiles.DefaultProfile;
				if (_conferenceEndpoint.ProfileId == null )
					_conferenceEndpoint.ProfileId = defaultProfile.Id;
				if( _conferenceEndpoint.BridgeId == null )
					_conferenceEndpoint.BridgeId = defaultProfile.BridgeId;
            }
        }

        public string RoomName
        {
            get { return _room.Name; }
        }

        public string Name
        {
            get
            {
                return Endpoint != null ? Endpoint.Name : null;
            }
        }

        public RoomId Id
        {
            get { return _room.Id; }
        }

        public EndpointId EndpointId
        {
            get
            {
                return _conferenceEndpoint != null ? _conferenceEndpoint.EndpointId : null;
            }
        }

        public EndpointProfileId ProfileId
        {
            get
            {
                return _conferenceEndpoint != null ? _conferenceEndpoint.ProfileId : null;
            }
            set
            {
                if (_conferenceEndpoint != null)
                {
                    _conferenceEndpoint.ProfileId = value;
                }
            }
        }

        public BridgeId BridgeId
        {
            get
            {
                return _conferenceEndpoint != null ? _conferenceEndpoint.BridgeId : null;
            }
        }

        public bool UseDefault
        {
            get
            {
                return _conferenceEndpoint != null ? _conferenceEndpoint.UseDefault : false;
            }
            set
            {
                if (_conferenceEndpoint != null)
                {
                    _conferenceEndpoint.UseDefault = value;
                }
            }
        }

		public string BridgeName
    	{
    		set
    		{
				_bridgeName = value;
    		} 
			get
			{
				return _bridgeName;
			}
		}

        public ConferenceEndpointCallMode Caller
        {
            get
            {
                return _conferenceEndpoint != null ? _conferenceEndpoint.Caller : ConferenceEndpointCallMode.Callee;
            }
            set
            {
                if (_conferenceEndpoint != null) _conferenceEndpoint.Caller = value;
            }
        }

        internal ConferenceEndpoint ConferenceEndpoint
        {
            get
            {
                return _conferenceEndpoint;
            }
        }

        internal Endpoint Endpoint
        {
            get { return _endpoint; }
        }
    }
}