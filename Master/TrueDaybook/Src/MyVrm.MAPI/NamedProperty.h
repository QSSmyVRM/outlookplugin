#pragma once

namespace MyVrm
{
	namespace MAPI
	{
		public enum class NamedPropertyKind : int
		{
			Id = MNID_ID,
			String = MNID_STRING
		};
		[FlagsAttribute]
		public enum class CustomFlagPropertyFlags : long
		{
			OneOff = INSP_ONEOFFFLAGS,
			PropertyDefinition = INSP_PROPDEFINITION
		};

		public ref class NamedProperty
		{
		public:
			// Property set guids
			// [MS-OXPROPS]
			static initonly System::Guid PSETID_PublicStrings = System::Guid("{00020329-0000-0000-C000-000000000046}");
			static initonly System::Guid PSETID_Common = System::Guid("{00062008-0000-0000-C000-000000000046}");
			static initonly System::Guid PSETID_Address = System::Guid("{00062004-0000-0000-C000-000000000046}");
			static initonly System::Guid PSETID_InternetHeaders = System::Guid("{00020386-0000-0000-C000-000000000046}");
			static initonly System::Guid PSETID_Appointment = System::Guid("{00062002-0000-0000-C000-000000000046}");
			static initonly System::Guid PSETID_Meeting = System::Guid("{6ED8DA90-450B-101B-98DA-00AA003F1305}");
			static initonly System::Guid PSETID_Log = System::Guid("{0006200A-0000-0000-C000-000000000046}");
			static initonly System::Guid PSETID_Note = System::Guid("{0006200E-0000-0000-C000-000000000046}");
			static initonly System::Guid PSETID_PostRss = System::Guid("{00062041-0000-0000-C000-000000000046}");
			static initonly System::Guid PSETID_Task = System::Guid("{00062003-0000-0000-C000-000000000046}");
			static initonly System::Guid PSETID_UnifiedMessaging = System::Guid("{4442858E-A9E3-4E80-B900-317A210CC15B}");
			static initonly System::Guid PSETID_Mapi = System::Guid("{00020328-0000-0000-C000-000000000046}");
			static initonly System::Guid PSETID_AirSync = System::Guid("{71035549-0739-4DCB-9163-00F0580DBBDF}");
			static initonly System::Guid PSETID_Sharing = System::Guid("{00062040-0000-0000-C000-000000000046}");

			//Properties
		private:
			static const int dispidApptRecur = 0x00008216;
			static const int dispidRecurring = 0x00008223;
			static const int dispidClipStart = 0x00008235;
			static const int dispidClipEnd = 0x00008236;
			static const int dispidGlobalObjectId = 0x00000003;
			static const int dispidFormStorage = 0x850F;
			static const int dispidPageDirStream = 0x8513;
			static const int dispidFormPropStream = 0x851B;
			static const int dispidPropDefStream = 0x8540;
			static const int dispidScriptStream = 0x8541;
			static const int dispidCustomFlag = 0x8542;
		public:
			static initonly NamedProperty^ PidLidAppointmentRecur =
				gcnew NamedProperty(NamedProperty::PSETID_Appointment, dispidApptRecur, array<Byte>::typeid);
            static initonly NamedProperty^ PidLidRecurring =
				gcnew NamedProperty(NamedProperty::PSETID_Appointment, dispidRecurring, System::Boolean::typeid);
            static initonly NamedProperty^ PidLidClipStart =
				gcnew NamedProperty(NamedProperty::PSETID_Appointment, dispidClipStart, System::DateTime::typeid);
            static initonly NamedProperty^ PidLidClipEnd =
				gcnew NamedProperty(NamedProperty::PSETID_Appointment, dispidClipEnd, System::DateTime::typeid);
            static initonly NamedProperty^ PidNameContentClass =
				gcnew NamedProperty(NamedProperty::PSETID_InternetHeaders, "content-class", System::String::typeid);
            static initonly NamedProperty^ PidLidGlobalObjectId =
				gcnew NamedProperty(NamedProperty::PSETID_Meeting, dispidGlobalObjectId, array<Byte>::typeid);
            static initonly NamedProperty^ PidLidFormStorage =
				gcnew NamedProperty(NamedProperty::PSETID_Common, dispidFormStorage, array<Byte>::typeid);
            static initonly NamedProperty^ PidLidPageDirStream =
				gcnew NamedProperty(NamedProperty::PSETID_Common, dispidPageDirStream, array<Byte>::typeid);
            static initonly NamedProperty^ PidLidFormPropStream =
				gcnew NamedProperty(NamedProperty::PSETID_Common, dispidFormPropStream, array<Byte>::typeid);
            static initonly NamedProperty^ PidLidPropDefStream =
				gcnew NamedProperty(NamedProperty::PSETID_Common, dispidPropDefStream, array<Byte>::typeid);
            static initonly NamedProperty^ PidLidScriptStream =
				gcnew NamedProperty(NamedProperty::PSETID_Common, dispidScriptStream, array<Byte>::typeid);
            static initonly NamedProperty^ PidLidCustomFlag =
				gcnew NamedProperty(NamedProperty::PSETID_Common, dispidCustomFlag, System::Int32::typeid);
		public:
			NamedProperty(System::Guid guid, int id, System::Type^ type);
			NamedProperty(System::Guid guid, String^ name, System::Type^ type);

			property NamedPropertyKind Kind { NamedPropertyKind get();};
			property System::Guid Guid { System::Guid get();};
			property int Id { int get();};
			property String^ Name { String^ get();};
			property System::Type^ Type {System::Type^ get();};
		internal:
			int GetBytesToMarshal();
			void MarshalToNative(MAPINAMEID* pPropName, BYTE** ppData);
		private:
			NamedPropertyKind kind;
			System::Guid guid;
			int id;
			String^ name;
			System::Type^ type;
		};
	}
}
