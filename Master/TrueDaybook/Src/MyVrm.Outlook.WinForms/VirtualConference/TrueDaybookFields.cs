﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using CallWebApi.Classes;
using MyVrm.Outlook;

namespace MyVrm.Outlook.WinForms.Conference
{
	public class TrueDaybookFields
	{
		private string _subject;
		private string _body;
		private DateTime _start;
		private DateTime _end;
		ReadOnlyCollection<OutlookRecipient> _recipients;

		public TrueDaybookFields(OutlookAppointment appItem)
		{
			Copy(appItem);
		}

		public void Clear()
		{
			_subject = string.Empty;
			_body = string.Empty;
			_start = new DateTime();
			_end = new DateTime();
			_recipients = null;
		}

		public void Copy(OutlookAppointment appItem)
		{
			Clear();
			if (appItem != null)
			{
				_subject = appItem.Subject;
				_body = appItem.Body;
				_start = appItem.Start;
				_end = appItem.End;
				_recipients = new ReadOnlyCollection<OutlookRecipient>(appItem.Recipients);
			}
		}

		public bool IsDifferent(OutlookAppointment appItem)
		{
			bool bRet = false;

			if (appItem == null)
				bRet = true;

			if (!bRet && _subject != appItem.Subject)
				bRet = true;
			if (!bRet && _body != appItem.Body)
				bRet = true;
			if (!bRet && _start != appItem.Start)
				bRet = true;
			if (!bRet && _end != appItem.End)
				bRet = true;
			if (!bRet )
			{
				if(_recipients.Count != appItem.Recipients.Count )
					bRet = true;
				if (!bRet)
				{
					foreach (var recipient in appItem.Recipients)
					{
						if (_recipients.FirstOrDefault(r => r.Name == recipient.Name && r.SmtpAddress == recipient.SmtpAddress) == null)
						{
							bRet = true;
							break;
						}
					}
				}
			}
			return bRet;
		}
	}
}
