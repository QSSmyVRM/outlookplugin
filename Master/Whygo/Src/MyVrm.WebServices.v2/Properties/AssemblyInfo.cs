﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("MyVrm.WebServices")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("23376290-0ace-489c-8611-0f84ed693ed0")]
[assembly: InternalsVisibleTo("MyVrm.WebServices.Tests, PublicKey=00240000048000009400000006020000002400005253413100040000010001000b4d201325904cda88360544630d26ab3b93db22557d2b80e41e34125fc1af4cc68082fadeb366d6c3ec90324d79d952c852997072811de5b9d04cba30847fbe5b046ca3cf9c235fa2e945c113c30a5b93017dd01e6851b7d6aa26c9ed78fc33dd83559d55a6ca67fb27e48471a5704bdda4b292007004768e4d96afb5c5b3cd")]

