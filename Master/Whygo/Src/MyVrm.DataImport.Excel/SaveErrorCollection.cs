﻿using System.Collections;
using System.Collections.Generic;

namespace MyVrm.DataImport.Excel
{
    public class SaveErrorCollection : IEnumerable<SaveError>
    {
        private readonly List<SaveError> _errors = new List<SaveError>();

        internal void Add(SaveError error)
        {
            _errors.Add(error);
        }

        internal void AddRange(IEnumerable<SaveError> errors)
        {
            _errors.AddRange(errors);
        }

        internal int Count
        {
            get
            {
                return _errors.Count;
            }
        }

        #region Implementation of IEnumerable

        public IEnumerator<SaveError> GetEnumerator()
        {
            return _errors.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}
