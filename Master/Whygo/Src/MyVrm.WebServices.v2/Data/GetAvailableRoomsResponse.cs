/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace MyVrm.WebServices.Data
{
	internal class GetAvailableRoomsResponse : ServiceResponse
    {
		
		 readonly List<ManagedRoom> _rooms = new List<ManagedRoom>();

		internal ReadOnlyCollection<ManagedRoom> Rooms
        {
            get
            {
				return new ReadOnlyCollection<ManagedRoom>(_rooms);
            }
        }
		
        internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
        {
            do
            {
                reader.Read();
                if (reader.LocalName == "level3List")
                {
                    do
                    {
                        reader.Read();
                        if (reader.LocalName == "level3")
                        {
                            var topTier = new TopTier(reader.Service);
                            do
                            {
                                reader.Read();
                                if (reader.LocalName == "level3ID")
                                {
                                    topTier.Id = new TierId(reader.ReadElementValue());
                                }
                                else if (reader.LocalName == "level3Name")
                                {
                                    topTier.Name = reader.ReadElementValue();
                                }
                                else if (reader.LocalName == "level2List")
                                {
                                    do
                                    {
                                        reader.Read();
                                        if (reader.LocalName == "level2")
                                        {
                                            var middleTier = new MiddleTier(reader.Service);
                                            do
                                            {
                                                reader.Read();
                                                if (reader.LocalName == "level2ID")
                                                {
                                                    middleTier.Id = new TierId(reader.ReadElementValue());
                                                }
                                                else if (reader.LocalName == "level2Name")
                                                {
                                                    middleTier.Name = reader.ReadElementValue();
                                                }
                                                else if (reader.LocalName == "level1List")
                                                {
													var rooms = new ManagedRoomCollection();//AvailableRoomCollection();
                                                    rooms.LoadFromXml(reader);
                                                    foreach (var room in rooms)
                                                    {
                                                        room.TopTier = topTier;
                                                        room.MiddleTier = middleTier;
                                                    }
                                                    _rooms.AddRange(rooms);
                                                }
                                                else
                                                {
                                                    reader.SkipCurrentElement();
                                                }

                                            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "level2"));
                                        }
                                        else
                                        {
                                            reader.SkipCurrentElement();
                                        }

                                    } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "level2List"));
                                }
                                else
                                {
                                    reader.SkipCurrentElement();
                                }

                            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "level3"));
                        }
                        else
                        {
                            reader.SkipCurrentElement();
                        }

                    } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "level3List"));
                }
                else
                {
                    reader.SkipCurrentElement();
                }

            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "locationList"));
        }
    }
}