﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System.Collections.Generic;

namespace MyVrm.WebServices.Data
{
    public class ConfGuestRoomCollection : ComplexPropertyCollection<ConfGuestRoom>
    {
        public override object Clone()
        {
            ConfGuestRoomCollection copy = new ConfGuestRoomCollection();
            foreach (var participant in Items)
            {
                ConfGuestRoom copyPart = (ConfGuestRoom)participant.Clone();
                copy.Add(copyPart);
            }
            return copy;
        }

        public void Add(ConfGuestRoom ConfGuestRoom)
        {
            InternalAdd(ConfGuestRoom);
        }

        public void AddRange(IEnumerable<ConfGuestRoom> participants)
        {
            InternalAddRange(participants);
        }

        public void Insert(int index, ConfGuestRoom participant)
        {
            //Items.Insert(index, participant);
            InternalInsert(index, participant);
        }

        public void Remove(ConfGuestRoom participant)
        {
            InternalRemove(participant);
        }

        public void RemoveAt(int index)
        {
            var participant = Items[index];
            Remove(participant);
        }

        public void Clear()
        {
            InternalClear();
        }

        #region Overrides of ComplexPropertyCollection<ConfGuestRoom>

        internal override ConfGuestRoom CreateComplexProperty(string xmlElementName)
        {
            return new ConfGuestRoom();
        }

        internal override string GetCollectionItemXmlElementName(ConfGuestRoom complexProperty)
        {
            return "ConfGuestRoom";
        }

        #endregion
    }
}
