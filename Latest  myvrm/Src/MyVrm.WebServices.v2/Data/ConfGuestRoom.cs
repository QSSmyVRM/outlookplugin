﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System.Text;

namespace MyVrm.WebServices.Data
{
    public enum ConfGuestRoomCollectionInvitationMode
    {
        CcOnly = 0,
        External = 1,
        Room = 2
    }

    public class ConfGuestRoom : ComplexProperty
    {
        public override object Clone()//CopyTo(ConfGuestRoomCollection ConfGuestRoomCollection)
        {
            //if (ConfGuestRoomCollection == null)
            ConfGuestRoom ConfGuestRoom = new ConfGuestRoom();
            ConfGuestRoom.GuestRoomName = GuestRoomName;
            ConfGuestRoom.FirstName = FirstName;
            ConfGuestRoom.LastName = LastName;
            // ConfGuestRoom.InvitationMode = InvitationMode;
            ConfGuestRoom.Address = Address;
            ConfGuestRoom.AddressType = AddressType;
            ConfGuestRoom.AudioVideoMode = AudioVideoMode;
            ConfGuestRoom.BridgeId = BridgeId;
            ConfGuestRoom.ConnectionType = ConnectionType;
            ConfGuestRoom.DftConnecttype = DftConnecttype;
            ConfGuestRoom.Email = Email;
            ConfGuestRoom.MaxLineRate = MaxLineRate;
            ConfGuestRoom.Protocol =Protocol ;//ZD 104322

            if (UserId != null)
                ConfGuestRoom.UserId = new UserId(UserId.Id);
            return ConfGuestRoom;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj))
                return true;

            ConfGuestRoom ConfGuestRoomCollection = obj as ConfGuestRoom;
            if (ConfGuestRoomCollection == null)
                return false;
            if (ConfGuestRoomCollection.AudioVideoMode != AudioVideoMode)
                return false;
            if (ConfGuestRoomCollection.ID != ID)
                return false;
            if (ConfGuestRoomCollection.Email != Email)
                return false;
            if (ConfGuestRoomCollection.FirstName != FirstName)
                return false;
            if (ConfGuestRoomCollection.InvitationMode != InvitationMode)
                return false;
            if (ConfGuestRoomCollection.LastName != LastName)
                return false;
            if (ConfGuestRoomCollection.Notify != Notify)
                return false;


            return true;
        }

        internal ConfGuestRoom()
        {
            InvitationMode = ConfGuestRoomCollectionInvitationMode.Room;
            AudioVideoMode = MediaType.AudioVideo;
        }

        public ConfGuestRoom(UserId id)
        {
            UserId = id;
        }


        public ConfGuestRoom(string firstName, string lastName, string email)
        {
            UserId = UserId.New;
            FirstName = firstName;
            LastName = lastName;
            Email = email;
        }

        public ConfGuestRoom(string email)
        {
            UserId = UserId.New;
            Email = email;
        }

        public UserId UserId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public bool Notify { get; set; }
        public int? DefaultProtocol { get; set; }

        public MediaType AudioVideoMode { get; set; }
        public string ID { get; set; }
        public ConfGuestRoomCollectionInvitationMode InvitationMode { get; set; }
        private EndpointAddress _address = new EndpointAddress();

        public string LoginUserID { get; set; }
        public string GuestRoomName { get; set; }
        public int DftConnecttype { get; set; }
        public AddressType? AddressType { get; set; }
        public BridgeId BridgeId { get; set; }
        public string EndpointName { get; set; }
        public string RoomID { get; set; }
        public EndpointId EndPointID { get; set; }
        public EndpointProfileId ProfileID { get; set; }
        public string Address
        {
            get { return _address.Address; }
            set { _address.Address = value; }
        }
        public int VideoEquipmentid { get; set; }
        public int MaxLineRate { get; set; }
        public int? ConnectionType { get; set; }
        public BridgeId BridgeID { get; set; }
        public string GuestRoomID { get; set; }
        public int? Protocol { get; set; }
        public ConferenceEndpoint ConfEnpoint { get; set; }
        public override string ToString()
        {
            var sb = new StringBuilder();
            if (!string.IsNullOrEmpty(FirstName) || !string.IsNullOrEmpty(LastName))
            {
                sb.Append(FirstName + " " + LastName);
            }
            if (!string.IsNullOrEmpty(Email))
            {
                if (sb.Length > 0)
                    sb.Append(" (" + Email + ")");
                else
                    sb.Append(Email);
            }
            else
            {
                sb.Append(base.ToString());
            }
            return sb.ToString();
        }
        
        internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
        {
            switch (reader.LocalName)
            {
                case "ContactName":
                    {
                        string s = reader.ReadValue();
                        FirstName = s.Split(' ')[0];
                        LastName = s.Split(' ')[1];
                        return true;
                    }
                case "ContactEmail":
                    {
                        Email = reader.ReadValue();
                        return true;
                    }
                case "RoomID":
                    {
                        var value = reader.ReadElementValue();
                        if (string.IsNullOrEmpty(value))
                        {
                            RoomID = null;
                            return true;
                        }
                        RoomID = value.ToString();
                        return true;
                    }

                case "ID":
                    {
                        ID = reader.ReadValue();
                        return true;
                    }

                case "EndpointID":
                    {
                        var value = reader.ReadElementValue();
                        if (string.IsNullOrEmpty(value))
                        {
                            EndPointID = null;
                            return true;
                        }
                        EndPointID = new EndpointId(value);
                        return true;

                    }
                case "LoginUserID":
                    {
                        UserId = new UserId();
                        UserId.LoadFromXml(reader, "LoginUserID");
                        return true;
                    }
                case "AddressType":
                    {
                            int val = reader.ReadElementValue<int>();
                            AddressType = (AddressType)val;
                        return true;
                    }
                case "Address":
                    {
                        Address = reader.ReadElementValue();
                        return true;
                    }
                case "MaxLineRate":
                    {
                        MaxLineRate = reader.ReadElementValue<int>();
                        return true;
                    }
                case "ConnectionType":
                    {
                        ConnectionType = reader.ReadElementValue<int>();
                        return true;
                    }
                case "Protocol":
                    {
                        Protocol = reader.ReadElementValue<int>();
                        return true;
                    }
                case "VideoEquipmentid":
                    {
                        VideoEquipmentid = reader.ReadElementValue<int>();
                        return true;
                    }
                case "Profile":
                    {
                        LoadFromXml(reader, "Profile");
                        return true;
                    }
                case "Profiles":
                    {
                        LoadFromXml(reader, "Profiles");
                        return true;
                    }
                case "GuestRoomName":
                    {
                        EndpointName = reader.ReadValue();
                        return true;
                    }
                case "EndpointName":
                    {
                        EndpointName = reader.ReadValue();
                        return true;
                    }
                case "ProfileID":
                    {
                        var value = reader.ReadElementValue();
                        if (string.IsNullOrEmpty(value))
                        {
                            ProfileID = null;
                            return true;
                        }
                        ProfileID = new EndpointProfileId(value);
                        return true;
                    }
                case "DftConnecttype":
                    {
                        ConnectionType = reader.ReadElementValue<int>();
                        return true;
                    }
                case "BridgeID":
                    {
                        var value = reader.ReadElementValue();
                        if (string.IsNullOrEmpty(value))
                        {
                            BridgeId = null;
                            return true;
                        }
                        BridgeId = new BridgeId(value);
                        return true;
                    }
            }
            return false;
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            UserId.WriteToXml(writer, "LoginUserID");
            MyVrmService.Service.OrganizationId.WriteToXml(writer, "organizationID");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "GuestRoomUID", "0");
            if (!string.IsNullOrEmpty(RoomID))
                writer.WriteElementValue(XmlNamespace.NotSpecified, "GuestRoomID", RoomID);
            else
                writer.WriteElementValue(XmlNamespace.NotSpecified, "GuestRoomID", "new");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "GuestRoomName", EndpointName);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "ContactName", FirstName + " " + LastName);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "ContactEmail", Email);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "ContactPhoneNo", "");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "GuestContactPhoneNo", "");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "GuestAdmin", Email);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "GuestAdminID", "0");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "IsGuestAdmin", "0");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "DftConnecttype", ConnectionType);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "RoomAddress", "");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "State", "");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "City", "");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "ZipCode", "");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "Country", "225");//For US by default
            //writer.WriteElementValue(XmlNamespace.NotSpecified, "Tier1", "1");//Commentd for ZD 101491    
            //writer.WriteElementValue(XmlNamespace.NotSpecified, "Tier2", "1");//Commented for ZD 101491
            writer.WriteStartElement(XmlNamespace.NotSpecified, "Profiles");
            writer.WriteStartElement(XmlNamespace.NotSpecified, "Profile");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "VideoEquipment", VideoEquipmentid);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "EndpointName", EndpointName);

            writer.WriteElementValue(XmlNamespace.NotSpecified, "ProfileName", EndpointName + "_1");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "AddressType", AddressType);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "Address", Address);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "MaxLineRate", MaxLineRate);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "isDefault", "1");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "ConnectionType", ConnectionType);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "Protocol", Protocol);

            if (BridgeId != null)
            {
                BridgeId.WriteToXml(writer, "BridgeID");
            }
            else
            {
                writer.WriteElementValue(XmlNamespace.NotSpecified, "BridgeID", null);
            }
            writer.WriteElementValue(XmlNamespace.NotSpecified, "Deleted", "0");
            writer.WriteEndElement();
            writer.WriteEndElement();
            if (ConfEnpoint == null)
            {
                ConfEnpoint = new ConferenceEndpoint();
                ConfEnpoint.WriteElementsToXml(writer);
            }
            else
                ConfEnpoint.WriteElementsToXml(writer);
            //writer.WriteEndElement(); // Added Tag for save
            MyVrmService.TraceSource.TraceInformation(writer.ToString());
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
