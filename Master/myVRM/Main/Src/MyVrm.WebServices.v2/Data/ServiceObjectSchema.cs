﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace MyVrm.WebServices.Data
{
    public abstract class ServiceObjectSchema : IEnumerable<PropertyDefinition>
    {
        private readonly Dictionary<string, PropertyDefinition> _properties;
        private readonly List<PropertyDefinition> _visibleProperties;
        private static readonly LazyMember<List<Type>> AllSchemaTypes;
        private static readonly object LockObject;

        static ServiceObjectSchema()
        {
            LockObject = new object();
            AllSchemaTypes =
                new LazyMember<List<Type>>(
                    () => new List<Type> {typeof (LoggedOnUserSchema)});
        }

        internal ServiceObjectSchema()
        {
            _properties = new Dictionary<string, PropertyDefinition>();
            _visibleProperties = new List<PropertyDefinition>();
            RegisterProperties();
        }

        #region Implementation of IEnumerable

        public IEnumerator<PropertyDefinition> GetEnumerator()
        {
            return _visibleProperties.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        internal virtual void RegisterProperties()
        {
        }

        internal void RegisterProperty(PropertyDefinition property)
        {
            RegisterProperty(property, false);
        }

        private void RegisterProperty(PropertyDefinition property, bool isInternal)
        {
            _properties.Add(property.XmlElementName, property);
            if (!isInternal)
            {
                _visibleProperties.Add(property);
            }
        }
        internal delegate void PropertyFieldInfoDelegate(PropertyDefinition propertyDefinition, FieldInfo fieldInfo);

        internal static void InitializeSchemaPropertyNames()
        {
            lock (LockObject)
            {
                foreach (Type type in AllSchemaTypes.Member)
                {
                    ForeachPublicStaticPropertyFieldInType(type, (propDef, fieldInfo) => propDef.Name = fieldInfo.Name);
                }
            }

        }

        internal static void ForeachPublicStaticPropertyFieldInType(Type type, PropertyFieldInfoDelegate propFieldDelegate)
        {
            foreach (FieldInfo info in type.GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.DeclaredOnly))
            {
                if ((info.FieldType == typeof(PropertyDefinition)) || info.FieldType.IsSubclassOf(typeof(PropertyDefinition)))
                {
                    var propertyDefinition = (PropertyDefinition)info.GetValue(null);
                    propFieldDelegate(propertyDefinition, info);
                }
            }
        }

        internal bool TryGetPropertyDefinition(string xmlElementName, out PropertyDefinition propertyDefinition)
        {
            return _properties.TryGetValue(xmlElementName, out propertyDefinition);
        }
    }
}
