﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    public class CateringProviderMenu : ComplexProperty
    {
		public override object Clone()
		{
			CateringProviderMenu copy = new CateringProviderMenu();
			copy.Id = Id;
			copy.Items = (CateringProviderMenuItemCollection)Items.Clone();
			copy.Name = string.Copy(Name);
			copy.Price = Price;

			return copy;
		}

        internal CateringProviderMenu()
        {
            Items = new CateringProviderMenuItemCollection();
        }

        public int Id { get; internal set; }
        public string Name { get; internal set; }
        public decimal Price { get; internal set; }
        public CateringProviderMenuItemCollection Items { get; private set; }

        public override string ToString()
        {
            return !string.IsNullOrEmpty(Name) ? Name : base.ToString();
        }

        internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
        {
            switch(reader.LocalName)
            {
                case "ID":
                {
                    Id = reader.ReadElementValue<int>();
                    return true;
                }
                case "Name":
                {
                    Name = reader.ReadElementValue();
                    return true;
                }
                case "Price":
                {
                    Price = reader.ReadElementValue<decimal>();
                    return true;
                }
                case "ItemsList":
                {
                    Items.LoadFromXml(reader, "ItemsList");
                    return true;
                }
            }
            return false;
        }
    }
}
