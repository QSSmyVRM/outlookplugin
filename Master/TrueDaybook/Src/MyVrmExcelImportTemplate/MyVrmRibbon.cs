﻿using System;
using System.Net;
using System.Windows.Forms;
using System.Linq;
using Microsoft.Office.Tools.Ribbon;
using MyVrm.WebServices.Data;

namespace MyVrmExcelImportTemplate
{
    public partial class MyVrmRibbon : OfficeRibbon
    {
        public MyVrmRibbon()
        {
            InitializeComponent();
        }

        private void MyVrmRibbon_Load(object sender, RibbonUIEventArgs e)
        {
            UpdateOrganizationLabel();
            webServiceUrlLabel.Label = Globals.ThisWorkbook.SavedServiceUrl;
        }

        private void UpdateOrganizationLabel()
        {
            organizationNameLabel.Label =
                Globals.ThisWorkbook.Organizations.First(
                    match => match.Id.ToString() == Globals.ThisWorkbook.SavedOrganizationId).Name;
        }

        private void publishButton_Click(object sender, RibbonControlEventArgs e)
        {
            var table = Globals.ThisWorkbook.SelectedTable;
            if (table != null)
            {
                table.Publish();
            }
        }

        private void changeOrganizationButton_Click(object sender, RibbonControlEventArgs e)
        {
            using (var dialog = new OrganizationSelectionDialog())
            {
                if (dialog.ShowDialog(null) == DialogResult.OK)
                {
                    var workbook = Globals.ThisWorkbook;
                    workbook.Service.SwitchOrganization(dialog.SelectedOrganization);
                    workbook.SavedOrganizationId = workbook.Service.OrganizationId.Id;
                    UpdateOrganizationLabel();
                }
            }
        }

        private void changeConnectionSettingsButton_Click(object sender, RibbonControlEventArgs e)
        {
            using(ConnectionDialog dialog = new ConnectionDialog())
            {
                dialog.Url = Globals.ThisWorkbook.Service.Url;
                dialog.AuthenticationMode = Globals.ThisWorkbook.Service.AuthenticationMode;
                dialog.UseDefaultCredentials = Globals.ThisWorkbook.Service.UseDefaultCredential;
                if (Globals.ThisWorkbook.Service.Credential != null)
                {
                    dialog.User = Globals.ThisWorkbook.Service.Credential.UserName;
                    dialog.Domain = Globals.ThisWorkbook.Service.Credential.Domain;
                    dialog.Password = Globals.ThisWorkbook.Service.Credential.Password;
                }
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    Globals.ThisWorkbook.Service.Url = dialog.Url;
                    Globals.ThisWorkbook.Service.AuthenticationMode = dialog.AuthenticationMode;
                    Globals.ThisWorkbook.Service.UseDefaultCredential = dialog.UseDefaultCredentials;
                    if (dialog.AuthenticationMode == AuthenticationMode.Windows && !dialog.UseDefaultCredentials)
                    {
                        Globals.ThisWorkbook.Service.Credential = new NetworkCredential(dialog.User, dialog.Password,
                                                                                        dialog.Domain);
                    }
                    else
                    {
                        Globals.ThisWorkbook.Service.Credential = new NetworkCredential(dialog.User, dialog.Password);
                    }
                }
            }
        }

        private void aboutButton_Click(object sender, RibbonControlEventArgs e)
        {
            using(AboutDialog dialog = new AboutDialog())
            {
                dialog.ShowDialog();
            }
        }
    }
}
