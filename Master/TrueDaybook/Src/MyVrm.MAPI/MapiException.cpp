#include "StdAfx.h"
#include "MapiException.h"

namespace MyVrm
{
	namespace MAPI
	{
		MapiException::MapiException(SerializationInfo ^info, StreamingContext context) : Exception(info, context)
		{
		}

		MapiException::MapiException(HRESULT hr, String ^message) : Exception(message)
		{
			HResult = hr;
		}

		MapiException::MapiException(String^ localizedString, String^ message, HRESULT hr) : Exception(resourceManager->GetString(localizedString) + " : " + message + " (hr=0x" + hr.ToString("x") + ")")
		{
			
		}

		void MapiException::ThrowIfError(HRESULT hResult, String ^message)
		{
			if (FAILED(hResult))
			{
				Exception^ exception = Create(hResult, message);
				if (exception)
				{
					throw exception;
				}
			}
		}

		Exception^ MapiException::Create(HRESULT hResult, String^ message)
		{
			if (hResult == SUCCESS_SUCCESS)
			{
				return nullptr;
			}
			if (HR_SUCCEEDED(hResult))
			{
				switch (hResult)
				{
					case MAPI_W_NO_SERVICE:
					case MAPI_W_ERRORS_RETURNED:
					case MAPI_W_POSITION_CHANGED:
					case MAPI_W_APPROX_COUNT:
					case MAPI_W_CANCEL_MESSAGE:
					case MAPI_W_PARTIAL_COMPLETION:
					{
						break;
					}
				}
				return nullptr;
			}
			System::Diagnostics::Trace::WriteLine("ERROR: "+message);
			Exception ^exception = nullptr;
			switch(hResult)
			{
				case MAPI_E_CALL_FAILED:
				{
					exception = gcnew MapiCallFailedException(hResult, message);
					break;
				}
				case MAPI_E_NOT_ENOUGH_MEMORY:
				{
					exception = gcnew MapiNotEnoughMemoryException(hResult, message);
					break;
				}
				case MAPI_E_INVALID_PARAMETER:
				{
					exception = gcnew MapiInvalidParameterException(hResult, message);
					break;
				}
				case MAPI_E_INTERFACE_NOT_SUPPORTED:
				{
					exception = gcnew MapiInterfaceNotSupportedException(hResult, message);
					break;
				}
				case MAPI_E_NO_ACCESS:
				{
					exception = gcnew MapiNoAccessException(hResult, message);
					break;
				}
				case MAPI_E_NO_SUPPORT:
				{
					exception = gcnew MapiNoSupportException(hResult, message);
					break;
				}
				case MAPI_E_BAD_CHARWIDTH:
				{
					exception = gcnew MapiBadCharWidthException(hResult, message);
					break;
				}
				case MAPI_E_STRING_TOO_LONG:
				{
					exception = gcnew MapiStringTooLongException(hResult, message);
					break;
				}
				case MAPI_E_UNKNOWN_FLAGS:
				{
					exception = gcnew MapiUnknownFlagsException(hResult, message);
					break;
				}
				case MAPI_E_INVALID_ENTRYID:
				{
					exception = gcnew MapiInvalidEntryIdException(hResult, message);
					break;
				}
				case MAPI_E_INVALID_OBJECT:
				{
					exception = gcnew MapiInvalidObjectException(hResult, message);
					break;
				}
				case MAPI_E_OBJECT_CHANGED:
				{
					exception = gcnew MapiObjectChangedException(hResult, message);
					break;
				}
				case MAPI_E_OBJECT_DELETED:
				{
					exception = gcnew MapiObjectDeletedException(hResult, message);
					break;
				}
				case MAPI_E_BUSY:
				{
					exception = gcnew MapiBusyException(hResult, message);
					break;
				}
				case MAPI_E_NOT_ENOUGH_DISK:
				{
					exception = gcnew MapiNotEnoughDiskException(hResult, message);
					break;
				}
				case MAPI_E_NOT_ENOUGH_RESOURCES:						
				{
					exception = gcnew MapiNotEnoughResourcesException(hResult, message);
					break;
				}
				case MAPI_E_NOT_FOUND:
				{
					exception = gcnew MapiNotFoundException(hResult, message);
					break;
				}
				case MAPI_E_VERSION:				 					
				{
					exception = gcnew MapiVersionException(hResult, message);
					break;
				}
				case MAPI_E_LOGON_FAILED:
				{
					exception = gcnew MapiLogonFailedException(hResult, message);
					break;
				}
				case MAPI_E_SESSION_LIMIT:							
				{
					exception = gcnew MapiSessionLimitException(hResult, message);
					break;
				}
				case MAPI_E_USER_CANCEL:								
				{
					exception = gcnew MapiUserCancelException(hResult, message);
					break;
				}
				case MAPI_E_UNABLE_TO_ABORT:							
				{
					exception = gcnew MapiUnableToAbortException(hResult, message);
					break;
				}
				case MAPI_E_NETWORK_ERROR:							
				{
					exception = gcnew MapiNetworkErrorException(hResult, message);
					break;
				}
				case MAPI_E_DISK_ERROR:								
				{
					exception = gcnew MapiDiskErrorException(hResult, message);
					break;
				}
				case MAPI_E_TOO_COMPLEX:			 					
				{
					exception = gcnew MapiTooComplexException(hResult, message);
					break;
				}
				case MAPI_E_BAD_COLUMN:								
				{
					exception = gcnew MapiBadColumnException(hResult, message);
					break;
				}
				case MAPI_E_EXTENDED_ERROR:							
				{
					exception = gcnew MapiExtendedErrorException(hResult, message);
					break;
				}
				case MAPI_E_COMPUTED:									
				{
					exception = gcnew MapiComputedException(hResult, message);
					break;
				}
				case MAPI_E_CORRUPT_DATA:								
				{
					exception = gcnew MapiCorruptDataException(hResult, message);
					break;
				}
				case MAPI_E_UNCONFIGURED:
				{
					exception = gcnew MapiUnconfiguredException(hResult, message);
					break;
				}
				case MAPI_E_FAILONEPROVIDER:							
				{
					exception = gcnew MapiFailOneProviderException(hResult, message);
					break;
				}
				case MAPI_E_UNKNOWN_CPID:								
				{
					exception = gcnew MapiUnknownCodePageException(hResult, message);
					break;
				}
				case MAPI_E_UNKNOWN_LCID:								
				{
					exception = gcnew MapiUnknownLocaleException(hResult, message);
					break;
				}
				case MAPI_E_PASSWORD_CHANGE_REQUIRED:
				{
					exception = gcnew MapiPasswordChangeRequiredException(hResult, message);
					break;
				}
				case MAPI_E_PASSWORD_EXPIRED:							
				{
					exception = gcnew MapiPasswordExpiredException(hResult, message);
					break;
				}
				case MAPI_E_INVALID_WORKSTATION_ACCOUNT:
				{
					exception = gcnew MapiInvalidWorkstationAccountException(hResult, message);
					break;
				}
				case MAPI_E_INVALID_ACCESS_TIME:			
				{
					exception = gcnew MapiInvalidAccessTimeException(hResult, message);
					break;
				}
				case MAPI_E_ACCOUNT_DISABLED:						
				{
					exception = gcnew MapiAccountDisabledException(hResult, message);
					break;
				}
				case MAPI_E_END_OF_SESSION:						
				{
					exception = gcnew MapiEndOfSessionException(hResult, message);
					break;
				}
				case MAPI_E_UNKNOWN_ENTRYID:							
				{
					exception = gcnew MapiUnknownEntryIdException(hResult, message);
					break;
				}
				case MAPI_E_MISSING_REQUIRED_COLUMN:
				{
					exception = gcnew MapiMissingRequiredColumnException(hResult, message);
					break;
				}
				case MAPI_E_BAD_VALUE:				
				{
					exception = gcnew MapiBadValueException(hResult, message);
					break;
				}
				case MAPI_E_INVALID_TYPE:
				{
					exception = gcnew MapiInvalidTypeException(hResult, message);
					break;
				}
				case MAPI_E_TYPE_NO_SUPPORT:
				{
					exception = gcnew MapiTypeNoSupportException(hResult, message);
					break;
				}
				case MAPI_E_UNEXPECTED_TYPE:							
				{
					exception = gcnew MapiUnexpectedTypeException(hResult, message);
					break;
				}
				case MAPI_E_TOO_BIG:
				{
					exception = gcnew MapiTooBigException(hResult, message);
					break;
				}
				case MAPI_E_DECLINE_COPY:
				{
					exception = gcnew MapiDeclineCopyException(hResult, message);
					break;
				}
				case MAPI_E_UNEXPECTED_ID:
				{
					exception = gcnew MapiUnexpectedIdException(hResult, message);
					break;
				}
				case MAPI_E_UNABLE_TO_COMPLETE:
				{
					exception = gcnew MapiUnableToCompleteException(hResult, message);
					break;
				}
				case MAPI_E_TIMEOUT:	 					
				{
					exception = gcnew MapiTimeOutException(hResult, message);
					break;
				}
				case MAPI_E_TABLE_EMPTY:
				{
					exception = gcnew MapiTableEmptyException(hResult, message);
					break;
				}
				case MAPI_E_TABLE_TOO_BIG:
				{
					exception = gcnew MapiTableTooBigException(hResult, message);
					break;
				}
				case MAPI_E_INVALID_BOOKMARK:
				{
					exception = gcnew MapiInvalidBookmarkException(hResult, message);
					break;
				}
				case MAPI_E_WAIT:		  					
				{
					exception = gcnew MapiWaitException(hResult, message);
					break;
				}
				case MAPI_E_CANCEL:
				{
					exception = gcnew MapiCancelException(hResult, message);
					break;
				}
				case MAPI_E_NOT_ME:				  					
				{
					exception = gcnew MapiNotMeException(hResult, message);
					break;
				}
				case MAPI_E_CORRUPT_STORE:
				{
					exception = gcnew MapiCorruptStoreException(hResult, message);
					break;
				}
				case MAPI_E_NOT_IN_QUEUE:			  					
				{
					exception = gcnew MapiNotInQueueException(hResult, message);
					break;
				}
				case MAPI_E_NO_SUPPRESS:		  					
				{
					exception = gcnew MapiNoSuppressException(hResult, message);
					break;
				}
				case MAPI_E_COLLISION:		  					
				{
					exception = gcnew MapiCollisionException(hResult, message);
					break;
				}
				case MAPI_E_NOT_INITIALIZED:							
				{
					exception = gcnew MapiNotInitializedException(hResult, message);
					break;
				}
				case MAPI_E_NON_STANDARD:								
				{
					exception = gcnew MapiNonStandardException(hResult, message);
					break;
				}
				case MAPI_E_NO_RECIPIENTS:							
				{
					exception = gcnew MapiNoRecipientsException(hResult, message);
					break;
				}
				case MAPI_E_SUBMITTED:								
				{
					exception = gcnew MapiSubmittedException(hResult, message);
					break;
				}
				case MAPI_E_HAS_FOLDERS:								
				{
					exception = gcnew MapiHasFoldersException(hResult, message);
					break;
				}
				case MAPI_E_HAS_MESSAGES:								
				{
					exception = gcnew MapiHasMessagesException(hResult, message);
					break;
				}
				case MAPI_E_FOLDER_CYCLE:								
				{
					exception = gcnew MapiFolderCycleException(hResult, message);
					break;
				}
				case MAPI_E_AMBIGUOUS_RECIP:							
				{
					exception = gcnew MapiAmbiguousRecipientException(hResult, message);
					break;
				}
			}
			if (exception == nullptr)
			{
				exception = gcnew MapiCallFailedException(hResult, message);
			}
			return exception;
		}
	}
}
