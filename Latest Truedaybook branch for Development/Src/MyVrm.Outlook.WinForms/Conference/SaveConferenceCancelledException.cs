﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Runtime.Serialization;

namespace MyVrm.Outlook.WinForms.Conference
{
    [Serializable]
    public class SaveConferenceCancelledException : Exception
    {
        public SaveConferenceCancelledException()
        {
        }

        public SaveConferenceCancelledException(string message) : base(message)
        {
        }

        public SaveConferenceCancelledException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public SaveConferenceCancelledException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
