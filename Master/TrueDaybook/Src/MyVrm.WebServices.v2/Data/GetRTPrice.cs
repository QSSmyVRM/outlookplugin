﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	public enum HourType
	{
		Early,
		Open, 
		After,
		Crazy
	}

	public class HourPrice
	{
		public HourType HourType { get; set; }
		public double Price { get; set; }
		public DateTime Start { get; set; }
		public DateTime End { get; set; }
	}
	//Request
	class GetRTPriceRequest : ServiceRequestBase<GetRTPriceResponse>
	{
		public RoomId RoomId;

		internal GetRTPriceRequest(MyVrmService service)
			: base(service)
		{
		}

		#region Overrides of ServiceRequestBase<GetTemplateResponse>

		internal override string GetXmlElementName()
		{
			return "login";
		}

		internal override string GetCommandName()
		{
			return Constants.GetRTPriceCommandName;
		}

		internal override string GetResponseXmlElementName()
		{
			return "GetRTPrice";
		}

		internal override GetRTPriceResponse ParseResponse(MyVrmServiceXmlReader reader)
		{
			var response = new GetRTPriceResponse();
			response.ReadElementsFromXml(reader);
			return response;
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			if (UserId != null)
				UserId.WriteToXml(writer, "userID");
		}

		#endregion
	}

	//Response
	public class GetRTPriceResponse : ServiceResponse
	{
		//Result 
		public List<HourPrice> HourPriceList = new List<HourPrice>();

		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{

		}
	}
}
