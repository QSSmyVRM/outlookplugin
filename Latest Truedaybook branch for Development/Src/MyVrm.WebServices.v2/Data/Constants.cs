﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace MyVrm.WebServices.Data
{
    internal static class Constants
    {
        public const string MyVRMServiceVersion = "2.0";
        public const string MyVRMServiceDefaultPath = "/en/myvrmws.asmx";
        public const string MyVRMServiceDefaultServiceFile = "myvrmws.asmx";
        public const string MyVRMServiceDefaultExtension = ".asmx";
        public const string NewConferenceId = "new";
        public const string NewServiceId = "new";
		public const string NewRoomId = "new";
		public const string UseDefaultId = "-1";
        public const string DateFormat = "M/d/yyyy";
        public const string DateTimeFormat = "MM/dd/yyyy hh:mm tt";
        public const string DateTimeSecFormat = "MM/dd/yyyy h:mm:ss tt";
        public const string TimeFormat = "h:mm tt";
        public const string BeforeNoonAbbr = "AM";
        public const string AfterNoonAbbr = "PM";
        public const DayOfWeek FirstDayOfWeek = DayOfWeek.Sunday;
        public const string InvalidConferenceNameRegularExpression = "[&<>'\\+%\\\\/\\(\\);\\?|\\^=!`,\\[\\]\\{\\}:#\\$@~\"]";
        public const string SetConferenceCommandName = "SetConference";
        public const string SetAdvancedAvSettingsCommandName = "SetAdvancedAVSettings";
        public const string DeleteConferenceCommandName = "DeleteConference";
        public const string GetOldConferenceCommandName = "GetOldConference";
        public const string GetLocationsCommandName = "GetLocations";
        public const string GetLocations2CommmandName = "GetLocations2";
        public const string ManageConferenceRoomCommandName = "ManageConfRoom";
        public const string GetManagerUserCommandName = "GetManageUser";
        //public const string GetRoomDailyViewCommandName = "GetRoomDailyView";
        public const string GetRoomWeeklyViewCommandName = "GetRoomWeeklyView";
        public const string GetRoomMonthlyViewCommandName = "GetRoomMonthlyView";
        public const string GetHomeCommandName = "GetHome";
        public const string GetBridgesCommandName = "GetBridges";
        public const string GetEndpointDetailsCommandName = "GetEndpointDetails";
        public const string GetOldRoomCommandName = "GetOldRoom";
        public const string GetRoomProfileCommandName = "GetRoomProfile";
        public const string SearchRoomsCommandName = "SearchRooms";
        public const string GetOldUserCommandName = "GetOldUser";
		public const string GetRecurDateListCommandName = "GetRecurDateList";

		public const string SetTemplateCommandName = "SetTemplate";
		public const string GetTemplateCommandName = "GetTemplate";
		public const string GetTemplateListCommandName = "GetTemplateList";
		public const string GetNewTemplateCommandName = "GetNewTemplate";
		public const string GetOldTemplateCommandName = "GetOldTemplate";
		public const string SearchTemplateCommandName = "SearchTemplate";
		public const string DeleteTemplateCommandName = "DeleteTemplate";
		public const string SetPreferedRoomCommandName = "SetPreferedRoom";
		public const string GetPreferedRoomCommandName = "GetPreferedRoom";
		public const string SearchConferenceCommandName = "SearchConference";
		public const string SetApproveConferenceCommandName = "SetApproveConference";

		//whyGo
    	public const string GetPublicCommandName = "GetPublic";
		public const string GetLocationDetailsCommandName = "GetLocationDetails";
		public const string GetRTPriceCommandName = "GetRTPrice";
		public const string GetAvailabilityCommandName = "GetAvailability";
		public const string GetPrivatePublicRoomIDCommandName = "GetPrivatePublicRoomID";
		public const string GetPublicRoomsAvailabilityCommandName = "GetPublicRoomsAvailability";
		public const string GetPublicRoomsPricesCommandName = "GetPublicRoomsPrices";
		public const string SetPrivatePublicRoomProfileCommandName = "SetPrivatePublicRoomProfile";
		public const string GetPrivatePublicRoomProfileCommandName = "GetPrivatePublicRoomProfile";

    	public const string GetAllRoomsBasicInfoCommandName = "GetAllRoomsBasicInfo";
    	public const string GetConfAvailableRoomCommandName = "GetConfAvailableRoom";

		public const string TDGetAvailableSeatsCommandName = "TDGetAvailableSeats";
		public const string TDGetMaximumAvailableSeatsCommandName = "TDGetMaximumAvailableSeats";
		public const string TDGetDefaultSettingsCommandName = "TDGetDefaultSettings";
		public const string TDDeleteConferenceCommandName = "TDDeleteConference";
		public const string TDScheduleNewConferenceCommandName = "TDScheduleNewConference";
		public const string TDUpdateConferenceCommandName = "TDUpdateConference";
    	public const string TDGetSlotAvailableSeatsCommandName = "TDGetSlotAvailableSeats";
    	public const string TDGetTemplateListCommandName = "TDGetTemplateList";
		public const string TDGetTemplateCommandName = "TDGetTemplate";
    }
}
