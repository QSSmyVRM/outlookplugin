﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraLayout.Utils;
using DevExpress.XtraScheduler;
using DevExpress.XtraScheduler.Drawing;
using DevExpress.XtraTreeList;
using MyVrm.Common.ComponentModel;
using MyVrm.Common.Threading;
using MyVrm.Outlook.WinForms.Conference;
using MyVrm.Outlook.WinForms.Properties;
using MyVrm.WebServices.Data;
using ThreadState = System.Threading.ThreadState;

namespace MyVrm.Outlook.WinForms.Calendar
{
    public partial class RoomsCalendarDialog : Dialog
    {
        private readonly DataList<ManagedRoom> _rooms = new DataList<ManagedRoom>();

        private readonly AdvancedThreadPool _threadPool = new AdvancedThreadPool();
        private readonly List<WorkItem> _threadCookies = new List<WorkItem>();

        private delegate void UpdateAppointmentsDelegate(RoomId roomId, IEnumerable<ConferenceOccurrence> occurrences);
        private readonly UpdateAppointmentsDelegate _updateAppts;

        readonly DataList<RoomId> _updatingRooms = new DataList<RoomId>();
        private readonly Dictionary<ConferenceType, int> _conferenceLabelIds = new Dictionary<ConferenceType, int>();

        private TimeInterval _lastFetchedInterval = new TimeInterval();
    	
		public DataList<ManagedRoom> Rooms
    	{
    		set
    		{
    			_rooms.Clear(); 
				_rooms.AddRange( value);
    		}
    		get { return _rooms; }
    	}

		public enum RoomDisplayMode
		{
			All,
			Private,
			Public
		}
		public RoomDisplayMode RoomMode { get; set; }

    	public TreeList RoomListControl
		{
			get { return roomsList; }
		}

        public RoomsCalendarDialog()
        {
			MyVrmAddin.TraceSource.TraceInfoEx("RoomsCalendarDialog In");

            InitializeComponent();
            Text = Strings.RoomsCalendarDialogCaption;
            CancelVisible = false;
			ApplyVisible = false;
        	ApplyEnabled = false;
            roomListLayoutItem.Text = Strings.RoomsCalendarDialogRoomListCaption;
			roomListLayoutItem.CustomizationFormText = Strings.RoomsCalendarDialogRoomListCaption;
            columnRoomName.Caption = Strings.RoomsCalendarDialogRoomListNameColumn;
            refreshCalendarButton.Text = Strings.RoomsCalendarDialogRefreshCalendarButtonText;
            updatingLabelControl.Text = Strings.RoomsCalendarDialogUpdatingLabelText;

            updatingLabelControlLayoutItem.Visibility = LayoutVisibility.Never;
            _updateAppts  = new UpdateAppointmentsDelegate(UpdateAppointments);
            roomsList.DataSource = _rooms;
            _updatingRooms.ListChanged += UpdatingRoomsListChanged;
            schedulerStorage.Appointments.CustomFieldMappings.Add(new AppointmentCustomFieldMapping("Locations",
                                                                                                    "Locations",
                                                                                                    FieldValueType.
                                                                                                        Object));
            _conferenceLabelIds.Add(ConferenceType.AudioVideo,
                                    schedulerStorage.Appointments.Labels.Add(Color.FromArgb(189, 182, 255),
                                                                             "Audio/Video Conference"));
            _conferenceLabelIds.Add(ConferenceType.RoomConference,
                                    schedulerStorage.Appointments.Labels.Add(Color.FromArgb(247, 105, 82),
                                                                             "Room Conference"));
            _conferenceLabelIds.Add(ConferenceType.AudioOnly,
                                    schedulerStorage.Appointments.Labels.Add(Color.FromArgb(239, 162, 214),
                                                                             "Audio-Only Conference"));
            _conferenceLabelIds.Add(ConferenceType.PointToPoint,
                                    schedulerStorage.Appointments.Labels.Add(Color.FromArgb(132, 239, 156),
                                                                             "Point-to-Point Conference"));
			_conferenceLabelIds.Add(ConferenceType.VMR,
									schedulerStorage.Appointments.Labels.Add(Color.FromArgb(255, 255, 179),
																			 "Virtual Meeting Conference"));
			MyVrmAddin.TraceSource.TraceInfoEx("RoomsCalendarDialog Out");
        }
        private delegate void UpdateUpdatingLabelVisibilityDelegate(int count);

        private void UpdatingRoomsListChanged(object sender, ListChangedEventArgs e)
        {
			MyVrmAddin.TraceSource.TraceInfoEx("UpdatingRoomsListChanged In");

            var coll = (ICollection) sender;
            UpdateUpdatingLabelVisibilityDelegate updateUpdatingLabelVisibility =
                count => updatingLabelControlLayoutItem.Visibility =
                         count > 0
                             ? LayoutVisibility.Always
                             : LayoutVisibility.Never;
            Invoke(updateUpdatingLabelVisibility, coll.Count);
			MyVrmAddin.TraceSource.TraceInfoEx("UpdatingRoomsListChanged Out");
        }

		private void RoomsCalendarDialog_Load(object sender, EventArgs e)
        {
			MyVrmAddin.TraceSource.TraceInfoEx("RoomsCalendarDialog_Load In");
            var cursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            schedulerControl.GoToToday();
            try
            {
				MyVrmAddin.TraceSource.TraceInfoEx("Before GetActieManagedRooms ->");
            	ReadOnlyCollection<ManagedRoom> allActiveRooms = MyVrmService.Service.GetActiveManagedRooms();
				MyVrmAddin.TraceSource.TraceInfoEx("<- After GetActieManagedRooms ");
				//Collection<ManagedRoom> res = new Collection<ManagedRoom>();
            	IEnumerable<ManagedRoom> res;
				switch (RoomMode)
				{
					case RoomDisplayMode.Private:
						res = allActiveRooms != null ? allActiveRooms.Where(a => a.IsPublic == false) : null;
						break;
					case RoomDisplayMode.Public:
						res = allActiveRooms != null ? allActiveRooms.Where(a => a.IsPublic) : null;
						break;
					default:
						res = allActiveRooms;
						break;
				}
            	_rooms.AddRange(res);
            	// _rooms.AddRange(MyVrmService.Service.GetActiveManagedRooms());
            }
			//catch(Exception exception)
			//{

			//}
            finally
            {
                Cursor.Current = cursor;
				MyVrmAddin.TraceSource.TraceInfoEx("RoomsCalendarDialog_Load Out");
            }
        }
		
    	//private static bool bIsOnCloseProcessing = false;
    	private ProgressDialog _progressDlg = null;
		private BackgroundWorker _backgroundWorker = new BackgroundWorker();
		private void BgrWorker_DoWork(object sender, DoWorkEventArgs e)
		{
			BackgroundWorker objWorker = sender as BackgroundWorker;

			ProgressDialog dlg = (ProgressDialog) e.Argument;
			int iPercentage = 0;
			int iStep = 10;
			
			while (_threadCookies.Count > 0)
			{
				var workItem = _threadCookies[0];
				_threadCookies.RemoveAt(0);
				_threadPool.Cancel(workItem, true);
				objWorker.ReportProgress(iPercentage, dlg);
				iPercentage += iStep;
				if (iPercentage > 100)
					iPercentage = 0;
			}
			while (!_threadPool.WaitOne(100, false))
			{
				Application.DoEvents();
				objWorker.ReportProgress(iPercentage, dlg);
				iPercentage += iStep;
				if (iPercentage > 100)
					iPercentage = 0;
			}
			_threadPool.Dispose();
		}
		private void BgrWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			if (_progressDlg != null)
			{
				_progressDlg.Close();
				_progressDlg.Dispose();
				_progressDlg = null;
			}
		}

		private void BgrWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			var dlg = (ProgressDialog)e.UserState;
			dlg.StepProrgessValue(e.ProgressPercentage);
		}
		public void ProgressDlg_Closing(object sender, CancelEventArgs e)
		{
			e.Cancel = _backgroundWorker.IsBusy;
			if(_backgroundWorker.IsBusy)
			    Cursor.Current = Cursors.WaitCursor;
		}
        protected override void OnClosing(CancelEventArgs e)
        {
            if (!_threadPool.HasWorkingThreads)
                return;

            _progressDlg = new ProgressDialog {Text = WinForms.Strings.ProgressBarMsgTxt};

            _backgroundWorker.WorkerReportsProgress = true;
			_backgroundWorker.WorkerSupportsCancellation = true;

			_backgroundWorker.DoWork += BgrWorker_DoWork;
			_backgroundWorker.ProgressChanged += BgrWorker_ProgressChanged;
			_backgroundWorker.RunWorkerCompleted += BgrWorker_RunWorkerCompleted;

			_backgroundWorker.RunWorkerAsync(_progressDlg);

            _progressDlg.Closing += ProgressDlg_Closing;
            _progressDlg.ShowDialog();
			MyVrmAddin.TraceSource.TraceInfoEx("OnClosing Out");
        }

    	

    	private void toolTipController_BeforeShow(object sender, ToolTipControllerShowEventArgs e)
        {
            // Get the ToolTipController.
            var controller = sender as ToolTipController;

            if (controller == null) return;
            // Check, if it's an appointment or not.
            // If it's not an appointment, then exit.
            var aptViewInfo = controller.ActiveObject as AppointmentViewInfo;
            if (aptViewInfo == null) return;
            var appointment = aptViewInfo.Appointment;

            e.Title = appointment.Subject;
            var locations = appointment.GetValue(schedulerStorage, "Locations") as ConferenceOccurrenceRoomCollection;
            var sb = new StringBuilder();
            if (locations != null)
            {
                foreach (var location in locations)
                {
                    sb.Append("\t");
                    sb.AppendLine(location.Name);
                }
            }
            e.ToolTip = string.Format(Strings.RoomsCalendarDialogAppointmentTooltipText, appointment.Start, appointment.End, sb);
        }

        private void UpdateAppointments(RoomId roomId, IEnumerable<ConferenceOccurrence> occurrences)
        {
			MyVrmAddin.TraceSource.TraceInfoEx("UpdateAppointments In");
            var cursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            schedulerStorage.BeginUpdate();

            try
            {

                foreach (var occurrence in occurrences)
                {
                    var appointment = schedulerStorage.CreateAppointment(AppointmentType.Normal);
                    appointment.Start = occurrence.Date;
                    appointment.End = occurrence.Date + occurrence.Duration;
                    appointment.Subject = occurrence.ConferenceName;
                    appointment.ResourceId = roomId;
                    appointment.StatusId =
                        schedulerStorage.Appointments.Statuses.GetStandardStatusId(AppointmentStatusType.Busy);
                    int labelId;
                    if (_conferenceLabelIds.TryGetValue(occurrence.ConferenceType, out labelId))
                    {
                        appointment.LabelId = labelId;
                    }
                    appointment.SetValue(schedulerStorage, "Locations", occurrence.Rooms);
                    schedulerStorage.Appointments.Add(appointment);
                }
            }
            catch (Exception exception)
            {
                MyVrmAddin.TraceSource.TraceException(exception, true);
            }
            finally
            {
                schedulerStorage.EndUpdate();
                Cursor.Current = cursor;
				MyVrmAddin.TraceSource.TraceInfoEx("UpdateAppointments Out");
            }
        }

		public void ExternalCalendarUpdate()
		{
			refreshCalendarButton_Click(null, null);
		}

    	private void refreshCalendarButton_Click(object sender, EventArgs e)
        {
			MyVrmAddin.TraceSource.TraceInfoEx("refreshCalendarButton_Click In");
            var roomIds = new List<RoomId>();
            roomsList.NodesIterator.DoOperation(node => { if (node.Checked) roomIds.Add(_rooms[node.Id].Id); });

            schedulerStorage.BeginUpdate();
            schedulerStorage.Resources.Clear();
            schedulerStorage.Appointments.Clear();
            try
            {
                foreach (var managedRoom in roomIds.Select(roomId => _rooms.FirstOrDefault(room => room.Id == roomId)))
                {
                    schedulerStorage.Resources.Add(new Resource(managedRoom.Id, managedRoom.Name));

                }
            }
            finally
            {
                schedulerStorage.EndUpdate();
            }
            _lastFetchedInterval = new TimeInterval();
            FetchAppointmentsForVisibleInterval(roomIds);
			MyVrmAddin.TraceSource.TraceInfoEx("refreshCalendarButton_Click Out");
        }

        private void FetchAppointmentsForVisibleInterval(IEnumerable<RoomId> roomIds)
        {
			MyVrmAddin.TraceSource.TraceInfoEx("FetchAppointmentsForVisibleInterval In");

            if (roomIds.Count() == 0)
            {
                _lastFetchedInterval = new TimeInterval();
				MyVrmAddin.TraceSource.TraceInfoEx("FetchAppointmentsForVisibleInterval Out, 0 rooms");
				return;
            }
            var start = schedulerControl.ActiveView.GetVisibleIntervals().Start;
            var end = schedulerControl.ActiveView.GetVisibleIntervals().End;
            
            var interval = new TimeInterval(start, end);

            if (interval.Start >= _lastFetchedInterval.Start && interval.End <= _lastFetchedInterval.End)
            {
				MyVrmAddin.TraceSource.TraceInfoEx("FetchAppointmentsForVisibleInterval Out, in range 1");
				return;
            }
            if (interval.Start.Month == _lastFetchedInterval.Start.Month && interval.Start.Year == _lastFetchedInterval.Start.Year
                && interval.End.Month == _lastFetchedInterval.End.Month && interval.End.Year == _lastFetchedInterval.End.Year)
            {
				MyVrmAddin.TraceSource.TraceInfoEx("FetchAppointmentsForVisibleInterval Out, in range 2");
				return;
            }
            
            _lastFetchedInterval = interval;

            schedulerStorage.Appointments.Clear();

            _updatingRooms.AddRange(roomIds);

            foreach (var roomId in roomIds)
            {

            	_threadPool.QueueUserWorkItem(state =>
            	                              	{
            	                              		var id = (RoomId) state;
            	                              		try
            	                              		{
            	                              			var months = end.Month - start.Month;
            	                              			for (int i = 0; i <= months; i++)
            	                              			{
            	                              				if (Thread.CurrentThread.ThreadState == ThreadState.AbortRequested)
            	                              					break;
            	                              				
            	                              				var occurrences = MyVrmService.Service.GetRoomMonthlyCalendar(id,
            	                              				                                                              start.
            	                              				                                                              	AddMonths(
            	                              				                                                              		i));
            	                              				Invoke(_updateAppts, id, occurrences);
            	                              			}
            	                              			_updatingRooms.Remove(id);
            	                              		}
            	                              		catch (Exception exception)
            	                              		{
            	                              			_updatingRooms.Remove(id);
            	                              			MyVrmAddin.TraceSource.TraceException(exception);
            	                              		}
            	                              	}, roomId);
            }
			MyVrmAddin.TraceSource.TraceInfoEx("FetchAppointmentsForVisibleInterval Out");
		}

        private void schedulerControl_VisibleIntervalChanged(object sender, EventArgs e)
        {
			MyVrmAddin.TraceSource.TraceInfoEx("schedulerControl_VisibleIntervalChanged In");

            var roomIds = new List<RoomId>();
            roomsList.NodesIterator.DoOperation(node => { if (node.Checked) roomIds.Add(_rooms[node.Id].Id); });

            FetchAppointmentsForVisibleInterval(roomIds);
			MyVrmAddin.TraceSource.TraceInfoEx("schedulerControl_VisibleIntervalChanged Out");
		}
    }
}
