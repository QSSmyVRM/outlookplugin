﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
    internal class GetConfigurationSettingsRequest : ServiceRequestBase<GetConfigurationSettingsResponse>
    {
        public GetConfigurationSettingsRequest(MyVrmService service) : base(service)
        {
        }

        
        #region Overrides of ServiceRequestBase<GetConfigurationSettingsResponse>

        internal override string GetXmlElementName()
        {
            return "login";
        }

        internal override string GetCommandName()
        {
            return "GetSuperAdmin";
        }

        internal override string GetResponseXmlElementName()
        {
            return "preference";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            UserId.WriteToXml(writer);
            OrganizationId.WriteToXml(writer);
        }

        #endregion
    }
}
