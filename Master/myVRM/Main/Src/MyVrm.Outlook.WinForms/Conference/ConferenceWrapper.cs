﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using DevExpress.XtraEditors.DXErrorProvider;
using MyVrm.Common.ComponentModel;
using MyVrm.Outlook.WinForms.RecurrentConferences;
using MyVrm.WebServices.Data;
using Exception = System.Exception;
using RecurrencePattern = MyVrm.WebServices.Data.RecurrencePattern;
using System.Text;
using Microsoft.Office.Interop.Outlook;
using System.Runtime.InteropServices;
using MyVrm.Outlook;
using MyVrm.Outlook.WinForms;
using MyVrm.Outlook.WinForms.Conference;
using MyVrm.Outlook.WinForms.Options;
using MyVrm.WebServices.Data;

using System.Configuration;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Resources;


namespace MyVrm.Outlook.WinForms.Conference
{
	public delegate void AfterSaveEventHandler(object sender, EventArgs e);
	public delegate void ExternalUpdateEventHandler(object sender, EventArgs e);

    public class ConferenceWrapper : INotifyPropertyChanged, IDXDataErrorInfo
    {
        private readonly OutlookAppointment _appointment;

        private DataList<RoomId> _rooms;
        private DataList<Participant> _participants;
		private DataList<Participant> _externalParticipants;
        private DataList<ConfGuestRoom> _confGuestRooms;//ZD 101343
		//From York
		private ReadOnlyCollection<AudioUser> _audioUsers;
        private Collection<AudioConferenceBridge> _audioConferenceBridges;
        // Room endpoints
        private readonly DataList<ConferenceEndpoint> _roomEndpoints = new DataList<ConferenceEndpoint>();
        private CustomAttributeId _specialInstructionsAttrId;
       // private CustomAttributeId _EntityCodeAttrId; //ZD 102909
        //public int Entitycodestatus;

         private User _userProfile;//ZD 102193
        private User UserProfile
        {
            get { return _userProfile ?? (_userProfile = MyVrmService.Service.GetUser()); }
        }

        //For VMR  in ICAl  //ZD 103387 start
        public string StripTagsRegex(string source) 
        {
            return Regex.Replace(source, "<.*?>", string.Empty);
        }

        /// <summary>
        /// Compiled regular expression for performance.
        /// </summary>
        static Regex _htmlRegex = new Regex("<.*?>", RegexOptions.Compiled);

        /// <summary>
        /// Remove HTML from string with compiled Regex.
        /// </summary>
        public string StripTagsRegexCompiled(string source)
        {
            return _htmlRegex.Replace(source, string.Empty);
        }

        public string StripTagsCharArray(string source)
        {
            char[] array = new char[source.Length];
            int arrayIndex = 0;
            bool inside = false;

            for (int i = 0; i < source.Length; i++)
            {
                char let = source[i];
                if (let == '<')
                {
                    inside = true;
                    continue;
                }
                if (let == '>')
                {
                    inside = false;
                    continue;
                }
                if (!inside)
                {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }
            return new string(array, 0, arrayIndex);
        }

        //ZD 103387  End

		private Collection<AudioConferenceBridge> _externalRoomsAVSettings;

        public ConferenceWrapper(OutlookAppointment appointment, MyVrmService service)
        {
            if (appointment == null) 
                throw new ArgumentNullException("appointment");
            if (service == null)
                throw new ArgumentNullException("service");
            Conference = BindToConference(appointment, service);
            appointment.Body = appointment.Body + " "; //ZD 100818 start
            //if (Conference.Description == "N/A")
            //{
            //    appointment.Body = " ";
            //}
            //else
            //    appointment.Body = Conference.Description + " "; //103000 End,ZD 103012(Host Ical empty)
            _appointment = appointment;
            _appointment.PropertyChanged += AppointmentPropertyChanged;
            _appointment.Saving += AppointmentSaving;
            _appointment.BeforeDelete += AppointmentBeforeDelete;
            _appointment.Sending += AppointmentSending;

			ExternalParticipants.CopyFrom( Participants.Where(a => a.InvitationMode == ParticipantInvitationMode.External));
			AudioConferenceBridges.Clear();
            RoomEndpoints.Clear();
			ExternalRoomsAVSettings.Clear();
            if (Conference.AdvancedAudioVideoSettings != null && Conference.AdvancedAudioVideoSettings.Endpoints != null)
			{
                foreach (var conferenceEndpoint in Conference.AdvancedAudioVideoSettings.Endpoints)
				{
                    switch (conferenceEndpoint.Type)
                    {
                        case ConferenceEndpointType.User:
							if (conferenceEndpoint.EndpointId != EndpointId.Empty)
								AudioConferenceBridges.Add(new AudioConferenceBridge(conferenceEndpoint));
							else
							{
								ExternalRoomsAVSettings.Add(new AudioConferenceBridge(conferenceEndpoint));
							}
                            Participants.Remove(Participants.FirstOrDefault(party => party.UserId == conferenceEndpoint.Id));
                            break;
                        case ConferenceEndpointType.Room:
                            RoomEndpoints.Add(conferenceEndpoint);
                            break;
                    }
				}
			}
            Initialize();
        }

		public IEnumerable<AudioUser> AudioUsers
		{
			get
			{
				return _audioUsers ?? (_audioUsers = MyVrmService.Service.GetAudioUsers());
			}
		}

    	
    	public Collection<AudioConferenceBridge> AudioConferenceBridges
    	{
    		get { return _audioConferenceBridges ?? (_audioConferenceBridges = new Collection<AudioConferenceBridge>()); }
    	}

		public Collection<AudioConferenceBridge> ExternalRoomsAVSettings
    	{
			get { return _externalRoomsAVSettings ?? (_externalRoomsAVSettings = new Collection<AudioConferenceBridge>()); }
    	}
		
		public static WebServices.Data.Conference BindToConference(OutlookAppointment appointment, MyVrmService service)
        {
			WebServices.Data.Conference conference;
            if (appointment.LinkedWithConference)
            {
                switch(appointment.RecurrenceState)
                {
                    case RecurrenceState.NotRecurring:
                    case RecurrenceState.Master:
                    {
                        conference = WebServices.Data.Conference.Bind(MyVrmService.Service,
                                                                      appointment.ConferenceId);
                        break;
                    }
                    case RecurrenceState.Occurrence:
                    case RecurrenceState.Exception:
                    {
                        if (appointment.ConferenceId.IsRecurringId)
                        {
                            conference = WebServices.Data.Conference.Bind(MyVrmService.Service,
                                                                          appointment.ConferenceId,
																		  appointment.Start.Date, 
																		  appointment.StartInStartTimeZone,
																		  appointment.EndInEndTimeZone);
                        }
                        else
                        {
                            conference = WebServices.Data.Conference.Bind(MyVrmService.Service,
                                                                          appointment.ConferenceId);
                        }
                        break;
                    }
                    default:
                        throw new ArgumentOutOfRangeException("appointment",
                                                              string.Format("Unknown appointment recurrence state {0}",
                                                                            appointment.RecurrenceState));
                }
				conference.StartDate = appointment.StartInStartTimeZone;
				conference.EndDate = appointment.StartInStartTimeZone + appointment.Duration;

				if (conference.AppointmentTime != null)
				{
					conference.AppointmentTime.StartTime = appointment.StartInStartTimeZone.TimeOfDay;
					conference.AppointmentTime.EndTime = conference.AppointmentTime.StartTime + appointment.Duration;
				}
				if (conference.RecurrencePattern != null)
				{
					conference.RecurrencePattern.StartTime = conference.AppointmentTime.StartTime;
					conference.RecurrencePattern.EndTime = conference.AppointmentTime.EndTime;
				}
				conference.ClearChangeLog();
            }
            else
            {
                conference = new WebServices.Data.Conference(service);
            }
            return conference;
        }
        public int gtConfResponse = MyVrmService.Service.GetNewConference().GetGuestUserLiscense;//ZD 101343

        public OutlookAppointment Appointment
        {
            get
            {
                return _appointment;
            }
        }

		public WebServices.Data.Conference Conference { get; private set; }

        public string Name
        {
            get
            {
                return Conference.Name;
            }
            set
            {
                if (value != Conference.Name)
                {
                    Conference.Name = value;
                    NotifyPropertyChanged("Name");
                }
            }
        }

        public string Description
        {
            get
            {
                return Conference.Description;
            }
            set
            {
                Conference.Description = value;
            }
        }

        public ConferenceType Type
        {
            get
            {
                return Conference.Type;
            }
            set
            {
                if (value != Conference.Type)
                {
                    Conference.Type = value;
					if (Type == ConferenceType.RoomConference || 
						Conference.VMRType != VMRConferenceType.None) //for a VMR conference
                    {
                        ClearEndpoints();
                    }
                    NotifyPropertyChanged("Type");
                }
            }
        }

        public bool AllDayEvent
        {
            get
            {
                return Appointment.AllDayEvent;
            }

            set
            {
                if (Appointment.AllDayEvent != value)
                {
                    Appointment.AllDayEvent = value;
                    NotifyPropertyChanged("AllDayEvent");
                }
            }
        }
        

        public DateTime StartDate
        {
            get
            {
                return Conference.StartDate;
            }
            set
            {
                if (value != Conference.StartDate)
                {
                    Conference.StartDate = value; 
                    NotifyPropertyChanged("StartDate");
                    if (Appointment.StartInStartTimeZone.TimeOfDay != TimeSpan.FromHours(00.00) && Appointment.EndInEndTimeZone.TimeOfDay != TimeSpan.FromHours(00.00))
                    {
                       Conference.EndDate = Conference.StartDate + TimeSpan.FromMinutes(MyVrmService.Service.OrganizationOptions.DefaultConfDuration); //ZD 102641
                       NotifyPropertyChanged("EndDate");
                    } //ZD 102641
                }
            }
        }

        public DateTime EndDate
        {
            get
            {
                return Conference.EndDate;
            }
            set
            {
                if (value != Conference.EndDate)
                {
                    Conference.EndDate = value;
                    NotifyPropertyChanged("EndDate");
                }
            }
        }

        public TimeZoneInfo TimeZone
        {
            get
            {
                return Conference.TimeZone;
            }
            set
            {
                if (!Conference.TimeZone.Equals(value))// != value))
                {
                    Conference.TimeZone = value;
                    NotifyPropertyChanged("TimeZone");
                }
            }
        }

        public bool IsRecurring
        {
            get
            {
                return Conference.IsRecurring;
            }
            set
            {
                if (Conference.IsRecurring != value)
                {
                    Conference.IsRecurring = value;
                    NotifyPropertyChanged("IsRecurring");
                }
            }
        }

        
        public RecurrencePattern RecurrencePattern
        {
            get
            {
                return Conference.RecurrencePattern;
            }
            set
            {
                if (Conference.RecurrencePattern != value)
                {
                    Conference.RecurrencePattern = value;
                    NotifyPropertyChanged("RecurrencePattern");
                }
            }
        }

        public DataList<Participant> Participants
        {
            get
            {
                if (_participants == null)
                {
                    _participants = new DataList<Participant>();
                    _participants.CopyFrom(Conference.Participants);
                    _participants.ListChanged += ParticipantsListChanged;
                }
                return _participants;
            }
        }

		public DataList<Participant> ExternalParticipants
		{
			get
			{
				if (_externalParticipants == null)
				{
					_externalParticipants = new DataList<Participant>();
					_externalParticipants.CopyFrom(Conference.Participants.Where(a=>a.InvitationMode == ParticipantInvitationMode.External));

					_externalParticipants.ListChanged += ExternalParticipantsListChanged;
				}
				return _externalParticipants;
			}
		}
        // ZD 101343 starts

        public DataList<ConfGuestRoom> ConfGuestRooms
        {
            get
            {
                if (_confGuestRooms == null)
                {
                    _confGuestRooms = new DataList<ConfGuestRoom>();
                    _confGuestRooms.CopyFrom(Conference.ConfGuestRooms);

                    _confGuestRooms.ListChanged += ConfGuestRoomsChanged;
                }
                return _confGuestRooms;
            }
        }
        // ZD 101343 Ends

        public DataList<RoomId> LocationIds
        {
            get
            {
                if (_rooms == null)
                {
                    _rooms = new DataList<RoomId>();
                    _rooms.CopyFrom(Conference.Rooms);
                    _rooms.ListChanged += RoomsListChanged;
                }
                return _rooms;
            }
        }

        public UserId Host
        {
            get
            {
                return Conference.HostId;
            }
            set
            {
                Conference.HostId = value;
            }
        }

        public bool IsNew
        {
            get
            {
                return Conference.IsNew;
            }
        }

        public ConferenceId Id
        {
            get
            {
                return Conference.Id;
            }
        }

        public TimeSpan Duration
        {
            get
            {
                return Conference.Duration;      
            }
            set
            {
                Conference.Duration = value;
                NotifyPropertyChanged("Duration");
            }
        }

        public string Password
        {
            get
            {
                return Conference.Password;
            }
            set
            {
                if (value == Conference.Password) 
                    return;
                Conference.Password = value;
                NotifyPropertyChanged("Password");
            }
        }


        public bool IsImmediate
        {
            get
            {
                return Conference.IsImmediate;
            }
            set
            {
                Conference.IsImmediate = value;
            }
        }

        public bool IsPublic
        {
            get
            {
                return Conference.IsPublic;
            }
            set
            {
                Conference.IsPublic = value;
            }
        }
        //102651 start
        public int MeetandGreetBuffer
        {
            get
            {
                return Conference.MeetandGreetBuffer;
            }
            set
            {
                Conference.MeetandGreetBuffer = value;
            }
        }
        // 102651 end
		/**/
		public bool IsCloudConferencing
		{
			get
			{
				return Conference.IsCloudConferencing;
			}
			set
			{
				Conference.IsCloudConferencing = value;
			}
		}
		public bool IsPCconference
		{
			get
			{
				return Conference.PCVendorId != PCConferenceVendor.None;//Conference.IsPCconference;
			}
			set
			{
				Conference.IsPCconference = value;
			}
		}
		public VMRConferenceType VMRType
		{
			get
			{
				return Conference.VMRType;
			}
			set
			{
				if (value != Conference.VMRType)
				{
					Conference.VMRType = value;
					NotifyPropertyChanged("VMRType");
				}
			}
		}
		public PCConferenceVendor PCVendorId
		{
			get
			{
				return Conference.PCVendorId;
			}
			set
			{
				Conference.PCVendorId = value;
			}
		}

        public MyVrm.WebServices.Data.NetworkSwitchingOptions IsSecured
		{
			get
			{
				return Conference.IsSecured;
			}
			set
			{
				Conference.IsSecured = value;
			}
		}

		public bool IsSecuredEnabledForUser
		{
			get
			{
				return Conference.IsSecuredEnabledForUser;
			}
			set
			{
				Conference.IsSecuredEnabledForUser = value;
			}
		}
		public ConciergeSupportParams ConciergeSupportParams
		{
			get
			{
				return Conference.ConciergeSupportParams;
			}
			set
			{
				Conference.ConciergeSupportParams = value;
			}
		}

		public ConfMessageCollection ConfMessageCollection
		{
			get
			{
				return Conference.ConfMessageCollection;
			}
			set
			{
				Conference.ConfMessageCollection = value;
			}
		}
		/**/

        public bool HostPresent
        {
            get
            {
                return Appointment != null && Conference.Participants.FirstOrDefault(
                    participant =>
                    string.Equals(Appointment.CurrentUser.SmtpAddress, participant.Email,
                                                               StringComparison.InvariantCultureIgnoreCase)) != null;
            }
            set
            {
                if (Appointment == null) 
                    return;
                var participant = Conference.Participants.FirstOrDefault(
                    item =>
                    string.Equals(Appointment.CurrentUser.SmtpAddress, item.Email,
                                                               StringComparison.InvariantCultureIgnoreCase));
                if (participant != null && !value)
                {
                    Participants.Remove(participant);
                }
                else if (participant == null && value)
                {
                    Participants.Add(new Participant(Appointment.CurrentUser.Name, "", Appointment.CurrentUser.SmtpAddress)
                                     {
                                         InvitationMode = ParticipantInvitationMode.CcOnly,
                                         Notify = false
                                     });
                }
            }
        }
        /// <summary>
        /// Indicates that the conference is available for read-only mode.
        /// </summary>
        public bool IsAllowedToModify
        {
            get { return Conference.IsAllowedToModify; }
        }

        public DataList<ConferenceEndpoint> RoomEndpoints
        {
            get { return _roomEndpoints; }
        }

        public string SpecialInstructions
        {
            get
            {
                var attribute = GetSpecialInstructionsAttribute();
                return attribute.Value;
            }
            set
            {
                var attribute = GetSpecialInstructionsAttribute();
                attribute.Value = value;
            }
        }

        public string Entitycode
        {
            get
            {
                var attribute = GetEntitycodeAttribute();
                return attribute.Value;
            }
            set
            {
                var attribute = GetEntitycodeAttribute();
                //ZD 103265 start
                if (Regex.IsMatch(value, Strings.EntitycodeRegexPattern))
                {
                    throw new Exception(Strings.EntitycodeInvalidCharacters);
                }
                else
                   attribute.Value = value;
                //ZD 103265 End
            }
        }

        public string Entitycodeid
        {
            get
            {
                var attribute = GetEntitycodeAttribute();
                return attribute.OptionId;
            }
            set
            {
                var attribute = GetEntitycodeAttribute();
                attribute.OptionId = value;
            }
        }

        private CustomAttribute GetSpecialInstructionsAttribute()
        {
            if (_specialInstructionsAttrId == null)
            {
                var attributeDefinition =
                    MyVrmService.Service.GetCustomAttributeDefinitions().FirstOrDefault(
                        attrDef =>
                        attrDef.CreateType == CustomAttributeCreateType.System &&
                        string.Equals(attrDef.Title, "Special Instructions"));
                if (attributeDefinition != null)
                {
                    _specialInstructionsAttrId = attributeDefinition.Id;
                }
            }
            var attribute = Conference.CustomAttributes.FirstOrDefault(attr => attr.Id == _specialInstructionsAttrId);
            if (attribute == null)
            {
                attribute = new CustomAttribute {Id = _specialInstructionsAttrId, Type = CustomAttributeType.MultilineText};
                Conference.CustomAttributes.Add(attribute);
            }
            return attribute;
        }

        public int Entitycodestatusswitch
        {
            get
            {
                int Entitycodestatus = GetEntitycodeswitchAttribute();
                return Entitycodestatus;
            }
        }
        private int GetEntitycodeswitchAttribute()
        {
            int Entityenablestatus = 0;
                var attributeDefinition =
                    MyVrmService.Service.GetCustomAttributeDefinitions().FirstOrDefault(
                        attrDef =>
                        attrDef.CreateType == CustomAttributeCreateType.System &&
                        string.Equals(attrDef.Title, "Entity Code"));
                if (attributeDefinition != null)
                {
                    Entityenablestatus = attributeDefinition.Status;
                }
            return Entityenablestatus;
        }

        private CustomAttribute GetEntitycodeAttribute()
        {
            if (_EntityCodeAttrId == null)
            {
                var attributeDefinition =
                    MyVrmService.Service.GetCustomAttributeDefinitions().FirstOrDefault(
                        attrDef =>
                        attrDef.CreateType == CustomAttributeCreateType.System &&
                        string.Equals(attrDef.Title, "Entity Code"));

                if (attributeDefinition != null)
                {
                    _EntityCodeAttrId = attributeDefinition.Id;
                }
            }
            var attribute = Conference.CustomAttributes.FirstOrDefault(attr => attr.Id == _EntityCodeAttrId);
            if (attribute == null)
            {
                attribute = new CustomAttribute { Id = _EntityCodeAttrId, Type = CustomAttributeType.DropDownList };

                Conference.CustomAttributes.Add(attribute);
            }
            return attribute;

        }

        public event ExternalUpdateEventHandler ExternalUpdate;

		public virtual void OnExternalUpdate(EventArgs e)
		{
			ExternalUpdateEventHandler handler = ExternalUpdate;
			if (handler != null) handler(this, e);
		}

        public event CancelEventHandler BeforeSave;

 		private void InvokeBeforeSave(CancelEventArgs e)
        {
            var handler = BeforeSave;
            if (handler != null) handler(this, e);
        }

		public event AfterSaveEventHandler AfterSave;

		private void OnAfterSave(EventArgs e)
		{
			//Clear non-outlook property
			Appointment.SetNonOutlookProperty(false);

			var handler = AfterSave;
			if (handler != null) handler(this, e);
		}

        public void FillFromTemplate(WebServices.Data.Conference template)
        {
            Conference.Name = template.Name;
            //+ Outlook assign 
            Appointment.Subject = template.Name;

            Conference.Password = template.Password;

            //Fake assign indeed
            Conference.Description = template.Description;
            //Real assign
            Appointment.Body = template.Description;

            Conference.Duration = template.Duration;
            //+ Outlook assign 
            Appointment.Duration = template.Duration;

            //conferenceWrapper.template.Type = template.Type;

            //Fake assign indeed
            Conference.Rooms.Clear();
            Conference.Rooms.AddRange(template.Rooms);
            //Real assign
            LocationIds.Clear();
            LocationIds.AddRange(template.Rooms);

            //Assign participants
            Participants.Clear();
            //Outlook assign - for mail notification sending
            var participantsEmialList = template.Participants.Select(participant => participant.Email).ToList();
            Appointment.SetRecipients(participantsEmialList);

            Conference.IsPublic = template.IsPublic;
            OnExternalUpdate(new EventArgs());
        }

		public virtual bool Save()
		{
			bool bCancelSave = false;
            string strVMR = "";

            try
            {
                // ignore any changes
                if (!IsAllowedToModify) return true;
                var e = new CancelEventArgs(false);
                InvokeBeforeSave(e);
                if (!e.Cancel)
                {
                    // Outlook doesn't fire PropertyChanged event for Body property so we always set conference description to appointment's body.
                    //if(Appointment.Body != null)
                      //  Conference.Description = Appointment.Body.Trim(); //ZD 100818 //ZD 102466
                    if (Appointment.Body != null)
                    {
                        string ConferenceDescriptionAPP = Appointment.Body.Trim() + "\\n\\n";
                        //Appointment.Body.Trim() + "\\n\\n";
                        string ConfDefaultBodyAPPText_1 = "*********DO NOT DELETE OR CHANGE ANY OF THE TEXT BELOW THIS LINE*********";
                        string[] stringSeparatorsAPP = new string[] { "*********DO NOT DELETE OR CHANGE ANY OF THE TEXT BELOW THIS LINE*********" };

                        if (ConferenceDescriptionAPP.Contains(ConfDefaultBodyAPPText_1))
                        {
                            string[] part1 = ConferenceDescriptionAPP.Split(stringSeparatorsAPP, StringSplitOptions.None);
                            ConferenceDescriptionAPP = part1[0];
                            Conference.Description = ConferenceDescriptionAPP;
                        }
                        else
                        {
                            Conference.Description = Appointment.Body.Trim();
                        }
                    }

                    //Force to update properties (While moving calendar item Outlook doesn't fire events for property changes 
                    Conference.Name = Appointment.Subject;
                    
                        Conference.StartDate = Appointment.StartInStartTimeZone;
                        Conference.EndDate = Appointment.EndInEndTimeZone;

                    //here fix
                        if (Conference.VMRType == VMRConferenceType.Personal)
                        {
                            if (Conference.AudioVideoParameters == null)
                                Conference.AudioVideoParameters = new AdvancedAudioVideoParameters();
                            GetNewConferenceResponse response =  MyVrmService.Service.GetNewConference();
                            Conference.AudioVideoParameters.InternalBridge = response.internalBridge;
                            Conference.AudioVideoParameters.ExternalBridge = response.externalBridge;
                        }

                    //here fix
					Conference.ModifiedOccurrences.Clear();

                    List<OutlookAppointmentWithMark> outLookAppointmentList;
                    while (true)
                    {
                        // Resolve recurring conflicts
                        if (!ResolveRecurringConflicts(out outLookAppointmentList))
                            return false;
                        SaveEndpoints();
                        //ZD  100818 starts
                        if (chkExternalRooms())
                        {
                            TimeSpan DateRem = Appointment.StartInStartTimeZone - DateTime.Now;
                                int hoursRem = (int)DateRem.TotalHours;
                               // var preference = MyVrmService.Service.OrganizationOptions.EnableGuestLocWarningMsg 
                                if (MyVrmService.Service.OrganizationOptions.EnableGuestLocWarningMsg == 1)
                                {
                                    int i = MyVrmService.Service.OrganizationOptions.GuestLocApprovalTime;
                                    int j = 0;
                                    if (i == 0)
                                    {
                                        j = 24;
                                    }
                                    else if (i == 1)
                                    {
                                        j = 48;
                                    }
                                    else if (i == 2)
                                    {
                                        j = 72;
                                    }
                                    if (j > hoursRem)
                                    {
                                        //System.Windows.Forms.MessageBox.Show(, Strings.myVRMInstalHeader);
                                        System.Windows.Forms.MessageBox.Show(Strings.GuestLocations + j + Strings.GuestLocationshours, Strings.Attention);
                                    }
                                    else
                                    {
                                        System.Windows.Forms.MessageBox.Show(Strings.VideoHelpDesk + j + Strings.VideoHelpDesk1,Strings.Attention);
                                    }
                                }
                              
                        }
                        //ZD  100818 Ends
                        //Save to myVRM
                        try
                        {
                        	bCancelSave = false;
							if(_conferenceCopy == null || !_conferenceCopy.Equals(Conference))
								Conference.Save();
							else
							{
								bCancelSave = true;
							}
                            break;
                        }
                        catch (RoomConflictException exception)
                        {
                            UIHelper.ShowError(exception.Message);
                            if (!Conference.IsRecurring)
                                return false;
                        }
                        catch (OutOfSystemAvailabilityException exception)
                        {
                            UIHelper.ShowError(exception.Message);
							if (!Conference.IsRecurring || Conference.IsRecurring && Conference.Rooms.Count == 0)
                                return false;
                        }
                    }
					if (Conference.IsRecurring && !bCancelSave )
                    {
                        if (outLookAppointmentList != null && outLookAppointmentList.Count > 0)
                        {
                            // Save customized conference instances to Outlook (as exceptions)
                            Appointment.Save();

							List<OutlookAppointmentWithMark> outLookDeletedAppointmentList = new List<OutlookAppointmentWithMark>();
							List<OutlookAppointmentWithMark> outLookChangedAppointmentList = new List<OutlookAppointmentWithMark>();

							foreach (var entry in outLookAppointmentList)
							{
								if (entry.IsDeleted)
									outLookDeletedAppointmentList.Add(entry);
								else
									outLookChangedAppointmentList.Add(entry);
							}
							//Remove all deleted occurences
							foreach (var entry in outLookDeletedAppointmentList)
							{
								try
								{
									using (var outlookAppointment = Appointment.GetOccurrence(entry.OriginalStart))
									{
										if (outlookAppointment != null )
											outlookAppointment.Delete();
									}
								}
								catch (Exception exception)
								{
									UIHelper.ShowError(exception.Message);
								}
							}
							//Assign the rest occurences
							foreach (var entry in outLookChangedAppointmentList)
                            {
								try
								{
									using (OutlookAppointment outlookAppointment = Appointment.GetOccurrence(entry.OriginalStart))
									{
                                        if (outlookAppointment != null)
										{
											outlookAppointment.Start = entry.AppointmentStartTime;
											outlookAppointment.End = entry.AppointmentEndTime;
											outlookAppointment.Save();
										}
									}
								}
								catch (Exception exception)
								{
									UIHelper.ShowError(exception.Message);
								}
                            }
                        }
                    }
                    Conference.UpdateCalendarUniqueId(Appointment.GlobalAppointmentId);
                    Appointment.ConferenceId = Conference.Id;
                    //int ConferenceId  =int(Conference.Id);
                    //FillHostIcalbody(ConferenceId);
                    #region  FillHostIcalbody
                    WebServices.Data.Conference oldConf = MyVrmService.Service.GetConference(Conference.Id);
                                    ConferenceId uniqueId = oldConf.UniqueId;
                                    if (Conference != null)
                                    {
                                       
                                        GetEmailContentResponse responseforicalbody = MyVrmService.Service.GetEmailContentRequest(Conference.Id);
                                        string icalbody = responseforicalbody.body; //102193
                                        StringBuilder sb = new StringBuilder(icalbody);
                                       
                                        string conferencename = " " + Conference.Name;
                                        sb.Replace("{4}", conferencename);
                                        string uniqueID = " " + oldConf.UniqueId;
                                        sb.Replace("{5}", uniqueID);
                                        //sb.Replace("{70}",);
                                        String Usertimeformat = "hh:mm tt";
                                        if (UserProfile.timeFormat == "1")
                                            Usertimeformat = "hh:mm tt";
                                        else
                                            Usertimeformat = "HH:mm";
                                        string StartDateTime = " " + Conference.StartDate.ToString(UserProfile.dateFormat) + " " + Conference.StartDate.ToString(Usertimeformat) + " " + UserProfile.TimeZone;
                                        //DateTime setupdura = Convert.ToDateTime(conference.SetupDur);
                                        //string setDuration = (conference.StartDate - Convert.ToDateTime(conference.SetupDur)).ToString();
                                        TimeSpan setupdur = TimeSpan.FromMinutes(MyVrmService.Service.OrganizationOptions.SetupTime);
                                        string SetupTime = " " + (Conference.StartDate - setupdur).ToString(UserProfile.dateFormat) + " " + (Conference.StartDate - setupdur).ToString(Usertimeformat) + " " + UserProfile.TimeZone;
                                        //double teardown = Convert.ToDouble(Teardown);
                                        string EndDateTime = " " + (Conference.EndDate.AddMinutes(MyVrmService.Service.OrganizationOptions.TearDownTime)).ToString(UserProfile.dateFormat) + " " + (Conference.EndDate.AddMinutes(MyVrmService.Service.OrganizationOptions.TearDownTime)).ToString(Usertimeformat) + " " + UserProfile.TimeZone;
                                        sb.Replace("{7}", StartDateTime);
                                        sb.Replace("{6}", SetupTime);
                                        sb.Replace("{8}", EndDateTime);
                                        string temp = " " + String.Format("{0}", Conference.Duration.TotalMinutes.ToString());
                                        sb.Replace("{9}", temp + " mins");
                                        sb.Replace("{10}", " " + oldConf.HostName);
                                        StringBuilder sbical = new StringBuilder();
                                        string t1 = "", t2 = "", t3 = "";
                                        temp = "";
                                        //WebServices.Data.Conference conference;

                                        if (oldConf.IsRecurring)
                                        {
                                            //GetoldRecurranceResponse Recurresponse = MyVrmService.Service.GetoldRecurranceRequest(conference.Id);


                                            if (oldConf.RecurrencePattern.RecurrenceType == RecurrenceType.Daily)
                                            {

                                                if (Conference.StartDate != null && oldConf.RecurrencePattern.StartTime != null && String.Format(": {0}", Conference.Duration) != null)
                                                {


                                                    //if (Recurresponse.endtype == "1")
                                                    t1 = "Occurs every " + oldConf.RecurrencePattern.Interval + " day(s) effective " + Conference.StartDate.ToString("MM/dd/yy") + " occurs" + oldConf.RecurrencePattern.Occurrences + "  time(s)  from  " + Conference.StartDate.ToString("hh:mm tt") + " for " + Conference.Duration.TotalMinutes.ToString() + " mins";
                                                    //if (Recurresponse.endtype == "2")
                                                    //    //t1 = "Occurs every " + oldConf.RecurrencePattern.Interval + " day(s) effective " + oldConf.RecurrencePattern.StartDate.ToString("MM/dd/yy") + " from  " + oldConf.StartDate.ToString("HH:mm") +" for " + String.Format(": {0}", conference.Duration) + " mins";
                                                    //    t2 = "Occurs every " + oldConf.RecurrencePattern.Interval + " day(s) effective " + conference.StartDate.ToString("MM/dd/yy") + " occurs " + oldConf.RecurrencePattern.Occurrences + " time(s) from " + conference.StartDate.ToString("hh:mm") + " for " + conference.Duration.TotalMinutes.ToString() + " mins";
                                                    //if (Recurresponse.endtype == "3")
                                                    //    t3 = "Occurs every " + oldConf.RecurrencePattern.Interval + " day(s) effective " + conference.StartDate.ToString("MM/dd/yy") + " until " + oldConf.RecurrencePattern.EndDate + " from {4} for " + conference.Duration.TotalMinutes.ToString() + " mins";

                                                }
                                            }

                                            else if (oldConf.RecurrencePattern.RecurrenceType == RecurrenceType.Weekly)
                                            {
                                                if (oldConf.RecurrencePattern.StartDate != null && oldConf.RecurrencePattern.StartTime != null && String.Format(": {0}", Conference.Duration) != null)
                                                {
                                                    // if (Recurresponse.endtype == "1")
                                                    t1 = "Occurs every weekday effective " + Conference.StartDate.ToString("MM/dd/yy") + " from " + Conference.StartDate.ToString("hh:mm tt") + " for " + Conference.Duration.TotalMinutes.ToString() + " mins"; // +" mins"; //ZD 100528
                                                    //if (Recurresponse.endtype == "2")
                                                    // t2 = "Occurs every weekday effective " + conference.StartDate.ToString("MM/dd/yy") + " occurs  " + oldConf.RecurrencePattern.Occurrences + " time(s) from " + conference.StartDate.ToString("hh:mm tt") + " for " + conference.Duration.TotalMinutes.ToString() + " mins"; ; //ZD 100528
                                                    //if (Recurresponse.endtype == "3")
                                                    // t3 = "Occurs every weekday effective  " + conference.StartDate.ToString("MM/dd/yy") + " until " + oldConf.RecurrencePattern.Occurrences + " from " + conference.EndDate.ToString("hh:mm tt") + " for " + conference.Duration.TotalMinutes.ToString() + " mins";
                                                }
                                            }
                                            else if (oldConf.RecurrencePattern.RecurrenceType == RecurrenceType.Monthly)
                                            {
                                                if (oldConf.RecurrencePattern.StartDate != null && oldConf.RecurrencePattern.StartTime != null && String.Format(": {0}", Conference.Duration) != null)
                                                {
                                                    //if (Recurresponse.endtype == "1")
                                                    t1 = "Occurs every weekday effective " + Conference.StartDate.ToString("MM/dd/yy") + " from " + Conference.StartDate.ToString("hh:mm tt") + " for " + Conference.Duration.TotalMinutes.ToString() + " mins";//  +" mins"; //ZD 100528
                                                    //if (Recurresponse.endtype == "2")
                                                    //t2 = "Occurs every weekday effective " + conference.StartDate.ToString("MM/dd/yy") + " occurs  " + oldConf.RecurrencePattern.Occurrences + " time(s) from " + conference.StartDate.ToString("hh:mm tt") + " for " + conference.Duration.TotalMinutes.ToString() + " mins";// +" mins"; //ZD 100528
                                                    //if (Recurresponse.endtype == "3")
                                                    //t3 = "Occurs every weekday effective  " + conference.StartDate.ToString("MM/dd/yy") + " until " + oldConf.RecurrencePattern.Occurrences + " from " + conference.EndDate.ToString("hh:mm tt") + " for " + conference.Duration.TotalMinutes.ToString() + " mins"; //ZD 100528
                                                }
                                            }
                                            else if (oldConf.RecurrencePattern.RecurrenceType == RecurrenceType.Yearly)
                                            {
                                                if (oldConf.RecurrencePattern.StartDate != null && oldConf.RecurrencePattern.StartTime != null && String.Format(": {0}", Conference.Duration) != null)
                                                {
                                                    // if (Recurresponse.endtype == "1")
                                                    t1 = "Occurs every weekday effective " + Conference.StartDate.ToString("MM/dd/yy") + " from " + Conference.StartDate.ToString("hh:mm tt") + " for " + Conference.Duration.TotalMinutes.ToString() + " mins";// +" mins"; //ZD 100528
                                                    //if (Recurresponse.endtype == "2")
                                                    // t2 = "Occurs every weekday effective " + conference.StartDate.ToString("MM/dd/yy") + " occurs  " + oldConf.RecurrencePattern.Occurrences + " time(s) from " + conference.StartDate.ToString("hh:mm tt") + " for " + conference.Duration.TotalMinutes.ToString() + " mins"; //ZD 100528
                                                    //if (Recurresponse.endtype == "3")
                                                    // t3 = "Occurs every weekday effective  " + conference.StartDate.ToString("MM/dd/yy") + " until " + oldConf.RecurrencePattern.Occurrences + " from " + conference.EndDate.ToString("hh:mm tt") + " for " + conference.Duration.TotalMinutes.ToString() + " mins";//ZD 100528
                                                }
                                            }

                                            // oldConf.RecurrencePattern.RecurrenceType


                                            temp = t1 + "\\n" + t2 + "\\n" + t3 + "\\n";

                                        }
                                        else
                                        {
                                            temp += " N/A";
                                        }
                                        sb.Replace("{12}", temp);

                                        if (MyVrmService.Service.StartMode == "0")
                                        {
                                            sb.Replace("{70}", " Yes");
                                        }
                                        else
                                        {
                                            sb.Replace("{70}", " No");
                                        }
                                        sb.Replace("{65}", " " + oldConf.HostName);
                                        temp = "";
                                        //sb.Replace("{61}", "");
                                        if (MyVrmService.Service.OrganizationOptions.EnableConferencePassword)
                                        {
                                            temp = "Password: ";
                                            if (!String.IsNullOrEmpty(oldConf.Password))
                                                temp += oldConf.Password;
                                            else
                                                temp += "N/A";
                                            sb.Replace("{61}", temp + "\\n");
                                            sb.AppendLine();

                                        }
                                        else
                                            sb.Replace("{61}", "");
                                        //conference.VMRType == VMRConferenceType.Room && vmrDetails.Length > 0)
                                        //sb.Replace("{77}", "");
                                        temp = "";
                                        sbical = new StringBuilder();
                                        StringBuilder PCContent = new StringBuilder();
                                        if (MyVrmService.Service.enableDesktopVideo)
                                        {
                                            sbical.AppendLine();
                                            if (Conference.PCVendorId == PCConferenceVendor.BlueJeans)
                                            {
                                                int PCID = 1;
                                                //GetEmailContentResponse responseforicalbody = MyVrmService.Service.GetEmailContentRequest(conference.Id);
                                                FetchSelectedPCDetailsResponse ConfPCDeatils = MyVrmService.Service.FetchSelectedPCDetailsRequest(PCID);
                                                //string PCContentstring = MyVrmService.Service.FetchSelectedPCDetailsRequest(PCID);

                                                PCContent.Append("\n --------------------------------------------------------------------------------------- \n");
                                                PCContent.Append("To join or start the meeting,go to: " + ConfPCDeatils.Description + " \n Or join directly with the following options:\n");
                                                PCContent.Append("\n Skype:" + ConfPCDeatils.SkypeURL + "\n \n Video Conferencing Systems:\n");
                                                PCContent.Append("Dial-in IP: " + ConfPCDeatils.VCDialinIP + "\n Meeting ID: " + ConfPCDeatils.VCMeetingID + "\n \n Phone Conferencing:\n");
                                                PCContent.Append("Dial in toll number: " + ConfPCDeatils.PCDialinNum + "\n Dial in toll free number: " + ConfPCDeatils.PCDialinFreeNum + "\n Meeting ID: " + ConfPCDeatils.PCMeetingID + "\n \n");
                                                PCContent.Append("***********************************************************\n");
                                                PCContent.Append("First time joining a Blue Jeans Video Meeting?\nFor detailed instructions: " + ConfPCDeatils.Intructions + "\n");
                                                PCContent.Append("***********************************************************\n");
                                                PCContent.Append("***********************************************************\n");
                                                PCContent.Append("Test your video connection,talk to Jean our video tester by clicking here\n \n " + ConfPCDeatils.Intructions + "\n");
                                                PCContent.Append("***********************************************************\n");
                                                PCContent.Append("(c) Blue Jeans Network 2012");
                                            }
                                            else if (Conference.PCVendorId == PCConferenceVendor.Jabber)
                                            {
                                                int PCID = 2;

                                                FetchSelectedPCDetailsResponse ConfPCDeatils = MyVrmService.Service.FetchSelectedPCDetailsRequest(PCID);
                                                PCContent.Append("\n --------------------------------------------------------------------------------------- \n");
                                                PCContent.Append("To join or start the meeting,go to:" + ConfPCDeatils.Description + "\n Or join directly with the following options:\n");
                                                PCContent.Append("\n Skype:\n" + ConfPCDeatils.SkypeURL + "\n \n Video Conferencing Systems:\n");
                                                PCContent.Append("Dial-in IP: " + ConfPCDeatils.VCDialinIP + "\n Meeting ID: " + ConfPCDeatils.VCMeetingID + "\n \nPhone Conferencing:\n");
                                                PCContent.Append("Dial in toll number: " + ConfPCDeatils.PCDialinNum + "\n Dial in toll free number: " + ConfPCDeatils.PCDialinFreeNum + "\n Meeting ID: " + ConfPCDeatils.PCMeetingID + "\n");
                                                PCContent.Append("***********************************************************\n");
                                                PCContent.Append("\n First time joining a Jabber Video Meeting?\n For detailed instructions: " + ConfPCDeatils.Intructions + "\n");
                                                PCContent.Append("***********************************************************\n");
                                                PCContent.Append("**********************************************************\n");
                                                PCContent.Append("Test your video connection,talk to our video tester by clicking here \n " + ConfPCDeatils.Intructions + "\n\n");
                                                PCContent.Append("***********************************************************\n");
                                                PCContent.Append("(c) Jabber Network 2012");
                                            }
                                            else if (Conference.PCVendorId == PCConferenceVendor.Linc)
                                            {
                                                int PCID = 3;

                                                FetchSelectedPCDetailsResponse ConfPCDeatils = MyVrmService.Service.FetchSelectedPCDetailsRequest(PCID);

                                                PCContent.Append("\n --------------------------------------------------------------------------------------- \n");
                                                PCContent.Append("To join or start the meeting,go to:\n " + ConfPCDeatils.Description + "\n Or join directly with the following options:\n");
                                                PCContent.Append("\n Skype:\n" + ConfPCDeatils.SkypeURL + "\n \n Video Conferencing Systems:\n");
                                                PCContent.Append("Dial-in IP: " + ConfPCDeatils.VCDialinIP + "\n Meeting ID: " + ConfPCDeatils.VCMeetingID + "\n \n Phone Conferencing:\n");
                                                PCContent.Append("Dial in toll number: " + ConfPCDeatils.PCDialinNum + "\n Dial in toll free number: " + ConfPCDeatils.PCDialinFreeNum + "\n Meeting ID: " + ConfPCDeatils.PCMeetingID + "\n");
                                                PCContent.Append("\n ***********************************************************\n");
                                                PCContent.Append("\nFirst time joining a Lync Video Meeting?\nFor detailed instructions: \n" + ConfPCDeatils.Intructions + "\n");
                                                PCContent.Append("***********************************************************\n");
                                                PCContent.Append("***********************************************************\n");
                                                PCContent.Append("Test your video connection,talk to our video tester by clicking here \n" + ConfPCDeatils.Intructions + "\n \n ");
                                                PCContent.Append("***********************************************************\n");
                                                PCContent.Append("(c) Lync Network 2012");
                                            }
                                            else if (Conference.PCVendorId == PCConferenceVendor.Vidtel)
                                            {
                                                int PCID = 4;

                                                FetchSelectedPCDetailsResponse ConfPCDeatils = MyVrmService.Service.FetchSelectedPCDetailsRequest(PCID);

                                                PCContent.Append("\n --------------------------------------------------------------------------------------- \n");
                                                PCContent.Append("To join or start the meeting,go to:\n" + ConfPCDeatils.Description + "\n Or join directly with the following options:\n");
                                                PCContent.Append("\n \n Skype:\n" + ConfPCDeatils.SkypeURL + "\n Video Conferencing Systems:\n");
                                                PCContent.Append(" Dial-in SIP:" + ConfPCDeatils.VCDialinSIP + "\n Dial-in H.323: " + ConfPCDeatils.VCDialinH323 + "\n PIN: " + ConfPCDeatils.VCPin + "\n \n Phone Conferencing:\n");
                                                PCContent.Append("Dial in toll number: " + ConfPCDeatils.PCDialinNum + "\n Dial in toll free number: " + ConfPCDeatils.PCDialinFreeNum + "\n PIN: " + ConfPCDeatils.PCPin + "\n");
                                                PCContent.Append("\n (c) Vidtel Network 2012");
                                            }
                                            else
                                            {
                                                temp = "Desktop Video Details:N/A ";
                                                PCContent.AppendLine(temp);
                                            }


                                            sb.Replace("{77}", PCContent.ToString());
                                        }
                                        else
                                            sb.Replace("{77}", "");
                                        temp = "";
                                        string temp1 = "";

                                        //sb.Replace("{91}", "");
                                        //sb.Replace("{92}", "");
                                        // sb.AppendLine();
                                        var contactInfo = MyVrmService.Service.OrganizationOptions.Contact;
                                        sb.Replace("{37}", " " + contactInfo.Name);
                                        sb.Replace("{38}", " " + contactInfo.Email);
                                        sb.Replace("{39}", " " + contactInfo.Phone);

                                        string extno = "";
                                        var roomNames = new string[Conference.Rooms.Count];
                                        string vmrDetails = string.Empty;

                                        for (var i = 0; i < Conference.Rooms.Count; i++)
                                        {

                                            var roomId = Conference.Rooms[i];
                                            var roomProfile = MyVrmAddin.Instance.GetRoomFromCache(roomId);

                                            if (roomProfile.IsVMR)
                                            {
                                                vmrDetails += "Internal Bridge:";
                                                vmrDetails += " ";
                                                vmrDetails += roomProfile.Name;
                                                vmrDetails += " > ";
                                                vmrDetails += roomProfile.InternalNumber;
                                                vmrDetails += "\n";
                                                vmrDetails += "External Bridge:";
                                                vmrDetails += " ";
                                                vmrDetails += roomProfile.Name;
                                                vmrDetails += " > ";
                                                vmrDetails += roomProfile.ExternalNumber;

                                            }

                                            if (roomProfile != null)
                                            {
                                                roomNames[i] = roomProfile.Name;

                                            }
                                        }


                                        //sb.AppendLine();
                                        /// temp = Strings.txtLocations + " :";
                                        ///sb.AppendLine(temp);
                                        ///
                                        //sb.Replace("{13}", "");
                                        //sb.Replace("{57}", "");

                                        temp = "";
                                        sbical = new StringBuilder();
                                        if (Conference.Rooms.Count == 0)
                                            sb.Replace("{13}", "N/A");

                                        //sb.Replace("{15}", "");
                                        //sb.Replace("{57}", "");
                                        temp = " ";
                                        foreach (RoomId room in Conference.Rooms)
                                        {
                                            //temp += ": " + room + " (";

                                            temp += MyVrmAddin.Instance.GetRoomFromCache(room).TopTierName;
                                            temp += ">";
                                            temp += MyVrmAddin.Instance.GetRoomFromCache(room).MiddleTierName;
                                            temp += ">";
                                            temp += MyVrmAddin.Instance.GetRoomFromCache(room).Name;
                                            temp += "";
                                            if (MyVrmService.Service.OrganizationOptions.EnableConfTZinLoc)
                                            {
                                                temp += "( ";
                                                temp += Conference.StartDate.ToString(UserProfile.dateFormat);
                                                temp += "";
                                                temp += MyVrmAddin.Instance.GetRoomFromCache(room).TimeZone;
                                                temp += " )";
                                                temp += "\n";
                                            }

                                            string roomaddress = "";
                                            if (!string.IsNullOrEmpty(MyVrmAddin.Instance.GetRoomFromCache(room).Floor))
                                                roomaddress = MyVrmAddin.Instance.GetRoomFromCache(room).Floor + " ";
                                            if (!string.IsNullOrEmpty(MyVrmAddin.Instance.GetRoomFromCache(room).Number))
                                                roomaddress += " # " + MyVrmAddin.Instance.GetRoomFromCache(room).Number + " " + " , ";
                                            if (!string.IsNullOrEmpty(MyVrmAddin.Instance.GetRoomFromCache(room).StreetAddress1))
                                                roomaddress += MyVrmAddin.Instance.GetRoomFromCache(room).StreetAddress1 + " " + " , ";
                                            if (!string.IsNullOrEmpty(MyVrmAddin.Instance.GetRoomFromCache(room).StreetAddress2))
                                                roomaddress += MyVrmAddin.Instance.GetRoomFromCache(room).StreetAddress2 + " " + " , " + "\\n";
                                            if (!string.IsNullOrEmpty(MyVrmAddin.Instance.GetRoomFromCache(room).City))
                                                roomaddress += MyVrmAddin.Instance.GetRoomFromCache(room).City + " " + " , ";
                                            if (!string.IsNullOrEmpty(MyVrmAddin.Instance.GetRoomFromCache(room).State))
                                                roomaddress += MyVrmAddin.Instance.GetRoomFromCache(room).State + " " + " , ";
                                            if (!string.IsNullOrEmpty(MyVrmAddin.Instance.GetRoomFromCache(room).Country))
                                                roomaddress += MyVrmAddin.Instance.GetRoomFromCache(room).Country + " ";
                                            if (!string.IsNullOrEmpty(MyVrmAddin.Instance.GetRoomFromCache(room).ZipCode))
                                                roomaddress += "-" + MyVrmAddin.Instance.GetRoomFromCache(room).ZipCode + " " + "\\n";
                                            if (!string.IsNullOrEmpty(MyVrmAddin.Instance.GetRoomFromCache(room).MapLink))
                                                roomaddress += " Map Link :" + MyVrmAddin.Instance.GetRoomFromCache(room).MapLink + " " + "\\n";
                                            temp += roomaddress;
                                            sbical.AppendLine(temp);
                                            //if(temp == "")
                                            //sb.Replace("{13}", "N/A");
                                            //else 
                                            // sb.Replace("{13}", temp);

                                            EndpointId endptid = MyVrmAddin.Instance.GetRoomFromCache(room).EndpointId;
                                            //sb.Replace("{15}", "");
                                            //sb.Replace("{57}", "");

                                            //sbical.AppendLine();
                                            //if(oldConf.AdvancedAudioVideoSettings.Endpoints

                                            if (MyVrmService.Service.OrganizationOptions.EnableEPDetails)
                                            {
                                                if (oldConf.AdvancedAudioVideoSettings.Endpoints != null)
                                                {
                                                    if (oldConf.AdvancedAudioVideoSettings.Endpoints.Count > 0)
                                                    {
                                                        foreach (ConferenceEndpoint endpt in oldConf.AdvancedAudioVideoSettings.Endpoints)
                                                        {
                                                            if (endpt.EndpointId == endptid)
                                                            {
                                                                if (endpt.Connection == MediaType.AudioVideo)
                                                                {
                                                                    string n = "";
                                                                    if (endpt.ConnectionType > 0)
                                                                    {
                                                                        switch (endpt.ConnectionType)
                                                                        {
                                                                            case 1: n = "Dial in";
                                                                                break;
                                                                            case 2: n = "Dial out";
                                                                                break;
                                                                            case 3: n = "Direct";
                                                                                break;
                                                                            default: n = "N/A";
                                                                                break;
                                                                        }
                                                                    }

                                                                    if (endpt.ConnectionType == 1)
                                                                    {
                                                                        //if (endpt.AddressType.ToString() == "4")
                                                                        //{
                                                                        string VMRDialInNum = "";
                                                                        if (endpt.IsOutside == true)
                                                                            VMRDialInNum = MyVrmAddin.Instance.GetRoomFromCache(room).ExternalNumber;
                                                                        else
                                                                            VMRDialInNum = MyVrmAddin.Instance.GetRoomFromCache(room).InternalNumber;

                                                                        temp = "Endpoint: ";
                                                                        sbical.Append(temp);
                                                                        temp = "(Endpoint Address: " + endpt.Address + "; Connect Type - " + n;

                                                                        if ((MyVrmAddin.Instance.GetRoomFromCache(room).IsVMR) && VMRDialInNum != "")
                                                                        {
                                                                            temp += "; Bridge Dial In Info - " + VMRDialInNum + ") \\n";
                                                                        }
                                                                        else
                                                                        {
                                                                            int bridgeid = 0;
                                                                            int.TryParse(endpt.BridgeId.ToString(), out bridgeid);
                                                                            GetOldBridgeResponse Bridgedetails = MyVrmService.Service.GetOldBridgeRequest(bridgeid);
                                                                            string BridgeAddress = Bridgedetails.E164Dialnumber;
                                                                            extno = Bridgedetails.BridgeExtNo;

                                                                            if (!string.IsNullOrEmpty(BridgeAddress)) //ZD 101145
                                                                                temp += ";Bridge Address - " + BridgeAddress;
                                                                            if (MyVrmService.Service.OrganizationOptions.showBridgeExt) //FB 2610
                                                                                temp += "; Bridge  # - " + extno;
                                                                        }
                                                                        temp += ") \\n";

                                                                        sbical.Append(temp);


                                                                    }
                                                                    else
                                                                    {
                                                                        temp = "Endpoint: ";
                                                                        sbical.Append(temp);
                                                                        temp = "(Endpoint Address: " + endpt.Address + "; Connect Type - " + n + ")";
                                                                        sbical.Append(temp);
                                                                    }
                                                                }

                                                                else if (endpt.Connection == MediaType.AudioOnly)
                                                                {
                                                                    //temp = "Audio Information: ";
                                                                    string n = "";
                                                                    if (endpt.ConnectionType > 0)
                                                                    {
                                                                        switch (endpt.ConnectionType)
                                                                        {
                                                                            case 1: n = "Dial in";
                                                                                break;
                                                                            case 2: n = "Dial out";
                                                                                break;
                                                                            case 3: n = "Direct";
                                                                                break;
                                                                            default: n = "N/A";
                                                                                break;
                                                                        }

                                                                        temp = "(Endpoint Address: " + endpt.Address + "; Connection Type - " + n + ")";
                                                                        sbical.AppendLine(temp);
                                                                        // sb.Replace("{15}", sbical.ToString());
                                                                    }

                                                                }
                                                                break;
                                                            }
                                                        }
                                                    }

                                                }
                                            }

                                            temp = "";
                                            if (MyVrmService.Service.OrganizationOptions.EnableRoomAdminDetails)
                                            {
                                                temp += MyVrmAddin.Instance.GetRoomFromCache(room).AssistantName;
                                                temp += "(Email - ";
                                                temp += MyVrmAddin.Instance.GetRoomFromCache(room).AssistantInchargeEmail;
                                                temp += ")";
                                                temp += "\n";
                                                //temp = "";
                                                sbical.AppendLine(temp);
                                            }
                                            else
                                            {
                                                temp += "";
                                                sbical.AppendLine(temp);
                                            }
                                        }
                                        if (!string.IsNullOrEmpty(sbical.ToString()))
                                            sb.Replace("{13}", sbical.ToString());
                                        else
                                            sb.Replace("{13}", "N/A");
                                        // sb.Replace("{97}", "");
                                        sb.Replace("{57}", "");
                                        sb.Replace("{99}", "");
                                        sb.Replace("{100}", "");
                                        sb.Replace("{101}", "");
                                        sb.Replace("{102}", "");
                                        sb.Replace("{103}", "");

                                        sbical = new StringBuilder();
                                        string dialIn = oldConf.ConfDialIn;

                                        string dialinInformation = "Dial-in Information: ";
                                        if (!string.IsNullOrEmpty(extno))
                                        {
                                            sbical.AppendLine();
                                            dialinInformation += extno;
                                            sbical.AppendLine(dialinInformation);
                                            sb.Replace("{97}", sbical.ToString() + "\\n");
                                        }
                                        else
                                        {
                                            sbical.AppendLine();
                                            dialinInformation += "N/A";
                                            sbical.AppendLine(dialinInformation);
                                            sb.Replace("{97}", sbical.ToString() + "\\n");
                                        }


                                        temp1 = "";
                                        temp1 = "External Participant(s) : ";
                                        int n1 = 0;
                                        string connectype = "";
                                        string ExternalUserEmail = "";
                                        temp = "";
                                        string Audiotemp = "Audio Information: ";
                                        string Audiotemp1 = "";
                                        string participantCode = "";
                                        bool AudioAddon = false;

                                        foreach (ConferenceEndpoint cfdt in Conference.AdvancedAudioVideoSettings.Endpoints)
                                        {

                                            if (cfdt.Type == ConferenceEndpointType.User)
                                            {
                                                n1 = 1;
                                                AudioAddon = false;
                                                if (!string.IsNullOrEmpty(cfdt.ExternalUserEmail))
                                                {
                                                    foreach (Participant party in Conference.ExternalParticipants)
                                                    {
                                                        if (cfdt.ExternalUserEmail == party.Email)
                                                        {
                                                            temp += party.FirstName + " " + party.LastName;
                                                            break;
                                                        }

                                                    }

                                                    if (!string.IsNullOrEmpty(cfdt.ExternalUserEmail))
                                                        ExternalUserEmail = cfdt.ExternalUserEmail;
                                                }
                                                else
                                                {
                                                    _audioUsers = MyVrmService.Service.GetAudioUsers();
                                                    foreach (AudioUser audioUser in _audioUsers)
                                                    {

                                                        if (cfdt.Id == audioUser.Id)
                                                        {
                                                            cfdt.ExternalUserEmail = audioUser.Email;
                                                            temp += audioUser.FirstName + "" + audioUser.LastName;
                                                            participantCode = audioUser.participantCode;
                                                            AudioAddon = audioUser.AudioAddon;
                                                            break;
                                                        }
                                                    }
                                                }
                                                if (!string.IsNullOrEmpty(cfdt.ExternalUserEmail))
                                                {
                                                    temp += " ( Email - ";
                                                    temp += cfdt.ExternalUserEmail;
                                                    temp += ") ";
                                                    temp += "\n";
                                                }
                                                temp += "Connection : ";
                                                switch (cfdt.Connection)
                                                {
                                                    case MediaType.AudioOnly:
                                                        connectype = "Audio Only";
                                                        break;
                                                    case MediaType.AudioVideo:
                                                        connectype = "Audio/Video";
                                                        break;
                                                    case MediaType.NoAudioVideo:
                                                        connectype = "No Audio Video";
                                                        break;
                                                    default: connectype = "N/A";
                                                        break;
                                                }
                                                temp += connectype + " ";
                                                temp += "\n";
                                                temp += "IP/ISDN address: ";
                                                if (AudioAddon == true)
                                                {
                                                    temp += cfdt.Address;
                                                    if (!string.IsNullOrEmpty(cfdt.ConferenceCode))
                                                        temp += "D" + cfdt.ConferenceCode;
                                                    if (!string.IsNullOrEmpty(cfdt.ConferenceCode))
                                                        temp += "+" + cfdt.LeaderPin;
                                                }
                                                else
                                                    temp += cfdt.Address;
                                                temp += "\n";
                                                temp += "Connection type : ";
                                                switch (cfdt.ConnectionType)
                                                {
                                                    case 1: connectype = "Dial in";
                                                        break;
                                                    case 2: connectype = "Dial out";
                                                        break;
                                                    case 3: connectype = "Direct";
                                                        break;
                                                    default: connectype = "N/A";
                                                        break;
                                                }
                                                temp += connectype;
                                                temp += "\\n";
                                                temp += "\\n";

                                                if (AudioAddon == true)
                                                {

                                                    if (!string.IsNullOrEmpty(cfdt.Address))
                                                        Audiotemp1 += "Audio Dial In # :" + " " + cfdt.Address + " ";

                                                    if (!string.IsNullOrEmpty(cfdt.ConferenceCode))
                                                        Audiotemp1 += "Conference Code :" + " " + cfdt.ConferenceCode + " ";
                                                    else
                                                        Audiotemp1 += "Conference Code : N/A" + " ";

                                                    if (!string.IsNullOrEmpty(participantCode))
                                                        Audiotemp1 += "Participant Code :" + " " + participantCode + " ";
                                                    else
                                                        Audiotemp1 += "Participant Code : N/A" + " ";

                                                    Audiotemp1 += "\n";
                                                }

                                            }
                                        }
                                        if (n1 == 1)
                                            temp = temp1 + " " + temp;
                                        else
                                            temp = temp1 + " N/A";
                                        //sb.AppendLine(temp);
                                        //sb.AppendLine();
                                        sb.Replace("{105}", temp + "\\n");
                                        sb.AppendLine();
                                        n1 = 0;

                                        if (!string.IsNullOrEmpty(Audiotemp1))
                                        {
                                            Audiotemp1 = "\n" + Audiotemp + Audiotemp1;
                                            sb.Replace("{15}", Audiotemp1);
                                            //sb.AppendLine();
                                        }
                                        else
                                        {
                                            Audiotemp1 = "\n" + Audiotemp + "N/A";
                                            sb.Replace("{15}", Audiotemp1);
                                            //sb.AppendLine();
                                        }
                                        sb.Replace("{15}", "");
                                        if (Conference.Rooms.Count > 0)
                                        {
                                            temp = "";
                                            foreach (ConferenceEndpoint cfdt in Conference.AdvancedAudioVideoSettings.Endpoints)
                                            {

                                                if (cfdt.Type == ConferenceEndpointType.Room)
                                                {

                                                    if (!String.IsNullOrEmpty(cfdt.EndPointName))
                                                    {
                                                        n1 = 1;
                                                        temp += MyVrmService.Service.OrganizationOptions.OnFlyTier.DefTopTier;
                                                        temp += " > ";
                                                        temp += MyVrmService.Service.OrganizationOptions.OnFlyTier.DefMiddleTier;
                                                        temp += " > ";
                                                        temp += cfdt.EndPointName;
                                                        temp += " \n ";
                                                        if (!string.IsNullOrEmpty(cfdt.ExternalUserEmail))
                                                        {
                                                            temp += "Contact Email > ";
                                                            temp += cfdt.ExternalUserEmail;
                                                            temp += " \n ";
                                                        }
                                                        temp += "Endpoint : ";
                                                        //sb.AppendLine(temp);
                                                        string n = "";
                                                        // temp += " \n ";
                                                        if (cfdt.ConnectionType > 0)
                                                        {

                                                            switch (cfdt.ConnectionType)
                                                            {
                                                                case 1: n = "Dial in";
                                                                    break;
                                                                case 2: n = "Dial out";
                                                                    break;
                                                                case 3: n = "Direct";
                                                                    break;
                                                                default: n = "N/A";
                                                                    break;
                                                            }
                                                        }
                                                        temp += "(Endpoint Address: " + cfdt.Address + "; Connect Type - " + n + ")" + " \n ";

                                                    }
                                                }
                                            }
                                        }
                                        if (n1 == 1)
                                            //sb.AppendLine(temp1 + temp);
                                            sb.Replace("{68}", " " + temp);
                                        else
                                            sb.Replace("{68}", " " + "N/A");



                                        sb.Replace("{91}", "");
                                        sb.Replace("{92}", "");
                                        ////  sb.AppendLine();

                                        //sb.AppendLine();
                                        ////temp = "Audio Information: ";
                                        ////if (String.IsNullOrEmpty(oldConf.AudioVideoParameters.AudioCodec.ToString()))
                                        ////    temp += oldConf.AudioVideoParameters.AudioCodec.ToString();
                                        ////else
                                        ////    temp += "N/A";
                                        //// sb.AppendLine(temp);
                                        ////   sb.AppendLine();


                                        // sb.Replace("{63}", ""); 
                                        string ConferenceSupporttemp = "";
                                        sbical = new StringBuilder();
                                        if (MyVrmService.Service.OrganizationOptions.EnableCongSupport)
                                        {
                                            sbical.AppendLine("Conference Support:");
                                            //sbical.AppendLine();
                                            if (MyVrmService.Service.OrganizationOptions.OnSiteAVSupportinEmail)
                                            {
                                                ConferenceSupporttemp = "On-Site A/V Support: ";
                                                if (oldConf.ConciergeSupportParams.OnSiteAVSupport)
                                                    ConferenceSupporttemp += "Yes";
                                                else
                                                    ConferenceSupporttemp += "No";
                                                sbical.AppendLine(ConferenceSupporttemp);
                                            }
                                            if (MyVrmService.Service.OrganizationOptions.MeetandGreetinEmail)
                                            {
                                                ConferenceSupporttemp = "Meet and Greet: ";
                                                if (oldConf.ConciergeSupportParams.MeetandGreet)
                                                    ConferenceSupporttemp += "Yes";
                                                else
                                                    ConferenceSupporttemp += "No";
                                                sbical.AppendLine(ConferenceSupporttemp);
                                            }
                                            if (MyVrmService.Service.OrganizationOptions.ConciergeMonitoringinEmail)
                                            {
                                                ConferenceSupporttemp = "Call Monitoring: ";
                                                if (oldConf.ConciergeSupportParams.ConciergeMonitoring)
                                                    ConferenceSupporttemp += "Yes";
                                                else
                                                    ConferenceSupporttemp += "No";
                                                sbical.AppendLine(ConferenceSupporttemp);
                                            }
                                            if (MyVrmService.Service.OrganizationOptions.DedicatedVNOCOperatorinEmail)
                                            {
                                                ConferenceSupporttemp = "Dedicated VNOC Operator: ";
                                                if (oldConf.ConciergeSupportParams.DedicatedVNOCOperator)
                                                    ConferenceSupporttemp += "Yes";
                                                else
                                                    ConferenceSupporttemp += "No";
                                                sbical.AppendLine(ConferenceSupporttemp);
                                            }
                                            string ConferenceSupport = sbical.AppendLine().ToString();
                                            sb.Replace("{63}", ConferenceSupport);
                                        }
                                        else
                                            sb.Replace("{63}", "");

                                        sbical = new StringBuilder();
                                        sbical.AppendLine("Custom Options: ");
                                        temp = "";
                                        string CustomAttributes = "";
                                        if (oldConf.CustomAttributes != null)
                                        {
                                            foreach (CustomAttribute CustAttr in oldConf.CustomAttributes)
                                            {
                                                if (!string.IsNullOrEmpty(CustAttr.Value) && CustAttr.FieldController == "Special Instructions")
                                                    temp = CustAttr.FieldController + ": " + CustAttr.Value + " \n ";
                                                if (CustAttr.Status == 0) //ZD 103181
                                                {
                                                    if (!string.IsNullOrEmpty(CustAttr.Value) && CustAttr.FieldController == "Entity Code")
                                                        temp += CustAttr.FieldController + ": " + CustAttr.Value;
                                                }
                                            }

                                            if (string.IsNullOrEmpty(temp))
                                            {
                                                sbical = new StringBuilder();
                                                temp = "Custom Options: N/A";
                                                CustomAttributes = sbical.Append(temp).ToString();
                                            }
                                            else
                                            {

                                                sbical.AppendLine();
                                                CustomAttributes = sbical.AppendLine(temp).ToString();
                                            }
                                            sb.Replace("{16}", CustomAttributes);
                                        }
                                        else
                                        {
                                            sb.Replace("{16}", "");
                                        }



                                        // sb.AppendLine();
                                        var hostProfile = MyVrmService.Service.GetUser(Conference.HostId);

                                        sbical = new StringBuilder();
                                        if (Conference.VMRType == VMRConferenceType.Room && vmrDetails.Length > 0)
                                        {
                                            //  sb.AppendLine();
                                            sbical.AppendLine();
                                            //sbical.AppendLine(Strings.VmrDetails);
                                            sbical.AppendLine(vmrDetails);
                                        }
                                        else if (Conference.VMRType == VMRConferenceType.Personal)
                                        {
                                            GetNewConferenceResponse response = MyVrmService.Service.GetNewConference();
                                            if (response != null)
                                            {
                                                // sb.AppendLine();
                                                sbical.AppendLine();
                                                //sbical.AppendLine(Strings.VmrDetails);
                                                string details = "Internal Bridge:";
                                                details += response.internalBridge;
                                                details += "\n";
                                                details += "External Bridge:";

                                                details += response.externalBridge;
                                                sbical.AppendLine(details);
                                            }
                                        }
                                        else if (Conference.VMRType == VMRConferenceType.External)
                                        {
                                            GetNewConferenceResponse response = MyVrmService.Service.GetNewConference();
                                            if (response != null)
                                            {
                                                //  sb.AppendLine();
                                                sbical.AppendLine();
                                                //sbical.AppendLine(Strings.VmrDetails);
                                                strVMR = StripTagsRegex(response.PrivateVMR);
                                                strVMR = StripTagsRegexCompiled(response.PrivateVMR);
                                                strVMR = StripTagsCharArray(response.PrivateVMR);
                                                strVMR = strVMR.Replace("\n\n\n", "\n").Replace("\n\n", "\n");
                                                string details = strVMR;
                                                sbical.AppendLine(details);
                                            }
                                        }

                                        if (!string.IsNullOrEmpty(sbical.ToString()))
                                        {
                                            //sbical.AppendLine("VMR :");
                                            //sbical.AppendLine(vmrDetails);
                                            sbical.AppendLine();
                                            string VMR = sbical.AppendLine().ToString();
                                            sb.Replace("{69}", " " + VMR);
                                        }
                                        else
                                            sb.Replace("{69}", " N/A \n\n");

                                        sb.Replace("{73}", "");
                                        sb.Replace("{79}", "");
                                        //ZD 103112 start
                                        string ConferenceDescription = Appointment.Body.Trim() + "\\n\\n";
                                        string ConfDefaultBodyText_1 = "*********DO NOT DELETE OR CHANGE ANY OF THE TEXT BELOW THIS LINE*********";
                                        string[] stringSeparators = new string[] { "*********DO NOT DELETE OR CHANGE ANY OF THE TEXT BELOW THIS LINE*********" };
                                        string ConfDefaultBodyText_2 = "\\n\\n*********DO NOT DELETE OR CHANGE ANY OF THE TEXT BELOW THIS LINE*********\\n\\n";

                                        if (ConferenceDescription.Contains(ConfDefaultBodyText_1))
                                        {
                                            string[] part1 = ConferenceDescription.Split(stringSeparators, StringSplitOptions.None);
                                            ConferenceDescription = part1[0];

                                            ConferenceDescription = ConferenceDescription + ConfDefaultBodyText_2;
                                        }
                                        else
                                        {

                                            ConferenceDescription = ConferenceDescription + ConfDefaultBodyText_2;
                                        }
                                        string icalmsgbody = ConferenceDescription + sb.ToString();
                                        //icalmsgbody = icalmsgbody.Replace("@", System.Environment.NewLine);
                                        Appointment.Body = icalmsgbody.Replace("\\n", Environment.NewLine);
                                        //Appointment.Body.Save();
                                        //   meetingItem.Send();
                                        //ZD 103112 End
                                    }
                    #endregion
                }
				if (!e.Cancel)
					OnAfterSave(e);
            	e.Cancel = bCancelSave;
                return !e.Cancel;
			}
			catch (SaveConferenceCancelledException)
			{
				return false;
			}
        }

        private bool ResolveRecurringConflicts(out List<OutlookAppointmentWithMark> outLookAppointmentList)
        {
            outLookAppointmentList = new List<OutlookAppointmentWithMark>();
            if (Conference.IsRecurring && Conference.Rooms.Count > 0)
            {
                var cursor = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                RecurrenceConferencesResponse retResponse;
                var id = Conference.Id ?? ConferenceId.NewConferenceId;

                try
                {
                    retResponse = MyVrmService.Service.GetConcurrentConfsList(Conference);
                }
                finally
                {
                    Cursor.Current = cursor;
                }
                if (!retResponse.HasConflicts)
                    return true;
                if (DialogResult.No ==
                           UIHelper.ShowMessage(Strings.ResolveRecConfConflictMsg, MessageBoxButtons.YesNo,
                                                MessageBoxIcon.Question))
                    return false;
                
                var roomTimetable = new List<RoomTimetable>();
                var minDate = DateTime.MaxValue;
                var maxDate = DateTime.MinValue;
				var workingTime = new OrganizationOptions.SystemAvailableTime();

                var retOccurs = retResponse.Occurences;
                foreach (var occur in retOccurs)
                {
					if (minDate > occur.StartDate)
						minDate = occur.StartDate;
					if (maxDate < occur.StartDate)
						maxDate = occur.StartDate;
                }

                cursor = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                try
                {
                    foreach (var roomId in Conference.Rooms)
                    {
						for (DateTime monDate = minDate; monDate.Year <= maxDate.Year && monDate.Month <= maxDate.Month; monDate = monDate.AddMonths(1))
                        {
                            var res = MyVrmService.Service.GetRoomMonthlyCalendar(roomId, monDate);
                            roomTimetable.Add(new RoomTimetable(MyVrmAddin.Instance.GetRoomFromCache(roomId), res));
                        }
                    }
					OrganizationOptions orgOptions = MyVrmService.Service.GetOrganizationOptions();
					if (orgOptions != null)
						workingTime = orgOptions.AvailableTime;
                }
                finally
                {
                    Cursor.Current = cursor;
                }

            	DateTime rightBorder = ConflictResolveDialog.SchedulerMaxDateTime;
				if( Conference.RecurrencePattern.HasEnd )
				{
					rightBorder = Conference.RecurrencePattern.GetLastOccurrence().End;
					rightBorder = rightBorder.Date.AddDays(1);
				}
            	using (var dlg = new ConflictResolveDialog(roomTimetable, retOccurs.ToList(), id, Name, workingTime,
					Conference.StartDate, rightBorder))
                {
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
						OccurrenceInfoExtendCollection resultAppointments = dlg.ResultAppointments;
                        // get dates
                        var occurrences = Conference.RecurrencePattern.GetOccurrenceInfoList(minDate,
                                                                                             maxDate.Date.AddDays(1));
                        Conference.RecurrencePattern.RecurrenceType = RecurrenceType.Custom;
                        // fill custom dates
                        Conference.RecurrencePattern.CustomDates.Clear();
                        Conference.ModifiedOccurrences.Clear();

                        foreach (var occurrenceInfo in occurrences)
                        {
                            var added = false;
                            var deleted = false;

                            foreach (var occ in //indeed only one element
								resultAppointments.Where(
                                    occ => occurrenceInfo.Start.Date == occ.OrigStart.Date))
                            {
                                var outlookApp = new OutlookAppointmentWithMark
                                                     {
                                                         AppointmentStartTime = occ.Start,
                                                         AppointmentEndTime = occ.End,
                                                         OriginalStart = occurrenceInfo.Start,
                                                         IsDeleted = occ.IsDeleted
                                                     };

                                if (occ.IsDeleted)
                                {
                                    deleted = true;
                                }
										
                                outLookAppointmentList.Add(outlookApp);

                                if (!deleted)
                                {
									Conference.RecurrencePattern.CustomDates.Add(occ.Start);
                                    Conference.ModifiedOccurrences.Add(occ);
                                    added = true;
                                }
                                break;
                            }
                            if (!added && !deleted)
                            {
                                Conference.RecurrencePattern.CustomDates.Add(occurrenceInfo.Start);
                                Conference.ModifiedOccurrences.Add(occurrenceInfo);
                            }
                        }
						//Sort CustomDates and ModifiedOccurrences in calendar order
						Conference.RecurrencePattern.CustomDates.Sort();
						OccurrenceInfoCollection orderedModifiedOccurrences = new OccurrenceInfoCollection();
						foreach( DateTime dt in Conference.RecurrencePattern.CustomDates)
						{
							orderedModifiedOccurrences.Add(Conference.ModifiedOccurrences.FirstOrDefault(occInfo => occInfo.Start == dt));
						}
						Conference.ModifiedOccurrences.Clear();
                    	foreach (var orderedModifiedOccurrence in orderedModifiedOccurrences)
                    	{
							Conference.ModifiedOccurrences.Add(orderedModifiedOccurrence);
                    	}
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private void SaveEndpoints()
        {
            Conference.AdvancedAudioVideoSettings.Endpoints.Clear();
            // Add room endpoints
            foreach (var conferenceEndpoint in RoomEndpoints)
            {
                Conference.AdvancedAudioVideoSettings.Endpoints.Add(conferenceEndpoint);
            }
            // Add audio conference bridges
            foreach (var audioConferenceBridge in AudioConferenceBridges)
            {
                var participant = Conference.Participants.FirstOrDefault(
                    part => part.UserId == audioConferenceBridge.Endpoint.Id as UserId);
                if (participant == null)
                {
                    Conference.Participants.Add(new Participant(audioConferenceBridge.Endpoint.Id as UserId)
                                                    {
                                                        AudioVideoMode =
                                                            MediaType.AudioOnly,
                                                        InvitationMode =
                                                            ParticipantInvitationMode.External
                                                    });
                }

                Conference.AdvancedAudioVideoSettings.Endpoints.Add(audioConferenceBridge.Endpoint);
            }

			// Add external rooms 
			if( Conference.Participants != null)
			{
				Conference.Participants.AddRange(Conference.ExternalParticipants);
			}
			foreach (var externalRoom in ExternalRoomsAVSettings)
			{
				//var participant = Conference.ExternalParticipants.FirstOrDefault(
				//    part => part.UserId == externalRoom.Endpoint.Id as UserId);
				//if (participant != null)
				//{
				//    if (Conference.Participants.FirstOrDefault(a => a.UserId == participant.UserId) == null)
				//    {
				//        Conference.Participants.Add(new Participant(externalRoom.Endpoint.Id as UserId)
				//        {
				//            AudioVideoMode =
				//                MediaType.AudioVideo,
				//            InvitationMode =
				//                ParticipantInvitationMode.External
				//        });
				//    }
				//}

				Conference.AdvancedAudioVideoSettings.Endpoints.Add(externalRoom.Endpoint);
			}
        }

        #region Implementation of IDXDataErrorInfo

        public void GetPropertyError(string propertyName, ErrorInfo info)
        {
            switch (propertyName)
            {
                case "Name":
                    if (string.IsNullOrEmpty(Name))
                    {
                        info.ErrorText = Strings.ConferenceNameRequired;
                    }
                    else if (Regex.IsMatch(Name, Strings.ConferenceNameRegexPattern))
                    {
                        info.ErrorText = Strings.ConferenceNameContainsInvalidCharacters;
                    }
                    break;
                case "Password":
                    if (MyVrmService.Service.OrganizationOptions.EnableConferencePassword)
                    {
                        if (!string.IsNullOrEmpty(Password))
                        {
                            Regex regex = new Regex(@"^[0-9]+$");
                            if (!regex.IsMatch(Password))
                            {
                                info.ErrorText = Strings.ConferencePasswordNumeric;
                                break;
                            }
                            if (Password.Length < (MyVrmService.Service.OrganizationOptions.PasswordCharLength) || Password[0] == '0')
                            {
                                info.ErrorText = Strings.ConferencePasswordrule + MyVrmService.Service.OrganizationOptions.PasswordCharLength + Strings.ConferencePasswordrule1; //ZD 102562
                                
                                //info.ErrorText = "Minimum Length is "+ MyVrmService.Service.OrganizationOptions.PasswordCharLength + ". First digit should be non-zero";
                            }
                        }
                    }
                    break;
            }
        }

        public void GetError(ErrorInfo info)
        {
        }

        #endregion

        private void Initialize()
        {

            if (!IsNew)
                return;
            Name = Appointment.Subject;
            Description = Appointment.Body;
            StartDate = Appointment.StartInStartTimeZone;
            EndDate = Appointment.EndInEndTimeZone;

            TimeZone = Appointment.StartTimeZone;
            AllDayEvent = Appointment.AllDayEvent;
            var currentUser = Appointment.CurrentUser;
            IsRecurring = Appointment.IsRecurring;
            if (IsRecurring)
            {
                RecurrencePattern = Appointment.GetRecurrencePattern(Conference.TimeZone);
            }
            var email = "";
            Func<Participant, bool> findParticipantByEmailPredicate =
                match => string.Equals(match.Email, email, StringComparison.InvariantCultureIgnoreCase);
            email = currentUser.SmtpAddress;
            if (Participants.FirstOrDefault(findParticipantByEmailPredicate) == null)
            {
                Participants.Add(new Participant(currentUser.SmtpAddress)
                                     {
                                         FirstName = currentUser.Name,
                                         InvitationMode = ParticipantInvitationMode.Room
                                     });
            }

            foreach (var recipient in Appointment.Recipients)
            {
                email = recipient.SmtpAddress;
                if (Participants.FirstOrDefault(findParticipantByEmailPredicate) == null)
                {
                    Participants.Add(new Participant(recipient.SmtpAddress)
                                         {
                                             FirstName = recipient.Name,
                                             InvitationMode = ParticipantInvitationMode.Room
                                         });
                }
            }
        }

        private void ClearEndpoints()
        {
            AudioConferenceBridges.Clear();
            RoomEndpoints.Clear();
			ExternalRoomsAVSettings.Clear();
        }

        #region Implementation of INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;


        private void NotifyPropertyChanged(String info)
        {
            UpdateAppointment(info);
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        #endregion

        void UpdateAppointment(string conferencePropertyName)
        {
            switch(conferencePropertyName)
            {
                case "StartDate":
                {
                    Appointment.StartInStartTimeZone = StartDate;
                    Appointment.StartTimeZone = TimeZone;
                    break;
                }
                case "EndDate":
                {
                    if (!Appointment.IsRecurring)
                    {
                        Appointment.EndInEndTimeZone = EndDate; //ZD 102305
                    }
                    else
                        Appointment.EndInEndTimeZone = Appointment.EndInEndTimeZone;

                    Appointment.EndTimeZone = TimeZone;
                    break;
                }
                case "Duration":
                {
                    Appointment.Duration = Duration;
                    break;
                }
                case "Name":
                {
                    Appointment.Subject = Name;
                    break;
                }
            }
        }

    	private static WebServices.Data.Conference _conferenceCopy = null;

		public void ClearConfCopy()
		{
			_conferenceCopy = null;
		}

    	public bool IsConfChanged()
		{
			if (_conferenceCopy != null)
				return !_conferenceCopy.Equals(Conference);
			return true;
		}

    	void AppointmentSending(object sender, CancelEventArgs e)
        {
			AppointmentSaving(sender, e);
        }
		//static bool bInternalChange = false;

        private void AppointmentSaving(object sender, CancelEventArgs e)
        {
            try
            {
				bool nonOutlookPropsChanged = Appointment.GetNonOutlookProperty();
				
				if (Conference.IsDirty || nonOutlookPropsChanged )
				{
					e.Cancel = !Save();
					if (!e.Cancel)
					{
						UIHelper.ShowConferenceIsSuccessfullyCreatedPopup(null);
                        
						_conferenceCopy = (WebServices.Data.Conference) Conference.Clone();
					}
				}
            }
            catch (Exception exception)
            {
                UIHelper.ShowError(exception.Message);
                e.Cancel = true;
            }
        }

        void AppointmentPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            try
            {
				//Trace.WriteLine("** !!! ** PropertyName = " + e.PropertyName);
               
                switch (e.PropertyName)
                {
                    case "Subject":
                    {
                        Name = Appointment.Subject;
                        break;
                    }
                    case "BusyStatus":
                    {
                        TimeZone = Appointment.StartTimeZone; //ZD 102305
                        StartDate = Appointment.StartInStartTimeZone;
                        EndDate = Appointment.EndInEndTimeZone;
                        break;
                    }
                    case "StartInStartTimeZone":
                    {
                        TimeZone = Appointment.StartTimeZone; //ZD 102305
                        StartDate = Appointment.StartInStartTimeZone;
                        EndDate = Appointment.EndInEndTimeZone;
                        AllDayEvent = Appointment.AllDayEvent;
                       
                        //TimeZone = Appointment.StartTimeZone;
                        break;
                    }
                    case "EndInEndTimeZone":
                    {
                        StartDate = Appointment.StartInStartTimeZone;
                        EndDate = Appointment.EndInEndTimeZone;
                        AllDayEvent = Appointment.AllDayEvent;
                        break;
                    }
                    case "AllDayEvent":
                    {
                        AllDayEvent = Appointment.AllDayEvent;
                        break;
                    }
                   // case "Duration":
                    //{
                      //  Duration = Appointment.Duration;
                        //break;
                    //}
                    //case "Start":
                    //{
                    //    StartDate = Appointment.StartInStartTimeZone;
                    //    //EndDate = Appointment.EndInEndTimeZone;
                    //    TimeZone = Appointment.StartTimeZone;
                    //    break;
                    //}

                    //case "End":
                    //{
                    //    StartDate = Appointment.StartInStartTimeZone;
                    //    EndDate = Appointment.EndInEndTimeZone;
                    //    break;
                    //}
                    case "IsRecurring":
                    {
                        IsRecurring = Appointment.IsRecurring;
                        if (Appointment.IsRecurring)
                        {
                            RecurrencePattern = Appointment.GetRecurrencePattern(Conference.TimeZone);
                        }
                        break;
                    }
                    case "Recipients":
                    {
                        foreach (var recipient in from recipient in Appointment.Recipients
                                                  let participant = Participants.FirstOrDefault(item => string.Equals(recipient.SmtpAddress, item.Email, StringComparison.InvariantCultureIgnoreCase))
                                                  where participant == null
                                                  select recipient)
                        {
                            Participants.Add(new Participant(recipient.Name, "", recipient.SmtpAddress)
                                             {
                                                 InvitationMode = ParticipantInvitationMode.Room
                                             });
                        }
                        var participantsToDelete =
                            Participants.Where(participant =>
                                               !Appointment.Recipients.Any(
                                                   recipient => string.Equals(recipient.SmtpAddress, participant.Email, StringComparison.InvariantCultureIgnoreCase)) &&
                                               !string.Equals(Appointment.CurrentUser.SmtpAddress, participant.Email, StringComparison.InvariantCultureIgnoreCase)).ToList();
                        foreach (var participant in participantsToDelete)
                        {
                            Participants.Remove(participant);
                        }
                        break;
                    }
                }
            }
            catch (Exception exception)
            {
                MyVrmAddin.TraceSource.TraceException(exception);
                UIHelper.ShowError(exception.Message);
            }
        }

        void AppointmentBeforeDelete(object sender, CancelEventArgs e)
        {
        }

        private void RoomsListChanged(object sender, ListChangedEventArgs e)
        {
            switch(e.ListChangedType)
            {
                case ListChangedType.Reset:
                {
                    Conference.Rooms.Clear();
                    Conference.Rooms.AddRange(_rooms);
                    break;
                }
                case ListChangedType.ItemAdded:
                {
                    Conference.Rooms.Insert(e.NewIndex, _rooms[e.NewIndex]);
                    break;
                }
                case ListChangedType.ItemDeleted:
                {
                    Conference.Rooms.RemoveAt(e.NewIndex);
                    break;
                }
				case ListChangedType.ItemMoved:
        			RoomId id = Conference.Rooms[e.OldIndex];
        			Conference.Rooms.RemoveAt(e.OldIndex);
        			Conference.Rooms.Insert(e.NewIndex, id);
            		break;
                case ListChangedType.ItemChanged:
                {
                    Conference.Rooms[e.NewIndex] = _rooms[e.NewIndex];
                    break;
                }
                case ListChangedType.PropertyDescriptorAdded:
                    break;
                case ListChangedType.PropertyDescriptorDeleted:
                    break;
                case ListChangedType.PropertyDescriptorChanged:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        void ParticipantsListChanged(object sender, ListChangedEventArgs e)
        {
            switch(e.ListChangedType)
            {
                case ListChangedType.Reset:
                {
                    Conference.Participants.Clear();
                    Conference.Participants.AddRange(_participants);
                    break;
                }
                case ListChangedType.ItemAdded:
                {
                    Conference.Participants.Insert(e.NewIndex, _participants[e.NewIndex]);
                    break;
                }
                case ListChangedType.ItemDeleted:
                {
                    var email = Conference.Participants[e.NewIndex].Email;
                    Conference.Participants.RemoveAt(e.NewIndex);
                    Appointment.RemoveRecipient(email);
                    break;
                }
                case ListChangedType.ItemMoved:
                {
                    break;
                }
                case ListChangedType.ItemChanged:
                    break;
                case ListChangedType.PropertyDescriptorAdded:
                    break;
                case ListChangedType.PropertyDescriptorDeleted:
                    break;
                case ListChangedType.PropertyDescriptorChanged:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

		void ExternalParticipantsListChanged(object sender, ListChangedEventArgs e)
		{
			switch (e.ListChangedType)
			{
				case ListChangedType.Reset:
					{
						Conference.ExternalParticipants.Clear();
						Conference.ExternalParticipants.AddRange(_externalParticipants);
						break;
					}
				case ListChangedType.ItemAdded:
					{
						Conference.ExternalParticipants.Insert(e.NewIndex, _externalParticipants[e.NewIndex]);
						break;
					}
				case ListChangedType.ItemDeleted:
					{
						var email = Conference.ExternalParticipants[e.NewIndex].Email;
						Conference.ExternalParticipants.RemoveAt(e.NewIndex);
						Appointment.RemoveRecipient(email);
						break;
					}
				case ListChangedType.ItemMoved:
					{
						break;
					}
				case ListChangedType.ItemChanged:
					break;
				case ListChangedType.PropertyDescriptorAdded:
					break;
				case ListChangedType.PropertyDescriptorDeleted:
					break;
				case ListChangedType.PropertyDescriptorChanged:
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
        // ZD 101343 starts
        void ConfGuestRoomsChanged(object sender, ListChangedEventArgs e)
        {
            switch (e.ListChangedType)
            {
                case ListChangedType.Reset:
                    {
                        Conference.ConfGuestRooms.Clear();
                        Conference.ConfGuestRooms.AddRange(_confGuestRooms);
                        break;
                    }
                case ListChangedType.ItemAdded:
                    {
                        Conference.ConfGuestRooms.Insert(e.NewIndex, _confGuestRooms[e.NewIndex]);
                        break;
                    }
                case ListChangedType.ItemDeleted:
                    {
                        var email = Conference.ConfGuestRooms[e.NewIndex].Email;
                        Conference.ConfGuestRooms.RemoveAt(e.NewIndex);
                        Appointment.RemoveRecipient(email);
                        break;
                    }
                case ListChangedType.ItemMoved:
                    {
                        break;
                    }
                case ListChangedType.ItemChanged:
                    break;
                case ListChangedType.PropertyDescriptorAdded:
                    break;
                case ListChangedType.PropertyDescriptorDeleted:
                    break;
                case ListChangedType.PropertyDescriptorChanged:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        // ZD 101343 Ends
//ZD 100818 Starts
        #region Check ExternalRooms
        public bool chkExternalRooms()                        //ZD  100818 
        {
            if (Conference.ExternalParticipants.Count > 0 || Conference.ConfGuestRooms.Count > 0) //ZD 101537
            {
                return true;
            }
            return false;
        }
        #endregion
		//ZD 100818 ends
    }

       
}