using System;

namespace MyVrm.DataImport.Excel.WinForms
{
    /// <summary>
    /// Represents the collection of column headers in a ListView control.
    /// </summary>
    public class ColumnHeaderCollection : System.Collections.IList
    {
        private DataListView owner;
        private System.Collections.ArrayList columnHeadersArray = new System.Collections.ArrayList(2);

        /// <summary>
        /// Initializes a new instance of the ListView.ColumnHeaderCollection class.
        /// </summary>
        /// <param name="owner">The ListView control that owns this collection.</param>
        public ColumnHeaderCollection(DataListView owner) : base()
        {
            this.owner = owner;
        }

        /// <summary>
        /// Adds a column header to the collection with specified text, width, and alignment settings.
        /// </summary>
        /// <param name="text">The text to display in the column header.</param>
        /// <param name="displayMember"></param>
        /// <param name="width">The initial width of the column header. Set to -1 to autosize the column header to the size of the largest subitem text in the column or -2 to autosize the column header to the size of the text of the column header.</param>
        /// <param name="textAlign">One of the HorizontalAlignment values.</param>
        /// <returns>The ColumnHeader that was created and added to the collection.</returns>
        public virtual ColumnHeader Add(string text, string displayMember, int width, System.Windows.Forms.HorizontalAlignment textAlign)
        {
            ColumnHeader columnHeader = new ColumnHeader();
            columnHeader.Text = text;
            columnHeader.DisplayMember = displayMember;
            columnHeader.Width = width;
            columnHeader.TextAlign = textAlign;
            Add(columnHeader);
            return columnHeader;
        }

        /// <summary>
        /// Adds an existing ColumnHeader to the collection.
        /// </summary>
        /// <param name="columnHeader">The ColumnHeader to add to the collection.</param>
        /// <returns>The zero-based index into the collection where the item was added.</returns>
        public virtual int Add(ColumnHeader columnHeader)
        {
            return this.columnHeadersArray.Add(columnHeader);
        }

        /// <summary>
        /// Adds an array of column headers to the collection.
        /// </summary>
        /// <param name="columnHeaders">An array of ColumnHeader objects to add to the collection.</param>
        public virtual void AddRange(ColumnHeader[] columnHeaders)
        {
            if (columnHeaders == null)
            {
                throw new ArgumentNullException("columnHeaders");
            }
            this.columnHeadersArray.AddRange(columnHeaders);
        }

        /// <summary>
        /// Gets or sets the column header at the specified index within the collection.
        /// </summary>
        public ColumnHeader this[int index]
        {
            get
            {
                return (ColumnHeader)this.columnHeadersArray[index];
            }
        }

        /// <summary>
        /// Removes all column headers from the collection.
        /// </summary>
        public virtual void Clear()
        {
            this.columnHeadersArray.Clear();   
        }

        /// <summary>
        /// Determines whether the specified column header is located in the collection.
        /// </summary>
        /// <param name="columnHeader">A ColumnHeader representing the column header to locate in the collection.</param>
        /// <returns>true if the column header is contained in the collection; otherwise, false.</returns>
        public bool Contains(ColumnHeader columnHeader)
        {
            return this.columnHeadersArray.Contains(columnHeader);
        }

        /// <summary>
        /// Returns the index within the collection of the specified column header.
        /// </summary>
        /// <param name="columnHeader">A ColumnHeader representing the column header to locate in the collection.</param>
        /// <returns>The zero-based index of the column header's location in the collection. If the column header is not located in the collection, the return value is negative one (-1).</returns>
        public int IndexOf(ColumnHeader columnHeader)
        {
            return this.columnHeadersArray.IndexOf(columnHeader);
        }

        /// <summary>
        /// Inserts an existing column header into the collection at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index location where the column header is inserted.</param>
        /// <param name="columnHeader">The ColumnHeader to insert into the collection.</param>
        public void Insert(int index, ColumnHeader columnHeader)
        {
            if (index < 0 || index > Count)
            {
                throw new ArgumentOutOfRangeException("index");
            }
            this.columnHeadersArray.Insert(index, columnHeader);
        }

        /// <summary>
        /// Removes the specified column header from the collection.
        /// </summary>
        /// <param name="columnHeader">A ColumnHeader representing the column header to remove from the collection.</param>
        public void Remove(ColumnHeader columnHeader)
        {
            int index = IndexOf(columnHeader);
            if(index != -1)
            {
                RemoveAt(index);
            }
        }

        /// <summary>
        /// Removes the column header at the specified index within the collection.
        /// </summary>
        /// <param name="index">The zero-based index of the column header to remove.</param>
        public void RemoveAt(int index)
        {
            this.columnHeadersArray.RemoveAt(index);
        }

        #region IList Members

        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        object System.Collections.IList.this[int index]
        {
            get
            {
                return this[index];
            }
            set
            {
                throw new NotSupportedException();
            }
        }

        void System.Collections.IList.Insert(int index, object value)
        {
            if (value as ColumnHeader != null)
            {
                Insert(index, (ColumnHeader)value);
            }
        }

        void System.Collections.IList.Remove(object value)
        {
            if (value as ColumnHeader != null)
            {
                Remove((ColumnHeader)value);
            }
        }

        bool System.Collections.IList.Contains(object value)
        {
            if (value as ColumnHeader != null)
            {
                return Contains((ColumnHeader)value);
            }
            return false;
        }

        int System.Collections.IList.IndexOf(object value)
        {
            if (value as ColumnHeader != null)
            {
                IndexOf((ColumnHeader)value);
            }
            return -1;
        }

        int System.Collections.IList.Add(object value)
        {
            if (value as ColumnHeader != null)
            {
                return Add((ColumnHeader)value);
            }
            throw new ArgumentException("ColumnHeaderCollectionInvalidArgument");
        }

        bool System.Collections.IList.IsFixedSize
        {
            get
            {
                return false;
            }
        }

        #endregion

        #region ICollection Members

        public bool IsSynchronized
        {
            get
            {
                return true;
            }
        }

        public int Count
        {
            get
            {
                return this.columnHeadersArray.Count;
            }
        }

        public void CopyTo(Array array, int index)
        {
            if (Count > 0)
            {
                this.columnHeadersArray.CopyTo(array, index);
            }
        }

        public object SyncRoot
        {
            get
            {
                return this;
            }
        }

        #endregion

        #region IEnumerable Members

        /// <summary>
        /// Returns an enumerator to use to iterate through the column header collection.
        /// </summary>
        /// <returns>An IEnumerator that represents the column header collection.</returns>
        public virtual System.Collections.IEnumerator GetEnumerator()
        {
            return this.columnHeadersArray.GetEnumerator();
        }

        #endregion
    }
}