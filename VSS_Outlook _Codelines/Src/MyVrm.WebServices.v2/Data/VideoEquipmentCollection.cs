﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    public class VideoEquipmentCollection : BaseCollection<VideoEquipment>
    {
        internal VideoEquipmentCollection()
        {
        }

        #region Overrides of BaseCollection<VideoEquipment>

        internal override void LoadFromXml(MyVrmServiceXmlReader reader)
        {
            do
            {
                reader.Read();
                if (reader.IsStartElement(XmlNamespace.NotSpecified, "Equipment"))
                {
                    var videEquipment = new VideoEquipment();
                    videEquipment.LoadFromXml(reader);
                    Items.Add(videEquipment);
                }
                else
                {
                    reader.SkipCurrentElement();
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "VideoEquipment"));
        }

        #endregion
    }
}
