﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.WebServices.Data
{
    public class CateringProviderMenuItem : ComplexProperty
    {
        public int Id { get; internal set; }
        public string Name { get; internal set; }

		public override object Clone()
		{
			CateringProviderMenuItem copy = new CateringProviderMenuItem();
			copy.Name = string.Copy(Name);
			copy.Id = Id;

			return copy;
		}

        public override string ToString()
        {
            return !string.IsNullOrEmpty(Name) ? Name : base.ToString();
        }

        internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
        {
            switch(reader.LocalName)
            {
                case "ID":
                {
                    Id = reader.ReadElementValue<int>();
                    return true;
                }
                case "Name":
                {
                    Name = reader.ReadElementValue();
                    return true;
                }
            }
            return false;
        }
    }
}
