﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace CallWebApi.Classes
{
    /// <summary>
    /// Request to authentication action
    /// </summary>
    public class AuthRequest
    {
        /// <summary>
        /// User name/email
        /// </summary>
        public string Name { set; get; }

        /// <summary>
        /// User password
        /// </summary>
        public string Password { set; get; }
    }
}