﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop.Excel.Extensions;

namespace MyVrm.DataImport.Excel
{
    /// <summary>
    /// Represents a hidden worksheet.
    /// </summary>
    internal class HiddenWorksheet
    {
        private const string HiddenWorksheetPrefix = "myVRM_ValidationWS_";
        private Worksheet _worksheet;
        private List<int> _freeColumns;

        internal HiddenWorksheet(Workbook workbook)
        {
            // save active sheet
            var activeSheet = (Worksheet)workbook.ActiveSheet;
            _worksheet = (Worksheet)workbook.Worksheets.Add(Type.Missing, workbook.Worksheets[workbook.Worksheets.Count], 1,
                                                 Type.Missing);
            // restore previously active sheet
            if (activeSheet != null)
            {
                activeSheet.Activate();
            }
#if DEBUG
            // For debug purposes only. Make the sheet hidden (user can unhide this sheet)
            Worksheet.Visible = XlSheetVisibility.xlSheetHidden;
#else
            // Make the worksheet very hidden (end-user can't unhide this sheet)
            Worksheet.Visible = XlSheetVisibility.xlSheetVeryHidden;
#endif
            _worksheet.Name = MakeUniqueName(workbook);
            
            _freeColumns = new List<int>();
            for (int i = 1; i <= Worksheet.Columns.Count; i++)
            {
                _freeColumns.Add(i);
            }
        }

        internal HiddenWorksheet(Worksheet worksheet)
        {
            if (worksheet == null) 
                throw new ArgumentNullException("worksheet");

            if (!IsHiddenWorksheet(worksheet))
            {
                 throw new ArgumentException("Not a myVRM hidden worksheet");   
            }
            _worksheet = worksheet;
            InitializeFreeColumns();
        }

        private void InitializeFreeColumns()
        {
            _freeColumns = new List<int>();
            var cells = _worksheet.Cells;
            int columnsCount = cells.Columns.Count;
            var startCell = cells.Item(1, 1);
            var endCell = cells.Item(1, columnsCount);
            object[,] valuesArray = cells.get_Range(startCell, endCell).get_Value(Type.Missing) as object[,];
            for (int i = 0; i < _worksheet.Columns.Count; i++)
            {
                object value = valuesArray[i, 1];
                string val = (value != null) ? value.ToString() : null;
                if (string.IsNullOrEmpty(val))
                {
                    _freeColumns.Add(i);
                }
            }
        }

        internal Worksheet Worksheet
        {
            get { return _worksheet; }
        }

        public bool HasFreeColumns
        {
            get
            {
                return _freeColumns.Count > 0;
            }
        }

        internal static bool IsHiddenWorksheet(Worksheet worksheet)
        {
            return worksheet.Name.StartsWith(HiddenWorksheetPrefix, StringComparison.OrdinalIgnoreCase);
        }

        public Range Add(ICollection values, string numberFormat)
        {
            int valuesCount = values.Count;
            int freeColumnIndex = _freeColumns[0];

            var valuesArray = new object[valuesCount,1];
            int i = 0;
            foreach (var value in values)
            {
                string val = value as string;
                if (val == string.Empty)
                {
                    val = null;
                }
                valuesArray[i, 0] = val;
                i++;
            }
            var cells = Worksheet.Cells;
            var startCell = cells.Item(1, freeColumnIndex);
            var endCell = cells.Item(i, freeColumnIndex);
            var valuesRange = cells.get_Range(startCell, endCell);
            valuesRange.NumberFormat = numberFormat;
            valuesRange.set_Value(Type.Missing, valuesArray);
            _freeColumns.RemoveAt(0);
            return valuesRange;
        }

        private static string MakeUniqueName(Workbook workbook)
        {
            var worksheetNames = new List<string>(workbook.Worksheets.Count);
            foreach (var worksheet in workbook.Worksheets.Items<Worksheet>())
            {
                worksheetNames.Add(worksheet.Name);
            }
            string name;
            int num = 1;
            do
            {
                name = HiddenWorksheetPrefix + num;
                num++;
            } while (worksheetNames.Contains(name));
            return name;
        }
    }
}
