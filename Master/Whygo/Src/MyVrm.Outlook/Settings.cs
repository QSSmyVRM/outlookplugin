﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;
using MyVrm.Outlook.Configuration;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook
{
    public class Settings : ApplicationSettingsBase
    {
        private static readonly Settings defaultInstance = ((Settings) (Synchronized(new Settings())));
        static readonly byte[] SAditionalEntropy = {  0x9b, 0xce, 0x54, 0x71, 0x34, 0xf2, 0xdf, 0xb0  };

        public static Settings Default
        {
            get { return defaultInstance; }
        }

        [UserScopedSetting]
        [SpecialSetting(SpecialSetting.WebServiceUrl)]
        public string MyVrmServiceUrl
        {
            get { return ((string) (this["MyVrmServiceUrl"])); }
            set { this["MyVrmServiceUrl"] = value; }
        }

        [UserScopedSetting]
        [DefaultSettingValue("")]
        public string UserName
        {
            get { return ((string) (this["UserName"])); }
            set { this["UserName"] = value; }
        }

        [UserScopedSetting]
        [DefaultSettingValue("")]
        [EncryptedSetting]
        public string UserPassword
        {
            get
            {
                if (string.IsNullOrEmpty((string)this["UserPassword"]))
                {
                    return (string)this["UserPassword"];
                }
                var bytes = Convert.FromBase64String((string) this["UserPassword"]);
                byte[] decryptedData = ProtectedData.Unprotect(bytes, SAditionalEntropy,
                                                               DataProtectionScope.CurrentUser);
                return UnicodeEncoding.Unicode.GetString(decryptedData);
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    this["UserPassword"] = value;
                }
                else
                {
                    byte[] dataToEncrypt = UnicodeEncoding.Unicode.GetBytes(value);
                    byte[] bytes = ProtectedData.Protect(dataToEncrypt, SAditionalEntropy, DataProtectionScope.CurrentUser);
                    this["UserPassword"] = Convert.ToBase64String(bytes);
                }
            }
        }

        [UserScopedSetting]
        [DefaultSettingValue("")]
        public string UserDomain
        {
            get { return ((string) (this["UserDomain"])); }
            set { this["UserDomain"] = value; }
        }

        [UserScopedSetting]
        [DefaultSettingValue("True")]
        public bool UseDefaultCredentials
        {
            get { return ((bool) (this["UseDefaultCredentials"])); }
            set { this["UseDefaultCredentials"] = value; }
        }

        [UserScopedSetting]
        [DefaultSettingValue("Windows")]
        public AuthenticationMode AuthenticationMode
        {
            get { return ((AuthenticationMode) (this["AuthenticationMode"])); }
            set { this["AuthenticationMode"] = value; }
        }

        [UserScopedSetting]
        [DefaultSettingValue("False")]
        public bool IsNewSettings
        {
            get { return ((bool)(this["IsNewSettings"])); }
            set { this["IsNewSettings"] = value; }
        }

        public void UpdateProvider(SettingsProvider provider)
        {
            foreach (SettingsProperty property in Properties)
            {
                property.Provider = provider;
            }
            Providers.Clear();
            Providers.Add(provider);
        }
    }
}