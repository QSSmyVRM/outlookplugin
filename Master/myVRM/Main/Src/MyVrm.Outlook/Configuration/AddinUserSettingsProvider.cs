﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace MyVrm.Outlook.Configuration
{
    public class AddinUserSettingsProvider : SettingsProvider
    {
        private Hashtable _settingsStore = new Hashtable();
        private readonly string _configPath;

        public AddinUserSettingsProvider(string configPath)
        {
            _configPath = configPath;
        }
        #region Overrides of SettingsProvider

        public override SettingsPropertyValueCollection GetPropertyValues(SettingsContext context, SettingsPropertyCollection collection)
        {
            if (File.Exists(_configPath))
            {
                var memoryStream = new FileStream(_configPath, FileMode.Open);
                try
                {
                    _settingsStore = (Hashtable)new BinaryFormatter().Deserialize(memoryStream);
                }
                finally
                {
                    memoryStream.Close();
                }
            }
            var values = new SettingsPropertyValueCollection();
            var key = (string)context["GroupName"];
            var dict = (IDictionary)_settingsStore[key];
            foreach (SettingsProperty property in collection)
            {
                var propertyName = property.Name;
                var propertyValue = new SettingsPropertyValue(property);
                if (dict != null && dict.Contains(propertyName))
                {
                    try
                    {
                        propertyValue.PropertyValue = Convert.ChangeType(dict[propertyName], property.PropertyType);
                    }
                    catch (InvalidCastException)
                    {
                        propertyValue.PropertyValue = propertyValue.Property.DefaultValue;
                    }
                }
                else if (!string.IsNullOrEmpty((string)property.DefaultValue))
                {
                    if (typeof(Enum).IsAssignableFrom(property.PropertyType))
                    {
                        propertyValue.PropertyValue = Enum.Parse(property.PropertyType, (string)property.DefaultValue, true);
                    }
                    else
                    {
                        propertyValue.SerializedValue = property.DefaultValue;
                    }
                }
                else
                {
                    propertyValue.PropertyValue = null;
                }
                values.Add(propertyValue);
            }
            return values;
        }

        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
        {
            if (string.IsNullOrEmpty(name))
            {
                name = "AddinUserSettingsProvider";
            }
            base.Initialize(name, config);
        }

        public override void SetPropertyValues(SettingsContext context, SettingsPropertyValueCollection collection)
        {
            IDictionary dict = new Hashtable();
            foreach (SettingsPropertyValue value in collection)
            {
                dict.Add(value.Name, value.PropertyValue);
            }
            var key = (string)context["GroupName"];
            _settingsStore[key] = dict;
            var memoryStream = new FileStream(_configPath, FileMode.OpenOrCreate);
            try
            {
                new BinaryFormatter().Serialize(memoryStream, _settingsStore);
            }
            finally
            {
                memoryStream.Close();
            }
        }

        public override string ApplicationName { get; set; }

        #endregion
    }
}