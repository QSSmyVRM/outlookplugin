﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace MyVrm.Outlook
{
    public class CustomFormReadOnlyCollection : ReadOnlyCollection<CustomFormControl>
    {
        internal class CustomFormList : IList<CustomFormControl>
        {
            public CustomFormList()
            {
            }

            public CustomFormList(IList<CustomFormControl> list)
            {
                InnerList = list;
            }
            public IList<CustomFormControl> InnerList { get; set; }

            public IEnumerator<CustomFormControl> GetEnumerator()
            {
                return InnerList.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            public void Add(CustomFormControl item)
            {
                InnerList.Add(item);
            }

            public void Clear()
            {
                InnerList.Clear();
            }

            public bool Contains(CustomFormControl item)
            {
                return InnerList.Contains(item);
            }

            public void CopyTo(CustomFormControl[] array, int arrayIndex)
            {
                InnerList.CopyTo(array, arrayIndex);
            }

            public bool Remove(CustomFormControl item)
            {
                return InnerList.Remove(item);
            }

            public int Count
            {
                get { return InnerList.Count; }
            }

            public bool IsReadOnly
            {
                get { return InnerList.IsReadOnly; }
            }

            public int IndexOf(CustomFormControl item)
            {
                return InnerList.IndexOf(item);
            }

            public void Insert(int index, CustomFormControl item)
            {
                InnerList.Insert(index, item);
            }

            public void RemoveAt(int index)
            {
                InnerList.RemoveAt(index);
            }

            public CustomFormControl this[int index]
            {
                get { return InnerList[index]; }
                set { InnerList[index] = value; }
            }
        }
        public CustomFormReadOnlyCollection() : base(new CustomFormList(new CustomFormControl[]{}))
        {
        }
        public CustomFormReadOnlyCollection( IList<CustomFormControl> list)
            : base(new CustomFormList(list))
        {
        }

        public void InitInnerList(IList<CustomFormControl> list)
        {
            var items = (CustomFormList)Items;
            items.InnerList = list;
        }

        protected TCustomFormControl FindFirst<TCustomFormControl>() where TCustomFormControl : CustomFormControl
        {
            return Items.OfType<TCustomFormControl>().FirstOrDefault();
        }
    }
}
