﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Forms;

namespace MyVrm.WebServices.Data.TrueDaybook
{
	public class TrueDaybookDefaults
	{
		public static string ConstDefaultLocation = Strings.DefaultLocation;
		public string Location = Strings.DefaultLocation; // "[virtual room]";

		private string _body;
		public string Body 
		{
			get { return _body; }
		}
		private string _subject;
		public string Subject
		{
			get { return _subject; }
		}
		private TxtTypeEnum _fmt;
		public TxtTypeEnum Fmt
		{
			get { return _fmt; }
		}

		public enum TxtTypeEnum
		{
			Txt,
			Rtf
		}

		object GetDataFormats(TxtTypeEnum fmt)
		{
			switch (fmt)
			{
				default:
				case TxtTypeEnum.Txt:
					return DataFormats.Text;
				case TxtTypeEnum.Rtf:
					return DataFormats.Rtf;
			}
		}

		public TrueDaybookDefaults(string subject, string body, TxtTypeEnum fmt)
		{
			_fmt = fmt;
			DataObject dataObject = new DataObject(body, GetDataFormats(fmt));
			_subject = subject;
			_body = body;
		}
	}
}
