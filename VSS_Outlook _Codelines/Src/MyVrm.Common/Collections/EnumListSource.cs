/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections;
using System.ComponentModel;

namespace MyVrm.Common.Collections
{
    /// <summary>
    /// Represents a list of <see cref="LocalizedEnum"/>. 
    /// </summary>
    public class EnumListSource : Component, IListSource
    {
        private Type _enumType;

        /// <summary>
        /// Initialize a new instance of the <see cref="EnumListSource"/>
        /// </summary>
        public EnumListSource()
        {
        }
        /// <summary>
        /// Initialize a new instance of the <see cref="EnumListSource"/> that contains elements copied from the specified enumeration.
        /// </summary>
        /// <param name="enumType">The enumeration whose values are copied the the new list.</param>
        public EnumListSource(Type enumType)
        {
            _enumType = enumType;
        }

        #region IListSource Members

        /// <summary>
        /// Returns list of <see cref="LocalizedEnum"/>.
        /// </summary>
        /// <returns>List of <see cref="LocalizedEnum"/>.</returns>
        public IList GetList()
        {
            Array enumValues = GetValues();
            var list = new ArrayList(enumValues.Length);
            for (int i = 0; i < enumValues.Length; i++)
            {
                var enumValue = (Enum) enumValues.GetValue(i);
                string description = LocalizedDescriptionAttribute.FromEnum(_enumType, enumValue);
                list.Add(new LocalizedEnum(description, enumValue));
            }
            return list;
        }

        /// <summary>
        /// Gets a value indicating whether the collection is a collection of <see cref="IList"/> objects.
        /// </summary>
        public bool ContainsListCollection
        {
            get { return false; }
        }

        /// <summary>
        /// Gets or sets type of enum.
        /// </summary>
        [DefaultValue((string) null)]
        public Type EnumType
        {
            get { return _enumType; }
            set { _enumType = value; }
        }

        #endregion
        /// <summary>
        /// Default text for display member.
        /// </summary>
        public const string DisplayMemberColumnName = "Text";
        /// <summary>
        /// Default text for value member.
        /// </summary>
        public const string ValueMemberColumnName = "Value";
        /// <summary>
        /// Returns array of enumeration members
        /// </summary>
        /// <returns>Array of enumeration members</returns>
        protected virtual Array GetValues()
        {
            return Enum.GetValues(EnumType);
        }
    }
}