﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Utils;
using MyVrm.Common;
using MyVrm.Common.ComponentModel;
using MyVrm.WebServices.Data;
using MyVrm.WebServices.Data.Files;
using Exception = System.Exception;

namespace MyVrm.Outlook.WinForms.Conference
{
    public partial class FilesPage : ConferencePage
    {
        private class FileRecord
        {
            public string Name { get; set; }
            public string LocalPath { get; set; }
            public string RemoteName { get; set; }
            public bool IsUploaded { get; set; }
        }

        readonly DataList<FileRecord> _fileRecords = new DataList<FileRecord>();

        public FilesPage()
        {
            InitializeComponent();
            Text = Strings.FilesPageText;
            addFileBarButton.Caption = Strings.FileAddText;
            uploadBarButton.Caption = Strings.FileUploadText;
            fileNameColumn.Caption = Strings.FileNameColumnCaption;
            uploadedColumn.Caption = Strings.IsUploadedColumnCaption;
            uploadedTextEdit.DisplayFormat.FormatType = FormatType.Custom;
            uploadedTextEdit.DisplayFormat.FormatString = "yn";
            uploadedTextEdit.DisplayFormat.Format = new BooleanCustomFormatter();
			ConferenceBindingSource.DataSourceChanged += ConferenceBindingSource_DataSourceChanged;
			filesList.DataSource = _fileRecords;
        }

        protected override void OnReadOnlyChanged(ReadOnlyChangedEventArgs e)
        {
            base.OnReadOnlyChanged(e);
            bar1.Visible = !e.ReadOnly;
            filesList.Enabled = !e.ReadOnly;
        }

		void ConferenceBindingSource_DataSourceChanged(object sender, EventArgs e)
		{
			if (Conference != null)
				Reset();
		}

    	public void Reset()
		{
			_fileRecords.Clear();
			_fileRecords.AddRange(
			   Conference.Conference.Files.Select(
				   file => new FileRecord { Name = Path.GetFileName(file), RemoteName = file, IsUploaded = true }));

    		FileRecord emptyfile = null;
			do
			{
				emptyfile = _fileRecords.FirstOrDefault(file => file.Name.Length <= 0);
				if (emptyfile != null)
					_fileRecords.Remove(emptyfile);
    		} while (emptyfile != null);
    		
		}

    	protected override void OnApplying(System.ComponentModel.CancelEventArgs e)
        {
            base.OnApplying(e);
            var newFilesCount = _fileRecords.Where(rec => !rec.IsUploaded).Count();
            if (newFilesCount > 0)
            {
                var dlgResult = ShowMessage(string.Format(Strings.FilesNeedToBeUploaded, newFilesCount),
                                            MessageBoxButtons.YesNoCancel);
                e.Cancel = dlgResult == DialogResult.Cancel;
                if (e.Cancel)
                    return;
                if (dlgResult == DialogResult.Yes)
                    UploadFiles();
            }
			if (Conference != null && Conference.Conference != null && Conference.Conference.Files != null)
			{
				Conference.Conference.Files.Clear();
				foreach (var fileRecord in _fileRecords.Where(rec => rec.IsUploaded))
				{
					Conference.Conference.Files.Add(fileRecord.RemoteName);
				}
			}
        }

        private void addFileBarButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
			//if (_fileRecords.Count >= 3)
			//{
			//    UIHelper.ShowError(Strings.ReachedMaxNumberOfFiles);
			//    return;
			//}
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                _fileRecords.AddRange(
                    openFileDialog.FileNames.Select(
                        file => new FileRecord {Name = Path.GetFileName(file), LocalPath = file, IsUploaded = false}));
                if (_fileRecords.RaiseListChangedEvents)
                {
                    if (Conference != null)
                        Conference.Appointment.SetNonOutlookProperty(true);
                }
            }
        }

        private void FilesPage_Load(object sender, System.EventArgs e)
        {
			Reset();
//            filesList.DataSource = _fileRecords;
			//_fileRecords.AddRange(
			//    Conference.Conference.Files.Select(
			//        file => new FileRecord {Name = Path.GetFileName(file), RemoteName = file, IsUploaded = true}));
        }

        private void uploadFileBarButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            UploadFiles();
        }

        private void UploadFiles()
        {
            var cursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            try
            {
				if(_fileRecords.Where(rec => !rec.IsUploaded).Count() > 0 && Conference != null)
					Conference.Appointment.SetNonOutlookProperty(true);

                foreach (var fileRecord in _fileRecords.Where(rec => !rec.IsUploaded))
                {
                    try
                    {
                        byte[] data;
                        using (var fileStm = File.OpenRead(fileRecord.LocalPath))
                        {
                            data = new byte[fileStm.Length];
                            fileStm.Read(data, 0, data.Length);
                        }
                        var fileInfo = new UploadFileInfo(fileRecord.Name, data);
                        MyVrmService.Service.UploadFile(fileInfo);
                        fileRecord.IsUploaded = true;
                        fileRecord.RemoteName = fileInfo.RemotePath;
                    }
                    catch (Exception exception)
                    {
                        MyVrmAddin.TraceSource.TraceException(exception);
                        ShowError(exception.Message);
                    }
                }
                _fileRecords.ResetBindings();
            }
            finally
            {
                Cursor.Current = cursor;
            }
        }
    }
}
