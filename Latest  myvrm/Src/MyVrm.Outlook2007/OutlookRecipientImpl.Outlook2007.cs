﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Outlook;
using Microsoft.Office.Interop.Outlook.Extensions;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook
{
    internal partial class OutlookRecipientImpl
    {
        public override string SmtpAddress
        {
            get
            {
                const string PR_SMTP_ADDRESS = "http://schemas.microsoft.com/mapi/proptag/0x39FE001E";
                if (_smtpAddress == null)
                {
                    if (!_recipient.Resolved)
                    {
                        return null;
                    }
                    var propertyAccessor = _recipient.PropertyAccessor;
                    try
                    {
                        _smtpAddress = propertyAccessor.GetProperty(PR_SMTP_ADDRESS).ToString();
                    }
                    catch
                    {

                        return null;
                    }
                }
                return _smtpAddress;
            }
        }
        protected override List<FreeBusyInfo> GetFreeBusyFromDefaultCalendarFolder(DateTime start, DateTime end)
        {
            var freeBusyInfos = new List<FreeBusyInfo>();
            if (_calendarFolder == null)
            {
                if (_recipient.Resolved)
                {
                    MyVrmAddin.TraceSource.TraceInformation("GetFreeBusyFromDefaultCalendarFolder: GetNamespace");
                    var nameSpace = _recipient.Application.GetNamespace("MAPI");
                    try
                    {
                        if (_recipient.Application.Session.CurrentUser.EntryID == _recipient.EntryID)
                        {
                            MyVrmAddin.TraceSource.TraceInformation("GetFreeBusyFromDefaultCalendarFolder: GetDefaultFolder");
                            _calendarFolder =
                                nameSpace.GetDefaultFolder(
                                    OlDefaultFolders.olFolderCalendar);
                        }
                        else
                        {
                            var addressEntry = _recipient.AddressEntry;
                            if (addressEntry.AddressEntryUserType == OlAddressEntryUserType.olExchangeUserAddressEntry)
                            {
                                MyVrmAddin.TraceSource.TraceInformation("GetFreeBusyFromDefaultCalendarFolder: GetSharedDefaultFolder");
                                _calendarFolder =
                                    nameSpace.GetSharedDefaultFolder(_recipient,
                                                                     OlDefaultFolders.
                                                                         olFolderCalendar);
                            }
                        }
                    }
                    finally
                    {
                       // Marshal.ReleaseComObject(nameSpace);
                        Marshal.FinalReleaseComObject(nameSpace);
                        GC.Collect();
                        GC.WaitForPendingFinalizers();
                        GC.Collect();
                        MyVrmAddin.TraceSource.TraceInformation("Namespace getting released");

                    }
                }
            }
            if (_calendarFolder != null)
            {
                MyVrmAddin.TraceSource.TraceInformation("GetFreeBusyFromDefaultCalendarFolder: foreach AppointmentItems");

                var calendarFolder = ((Folder)_calendarFolder);
                var appointments = from item in calendarFolder.Items.AsQueryable<ExtendedAppointment>()
                                   where item.Start >= start && item.End <= end
                                   select item;
                //MyVrmAddin.TraceSource.TraceInformation("\n Outlook Recipents Appointment count = " + appointments.Count());

                foreach (var appointment in appointments)
                {
                    MyVrmAddin.TraceSource.TraceInformation("\n Outlook Recipents .Check -1.....");
                 //   if(appointment.Item.i)
                    using (var outlookAppointmentImpl = new OutlookAppointmentImpl(appointment.Item))
                    {
                        MyVrmAddin.TraceSource.TraceInformation("\n Outlook Recipents .Check -12.....");

                        if (outlookAppointmentImpl.IsRecurring)
                        {
                          //  if(outlookAp)
                            MyVrmAddin.TraceSource.TraceInformation("\n Outlook Recipents .Check -123.....");
                            if (appointment.Item.GlobalAppointmentID=="040000008200E00074C5B7101A82E00800000000C0B30EB7E70BD10100000000000000001000000055DD10DD1642D04F94D5D3FE52B153E4")
                            {
                                MyVrmAddin.TraceSource.TraceInformation("\n Outlook Recipents .Check -12345............"+outlookAppointmentImpl.Subject);
                                MyVrmAddin.TraceSource.TraceInformation("\n Outlook Recipents .Check -12345 appointment start............" + outlookAppointmentImpl.Start);
                                MyVrmAddin.TraceSource.TraceInformation("\n Outlook Recipents .Check -12345 appointment end............" + outlookAppointmentImpl.End);
                              //  MyVrmAddin.TraceSource.TraceInformation("\n Outlook Recipents .Check -12345 appointment end............" + outlookAppointmentImpl.);

                                MyVrmAddin.TraceSource.TraceInformation("\n Outlook Recipents .Check -12345 condition end............" + end);
                                MyVrmAddin.TraceSource.TraceInformation("\n Outlook Recipents .Check -12345 condition start............" + start);

                            }
                            var recurrencePattern =
                                outlookAppointmentImpl.GetRecurrencePattern(outlookAppointmentImpl.StartTimeZone);
                            MyVrmAddin.TraceSource.TraceInformation("\n Outlook Recipents .Check -12345678 into 4............");
                            if (appointment.Item.GlobalAppointmentID != "040000008200E00074C5B7101A82E00800000000C0B30EB7E70BD10100000000000000001000000055DD10DD1642D04F94D5D3FE52B153E4")
                            {
                            IEnumerable<OccurrenceInfo> occurrenceInfos =
                                recurrencePattern.GetOccurrenceInfoList(start, end);
                            MyVrmAddin.TraceSource.TraceInformation("\n Outlook Recipents .Check -1234567 into 3............");
                            if (occurrenceInfos.Count()>0)
                            {
                                MyVrmAddin.TraceSource.TraceInformation("\n Outlook Recipents .Check -1234567 into 5............");
                           
                            foreach (var occurinf in occurrenceInfos)
                            {
                                MyVrmAddin.TraceSource.TraceInformation("\n Outlook Recipents .Check -123456............" + occurinf.Start+"....End...."+ occurinf.End);

                            }
                              }
                            freeBusyInfos.AddRange(
                                occurrenceInfos.Select(
                                    occurrenceInfo =>
                                    new FreeBusyInfo(outlookAppointmentImpl.BusyStatus, occurrenceInfo.Start,
                                                     occurrenceInfo.End, outlookAppointmentImpl.Subject)));
                            }
                        }
                        else
                        {
                            MyVrmAddin.TraceSource.TraceInformation("\n Outlook Recipents .Check -1234.....");
                            if (outlookAppointmentImpl.Start >= start && outlookAppointmentImpl.End <= end)
                            {
                                freeBusyInfos.Add(new FreeBusyInfo(outlookAppointmentImpl.BusyStatus,
                                                                   outlookAppointmentImpl.Start,
                                                                   outlookAppointmentImpl.End,
                                                                   outlookAppointmentImpl.Subject));
                            }
                        }
                    }
                }
            }
            return freeBusyInfos;
        }
    }
}