﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System.IO;
using System.Xml;

namespace MyVrm.Common.Xml
{
    public static class XmlUtility
    {
        /// <summary>
        /// Reads the current element and returns the element contents as byte[] object.
        /// </summary>
        /// <param name="reader">The xml reader.</param>
        /// <param name="isBase64">true if the current element contains Base64 content.</param>
        /// <returns>Array of bytes that represents an image.</returns>
        public static byte[] ReadElementContentAsByteArray(this XmlReader reader, bool isBase64)
        {
            var buffer = new byte[0x400];
            var stream = new MemoryStream();
            using (var writer = new BinaryWriter(stream))
            {
                int readBytes;
                if (isBase64)
                {
                    while ((readBytes = reader.ReadElementContentAsBase64(buffer, 0,
                                                                          buffer.Length)) > 0)
                    {
                        writer.Write(buffer, 0, readBytes);
                    }
                }
                else
                {
                    while ((readBytes = reader.ReadElementContentAsBinHex(buffer, 0,
                                                                          buffer.Length)) > 0)
                    {
                        writer.Write(buffer, 0, readBytes);
                    }
                }
                return stream.ToArray();
            }
        }
    }
}
