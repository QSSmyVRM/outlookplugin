﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Maps
{
    public abstract class MapsUri
    {
        public string Location { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}
