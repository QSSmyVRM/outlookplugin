﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;

namespace MyVrm.WebServices.Data
{
    public sealed class WorkOrderItemCollection : ComplexPropertyCollection<WorkOrderItem>
    {
		public override object Clone()
		{
			WorkOrderItemCollection copy = new WorkOrderItemCollection();
			foreach (var item in this)
			{
				copy.Add((WorkOrderItem)item.Clone()); //???????
			}

			return copy;
		}

        internal WorkOrderItemCollection()
        {
        }

        public void Add(WorkOrderItem workOrderItem)
        {
            InternalAdd(workOrderItem);
        }

        public void AddRange(IEnumerable<WorkOrderItem> workOrderItems)
        {
            InternalAddRange(workOrderItems);
        }

        public void Remove(WorkOrderItem workOrderItem)
        {
            InternalRemove(workOrderItem);
        }

        public void Clear()
        {
            InternalClear();
        }
        #region Overrides of ComplexPropertyCollection<WorkOrderItem>

        internal override WorkOrderItem CreateComplexProperty(string xmlElementName)
        {
            return new WorkOrderItem();
        }

        internal override string GetCollectionItemXmlElementName(WorkOrderItem complexProperty)
        {
            return "Item";
        }

        #endregion
    }
}
