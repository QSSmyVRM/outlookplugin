﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows.Forms;
using Microsoft.Office.Core;
using Microsoft.Office.Tools.Excel;
using MyVrm.Common.Diagnostics;
using MyVrm.DataImport.Excel;
using MyVrm.WebServices.Data;
using System.Diagnostics;

namespace MyVrmExcelImportTemplate
{
    public partial class ThisWorkbook
    {
        private const string ServiceUrlDocumentPropertyName = "MyVrm_ServiceUrl";
        private const string ServiceAuthModeDocumentPropertyName = "MyVrm_ServiceAuthMode";
        private const string ServiceUserDocumentPropertyName = "MyVrm_ServiceUser";
        private const string ServiceUserDomainDocumentPropertyName = "MyVrm_ServiceUserDomain";
        private const string OrganizationIdDocumentPropertyName = "MyVRM_OrganizationId";
        private MyVrmService _service;
        private MyVrmTraceSource _traceSource;
        private ValidationListManager _validationListManager;
        private ReadOnlyCollection<Organization> _organizations;
        private Dictionary<string, ExcelTable> _listObjectToTable;
        private ExcelTable _selectedTable;

        public MyVrmService Service
        {
            get { return _service; }
        }

        public ReadOnlyCollection<Organization> Organizations
        {
            get
            {
                if (_organizations == null)
                {
                    _organizations = Service.GetOrganizationList();
                }
                return _organizations;
            }
        }

        public ValidationListManager ValidationListManager
        {
            get { return _validationListManager; }
        }

        internal ExcelTable SelectedTable
        {
            get { return _selectedTable; }
        }

        internal void RegisterTable(ListObject listObject, ExcelTable table)
        {
            _listObjectToTable.Add(listObject.Name, table);
            listObject.Selected += listObject_Selected;
            listObject.Deselected += listObject_Deselected;
        }

        internal string SavedServiceUrl
        {
            get
            {
                return GetDocumentProperty(ServiceUrlDocumentPropertyName);
            }
            set
            {
                DocumentProperties properties = (DocumentProperties)CustomDocumentProperties; 
                if (GetDocumentProperty(ServiceUrlDocumentPropertyName) != null)
                {
                    properties[ServiceUrlDocumentPropertyName].Delete();
                }
                properties.Add(ServiceUrlDocumentPropertyName, false, MsoDocProperties.msoPropertyTypeString, value, missing);
            }
        }

        internal AuthenticationMode SavedServiceAuthMode
        {
            get
            {
                var documentProperty = GetDocumentProperty(ServiceAuthModeDocumentPropertyName);
                return string.IsNullOrEmpty(documentProperty)
                           ? AuthenticationMode.Custom
                           : (AuthenticationMode) Enum.Parse(typeof (AuthenticationMode), documentProperty);
            }
            set
            {
                DocumentProperties properties = (DocumentProperties)CustomDocumentProperties;
                if (GetDocumentProperty(ServiceAuthModeDocumentPropertyName) != null)
                {
                    properties[ServiceAuthModeDocumentPropertyName].Delete();
                }
                properties.Add(ServiceAuthModeDocumentPropertyName, false, MsoDocProperties.msoPropertyTypeString, value, missing);
            }
        }

        internal string SavedServiceUserName
        {
            get
            {
                return GetDocumentProperty(ServiceUserDocumentPropertyName);
            }
            set
            {
                DocumentProperties properties = (DocumentProperties)CustomDocumentProperties;
                if (GetDocumentProperty(ServiceUserDocumentPropertyName) != null)
                {
                    properties[ServiceUserDocumentPropertyName].Delete();
                }
                properties.Add(ServiceUserDocumentPropertyName, false, MsoDocProperties.msoPropertyTypeString, value, missing);
            }
        }

        internal string SavedServiceUserDomain
        {
            get
            {
                return GetDocumentProperty(ServiceUserDomainDocumentPropertyName);
            }
            set
            {
                DocumentProperties properties = (DocumentProperties)CustomDocumentProperties;
                if (GetDocumentProperty(ServiceUserDomainDocumentPropertyName) != null)
                {
                    properties[ServiceUserDomainDocumentPropertyName].Delete();
                }
                properties.Add(ServiceUserDomainDocumentPropertyName, false, MsoDocProperties.msoPropertyTypeString, value, missing);
            }
        }

        internal string SavedOrganizationId
        {
            get
            {
                return GetDocumentProperty(OrganizationIdDocumentPropertyName);
            }
            set
            {
                DocumentProperties properties = (DocumentProperties)CustomDocumentProperties;
                if (GetDocumentProperty(OrganizationIdDocumentPropertyName) != null)
                {
                    properties[OrganizationIdDocumentPropertyName].Delete();
                }
                properties.Add(OrganizationIdDocumentPropertyName, false, MsoDocProperties.msoPropertyTypeString, value, missing);
            }
        }

        void listObject_Deselected(Microsoft.Office.Interop.Excel.Range Target)
        {
            Globals.Ribbons.MyVrmRibbon.publishButton.Enabled = false;
            _selectedTable = null;
        }

        void listObject_Selected(Microsoft.Office.Interop.Excel.Range Target)
        {
            var listObject = Target.ListObject;
            if (_listObjectToTable.ContainsKey(listObject.Name))
            {
                _selectedTable = _listObjectToTable[listObject.Name];
                Globals.Ribbons.MyVrmRibbon.publishButton.Enabled = true;
            }
        }

        private void ThisWorkbook_Open()
        {
            Globals.GeneralSheet.Activate();
        }

        private void ThisWorkbook_Startup(object sender, System.EventArgs e)
        {
            _traceSource = new MyVrmTraceSource("myVRM");
            MyVrmService.TraceSource = _traceSource;
            _service = MyVrmService.Service;

            ShowConnectionDialog();

            var savedOrganizationId = SavedOrganizationId;
            if (!string.IsNullOrEmpty(savedOrganizationId))
            {
                Service.SwitchOrganization(new OrganizationId(savedOrganizationId));
            }
            else
            {
                using(var dialog = new OrganizationSelectionDialog())
                {
                    if (dialog.ShowDialog(null) == DialogResult.OK)
                    {
                        Service.SwitchOrganization(dialog.SelectedOrganization);
                        SavedOrganizationId = Service.OrganizationId.Id;
                    }
                    else
                    {
                        Close(false, missing, missing);
                    }
                }
            }
            _validationListManager = new ValidationListManager(this.InnerObject, Service);

            _listObjectToTable = new Dictionary<string, ExcelTable>();

        }

        internal void ShowConnectionDialog()
        {
            using(ConnectionDialog dialog = new ConnectionDialog())
            {
                dialog.Url = SavedServiceUrl;
                dialog.UseDefaultCredentials = (dialog.AuthenticationMode == AuthenticationMode.Windows) &&
                                               (string.IsNullOrEmpty(dialog.User) && string.IsNullOrEmpty(dialog.Domain));
                dialog.AuthenticationMode = SavedServiceAuthMode;
                dialog.User = SavedServiceUserName;
                dialog.Domain = SavedServiceUserDomain;
                if (dialog.ShowDialog(null) == DialogResult.OK)
                {
                    Service.Url = dialog.Url;
                    Service.AuthenticationMode = dialog.AuthenticationMode;

                    if(dialog.AuthenticationMode == AuthenticationMode.Windows)
                    {
                        if (!dialog.UseDefaultCredentials)
                        {
                            Service.Credential = new NetworkCredential(dialog.User, dialog.Password, dialog.Domain);
                        }
                        else
                        {
                            Service.UseDefaultCredential = true;
                        }
                    }
                    else
                    {
                        Service.Credential = new NetworkCredential(dialog.User, dialog.Password);
                    }
                }
                else
                {
                    Close(false, missing, missing);
                    return;
                }
            }
            SavedServiceUrl = Service.Url;
            SavedServiceAuthMode = Service.AuthenticationMode;
            if (!Service.UseDefaultCredential)
            {
                SavedServiceUserName = Service.Credential.UserName;
                SavedServiceUserDomain = Service.Credential.Domain;
            }
        }

        private void ThisWorkbook_Shutdown(object sender, System.EventArgs e)
        {
        }

        #region VSTO Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
	    private void InternalStartup()
        {
            this.Open += new Microsoft.Office.Interop.Excel.WorkbookEvents_OpenEventHandler(this.ThisWorkbook_Open);
            this.Shutdown += new System.EventHandler(this.ThisWorkbook_Shutdown);
            this.Startup += new System.EventHandler(this.ThisWorkbook_Startup);
            this.BeforeSave += new Microsoft.Office.Interop.Excel.WorkbookEvents_BeforeSaveEventHandler(this.ThisWorkbook_BeforeSave);

        }
        
        #endregion

        private void ThisWorkbook_BeforeSave(bool SaveAsUI, ref bool Cancel)
        {
        }

        private string GetDocumentProperty(string propertyName)
        {
            DocumentProperties properties;
            properties = (DocumentProperties)CustomDocumentProperties;

            foreach (DocumentProperty prop in properties)
            {
                if (prop.Name == propertyName)
                {
                    return prop.Value.ToString();
                }
            }
            return null;
        }
    }
}
