﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	internal class TDGetDefaultSettingsRequest : ServiceRequestBase<TDGetDefaultSettingsResponse>
	{
		internal TDGetDefaultSettingsRequest(MyVrmService service)
			: base(service)
		{
		}

		#region Overrides of ServiceRequestBase

		internal override string GetXmlElementName()
		{
			return "TDGetDefaultSettings";
		}

		internal override string GetCommandName()
		{
			return Constants.TDGetDefaultSettingsCommandName;
		}

		internal override string GetResponseXmlElementName()
		{
			return "TDGetDefaultSettings";
		}

		internal override TDGetDefaultSettingsResponse ParseResponse(MyVrmServiceXmlReader reader)
		{
			var response = new TDGetDefaultSettingsResponse(); 
			response.LoadFromXml(reader, GetResponseXmlElementName());
			return response;
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			UserId.WriteToXml(writer, "userID");
		}
		#endregion
	}

	public class TDGetDefaultSettingsResponse : ServiceResponse
	{
		public int MaxSeats { get; internal set; }
		public string DefaultSubject { set; get; }
		public string DefaultBody { set; get; }
		public string RetMessage { set; get; }
        public int RetCode { set; get; }
		public long SettingsVersion { set; get; }
		public XMLError XMLError { set; get; }

		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{
			while (!reader.IsEndElement(XmlNamespace.NotSpecified, "retState"))
			{
				try
				{
					switch (reader.LocalName)
					{
						case "maximumAvailableSeats":
							MaxSeats = reader.ReadElementValue<int>();
							break;
						case "defaultAppointmentSubject":
							DefaultSubject = reader.ReadElementValue();
							break;
						case "defaultAppointmentInvitation":
							DefaultBody = reader.ReadElementValue();
							break;
						case "retCode":
							RetCode = reader.ReadElementValue<int>();
							break;
						case "retMessage":
							RetMessage = reader.ReadElementValue();
							break;
						case "settingsVersion":
							SettingsVersion = reader.ReadElementValue<long>();
							break;
						case "error":
							XMLError = new XMLError();
							XMLError.LoadFromXml(reader, XmlNamespace.NotSpecified, "error");
							break;
					}
					reader.Read();
				}
				catch (Exception ex)
				{
					MyVrmService.TraceSource.TraceError(reader.LocalName + ": " + ex.Message, null);
					reader.Read();
					//throw;
				}
			}
		}
	}
}