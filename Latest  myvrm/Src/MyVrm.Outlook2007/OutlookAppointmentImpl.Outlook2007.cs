﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using TimeZone = Microsoft.Office.Interop.Outlook.TimeZone;

namespace MyVrm.Outlook
{
    public partial class OutlookAppointmentImpl
    {
        public override TimeZoneInfo StartTimeZone
        {
            get
            {
                return TimeZoneInfo.FindSystemTimeZoneById(Item.StartTimeZone.ID);
            }
            set
            {
                foreach (TimeZone timeZone in Item.Application.TimeZones)
                {
                    if (timeZone.ID.Equals(value.Id, StringComparison.InvariantCultureIgnoreCase))
                    {
                        Item.StartTimeZone = timeZone;
                        NotifyPropertyChanged("StartTimeZone");
                        break;
                    }
                }
            }
        }

        public override TimeZoneInfo EndTimeZone
        {
            get
            {
                return TimeZoneInfo.FindSystemTimeZoneById(Item.EndTimeZone.ID);
            }
            set
            {
                foreach (TimeZone timeZone in Item.Application.TimeZones)
                {
                    if (timeZone.ID.Equals(value.Id, StringComparison.InvariantCultureIgnoreCase))
                    {
                        Item.EndTimeZone = timeZone;
                        NotifyPropertyChanged("EndTimeZone");
                        break;
                    }
                }
            }
        }

        public override DateTime StartInStartTimeZone
        {
            get { return Item.StartInStartTimeZone; }
            set
            {
                if (Item.StartInStartTimeZone != value)
                {
                    Item.StartInStartTimeZone = value;
                }
            }
        }

        public override DateTime EndInEndTimeZone
        {
            get { return Item.EndInEndTimeZone; }
            set
            {
                if (Item.EndInEndTimeZone != value)
                {
                    Item.EndInEndTimeZone = value;
                }
            }
        }

        public override string GlobalAppointmentId
        {
            get { return Item.GlobalAppointmentID; }
        }

        public override void RemoveRecipient(string email)
        {
            var idx = 0;
            for (var i = 1; i <= Item.Recipients.Count; i++)
            {
                var recipient = Item.Recipients[i];
                var exchangeUser = recipient.AddressEntry.GetExchangeUser();
                var smtpAddress = exchangeUser != null ? exchangeUser.PrimarySmtpAddress : recipient.Address;
                if (email.Equals(smtpAddress, StringComparison.InvariantCultureIgnoreCase))
                {
                    idx = i;
                    break;
                }
            }
            if (idx > 0)
            {
                Item.Recipients.Remove(idx);
            }
        }

        partial void ItemPropertyChanged(string name)
        {
            if (name == "Resources")
            {
                NotifyPropertyChanged("Recipients");
            }
            else
            {
                NotifyPropertyChanged(name);
            }
        }

        partial void RemoveOneOff(bool removePropDef)
        {
            //Do nothing.
        }
    }
}