﻿using System;
using System.Runtime.InteropServices;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Outlook;
using MyVrm.Common;

namespace MyVrm.Outlook
{
    public class OfficeCommandBar : DisposableObject
    {
        public CommandBar CommandBar { get; protected set; }
        public string Name { get; set; }

        public event EventHandler Close;

        internal void Init(Inspector inspector)
        {
            Inspector = inspector;
            ((InspectorEvents_10_Event)Inspector).Close += InspectorClose;
            Initialize();
        }

        protected Inspector Inspector { get; private set; }

        protected virtual void Initialize()
        {
            CommandBar = Inspector.CommandBars.Add(Name, MsoBarPosition.msoBarTop, false, true);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && !IsDisposed)
            {
                OnClose();
                Marshal.ReleaseComObject(CommandBar);
                CommandBar = null;
            }
            GC.SuppressFinalize(this);
        }

        protected virtual void OnClose()
        {
            ThrowIfDisposed();
            if (Close != null)
            {
                Close(this, EventArgs.Empty);
            }
            CommandBar.Delete();
        }

        void InspectorClose()
        {
            ((InspectorEvents_10_Event)Inspector).Close -= InspectorClose;
            OnClose();
            Inspector = null;
        }

    }
}
