﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.Outlook.WinForms.Conference
{
	partial class PublicRoomList
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
			DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PublicRoomList));
			DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
			this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.publicRoomBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
			this.addButton = new DevExpress.XtraEditors.SimpleButton();
			this.checkEditAdvMode = new DevExpress.XtraEditors.CheckEdit();
			this.checkEditBaseMode = new DevExpress.XtraEditors.CheckEdit();
			this.roomsCalendarButton = new DevExpress.XtraEditors.SimpleButton();
			this.removeButton = new DevExpress.XtraEditors.SimpleButton();
			this.checkEditPublicOnly = new DevExpress.XtraEditors.CheckEdit();
			this.textEditSearchFor = new DevExpress.XtraEditors.TextEdit();
			this.checkEditGridMode = new DevExpress.XtraEditors.CheckEdit();
			this.roomListBoxControl = new DevExpress.XtraEditors.ListBoxControl();
			this.checkEditPublicAndPrivate = new DevExpress.XtraEditors.CheckEdit();
			this.gridControl_RoomsInfo = new DevExpress.XtraGrid.GridControl();
			this.layoutView2 = new DevExpress.XtraGrid.Views.Layout.LayoutView();
			this.layoutViewColumn1 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
			this.layoutViewField1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
			this.layoutViewColumn2 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
			this.layoutViewField2 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
			this.layoutViewColumn3 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
			this.repositoryItemPictureEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
			this.layoutViewField3 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
			this.layoutViewColumn4 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
			this.repositoryItemButtonEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
			this.layoutViewField4 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
			this.layoutViewColumn5 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
			this.layoutViewField5 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
			this.layoutViewColumn6 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
			this.layoutViewField6 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
			this.layoutViewColumn7 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
			this.layoutViewField7 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
			this.layoutViewColumn8 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
			this.repositoryAddRoom = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
			this.layoutViewField_layoutViewColumn8 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
			this.colIsFavorite = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
			this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
			this.layoutViewField_colIsFavorite = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
			this.colRoomType = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
			this.layoutViewField_colRoomType = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
			this.layoutViewCard1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewCard();
			this.item1 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.item3 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.item4 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.item6 = new DevExpress.XtraLayout.SimpleSeparator();
			this.item2 = new DevExpress.XtraLayout.SimpleSeparator();
			this.item5 = new DevExpress.XtraLayout.SimpleSeparator();
			this.item9 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
			this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
			this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.gridColumnShowDetails = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colRoomType1 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.colIsFavorite1 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.checkEditCardMode = new DevExpress.XtraEditors.CheckEdit();
			this.comboBoxSearchColumn = new DevExpress.XtraEditors.ComboBoxEdit();
			this.checkEditShowFavorites = new DevExpress.XtraEditors.CheckEdit();
			this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.roomListLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.calendarLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
			this.emptySpaceBelowAddRemoveBn = new DevExpress.XtraLayout.EmptySpaceItem();
			this.layoutControlItemBnAdd = new DevExpress.XtraLayout.LayoutControlItem();
			this.emptySpaceAboveAddRemoveBn = new DevExpress.XtraLayout.EmptySpaceItem();
			this.layoutControlItemBnRemove = new DevExpress.XtraLayout.LayoutControlItem();
			((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).BeginInit();
			this.ContentPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.publicRoomBindingSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
			this.layoutControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.checkEditAdvMode.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.checkEditBaseMode.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.checkEditPublicOnly.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textEditSearchFor.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.checkEditGridMode.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.roomListBoxControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.checkEditPublicAndPrivate.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridControl_RoomsInfo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutView2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutViewField1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutViewField2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutViewField3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutViewField4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutViewField5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutViewField6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutViewField7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.repositoryAddRoom)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colIsFavorite)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colRoomType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutViewCard1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.item1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.item3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.item4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.item6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.item2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.item5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.item9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.checkEditCardMode.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.comboBoxSearchColumn.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.checkEditShowFavorites.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.roomListLayoutControlItem)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.calendarLayoutControlItem)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceBelowAddRemoveBn)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBnAdd)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceAboveAddRemoveBn)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBnRemove)).BeginInit();
			this.SuspendLayout();
			// 
			// ContentPanel
			// 
			this.ContentPanel.Controls.Add(this.layoutControl1);
			this.ContentPanel.Size = new System.Drawing.Size(1159, 440);
			// 
			// gridColumn1
			// 
			this.gridColumn1.Caption = "gridColumn1";
			this.gridColumn1.FieldName = "pictureCol";
			this.gridColumn1.Name = "gridColumn1";
			this.gridColumn1.Visible = true;
			this.gridColumn1.VisibleIndex = 0;
			// 
			// gridColumn2
			// 
			this.gridColumn2.Caption = "gridColumn2";
			this.gridColumn2.FieldName = "propCol";
			this.gridColumn2.Name = "gridColumn2";
			this.gridColumn2.Visible = true;
			this.gridColumn2.VisibleIndex = 1;
			// 
			// publicRoomBindingSource
			// 
			this.publicRoomBindingSource.DataSource = typeof(MyVrm.Outlook.WinForms.Conference.PublicRoom);
			// 
			// layoutControl1
			// 
			this.layoutControl1.Controls.Add(this.addButton);
			this.layoutControl1.Controls.Add(this.checkEditAdvMode);
			this.layoutControl1.Controls.Add(this.checkEditBaseMode);
			this.layoutControl1.Controls.Add(this.roomsCalendarButton);
			this.layoutControl1.Controls.Add(this.removeButton);
			this.layoutControl1.Controls.Add(this.checkEditPublicOnly);
			this.layoutControl1.Controls.Add(this.textEditSearchFor);
			this.layoutControl1.Controls.Add(this.checkEditGridMode);
			this.layoutControl1.Controls.Add(this.roomListBoxControl);
			this.layoutControl1.Controls.Add(this.checkEditPublicAndPrivate);
			this.layoutControl1.Controls.Add(this.gridControl_RoomsInfo);
			this.layoutControl1.Controls.Add(this.checkEditCardMode);
			this.layoutControl1.Controls.Add(this.comboBoxSearchColumn);
			this.layoutControl1.Controls.Add(this.checkEditShowFavorites);
			this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.layoutControl1.Location = new System.Drawing.Point(0, 0);
			this.layoutControl1.Name = "layoutControl1";
			this.layoutControl1.Root = this.layoutControlGroup1;
			this.layoutControl1.Size = new System.Drawing.Size(1159, 440);
			this.layoutControl1.TabIndex = 1;
			this.layoutControl1.Text = "layoutControl1";
			// 
			// addButton
			// 
			this.addButton.Location = new System.Drawing.Point(929, 126);
			this.addButton.Name = "addButton";
			this.addButton.Size = new System.Drawing.Size(20, 147);
			this.addButton.StyleController = this.layoutControl1;
			this.addButton.TabIndex = 18;
			this.addButton.Text = ">";
			this.addButton.ToolTip = "Add the selected room to the list";
			this.addButton.Visible = false;
			this.addButton.Click += new System.EventHandler(this.addButton_Click);
			// 
			// checkEditAdvMode
			// 
			this.checkEditAdvMode.Location = new System.Drawing.Point(631, 32);
			this.checkEditAdvMode.Name = "checkEditAdvMode";
			this.checkEditAdvMode.Properties.Caption = "Advanced";
			this.checkEditAdvMode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
			this.checkEditAdvMode.Size = new System.Drawing.Size(285, 21);
			this.checkEditAdvMode.StyleController = this.layoutControl1;
			this.checkEditAdvMode.TabIndex = 17;
			this.checkEditAdvMode.CheckedChanged += new System.EventHandler(this.checkEditAdvMode_CheckedChanged);
			// 
			// checkEditBaseMode
			// 
			this.checkEditBaseMode.Location = new System.Drawing.Point(429, 32);
			this.checkEditBaseMode.Name = "checkEditBaseMode";
			this.checkEditBaseMode.Properties.Caption = "Base";
			this.checkEditBaseMode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
			this.checkEditBaseMode.Size = new System.Drawing.Size(190, 21);
			this.checkEditBaseMode.StyleController = this.layoutControl1;
			this.checkEditBaseMode.TabIndex = 16;
			this.checkEditBaseMode.CheckedChanged += new System.EventHandler(this.checkEditBaseMode_CheckedChanged);
			// 
			// roomsCalendarButton
			// 
			this.roomsCalendarButton.Location = new System.Drawing.Point(1001, 406);
			this.roomsCalendarButton.Name = "roomsCalendarButton";
			this.roomsCalendarButton.Size = new System.Drawing.Size(108, 24);
			this.roomsCalendarButton.StyleController = this.layoutControl1;
			this.roomsCalendarButton.TabIndex = 15;
			this.roomsCalendarButton.Text = "Rooms Calendar";
			this.roomsCalendarButton.ToolTip = "Show a calendar for non-public rooms";
			this.roomsCalendarButton.Click += new System.EventHandler(this.roomsCalendarButton_Click);
			// 
			// removeButton
			// 
			this.removeButton.Location = new System.Drawing.Point(929, 277);
			this.removeButton.Name = "removeButton";
			this.removeButton.Size = new System.Drawing.Size(20, 161);
			this.removeButton.StyleController = this.layoutControl1;
			this.removeButton.TabIndex = 19;
			this.removeButton.Text = "<";
			this.removeButton.ToolTip = "Remove the selected room from the list";
			this.removeButton.Visible = false;
			this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
			// 
			// checkEditPublicOnly
			// 
			this.checkEditPublicOnly.Location = new System.Drawing.Point(936, 62);
			this.checkEditPublicOnly.Name = "checkEditPublicOnly";
			this.checkEditPublicOnly.Properties.Caption = "Public Only";
			this.checkEditPublicOnly.Size = new System.Drawing.Size(214, 21);
			this.checkEditPublicOnly.StyleController = this.layoutControl1;
			this.checkEditPublicOnly.TabIndex = 13;
			this.checkEditPublicOnly.CheckedChanged += new System.EventHandler(this.checkEditXX_CheckedChanged);
			// 
			// textEditSearchFor
			// 
			this.textEditSearchFor.Location = new System.Drawing.Point(81, 94);
			this.textEditSearchFor.Name = "textEditSearchFor";
			this.textEditSearchFor.Size = new System.Drawing.Size(190, 22);
			this.textEditSearchFor.StyleController = this.layoutControl1;
			this.textEditSearchFor.TabIndex = 9;
			this.textEditSearchFor.EditValueChanged += new System.EventHandler(this.textEditSearchFor_EditValueChanged);
			// 
			// checkEditGridMode
			// 
			this.checkEditGridMode.Location = new System.Drawing.Point(222, 32);
			this.checkEditGridMode.Name = "checkEditGridMode";
			this.checkEditGridMode.Properties.Caption = "Table Mode";
			this.checkEditGridMode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
			this.checkEditGridMode.Size = new System.Drawing.Size(189, 21);
			this.checkEditGridMode.StyleController = this.layoutControl1;
			this.checkEditGridMode.TabIndex = 5;
			this.checkEditGridMode.CheckedChanged += new System.EventHandler(this.checkEditGridMode_CheckedChanged);
			// 
			// roomListBoxControl
			// 
			this.roomListBoxControl.Location = new System.Drawing.Point(956, 151);
			this.roomListBoxControl.Name = "roomListBoxControl";
			this.roomListBoxControl.Padding = new System.Windows.Forms.Padding(2);
			this.roomListBoxControl.Size = new System.Drawing.Size(198, 246);
			this.roomListBoxControl.StyleController = this.layoutControl1;
			this.roomListBoxControl.TabIndex = 10;
			this.roomListBoxControl.ToolTip = "Double click on a room to remove it from the list of selected rooms";
			this.roomListBoxControl.DoubleClick += new System.EventHandler(this.roomListBoxControl_DoubleClick);
			// 
			// checkEditPublicAndPrivate
			// 
			this.checkEditPublicAndPrivate.Location = new System.Drawing.Point(936, 91);
			this.checkEditPublicAndPrivate.Name = "checkEditPublicAndPrivate";
			this.checkEditPublicAndPrivate.Properties.Caption = "Public and Private";
			this.checkEditPublicAndPrivate.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
			this.checkEditPublicAndPrivate.Size = new System.Drawing.Size(214, 21);
			this.checkEditPublicAndPrivate.StyleController = this.layoutControl1;
			this.checkEditPublicAndPrivate.TabIndex = 12;
			this.checkEditPublicAndPrivate.Visible = false;
			this.checkEditPublicAndPrivate.CheckedChanged += new System.EventHandler(this.checkEditXX_CheckedChanged);
			// 
			// gridControl_RoomsInfo
			// 
			this.gridControl_RoomsInfo.DataSource = this.publicRoomBindingSource;
			this.gridControl_RoomsInfo.Location = new System.Drawing.Point(4, 128);
			this.gridControl_RoomsInfo.MainView = this.layoutView2;
			this.gridControl_RoomsInfo.Name = "gridControl_RoomsInfo";
			this.gridControl_RoomsInfo.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPictureEdit2,
            this.repositoryItemButtonEdit2,
            this.repositoryAddRoom,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEdit1,
            this.repositoryItemButtonEdit1});
			this.gridControl_RoomsInfo.Size = new System.Drawing.Size(918, 308);
			this.gridControl_RoomsInfo.TabIndex = 7;
			this.gridControl_RoomsInfo.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.layoutView2,
            this.gridView3});
			this.gridControl_RoomsInfo.DoubleClick += new System.EventHandler(this.gridControl_RoomsInfo_DoubleClick);
			this.gridControl_RoomsInfo.MouseHover += new System.EventHandler(this.gridControl_RoomsInfo_MouseHover);
			// 
			// layoutView2
			// 
			this.layoutView2.ActiveFilterEnabled = false;
			this.layoutView2.CardHorzInterval = 1;
			this.layoutView2.CardMinSize = new System.Drawing.Size(636, 185);
			this.layoutView2.CardVertInterval = 1;
			this.layoutView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.layoutViewColumn1,
            this.layoutViewColumn2,
            this.layoutViewColumn3,
            this.layoutViewColumn4,
            this.layoutViewColumn5,
            this.layoutViewColumn6,
            this.layoutViewColumn7,
            this.layoutViewColumn8,
            this.colIsFavorite,
            this.colRoomType});
			this.layoutView2.GridControl = this.gridControl_RoomsInfo;
			this.layoutView2.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutViewField1});
			this.layoutView2.Name = "layoutView2";
			this.layoutView2.OptionsBehavior.AllowRuntimeCustomization = false;
			this.layoutView2.OptionsBehavior.ScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Auto;
			this.layoutView2.OptionsCustomization.AllowFilter = false;
			this.layoutView2.OptionsSingleRecordMode.StretchCardToViewHeight = true;
			this.layoutView2.OptionsSingleRecordMode.StretchCardToViewWidth = true;
			this.layoutView2.OptionsView.CardArrangeRule = DevExpress.XtraGrid.Views.Layout.LayoutCardArrangeRule.AllowPartialCards;
			this.layoutView2.OptionsView.ShowCardCaption = false;
			this.layoutView2.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
			this.layoutView2.OptionsView.ViewMode = DevExpress.XtraGrid.Views.Layout.LayoutViewMode.Column;
			this.layoutView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.layoutViewColumn2, DevExpress.Data.ColumnSortOrder.Ascending)});
			this.layoutView2.TemplateCard = this.layoutViewCard1;
			// 
			// layoutViewColumn1
			// 
			this.layoutViewColumn1.AppearanceCell.Options.UseTextOptions = true;
			this.layoutViewColumn1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.layoutViewColumn1.FieldName = "ID";
			this.layoutViewColumn1.LayoutViewField = this.layoutViewField1;
			this.layoutViewColumn1.Name = "layoutViewColumn1";
			this.layoutViewColumn1.OptionsColumn.AllowEdit = false;
			this.layoutViewColumn1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
			this.layoutViewColumn1.OptionsColumn.ReadOnly = true;
			this.layoutViewColumn1.OptionsFilter.AllowAutoFilter = false;
			this.layoutViewColumn1.OptionsFilter.AllowFilter = false;
			// 
			// layoutViewField1
			// 
			this.layoutViewField1.EditorPreferredWidth = 399;
			this.layoutViewField1.Location = new System.Drawing.Point(181, 0);
			this.layoutViewField1.Name = "layoutViewField1";
			this.layoutViewField1.Size = new System.Drawing.Size(483, 20);
			this.layoutViewField1.TextSize = new System.Drawing.Size(75, 16);
			this.layoutViewField1.TextToControlDistance = 5;
			this.layoutViewField1.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
			// 
			// layoutViewColumn2
			// 
			this.layoutViewColumn2.AppearanceCell.Options.UseTextOptions = true;
			this.layoutViewColumn2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.layoutViewColumn2.Caption = "Room Name";
			this.layoutViewColumn2.FieldName = "Room Name";
			this.layoutViewColumn2.LayoutViewField = this.layoutViewField2;
			this.layoutViewColumn2.Name = "layoutViewColumn2";
			this.layoutViewColumn2.OptionsColumn.AllowEdit = false;
			this.layoutViewColumn2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.layoutViewColumn2.OptionsColumn.ReadOnly = true;
			this.layoutViewColumn2.OptionsFilter.AllowAutoFilter = false;
			this.layoutViewColumn2.OptionsFilter.AllowFilter = false;
			// 
			// layoutViewField2
			// 
			this.layoutViewField2.EditorPreferredWidth = 399;
			this.layoutViewField2.Location = new System.Drawing.Point(181, 20);
			this.layoutViewField2.Name = "layoutViewField2";
			this.layoutViewField2.Size = new System.Drawing.Size(483, 20);
			this.layoutViewField2.TextSize = new System.Drawing.Size(75, 16);
			this.layoutViewField2.TextToControlDistance = 5;
			// 
			// layoutViewColumn3
			// 
			this.layoutViewColumn3.AppearanceCell.Options.UseTextOptions = true;
			this.layoutViewColumn3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.layoutViewColumn3.ColumnEdit = this.repositoryItemPictureEdit2;
			this.layoutViewColumn3.FieldName = "Picture";
			this.layoutViewColumn3.LayoutViewField = this.layoutViewField3;
			this.layoutViewColumn3.Name = "layoutViewColumn3";
			this.layoutViewColumn3.OptionsColumn.AllowEdit = false;
			this.layoutViewColumn3.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
			this.layoutViewColumn3.OptionsColumn.ReadOnly = true;
			this.layoutViewColumn3.OptionsFilter.AllowAutoFilter = false;
			this.layoutViewColumn3.OptionsFilter.AllowFilter = false;
			this.layoutViewColumn3.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
			// 
			// repositoryItemPictureEdit2
			// 
			this.repositoryItemPictureEdit2.Name = "repositoryItemPictureEdit2";
			this.repositoryItemPictureEdit2.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
			// 
			// layoutViewField3
			// 
			this.layoutViewField3.EditorPreferredWidth = 177;
			this.layoutViewField3.Location = new System.Drawing.Point(0, 0);
			this.layoutViewField3.Name = "layoutViewField3";
			this.layoutViewField3.Size = new System.Drawing.Size(181, 182);
			this.layoutViewField3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.SupportHorzAlignment;
			this.layoutViewField3.TextSize = new System.Drawing.Size(0, 0);
			this.layoutViewField3.TextToControlDistance = 0;
			this.layoutViewField3.TextVisible = false;
			// 
			// layoutViewColumn4
			// 
			this.layoutViewColumn4.Caption = "Show Details";
			this.layoutViewColumn4.ColumnEdit = this.repositoryItemButtonEdit2;
			this.layoutViewColumn4.LayoutViewField = this.layoutViewField4;
			this.layoutViewColumn4.Name = "layoutViewColumn4";
			this.layoutViewColumn4.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
			this.layoutViewColumn4.OptionsFilter.AllowAutoFilter = false;
			this.layoutViewColumn4.OptionsFilter.AllowFilter = false;
			this.layoutViewColumn4.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
			// 
			// repositoryItemButtonEdit2
			// 
			this.repositoryItemButtonEdit2.AutoHeight = false;
			this.repositoryItemButtonEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Show Room Info", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
			this.repositoryItemButtonEdit2.Name = "repositoryItemButtonEdit2";
			this.repositoryItemButtonEdit2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
			this.repositoryItemButtonEdit2.Click += new System.EventHandler(this.ShowDetails);
			// 
			// layoutViewField4
			// 
			this.layoutViewField4.EditorPreferredWidth = 115;
			this.layoutViewField4.ImageToTextDistance = 0;
			this.layoutViewField4.Location = new System.Drawing.Point(293, 156);
			this.layoutViewField4.Name = "layoutViewField4";
			this.layoutViewField4.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutViewField4.Size = new System.Drawing.Size(118, 26);
			this.layoutViewField4.TextSize = new System.Drawing.Size(0, 0);
			this.layoutViewField4.TextToControlDistance = 0;
			this.layoutViewField4.TextVisible = false;
			// 
			// layoutViewColumn5
			// 
			this.layoutViewColumn5.FieldName = "Address";
			this.layoutViewColumn5.LayoutViewField = this.layoutViewField5;
			this.layoutViewColumn5.Name = "layoutViewColumn5";
			this.layoutViewColumn5.OptionsColumn.AllowEdit = false;
			this.layoutViewColumn5.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.layoutViewColumn5.OptionsColumn.ReadOnly = true;
			this.layoutViewColumn5.OptionsFilter.AllowAutoFilter = false;
			this.layoutViewColumn5.OptionsFilter.AllowFilter = false;
			// 
			// layoutViewField5
			// 
			this.layoutViewField5.EditorPreferredWidth = 399;
			this.layoutViewField5.Location = new System.Drawing.Point(181, 42);
			this.layoutViewField5.Name = "layoutViewField5";
			this.layoutViewField5.Size = new System.Drawing.Size(483, 20);
			this.layoutViewField5.TextSize = new System.Drawing.Size(75, 16);
			this.layoutViewField5.TextToControlDistance = 5;
			// 
			// layoutViewColumn6
			// 
			this.layoutViewColumn6.AppearanceCell.Options.UseTextOptions = true;
			this.layoutViewColumn6.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.layoutViewColumn6.FieldName = "Capacity";
			this.layoutViewColumn6.LayoutViewField = this.layoutViewField6;
			this.layoutViewColumn6.Name = "layoutViewColumn6";
			this.layoutViewColumn6.OptionsColumn.AllowEdit = false;
			this.layoutViewColumn6.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.layoutViewColumn6.OptionsColumn.ReadOnly = true;
			this.layoutViewColumn6.OptionsFilter.AllowAutoFilter = false;
			this.layoutViewColumn6.OptionsFilter.AllowFilter = false;
			// 
			// layoutViewField6
			// 
			this.layoutViewField6.EditorPreferredWidth = 399;
			this.layoutViewField6.Location = new System.Drawing.Point(181, 84);
			this.layoutViewField6.Name = "layoutViewField6";
			this.layoutViewField6.Size = new System.Drawing.Size(483, 20);
			this.layoutViewField6.TextSize = new System.Drawing.Size(75, 16);
			this.layoutViewField6.TextToControlDistance = 5;
			// 
			// layoutViewColumn7
			// 
			this.layoutViewColumn7.FieldName = "State";
			this.layoutViewColumn7.LayoutViewField = this.layoutViewField7;
			this.layoutViewColumn7.Name = "layoutViewColumn7";
			this.layoutViewColumn7.OptionsColumn.AllowEdit = false;
			this.layoutViewColumn7.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.layoutViewColumn7.OptionsColumn.ReadOnly = true;
			this.layoutViewColumn7.OptionsFilter.AllowAutoFilter = false;
			this.layoutViewColumn7.OptionsFilter.AllowFilter = false;
			// 
			// layoutViewField7
			// 
			this.layoutViewField7.EditorPreferredWidth = 399;
			this.layoutViewField7.Location = new System.Drawing.Point(181, 62);
			this.layoutViewField7.Name = "layoutViewField7";
			this.layoutViewField7.Size = new System.Drawing.Size(483, 20);
			this.layoutViewField7.TextSize = new System.Drawing.Size(75, 16);
			this.layoutViewField7.TextToControlDistance = 5;
			// 
			// layoutViewColumn8
			// 
			this.layoutViewColumn8.Caption = "layoutViewColumnAddBtn";
			this.layoutViewColumn8.ColumnEdit = this.repositoryAddRoom;
			this.layoutViewColumn8.LayoutViewField = this.layoutViewField_layoutViewColumn8;
			this.layoutViewColumn8.Name = "layoutViewColumn8";
			this.layoutViewColumn8.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
			this.layoutViewColumn8.OptionsColumn.ShowCaption = false;
			this.layoutViewColumn8.OptionsFilter.AllowAutoFilter = false;
			this.layoutViewColumn8.OptionsFilter.AllowFilter = false;
			// 
			// repositoryAddRoom
			// 
			this.repositoryAddRoom.AutoHeight = false;
			this.repositoryAddRoom.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Add to conference", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
			this.repositoryAddRoom.Name = "repositoryAddRoom";
			this.repositoryAddRoom.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
			this.repositoryAddRoom.Click += new System.EventHandler(this.repositoryAddRoom_Click);
			// 
			// layoutViewField_layoutViewColumn8
			// 
			this.layoutViewField_layoutViewColumn8.EditorPreferredWidth = 123;
			this.layoutViewField_layoutViewColumn8.ImageToTextDistance = 0;
			this.layoutViewField_layoutViewColumn8.Location = new System.Drawing.Point(429, 156);
			this.layoutViewField_layoutViewColumn8.Name = "layoutViewField_layoutViewColumn8";
			this.layoutViewField_layoutViewColumn8.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutViewField_layoutViewColumn8.Size = new System.Drawing.Size(126, 26);
			this.layoutViewField_layoutViewColumn8.TextSize = new System.Drawing.Size(0, 0);
			this.layoutViewField_layoutViewColumn8.TextToControlDistance = 0;
			this.layoutViewField_layoutViewColumn8.TextVisible = false;
			// 
			// colIsFavorite
			// 
			this.colIsFavorite.AppearanceCell.Options.UseTextOptions = true;
			this.colIsFavorite.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.colIsFavorite.AppearanceHeader.Options.UseTextOptions = true;
			this.colIsFavorite.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.colIsFavorite.ColumnEdit = this.repositoryItemTextEdit1;
			this.colIsFavorite.FieldName = "IsFavorite";
			this.colIsFavorite.LayoutViewField = this.layoutViewField_colIsFavorite;
			this.colIsFavorite.Name = "colIsFavorite";
			this.colIsFavorite.OptionsColumn.AllowEdit = false;
			this.colIsFavorite.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.colIsFavorite.OptionsColumn.ReadOnly = true;
			this.colIsFavorite.OptionsFilter.AllowAutoFilter = false;
			this.colIsFavorite.OptionsFilter.AllowFilter = false;
			// 
			// repositoryItemTextEdit1
			// 
			this.repositoryItemTextEdit1.AutoHeight = false;
			this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
			// 
			// layoutViewField_colIsFavorite
			// 
			this.layoutViewField_colIsFavorite.EditorPreferredWidth = 399;
			this.layoutViewField_colIsFavorite.Location = new System.Drawing.Point(181, 124);
			this.layoutViewField_colIsFavorite.Name = "layoutViewField_colIsFavorite";
			this.layoutViewField_colIsFavorite.Size = new System.Drawing.Size(483, 20);
			this.layoutViewField_colIsFavorite.TextSize = new System.Drawing.Size(75, 16);
			this.layoutViewField_colIsFavorite.TextToControlDistance = 5;
			// 
			// colRoomType
			// 
			this.colRoomType.AppearanceCell.Options.UseTextOptions = true;
			this.colRoomType.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.colRoomType.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
			this.colRoomType.Caption = "Type";
			this.colRoomType.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
			this.colRoomType.FieldName = "RoomType";
			this.colRoomType.LayoutViewField = this.layoutViewField_colRoomType;
			this.colRoomType.Name = "colRoomType";
			this.colRoomType.OptionsColumn.AllowEdit = false;
			this.colRoomType.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.colRoomType.OptionsColumn.ReadOnly = true;
			this.colRoomType.OptionsFilter.AllowAutoFilter = false;
			this.colRoomType.OptionsFilter.AllowFilter = false;
			// 
			// layoutViewField_colRoomType
			// 
			this.layoutViewField_colRoomType.EditorPreferredWidth = 399;
			this.layoutViewField_colRoomType.Location = new System.Drawing.Point(181, 104);
			this.layoutViewField_colRoomType.Name = "layoutViewField_colRoomType";
			this.layoutViewField_colRoomType.Size = new System.Drawing.Size(483, 20);
			this.layoutViewField_colRoomType.TextSize = new System.Drawing.Size(75, 16);
			this.layoutViewField_colRoomType.TextToControlDistance = 5;
			// 
			// layoutViewCard1
			// 
			this.layoutViewCard1.CustomizationFormText = "TemplateCard";
			this.layoutViewCard1.ExpandButtonLocation = DevExpress.Utils.GroupElementLocation.AfterText;
			this.layoutViewCard1.GroupBordersVisible = false;
			this.layoutViewCard1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutViewField3,
            this.layoutViewField1,
            this.item1,
            this.item3,
            this.item4,
            this.layoutViewField6,
            this.layoutViewField5,
            this.layoutViewField2,
            this.layoutViewField7,
            this.item6,
            this.item2,
            this.item5,
            this.layoutViewField4,
            this.layoutViewField_layoutViewColumn8,
            this.item9,
            this.layoutViewField_colIsFavorite,
            this.layoutViewField_colRoomType});
			this.layoutViewCard1.Name = "layoutViewTemplateCard";
			this.layoutViewCard1.OptionsItemText.TextToControlDistance = 5;
			this.layoutViewCard1.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
			this.layoutViewCard1.Text = "TemplateCard";
			// 
			// item1
			// 
			this.item1.CustomizationFormText = "item1";
			this.item1.Location = new System.Drawing.Point(181, 144);
			this.item1.Name = "item1";
			this.item1.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
			this.item1.Size = new System.Drawing.Size(483, 10);
			this.item1.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
			this.item1.Text = "item1";
			this.item1.TextSize = new System.Drawing.Size(0, 0);
			// 
			// item3
			// 
			this.item3.CustomizationFormText = "item3";
			this.item3.Location = new System.Drawing.Point(555, 156);
			this.item3.MinSize = new System.Drawing.Size(106, 26);
			this.item3.Name = "item3";
			this.item3.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
			this.item3.Size = new System.Drawing.Size(109, 26);
			this.item3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.item3.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
			this.item3.Text = "item3";
			this.item3.TextSize = new System.Drawing.Size(0, 0);
			// 
			// item4
			// 
			this.item4.CustomizationFormText = "item4";
			this.item4.Location = new System.Drawing.Point(181, 156);
			this.item4.MinSize = new System.Drawing.Size(106, 26);
			this.item4.Name = "item4";
			this.item4.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
			this.item4.Size = new System.Drawing.Size(112, 26);
			this.item4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.item4.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
			this.item4.Text = "item4";
			this.item4.TextSize = new System.Drawing.Size(0, 0);
			// 
			// item6
			// 
			this.item6.CustomizationFormText = "item6";
			this.item6.Location = new System.Drawing.Point(181, 40);
			this.item6.Name = "item6";
			this.item6.Size = new System.Drawing.Size(483, 2);
			this.item6.Text = "item6";
			// 
			// item2
			// 
			this.item2.CustomizationFormText = "item2";
			this.item2.Location = new System.Drawing.Point(181, 82);
			this.item2.Name = "item2";
			this.item2.Size = new System.Drawing.Size(483, 2);
			this.item2.Text = "item2";
			// 
			// item5
			// 
			this.item5.CustomizationFormText = "item5";
			this.item5.Location = new System.Drawing.Point(181, 154);
			this.item5.Name = "item5";
			this.item5.Size = new System.Drawing.Size(483, 2);
			this.item5.Text = "item5";
			// 
			// item9
			// 
			this.item9.CustomizationFormText = "item9";
			this.item9.Location = new System.Drawing.Point(411, 156);
			this.item9.Name = "item9";
			this.item9.Size = new System.Drawing.Size(18, 26);
			this.item9.Text = "item9";
			this.item9.TextSize = new System.Drawing.Size(0, 0);
			// 
			// repositoryItemCheckEdit1
			// 
			this.repositoryItemCheckEdit1.AutoHeight = false;
			this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
			// 
			// repositoryItemButtonEdit1
			// 
			this.repositoryItemButtonEdit1.AutoHeight = false;
			serializableAppearanceObject3.Image = global::MyVrm.Outlook.WinForms.Properties.Resources.information;
			serializableAppearanceObject3.Options.UseImage = true;
			this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonEdit1.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "Show room information", null, null, true)});
			this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
			this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
			this.repositoryItemButtonEdit1.Click += new System.EventHandler(this.ShowDetails);
			// 
			// gridView3
			// 
			this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnShowDetails,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.colRoomType1,
            this.colIsFavorite1});
			this.gridView3.GridControl = this.gridControl_RoomsInfo;
			this.gridView3.Name = "gridView3";
			this.gridView3.OptionsCustomization.AllowColumnMoving = false;
			this.gridView3.OptionsCustomization.AllowFilter = false;
			this.gridView3.OptionsCustomization.AllowRowSizing = true;
			this.gridView3.OptionsFilter.AllowColumnMRUFilterList = false;
			this.gridView3.OptionsFilter.AllowFilterEditor = false;
			this.gridView3.OptionsFilter.AllowMRUFilterList = false;
			this.gridView3.OptionsHint.ShowCellHints = false;
			this.gridView3.OptionsHint.ShowColumnHeaderHints = false;
			this.gridView3.OptionsHint.ShowFooterHints = false;
			this.gridView3.OptionsMenu.EnableColumnMenu = false;
			this.gridView3.OptionsMenu.EnableFooterMenu = false;
			this.gridView3.OptionsMenu.EnableGroupPanelMenu = false;
			this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
			this.gridView3.OptionsView.RowAutoHeight = true;
			this.gridView3.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
			this.gridView3.OptionsView.ShowGroupPanel = false;
			this.gridView3.OptionsView.ShowIndicator = false;
			this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn5, DevExpress.Data.ColumnSortOrder.Ascending)});
			// 
			// gridColumnShowDetails
			// 
			this.gridColumnShowDetails.Caption = "Show Details";
			this.gridColumnShowDetails.ColumnEdit = this.repositoryItemButtonEdit1;
			this.gridColumnShowDetails.MinWidth = 113;
			this.gridColumnShowDetails.Name = "gridColumnShowDetails";
			this.gridColumnShowDetails.Visible = true;
			this.gridColumnShowDetails.VisibleIndex = 0;
			this.gridColumnShowDetails.Width = 113;
			// 
			// gridColumn3
			// 
			this.gridColumn3.ColumnEdit = this.repositoryItemPictureEdit2;
			this.gridColumn3.FieldName = "Picture";
			this.gridColumn3.Name = "gridColumn3";
			this.gridColumn3.OptionsColumn.AllowEdit = false;
			this.gridColumn3.OptionsFilter.AllowAutoFilter = false;
			// 
			// gridColumn4
			// 
			this.gridColumn4.AppearanceCell.Options.UseTextOptions = true;
			this.gridColumn4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.gridColumn4.FieldName = "ID";
			this.gridColumn4.Name = "gridColumn4";
			this.gridColumn4.OptionsColumn.AllowEdit = false;
			this.gridColumn4.OptionsFilter.AllowAutoFilter = false;
			this.gridColumn4.Width = 30;
			// 
			// gridColumn5
			// 
			this.gridColumn5.FieldName = "Name";
			this.gridColumn5.Name = "gridColumn5";
			this.gridColumn5.OptionsColumn.AllowEdit = false;
			this.gridColumn5.OptionsFilter.AllowAutoFilter = false;
			this.gridColumn5.Visible = true;
			this.gridColumn5.VisibleIndex = 1;
			this.gridColumn5.Width = 122;
			// 
			// gridColumn6
			// 
			this.gridColumn6.FieldName = "Address";
			this.gridColumn6.Name = "gridColumn6";
			this.gridColumn6.OptionsColumn.AllowEdit = false;
			this.gridColumn6.OptionsFilter.AllowAutoFilter = false;
			this.gridColumn6.Visible = true;
			this.gridColumn6.VisibleIndex = 2;
			this.gridColumn6.Width = 76;
			// 
			// gridColumn7
			// 
			this.gridColumn7.FieldName = "State";
			this.gridColumn7.Name = "gridColumn7";
			this.gridColumn7.OptionsColumn.AllowEdit = false;
			this.gridColumn7.OptionsFilter.AllowAutoFilter = false;
			this.gridColumn7.Visible = true;
			this.gridColumn7.VisibleIndex = 3;
			this.gridColumn7.Width = 79;
			// 
			// gridColumn8
			// 
			this.gridColumn8.AppearanceCell.Options.UseTextOptions = true;
			this.gridColumn8.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.gridColumn8.FieldName = "Capacity";
			this.gridColumn8.Name = "gridColumn8";
			this.gridColumn8.OptionsColumn.AllowEdit = false;
			this.gridColumn8.OptionsFilter.AllowAutoFilter = false;
			this.gridColumn8.Visible = true;
			this.gridColumn8.VisibleIndex = 4;
			this.gridColumn8.Width = 95;
			// 
			// colRoomType1
			// 
			this.colRoomType1.AppearanceCell.Options.UseTextOptions = true;
			this.colRoomType1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.colRoomType1.FieldName = "RoomType";
			this.colRoomType1.Name = "colRoomType1";
			this.colRoomType1.OptionsColumn.AllowEdit = false;
			this.colRoomType1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.colRoomType1.OptionsColumn.ReadOnly = true;
			this.colRoomType1.OptionsFilter.AllowAutoFilter = false;
			this.colRoomType1.OptionsFilter.AllowFilter = false;
			this.colRoomType1.Visible = true;
			this.colRoomType1.VisibleIndex = 5;
			this.colRoomType1.Width = 92;
			// 
			// colIsFavorite1
			// 
			this.colIsFavorite1.ColumnEdit = this.repositoryItemTextEdit1;
			this.colIsFavorite1.FieldName = "IsFavorite";
			this.colIsFavorite1.Name = "colIsFavorite1";
			this.colIsFavorite1.OptionsColumn.AllowEdit = false;
			this.colIsFavorite1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.colIsFavorite1.OptionsColumn.ReadOnly = true;
			this.colIsFavorite1.OptionsFilter.AllowAutoFilter = false;
			this.colIsFavorite1.OptionsFilter.AllowFilter = false;
			this.colIsFavorite1.Visible = true;
			this.colIsFavorite1.VisibleIndex = 6;
			this.colIsFavorite1.Width = 102;
			// 
			// checkEditCardMode
			// 
			this.checkEditCardMode.Location = new System.Drawing.Point(18, 32);
			this.checkEditCardMode.Name = "checkEditCardMode";
			this.checkEditCardMode.Properties.Caption = "Card Mode";
			this.checkEditCardMode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
			this.checkEditCardMode.Size = new System.Drawing.Size(192, 21);
			this.checkEditCardMode.StyleController = this.layoutControl1;
			this.checkEditCardMode.TabIndex = 4;
			this.checkEditCardMode.CheckedChanged += new System.EventHandler(this.checkEditCardMode_CheckedChanged);
			// 
			// comboBoxSearchColumn
			// 
			this.comboBoxSearchColumn.Location = new System.Drawing.Point(366, 94);
			this.comboBoxSearchColumn.Name = "comboBoxSearchColumn";
			this.comboBoxSearchColumn.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.comboBoxSearchColumn.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
			this.comboBoxSearchColumn.Size = new System.Drawing.Size(551, 22);
			this.comboBoxSearchColumn.StyleController = this.layoutControl1;
			this.comboBoxSearchColumn.TabIndex = 8;
			this.comboBoxSearchColumn.SelectedIndexChanged += new System.EventHandler(this.comboBoxSearchColumn_SelectedIndexChanged);
			// 
			// checkEditShowFavorites
			// 
			this.checkEditShowFavorites.Location = new System.Drawing.Point(936, 33);
			this.checkEditShowFavorites.Name = "checkEditShowFavorites";
			this.checkEditShowFavorites.Properties.Caption = "Favorites Only";
			this.checkEditShowFavorites.Size = new System.Drawing.Size(214, 21);
			this.checkEditShowFavorites.StyleController = this.layoutControl1;
			this.checkEditShowFavorites.TabIndex = 11;
			this.checkEditShowFavorites.CheckedChanged += new System.EventHandler(this.checkEditXX_CheckedChanged);
			// 
			// layoutControlGroup1
			// 
			this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
			this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
			this.layoutControlGroup1.GroupBordersVisible = false;
			this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.layoutControlGroup4,
            this.layoutControlGroup5,
            this.emptySpaceBelowAddRemoveBn,
            this.layoutControlItemBnAdd,
            this.emptySpaceAboveAddRemoveBn,
            this.layoutControlItemBnRemove});
			this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup1.Name = "Root";
			this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup1.Size = new System.Drawing.Size(1159, 440);
			this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup1.Text = "Root";
			this.layoutControlGroup1.TextVisible = false;
			// 
			// layoutControlItem4
			// 
			this.layoutControlItem4.Control = this.gridControl_RoomsInfo;
			this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
			this.layoutControlItem4.Location = new System.Drawing.Point(0, 124);
			this.layoutControlItem4.MinSize = new System.Drawing.Size(104, 24);
			this.layoutControlItem4.Name = "layoutControlItem4";
			this.layoutControlItem4.Size = new System.Drawing.Size(926, 316);
			this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem4.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
			this.layoutControlItem4.Text = "layoutControlItem4";
			this.layoutControlItem4.TextLocation = DevExpress.Utils.Locations.Top;
			this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem4.TextToControlDistance = 0;
			this.layoutControlItem4.TextVisible = false;
			// 
			// layoutControlGroup2
			// 
			this.layoutControlGroup2.CustomizationFormText = "Rooms to Show ";
			this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem6});
			this.layoutControlGroup2.Location = new System.Drawing.Point(927, 0);
			this.layoutControlGroup2.Name = "layoutControlGroup2";
			this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
			this.layoutControlGroup2.Size = new System.Drawing.Size(232, 124);
			this.layoutControlGroup2.Text = "Rooms to Show ";
			// 
			// layoutControlItem7
			// 
			this.layoutControlItem7.Control = this.checkEditPublicOnly;
			this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
			this.layoutControlItem7.Location = new System.Drawing.Point(0, 31);
			this.layoutControlItem7.MinSize = new System.Drawing.Size(93, 29);
			this.layoutControlItem7.Name = "layoutControlItem7";
			this.layoutControlItem7.Size = new System.Drawing.Size(222, 29);
			this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem7.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
			this.layoutControlItem7.Text = "layoutControlItem7";
			this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem7.TextToControlDistance = 0;
			this.layoutControlItem7.TextVisible = false;
			// 
			// layoutControlItem8
			// 
			this.layoutControlItem8.Control = this.checkEditPublicAndPrivate;
			this.layoutControlItem8.CustomizationFormText = "layoutControlItem7";
			this.layoutControlItem8.Location = new System.Drawing.Point(0, 60);
			this.layoutControlItem8.MinSize = new System.Drawing.Size(132, 29);
			this.layoutControlItem8.Name = "layoutControlItem8";
			this.layoutControlItem8.Size = new System.Drawing.Size(222, 32);
			this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem8.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
			this.layoutControlItem8.Text = "layoutControlItem8";
			this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem8.TextToControlDistance = 0;
			this.layoutControlItem8.TextVisible = false;
			this.layoutControlItem8.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
			// 
			// layoutControlItem6
			// 
			this.layoutControlItem6.Control = this.checkEditShowFavorites;
			this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
			this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem6.MinSize = new System.Drawing.Size(112, 31);
			this.layoutControlItem6.Name = "layoutControlItem6";
			this.layoutControlItem6.Size = new System.Drawing.Size(222, 31);
			this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem6.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 4, 2);
			this.layoutControlItem6.Text = "layoutControlItem6";
			this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem6.TextToControlDistance = 0;
			this.layoutControlItem6.TextVisible = false;
			// 
			// layoutControlGroup3
			// 
			this.layoutControlGroup3.CustomizationFormText = "Selected rooms:";
			this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.roomListLayoutControlItem,
            this.layoutControlGroup8});
			this.layoutControlGroup3.Location = new System.Drawing.Point(951, 124);
			this.layoutControlGroup3.Name = "layoutControlGroup3";
			this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
			this.layoutControlGroup3.Size = new System.Drawing.Size(208, 316);
			this.layoutControlGroup3.Text = "Selected rooms:";
			// 
			// roomListLayoutControlItem
			// 
			this.roomListLayoutControlItem.AppearanceItemCaption.Options.UseTextOptions = true;
			this.roomListLayoutControlItem.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.roomListLayoutControlItem.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.roomListLayoutControlItem.Control = this.roomListBoxControl;
			this.roomListLayoutControlItem.CustomizationFormText = "roomListLayoutControlItem";
			this.roomListLayoutControlItem.Location = new System.Drawing.Point(0, 0);
			this.roomListLayoutControlItem.MinSize = new System.Drawing.Size(54, 20);
			this.roomListLayoutControlItem.Name = "roomListLayoutControlItem";
			this.roomListLayoutControlItem.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 2);
			this.roomListLayoutControlItem.Size = new System.Drawing.Size(198, 250);
			this.roomListLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.roomListLayoutControlItem.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 2);
			this.roomListLayoutControlItem.Text = "roomListLayoutControlItem";
			this.roomListLayoutControlItem.TextLocation = DevExpress.Utils.Locations.Top;
			this.roomListLayoutControlItem.TextSize = new System.Drawing.Size(0, 0);
			this.roomListLayoutControlItem.TextToControlDistance = 0;
			this.roomListLayoutControlItem.TextVisible = false;
			// 
			// layoutControlGroup8
			// 
			this.layoutControlGroup8.CustomizationFormText = "layoutControlGroup8";
			this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.calendarLayoutControlItem});
			this.layoutControlGroup8.Location = new System.Drawing.Point(0, 250);
			this.layoutControlGroup8.Name = "layoutControlGroup8";
			this.layoutControlGroup8.Padding = new DevExpress.XtraLayout.Utils.Padding(40, 40, 0, 0);
			this.layoutControlGroup8.Size = new System.Drawing.Size(198, 34);
			this.layoutControlGroup8.Text = "layoutControlGroup8";
			this.layoutControlGroup8.TextVisible = false;
			// 
			// calendarLayoutControlItem
			// 
			this.calendarLayoutControlItem.Control = this.roomsCalendarButton;
			this.calendarLayoutControlItem.CustomizationFormText = "calendarLayoutControlItem";
			this.calendarLayoutControlItem.Location = new System.Drawing.Point(0, 0);
			this.calendarLayoutControlItem.MinSize = new System.Drawing.Size(109, 27);
			this.calendarLayoutControlItem.Name = "calendarLayoutControlItem";
			this.calendarLayoutControlItem.Size = new System.Drawing.Size(112, 28);
			this.calendarLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.calendarLayoutControlItem.Text = "Rooms Calendar";
			this.calendarLayoutControlItem.TextSize = new System.Drawing.Size(0, 0);
			this.calendarLayoutControlItem.TextToControlDistance = 0;
			this.calendarLayoutControlItem.TextVisible = false;
			// 
			// layoutControlGroup4
			// 
			this.layoutControlGroup4.CustomizationFormText = "View Mode";
			this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup6,
            this.layoutControlGroup7});
			this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup4.Name = "layoutControlGroup4";
			this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
			this.layoutControlGroup4.Size = new System.Drawing.Size(926, 63);
			this.layoutControlGroup4.Text = "View Mode";
			// 
			// layoutControlGroup6
			// 
			this.layoutControlGroup6.CustomizationFormText = "layoutControlGroup6";
			this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem9,
            this.layoutControlItem10});
			this.layoutControlGroup6.Location = new System.Drawing.Point(411, 0);
			this.layoutControlGroup6.Name = "layoutControlGroup6";
			this.layoutControlGroup6.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup6.Size = new System.Drawing.Size(505, 31);
			this.layoutControlGroup6.Text = "layoutControlGroup6";
			this.layoutControlGroup6.TextVisible = false;
			// 
			// layoutControlItem9
			// 
			this.layoutControlItem9.Control = this.checkEditBaseMode;
			this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
			this.layoutControlItem9.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem9.MinSize = new System.Drawing.Size(87, 25);
			this.layoutControlItem9.Name = "layoutControlItem9";
			this.layoutControlItem9.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 2, 2, 2);
			this.layoutControlItem9.Size = new System.Drawing.Size(202, 25);
			this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem9.Text = "layoutControlItem9";
			this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem9.TextToControlDistance = 0;
			this.layoutControlItem9.TextVisible = false;
			// 
			// layoutControlItem10
			// 
			this.layoutControlItem10.Control = this.checkEditAdvMode;
			this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
			this.layoutControlItem10.Location = new System.Drawing.Point(202, 0);
			this.layoutControlItem10.MinSize = new System.Drawing.Size(87, 25);
			this.layoutControlItem10.Name = "layoutControlItem10";
			this.layoutControlItem10.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 2, 2, 2);
			this.layoutControlItem10.Size = new System.Drawing.Size(297, 25);
			this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem10.Text = "layoutControlItem10";
			this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem10.TextToControlDistance = 0;
			this.layoutControlItem10.TextVisible = false;
			// 
			// layoutControlGroup7
			// 
			this.layoutControlGroup7.CustomizationFormText = "layoutControlGroup7";
			this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2});
			this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup7.Name = "layoutControlGroup7";
			this.layoutControlGroup7.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup7.Size = new System.Drawing.Size(411, 31);
			this.layoutControlGroup7.Text = "layoutControlGroup7";
			this.layoutControlGroup7.TextVisible = false;
			// 
			// layoutControlItem1
			// 
			this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
			this.layoutControlItem1.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
			this.layoutControlItem1.Control = this.checkEditCardMode;
			this.layoutControlItem1.CustomizationFormText = "Card Mode";
			this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem1.MinSize = new System.Drawing.Size(89, 25);
			this.layoutControlItem1.Name = "layoutControlItem1";
			this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 2, 2, 2);
			this.layoutControlItem1.Size = new System.Drawing.Size(204, 25);
			this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem1.Text = "layoutControlItem1";
			this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem1.TextToControlDistance = 0;
			this.layoutControlItem1.TextVisible = false;
			// 
			// layoutControlItem2
			// 
			this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
			this.layoutControlItem2.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
			this.layoutControlItem2.Control = this.checkEditGridMode;
			this.layoutControlItem2.CustomizationFormText = "Grid Mode";
			this.layoutControlItem2.Location = new System.Drawing.Point(204, 0);
			this.layoutControlItem2.MinSize = new System.Drawing.Size(85, 25);
			this.layoutControlItem2.Name = "layoutControlItem2";
			this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 2, 2, 2);
			this.layoutControlItem2.Size = new System.Drawing.Size(201, 25);
			this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem2.Text = "Grid Mode";
			this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem2.TextToControlDistance = 0;
			this.layoutControlItem2.TextVisible = false;
			// 
			// layoutControlGroup5
			// 
			this.layoutControlGroup5.CustomizationFormText = "Search Parameters";
			this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem3});
			this.layoutControlGroup5.Location = new System.Drawing.Point(0, 63);
			this.layoutControlGroup5.Name = "layoutControlGroup5";
			this.layoutControlGroup5.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
			this.layoutControlGroup5.Size = new System.Drawing.Size(926, 61);
			this.layoutControlGroup5.Text = "Search Parameters";
			// 
			// layoutControlItem5
			// 
			this.layoutControlItem5.Control = this.textEditSearchFor;
			this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
			this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem5.MinSize = new System.Drawing.Size(123, 26);
			this.layoutControlItem5.Name = "layoutControlItem5";
			this.layoutControlItem5.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 2, 2, 2);
			this.layoutControlItem5.Size = new System.Drawing.Size(270, 29);
			this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem5.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
			this.layoutControlItem5.Text = "Search for:";
			this.layoutControlItem5.TextSize = new System.Drawing.Size(65, 16);
			// 
			// layoutControlItem3
			// 
			this.layoutControlItem3.Control = this.comboBoxSearchColumn;
			this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
			this.layoutControlItem3.Location = new System.Drawing.Point(270, 0);
			this.layoutControlItem3.MinSize = new System.Drawing.Size(152, 26);
			this.layoutControlItem3.Name = "layoutControlItem3";
			this.layoutControlItem3.Padding = new DevExpress.XtraLayout.Utils.Padding(20, 2, 2, 2);
			this.layoutControlItem3.Size = new System.Drawing.Size(646, 29);
			this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem3.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
			this.layoutControlItem3.Text = "Search by:";
			this.layoutControlItem3.TextSize = new System.Drawing.Size(65, 16);
			// 
			// emptySpaceBelowAddRemoveBn
			// 
			this.emptySpaceBelowAddRemoveBn.CustomizationFormText = "emptySpaceItem3";
			this.emptySpaceBelowAddRemoveBn.Location = new System.Drawing.Point(926, 245);
			this.emptySpaceBelowAddRemoveBn.MinSize = new System.Drawing.Size(1, 24);
			this.emptySpaceBelowAddRemoveBn.Name = "emptySpaceBelowAddRemoveBn";
			this.emptySpaceBelowAddRemoveBn.Size = new System.Drawing.Size(1, 195);
			this.emptySpaceBelowAddRemoveBn.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.emptySpaceBelowAddRemoveBn.Text = "emptySpaceBelowAddRemoveBn";
			this.emptySpaceBelowAddRemoveBn.TextSize = new System.Drawing.Size(0, 0);
			// 
			// layoutControlItemBnAdd
			// 
			this.layoutControlItemBnAdd.Control = this.addButton;
			this.layoutControlItemBnAdd.CustomizationFormText = ">";
			this.layoutControlItemBnAdd.Location = new System.Drawing.Point(927, 124);
			this.layoutControlItemBnAdd.MinSize = new System.Drawing.Size(1, 27);
			this.layoutControlItemBnAdd.Name = "layoutControlItemBnAdd";
			this.layoutControlItemBnAdd.Size = new System.Drawing.Size(24, 151);
			this.layoutControlItemBnAdd.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItemBnAdd.Text = ">";
			this.layoutControlItemBnAdd.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItemBnAdd.TextToControlDistance = 0;
			this.layoutControlItemBnAdd.TextVisible = false;
			// 
			// emptySpaceAboveAddRemoveBn
			// 
			this.emptySpaceAboveAddRemoveBn.CustomizationFormText = "emptySpaceItem4";
			this.emptySpaceAboveAddRemoveBn.Location = new System.Drawing.Point(926, 0);
			this.emptySpaceAboveAddRemoveBn.MinSize = new System.Drawing.Size(1, 24);
			this.emptySpaceAboveAddRemoveBn.Name = "emptySpaceAboveAddRemoveBn";
			this.emptySpaceAboveAddRemoveBn.Size = new System.Drawing.Size(1, 245);
			this.emptySpaceAboveAddRemoveBn.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.emptySpaceAboveAddRemoveBn.Text = "emptySpaceAboveAddRemoveBn";
			this.emptySpaceAboveAddRemoveBn.TextSize = new System.Drawing.Size(0, 0);
			// 
			// layoutControlItemBnRemove
			// 
			this.layoutControlItemBnRemove.Control = this.removeButton;
			this.layoutControlItemBnRemove.CustomizationFormText = "<";
			this.layoutControlItemBnRemove.Location = new System.Drawing.Point(927, 275);
			this.layoutControlItemBnRemove.MinSize = new System.Drawing.Size(1, 27);
			this.layoutControlItemBnRemove.Name = "layoutControlItemBnRemove";
			this.layoutControlItemBnRemove.Size = new System.Drawing.Size(24, 165);
			this.layoutControlItemBnRemove.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItemBnRemove.Text = "<";
			this.layoutControlItemBnRemove.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItemBnRemove.TextToControlDistance = 0;
			this.layoutControlItemBnRemove.TextVisible = false;
			// 
			// PublicRoomList
			// 
			this.ApplyEnabled = true;
			this.ApplyVisible = true;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
			this.ClientSize = new System.Drawing.Size(1183, 496);
			this.Name = "PublicRoomList";
			this.Text = "Room List";
			this.Load += new System.EventHandler(this.PublicRoomList_Load);
			this.Closed += new System.EventHandler(this.PublicRoomList_Closed);
			((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).EndInit();
			this.ContentPanel.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.publicRoomBindingSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
			this.layoutControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.checkEditAdvMode.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.checkEditBaseMode.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.checkEditPublicOnly.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textEditSearchFor.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.checkEditGridMode.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.roomListBoxControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.checkEditPublicAndPrivate.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridControl_RoomsInfo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutView2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutViewField1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutViewField2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutViewField3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutViewField4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutViewField5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutViewField6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutViewField7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.repositoryAddRoom)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colIsFavorite)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colRoomType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutViewCard1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.item1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.item3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.item4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.item6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.item2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.item5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.item9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.checkEditCardMode.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.comboBoxSearchColumn.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.checkEditShowFavorites.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.roomListLayoutControlItem)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.calendarLayoutControlItem)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceBelowAddRemoveBn)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBnAdd)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceAboveAddRemoveBn)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBnRemove)).EndInit();
			this.ResumeLayout(false);

		}


		#endregion

		private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
		private System.Windows.Forms.BindingSource publicRoomBindingSource;
		private DevExpress.XtraLayout.LayoutControl layoutControl1;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private DevExpress.XtraEditors.CheckEdit checkEditGridMode;
		private DevExpress.XtraEditors.CheckEdit checkEditCardMode;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
		private DevExpress.XtraGrid.GridControl gridControl_RoomsInfo;
		private DevExpress.XtraGrid.Views.Layout.LayoutView layoutView2;
		private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn1;
		private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn2;
		private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn3;
		private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit2;
		private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn4;
		private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit2;
		private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn5;
		private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn6;
		private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn7;
		private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumnShowDetails;
		private DevExpress.XtraEditors.ComboBoxEdit comboBoxSearchColumn;
		private DevExpress.XtraEditors.TextEdit textEditSearchFor;
		private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn8;
		private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryAddRoom;
		private DevExpress.XtraEditors.ListBoxControl roomListBoxControl;
		private DevExpress.XtraLayout.LayoutControlItem roomListLayoutControlItem;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
		private DevExpress.XtraEditors.CheckEdit checkEditShowFavorites;
		private DevExpress.XtraEditors.CheckEdit checkEditPublicAndPrivate;
		private DevExpress.XtraEditors.CheckEdit checkEditPublicOnly;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
		private DevExpress.XtraEditors.SimpleButton roomsCalendarButton;
		private DevExpress.XtraLayout.LayoutControlItem calendarLayoutControlItem;
		private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField1;
		private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField2;
		private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField3;
		private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField4;
		private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField5;
		private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField6;
		private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField7;
		private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn8;
		private DevExpress.XtraGrid.Columns.LayoutViewColumn colIsFavorite;
		private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colIsFavorite;
		private DevExpress.XtraGrid.Columns.LayoutViewColumn colRoomType;
		private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colRoomType;
		private DevExpress.XtraGrid.Views.Layout.LayoutViewCard layoutViewCard1;
		private DevExpress.XtraLayout.EmptySpaceItem item1;
		private DevExpress.XtraLayout.EmptySpaceItem item3;
		private DevExpress.XtraLayout.EmptySpaceItem item4;
		private DevExpress.XtraLayout.SimpleSeparator item6;
		private DevExpress.XtraLayout.SimpleSeparator item2;
		private DevExpress.XtraLayout.SimpleSeparator item5;
		private DevExpress.XtraLayout.EmptySpaceItem item9;
		private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
		private DevExpress.XtraGrid.Columns.GridColumn colRoomType1;
		private DevExpress.XtraGrid.Columns.GridColumn colIsFavorite1;
		private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
		private DevExpress.XtraEditors.CheckEdit checkEditAdvMode;
		private DevExpress.XtraEditors.CheckEdit checkEditBaseMode;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceBelowAddRemoveBn;
		private DevExpress.XtraEditors.SimpleButton removeButton;
		private DevExpress.XtraEditors.SimpleButton addButton;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemBnAdd;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceAboveAddRemoveBn;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemBnRemove;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
		private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
	}
}