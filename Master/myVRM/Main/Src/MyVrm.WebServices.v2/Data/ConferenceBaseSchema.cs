﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using MyVrm.Common.ComponentModel;

namespace MyVrm.WebServices.Data
{
    internal class ConferenceBaseSchema : ServiceObjectSchema
    {
        internal static readonly PropertyDefinition Id;
        internal static readonly PropertyDefinition UniqueId;
        internal static readonly PropertyDefinition Name;
        internal static readonly PropertyDefinition HostId;
        internal static readonly PropertyDefinition HostName;
        internal static readonly PropertyDefinition HostEmail;
        internal static readonly PropertyDefinition Origin;
        internal static readonly PropertyDefinition Password;
        internal static readonly PropertyDefinition IsImmediate;
        internal static readonly PropertyDefinition IsPublic;
        
        internal static readonly PropertyDefinition IsOpenForPublicRegistration;
        internal static readonly PropertyDefinition IsRecurring;
        internal static readonly PropertyDefinition AppointmentTime;
        internal static readonly PropertyDefinition RecurringText;
        internal static readonly PropertyDefinition RecurrencePattern;
        internal static readonly PropertyDefinition RecurrenceRange;
        internal static readonly PropertyDefinition StartDate;
        internal static readonly PropertyDefinition StartHour;
        internal static readonly PropertyDefinition StartMin;
        internal static readonly PropertyDefinition StartSet;
        internal static readonly PropertyDefinition TimeZone;
        internal static readonly PropertyDefinition Duration;
        internal static readonly PropertyDefinition Description;
        internal static readonly PropertyDefinition Participants;
        internal static readonly PropertyDefinition ConfGuestRooms;//101343
        internal static readonly PropertyDefinition Rooms;
        internal static readonly PropertyDefinition CustomAttributes;
        internal static readonly PropertyDefinition AudioVideoParameters;
		//internal static readonly PropertyDefinition AdvancedAudioVideoSettings;
		//internal static readonly PropertyDefinition InventoryWorkOrders;
		//internal static readonly PropertyDefinition HouseKeepingWorkOrders;
		//internal static readonly PropertyDefinition CateringWorkOrders;
        internal static readonly PropertyDefinition ModifyType;

		internal static readonly PropertyDefinition IsCloudConferencing;
		internal static readonly PropertyDefinition IsPCconference;
		internal static readonly PropertyDefinition VMRType;
		internal static readonly PropertyDefinition PCVendorId;
    	internal static readonly PropertyDefinition IsSecured;
    	internal static readonly PropertyDefinition ConciergeSupport;
		internal static readonly PropertyDefinition ConfMessageCollection;
        internal static readonly PropertyDefinition ConfDialInNum;
        internal static readonly PropertyDefinition BridgeList;//101343
		public static readonly ConferenceBaseSchema Instance;
        
        


        internal static readonly PropertyDefinition MeetandGreetBuffer; //ZD 102651
       
        


		static ConferenceBaseSchema()
        {
            Id = new ComplexPropertyDefinition<ConferenceId>("confID", () => new ConferenceId());
            UniqueId = new ComplexPropertyDefinition<ConferenceId>("confUniqueID", () => new ConferenceId());
            Name = new StringPropertyDefinition("confName", PropertyDefinitionFlags.CanSet | PropertyDefinitionFlags.CanUpdate);
            HostId = new ComplexPropertyDefinition<UserId>("hostId", "confHost", PropertyDefinitionFlags.CanSet | PropertyDefinitionFlags.CanUpdate,
                                                           () => new UserId());
            HostName = new StringPropertyDefinition("hostName");
            HostEmail = new StringPropertyDefinition("hostEmail");
            Origin = new GenericPropertyDefinition<ConferenceOrigin>("confOrigin", PropertyDefinitionFlags.CanSet | PropertyDefinitionFlags.CanUpdate);
            Password = new StringPropertyDefinition("confPassword", PropertyDefinitionFlags.CanSet | PropertyDefinitionFlags.CanUpdate);
            IsImmediate = new BoolPropertyDefinition("immediate", PropertyDefinitionFlags.CanSet | PropertyDefinitionFlags.CanUpdate);
            IsPublic = new BoolPropertyDefinition("publicConf", PropertyDefinitionFlags.CanSet | PropertyDefinitionFlags.CanUpdate);
            
            IsOpenForPublicRegistration = new BoolPropertyDefinition("dynamicInvite", PropertyDefinitionFlags.CanSet | PropertyDefinitionFlags.CanUpdate);
            IsRecurring = new BoolPropertyDefinition("recurring", PropertyDefinitionFlags.CanSet | PropertyDefinitionFlags.CanUpdate);
            AppointmentTime = new ComplexPropertyDefinition<AppointmentTime>("appointmentTime",
                                                                             PropertyDefinitionFlags.CanSet | PropertyDefinitionFlags.CanUpdate,
                                                                             () => new AppointmentTime());
            RecurringText = new StringPropertyDefinition("recurringText", PropertyDefinitionFlags.CanSet | PropertyDefinitionFlags.CanUpdate);
            RecurrencePattern = new ComplexPropertyDefinition<RecurrencePattern>("recurrencePattern",
                                                                                 PropertyDefinitionFlags.CanSet | PropertyDefinitionFlags.CanUpdate,
                                                                                 () =>
                                                                                 new RecurrencePattern(TimeZoneInfo.
                                                                                                           FindSystemTimeZoneById
                                                                                                           (
                                                                                                           TimeZoneInfo.
                                                                                                               Local.Id)));
            
            RecurrenceRange = new RecurrenceRangePropertyDefinition("recurrenceRange", PropertyDefinitionFlags.CanSet | PropertyDefinitionFlags.CanUpdate);

        
            

            StartDate = new StringPropertyDefinition("startDate", PropertyDefinitionFlags.CanSet | PropertyDefinitionFlags.CanUpdate);
            ConfDialInNum = new StringPropertyDefinition("confDialinNum", PropertyDefinitionFlags.CanSet | PropertyDefinitionFlags.CanUpdate);
            StartHour = new StringPropertyDefinition("startHour", PropertyDefinitionFlags.CanSet | PropertyDefinitionFlags.CanUpdate);
            StartMin = new StringPropertyDefinition("startMin", PropertyDefinitionFlags.CanSet | PropertyDefinitionFlags.CanUpdate);
            StartSet = new StringPropertyDefinition("startSet", PropertyDefinitionFlags.CanSet | PropertyDefinitionFlags.CanUpdate);

            TimeZone = new TimeZonePropertyDefinition("timeZone", PropertyDefinitionFlags.CanSet | PropertyDefinitionFlags.CanUpdate);
            Duration = new TimeSpanPropertyDefinition("durationMin", PropertyDefinitionFlags.CanSet | PropertyDefinitionFlags.CanUpdate);
            Description = new StringPropertyDefinition("description", PropertyDefinitionFlags.CanSet | PropertyDefinitionFlags.CanUpdate);
            
            Participants = new ComplexPropertyDefinition<ParticipantCollection>("partys",
                                                                                PropertyDefinitionFlags.
                                                                                    AutoInstantiateOnRead |
                                                                                PropertyDefinitionFlags.CanSet,
                                                                                () => new ParticipantCollection());
            ConfGuestRooms = new ComplexPropertyDefinition<ConfGuestRoomCollection>("ConfGuestRooms",
                                                                              PropertyDefinitionFlags.
                                                                                  AutoInstantiateOnRead |
                                                                              PropertyDefinitionFlags.CanSet,
                                                                              () => new ConfGuestRoomCollection()); //ZD 101224


            BridgeList = new ComplexPropertyDefinition<ConfGuestRoomCollection>("ConfBridges",
                                                                              PropertyDefinitionFlags.
                                                                                  AutoInstantiateOnRead |
                                                                              PropertyDefinitionFlags.CanSet,
                                                                              () => new ConfGuestRoomCollection()); //ZD 101224
            
            Rooms = new ComplexPropertyDefinition<RoomIdCollection>("locationList",
                                                                            PropertyDefinitionFlags.
                                                                                AutoInstantiateOnRead |
                                                                            PropertyDefinitionFlags.CanSet,
                                                                            () => new RoomIdCollection());
            
            CustomAttributes = new ComplexPropertyDefinition<CustomAttributeCollection>("CustomAttributesList",
                                                                                            PropertyDefinitionFlags.
                                                                                                AutoInstantiateOnRead |
                                                                                            PropertyDefinitionFlags.
                                                                                                CanSet,
                                                                                            () =>
                                                                                            new CustomAttributeCollection
                                                                                                ());
            AudioVideoParameters = new ComplexPropertyDefinition<AdvancedAudioVideoParameters>("advAVParam",
                                                                                               PropertyDefinitionFlags.
                                                                                                   AutoInstantiateOnRead |
                                                                                               PropertyDefinitionFlags.
                                                                                                   CanSet,
                                                                                               () =>
                                                                                               new AdvancedAudioVideoParameters
                                                                                                   ());
			//AdvancedAudioVideoSettings =
			//    new ComplexPropertyDefinition<ConferenceAdvancedAudioVideoSettings>("AdvancedAudioVideoSettings",
			//                                                                        PropertyDefinitionFlags.
			//                                                                            None,
			//                                                                        () =>
			//                                                                        new ConferenceAdvancedAudioVideoSettings
			//                                                                            ());
			//InventoryWorkOrders = new ComplexPropertyDefinition<WorkOrderCollection>("AudioVideoWorkOrders",
			//                                                                          () => new WorkOrderCollection());
			//HouseKeepingWorkOrders = new ComplexPropertyDefinition<WorkOrderCollection>("HouseKeepingWorkOrders",
			//                                                                          () => new WorkOrderCollection());
			//CateringWorkOrders = new ComplexPropertyDefinition<CateringWorkOrderCollection>("CateringWorkOrders",
			//                                                                                () =>
			//                                                                                new CateringWorkOrderCollection
			//                                                                                    ());
            ModifyType = new IntPropertyDefinition("ModifyType", PropertyDefinitionFlags.CanSet | PropertyDefinitionFlags.CanUpdate);

			VMRType = new IntPropertyDefinition("isVMR", PropertyDefinitionFlags.CanSet | PropertyDefinitionFlags.CanUpdate);
			IsCloudConferencing = new BoolPropertyDefinition("CloudConferencing", PropertyDefinitionFlags.CanSet | PropertyDefinitionFlags.CanUpdate);
			IsPCconference = new BoolPropertyDefinition("isPCconference", PropertyDefinitionFlags.CanSet | PropertyDefinitionFlags.CanUpdate);
            MeetandGreetBuffer = new IntPropertyDefinition("MeetandGreetBuffer", PropertyDefinitionFlags.CanSet | PropertyDefinitionFlags.CanUpdate); //ZD 102651
                
            
			PCVendorId = new IntPropertyDefinition("pcVendorId", PropertyDefinitionFlags.CanSet | PropertyDefinitionFlags.CanUpdate);
            
			IsSecured = new IntPropertyDefinition("Secured", PropertyDefinitionFlags.CanSet | PropertyDefinitionFlags.CanUpdate);

			ConciergeSupport = new ComplexPropertyDefinition<ConciergeSupportParams>("ConciergeSupport",
                                                                            PropertyDefinitionFlags.AutoInstantiateOnRead |
                                                                                               PropertyDefinitionFlags.CanSet,
                                                                                               () =>
                                                                                               new ConciergeSupportParams());
			ConfMessageCollection = new ComplexPropertyDefinition<ConfMessageCollection>("ConfMessageList",
																			PropertyDefinitionFlags.AutoInstantiateOnRead |
																							   PropertyDefinitionFlags.CanSet,
																							   () =>
																							   new ConfMessageCollection());

           

            Instance = new ConferenceSchema();
        }

        internal override void RegisterProperties()
        {
            base.RegisterProperties();
            RegisterProperty(Id);
            RegisterProperty(UniqueId);
            RegisterProperty(Name);
            RegisterProperty(HostId);
            RegisterProperty(HostName);
            RegisterProperty(HostEmail);
            RegisterProperty(Origin);
            RegisterProperty(Password);
            RegisterProperty(IsImmediate);
            RegisterProperty(IsPublic);
            
            RegisterProperty(IsOpenForPublicRegistration);
            RegisterProperty(IsRecurring);
            RegisterProperty(AppointmentTime);
            RegisterProperty(RecurringText);
            RegisterProperty(RecurrencePattern);
            RegisterProperty(RecurrenceRange);
            RegisterProperty(StartDate);
            RegisterProperty(StartHour);
            RegisterProperty(StartMin);
            RegisterProperty(StartSet);
            RegisterProperty(TimeZone);
            RegisterProperty(Duration);
            RegisterProperty(Description);
            RegisterProperty(Participants);
            RegisterProperty(ConfGuestRooms);//101343
            RegisterProperty(Rooms);
            RegisterProperty(CustomAttributes);
            RegisterProperty(AudioVideoParameters);
			//RegisterProperty(AdvancedAudioVideoSettings);
			//RegisterProperty(InventoryWorkOrders);
			//RegisterProperty(HouseKeepingWorkOrders);
			//RegisterProperty(CateringWorkOrders);
            RegisterProperty(ModifyType);
        	RegisterProperty(IsPCconference);
			RegisterProperty(VMRType);
			RegisterProperty(PCVendorId);
			RegisterProperty(IsCloudConferencing);
			RegisterProperty(IsSecured);
			RegisterProperty(ConciergeSupport);
			RegisterProperty(ConfMessageCollection);
            RegisterProperty(ConfDialInNum);
            

            RegisterProperty(MeetandGreetBuffer); //ZD 102651
            
            
        }
    }
}
