﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using MyVrm.Common.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace MyVrm.Common.Tests
{
    
    
    /// <summary>
    ///This is a test class for SecurityUtilsTest and is intended
    ///to contain all SecurityUtilsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SecurityUtilsTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for JoinNTUserName
        ///</summary>
        [TestMethod()]
        public void JoinNTUserNameTest()
        {
            var domainName = "Domain";
            var userName = "User";
            var expected = @"Domain\User";
            string actual;
            actual = SecurityUtils.JoinNTUserName(domainName, userName);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for SplitNTUserName
        ///</summary>
        [TestMethod()]
        public void SplitNTUserNameTest()
        {
            var userName = @"Domain\User";
            var domainName = string.Empty;
            var domainNameExpected = "Domain";
            var accountName = string.Empty;
            var  accountNameExpected = "User";
            SecurityUtils.SplitNTUserName(userName, out domainName, out accountName);
            Assert.AreEqual(domainNameExpected, domainName);
            Assert.AreEqual(accountNameExpected, accountName);
        }
    }
}
