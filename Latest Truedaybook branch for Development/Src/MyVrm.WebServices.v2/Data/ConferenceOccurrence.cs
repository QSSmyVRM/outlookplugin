﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.ObjectModel;

namespace MyVrm.WebServices.Data
{
    /// <summary>
    /// Represents a single occurrence of a conference.
    /// </summary>
    public class ConferenceOccurrence
    {
        public ConferenceId ConferenceId { get; private set; }
        public string ConferenceName { get; private set; }
        public ConferenceType ConferenceType { get; private set; }
        public DateTime Date { get; private set; }
        public TimeSpan SetupTime { get; private set; }
        public TimeSpan TeardownTime { get; private set; }
        public TimeSpan Duration { get; private set; }
        public UserId OwnerId { get; private set; }
        public int ParticipantId { get; private set; }
        public ConferenceOccurrenceRoomCollection Rooms { get; private set; }
        public bool IsImmediate { get; private set; }
        public bool IsFuture { get; private set; }
        public bool IsPublic { get; private set; }
        public bool IsPending { get; private set; }
        public bool IsApproval { get; private set; }
        public bool IsDeleted { get; private set; }

        internal ConferenceOccurrence()
        {
            Rooms = new ConferenceOccurrenceRoomCollection();
        }

        internal void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
        {
            do
            {
                reader.Read();
                if (reader.LocalName == "confID")
                {
                    ConferenceId = new ConferenceId();
                    ConferenceId.LoadFromXml(reader, "confID");
                }
                else if (reader.LocalName == "confName")
                {
                    ConferenceName = reader.ReadValue();
                }
                else if (reader.LocalName == "ConferenceType")
                {
                    ConferenceType = reader.ReadValue<ConferenceType>();
                }
                else if (reader.LocalName == "confDate")
                {
                    Date = Utilities.StringToDate(reader.ReadValue()).Date + Date.TimeOfDay;
                }
				else if (reader.LocalName == "confTime")
				{
                    Date += Utilities.TimeOfDayStringToTimeSpan(reader.ReadValue(), reader.Service.IsUtcEnabled);
				}
				else if (reader.LocalName == "setupTime")
				{
                    SetupTime = Utilities.TimeOfDayStringToTimeSpan(reader.ReadValue(), reader.Service.IsUtcEnabled);
				}
				else if (reader.LocalName == "teardownTime")
				{
					TeardownTime = Utilities.TimeOfDayStringToTimeSpan(reader.ReadValue(), reader.Service.IsUtcEnabled);
				}
				else if (reader.LocalName == "durationMin")
				{
					Duration = TimeSpan.FromMinutes(reader.ReadValue<int>());
				}
				else if (reader.LocalName == "owner")
				{
					OwnerId = new UserId();
					OwnerId.LoadFromXml(reader, "owner");
				}
				else if (reader.LocalName == "mainLocation")
				{
					Rooms.LoadFromXml(reader, "mainLocation");
				}
				else if (reader.LocalName == "isImmediate")
				{
					IsImmediate = reader.ReadValue<bool>();
				}
				else if (reader.LocalName == "isFuture")
				{
					IsFuture = reader.ReadValue<bool>();
				}
				else if (reader.LocalName == "isPublic")
				{
					IsPublic = reader.ReadValue<bool>();
				}
				else if (reader.LocalName == "isPending")
				{
					IsPending = reader.ReadValue<bool>();
				}
				else if (reader.LocalName == "isApproval")
				{
					IsApproval = reader.ReadValue<bool>();
				}
				else if (reader.LocalName == "deleted")
				{
					IsDeleted = reader.ReadValue<bool>();
				}
				else
				{
					reader.SkipCurrentElement();
				}
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, xmlElementName));
        }
    }
}
