﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;

namespace MyVrm.WebServices.Data
{
    internal class GetRoomRequest : ServiceRequestBase<GetRoomResponse>
    {
        internal GetRoomRequest(MyVrmService service) : base(service)
        {
        }

        internal RoomId RoomId { get; set; }

        #region Overrides of ServiceRequestBase

        internal override string GetXmlElementName()
        {
            return "GetRoomProfile";
        }

        internal override string GetCommandName()
        {
            return "GetRoomProfile";
        }

        internal override string GetResponseXmlElementName()
        {
            return "GetRoomProfile";
        }

        internal override GetRoomResponse ParseResponse(MyVrmServiceXmlReader reader)
        {
            var response = new GetRoomResponse {Room = new Room(Service)};
            response.LoadFromXml(reader, GetResponseXmlElementName());
            return response;
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            UserId.WriteToXml(writer, "UserID");
            RoomId.WriteToXml(writer, "RoomID");
        }

        #endregion
    }
}
