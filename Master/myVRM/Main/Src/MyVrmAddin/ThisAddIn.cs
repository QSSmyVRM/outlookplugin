﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using DevExpress.LookAndFeel;
using DevExpress.UserSkins;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Outlook;
using Microsoft.Office.Interop.Outlook.Extensions;
using Microsoft.Win32;
using MyVrm.Outlook;
using MyVrm.Outlook.WinForms;
using MyVrm.Outlook.WinForms.ApprovePendings;
using MyVrm.Outlook.WinForms.Calendar;
using MyVrm.Outlook.WinForms.Conference;
using MyVrm.Outlook.WinForms.Options;
using MyVrm.WebServices.Data;
using MyVrmAddin2003;
using MyVrmAddin2003.Properties;
using Exception = System.Exception;
using Image = System.Drawing.Image;

namespace MyVrmAddin2003
{
	public partial class ThisAddIn
	{
		private Explorer _currentExplorer;
		private List<AppointmentItem> _selectedAppointments = new List<AppointmentItem>();

		private Inspectors _inspectors;
		private Explorers _openExplorers;
		private readonly Dictionary<Guid, InspectorWrapper> _wrappedInspectors = new Dictionary<Guid, InspectorWrapper>();

		private readonly OutlookAppointmentImplCollection _appointments = new OutlookAppointmentImplCollection();
		private readonly Dictionary<OutlookAppointmentImpl, InspectorWrapper> _appointmentToWrappers = new Dictionary<OutlookAppointmentImpl, InspectorWrapper>();
		private readonly Dictionary<OutlookAppointmentImpl, ConferenceWrapper> _appointmentToConferenceWrappers = new Dictionary<OutlookAppointmentImpl, ConferenceWrapper>();
		private DefaultLookAndFeel _defaultLookAndFeel;

		CommandBar _myVrmCommandBar;
		CommandBarButton _conferenceButton;
		CommandBarButton _favoritesButton;
		CommandBarButton _roomCalendarButton;
		CommandBarButton _approvalPendingButton;
		CommandBarButton _optionsButton;

		private CommandBarComboBox _templateCombo;
		CommandBarButton _fillTemplateCombo;
		List<TemplateInfo> _templateList = new List<TemplateInfo>();

		const string myVRMRegFullPath = "HKEY_CURRENT_USER\\Software\\myVRM\\myVRM Outlook Add-in";
		const string myVRMRegPath = "Software\\myVRM\\myVRM Outlook Add-in"; //HKEY_CURRENT_USER\\
		const string myVRMToolbarVal = "myVRMToolbarEnabled";

		[EditorBrowsable(EditorBrowsableState.Never)]
		public OutlookAppointmentImplCollection Appointments
		{
			get { return _appointments; }
		}

		public Dictionary<OutlookAppointmentImpl, ConferenceWrapper> AppointmentToConferenceWrappers
		{
			get { return _appointmentToConferenceWrappers; }
		}

		public bool HasConferenceWrapper(AppointmentItem appointmentItem)
		{
			var outlookAppointmentImpl = Appointments[appointmentItem];
			if (outlookAppointmentImpl != null)
			{
				ConferenceWrapper conferenceWrapper;
				return AppointmentToConferenceWrappers.TryGetValue(outlookAppointmentImpl, out conferenceWrapper);
			}
			return false;
		}
		public ConferenceWrapper GetConferenceWrapperForAppointment(AppointmentItem appointmentItem, out OutlookAppointmentImpl outlookAppointmentImpl)
		{
			ConferenceWrapper conferenceWrapper;
			outlookAppointmentImpl = Appointments[appointmentItem];
			if (outlookAppointmentImpl == null)
			{
				outlookAppointmentImpl = new OutlookAppointmentImpl(appointmentItem);
				Appointments.Add(outlookAppointmentImpl);
			}
			if (!AppointmentToConferenceWrappers.TryGetValue(outlookAppointmentImpl, out conferenceWrapper))
			{
				conferenceWrapper = new ConferenceWrapper(outlookAppointmentImpl, MyVrmService.Service);
				AppointmentToConferenceWrappers.Add(outlookAppointmentImpl, conferenceWrapper);
			}
			appointmentItem.BeforeDelete += AppointmentItemBeforeDelete;
			return conferenceWrapper;
		}

		private void ThisAddIn_Startup(object sender, EventArgs e)
		{
			try
			{
                MyVrmAddin.TraceSource.TraceInformation("Add-in is starting up...");
				var thisAssembly = Assembly.GetExecutingAssembly();
				foreach (Attribute attribute in thisAssembly.GetCustomAttributes(true))
				{
					if (attribute is AssemblyFileVersionAttribute)
					{
						MyVrmAddin.ProductVersion = ((AssemblyFileVersionAttribute)attribute).Version;
					}
					else if (attribute is AssemblyProductAttribute)
					{
						MyVrmAddin.ProductName = ((AssemblyProductAttribute)attribute).Product;
					}
					else if (attribute is AssemblyCompanyAttribute)
					{
						MyVrmAddin.CompanyName = ((AssemblyCompanyAttribute)attribute).Company;
					}
					else if (attribute is AssemblyCopyrightAttribute)
					{
						MyVrmAddin.Copyright = ((AssemblyCopyrightAttribute)attribute).Copyright;
					}
				}
                MyVrmAddin.TraceSource.TraceInformation("OS version: {0}", Environment.OSVersion.ToString());
                MyVrmAddin.TraceSource.TraceInformation("Local timezone: {0}", TimeZoneInfo.Local.StandardName);
                MyVrmAddin.TraceSource.TraceInformation("OS language: {0}", CultureInfo.InstalledUICulture.LCID);
                MyVrmAddin.TraceSource.TraceInformation(".NET Framework version: {0}", Environment.Version.ToString());
                MyVrmAddin.TraceSource.TraceInformation("Outlook version: {0}", Application.Version);
                MyVrmAddin.TraceSource.TraceInformation("Add-in: {0}", MyVrmAddin.ProductName);
                MyVrmAddin.TraceSource.TraceInformation("Add-in version: {0}", MyVrmAddin.ProductVersion);

                MyVrmAddin.ProductStorageVersion = "1.0";
				MyVrmService.Service.UtcEnabled = 1; //??
				MyVrmService.TraceSource = MyVrmAddin.TraceSource;
				// Outlook Generic client
				MyVrmService.Service.ClientType = "02";
				// Client version
				MyVrmService.Service.ClientVersion = ClientVersion.Parse(MyVrmAddin.ProductVersion);
				
				UIService.DefaultCaption = MyVrmAddin.ProductDisplayName;
				OfficeSkins.Register();

				_defaultLookAndFeel = new DefaultLookAndFeel();
				_defaultLookAndFeel.LookAndFeel.Style = LookAndFeelStyle.Style3D;
				_defaultLookAndFeel.LookAndFeel.UseWindowsXPTheme = true;

				// Display EULA dialog if neccessary
				if (!MyVrmAddin.Instance.IsLicenseAgreementAccepted)
				{
					using (var eulaDlg = new LicenseAgreementDialog())
					{
						eulaDlg.Text = MyVrmAddin.ProductDisplayName;//ProductName;
						eulaDlg.EulaText = MyVrmAddin.Instance.DefaultLicenseAgreement;
						if (eulaDlg.ShowDialog() == DialogResult.Cancel)
						{
							return;
						}
					}
					MyVrmAddin.Instance.AcceptLicenseAgreement();
				}
				Application.OptionsPagesAdd += ApplicationOptionsPagesAdd;
				Application.ItemSend += ApplicationItemSend;
				_inspectors = Application.Inspectors;
				_inspectors.NewInspector += OnNewInspector;
				_currentExplorer = Application.ActiveExplorer();
				_currentExplorer.SelectionChange += OnExplorerSelectionChange;
				_openExplorers = Application.Explorers;
				_openExplorers.NewExplorer += openExplorers_NewExplorer;

				((ExplorerEvents_Event)_currentExplorer).Deactivate/*Close*/ += OnClose;
				Add_myVrmCommandBar();
				MyVrmAddin.Instance.Settings.Reload();
			}
			catch (Exception exception)
			{
				MyVrmAddin.TraceSource.TraceException(exception);
				UIHelper.ShowError(exception.Message);
			}
		}

		void openExplorers_NewExplorer(Explorer Explorer)
		{
			Explorer.Activate();
			((ExplorerEvents_Event)Explorer).Deactivate/*Close*/ += OnClose;
			Add_myVrmCommandBar();
		}

		/**/
		//Tool bar items' tags:
		private const string FavoritesButtonTag = "Favorite Rooms";
		private const string ConferenceButtonTag = "Conference";
		private const string RoomCalendarButtonTag = "Rooms Calendar";
		private const string ApprovalPendingButtonTag = "Conference Approval";
		private const string OptionsButtonTag = "Options";

		private void Add_myVrmCommandBar()
		{
			try
			{
				int _isVisible = 0;
				bool bValueIsExist = false;

				//Do nothing for Outlook 2010
				if (Application.Version.StartsWith("14."))
					return;

				RegistryKey myVRMKey = Registry.CurrentUser.OpenSubKey(myVRMRegPath);
				if (myVRMKey != null)
				{
					string[] values = myVRMKey.GetValueNames();
					bValueIsExist = values.Contains(myVRMToolbarVal);
				}
				if (bValueIsExist) //if key and value exist use them
				{
					object val = myVRMKey.GetValue(myVRMToolbarVal, 0);
					int.TryParse(val.ToString(), out _isVisible);
				}
				else //otherwice there is the first run - show toolbar
				{
					_isVisible = 1;
				}

				if (myVRMKey != null)
					myVRMKey.Close();

				CommandBarButton fbn = (CommandBarButton)Application.ActiveExplorer().CommandBars.FindControl(missing,
																							  missing, FavoritesButtonTag, true);
				CommandBar myVrmCommandBar = fbn != null ? fbn.Parent : null;

				if (myVrmCommandBar == null)
				{
					CommandBars cmdBars = Application.ActiveExplorer().CommandBars;
					myVrmCommandBar = cmdBars.Add(MyVrmAddin.ProductDisplayName, MsoBarPosition.msoBarTop, false, true);
					myVrmCommandBar.Visible = true;
					myVrmCommandBar.Enabled = true;
				}

				_myVrmCommandBar = myVrmCommandBar;
				_myVrmCommandBar.Visible = _isVisible > 0;
				//Create Conference button
				if (myVrmCommandBar.FindControl(missing, missing, ConferenceButtonTag, true, missing) == null)
				{
					_conferenceButton = (CommandBarButton)myVrmCommandBar.Controls.Add(
															MsoControlType.msoControlButton, missing, missing, missing, missing);
					_conferenceButton.Style = MsoButtonStyle.msoButtonIconAndCaption;
					_conferenceButton.Caption = Strings.NewConference;
					_conferenceButton.Picture = MyVrm.Outlook.WinForms.AxHost.GetIPictureDispFromPicture((Image) MyVrmAddin.Logo16);//Resources.myVRMConference);
					_conferenceButton.Mask = MyVrm.Outlook.WinForms.AxHost.GetMaskImage((Image)MyVrmAddin.Logo16);//Resources.myVRMConference);
					_conferenceButton.Tag = ConferenceButtonTag; //=ID
					_conferenceButton.Click += _conferenceButton_Click;
				}
				//Create from templates
				if (myVrmCommandBar.FindControl(missing, missing, "filltemplate", true, missing) == null)
				{
					_fillTemplateCombo = (CommandBarButton)myVrmCommandBar.Controls.Add(
																MsoControlType.msoControlButton, missing, missing, missing, missing);
					_fillTemplateCombo.Style = MsoButtonStyle.msoButtonCaption;
					_fillTemplateCombo.Caption = Strings.UpdateTemplateListMenuPoint;
					_fillTemplateCombo.Tag = "filltemplate"; //=ID
					_fillTemplateCombo.Click += _fillTemplateCombo_Click;
					_fillTemplateCombo.BeginGroup = true;
					_fillTemplateCombo.TooltipText = Strings.UpdateTemplateListMenuPointToolTip;
					_fillTemplateCombo.BeginGroup = true;
				}

				if (myVrmCommandBar.FindControl(missing, missing, "templateCombo", true, missing) == null)
				{
					_templateCombo = (CommandBarComboBox)myVrmCommandBar.Controls.Add(
						MsoControlType.msoControlComboBox, missing, missing, missing, missing);
					_templateCombo.Tag = "templateCombo";
					_templateCombo.Visible = true;
					_templateCombo.Change += _templateCombo_Change;
					_templateCombo.Width = 200;
					_templateCombo.Enabled = false;
					_templateCombo.Caption = Strings.TemplateListMenuPointToolTip;
				}
				//Approve conferences
				if (myVrmCommandBar.FindControl(missing, missing, ApprovalPendingButtonTag, true, missing) == null)
				{
					_approvalPendingButton = (CommandBarButton)myVrmCommandBar.Controls.Add(
															MsoControlType.msoControlButton, missing, missing, missing, missing);
					_approvalPendingButton.Style = MsoButtonStyle.msoButtonIconAndCaption;
					_approvalPendingButton.Caption = Strings.ConferenceApproval;
					_approvalPendingButton.Picture = MyVrm.Outlook.WinForms.AxHost.GetIPictureDispFromPicture(Resources.approveConf);//myVRMConference);
					_approvalPendingButton.Mask = MyVrm.Outlook.WinForms.AxHost.GetMaskImage(Resources.approveConf);//myVRMConference);
					_approvalPendingButton.Tag = ApprovalPendingButtonTag; //=ID
					_approvalPendingButton.Click += _approvalPendingButton_Click;
					_approvalPendingButton.BeginGroup = true;
				}
				//Room calendar
				if (myVrmCommandBar.FindControl(missing, missing, RoomCalendarButtonTag, true, missing) == null)
				{
					_roomCalendarButton = (CommandBarButton)myVrmCommandBar.Controls.Add(
															MsoControlType.msoControlButton, missing, missing, missing, missing);
					_roomCalendarButton.Style = MsoButtonStyle.msoButtonIconAndCaption;
					_roomCalendarButton.Caption = Strings.ShowRoomsCalendarButtonCaption;
					_roomCalendarButton.Picture = MyVrm.Outlook.WinForms.AxHost.GetIPictureDispFromPicture(Resources.RoomsCalendar);
					_roomCalendarButton.Mask = MyVrm.Outlook.WinForms.AxHost.GetMaskImage(Resources.RoomsCalendar);
					_roomCalendarButton.Tag = RoomCalendarButtonTag; //=ID
					_roomCalendarButton.Click += ConferenceCommandBar.ShowRoomsCalendarButtonClick;// _roomCalendarButton_Click;
				}
				//Favorite rooms
				if (fbn == null)
				{
					_favoritesButton = (CommandBarButton)myVrmCommandBar.Controls.Add(
															MsoControlType.msoControlButton, missing, missing, missing, missing);
					_favoritesButton.Style = MsoButtonStyle.msoButtonIconAndCaption;
					_favoritesButton.Caption = Strings.FavoriteRoomsButtonCaption;
					_favoritesButton.Picture = MyVrm.Outlook.WinForms.AxHost.GetIPictureDispFromPicture(Resources.FavoriteRooms);
					_favoritesButton.Mask = MyVrm.Outlook.WinForms.AxHost.GetMaskImage(Resources.FavoriteRooms);
					_favoritesButton.Tag = FavoritesButtonTag; //=ID
					_favoritesButton.Click += ConferenceCommandBar.FavoriteRoomsButtonClick;//_favoriteRoomsButton_Click;
				}
				//Create Options button
				if (myVrmCommandBar.FindControl(missing, missing, OptionsButtonTag, true, missing) == null)
				{
					_optionsButton = (CommandBarButton)myVrmCommandBar.Controls.Add(
															MsoControlType.msoControlButton, missing, missing, missing, missing);
					_optionsButton.Style = MsoButtonStyle.msoButtonIconAndCaption;
					_optionsButton.Caption = Strings.OptionsMenuPoint;
					_optionsButton.Picture = MyVrm.Outlook.WinForms.AxHost.GetIPictureDispFromPicture(Resources.Options);
					_optionsButton.Mask = MyVrm.Outlook.WinForms.AxHost.GetMaskImage(Resources.Options);
					_optionsButton.Tag = OptionsButtonTag; //=ID
					_optionsButton.Click += OptionsButtonClick;
				}
			}
			catch (Exception exception)
			{
				MyVrmAddin.TraceSource.TraceException(exception);
			}
		}

		private void _conferenceButton_Click(CommandBarButton ctrl, ref bool cancel)
		{
			var cursor = Cursor.Current;
			Cursor.Current = Cursors.WaitCursor;

			try
			{
				AppointmentItem appItem = (AppointmentItem)Application.CreateItem(OlItemType.olAppointmentItem);
				appItem.Display(missing);

				var inspectorWrapper = _wrappedInspectors.ElementAt(_wrappedInspectors.Count - 1).Value;
				Globals.ThisAddIn.ShowMyVRMForm((AppointmentInspectorWrapper)inspectorWrapper);
				inspectorWrapper.Inspector.SetCurrentFormPage(Strings.ConferencePageCaption);
			}
			catch (Exception exception)
			{
				MyVrmAddin.TraceSource.TraceException(exception);
				UIHelper.ShowError(exception.Message);
			}
			finally
			{
				Cursor.Current = cursor;
			}
		}

		private void _fillTemplateCombo_Click(CommandBarButton ctrl, ref bool cancel)
		{
			_templateCombo.Clear();
			_templateCombo.Enabled = false;
			GetTemplateListResponse ret = null;
			_templateList.Clear();

			var cursor = Cursor.Current;
			Cursor.Current = Cursors.WaitCursor;

			//Get data
			try
			{
				ret = MyVrmService.Service.GetTemplateList(GetTemplateListResponse.SortByType.ByName);
			}
			catch (Exception exception)
			{
				MyVrmAddin.TraceSource.TraceException(exception, true);
				UIHelper.ShowMessage(exception.Message, MessageBoxButtons.OK,
									 MessageBoxIcon.Exclamation);
			}
			finally
			{
				Cursor.Current = cursor;
			}
			if (ret != null && ret.templateList != null)
			{
				_templateList = ret.templateList;
				foreach (TemplateInfo templateInfo in ret.templateList)
				{
					_templateCombo.AddItem(templateInfo.Name, missing);
				}
				_templateCombo.Enabled = true;
				//_templateCombo.Execute();

				_templateCombo.SetFocus();
				SendKeys.Send("+({DOWN})");
			}
		}

		void _templateCombo_Change(CommandBarComboBox Ctrl)
		{
			int selected = _templateList.FindIndex(0, tempalte => tempalte.Name == Ctrl.Text);
			if (selected >= 0)
			{

				GetTemplateResponse response = null;
				var cursor = Cursor.Current;
				Cursor.Current = Cursors.WaitCursor;

				try
				{
					response = MyVrmService.Service.GetTemplate(int.Parse(_templateList[selected].ID));
				}
				catch (Exception exception)
				{
					MyVrmAddin.TraceSource.TraceException(exception, true);
					UIHelper.ShowMessage(exception.Message, MessageBoxButtons.OK,
										 MessageBoxIcon.Exclamation);
				}
				finally
				{
					Cursor.Current = cursor;
				}

				if (response != null)
				{
					OutlookAppointmentImpl apptImpl;

					AppointmentItem appItem = (AppointmentItem)Application.CreateItem(OlItemType.olAppointmentItem);

					var conferenceWrapper = Globals.ThisAddIn.GetConferenceWrapperForAppointment(appItem, out apptImpl);

					Conference template = response.Conference;
					conferenceWrapper.Host = MyVrmService.Service.UserId;
					conferenceWrapper.Conference.Name = template.Name;
					//+ Outlook assign 
					conferenceWrapper.Appointment.Subject = template.Name;

					conferenceWrapper.Conference.Password = template.Password;

					//Fake assign indeed
					conferenceWrapper.Conference.Description = template.Description;
					//Real assign
					conferenceWrapper.Appointment.Body = template.Description;

					conferenceWrapper.Conference.Duration = template.Duration;
					//+ Outlook assign 
					conferenceWrapper.Appointment.Duration = template.Duration;

					//conferenceWrapper.template.Type = template.Type;

					//Fake assign indeed
					conferenceWrapper.Conference.Rooms.Clear();
					conferenceWrapper.Conference.Rooms.AddRange(template.Rooms);
					//Real assign
					conferenceWrapper.LocationIds.Clear();
					conferenceWrapper.LocationIds.AddRange(template.Rooms);

					//Assign participants
					conferenceWrapper.Participants.Clear();
					//Outlook assign - for mail notification sending
					var participantsEmialList = template.Participants.Select(participant => participant.Email).ToList();
					conferenceWrapper.Appointment.SetRecipients(participantsEmialList);

					conferenceWrapper.Conference.IsPublic = template.IsPublic;

					appItem.Display(missing);
				}
			}
		}

		private void _approvalPendingButton_Click(CommandBarButton ctrl, ref bool cancel)
		{
			List<ConfInfo> FoundConfs = new List<ConfInfo>();
			ConferenceApprovalDialog.GetConfsForApprove(out FoundConfs);

			if (FoundConfs.Count > 0)
			{
				//Run approval dlg
				using (var dlg = new ConferenceApprovalDialog())
				{
					dlg.FoundConfs = FoundConfs;
					dlg.ShowDialog();
				}
			}
			else
			{
				UIHelper.ShowMessage(Strings.NoConferencesToApproveMsg, MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void OptionsButtonClick(CommandBarButton ctrl, ref bool cancel)
		{
			using (var optDlg = new OptionsAsDlg())
			{
				CancelEventArgs e = new CancelEventArgs();
				if (optDlg.ShowDialog() == DialogResult.OK)
					optDlg.optionsControl.External_OnApplying(e);
			}
		}

		private void OnClose()
		{
			try
			{
				bool bVisible = false;
				CommandBarButton fbn = (CommandBarButton)Application.ActiveExplorer().CommandBars.FindControl(missing,
																							  missing, FavoritesButtonTag, true);
				CommandBar myVrmCommandBar = fbn != null ? fbn.Parent : null;
				if (myVrmCommandBar != null)
				{
					bVisible = myVrmCommandBar.Visible;
				}
				Registry.SetValue(myVRMRegFullPath, myVRMToolbarVal, bVisible ? 1 : 0);

			}
			catch (Exception e)
			{
				MyVrmAddin.TraceSource.TraceException(e);
				UIHelper.ShowError(e.Message);
			}
		}
		/**/
		private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
		{
			try
			{
				MyVrmAddin.TraceSource.TraceInformation("Add-in is shutting down...");
				Application.OptionsPagesAdd -= ApplicationOptionsPagesAdd;
				Application.ItemSend -= ApplicationItemSend;
				_inspectors.NewInspector -= OnNewInspector;
				_openExplorers.NewExplorer -= openExplorers_NewExplorer;
				((ExplorerEvents_Event)_currentExplorer).Deactivate/*Close*/ -= OnClose;
				_currentExplorer.SelectionChange -= OnExplorerSelectionChange;
				foreach (var appointmentImpl in Appointments.Where(appointmentImpl => appointmentImpl != null))
				{
					appointmentImpl.Item.BeforeDelete -= AppointmentItemBeforeDelete;
					appointmentImpl.Dispose();
				}
				Appointments.Clear();
			}
			catch (Exception exception)
			{
				MyVrmAddin.TraceSource.TraceException(exception);
			}
		}

		private void ApplicationItemSend(object item, ref bool cancel)
		{
			AppointmentItem apptItem = null;
			try
			{
				if (item is MeetingItem)
				{
					var meetingItem = (MeetingItem)item;
					apptItem = meetingItem.GetAssociatedAppointment(false);
					// Ignore cancelled meetings
					if (apptItem.MeetingStatus == OlMeetingStatus.olMeetingCanceled)
					{
						return;
					}
					var conferenceId = OutlookAppointmentImpl.GetConferenceId(apptItem);
					if (conferenceId != null)
					{
						var appointmentItem = Appointments[conferenceId];
						if (appointmentItem != null)
						{
							var conferenceWrapper = AppointmentToConferenceWrappers[appointmentItem];
							conferenceWrapper.Conference.UpdateCalendarUniqueId(appointmentItem.GlobalAppointmentId);
							if (UIHelper.AppendConferenceDetailsToMeeting)
							{
								var conference = conferenceWrapper.Conference;
								if (conference != null)
								{
									var body = meetingItem.Body;
									var sb = new StringBuilder(body);
									sb.AppendLine();
									sb.AppendLine();
									sb.AppendLine("You have been invited to a meeting with the following information");
									sb.AppendLine();
									sb.AppendFormat("Conference Name: {0}", conference.Name);
									sb.AppendLine();
									sb.AppendFormat("Unique ID: {0}", conference.UniqueId);
									sb.AppendLine();
									sb.AppendFormat("Date and Time: {0} {1}", conference.StartDate.ToString("g"),
													conference.TimeZone.DisplayName);
									sb.AppendLine();
									sb.AppendFormat("Duration: {0}", conference.Duration);
									var hostProfile = MyVrmService.Service.GetUser(conference.HostId);
									if (hostProfile != null)
									{
										sb.AppendLine();
										sb.AppendFormat("Host: {0} ({1})", hostProfile.UserName, hostProfile.Email);
									}
									var roomNames = new string[conference.Rooms.Count];
									for (var i = 0; i < conference.Rooms.Count; i++)
									{
										var roomId = conference.Rooms[i];
										var roomProfile = MyVrmAddin.Instance.GetRoomFromCache(roomId);
										if (roomProfile != null)
										{
											roomNames[i] = roomProfile.Name;
										}
									}
									sb.AppendLine();
									sb.AppendFormat("Location(s): {0}", string.Join("; ", roomNames));
									meetingItem.Body = sb.ToString();
									meetingItem.Save();
								}
							}
						}
					}
				}
			}
			catch (Exception e)
			{
				MyVrmAddin.TraceSource.TraceException(e);
				UIHelper.ShowError(e.Message);
				cancel = true;
			}
			finally
			{
				if (apptItem != null)
				{
					Marshal.ReleaseComObject(apptItem);
				}
			}
		}


        private void OnNewInspector(Inspector inspector)
        {
            try
            {
                if (!MyVrmAddin.Instance.IsLicenseAgreementAccepted)
                    return;
                var item = inspector.CurrentItem as AppointmentItem;
                if (item != null && (item.MeetingStatus != OlMeetingStatus.olMeetingReceived && item.MeetingStatus != OlMeetingStatus.olMeetingCanceled))
                {

					var inspectorWrapper = (AppointmentInspectorWrapper)InspectorWrapper.GetWrapperFor(inspector);
					if (inspectorWrapper != null)
					{
						inspectorWrapper.Closed += InspectorWrapperClose;
						_wrappedInspectors.Add(inspectorWrapper.Id, inspectorWrapper);
						var commandBar = new ConferenceCommandBar(inspectorWrapper);
						if (OutlookAppointmentImpl.GetConferenceId(item) != null)
						{
							ShowMyVRMForm(inspectorWrapper);
						}
						CommandBarManager.RegisterCommandBar(commandBar, inspector);
						commandBar.IsEnableConferenceEnabled = OutlookAppointmentImpl.GetConferenceId(item) == null;
					}
				}
			}
			catch (Exception e)
			{
				MyVrmAddin.TraceSource.TraceException(e);
				UIHelper.ShowError(e.Message);
			}
		}

		internal void ShowMyVRMForm(AppointmentInspectorWrapper inspectorWrapper)
		{
			OutlookAppointmentImpl outlookAppointmentImpl;
			var conferenceWrapper = GetConferenceWrapperForAppointment(inspectorWrapper.Item, out outlookAppointmentImpl);
			_appointmentToWrappers.Add(outlookAppointmentImpl, inspectorWrapper);
			var conferencePage = new ConferencePage();
			CustomFormManager.CreateCustomForm(conferencePage, inspectorWrapper.Inspector);
			conferencePage.Conference = conferenceWrapper;
		}

		private void InspectorWrapperClose(object sender, EventArgs e)
		{
			try
			{
				var wrapper = (InspectorWrapper)sender;
				//Unsubscribe from appointment Saving event
				OutlookAppointmentImpl appointmentImpl = (from appointmentToWrapper in _appointmentToWrappers
														  where appointmentToWrapper.Value == wrapper
														  select appointmentToWrapper.Key).FirstOrDefault();
				if (appointmentImpl != null)
				{
					_appointmentToWrappers.Remove(appointmentImpl);
					AppointmentToConferenceWrappers.Remove(appointmentImpl);
					appointmentImpl.Item.BeforeDelete -= AppointmentItemBeforeDelete;
					appointmentImpl.Dispose();
				}
				//unsubscribe from inspector Close event
				wrapper.Closed -= InspectorWrapperClose;
				// remove from opened inspector collection
				_wrappedInspectors.Remove(wrapper.Id);
				// remove from appointments collection
				Appointments.Remove(appointmentImpl);
			}
			catch (Exception exception)
			{
				MyVrmAddin.TraceSource.TraceException(exception);
				UIHelper.ShowError(exception.Message);
			}
		}


		private void OnExplorerSelectionChange()
		{
			try
			{
				_selectedAppointments.ForEach(item => item.BeforeDelete -= AppointmentItemBeforeDelete);
				_selectedAppointments.Clear();
				if (Application.ActiveExplorer() != null)
				{
					_selectedAppointments = Application.ActiveExplorer().Selection.AppointmentItems().ToList();
					_selectedAppointments.ForEach(item => item.BeforeDelete += AppointmentItemBeforeDelete);
				}
			}
			catch (Exception e)
			{
				MyVrmAddin.TraceSource.TraceException(e);
				UIHelper.ShowError(e.Message);
			}
		}


		void AppointmentItemBeforeDelete(object item, ref bool cancel)
		{
			var appointmentItem = item as AppointmentItem;
			if (appointmentItem == null)
				return;
			using (var appointmentImpl = Globals.ThisAddIn.Appointments[appointmentItem] ??
								  new OutlookAppointmentImpl(appointmentItem))
			{
				try
				{
					if (!appointmentImpl.LinkedWithConference)
						return;
					var conferenceId = appointmentImpl.ConferenceId;
					if (conferenceId != null)
					{
						Conference conference = null;
						switch (appointmentItem.RecurrenceState)
						{
							// Delete the occurrence if the occurrence or exception of recurring appointment is being deleted
							case OlRecurrenceState.olApptOccurrence:
							case OlRecurrenceState.olApptException:
								{
									conference = Conference.Bind(MyVrmService.Service, conferenceId, appointmentImpl.Start,
										appointmentImpl.StartInStartTimeZone, appointmentImpl.EndInEndTimeZone);
									conference.DeleteOccurrance(appointmentImpl.Start);
									break;
								}
							// Delete the conference if master or not recurring appointment is being deleted
							case OlRecurrenceState.olApptNotRecurring:
							case OlRecurrenceState.olApptMaster:
								{
									conference = Conference.Bind(MyVrmService.Service, conferenceId);
									conference.Delete();
									break;
								}

						}
						appointmentImpl.UnlinkConference();
						Appointments.Remove(appointmentImpl);
					}
				}
				catch (Exception e)
				{
					MyVrmAddin.TraceSource.TraceException(e);
					UIHelper.ShowError(e.Message);
				}
			}
		}

		private void ApplicationOptionsPagesAdd(PropertyPages pages)
		{
			try
			{
				var ctrl2 = new OptionsPage();
				pages.Add(ctrl2, Strings.SchedulerOptionsPageTitle);
			}
			catch (Exception e)
			{
				MyVrmAddin.TraceSource.TraceException(e);
				UIHelper.ShowError(e.Message);
			}
		}

		#region VSTO generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InternalStartup()
		{
			Startup += ThisAddIn_Startup;
			Shutdown += ThisAddIn_Shutdown;
		}

		#endregion
	}
	internal sealed class ThisCustomFormCollection : CustomFormReadOnlyCollection
	{

		public ThisCustomFormCollection(IList<CustomFormControl> list) :
			base(list)
		{
		}


		internal WindowCustomFormCollection this[Inspector inspector]
		{
			get
			{
				return Globals.ThisAddIn.GetCustomForms<WindowCustomFormCollection>(inspector);
			}
		}
	}

	internal sealed partial class WindowCustomFormCollection : CustomFormReadOnlyCollection
	{
	}

	internal sealed partial class CommandBarCollection : CommandBarReadOnlyCollection
	{
	}

	internal sealed class ThisCommandBarCollection : CommandBarReadOnlyCollection
	{
		public ThisCommandBarCollection(IList<OfficeCommandBar> commandBars)
			: base(commandBars)
		{

		}

		internal CommandBarCollection this[Inspector inspector]
		{
			get { return Globals.ThisAddIn.GetCommandBars<CommandBarCollection>(inspector); }
		}
	}

	partial class Globals
	{
		private static ThisCustomFormCollection _thisCustomForms;
		private static ThisCommandBarCollection _thisCommandBars;

		internal static ThisCustomFormCollection CustomForms
		{
			get { return _thisCustomForms ?? (_thisCustomForms = new ThisCustomFormCollection(ThisAddIn.GetCustomForms())); }
		}

		internal static ThisCommandBarCollection CommandBars
		{
			get { return _thisCommandBars ?? (_thisCommandBars = new ThisCommandBarCollection(ThisAddIn.GetCommandBars())); }
		}
	}
}

