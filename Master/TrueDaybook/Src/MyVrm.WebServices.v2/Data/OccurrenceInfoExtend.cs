﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	public class OccurrenceInfoExtend : OccurrenceInfo
	{
		private bool _Deleted;
		private DateTime? _origStart;

		internal OccurrenceInfoExtend()
			: this(DateTime.MinValue, DateTime.MinValue, DateTime.MinValue)
		{
			_Deleted = false;
		}

		public bool IsDeleted{ get { return _Deleted; } set { _Deleted = value; }
		}
		public DateTime OrigStart
		{
			get
			{
				if (_origStart == null)
				{
					_origStart = Start; //??
				}
				return _origStart.Value;
			}
			set { _origStart = value; }
		}

		public OccurrenceInfoExtend(DateTime origStartTime, DateTime currStartTime, DateTime currEndTime)
		{
			_origStart = origStartTime;
			Start = currStartTime;
			End = currEndTime;
            SetupDuration = TimeSpan.Zero;
            TeardownDuration = TimeSpan.Zero;
        }
	}
}
