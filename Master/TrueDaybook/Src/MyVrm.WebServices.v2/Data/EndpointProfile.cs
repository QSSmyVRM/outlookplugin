﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using MyVrm.Common;

namespace MyVrm.WebServices.Data
{
    public enum AddressType
    {
        [LocalizedDescription("AddressTypeIpAddress", typeof(Strings))]
        IpAddress = 1,
        [LocalizedDescription("AddressTypeH323Id", typeof(Strings))]
        H323Id = 2,
        [LocalizedDescription("AddressTypeE164", typeof(Strings))]
        E164 = 3,
        [LocalizedDescription("AddressTypeISDNPhoneNumber", typeof(Strings))]
        IsdnPhoneNumber = 4,
        MPI = 5
    }

    public enum ConnectionType
    {
        [LocalizedDescription("ConnectionTypeDialinToMCU", typeof(Strings))]
        DialInToMCU = 1,
        [LocalizedDescription("ConnectionTypeDialoutFromMCU", typeof(Strings))]
        DialOutFromMCU = 2,
        [LocalizedDescription("ConnectionTypeDirectToMCU", typeof(Strings))]
        DirectToMCU = 3
    }
    
    public class EndpointProfile : ComplexProperty
    {
        public EndpointProfileId Id { get; private set; }

        public EndpointProfile()
        {
            McuAddressType = -1;    
        }

		public override object Clone()
		{
			EndpointProfile copy = new EndpointProfile();
			copy.Address = string.Copy(Address);
			copy.AddressType = AddressType;
			copy.BridgeId = new BridgeId(BridgeId.Id);
			copy.ConnectionType = ConnectionType;
			copy.DefaultProtocol = DefaultProtocol;
			copy.Deleted = Deleted;
			copy.EncryptionPreferred = EncryptionPreferred;
			copy.ExchangeId = string.Copy(ExchangeId);
			copy.Id = (EndpointProfileId)Id.Clone();
			copy.IsDefault = IsDefault;
			copy.IsOutside = IsOutside;
			copy.LineRate = LineRate;
			copy.McuAddress = string.Copy(McuAddress);
			copy.McuAddressType = McuAddressType;
			copy.Name = string.Copy(Name);
			copy.Password = string.Copy(Password);
			copy.Url = string.Copy(Url);
			copy.TelnetApi = TelnetApi;
			copy.VideoEquipment = VideoEquipment;

			return copy;
		}

		public EndpointProfile( EndpointProfile value, bool UseDefaultID)
		{
			this.Address = value.Address;
			this.AddressType = value.AddressType;
			this.BridgeId = value.BridgeId;
			this.ConnectionType = value.ConnectionType;
			this.DefaultProtocol = value.DefaultProtocol;
			this.Deleted = value.Deleted;
			this.EncryptionPreferred = value.EncryptionPreferred;
			this.ExchangeId = value.ExchangeId;
			this.IsDefault = value.IsDefault;
			this.IsOutside = value.IsOutside;
			this.LineRate = value.LineRate;
			this.McuAddress = value.McuAddress;
			this.McuAddressType = value.McuAddressType;
			this.Name = value.Name;
			this.Namespace = value.Namespace;
			this.Password = value.Password;
			this.TelnetApi = value.TelnetApi;
			this.Url = value.Url;
			this.VideoEquipment = value.VideoEquipment;
			this.Id = UseDefaultID ? EndpointProfileId.Default : new EndpointProfileId(value.Id.Id);
		}

        public string Name { get; set; }
        public bool Deleted { get; set; }
        public bool IsDefault { get; set; }
        public bool EncryptionPreferred { get; set; }
        public AddressType AddressType { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }
        public string Url { get; set; }
        public bool IsOutside { get; set; }
        public int VideoEquipment { get; set; }
        public int LineRate { get; set; }
        public BridgeId BridgeId { get; set; }
        public ConnectionType ConnectionType { get; set; }
        public int DefaultProtocol { get; set; }
        public string McuAddress { get; set; }
        public int McuAddressType { get; set; }
        public bool TelnetApi { get; set; }
        public string ExchangeId { get; set; }

        public override string ToString()
        {
            if (Name != null)
            {
                return Name;
            }
            return base.ToString();
        }

        internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
        {
            switch (reader.LocalName)
            {
                case "ProfileID":
                {
                    Id = new EndpointProfileId();
                    Id.LoadFromXml(reader, Id.GetXmlElementName());
                    return true;
                }
                case "ProfileName":
                {
                    Name = reader.ReadElementValue();
                    return true;
                }
                case "AddressType":
                {
                    AddressType = reader.ReadElementValue<AddressType>();
                    return true;
                }
                case "Password":
                {
                    Password = reader.ReadElementValue();
                    return true;
                }
                case "Address":
                {
                    Address = reader.ReadElementValue();
                    return true;
                }
                case "URL":
                {
                    Url = reader.ReadElementValue();
                    return true;
                }
                case "IsOutside":
                {
                    IsOutside = Utilities.BoolStringToBool(reader.ReadElementValue());
                    return true;
                }
                case "VideoEquipment":
                {
                    VideoEquipment = reader.ReadElementValue<int>();
                    return true;
                }
                case "LineRate":
                {
                    LineRate = reader.ReadElementValue<int>();
                    return true;
                }
                case "Bridge":
                {
                    BridgeId = new BridgeId();
                    BridgeId.LoadFromXml(reader, "Bridge");
                    return true;
                }
                case "ConnectionType":
                {
                    ConnectionType = reader.ReadElementValue<ConnectionType>();
                    return true;
                }
                case "DefaultProtocol":
                {
                    DefaultProtocol = reader.ReadElementValue<int>();
                    return true;
                }
                case "MCUAddress":
                {
                    McuAddress = reader.ReadElementValue();
                    return true;
                }
                case "MCUAddressType":
                {
                    McuAddressType = reader.ReadElementValue<int>();
                    return true;
                }
                case "ExchangeID":
                {
                    ExchangeId = reader.ReadElementValue();
                    return true;
                }
                case "TelnetAPI":
                {
                    TelnetApi = Utilities.BoolStringToBool(reader.ReadElementValue());
                    return true;
                }
            }
            return false;
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            base.WriteElementsToXml(writer);
            if (Id != null)
            {
                Id.WriteToXml(writer);    
            }
            else
            {
                EndpointProfileId.NewId.WriteToXml(writer);
            }
            if (Name != null)
            {
                writer.WriteElementValue(XmlNamespace.NotSpecified, "ProfileName", Name);
            }
            writer.WriteElementValue(XmlNamespace.NotSpecified, "Deleted", Deleted);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "Default", IsDefault);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "EncryptionPreferred", EncryptionPreferred);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "AddressType", AddressType);
            if (Password != null)
            {
                writer.WriteElementValue(XmlNamespace.NotSpecified, "Password", Password);
            }
            if (Address != null)
            {
                writer.WriteElementValue(XmlNamespace.NotSpecified, "Address", Address);
            }
            if (Url != null)
            {
                writer.WriteElementValue(XmlNamespace.NotSpecified, "URL", Url);
            }
            writer.WriteElementValue(XmlNamespace.NotSpecified, "IsOutside", IsOutside);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "ConnectionType", ConnectionType);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "VideoEquipment", VideoEquipment);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "LineRate", LineRate);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "DefaultProtocol", DefaultProtocol);
            if (BridgeId != null)
            {
                writer.WriteElementValue(XmlNamespace.NotSpecified, "Bridge", BridgeId.Id);
            }
            else
            {
                writer.WriteElementValue(XmlNamespace.NotSpecified, "Bridge", BridgeId.Default.Id);
            }
            if (McuAddress != null)
            {
                writer.WriteElementValue(XmlNamespace.NotSpecified, "MCUAddress", McuAddress);
            }
            writer.WriteElementValue(XmlNamespace.NotSpecified, "MCUAddressType", McuAddressType);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "TelnetAPI", TelnetApi);
            if (ExchangeId != null)
            {
                writer.WriteElementValue(XmlNamespace.NotSpecified, "ExchangeID", ExchangeId);
            }
        }
    }
}
