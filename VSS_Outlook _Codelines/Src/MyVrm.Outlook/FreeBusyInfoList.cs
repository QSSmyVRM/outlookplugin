﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Linq;

namespace MyVrm.Outlook
{
    internal class FreeBusyInfoList : List<FreeBusyInfo>
    {
        internal IList<FreeBusyInfo> GetFreeBusyInfos(DateTime start, DateTime end)
        {
            return this.Where(freeBusyInfo => freeBusyInfo.Start >= start && freeBusyInfo.Start <= end).ToList();
        }

        internal DateTime GetLastDate()
        {
            return this.Max(info => info.Start);
        }

        internal DateTime GetFirstDate()
        {
            return this.Min(info => info.Start);
        }
    }
}
