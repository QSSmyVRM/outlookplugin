﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace MyVrm.WebServices.Data
{
    internal abstract class SetTierRequestBase : ServiceRequestBase<SetTierResponse>
    {
        protected SetTierRequestBase(MyVrmService service) : base(service)
        {
        }

        public Tier Tier { get; set; }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            UserId.WriteToXml(writer, "UserID");
            OrganizationId.WriteToXml(writer);
            Tier.WriteToXmlPlain(writer);
        }

        internal override string GetResponseXmlElementName()
        {
            return "success";
        }
    }

    internal class SetTopTierRequest : SetTierRequestBase
    {
        public SetTopTierRequest(MyVrmService service) : base(service)
        {
        }

        #region Overrides of ServiceRequestBase

        internal override string GetXmlElementName()
        {
            return "SetTier1";
        }

        internal override string GetCommandName()
        {
            return "SetTier1";
        }

        #endregion
    }

    internal class SetMiddleTierRequest : SetTierRequestBase
    {
        public SetMiddleTierRequest(MyVrmService service) : base(service)
        {
        }

        #region Overrides of ServiceRequestBase

        internal override string GetXmlElementName()
        {
            return "SetTier2";
        }

        internal override string GetCommandName()
        {
            return "SetTier2";
        }
        #endregion
    }
}
