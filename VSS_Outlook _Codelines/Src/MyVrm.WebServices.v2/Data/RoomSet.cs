﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    public enum RoomSetType
    {
        Inventory = 1,
        Catering = 2,
        Housekeeping = 3
    }

    /// <summary>
    /// Represents 
    /// </summary>
    public class RoomSet
    {
        internal RoomSet()
        {
        }

        public int Id { get; internal set; }
        public RoomSetType Type { get; internal set; }
        public string Name { get; internal set; }

        internal void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
        {
            do
            {
                reader.Read();
                if (reader.LocalName == "ID")
                {
                    Id = reader.ReadValue<int>();
                }
                else if(reader.LocalName == "Type")
                {
                    Type = reader.ReadValue<RoomSetType>();
                }
                else if (reader.LocalName == "Name")
                {
                    Name = reader.ReadValue();
                }
                else
                {
                    reader.SkipCurrentElement();
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, xmlElementName));
        }

        public override string ToString()
        {
            return string.IsNullOrEmpty(Name) ? base.ToString() : Name;
        }
    }
}
