﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Collections.ObjectModel;
using System.Net;
using System.Security.Principal;
using MyVrm.Common.Diagnostics;
using MyVrm.Common.Security;
using MyVrm.WebServices.Data.Files;

namespace MyVrm.WebServices.Data
{
    public enum AuthenticationMode
    {
        Windows,
        Custom,
        Unknown
    }

    /// <summary>
    /// Represents a binding to the myVRM Web Service.
    /// </summary>
    public class MyVrmService : IDisposable
    {
        private string _url;
        private string _fullUrl;
        private AuthenticationMode _authenticationMode;
        private NetworkCredential _credential;
        private bool _useDefaultCredential;
        private bool _disposed;
        private bool _loggedOn;

        private static readonly MyVrmService Instance;
        private UserId _userId;
        private OrganizationId _organizationId = OrganizationId.Invalid;

    	private readonly object _lockObject = new object();

        private readonly MyVrmServiceOptions _options = new MyVrmServiceOptions();
        
        static MyVrmService()
        {
            Instance = new MyVrmService();
        }

        private MyVrmService()
        {
            AuthenticationMode = AuthenticationMode.Custom;
            UserId = UserId.DefaultUserId;
            _loggedOn = false;
            _disposed = false;
        }

        ~MyVrmService()
        {
            Dispose();
        }
        /// <summary>
        /// Get service instance.
        /// </summary>
        public static MyVrmService Service
        {
            get
            {
                return Instance;
            }
        }
        /// <summary>
        /// Gets or sets Web service URL.
        /// </summary>
        public string Url
        {
            get
            {
                return _url;
            }
            set
            {
                if (string.Compare(_url, value, StringComparison.InvariantCultureIgnoreCase) != 0)
                {
                    Logoff();
                    _url = value;
                    _fullUrl = MakeFullUrl(_url);
                }
            }
        }

        public string FullUrl
        {
            get { return _fullUrl; }
        }
        /// <summary>
        /// Gets logged on user's ID.
        /// </summary>
        public UserId UserId
        {
            get
            {
                LogonCheck();
                return _userId;
            }

            private set
            {
                _userId = value;
            }
        }

		public bool IsAVEnabled { get; set; }
//101456 starts
        public bool enableDesktopVideo { get; set; }
        public bool enableBlueJeans { get; set; }
        public bool enableJabber { get; set; }
        public bool enableLync { get; set; }
        public bool enableVidtel { get; set; }
//101456 ends
		public int UtcEnabled { get; set; }
		public bool IsUtcEnabled { get { return UtcEnabled == 1; } }
        public bool isMessageOverlay { get; set; }// zd 101857
        //ZD 101838
        public bool EnableAdditionalOption { get; set; }
        public bool EnableAVWorkOrder  { get; set; }
        public bool EnableCateringWO  { get; set; }
        public bool EnableFacilityWO { get; set; } //ZD 101838
        public string StartMode { get; set; }//ZD 102638
        
        
    	/// <summary>
        /// Gets current organization.
        /// </summary>
        public OrganizationId OrganizationId
        {
            get { return _organizationId; }
        }

        private OrganizationOptions _organizationOptions;
        /// <summary>
        /// Gets current organization's options.
        /// </summary>
        public OrganizationOptions OrganizationOptions
        {
            get { return _organizationOptions ?? (_organizationOptions = GetOrganizationOptions()); }
        }

        private OrganizationSettingsOld _organizationSettingsOld;

		private OrganizationSettingsNew _organizationSettings;
		public OrganizationSettingsNew OrganizationSettings
		{
			get { return _organizationSettings ?? (_organizationSettings = GetAllOrganizationSettings()); }
		}
		// ->
    	private Dictionary<int, string> _mediaTypes;
    	public Dictionary<int, string> MediaTypes
    	{
			get { return _mediaTypes ?? (_mediaTypes = GetMediaTypes() ); }
    	}

		private Dictionary<int, string> _lineRates;
		public Dictionary<int, string> LineRates
    	{
			get { return _lineRates ?? (_lineRates = GetLineRates()); }
    	}

		private Dictionary<int, string> _videoProtocols;
		public Dictionary<int, string> VideoProtocols
    	{
			get { return _videoProtocols ?? (_videoProtocols = GetVideoProtocols()); }
    	}

		private VideoEquipmentCollection _videoEquipment;
		public VideoEquipmentCollection VideoEquipment
    	{
			get { return _videoEquipment ?? (_videoEquipment = GetVideoEquipment()); }
    	}

		private Dictionary<int, string> _addressTypes;
		public Dictionary<int, string> AddressTypes
    	{
			get { return _addressTypes ?? (_addressTypes = GetAddressTypes()); }
    	}

		private ReadOnlyCollection<BridgeName>  _bridges;
		public ReadOnlyCollection<BridgeName> Bridges
    	{
			get { return _bridges ?? (_bridges = GetBridges()); }
    	}
		// <-
        /// <summary>
        /// Gets current organization's settings.
        /// </summary>
        public OrganizationSettingsOld OrganizationSettingsOld
        {
            get { return _organizationSettingsOld ?? (_organizationSettingsOld = GetOrganizationSettings()); }
        }
        /// <summary>
        /// True if user with <seealso cref="UserId"/> has logged on.
        /// </summary>
        public bool LoggedOn
        {
            get
            {
                return _loggedOn;
            }
        }
        /// <summary>
        /// Gets or sets credentials.
        /// </summary>
        public NetworkCredential Credential
        {
            get { return _credential; }
            set
            {
                if (value != _credential)
                {
                    Logoff();
                    _credential = value;
                }
            }
        }
        /// <summary>
        /// Gets or sets authentication mode.
        /// </summary>
        public AuthenticationMode AuthenticationMode
        {
            get { return _authenticationMode; }
            set
            {
                if (_authenticationMode != value)
                {
                    Logoff();
                    _authenticationMode = value;
                }
            }
        }
        /// <summary>
        /// Gets or sets using current or <seealso cref="Credential"/> credentials.
        /// </summary>
        public bool UseDefaultCredential
        {
            get { return _useDefaultCredential; }
            set
            {
                if (_useDefaultCredential != value)
                {
                    Logoff();
                    Credential = null;
                    _useDefaultCredential = value;
                }
            }
        }
        /// <summary>
        /// Gets or sets client version.
        /// </summary>
        public ClientVersion ClientVersion { get; set; }
        /// <summary>
        /// Get or sets client type (01 - Outlook York, 02 - Outlook Generic, 03 - BMS (based on York), etc.)
        /// </summary>
        public string ClientType { get; set; }
        public static MyVrmTraceSource TraceSource { get; set; }
        /// <summary>
        /// Gets service options.
        /// </summary>
        public MyVrmServiceOptions ServiceOptions
        {
            get { return _options; }
        }

        /// <summary>
        /// All transactions performed will be for the selected organization.
        /// </summary>
        /// <param name="id">Selected organization ID.</param>
        public void SwitchOrganization(OrganizationId id)
        {
            if (id == null)
            {
                throw new ArgumentNullException("id");
            }
            //if (!id.IsValid)
            //{
            //    throw new ArgumentException(Strings.InvalidOrganizationId, "id");
            //}
            lock (_lockObject)
            {
                _organizationOptions = null;
                _organizationSettings = null;
                _organizationId = id;
            }
        }
        /// <summary>
        /// Saves user's profile.
        /// </summary>
        /// <param name="user">User's profile.</param>
        /// <returns>User's ID.</returns>
        public UserId SaveUser(User user)
        {
            var request = new SaveUserRequest(this)
                              {
                                  User = user,
                                  UserId = UserId,
                                  OrganizationId = OrganizationId
                              };
            return request.Execute().UserId;
        }
        /// <summary>
        /// Gets user's profile from the system.
        /// </summary>
        /// <param name="userId">User's ID.</param>
        /// <returns>User's profile.</returns>
        public User GetUser(UserId userId)
        {
            var request = new GetUserRequest(this)
                              {
                                  UserId = UserId,
                                  RequestedUserId = userId
                              };
            return request.Execute().User;
        }
        /// <summary>
        /// Gets profile for current logged on user.
        /// </summary>
        /// <returns>User's profile.</returns>
        public User GetUser()
        {
            return GetUser(UserId);
        }
        /// <summary>
        /// Returns organization's configuration settings.
        /// </summary>
        /// <returns>Configuration settings.</returns>
        public ConfigurationSettings GetConfigurationSettings()
        {
            var request = new GetConfigurationSettingsRequest(this)
                              {
                                  OrganizationId = OrganizationId,
                                  UserId = UserId
                              };
            return request.Execute().Settings;
        }
        /// <summary>
        /// Saves configuration settings.
        /// </summary>
        /// <param name="configurationSettings">Configuration settings.</param>
        public void SaveConfigurationSettings(ConfigurationSettings configurationSettings)
        {
            var request = new SetConfigurationSettingsRequest(this)
                              {
                                  Settings = configurationSettings
                              };
            request.Execute();
        }

        /// <summary>
        /// Saves MCU (Multipoint Control Unit) aka bridge.
        /// </summary>
        /// <param name="bridge">Bridge to be saved.</param>
        /// <returns>True if bridge has been saved succefully.</returns>
        public bool SetBridge(Bridge bridge)
        {
            var request = new SetBridgeRequest(this)
                              {
                                  Bridge = bridge,
                                  UserId = UserId,
                                  OrganizationId = OrganizationId
                              };
            return request.Execute().Success;
        }
        /// <summary>
        /// Returns room's profile.
        /// </summary>
        /// <param name="roomId">Room's ID.</param>
        /// <returns>Room's profile.</returns>
        public Room GetRoom(RoomId roomId)
        {
           // RoomId RoomDummy = new RoomId("0");
           // if (RoomDummy != roomId)
           // {
                var request = new GetRoomRequest(this) { UserId = UserId, RoomId = roomId };
                return request.Execute().Room;
            //}
            //return new Room();
           // return Room();
        }
        /// <summary>
        /// Saves room's profile
        /// </summary>
        /// <param name="room">Room's profile.</param>
        public void SaveRoom(Room room)
        {
            var request = new SaveRoomRequest(this)
                              {
                                  Room = room,
                                  UserId = UserId,
                                  OrganizationId = OrganizationId
                              };
            request.Execute();
        }
        /// <summary>
        /// Deletes room from the system.
        /// </summary>
        /// <param name="roomId">Room's ID.</param>
        public void DeleteRoom(RoomId roomId)
        {
            var request = new DeleteRoomRequest(this)
                              {
                                  RoomId = roomId
                              };
            request.Execute();
        }
        /// <summary>
        /// Returns available user's roles in the system. 
        /// </summary>
        /// <returns>User's roles.</returns>
        public UserRoleCollection GetUserRoles()
        {
            var request = new GetUserRolesRequest(this)
                              {
                                  UserId = UserId
                              };
            return request.Execute().UserRoles;
        }

        public VideoEquipmentCollection GetVideoEquipment()
        {
            var request = new GetVideoEquipmentRequest(this)
                              {
                                  UserId = UserId
                              };
            return request.Execute().Equipments;
        }

        public BridgeTypeCollection GetBridgeTypes()
        {
            var request = new GetNewBridgeRequest(this)
                              {
                                  UserId = UserId
                              };
            return request.Execute().BridgeTypes;
        }
        /// <summary>
        /// Returns all available bridges (MCUs) in the system.
        /// </summary>
        /// <returns>Collection of  bridges.</returns>
        public ReadOnlyCollection<BridgeName> GetBridges()
        {
            var request = new GetBridgesRequest(this)
                              {
                                  UserId = UserId,
                                  OrganizationId = OrganizationId
                              };
            return request.Execute().BridgeNames;
        }

		/// <summary>
		/// Returns all available MCU profiles.
		/// </summary>
		/// <returns>Collection of  MCU Profiles.</returns>
		public ReadOnlyCollection<MCUProfile> GetMCUProfiles(string id)
		{
			var request = new GetMCUProfilesRequest(this)
			              	{
			              		UserId = UserId,
			              		_MCUId = id
							};
			return request.Execute().MCUProfiles;
		}

		public GetNewConferenceResponse GetNewConference()
		{
			var request = new GetNewConferenceRequest(this)
			              	{
			              		UserId = UserId,
							};
			return request.Execute();
		}

		public GetConfMsgResponse GetConfMsg()
		{
			var request = new GetConfMsgRequest(this)
			              	{
			              		UserId = UserId,
								OrganizationId = OrganizationId
							};
			return request.Execute();
		}

		public GetVNOCUserListResponse GetVNOCUserList()
		{
			var request = new GetVNOCUserListRequest(this)
			{
				UserId = UserId,
			};
			return request.Execute();
		}

       
       //ZD 103181 start
        public GetEntityCodeListResponse GetEntityCodes(int Languageid)
        {
            var request = new GetEntityCodeList(this)
            {
                UserId = UserId,
                OrganizationId = OrganizationId,
                languageid = Languageid
            };
            return request.Execute();
        }
		//ZD 103181 Endsss
        /// <summary>
        /// Returns available line rates.
        /// </summary>
        /// <returns>The collection of line rates.</returns>
        public LineRateCollection GetLineRate()
        {
            var request = new GetLineRateRequest(this)
                              {
                                  UserId = UserId
                              };
            return request.Execute().LineRates;
        }
        /// <summary>
        /// Saves endpoint.
        /// </summary>
        /// <param name="endpoint">Endpoint to be saved.</param>
        public void SaveEndpoint(Endpoint endpoint)
        {
            var request = new SetEndpointRequest(this)
                              {
                                  Endpoint = endpoint,
                                  UserId = UserId,
                                  OrganizationId = OrganizationId
                              };
            endpoint.Id = request.Execute().Id;
            endpoint.ClearChangeLog();
        }
        /// <summary>
        /// Returns endpoint's profile.
        /// </summary>
        /// <param name="endpointId">Endpoint's ID.</param>
        /// <returns>Endpoint's profile.</returns>
        public Endpoint GetEndpoint(EndpointId endpointId)
        {
            var request = new GetEndpointRequest(this)
                              {
                                  UserId = UserId,
                                  EndpointId = endpointId
                              };
            return request.Execute().Endpoint;
        }
        /// <summary>
        /// Saves the tier.
        /// </summary>
        /// <param name="tier">Tier to be saved.</param>
        internal void SaveTier(Tier tier)
        {
            SetTierRequestBase request;
            if (tier is TopTier)
            {
                request = new SetTopTierRequest(this);
            }
            else if (tier is MiddleTier)
            {
                request = new SetMiddleTierRequest(this);
            }
            else
            {
                return;
            }
            request.UserId = UserId;
            request.OrganizationId = OrganizationId;
            request.Tier = tier;
            request.Execute();
        }
        /// <summary>
        /// Returns all top tiers.
        /// </summary>
        /// <returns>Collection of top tiers.</returns>
        public ReadOnlyCollection<TopTier> GetTopTierList()
        {
            var request = new GetTopTierListRequest(this)
                              {
                                  UserId = UserId,
                                  OrganizationId = OrganizationId
                              };
            return request.Execute().Tiers;
        }
        /// <summary>
        /// Returns all middle tiers from <seealso cref="topTierId"/> top tier.
        /// </summary>
        /// <param name="topTierId">Top tier's ID.</param>
        /// <returns>Collection of middle tiers.</returns>
        public ReadOnlyCollection<MiddleTier> GetMiddleTierList(TierId topTierId)
        {
            var request = new GetMiddleTierListRequest(this)
            {
                UserId = UserId,
                OrganizationId = OrganizationId,
                ParentId = topTierId
            };
            return request.Execute().Tiers;
        }
        /// <summary>
        /// Returns all registered organizations.
        /// </summary>
        /// <returns>Collection of registered organizations.</returns>
        public ReadOnlyCollection<Organization> GetOrganizationList()
        {
            var request = new GetOrganizationListRequest(this)
                              {
                                  UserId = UserId
                              };
            return request.Execute().Organizations;
        }
        /// <summary>
        /// Returns organization's options.
        /// </summary>
        /// <returns>Organization's options.</returns>
        public OrganizationOptions GetOrganizationOptions()
        {
			ConfigurationSettings config = GetConfigurationSettings();
        	
            var request = new GetOrganizationOptionsRequest(this)
                              {
                                  UserId = UserId,
                                  OrganizationId = OrganizationId,
                              };
			GetOrganizationOptionsResponse response = request.Execute();
			response.Options.AvailableTime.SetupSystemTimeZoneID(config.SystemTimeZoneID);
            MyVrmTraceSource tracesource = new MyVrmTraceSource("doc.txt");

            tracesource.TraceError("err msg:");
            
			return response.Options;//request.Execute().Options;
        }
        /// <summary>
        /// Returns organization's settings.
        /// </summary>
        /// <returns>Organization's settings.</returns>
        public OrganizationSettingsOld GetOrganizationSettings()
        {
            var request = new GetOrganizationSettingsRequest(this)
            {
                UserId = UserId,
                OrganizationId = OrganizationId
            };
            return request.Execute().Settings;
        }
        
		/// <summary>
        /// Returns all organization's settings.
        /// </summary>
        /// <returns>Organization's all settings.</returns>
		public OrganizationSettingsNew GetAllOrganizationSettings()
        {
			var request = new GetAllOrgSettingsRequest(this)
            {
                //UserId = UserId,
                OrganizationId = OrganizationId
            };
            return request.Execute().Settings;
        }


		

        /// <summary>
        /// Returns all users.
        /// </summary>
        /// <returns></returns>
        public FindUsersResults FindUsers()
        {
            var request = new FindUsersRequest(this)
                              {
                                  UserId = UserId,
                                  OrganizationId = OrganizationId
                              };
            return request.Execute().Results;

        }



        /// <summary>
        /// Returns an existing conference.
        /// </summary>
        /// <param name="conferenceId">Conference's ID.</param>
        /// <returns>Conference.</returns>
        public Conference GetConference(ConferenceId conferenceId)
        {
            var request = new GetConferenceRequest(this)
                              {
                                  UserId = UserId,
                                  ConferenceId = conferenceId,
                              };
			Conference conf = request.Execute().Conference;

			//if( IsUtcEnabled )
			//{
			//    DateTime dt = new DateTime(conf.StartDate.Ticks, DateTimeKind.Utc).ToLocalTime();
			//    conf.StartDate = dt;

			//    dt = new DateTime(conf.AppointmentTime.StartTime.Ticks, DateTimeKind.Utc).ToLocalTime();
			//    conf.AppointmentTime.StartTime = dt.TimeOfDay;
			//}
        	return conf;
        }
        /// <summary>
        /// Saves conference to the system.
        /// </summary>
        /// <param name="conference">Conference to be saved.</param>
        internal void SaveConference(Conference conference)
        {
            //CheckModifyOperation(conference);
            if (conference.IsAllowedToModify)
            {
                conference.ModifyType = conference.IsNew ? 1 : 0;
                conference.IsPCconference = conference.PCVendorId != PCConferenceVendor.None;
                 if (conference.ConciergeSupportParams.MeetandGreet)
                    conference.MeetandGreetBuffer = MyVrmService.Service.OrganizationOptions.MeetandGreetBuffer; //ZD 102651
                 
               
                var request = new SaveConferenceRequest(this)
                                {
                                    UserId = UserId,
                                    Conference = conference
                                };
                SetConferenceDetails resp = request.Execute();
                conference.Id = resp.ConferenceId;

                
                //Correct for added external users obtained IDs (instead "new")
            //    foreach (var user in resp.OutExternalParticipants)
            //    {
                //    try
                 //   {
                //        var found = conference.AdvancedAudioVideoSettings.Endpoints.FirstOrDefault(a => a.ExternalUserEmail == user.Email);
                //        if (found != null)
//{
   //                         found.Id = user.UserId;
     //                   }
       //             }
         //           catch (Exception)
           //         {
                
                //        throw;
                  //  }
                //}
                // ZD 101343 starts

                //if (resp.OutConfRoomCollection != null)
                //{
                    //foreach (var user in resp.OutConfRoomCollection)
                  //  {
                    //    try
                      //  {

                         //   var found = conference.AdvancedAudioVideoSettings.Endpoints.FirstOrDefault(a => a.EndPointName == user.EndpointName);

                         //   if (found != null)
                          //  {
                          //      if(string.IsNullOrEmpty(found.ConfGRoomID))
                           //     found.ConfGRoomID = user.ID;
                            //    if(string.IsNullOrEmpty(found.EndPointName)) 
                           //     found.EndPointName = user.EndpointName;
                          //      if (found.EndpointId==null) 
                             //   found.EndpointId = user.EndPointID;
                        //        if (found.ProfileId==null) 
                             //   found.ProfileId = user.ProfileID;
                            //    if (found.BridgeId==null) 
                             //   found.BridgeId = user.BridgeId;
                         //   }
                        //}
                        //catch (Exception ex)
                        //{
                          //  MyVrmService.TraceSource.TraceInformation(ex.Message);

                        //}
                    //}
               // }
                // ZD 101343 Ends


            }
        }

        internal void CheckModifyOperation(Conference conference)
        {
            if (!conference.IsAllowedToModify)
            {
                throw new OperationNotPermittedException(Strings.NotAllowedModifyConferenceMessage);
            }
        }

        /// <summary>
        /// Saves the occurrence of recurring conference.
        /// </summary>
        /// <param name="conference">The conference to be saved.</param>
        /// <param name="origOccurrenceDate">The date of the occurrence.</param>
		internal void SaveConferenceOccurrence(Conference conference, DateTime origOccurrenceDate)
        {
            //CheckModifyOperation(conference);
			if (conference.IsAllowedToModify)
			{
				var request = new EditRecurringConferenceInstanceRequest(this)
				              	{
				              		UserId = UserId,
				              		ConferenceOccurrence = conference,
				              		OrigOccurrenceDate = origOccurrenceDate
				              	};
				conference.Id = request.Execute().ConferenceId;
			}
        }
        /// <summary>
        /// Deletes an existing conference.
        /// </summary>
        /// <param name="conferenceId">Conference to be deleted.</param>
        public void DeleteConference(ConferenceId conferenceId)
        {
            var request = new DeleteConferenceRequest(this)
                              {
                                  UserId = UserId,
                                  ConferenceId = conferenceId
                              };
            request.Execute();
        }
        /// <summary>
        /// Deletes the instance of the recurring conference.
        /// </summary>
        /// <param name="conferenceId">The conference ID.</param>
        /// <param name="instanceDate">Instance date.</param>
        public void DeleteRecurringConferenceInstance(ConferenceId conferenceId, DateTime instanceDate)
        {
            var request = new DeleteRecurringConferenceInstanceRequest(this)
                              {
                                  UserId = UserId,
                                  ConferenceId =new ConferenceId(conferenceId.ToString().Split(',')[0]),//conferenceId,
                                  InstanceDate = instanceDate
                              };
            request.Execute();
        }

        internal void SaveConferenceAdvancedAudioVideoSettings(ConferenceId conferenceId, ConferenceAdvancedAudioVideoSettings settings)
        {
            var request = new SetConferenceAdvancedAudioVideoSettingsRequest(this)
                          {
                              OrganizationId = OrganizationId,
                              UserId = UserId,
                              ConferenceId = conferenceId,
                              Settings = settings
                          };
            request.Execute();
        }

        internal ConferenceAdvancedAudioVideoSettings GetConferenceAdvancedAudioVideoSettings(ConferenceId conferenceId)
        {
            var request = new GetConferenceAdvancedAudioVideoSettingsRequest(this)
                          {
                              UserId = UserId,
                              ConferenceId = conferenceId
                          };
            return request.Execute().Settings;
        }

        internal void SaveConferenceWorkOrders(ConferenceId conferenceId, WorkOrderCollection workOrders)
        {
            var request = new SetConferenceWorkOrdersRequest(this)
                              {
                                  UserId = UserId,
                                  ConferenceId = conferenceId,
                                  WorkOrders = workOrders
                              };
            request.Execute();
        }

        internal ReadOnlyCollection<WorkOrderId> SearchConferenceWorkOrders(ConferenceId conferenceId, WorkOrderType type)
        {
            var request = new SearchConferenceWorkOrdersRequest(this)
                          {
                              UserId = UserId,
                              ConferenceId = conferenceId, 
                              Type = type,
                              SearchFilter = WorkOrderSearchFilter.All
                          };
            return request.Execute().WorkOrderIds;
        }

        internal WorkOrder GetWorkOrderDetails(ConferenceId conferenceId, WorkOrderId workOrderId)
        {
            var request = new GetWorkOrderDetailsRequest(this)
                          {
                              UserId = UserId,
                              ConferenceId = conferenceId,
                              WorkOrderId = workOrderId
                          };
            return request.Execute().WorkOrder;
        }

        internal CateringWorkOrder GetCateringWorkOrderDetails(ConferenceId conferenceId, WorkOrderId workOrderId)
        {
            var request = new GetProviderWorkOrderDetailsRequest(this)
                          {
                              UserId = UserId,
                              ConferenceId = conferenceId,
                              WorkOrderId = workOrderId
                          };
            return request.Execute().WorkOrder;
        }

        internal bool SaveCateringWorkOrders(ConferenceId conferenceId, CateringWorkOrderCollection workOrders)
        {
            var request = new SaveCateringWorkOrdersRequest(this)
                              {
                                  ConferenceId = conferenceId,
                                  WorkOrders = workOrders
                              };
            return request.Execute().Success;
        }

        internal bool DeleteWorkOrder(ConferenceId conferenceId, WorkOrderId workOrderId)
        {
			if (workOrderId != null)
			{
				var request = new DeleteWorkOrderRequest(this)
				              	{
				              		UserId = UserId,
				              		ConferenceId = conferenceId,
				              		WorkOrderId = workOrderId
				              	};
				return request.Execute().Success;
			}
        	return true; //?????
        }
        /// <summary>
        /// Returns list of rooms that has been modified by specified user and since the given date.
        /// </summary>
        /// <param name="lastModifiedBy">The user's ID.</param>
        /// <param name="lastModfiedDate">The date.</param>
        /// <returns></returns>
        public ReadOnlyCollection<RoomId> FindModifiedRooms(UserId lastModifiedBy, DateTime lastModfiedDate)
        {
            var request = new FindModifiedRoomsRequest(this)
                          {
                              LastModifiedBy = UserId,
                              LastModifiedDate = lastModfiedDate
                          };
            return request.Execute().Rooms;
        }

        //
        /// Returns active conference occurrences for the given room.
        /// </summary>
        /// <param name="roomId">The room's ID.</param>
        /// <param name="fromDate">The start date.</param>
        /// <returns>List of occurrences.</returns>
        public ReadOnlyCollection<ConferenceOccurrence> GetRoomDailyCalendar(RoomId roomId, DateTime fromDate)
        {
            return GetRoomDailyCalendar(roomId, fromDate, true);
        }
        /// <summary>
        /// Returns conference occurrences for the given room.
        /// </summary>
        /// <param name="roomId">The room's ID.</param>
        /// <param name="fromDate">The start date.</param>
        /// <param name="hideDeleted">true - to hide deleted occurrences.</param>
        /// <returns>List of occurrences.</returns>
        public ReadOnlyCollection<ConferenceOccurrence> GetRoomDailyCalendar(RoomId roomId, DateTime fromDate, bool hideDeleted)
        {
            var request = new GetRoomDaylyCalendarRequest(this)
            {
                UserId = UserId,
                RoomId = roomId,
                FromDate = fromDate
            };
            var occurrences = request.Execute().Occurences;
            return hideDeleted ?
                new ReadOnlyCollection<ConferenceOccurrence>(occurrences.Where(occ => !occ.IsDeleted).ToList())
                : occurrences;
        }
        //public ReadOnlyCollection<ConferenceOccurrence> GetRoomWeeklyCalendar(RoomId roomId, DateTime fromDate)
        //{
        //    var request = new GetRoomWeeklyCalendarRequest(this)
        //                  {
        //                      UserId = UserId,
        //                      RoomId = roomId,
        //                      FromDate = fromDate
        //                  };
        //    return request.Execute().Occurences;
        //}

        /// Returns active conference occurrences for the given room.
        /// </summary>
        /// <param name="roomId">The room's ID.</param>
        /// <param name="fromDate">The start date.</param>
        /// <returns>List of occurrences.</returns>
        public ReadOnlyCollection<ConferenceOccurrence> GetRoomWeeklyCalendar(RoomId roomId, DateTime fromDate)
        {
            return GetRoomWeeklyCalendar(roomId, fromDate, true);
        }
        /// <summary>
        /// Returns conference occurrences for the given room.
        /// </summary>
        /// <param name="roomId">The room's ID.</param>
        /// <param name="fromDate">The start date.</param>
        /// <param name="hideDeleted">true - to hide deleted occurrences.</param>
        /// <returns>List of occurrences.</returns>
        public ReadOnlyCollection<ConferenceOccurrence> GetRoomWeeklyCalendar(RoomId roomId, DateTime fromDate, bool hideDeleted)
        {
            var request = new GetRoomWeeklyCalendarRequest(this)
            {
                UserId = UserId,
                RoomId = roomId,
                FromDate = fromDate
            };
            var occurrences = request.Execute().Occurences;
            return hideDeleted ?
                new ReadOnlyCollection<ConferenceOccurrence>(occurrences.Where(occ => !occ.IsDeleted).ToList())
                : occurrences;
        }

        /// <summary>
        /// Returns active conference occurrences for the given room.
        /// </summary>
        /// <param name="roomId">The room's ID.</param>
        /// <param name="fromDate">The start date.</param>
        /// <returns>List of occurrences.</returns>
        public ReadOnlyCollection<ConferenceOccurrence> GetRoomMonthlyCalendar(RoomId roomId, DateTime fromDate)
        {
            return GetRoomMonthlyCalendar(roomId, fromDate, true);
        }
        /// <summary>
        /// Returns conference occurrences for the given room.
        /// </summary>
        /// <param name="roomId">The room's ID.</param>
        /// <param name="fromDate">The start date.</param>
        /// <param name="hideDeleted">true - to hide deleted occurrences.</param>
        /// <returns>List of occurrences.</returns>
        public ReadOnlyCollection<ConferenceOccurrence> GetRoomMonthlyCalendar(RoomId roomId, DateTime fromDate, bool hideDeleted)
        {
            var request = new GetRoomMonthlyCalendarRequest(this)
            {
                UserId = UserId,
                RoomId = roomId,
                FromDate = fromDate
            };
            var occurrences = request.Execute().Occurences;
            return hideDeleted ?
                new ReadOnlyCollection<ConferenceOccurrence>(occurrences.Where(occ => !occ.IsDeleted).ToList())
                : occurrences;
        }
        /// <summary>
        /// Returns list of <see cref="CateringProviderMenu"/> for the given room.
        /// </summary>
        /// <param name="roomId">The room's ID.</param>
        /// <param name="cateringServiceId">The catering service ID.</param>
        /// <returns>List of <see cref="CateringProviderMenu"/>.</returns>
        public ReadOnlyCollection<CateringProviderMenu> SearchProviderMenus(RoomId roomId, int cateringServiceId)
        {
            var request = new SearchProviderMenusRequest(this)
                          {
                              UserId = UserId,
                              RoomId = roomId,
                              CateringServiceId = cateringServiceId
                          };
            return request.Execute().Menus;
        }
        /// <summary>
        /// Returns collection of sets for the given room.
        /// </summary>
        /// <param name="roomId">The room's ID.</param>
        /// <param name="roomSetType">The type of set.</param>
        /// <returns>The collection of sets.</returns>
        public RoomSetCollection GetRoomSets(RoomId roomId, RoomSetType roomSetType)
        {
            var request = new GetRoomSetsRequest(this)
                          {
                              UserId = UserId,
                              RoomId = roomId,
                              SetType = roomSetType
                          };
            return request.Execute().RoomSets;
        }
        /// <summary>
        /// GetInventoryWoAdminDetails
        /// </summary>
        /// <param name="setId"></param>
        /// <param name="setType"></param>
        /// <returns></returns>
        public GetInventoryWOAdminResponse GetInventoryWoAdminDetails(int setId, RoomSetType setType)
        {
            var request = new GetInventoryWOAdminRequest(this)
                              {
                                  UserId = UserId,
                                  SetId = setId,
                                  SetType = setType
                              };
            return request.Execute();
        }
        public InventorySet GetInventoryDetails(int setId, RoomSetType setType)
        {
            var request = new GetInventoryDetailsRequest(this)
            {
                UserId = UserId,
                SetId = setId,
                SetType = setType
            };
            return request.Execute().InventorySet;
        }


        //public InventorySet GetInventoryDetails(int setId, RoomSetType setType, string PreselectedItems, DateTime WorkOrderStart, DateTime WorkOrderComplete, TimeZoneInfo WoTimeZone)//103771
        //{
        //    var request = new GetInventoryDetailsRequest(this)
        //    {
        //        UserId = UserId,
        //        SetId = setId,
        //        SetType = setType,
        //        WorkOrderStart = WorkOrderStart,
        //        WorkOrderComplete = WorkOrderComplete,
        //        PreselectedItems = PreselectedItems,//103771
        //        WoTimeZone = WoTimeZone
        //    };
        //    return request.Execute().InventorySet;
        //}
        /// <summary>
        /// Return catering services.
        /// </summary>
        /// <returns>The collection of catering services.</returns>
        public CateringServiceCollection GetCateringServices()
        {
            var request = new GetCateringServicesRequest(this) { UserId = UserId };
            return request.Execute().Services;
        }

        ////ZD 102037 start
        ///// <summary>
        ///// GetEmailContentResponse
        ///// </summary>
        ///// <param name="conferenceId"></param>
        ///// <param name="iCalId"></param>
        ///// <returns></returns>
        //public getemailcontentresponse getemailcontentservices(conferenceid conferenceid, string icalid)
        //{
        //    var request = new getemailcontentrequest(this)
        //    {
        //        userid = userid ,
        //        organizationid =organizationid,
        //        conferenceid = conferenceid,
        //        icalid = icalid

        //    };
        //    return request.execute().services;
        //}
        ////ZD 102037 End
        


        /// <summary>
        /// Returns all active (non-deleted) rooms.
        /// </summary>
        /// <returns>The collection of active rooms.</returns>
        public ReadOnlyCollection<ManagedRoom> GetActiveManagedRooms()
        {
            return new ReadOnlyCollection<ManagedRoom>(GetManagedRooms().Where(room => !room.Deleted).ToList());
        }
        /// <summary>
        /// Returns all rooms
        /// </summary>
        /// <returns>The collection of rooms.</returns>
        public ReadOnlyCollection<ManagedRoom> GetManagedRooms()
        {
            var request = new ManageConfRoomRequest(this)
            {
                UserId = UserId
            };
            return request.Execute().Rooms;
        }

		/// <summary>
		/// Returns available rooms available during particular period of time.
		/// </summary>
		/// <param name="start">When conference starts.</param>
		/// <param name="duration">Duration of conference.</param>
		/// <param name="timeZone">Conference timezone.</param>
		/// <param name="isRecurring">true if conference is recurring otherwise false.</param>
		/// <param name="mediaType">Media type.</param>
		/// <param name="conferenceType">Conference type.</param>
		/// <returns></returns>
		public ReadOnlyCollection<ManagedRoom> GetAvailableRooms(DateTime start, TimeSpan duration, TimeZoneInfo timeZone, bool isRecurring,
			MediaTypeFilter mediaType, ConferenceType conferenceType, ConferenceId conferenceId)
		{
			var request = new GetAvailableRoomsRequest(this)
			{
				UserId = UserId,
				ConferenceId = conferenceId ?? ConferenceId.NewConferenceId,
				Start = start,
				Duration = duration,
				TimeZone = timeZone,
				Recurring = isRecurring,
				MediaType = mediaType,
				ConferenceType = conferenceType
			};
			return request.Execute().Rooms;
		}

        /// <summary>
        /// Returns all audio users.
        /// </summary>
        /// <returns></returns>
        public ReadOnlyCollection<AudioUser> GetAudioUsers(DateTime start, TimeSpan duration, TimeZoneInfo timeZone, string conferenceId, bool Immediate) //ZD 104699 start
        {
            var request = new GetAudioUsersRequest(this)
                              {
                                  OrganizationId = OrganizationId,
                                  UserId = UserId,
                                  ConferenceId = conferenceId,
                                  Start = start,
                                  Duration = duration,
                                  TimeZone = timeZone,
                                  Immediate = Immediate
                              };
            return request.Execute().Users;
        } //ZD 104699 End
        /// <summary>
        /// Returns definition of custom attribute.
        /// </summary>
        /// <param name="id">The custom attribute's ID.</param>
        /// <returns>The definition of custom attribute.</returns>
        public CustomAttributeDefinition GetCustomAttributeDefinition(CustomAttributeId id)
        {
            var request = new GetCustomAttributeDefinitionRequest(this)
                              {
                                  OrganizationId = OrganizationId,
                                  Id = id
                              };
            return request.Execute().AttributeDefinition;
        }
        /// <summary>
        /// Returns all custom attributes regardless to department.
        /// </summary>
        /// <returns>List of custom attribute definitions.</returns>
        public  IEnumerable<CustomAttributeDefinition> GetCustomAttributeDefinitions()
        {
            var request = new GetCustomAttributeDefinitionsRequest(this)
                              {
                                  OrganizationId = OrganizationId,
                              };
            return request.Execute().Attributes;
        }
        /// <summary>
        /// Updates iCalendar ID for the given conference. myVRM uses this ID to send updates to conference participants.
        /// </summary>
        /// <param name="conferenceId">The conference ID.</param>
        /// <param name="iCalId">The iCalendar ID.</param>
        public void UpdateConferenceICalUniqueId(ConferenceId conferenceId, string iCalId)
        {
            var request = new UpdateConferenceICalUniqueIdRequest(this)
                              {
                                  OrganizationId = OrganizationId,
                                  UserId = UserId,
                                  ConferenceId = conferenceId,
                                  ICalId = iCalId
                              };
            request.Execute();
        }
        //ZD 102037
        /// <summary>
        /// GetEmailContent
        /// </summary>
        /// <param name="conferenceid"></param>
        /// <param name="icalid"></param>
        //public void GetEmailContent()
        //{
        //    var request = new 
        //    {
        //        UserId = UserId,
        //        OrganizationId = OrganizationId,

        //    };
        //    request.Execute();
        //}
        //ZD 102037
        /// <summary>
        /// Tests connection to myVRM Web Services.
        /// </summary>
        /// <param name="url">The myVRM Web Service URL.</param>
        /// <param name="mode">Authentication mode.</param>
        /// <param name="credential">The credential.</param>
        public static void TestConnection(string url, AuthenticationMode mode, NetworkCredential credential)
        {
            using (var service = new MyVrmService())
            {
                service.ClientType = Service.ClientType;
                service.ClientVersion = Service.ClientVersion;
                service.Url = url;
                service.AuthenticationMode = mode;
                service.UseDefaultCredential = credential == null;
                if (!service.UseDefaultCredential)
                    service.Credential = credential;
                service.Logon();
            }
        }

        public RecurrenceConferencesResponse GetConcurrentConfsList(Conference conference)
        {
            if (conference == null)
                throw new ArgumentNullException("conference");
            var request = new RecurrenceConferencesRequest(this)
            {
                RecurrencePattern = conference.RecurrencePattern,
                ConferenceId = conference.Id ?? ConferenceId.NewConferenceId,
                UserId = UserId,
                AppointmentTime = conference.GetAppointmentTime()
            };
            foreach (ConfRoom Cfr in conference.Rooms)
            {
                if (Cfr.ConfEndPt.Id.Id != "0") 
                request.RoomIds.Add(new RoomId(Cfr.ConfEndPt.Id.Id));
            }
            //request.RoomIds.AddRange(conference.Rooms);
            return request.Execute();
        }

        //
        public ConferenceSetTemplateResponse ConferenceSetTemplate(Conference conference,
            string TemplateName, string TemplateDescr, bool IsPublic, bool IsDefault)
        {
            if (conference == null)
                throw new ArgumentNullException("conference");

            var request = new ConferenceSetTemplateRequest(this)
            {
                UserId = UserId,
                Name = TemplateName,
                Descr = TemplateDescr,
                IsPublic = IsPublic,
                IsDefault = IsDefault,
                conference = conference,
            };
            return request.Execute();
        }

        //ZD 102037 start
        ///// <summary>
        ///// GetEmailContentResponse
        ///// </summary>
        ///// <param name="conferenceId"></param>
        ///// <param name="iCalId"></param>
        ///// <returns></returns>
        public GetEmailContentResponse GetEmailContentRequest(ConferenceId conferenceid, ConferenceType ConfType1)//104556
        {
            var request = new GetEmailContentRequest(this, ConfType1)//104556
            {
               UserId = UserId,
               OrganizationId =OrganizationId,
               //ICalId = icalid,
            };
            return request.Execute();
        }
        //public GetAudioUsersResponse GetAudioUsersRequest(UserId Userid)
        //{
        //    var request = new GetAudioUsersRequest(this)
        //    {
        //        UserId = Userid,
        //        OrganizationId = OrganizationId,
        //        //ICalId = icalid,
        //    };
        //    return request.Execute();
        //}
        //ZD 102037 End
        string PCId = "";
        public FetchSelectedPCDetailsResponse FetchSelectedPCDetailsRequest(int PCId)
        {
            var request = new FetchSelectedPCDetailsRequest(this)
            {
                UserId = UserId,
                OrganizationId = OrganizationId,
                PCID = PCId,
                //ICalId = icalid,
            };
            return request.Execute();
        }

        public GetoldRecurranceResponse GetoldRecurranceRequest(ConferenceId conferenceId)
        {
            var request = new GetoldRecurranceRequest(this)
            {

                UserId = UserId,
                ConferenceId = conferenceId,
               
                //ICalId = icalid,
            };
            return request.Execute();
        }
        public GetOldBridgeResponse GetOldBridgeRequest(int BridgeID)
        {
            var request = new GetOldBridgeRequest(this)
            {
                UserId = UserId,
                OrganizationId = OrganizationId,
                bridgeID = BridgeID,
                //ICalId = icalid,
            };
            return request.Execute();
        }

        //ZD 103364 start
        //public GetInventoryDetailsResponse GetInventoryAdmin (int setId, RoomSetType setType)
        //{
        //    var request = new GetInventoryDetailsRequest(this)
        //    {
        //         UserId = UserId,
        //         SetId = setId,
        //         SetType = setType
        //    };
        //    return request.Execute();
        //}

        //ZD 103364 End
        public GetTemplateListResponse GetTemplateList(GetTemplateListResponse.SortByType sortByType)
        {
            var request = new GetTemplateListRequest(this)
            {
                UserId = UserId,
                SortBy = sortByType
            };
            return request.Execute();
        }

        public GetTemplateResponse GetTemplate(int templateId)
        {
            var request = new GetTemplateRequest(this)
            {
                UserId = UserId,
                templateId = templateId
            };
            return request.Execute();
        }

		public ConferenceDeleteTemplateResponse ConferenceDeleteTemplate(int templateID)
		{
			if (templateID == 0)
				throw new ArgumentNullException("templateID");

			var request = new ConferenceDeleteTemplateRequest(this)
			{
				UserId = UserId,
				TemplateID = templateID
			};
			return request.Execute();
		}

        public SetPreferedRoomResponse SetPreferedRoom(IEnumerable<RoomId> rooms)
        {
            var request = new SetPreferedRoomRequest(this)
            {
                UserId = UserId,
                RoomList = rooms
            };
            return request.Execute();
        }

        public GetPreferedRoomResponse GetPreferedRoom()
        {
            var request = new GetPreferedRoomRequest(this)
            {
                UserId = UserId,
            };
            return request.Execute();
        }

        public void UploadFile(UploadFileInfo fileInfo)
        {
            UploadFiles(new[] {fileInfo});
        }

        public void UploadFiles(IEnumerable<UploadFileInfo> fileInfos)
        {
            var request = new UploadFilesRequest(this);
            var infos = fileInfos.ToList();
            request.FileInfos.AddRange(infos);
            var paths = request.Execute().Paths.ToList();
            for (var i = 0; i < infos.Count; i++)
            {
                var path = paths.ElementAt(i);
                infos[i].RemotePath = path;
            }
        }
		/**/
		public SearchConferenceResponse SearchConference( TApprovalPending ApprovalPending,
			string Host, string ConferenceId, string ConferenceName, int ConferenceParticipant, 
			TConferenceSearchType ConferenceSearchType, TConferenceStatus ConferenceStatus,
			int ConfUniqueId, DateTime dtFrom, DateTime dtTo, TPublic Public,
			TRecurrenceStyle RecurrenceStyle, int PageNo,
			TSortBy SortBy, TSelectionType SelectionType, string SelectedRooms,
			CustomAttributeCollection attributeCollection)
		{
			var request = new SearchConferenceRequest(this)
			{
				UserId = UserId,
				OrganizationId = OrganizationId,
				ApprovalPending = ApprovalPending,
				ConferenceHost = Host,
				ConferenceId = ConferenceId,
				ConferenceName = ConferenceName,
				ConferenceParticipant = ConferenceParticipant,
				ConferenceSearchType = ConferenceSearchType,
				ConferenceStatus = ConferenceStatus,
				ConfUniqueId = ConfUniqueId,
				SearchingDateFrom = dtFrom,
				SearchingDateTo = dtTo,
				Public = Public,
				RecurrenceStyle = RecurrenceStyle,
				PageNo = PageNo,
				SortBy = SortBy,
				SelectionType = SelectionType,
				SelectedRooms = SelectedRooms,
				CustomAttributes = attributeCollection,
			};
			return request.Execute();
		}
		/**/
		public SetApproveConferenceResponse SetApproveConference(List<ConferenceToApprove> approvedConfList)
		{
			var request = new SetApproveConferenceRequest(this)
			              	{
								UserId = UserId,
								//OrganizationId = OrganizationId,
			              		ApprovedConfList = approvedConfList
			              	};
			return request.Execute();
		}
		/**/
		public Dictionary<int, string> GetMediaTypes()
		{
			var request = new GetMediaTypesRequest(this)
			              	{
			              		UserId = UserId
			              	};
			var ret = request.Execute();
			return ret != null ? ret.MediaTypes : null;
		}

		public Dictionary<int, string> GetLineRates()
		{
			var request = new GetLineRatesRequest(this)
			{
				UserId = UserId
			};
			var ret = request.Execute();
			return ret != null ? ret.LineRates : null;
		}

		public Dictionary<int, string> GetVideoProtocols()
		{
			var request = new GetVideoProtocolsRequest(this)
			{
				UserId = UserId
			};
			var ret = request.Execute();
			return ret != null ? ret.VideoProtocols: null;
		}

		public Dictionary<int, string> GetAddressTypes()
		{
			var request = new GetAddressTypesRequest(this)
			{
				UserId = UserId
			};
			var ret = request.Execute();
			return ret != null ? ret.AddressTypes : null;
		}
		
    	/**/

        public void Dispose()
        {
            if (!_disposed)
            {
                Logoff();
                GC.SuppressFinalize(this);
                _disposed = true;
            }
        }

        internal void Logon()
        {
            DisposeCheck();
            Logoff();

            LogonRequest request;
            if (AuthenticationMode == AuthenticationMode.Windows)
            {
                var windowsIdentity = WindowsIdentity.GetCurrent();
                if (windowsIdentity == null)
                {
                    throw new MyVrmServiceLocalException(Strings.UnableToGetCurrentWindowsIdentity);
                }
                string userName;
                string domainName;
                if (UseDefaultCredential)
                {
                    
                    SecurityUtils.SplitNTUserName(windowsIdentity.Name, out domainName, out userName);
                }
                else
                {
                    userName = Credential.UserName;
                    domainName = Credential.Domain;
                }
                request = new LogonRequest(this, domainName, userName);
            }
            else
            {
                request = new LogonRequest(this, Credential);
            }
            LogonResponse response = request.Execute();
            UserId = response.UserId;
			IsAVEnabled = response.IsAVEnabled;
//101456 starts
            enableBlueJeans = response.enableBlueJeans;
            enableDesktopVideo = response.enableDesktopVideo;
            enableJabber = response.enableJabber;
            enableLync = response.enableLync;
            enableVidtel = response.enableVidtel;
//101456 ends
            //ZD 101838
            EnableAdditionalOption = response.EnableAdditionalOption;
            EnableAVWorkOrder = response.EnableAVWorkOrder;
            EnableCateringWO = response.EnableCateringWO;
            EnableFacilityWO = response.EnableFacilityWO;
            StartMode = response.StartMode;//ZD 102638
            SwitchOrganization(response.OrganizationId);
            _loggedOn = true;
        }

        internal void Validate()
        {
            if (string.IsNullOrEmpty(Url))
            {
                throw new MyVrmServiceLocalException(Strings.ServiceUrlMustBeSet);
            }
			var urlParser = new UriBuilder(Url);
			if (!Uri.CheckSchemeName(urlParser.Scheme) ||
				(urlParser.Scheme != Uri.UriSchemeHttp && urlParser.Scheme != Uri.UriSchemeHttps))
			{
				throw new MyVrmServiceLocalException(Strings.WrongURLSchema);
			}
			if (Uri.CheckHostName(urlParser.Host) == UriHostNameType.Unknown)
			{
				throw new MyVrmServiceLocalException(Strings.WrongHostName);
			}
			if ((AuthenticationMode == AuthenticationMode.Windows && !UseDefaultCredential) || AuthenticationMode == AuthenticationMode.Custom)
			{
			    if (string.IsNullOrEmpty(Credential.UserName))
			    {
				    throw new MyVrmServiceLocalException(Strings.UserMustBeSet);
			    }
                if (AuthenticationMode == AuthenticationMode.Windows && string.IsNullOrEmpty(Credential.Domain))
                {
                    throw new MyVrmServiceLocalException(Strings.DomainMustBeSet);
                }
            }

        }
        private void Logoff()
        {
            DisposeCheck();

            _loggedOn = false;
            UserId = null;
            SwitchOrganization(OrganizationId.Invalid);
        }

        private void DisposeCheck()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(GetType().ToString());
            }
        }

        private void LogonCheck()
        {
            if (!LoggedOn)
            {
                Logon();
            }
        }

        private static string MakeFullUrl(string url)
        {
            var uri = new Uri(url);
            var builder = new UriBuilder(uri.Scheme, uri.Host, uri.Port, Constants.MyVRMServiceDefaultPath);
            // If URL is the same as the default return it.
            if (builder.Uri == uri)
            {
                return uri.ToString();    
            }
            // If absoulute path of URL ends with .asmx return as is. Any parameters are ignored.
            if (uri.AbsolutePath.EndsWith(Constants.MyVRMServiceDefaultExtension, StringComparison.CurrentCultureIgnoreCase))
            {
                return uri.ToString();
            }
            // URL is in format http://host/en
            if (uri.Segments.Length == 2)
            {
                return new Uri(uri, uri.MakeRelativeUri(builder.Uri)).ToString();
            }
            return builder.Uri.ToString();
        }
    }
}
