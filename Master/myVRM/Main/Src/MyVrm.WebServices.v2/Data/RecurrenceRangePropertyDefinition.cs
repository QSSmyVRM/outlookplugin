﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    internal class RecurrenceRangePropertyDefinition : PropertyDefinition
    {
        public RecurrenceRangePropertyDefinition(string xmlElementName) : base(xmlElementName)
        {
        }

        public RecurrenceRangePropertyDefinition(string xmlElementName, PropertyDefinitionFlags flags) : base(xmlElementName, flags)
        {
        }

        public RecurrenceRangePropertyDefinition(string xmlElementName, string xmlElementNameForWrite, PropertyDefinitionFlags flags) : base(xmlElementName, xmlElementNameForWrite, flags)
        {
        }

        #region Overrides of PropertyDefinition

        internal override void LoadPropertyValueFromXml(MyVrmServiceXmlReader reader, PropertyBag propertyBag)
        {
            var range = RecurrenceRange.FromXml(reader, XmlNamespace.NotSpecified, XmlElementName);
            propertyBag[this] = range;
        }

        internal override void WritePropertyValueToXml(MyVrmXmlWriter writer, PropertyBag propertyBag)
        {
            // Do nothing. By design RecurrenceRange is stored in RecurrencePattern 
        }

        #endregion
    }
}
