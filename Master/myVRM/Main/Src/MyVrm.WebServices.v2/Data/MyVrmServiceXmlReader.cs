﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.IO;

namespace MyVrm.WebServices.Data
{
    internal class MyVrmServiceXmlReader : MyVrmXmlReader
    {
        private readonly MyVrmService _service;

        public MyVrmServiceXmlReader(Stream stream, MyVrmService service) : base(stream)
        {
            _service = service;
        }

        public MyVrmService Service
        {
            get { return _service; }
        }

        public TServiceObject ReadServiceObject<TServiceObject>(string xmlElementName, GetObjectInstanceDelegate<TServiceObject> getObjectInstanceDelegate, bool clearPropertyBag)  where TServiceObject : ServiceObject
        {
            TServiceObject item = default(TServiceObject);
            ReadStartElement(XmlNamespace.NotSpecified, xmlElementName);
            if (!IsEmptyElement)
            {
                do
                {
                    Read();
                    if (IsStartElement())
                    {
                        item = getObjectInstanceDelegate(Service, LocalName);
                        if (item == null)
                        {
                            SkipCurrentElement();
                        }
                        else
                        {
                            if (string.Compare(LocalName, item.GetXmlElementName(), StringComparison.Ordinal) != 0)
                            {
                                throw new MyVrmServiceLocalException(string.Format(Strings.TypeDoesNotMatch, LocalName,
                                                                                   item.GetXmlElementName()));
                            }
                            item.LoadFromXml(this, clearPropertyBag);
                        }
                    }
                } while (!IsEndElement(XmlNamespace.NotSpecified, xmlElementName));
            }
            return item;
        }
    }
}
