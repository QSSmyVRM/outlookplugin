﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Text;

namespace MyVrm.WebServices.Maps
{
    public class YahooMapsUri : MapsUri
    {
        private const string BaseAddress = "http://maps.yahoo.com/map";

        public override string ToString()
        {
            var uriBuilder = new UriBuilder(BaseAddress);
            var queryBuilder = new StringBuilder();
            // Latitude, longitude
            if (!string.IsNullOrEmpty(Longitude) && !string.IsNullOrEmpty(Latitude))
            {
                queryBuilder.AppendFormat("&lat={0}&lon={1}", Latitude, Longitude);
            }
            //Query
            if (!string.IsNullOrEmpty(Location))
            {
                queryBuilder.AppendFormat("&q1={0}", Location);
            }
            // Zoom level
            queryBuilder.Append("&zoom=16");
            // Map mode
            queryBuilder.Append("&mvt=m");
            uriBuilder.Query = queryBuilder.ToString();
            return uriBuilder.ToString();
        }
    }
}
