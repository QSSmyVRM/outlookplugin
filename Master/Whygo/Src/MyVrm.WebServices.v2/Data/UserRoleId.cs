﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Reflection;

namespace MyVrm.WebServices.Data
{
    public class UserRoleId : ServiceId
    {
		public override object Clone()
		{
			Type type = GetType();
			ConstructorInfo ctor2 = (ConstructorInfo) type.GetConstructor(BindingFlags.NonPublic | BindingFlags.Instance, null, new[] { typeof(string) }, null).Invoke(new object[] { Id });
			return ctor2;
		}

        #region Overrides of ServiceId

        internal override string GetXmlElementName()
        {
            return "ID";
        }
        #endregion
    }
}
