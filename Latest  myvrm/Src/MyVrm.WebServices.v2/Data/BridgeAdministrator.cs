﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    internal class BridgeAdministrator : ComplexProperty
    {
        public UserId Id { get; set; }

		public override object Clone()
		{
			BridgeAdministrator copy = new BridgeAdministrator();
			copy.Id = new UserId(Id.Id);

			return copy;
		}

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            Id.WriteToXml(writer, "ID");
        }
    }
}
