﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	internal class TDGetSlotAvailableSeatsRequest : ServiceRequestBase<TDGetSlotAvailableSeatsResponse>
	{
		internal DateTime StartDate;
		internal DateTime EndDate;
		internal string ConferenceId;

		internal TDGetSlotAvailableSeatsRequest(MyVrmService service)
			: base(service)
		{
		}

		#region Overrides of ServiceRequestBase

		internal override string GetXmlElementName()
		{
			return "TDGetSlotAvailableSeats";
		}

		internal override string GetCommandName()
		{
			return Constants.TDGetSlotAvailableSeatsCommandName;
		}

		internal override string GetResponseXmlElementName()
		{
			return "TDGetSlotAvailableSeats";
		}

		internal override TDGetSlotAvailableSeatsResponse ParseResponse(MyVrmServiceXmlReader reader)
		{
			var response = new TDGetSlotAvailableSeatsResponse(); 
			response.LoadFromXml(reader, GetResponseXmlElementName());
			return response;
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			UserId.WriteToXml(writer, "userID");
			writer.WriteElementValue(XmlNamespace.NotSpecified, "editConfId", ConferenceId);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "startDate",// "6/22/2013 9:00:00 AM");
				StartDate.ToString("s", DateTimeFormatInfo.InvariantInfo));
			writer.WriteElementValue(XmlNamespace.NotSpecified, "endDate", //"6/22/2013 10:00:00 AM");
			EndDate.ToString("s", DateTimeFormatInfo.InvariantInfo));
			writer.WriteElementValue(XmlNamespace.NotSpecified, "timezone", 33); 
		}
		#endregion
	}

	public class TDGetSlotAvailableSeatsResponse : ServiceResponse
	{
		public AvailSeatsCollection2 AvailSeatsCollection { get; /*internal*/ set; }
		public string RetMessage { set; get; }
        public int RetCode { set; get; }
		public long SettingsVersion { set; get; }
		public XMLError XMLError { set; get; }
		public int seatsNumberTotal { set; get; }

		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{
			int seatsNumber = 0;
			
			DateTime startDate = DateTime.MinValue;
			DateTime endDate = DateTime.MinValue;
			seatsNumberTotal = 0;

			try
			{
				while (!reader.IsEndElement(XmlNamespace.NotSpecified, "TotalOrgSeats"))
				{
					if(reader.LocalName.CompareTo("TotalOrgSeats") == 0)
					{
						seatsNumberTotal = reader.ReadElementValue<int>();
						break;
					}
					reader.Read();					
				}
			}
			catch (Exception ex)
			{
				MyVrmService.TraceSource.TraceError(reader.LocalName + ": " + ex.Message, null);
				reader.Read();
			}
			while (!reader.IsEndElement(XmlNamespace.NotSpecified, "availableSeatsList"))
			{
				try
				{
					switch (reader.LocalName)
					{
						case "startDate":
							DateTime.TryParse(reader.ReadElementValue(), out startDate);
							startDate = startDate.ToLocalTime();
							break;
						case "endDate":
							DateTime.TryParse(reader.ReadElementValue(), out endDate);
							endDate = endDate.ToLocalTime();
							break;
						case "seatsNumber":
							seatsNumber = reader.ReadElementValue<int>();
							break;
						case "availableSeats":
							if (reader.IsEndElement(XmlNamespace.NotSpecified, "availableSeats"))
							{
								AvailSeats2 availSeats = new AvailSeats2();
								availSeats.Seats = seatsNumber;
								availSeats.End = endDate;
								availSeats.Start = startDate;
								if (AvailSeatsCollection == null)
									AvailSeatsCollection = new AvailSeatsCollection2();
								AvailSeatsCollection.Add(availSeats);
							}
							break;
					}
					reader.Read();
				}
				catch (Exception ex)
				{
					MyVrmService.TraceSource.TraceError(reader.LocalName + ": " + ex.Message, null);
					reader.Read();
					//throw;
				}
			}
			while (!reader.IsEndElement(XmlNamespace.NotSpecified, "retState"))
			{
				try
				{
					switch (reader.LocalName)
					{
						case "retCode":
							RetCode = reader.ReadElementValue<int>();
							break;
						case "retMessage":
							RetMessage = reader.ReadElementValue();
							break;
						case "settingsVersion":
							SettingsVersion = reader.ReadElementValue<long>();
							break;
						case "error":
							XMLError = new XMLError();
							XMLError.LoadFromXml(reader, XmlNamespace.NotSpecified, "error");
							break;
					}
					reader.Read();
				}
				catch (Exception ex)
				{
					MyVrmService.TraceSource.TraceError(reader.LocalName + ": " + ex.Message, null);
					reader.Read();
					//throw;
				}
			}
		}
	}
}
