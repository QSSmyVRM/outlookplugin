﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.Outlook.WinForms.Conference
{
    partial class HousekeepingWorkOrdersPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
			if (disposing)
			{
				if (Conference != null)
					Conference.LocationIds.ListChanged -= LocationIds_ListChanged;
				if (_workOrderRecords != null)
					_workOrderRecords.ListChanged -= WorkOrderRecordsListChanged;
			}
			if (disposing && ConferenceBindingSource != null)
				ConferenceBindingSource.DataSourceChanged -= ConferenceBindingSource_DataSourceChanged;

            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HousekeepingWorkOrdersPage));
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.bar = new DevExpress.XtraBars.Bar();
            this.assignWorkOrderBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.editWorkOrderBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.removeWorkOrderBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.workOrderList = new DevExpress.XtraTreeList.TreeList();
            this.roomNameColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.workOrderNameColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.workOrderStatusColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.workOrderStatusRepositoryItemTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.workOrderRepositoryItemTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.roomIdColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.workOrderList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.workOrderRepositoryItemTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.workOrderStatusRepositoryItemTextEdit)).BeginInit();
           // ((System.ComponentModel.ISupportInitialize)(this.workOrderRepositoryItemTextEdit)).BeginInit();
            
            this.SuspendLayout();
            // 
            // barManager
            // 
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Images = this.imageList;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.assignWorkOrderBarButtonItem,
            this.editWorkOrderBarButtonItem,
            this.removeWorkOrderBarButtonItem});
            this.barManager.MaxItemId = 4;
            // 
            // bar
            // 
            this.bar.BarName = "Tools";
            this.bar.DockCol = 0;
            this.bar.DockRow = 0;
            this.bar.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.assignWorkOrderBarButtonItem),
            new DevExpress.XtraBars.LinkPersistInfo(this.editWorkOrderBarButtonItem),
            new DevExpress.XtraBars.LinkPersistInfo(this.removeWorkOrderBarButtonItem)});
            this.bar.OptionsBar.AllowQuickCustomization = false;
            this.bar.OptionsBar.DisableClose = true;
            this.bar.OptionsBar.DisableCustomization = true;
            this.bar.OptionsBar.DrawDragBorder = false;
            this.bar.OptionsBar.UseWholeRow = true;
            this.bar.Text = "Tools";
            // 
            // assignWorkOrderBarButtonItem
            // 
            this.assignWorkOrderBarButtonItem.Caption = "Assign";
            this.assignWorkOrderBarButtonItem.Id = 0;
            this.assignWorkOrderBarButtonItem.ImageIndex = 0;
            this.assignWorkOrderBarButtonItem.Name = "assignWorkOrderBarButtonItem";
            this.assignWorkOrderBarButtonItem.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.assignWorkOrderBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.assignWorkOrderBarButtonItem_ItemClick);
            // 
            // editWorkOrderBarButtonItem
            // 
            this.editWorkOrderBarButtonItem.Caption = "Edit";
            this.editWorkOrderBarButtonItem.Id = 1;
            this.editWorkOrderBarButtonItem.ImageIndex = 1;
            this.editWorkOrderBarButtonItem.Name = "editWorkOrderBarButtonItem";
            this.editWorkOrderBarButtonItem.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.editWorkOrderBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.editWorkOrderBarButtonItem_ItemClick);
            // 
            // removeWorkOrderBarButtonItem
            // 
            this.removeWorkOrderBarButtonItem.Caption = "Remove";
            this.removeWorkOrderBarButtonItem.Id = 3;
            this.removeWorkOrderBarButtonItem.ImageIndex = 2;
            this.removeWorkOrderBarButtonItem.Name = "removeWorkOrderBarButtonItem";
            this.removeWorkOrderBarButtonItem.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.removeWorkOrderBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.removeWorkOrderBarButtonItem_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(467, 30);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 252);
            this.barDockControlBottom.Size = new System.Drawing.Size(467, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 30);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 222);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(467, 30);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 222);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "Create.png");
            this.imageList.Images.SetKeyName(1, "Edit.png");
            this.imageList.Images.SetKeyName(2, "Remove.png");
            // 
            // workOrderList
            // 
            this.workOrderList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.roomNameColumn,
            this.workOrderNameColumn,
            this.workOrderStatusColumn,
            this.roomIdColumn});
            this.workOrderList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.workOrderList.Location = new System.Drawing.Point(0, 30);
            this.workOrderList.Name = "workOrderList";
            this.workOrderList.OptionsView.ShowButtons = false;
            this.workOrderList.OptionsView.ShowHorzLines = false;
            this.workOrderList.OptionsView.ShowIndicator = false;
            this.workOrderList.OptionsView.ShowRoot = false;
            this.workOrderList.OptionsView.ShowVertLines = false;
            this.workOrderList.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.workOrderRepositoryItemTextEdit,
            this.workOrderStatusRepositoryItemTextEdit});
            this.workOrderList.Size = new System.Drawing.Size(467, 222);
            this.workOrderList.TabIndex = 4;
            this.workOrderList.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.workOrderList_FocusedNodeChanged);
            this.workOrderList.DoubleClick += new System.EventHandler(this.workOrderList_DoubleClick);
            // 
            // roomNameColumn
            // 
            this.roomNameColumn.Caption = "Room";
            this.roomNameColumn.FieldName = "RoomName";
            this.roomNameColumn.Name = "roomNameColumn";
            this.roomNameColumn.OptionsColumn.AllowEdit = false;
            this.roomNameColumn.OptionsColumn.AllowFocus = false;
            this.roomNameColumn.OptionsColumn.AllowMove = false;
            this.roomNameColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.roomNameColumn.OptionsColumn.AllowSort = false;
            this.roomNameColumn.OptionsColumn.ReadOnly = true;
            this.roomNameColumn.OptionsColumn.ShowInCustomizationForm = false;
            this.roomNameColumn.Visible = true;
            this.roomNameColumn.VisibleIndex = 0;
            // 
            // workOrderNameColumn
            // 
            this.workOrderNameColumn.Caption = "Work Order";
            this.workOrderNameColumn.ColumnEdit = this.workOrderRepositoryItemTextEdit;
            this.workOrderNameColumn.FieldName = "Name";
            this.workOrderNameColumn.Name = "workOrderNameColumn";
            this.workOrderNameColumn.OptionsColumn.AllowEdit = false;
            this.workOrderNameColumn.OptionsColumn.AllowFocus = false;
            this.workOrderNameColumn.OptionsColumn.AllowMove = false;
            this.workOrderNameColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.workOrderNameColumn.OptionsColumn.AllowSort = false;
            this.workOrderNameColumn.OptionsColumn.ReadOnly = true;
            this.workOrderNameColumn.OptionsColumn.ShowInCustomizationForm = false;
            this.workOrderNameColumn.Visible = true;
            this.workOrderNameColumn.VisibleIndex = 1;

            // 
            // workOrderRepositoryItemTextEdit
            // 
            this.workOrderRepositoryItemTextEdit.AutoHeight = false;
            this.workOrderRepositoryItemTextEdit.Name = "workOrderRepositoryItemTextEdit";
            this.workOrderRepositoryItemTextEdit.NullText = "No assigned work order";
            this.workOrderRepositoryItemTextEdit.ReadOnly = true;
           

            // 
            // workOrderStatusColumn
            // 
            this.workOrderStatusColumn.AppearanceCell.Options.UseTextOptions = true;
            this.workOrderStatusColumn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.workOrderStatusColumn.ColumnEdit = this.workOrderStatusRepositoryItemTextEdit;
            this.workOrderStatusColumn.Caption = "Status";
            this.workOrderStatusColumn.FieldName = "Status";
            this.workOrderStatusColumn.Name = "workOrderStatusColumn";
            this.workOrderStatusColumn.OptionsColumn.AllowEdit = false;
            this.workOrderStatusColumn.OptionsColumn.AllowFocus = false;
            this.workOrderStatusColumn.OptionsColumn.AllowMove = false;
            this.workOrderStatusColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.workOrderStatusColumn.OptionsColumn.AllowSort = false;
            this.workOrderStatusColumn.OptionsColumn.ReadOnly = true;
            this.workOrderStatusColumn.OptionsColumn.ShowInCustomizationForm = false;
            this.workOrderStatusColumn.Visible = true;
            this.workOrderStatusColumn.VisibleIndex = 2;

            // 
            // workOrderStatusRepositoryItemTextEdit
            // 
            this.workOrderStatusRepositoryItemTextEdit.AutoHeight = false;
            this.workOrderStatusRepositoryItemTextEdit.Name = "workOrderStatusRepositoryItemTextEdit";
            this.workOrderStatusRepositoryItemTextEdit.NullText = "Not available";
            this.workOrderStatusRepositoryItemTextEdit.ReadOnly = true;
           

            // 
            // roomIdColumn
            // 
            this.roomIdColumn.Caption = "Room Id";
            this.roomIdColumn.FieldName = "RoomId";
            this.roomIdColumn.Name = "roomIdColumn";
            this.roomIdColumn.OptionsColumn.AllowEdit = false;
            this.roomIdColumn.OptionsColumn.AllowFocus = false;
            this.roomIdColumn.OptionsColumn.AllowMove = false;
            this.roomIdColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.roomIdColumn.OptionsColumn.AllowSort = false;
            this.roomIdColumn.OptionsColumn.ReadOnly = true;
            this.roomIdColumn.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // HousekeepingWorkOrdersPage
            // 
            this.Controls.Add(this.workOrderList);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "HousekeepingWorkOrdersPage";
            this.Size = new System.Drawing.Size(467, 252);
            this.Load += new System.EventHandler(this.HousekeepingWorkOrdersPage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.workOrderList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.workOrderRepositoryItemTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.workOrderStatusRepositoryItemTextEdit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar bar;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem assignWorkOrderBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem editWorkOrderBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem removeWorkOrderBarButtonItem;
        private DevExpress.XtraTreeList.TreeList workOrderList;
        private DevExpress.XtraTreeList.Columns.TreeListColumn roomNameColumn;
        private DevExpress.XtraTreeList.Columns.TreeListColumn workOrderNameColumn;
        private DevExpress.XtraTreeList.Columns.TreeListColumn workOrderStatusColumn;
        private DevExpress.XtraTreeList.Columns.TreeListColumn roomIdColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit workOrderRepositoryItemTextEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit workOrderStatusRepositoryItemTextEdit;
        private System.Windows.Forms.ImageList imageList;
    }
}
