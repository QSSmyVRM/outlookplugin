﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook.WinForms.ManageRooms
{
	public partial class ManageRoomsDialog : Dialog
	{
		private List<WGManagedRoom> _wgRoomList = new List<WGManagedRoom>();
		public ManageRoomsDialog()
		{
			InitializeComponent();
			OkText = Strings.CloseBtnTxt;
			ApplyEnabled = false;
			CancelEnabled = false;
			CancelVisible = false;
			ApplyVisible = false;
			Text = Strings.ManageRoomsTxt;
			if (roomList.Items.Count > 0)
				roomList.SelectedIndex = 0;
		}

		public void SetRoomList(List<WGManagedRoom> WGRoomList)
		{
			roomList.Items.Clear();
			_wgRoomList.Clear();
			_wgRoomList.AddRange(WGRoomList);

			if(WGRoomList != null && WGRoomList.Count > 0)
			{
				foreach (var wgManagedRoom in WGRoomList)
				{
					roomList.Items.Add(wgManagedRoom.RoomName);
				}
			}
			if (roomList.Items.Count > 0)
				roomList.SelectedIndex = 0;
		}

		private void addRoomBtn_Click(object sender, EventArgs e)
		{
			using (var createDlg = new CreateEditRoomDialog())
			{
				createDlg.RoomId = -1;
				createDlg.RoomType = WGRoomType.NoType;
				if( createDlg.ShowDialog() == DialogResult.OK)
				{
					GetPrivatePublicRoomIDResponse response = MyVrmService.Service.GetPrivatePublicRoomID();
					if (response != null && response.WGRoomList != null && response.WGRoomList.Count > 0)
						SetRoomList(response.WGRoomList);
				}
			}
		}

		private void editRoomBtn_Click(object sender, EventArgs e)
		{
			if (roomList.Items.Count > 0 && roomList.SelectedIndex != -1)
			{
				using (var editDlg = new CreateEditRoomDialog())
				{
					//Get selected room ID
					WGManagedRoom selectedRoom = _wgRoomList.Find(room => room.RoomName.CompareTo(roomList.SelectedItem) == 0);

					string oldName = roomList.SelectedItem.ToString();
					editDlg.RoomId = selectedRoom != null ? selectedRoom.RoomID : -1;
					editDlg.RoomType = selectedRoom != null ? selectedRoom.RoomType : WGRoomType.NoType;

					if( editDlg.ShowDialog() == DialogResult.OK)
					{
						GetPrivatePublicRoomIDResponse response = MyVrmService.Service.GetPrivatePublicRoomID();
						if (response != null && response.WGRoomList != null && response.WGRoomList.Count > 0)
							SetRoomList(response.WGRoomList);

						roomList.SelectedIndex = roomList.Items.IndexOf(oldName);
						if (roomList.SelectedIndex == -1)
							roomList.SelectedIndex = 0;
					}
				}
			}
		}

		private void removeRoomBtn_Click(object sender, EventArgs e)
		{
			if (roomList.Items.Count > 0 && roomList.SelectedIndex != -1)
			{
				string txtMsg = string.Format(Strings.RemoveRoomQuestionTxt, roomList.SelectedValue);
				if( UIHelper.ShowMessage(txtMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes )
				{
					//Delete room from server
					var del = _wgRoomList.Find(a => a.RoomName == (string) roomList.SelectedValue);
					if (del != null)
						MyVrmService.Service.DeleteRoom(new RoomId(del.RoomID.ToString()));
					//Delete room from list
					roomList.Items.RemoveAt(roomList.SelectedIndex);
				}
			}
		}

		private void servicePageLink_OpenLink(object sender, EventArgs e)
		{
			servicePageLink.ShowBrowser("http://www.videoconferencingbookingsystem.com");
		}

		private void ManageRoomsDialog_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (((ManageRoomsDialog)sender).DialogResult != DialogResult.OK)
				return;
		}
	}
}
