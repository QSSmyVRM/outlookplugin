﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.WebServices.Data
{
    public class AudioUser
    {
        internal AudioUser()
        {
        }

        public UserId Id { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string Email { get; private set; }
        public string Login { get; private set; }
        public bool AudioAddon { get; private set; }

        public override string ToString()
        {
            if (!string.IsNullOrEmpty(FirstName) && !string.IsNullOrEmpty(LastName))
            {
                return FirstName + " " + LastName;
            }
            if (!string.IsNullOrEmpty(Email))
            {
                return Email;
            }
            if (!string.IsNullOrEmpty(Login))
            {
                return Login;
            }
            if (Id != null)
            {
                return Id.ToString();
            }
            return base.ToString();
        }

        internal void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
        {
            do
            {
                reader.Read();
                switch (reader.LocalName)
                {
                    case "userID":
                        Id = new UserId();
                        Id.LoadFromXml(reader, "userID");
                        break;
                    case "firstName":
                        FirstName = reader.ReadElementValue();
                        break;
                    case "lastName":
                        LastName = reader.ReadElementValue();
                        break;
                    case "userEmail":
                        Email = reader.ReadElementValue();
                        break;
                    case "login":
                        Login = reader.ReadElementValue();
                        break;
                    case "audioaddon":
                        AudioAddon = reader.ReadElementValue<bool>();
                        break;
                    default:
                        reader.SkipCurrentElement();
                        break;
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, xmlElementName));
        }
    }
}
