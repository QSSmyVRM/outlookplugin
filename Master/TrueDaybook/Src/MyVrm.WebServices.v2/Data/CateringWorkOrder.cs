﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;

namespace MyVrm.WebServices.Data
{
    /// <summary>
    /// Represents a catering work order.
    /// </summary>
    [ServiceObjectDefinition("Workorder")]
    public class CateringWorkOrder : WorkOrderBase
    {
		public new /*override*/ object Clone()//CopyTo(CateringWorkOrder workOrder)
		{
			//if (workOrder == null)
			CateringWorkOrder workOrder = new CateringWorkOrder(Service);

			workOrder.CateringServiceId = CateringServiceId;
			workOrder.Comments = Comments;
			workOrder.DeliveryTime = DeliveryTime;

			if (Id != null)
			{
				WorkOrderId copyId = new WorkOrderId(Id.Id);
				//Id.SetFieldValue(ref copyId, Id);
				workOrder.PropertyBag[WorkOrderBaseSchema.Id] = copyId;
			}

			CateringWorkOrderMenuCollection copyMenus = null;
			if (Menus != null)
			{
				copyMenus = new CateringWorkOrderMenuCollection();
				foreach (var item in Menus.Items)
				{
					CateringWorkOrderMenu copyItem = //new CateringWorkOrderMenu();
					(CateringWorkOrderMenu)item.Clone();
					copyMenus.InternalAdd(copyItem);
				}
			}
			workOrder.PropertyBag[CateringWorkOrderSchema.Menus] = copyMenus;
			
			workOrder.Price = Price;
			workOrder.RoomId = RoomId;
			return workOrder;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(this, obj))
				return true;

			CateringWorkOrder workOrder = obj as CateringWorkOrder;
			if (workOrder == null)
				return false;

			if (workOrder.CateringServiceId != CateringServiceId)
				return false;
			if (workOrder.Comments != Comments)
				return false;
			if (workOrder.DeliveryTime != DeliveryTime)
				return false;
			if (workOrder.Id != Id)
				return false;
			if( workOrder.Menus != null && Menus == null ||
				workOrder.Menus == null && Menus != null )
				return false;
			if (workOrder.Menus != null && workOrder.Menus.Items != null && Menus != null && Menus.Items == null )
				return false;
			if (workOrder.Menus != null && workOrder.Menus.Items == null && Menus != null && Menus.Items != null )
				return false;
			if (workOrder.Menus != null && workOrder.Menus.Items != null && Menus != null && Menus.Items != null &&
					workOrder.Menus.Items.Count != Menus.Items.Count)
				return false;
			if (Menus != null && Menus.Items != null && workOrder.Menus != null && workOrder.Menus.Items != null)
			{
				for (int i = 0; i < Menus.Items.Count; i++)
				{
					if (!Menus.Items[i].Equals(workOrder.Menus.Items[i]))
						return false;
				}
			}
			if (workOrder.Price != Price)
				return false;
			if (workOrder.RoomId != RoomId)
				return false;
			
			return true;
		}
        public CateringWorkOrder(MyVrmService service) : base(service)
        {
        }

        public int CateringServiceId
        {
            get
            {
                int iRet = 0;
				if (PropertyBag[CateringWorkOrderSchema.CateringServiceId] != null)
					int.TryParse(PropertyBag[CateringWorkOrderSchema.CateringServiceId].ToString(), out iRet);
				return iRet; //(int)PropertyBag[CateringWorkOrderSchema.CateringServiceId];
            }
            set
            {
                PropertyBag[CateringWorkOrderSchema.CateringServiceId] = value;
            }
        }

        public string Comments
        {
            get
            {
                return (string)PropertyBag[CateringWorkOrderSchema.Comments];
            }
            set
            {
                PropertyBag[CateringWorkOrderSchema.Comments] = value;
            }
        }

        public DateTime DeliveryTime
        {
            get
            {
            	DateTime dt = (DateTime) PropertyBag[CateringWorkOrderSchema.DeliverByDate];
            	TimeSpan ts = (TimeSpan) PropertyBag[CateringWorkOrderSchema.DeliverByTime];

				return dt.Date + ts;
            }
            set
            {
                PropertyBag[CateringWorkOrderSchema.DeliverByDate] = value.Date;
                PropertyBag[CateringWorkOrderSchema.DeliverByTime] = value.TimeOfDay;
            }
        }

        public decimal Price
        {
            get
            {
                return (decimal)PropertyBag[CateringWorkOrderSchema.Price];
            }
            set
            {
                PropertyBag[CateringWorkOrderSchema.Price] = value;
            }
        }

        public CateringWorkOrderMenuCollection Menus
        {
            get
            {
                return (CateringWorkOrderMenuCollection)PropertyBag[CateringWorkOrderSchema.Menus];
            }
        }

        #region Overrides of ServiceObject

        internal override ServiceObjectSchema GetSchema()
        {
            return CateringWorkOrderSchema.Instance;
        }
        #endregion
		
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
    }
}
