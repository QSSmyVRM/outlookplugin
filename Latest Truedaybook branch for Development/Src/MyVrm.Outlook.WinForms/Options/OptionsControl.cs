using System;
using System.Windows.Forms;
using DevExpress.XtraLayout.Utils;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook.WinForms.Options
{
    public partial class OptionsControl : Page
    {
        public delegate void OptionsChangedHandler(object sender, EventArgs args);

        public OptionsControl()
        {
            InitializeComponent();
            labelControl3.Text = WinForms.Strings.ClearRoomLocalCacheLableText;
            labelControl2.Text = WinForms.Strings.ChangeConferenceOptionsLableText;
			labelControl1.Text = WinForms.Strings.ConferenceLableText;
            layoutControlItem14.CustomizationFormText = WinForms.Strings.LocalCacheCustomizationFormText;
            conferenceOptionsButton.Text = WinForms.Strings.ConferenceOptionsLableText + "...";
            connectionOptionsLabel.Text = WinForms.Strings.SetUpConnectionSettingsLableText;
            connectionOptionsGroupLabel.Text = WinForms.Strings.WebServicesLableText;
            connectionOptionsButton.Text = WinForms.Strings.ConnectionOptionsLableText + "...";
            clearCacheButton.Text = WinForms.Strings.ClearCacheLableText;
            aboutButton.Text = WinForms.Strings.AboutLableText;
        }


		public void External_OnApplying(System.ComponentModel.CancelEventArgs e)
		{
			OnApplying(e);
		}
        protected override void OnApplying(System.ComponentModel.CancelEventArgs e)
        {
            MyVrmAddin.Instance.Settings.Save();
            UIHelper.Instance.Settings.Save();
            e.Cancel = false;
        }

        public event OptionsChangedHandler OptionsChanged;

        void InvokeOptionsChanged(EventArgs args)
        {
            OptionsChangedHandler changed = OptionsChanged;
            if (changed != null) changed(this, args);
        }

        private void aboutButton_Click(object sender, EventArgs e)
        {
            using(var dlg = new AboutDialog())
            {
                dlg.ShowDialog(this);
            }
        }

        private void OptionsControl_Load(object sender, EventArgs e)
        {
			clearCacheButton.Visible = false;
			labelControl3.Visible = false;
			layoutControlItem8.Visibility = LayoutVisibility.Never;
			layoutControlItem14.Visibility = LayoutVisibility.Never;
        }

        private void clearCacheButton_Click(object sender, EventArgs e)
        {
            try
            {
                var cursor = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                try
                {
                    MyVrmAddin.Instance.ClearRoomCache();
                }
                finally
                {
                    Cursor.Current = cursor;    
                }
            }
            catch (Exception exception)
            {
                ShowError(exception.Message);
            }
        }

		private void connectionOptionsButton_Click(object sender, EventArgs e)
		{
			using (var dialog = new ConnectionOptionsDialog())
			{
				var settings = MyVrmAddin.Instance.Settings;
				dialog.ServiceUrl = settings.MyVrmServiceUrl;//"http://daybook.myvrm.com";
				dialog.AuthenticationMode = AuthenticationMode.Custom;//settings.AuthenticationMode;
				dialog.UseDefaultCredential = settings.UseDefaultCredentials;
			    dialog.UserDomain = settings.UserDomain;
				dialog.UserName = settings.UserName;
				dialog.UserPassword = settings.UserPassword;
				if (dialog.ShowDialog(this) == DialogResult.OK)
				{
					settings.MyVrmServiceUrl = dialog.ServiceUrl;
					settings.AuthenticationMode = dialog.AuthenticationMode;
					settings.UseDefaultCredentials = dialog.UseDefaultCredential;
					settings.UserDomain = dialog.UserDomain;
					settings.UserName = dialog.UserName;
					settings.UserPassword = dialog.UserPassword;
					InvokeOptionsChanged(EventArgs.Empty);
				}
			}
		}

		private void conferenceOptionsButton_Click(object sender, EventArgs e)
		{
            using(var dialog = new ConferenceOptionsDialog())
            {
				dialog.ShowPopupConferenceIsScheduled = UIHelper.Instance.Settings.ShowPopupWhenConferenceIsScheduled;
				dialog.AppendConferenceDetailsToMeeting = UIHelper.Instance.Settings.AppendConferenceDetailsToMeeting;
				dialog.DeletionOptionValue = UIHelper.Instance.Settings.DeletionOptionValue;

                if (dialog.ShowDialog(this) == DialogResult.OK)
                {
                    UIHelper.Instance.Settings.ShowPopupWhenConferenceIsScheduled =
                        dialog.ShowPopupConferenceIsScheduled;
                    UIHelper.Instance.Settings.AppendConferenceDetailsToMeeting =
                        dialog.AppendConferenceDetailsToMeeting;
					UIHelper.Instance.Settings.DeletionOptionValue = dialog.DeletionOptionValue;
                    InvokeOptionsChanged(EventArgs.Empty);
                }
            }
        }
    }
}