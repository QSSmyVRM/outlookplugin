﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.WebServices.Data
{
    public enum EndpointType
    {
        Endpoint,
        UserEndpoint,
        RoomEndpoint
    }

    /// <summary>
    /// Represents an endpoint.
    /// </summary>
    [ServiceObjectDefinition("Endpoint")]
    public class Endpoint : ServiceObject
    {
        public Endpoint(MyVrmService service) : base(service)
        {
        }

        public EndpointId Id
        {
            get
            {
                return (EndpointId) PropertyBag[EndpointSchema.Id];
            }
            internal set
            {
                PropertyBag[EndpointSchema.Id] = value;
            }
        }

        public string Name
        {
            get
            {
                return (string) PropertyBag[EndpointSchema.Name];
            }
            set
            {
                PropertyBag[EndpointSchema.Name] = value;
            }
        }

        public EndpointType Type
        {
            get
            {
                return (EndpointType) PropertyBag[EndpointSchema.Type];
            }
            set
            {
                PropertyBag[EndpointSchema.Type] = value;
            }
        }

        public EndpointProfileCollection Profiles
        {
            get
            {
                return (EndpointProfileCollection) PropertyBag[EndpointSchema.Profiles];
            }
        }

        internal EndpointProfileId DefaultProfileId
        {
            get
            {
                return (EndpointProfileId)PropertyBag[EndpointSchema.DefaultProfileId];
            }
        }

        public override string ToString()
        {
            if (Name != null)
            {
                return Name;
            }
            return base.ToString();
        }

        public void Save()
        {
            Service.SaveEndpoint(this);
        }

        #region Overrides of ServiceObject

        internal override ServiceObjectSchema GetSchema()
        {
            return EndpointSchema.Instance;
        }

        internal override PropertyDefinition GetIdPropertyDefinition()
        {
            return EndpointSchema.Id;
        }

        #endregion

    }
}
