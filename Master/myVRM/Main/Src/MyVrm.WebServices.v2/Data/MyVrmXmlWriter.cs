﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.IO;
using System.Text;
using System.Xml;

namespace MyVrm.WebServices.Data
{
    internal class MyVrmXmlWriter : IDisposable
    {
        private readonly MyVrmService _service;
        private readonly XmlWriter _xmlWriter;
        private bool _isDisposed;
        private static readonly Encoding Utf8Encoding;

        static MyVrmXmlWriter()
        {
            Utf8Encoding = new UTF8Encoding(false);
        }

        internal MyVrmXmlWriter(MyVrmService service, Stream stream)
        {
            _service = service;
            var xmlWriterSettings = new XmlWriterSettings
                                        {
                                            OmitXmlDeclaration = true,
                                            Indent = true,
                                            Encoding = Utf8Encoding,
                                            ConformanceLevel = ConformanceLevel.Fragment,
                                        };
            _xmlWriter = XmlWriter.Create(stream, xmlWriterSettings);
        }

        public MyVrmService Service
        {
            get { return _service; }
        }

        #region Implementation of IDisposable

        public void Dispose()
        {
            if (!_isDisposed)
            {
                _xmlWriter.Close();
            }
            _isDisposed = true;
        }

        #endregion

        public void Flush()
        {
            _xmlWriter.Flush();
        }

        public void WriteElementValue(XmlNamespace xmlNamespace, string localName, object value)
        {
            string strValue;
            if (!TryConvertObjectToString(value, out strValue))
            {
                throw new ServiceXmlSerializationException(string.Format(Strings.ElementValueCannotBeSerialized,
                                                                         value.GetType().Name, localName));
            }
            WriteStartElement(xmlNamespace, localName);
            WriteValue(strValue ?? string.Empty);
            WriteEndElement();
        }

        public void WriteStartElement(XmlNamespace xmlNamespace, string localName)
        {
            _xmlWriter.WriteStartElement(Utilities.GetNamespacePrefix(xmlNamespace), localName,
                                         Utilities.GetNamespaceUri(xmlNamespace));
        }

        public void WriteEndElement()
        {
            _xmlWriter.WriteEndElement();
        }

        public void WriteValue(string value)
        {
            _xmlWriter.WriteValue(value);
        }

        internal bool TryConvertObjectToString(object value, out string strValue)
        {
            strValue = null;
            if (value != null)
            {
                var convertible = value as IConvertible;
                if (value.GetType().IsEnum)
                {
                    strValue = Utilities.SerializeEnum((Enum)value);
                    return true;
                }
                if (convertible == null)
                {
                    var formattable = value as IFormattable;
                    if (formattable != null)
                    {
                        strValue = formattable.ToString(null, null);
                        return true;
                    }
                    return false;
                }
                TypeCode typeCode = convertible.GetTypeCode();
                if (typeCode != TypeCode.Boolean)
                {
                    if (typeCode == TypeCode.DateTime)
                    {
                        strValue = Utilities.DateTimeToString((DateTime)value);
                        return true;
                    }
                    strValue = convertible.ToString();
                    return true;
                }
                strValue = Utilities.BoolToBoolString((bool) value);
            }
            return true;
        }
    }
}
