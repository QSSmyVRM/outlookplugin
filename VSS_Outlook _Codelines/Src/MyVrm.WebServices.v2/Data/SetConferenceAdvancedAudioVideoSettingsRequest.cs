﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    internal class SetConferenceAdvancedAudioVideoSettingsRequest : ServiceRequestBase<SetConferenceAdvancedAudioVideoSettingsResponse>
    {
        internal SetConferenceAdvancedAudioVideoSettingsRequest(MyVrmService service) : base(service)
        {
        }

        internal ConferenceId ConferenceId { get; set; }
        internal ConferenceAdvancedAudioVideoSettings Settings { get; set; }

        #region Overrides of ServiceRequestBase<SetConferenceAdvancedAudioVideoSettingsResponse>

        internal override string GetXmlElementName()
        {
            return "SetAdvancedAVSettings";
        }

        internal override string GetCommandName()
        {
            return "SetAdvancedAVSettings";
        }

        internal override string GetResponseXmlElementName()
        {
            return "success";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            OrganizationId.WriteToXml(writer);
            UserId.WriteToXml(writer, "UserID");
            ConferenceId.WriteToXml(writer, "ConfID");
            Settings.WriteElementsToXml(writer);
        }

        #endregion
    }
}
