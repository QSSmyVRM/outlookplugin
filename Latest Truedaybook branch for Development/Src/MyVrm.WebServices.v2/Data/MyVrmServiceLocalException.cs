﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Runtime.Serialization;

namespace MyVrm.WebServices.Data
{
    /// <summary>
    /// Represents an error that occurs when a service operation fails locally.
    /// </summary>
    [Serializable]
    public class MyVrmServiceLocalException : Exception
    {
        public MyVrmServiceLocalException()
        {
        }

        public MyVrmServiceLocalException(string message) : base(message)
        {
        }

        public MyVrmServiceLocalException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public MyVrmServiceLocalException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
