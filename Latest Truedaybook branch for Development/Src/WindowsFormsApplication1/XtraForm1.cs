using System;
using DevExpress.XtraEditors;
using MyVrm.Outlook.WinForms;
using MyVrm.Outlook.WinForms.Calendar;
using MyVrm.Outlook.WinForms.Conference;
using MyVrm.WebServices.Data;

namespace WindowsFormsApplication1
{
    public partial class XtraForm1 : XtraForm
    {
        private OutlookAppointmentImpl _appointmentImpl;
        private ConferenceWrapper _conferenceWrapper;

        public XtraForm1()
        {
            InitializeComponent();
            _appointmentImpl = new OutlookAppointmentImpl(new OutlookRecipientImpl("Anton Chochia", "achochia@logic-lab.com"))
                                   {
                                       Subject = "test conference",
                                       Body = "",
                                       ConferenceId = null,
                                       Start = new DateTime(2010, 12, 2, 18, 0, 0, DateTimeKind.Local)
                                   };
            _appointmentImpl.End = _appointmentImpl.Start + TimeSpan.FromHours(1);
            //_appointmentImpl.AddRecipient(new OutlookRecipientImpl("Antyl", "antyl@mail.ru"));
            //_appointmentImpl.AddRecipient(new OutlookRecipientImpl("Anton", "achochia@hotmail.com"));
            //_appointmentImpl.IsRecurring = true;
            barEditItem1.EditValue = _appointmentImpl.Start.Date;
            barEditItem2.EditValue = _appointmentImpl.Start.Date;
            barEditItem3.EditValue = _appointmentImpl.End;
            //conferenceControl1.Conference =
            //    new ConferenceWrapper(Conference.Bind(MyVrmService.Service, new ConferenceId("331,1")), _appointmentImpl);
            _conferenceWrapper = new ConferenceWrapper(_appointmentImpl, MyVrmService.Service);
            conferenceControl1.Conference = _conferenceWrapper;
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                _conferenceWrapper.Save();
            }
            catch (SaveConferenceCancelledException )
            {
            }
            catch(Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
        }

        private void barEditItem1_EditValueChanged(object sender, EventArgs e)
        {
            if (barEditItem2.EditValue != null)
                _appointmentImpl.Start = (DateTime) barEditItem1.EditValue + ((DateTime)barEditItem2.EditValue).TimeOfDay;
            if (barEditItem3.EditValue != null)
                _appointmentImpl.End = (DateTime)barEditItem1.EditValue + ((DateTime)barEditItem3.EditValue).TimeOfDay;
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (var dlg = new AboutDialog())
            {
                dlg.ShowDialog();
            }
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
        }

        private void barButtonItem4_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (var dlg = new RoomsCalendarDialog())
            {
                dlg.ShowDialog(this);
            }
        }
    }
}