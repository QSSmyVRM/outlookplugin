// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

#pragma once

#include <initguid.h>
#include <comdef.h>
#include <intsafe.h>

#define USES_IID_IMAPIProp
#define USES_IID_IMAPIFolder
#define USES_IID_IMAPITable
#define USES_IID_IMAPISession
#define USES_IID_IMsgStore

#include <MAPIX.h>

#define INSP_ONEOFFFLAGS     0xD000000
#define INSP_PROPDEFINITION  0x2000000

using namespace System;
#include "MapiPtrs.h"
#include "MapiTags.h"