﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace MyVrm.WebServices.Data
{
    internal class GetConferenceResponse : ServiceResponse
    {
        internal Conference Conference { get; set; }

        internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
        {
            do
            {
                reader.Read();
                if (reader.IsStartElement(XmlNamespace.NotSpecified, "confInfo"))
                {
                    Conference.LoadFromXml(reader, true);
                }
                else
                {
                    if (reader.IsStartElement(XmlNamespace.NotSpecified, "userInfo"))
                    {
                        do
                        {
                            reader.Read();
                            if (reader.LocalName == "Secured")
                            {
                                Conference.IsSecuredEnabledForUser = reader.ReadElementValue<bool>();
                            }
                            else
                                reader.SkipCurrentElement();
                        } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "userInfo"));
                    }
                    else
                        reader.SkipCurrentElement();
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "conference"));
        }
    }
}
