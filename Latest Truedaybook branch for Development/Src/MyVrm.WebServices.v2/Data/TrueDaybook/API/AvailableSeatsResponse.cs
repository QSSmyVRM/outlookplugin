﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;

namespace CallWebApi.Classes
{
    /// <summary>
    /// Response with reserved seats  of company for some period
    /// </summary>
    public class AvailableSeatsResponse : BaseResponse
    {
        /// <summary>
        /// Conference, that reserve seats for some period
        /// </summary>
        public class Reservation
        {
            /// <summary>
            /// Date of start reservation
            /// </summary>
            public DateTime Date { set; get; }

            /// <summary>
            /// Available seats
            /// </summary>
            public int Seats { set; get; }
        }

        /// <summary>
        /// List with reserved conferences
        /// </summary>
        public List<Reservation> Reservations { set; get; }
    }
}