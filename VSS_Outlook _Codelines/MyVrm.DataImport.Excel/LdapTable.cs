﻿using Microsoft.Office.Interop.Excel.Extensions;
using MyVrm.DataImport.Excel.Extensions;
using MyVrm.WebServices.Data;
using ListObject=Microsoft.Office.Tools.Excel.ListObject;

namespace MyVrm.DataImport.Excel
{
    public class LdapTable : ExcelTable
    {
        private readonly ExcelTableColumn _serverIpAddressColumn;
        private readonly ExcelTableColumn _loginColumn;
        private readonly ExcelTableColumn _passwordColumn;
        private readonly ExcelTableColumn _portColumn;
        private readonly ExcelTableColumn _timeoutColumn;
        private readonly ExcelTableColumn _scheduleColumn;

        public LdapTable(ListObject listObject, MyVrmService service, ValidationListManager validationListManager) : base(listObject, service, validationListManager)
        {
            var listColumns = ListObject.ListColumns;

            _serverIpAddressColumn = new ExcelTableColumn(listColumns[1]){IsRequired = true};
            _loginColumn = new ExcelTableColumn(listColumns[2]){IsRequired = true};
            _passwordColumn = new ExcelTableColumn(listColumns[3]){IsRequired = true};
            _portColumn = new ExcelTableColumn(listColumns[4], typeof(int)){IsRequired = false};
            _timeoutColumn = new ExcelTableColumn(listColumns[5], typeof(int)){IsRequired = true};
            _scheduleColumn = new ExcelTableColumn(listColumns[6]){IsRequired = true};

            TableColumns.AddRange(new[]
                                      {
                                          _serverIpAddressColumn, _loginColumn, _passwordColumn, _portColumn,
                                          _timeoutColumn, _scheduleColumn
                                      });
            Initialize();
        }

        #region Overrides of ExcelTable

        protected override void Save(SaveErrorCollection errors)
        {
            foreach (var listRow in ListObject.ListRows.Items())
            {
                var settings = Service.GetConfigurationSettings();
                var ldapSettings = settings.Ldap;
                var range = listRow.Range;
                ldapSettings.Server = range.Item(1, _serverIpAddressColumn.Index).ValueAsString();
                ldapSettings.Login = range.Item(1, _loginColumn.Index).ValueAsString();
                ldapSettings.Password = range.Item(1, _passwordColumn.Index).ValueAsString();
                ldapSettings.Port = range.Item(1, _portColumn.Index).ValueAsInt();
                ldapSettings.Timeout = range.Item(1, _timeoutColumn.Index).ValueAsInt();
                Service.SaveConfigurationSettings(settings);
                break;
            }
        }

        #endregion
    }
}
