﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;

namespace MyVrm.WebServices.Data
{
    public enum MediaTypeFilter
    {
        All = -1,
        None = MediaType.NoAudioVideo,
        Audio = MediaType.AudioOnly,
        Video = MediaType.AudioVideo
    }

    internal class GetAvailableRoomsRequest : ServiceRequestBase<GetAvailableRoomsResponse>
    {

        internal ConferenceId ConferenceId { get; set; }
        internal bool Immediate { get; set; }
        internal bool Recurring { get; set; }
        internal DateTime Start { get; set; }
        internal TimeZoneInfo TimeZone { get; set; }
        internal TimeSpan Duration { get; set; }
        internal MediaTypeFilter MediaType { get; set; }
        internal ConferenceType ConferenceType { get; set; }

        public GetAvailableRoomsRequest(MyVrmService service) : base(service)
        {
            MediaType = MediaTypeFilter.All;
            ConferenceType = ConferenceType.AudioVideo;
            Start = DateTime.MinValue;
            TimeZone = TimeZoneInfo.Local;
            Duration = TimeSpan.Zero;
        }

        internal override string GetXmlElementName()
        {
            return "conferenceTime";
        }

        internal override string GetCommandName()
        {
            return "GetAvailableRoom";
        }

        internal override string GetResponseXmlElementName()
        {
            return "locationList";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            UserId.WriteToXml(writer);
            ConferenceId.WriteToXml(writer);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "immediate", Immediate);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "recurring", Recurring);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "startDate", Utilities.DateToString(Start.Date));
            writer.WriteElementValue(XmlNamespace.NotSpecified, "startHour", Utilities.HourToString(Start.Hour));
            writer.WriteElementValue(XmlNamespace.NotSpecified, "startMin", Start.Minute);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "startSet", Utilities.GetNoonAbbr(Start.TimeOfDay));
            writer.WriteElementValue(XmlNamespace.NotSpecified, "timeZone", TimeZoneConvertion.ConvertToTimeZoneId(TimeZone));
            writer.WriteElementValue(XmlNamespace.NotSpecified, "dirationMin", Duration.TotalMinutes);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "serviceType", -1);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "selected", "");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "mediaType", MediaType);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "confType", ConferenceType);
        }

    }
}
