﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MyVrm.Outlook.WinForms.Options
{
	public partial class OptionsAsDlg : MyVrm.Outlook.WinForms.Dialog
	{
		public OptionsAsDlg()
		{
			InitializeComponent();
            Text = WinForms.Strings.OptionsLableText;
			ApplyVisible = false;
			ApplyEnabled = false;
			CancelEnabled = true;
			CancelVisible = true;
			OkEnabled = true;
			OkVisible = true;
		}
	}
}
