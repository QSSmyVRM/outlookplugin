﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	internal class TDGetAvailableSeatsRequest : ServiceRequestBase<TDGetAvailableSeatsResponse>
	{
		internal DateTime StartDate;
		internal DateTime EndDate;
		internal string ConferenceId;

		internal TDGetAvailableSeatsRequest(MyVrmService service)
			: base(service)
		{
		}

		#region Overrides of ServiceRequestBase

		internal override string GetXmlElementName()
		{
			return "TDGetAvailableSeats";
		}

		internal override string GetCommandName()
		{
			return Constants.TDGetAvailableSeatsCommandName;
		}

		internal override string GetResponseXmlElementName()
		{
			return "TDGetAvailableSeats";
		}

		internal override TDGetAvailableSeatsResponse ParseResponse(MyVrmServiceXmlReader reader)
		{
			var response = new TDGetAvailableSeatsResponse(); 
			response.LoadFromXml(reader, GetResponseXmlElementName());
			return response;
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			UserId.WriteToXml(writer, "userID");
			writer.WriteElementValue(XmlNamespace.NotSpecified, "startDate", StartDate.ToString("s", DateTimeFormatInfo.InvariantInfo));
			writer.WriteElementValue(XmlNamespace.NotSpecified, "endDate", EndDate.ToString("s", DateTimeFormatInfo.InvariantInfo));
			writer.WriteElementValue(XmlNamespace.NotSpecified, "conferenceId", ConferenceId);
		}
		#endregion
	}

	public class TDGetAvailableSeatsResponse : ServiceResponse
	{
		public AvailSeatsCollection2 AvailSeatsCollection { get; internal set; }
		public string RetMessage { set; get; }
        public int RetCode { set; get; }
		public long SettingsVersion { set; get; }
		public XMLError XMLError { set; get; }

		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{
			int seatsNumber = 0;
			DateTime startDate = DateTime.MinValue;
			DateTime endDate = DateTime.MinValue;

			while (!reader.IsEndElement(XmlNamespace.NotSpecified, "availableSeatsList"))
			{
				try
				{
					switch (reader.LocalName)
					{
						case "startDate":
							DateTime.TryParse(reader.ReadElementValue(), out startDate);
							startDate = startDate.ToLocalTime();
							break;
						case "endDate":
							DateTime.TryParse(reader.ReadElementValue(), out endDate);
							endDate = endDate.ToLocalTime();
							break;
						case "seatsNumber":
							seatsNumber = reader.ReadElementValue<int>();
							break;
						case "availableSeats":
							if (reader.IsEndElement(XmlNamespace.NotSpecified, "availableSeats"))
							{
								//AvailSeats availSeats = new AvailSeats();
								//availSeats.Seats = seatsNumber;
								//availSeats.TimeSpan = endDate - startDate;
								//if (AvailSeatsCollection == null)
								//    AvailSeatsCollection = new AvailSeatsCollection();
								AvailSeats2 availSeats = new AvailSeats2();
								availSeats.Seats = seatsNumber;
								availSeats.End = endDate;
								availSeats.Start = startDate;
								if (AvailSeatsCollection == null)
									AvailSeatsCollection = new AvailSeatsCollection2();
								AvailSeatsCollection.Add(availSeats);
							}
							break;
					}
					reader.Read();
				}
				catch (Exception ex)
				{
					MyVrmService.TraceSource.TraceError(reader.LocalName + ": " + ex.Message, null);
					reader.Read();
					//throw;
				}
			}
			while (!reader.IsEndElement(XmlNamespace.NotSpecified, "retState"))
			{
				try
				{
					switch (reader.LocalName)
					{
						case "retCode":
							RetCode = reader.ReadElementValue<int>();
							break;
						case "retMessage":
							RetMessage = reader.ReadElementValue();
							break;
						case "settingsVersion":
							SettingsVersion = reader.ReadElementValue<long>();
							break;
						case "error":
							XMLError = new XMLError();
							XMLError.LoadFromXml(reader, XmlNamespace.NotSpecified, "error");
							break;
					}
					reader.Read();
				}
				catch (Exception ex)
				{
					MyVrmService.TraceSource.TraceError(reader.LocalName + ": " + ex.Message, null);
					reader.Read();
					//throw;
				}
			}
		}
	}
}
