﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using MyVrm.Common.Collections;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook.WinForms.Conference
{
    public partial class InventoryWorkOrderDialog : Dialog
    {
        
        public int InventoryWOAdmin = 0;//ZD 103364
        private class WorkOrderItemRecord
        {
            private readonly WorkOrderItem _workOrderItem;
            public readonly InventorySetItem _inventorySetItem;
            private System.Drawing.Image _itemImage;

            internal WorkOrderItemRecord(WorkOrderItem workOrderItem, InventorySetItem inventorySetItem)
            {
                _workOrderItem = workOrderItem;
                _inventorySetItem = inventorySetItem;
                RequestedQuantity = _workOrderItem.Quantity;
            }
            // public List<InventoryCharges> InventoryCharges {get;set;}

            public List<InventoryCharges> InventoryCharges
            {
                
                get 
                { 
                    _workOrderItem.InventoryCharges = _inventorySetItem.InventoryCharges;
                    return _workOrderItem.InventoryCharges;
                }
                set 
                {  _workOrderItem.InventoryCharges = _inventorySetItem.InventoryCharges;}
            }
            
            //public List<InventoryCharges> GEtInventoryCharges
            //{
            //    get 
            //    {
            //        return _inventorySetItem.InventoryCharges;
            //    }
            //    set
            //    {

            //    }
            //}
            public int WOAdminID
            {
                get
                {
                    return _workOrderItem.Id;
                }
            }

            public int InventoryItemId
            {
                get
                {
                    return _inventorySetItem.Id;
                }
            }
            public int Id
            {
                get
                {
                    return WorkOrderItem.Id;
                }
            }
            public string Name
            {
                get
                {
                    return _inventorySetItem.Name;
                }
            }
            public System.Drawing.Image Image
            {
                get {
                    return _itemImage ??
                           (_itemImage = System.Drawing.Image.FromStream(_inventorySetItem.Image.AsStream()));
                }
            }
            public string Comments
            {
                get
                {
                    return _inventorySetItem.Comments;
                }
            }
            public string Description
            {
                get
                {
                    return _inventorySetItem.Description;
                }
            }
            public uint Quantity
            {
                get
                {
                    return _inventorySetItem.Quantity;
                }
            }

            public uint RequestedQuantity { get; set; }

            internal WorkOrderItem WorkOrderItem
            {
                get { return _workOrderItem; }
            }
        }

        public InventoryWorkOrderDialog()
        {
            WorkOrderItemRecords = new List<WorkOrderItemRecord>();
            InitializeComponent();
        	layoutControlItem1.Text = Strings.RoomLableText;
        	layoutControlItem2.Text = Strings.SetNameLableText;
			layoutControlItem4.Text = Strings.DeliveryTypeLableText;
			layoutControlItem1.CustomizationFormText = Strings.RoomLableText;
			layoutControlItem2.CustomizationFormText = Strings.SetNameLableText;
			layoutControlItem4.CustomizationFormText = Strings.DeliveryTypeLableText;
			roomSetComboBox.Properties.NullText = Strings.SelectOneText;
        	itemNameColumn.Caption = Strings.NameColumnText;
			serialNumberColumn.Caption = Strings.SerialColumnText;
			itemImageColumn.Caption = Strings.ImageColumnText;
			commentsColumn.Caption = Strings.CommentsColumnText;
			descriptionColumn.Caption = Strings.DescriptionColumnText;
			itemQuantityColumn.Caption = Strings.QuantityInHandColumnText;
			requestedQuantityColumn.Caption = Strings.RequestedQuantityColumnText;

        	Text = Strings.WorkOrderLableText;
			ApplyEnabled = false;
			ApplyVisible = false;
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
         Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        public WorkOrder WorkOrder { get; set; }

        private List<WorkOrderItemRecord> WorkOrderItemRecords { get; set; }

        private void AudioVisualWorkOrderDialog_Load(object sender, EventArgs e)
        {
            var cursor = Cursor;
            Cursor = Cursors.WaitCursor;
            try
            {
                Text = string.Format("{0} - {1}", Text, WorkOrder.Name);
                deliveryTypeComboBox.Properties.Items.AddRange(
                    new EnumListSource {EnumType = typeof (WorkOrderDeliveryType)}.GetList());
                deliveryTypeComboBox.EditValue = WorkOrder.DeliveryType;
                roomNameLabel.Text = MyVrmAddin.Instance.GetRoomFromCache(WorkOrder.RoomId).Name;
                var roomSets =MyVrmService.Service.GetRoomSets(WorkOrder.RoomId, RoomSetType.Inventory);
                roomSetComboBox.Properties.Items.BeginUpdate();
                try
                {
                    foreach (var roomSet in roomSets)
                    {
                        roomSetComboBox.Properties.Items.Add(roomSet);
                    }
                    var workOrderRoomSet = roomSets.FirstOrDefault(s => s.Id == WorkOrder.SetId);
                    if (workOrderRoomSet != null)
                        roomSetComboBox.SelectedItem = workOrderRoomSet;
                    else
                    {
                        if (roomSets.Count == 1)
                        {
                            roomSetComboBox.SelectedIndex = 0;
                            roomSetComboBox.Refresh();
                        }
                    }
                }
                finally
                {
                    roomSetComboBox.Properties.Items.EndUpdate();
                }
            }
            finally
            {
                Cursor = cursor;
            }
        }

        private void roomSetComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var cursor = Cursor;
            Cursor = Cursors.WaitCursor;
            try
            {
                var roomSet = roomSetComboBox.SelectedItem as RoomSet;
                if (roomSet != null)
                {
                    var inventorySet = MyVrmService.Service.GetInventoryDetails(roomSet.Id, RoomSetType.Inventory);
                    var query = from inventoryItem in inventorySet.Items
                                join workOrderItem in WorkOrder.Items on inventoryItem.Id equals workOrderItem.Id into
                                    woItems
                                from woItem in woItems.DefaultIfEmpty(new WorkOrderItem {Id = inventoryItem.Id,Price=inventoryItem.Price,DeliveryCost=inventoryItem.DeliveryCost,DeliveryType=inventoryItem.DeliveryType,ServiceCharge=inventoryItem.ServiceCharge,InventoryCharges=inventoryItem.InventoryCharges})
                                select
                                    new WorkOrderItemRecord(woItem, inventoryItem);

                    WorkOrderItemRecords = query.ToList();
                    workOrderItemsList.DataSource = WorkOrderItemRecords;

                    GetInventoryWOAdminResponse admin = MyVrmService.Service.GetInventoryWoAdminDetails(roomSet.Id, RoomSetType.Inventory); //ZD 103364 
                    InventoryWOAdmin = admin.ID; //ZD 103364 
                   
                }
            }
            finally
            {
                Cursor = cursor;
            }
        }

        private void quantityRepositoryItemSpinEdit_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            //var newValue = Convert.ToInt32(e.NewValue);
			int newValue = 0;
			Int32.TryParse(e.NewValue.ToString(), out newValue);
        	int iQuantityColumn = 0;
			Int32.TryParse(workOrderItemsList.FocusedNode[itemQuantityColumn].ToString(), out iQuantityColumn);
			if (newValue < 0 || newValue > iQuantityColumn/*Convert.ToInt32(workOrderItemsList.FocusedNode[itemQuantityColumn])*/)
            {
                e.Cancel = true;
            }
        }

        private void AudioVisualWorkOrderDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult != DialogResult.OK) return;
            WorkOrderItemRecords.ForEach(rec => rec.WorkOrderItem.Quantity = rec.RequestedQuantity);
            
            
            var workOrderItems = from itemRecord in WorkOrderItemRecords
                                 where itemRecord.RequestedQuantity > 0
                                 select itemRecord.WorkOrderItem;
            if (roomSetComboBox.EditValue == null)
            {
                ShowMessage(Strings.SelectAudioVideoInventorySet);
                e.Cancel = true;
                return;
            }
            if (deliveryTypeComboBox.EditValue == null)
            {
                ShowMessage(Strings.SelectAudioVideoWorkOrderDeliveryType);
                e.Cancel = true;
                return;
            }
            if (workOrderItems.Count() == 0)
            {
                ShowMessage(Strings.SpecifyWorkOrderItemRequestQuantity);
                e.Cancel = true;
                return;
            }
            WorkOrder.SetId = ((RoomSet) roomSetComboBox.SelectedItem).Id;
            WorkOrder.DeliveryType = (WorkOrderDeliveryType)deliveryTypeComboBox.EditValue;
            decimal totalCost = 0;

            foreach (WorkOrderItem item in workOrderItems)
            {
                if (item.InventoryCharges != null)
                {
                    var q = from m in item.InventoryCharges where m.DeliveryType == (int)deliveryTypeComboBox.EditValue select m;
                    List<InventoryCharges> invCharges = q.ToList();
                    if (invCharges.Count > 0)
                    {
                        InventoryCharges invCharge = invCharges[0];
                        if (invCharge != null)
                        {
                            item.DeliveryCost = invCharge.DeliveryCost;
                            item.ServiceCharge = invCharge.ServiceCharge;
                            item.DeliveryType = (int)deliveryTypeComboBox.EditValue;
                        }
                    }
                }
                //ZD 103342 start
                else
                {
                    WorkOrderItemRecords.ForEach(rec => rec.WorkOrderItem.InventoryCharges = rec.InventoryCharges);
                    var q = from m in item.InventoryCharges where m.DeliveryType == (int)deliveryTypeComboBox.EditValue select m;
                    List<InventoryCharges> invCharges = q.ToList();
                    if (invCharges.Count > 0)
                    {
                        InventoryCharges invCharge = invCharges[0];
                        if (invCharge != null)
                        {
                            item.DeliveryCost = invCharge.DeliveryCost;
                            item.ServiceCharge = invCharge.ServiceCharge;
                            item.DeliveryType = (int)deliveryTypeComboBox.EditValue;
                        }
                    }
                }
                totalCost += item.Quantity * item.Price;
                totalCost += item.Quantity * item.ServiceCharge;
                totalCost += item.Quantity * item.DeliveryCost;
                
            }
            
            WorkOrder.Items.Clear();
            
            WorkOrder.Items.AddRange(workOrderItems);
            WorkOrder.TotalCost = totalCost;
            WorkOrder.WOAdministratorId = InventoryWOAdmin; //ZD 103364

        }
    }
}
