﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.Outlook.WinForms.ManageRooms
{
	partial class CreateEditRoomDialog
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreateEditRoomDialog));
			DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
			DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
			this.infoLable = new DevExpress.XtraLayout.LayoutControl();
			this.clearImageBn = new DevExpress.XtraEditors.SimpleButton();
			this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
			this.meetingCapacityCombo = new DevExpress.XtraEditors.ComboBoxEdit();
			this.addressMemoEdit = new DevExpress.XtraEditors.MemoEdit();
			this.roomLayoutComboBox = new DevExpress.XtraEditors.ImageComboBoxEdit();
			this.roomLayoutList = new System.Windows.Forms.ImageList(this.components);
			this.citySuburb = new DevExpress.XtraEditors.TextEdit();
			this.imagePictureEdit = new DevExpress.XtraEditors.PictureEdit();
			this.imageButton = new DevExpress.XtraEditors.SimpleButton();
			this.roomImageFilePath = new DevExpress.XtraEditors.TextEdit();
			this.ipSpeedCombo = new DevExpress.XtraEditors.ComboBoxEdit();
			this.vctpLocationName = new DevExpress.XtraEditors.TextEdit();
			this.isdnSpeedCombo = new DevExpress.XtraEditors.ComboBoxEdit();
			this.ipNumber = new DevExpress.XtraEditors.TextEdit();
			this.latitudeVal = new DevExpress.XtraEditors.TextEdit();
			this.longitudeVal = new DevExpress.XtraEditors.TextEdit();
			this.isdnNumber = new DevExpress.XtraEditors.TextEdit();
			this.technicalContactMail = new DevExpress.XtraEditors.TextEdit();
			this.technicalContactPhone = new DevExpress.XtraEditors.TextEdit();
			this.technicalContactName = new DevExpress.XtraEditors.TextEdit();
			this.vctpCapacityCombo = new DevExpress.XtraEditors.ComboBoxEdit();
			this.equipCheckedListBox = new DevExpress.XtraEditors.CheckedListBoxControl();
			this.caiteringCheckedListBox = new DevExpress.XtraEditors.CheckedListBoxControl();
			this.memoEdit2 = new DevExpress.XtraEditors.MemoEdit();
			this.memoEdit3 = new DevExpress.XtraEditors.MemoEdit();
			this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
			this.managerMail = new DevExpress.XtraEditors.TextEdit();
			this.managerPhone = new DevExpress.XtraEditors.TextEdit();
			this.managerName = new DevExpress.XtraEditors.TextEdit();
			this.siteCoordinatorMail = new DevExpress.XtraEditors.TextEdit();
			this.siteCoordinatorPhone = new DevExpress.XtraEditors.TextEdit();
			this.siteCoordinatorName = new DevExpress.XtraEditors.TextEdit();
			this.meetingOnlyLocationName = new DevExpress.XtraEditors.TextEdit();
			this.stateCombo = new DevExpress.XtraEditors.ComboBoxEdit();
			this.countryCombo = new DevExpress.XtraEditors.ComboBoxEdit();
			this.vctpLocationInfoIcon = new DevExpress.XtraEditors.LabelControl();
			this.freeRoomsLable = new DevExpress.XtraEditors.LabelControl();
			this.roomTypeCombo = new DevExpress.XtraEditors.ComboBoxEdit();
			this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
			this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
			this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
			this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.roomTabbedGroup = new DevExpress.XtraLayout.TabbedControlGroup();
			this.vctpGroup = new DevExpress.XtraLayout.LayoutControlGroup();
			this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.roomImageGroup = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
			this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
			this.clearImageControl = new DevExpress.XtraLayout.LayoutControlItem();
			this.geoCodeGroup = new DevExpress.XtraLayout.LayoutControlGroup();
			this.longitudeControl = new DevExpress.XtraLayout.LayoutControlItem();
			this.latitudeControl = new DevExpress.XtraLayout.LayoutControlItem();
			this.optionslGroup = new DevExpress.XtraLayout.LayoutControlGroup();
			this.vctpCapacityComboControl = new DevExpress.XtraLayout.LayoutControlItem();
			this.vctEquipMemo = new DevExpress.XtraLayout.LayoutControlItem();
			this.isdnNumberControl = new DevExpress.XtraLayout.LayoutControlItem();
			this.isdnSpeedControl = new DevExpress.XtraLayout.LayoutControlItem();
			this.ipNumberControl = new DevExpress.XtraLayout.LayoutControlItem();
			this.ipSpeedControl = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
			this.contactGroup = new DevExpress.XtraLayout.LayoutControlGroup();
			this.technicalContactNameControl = new DevExpress.XtraLayout.LayoutControlItem();
			this.technicalContactPhoneControl = new DevExpress.XtraLayout.LayoutControlItem();
			this.technicalContactEmailControl = new DevExpress.XtraLayout.LayoutControlItem();
			this.meetingGroup = new DevExpress.XtraLayout.LayoutControlGroup();
			this.locationGroup = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
			this.addressLayoutControl = new DevExpress.XtraLayout.LayoutControlItem();
			this.capacityMeetingRoomControlItem = new DevExpress.XtraLayout.LayoutControlItem();
			this.roomLayout = new DevExpress.XtraLayout.LayoutControlItem();
			this.cateringGroup = new DevExpress.XtraLayout.LayoutControlGroup();
			this.cateringCheckList = new DevExpress.XtraLayout.LayoutControlItem();
			this.managerGroup = new DevExpress.XtraLayout.LayoutControlGroup();
			this.managerNameControl = new DevExpress.XtraLayout.LayoutControlItem();
			this.managerPhoneControl = new DevExpress.XtraLayout.LayoutControlItem();
			this.managerEmailControl = new DevExpress.XtraLayout.LayoutControlItem();
			this.coordinatorGroup = new DevExpress.XtraLayout.LayoutControlGroup();
			this.coordinatorEmailControl = new DevExpress.XtraLayout.LayoutControlItem();
			this.coordinatorPhoneControl = new DevExpress.XtraLayout.LayoutControlItem();
			this.coordinatorNameControl = new DevExpress.XtraLayout.LayoutControlItem();
			this.descrGroup = new DevExpress.XtraLayout.LayoutControlGroup();
			this.descrMemo = new DevExpress.XtraLayout.LayoutControlItem();
			this.equipGroup = new DevExpress.XtraLayout.LayoutControlGroup();
			this.equipCheckList = new DevExpress.XtraLayout.LayoutControlItem();
			this.notesGroup = new DevExpress.XtraLayout.LayoutControlGroup();
			this.notesMemo = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
			this.speedLabelLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
			this.speedLabelLayout = new DevExpress.XtraLayout.LayoutControlItem();
			((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).BeginInit();
			this.ContentPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.infoLable)).BeginInit();
			this.infoLable.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.meetingCapacityCombo.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.addressMemoEdit.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.roomLayoutComboBox.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.citySuburb.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imagePictureEdit.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.roomImageFilePath.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ipSpeedCombo.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vctpLocationName.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.isdnSpeedCombo.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ipNumber.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.latitudeVal.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.longitudeVal.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.isdnNumber.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.technicalContactMail.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.technicalContactPhone.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.technicalContactName.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vctpCapacityCombo.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.equipCheckedListBox)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.caiteringCheckedListBox)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.memoEdit2.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.memoEdit3.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.managerMail.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.managerPhone.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.managerName.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.siteCoordinatorMail.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.siteCoordinatorPhone.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.siteCoordinatorName.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.meetingOnlyLocationName.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.stateCombo.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.countryCombo.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.roomTypeCombo.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.roomTabbedGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vctpGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.roomImageGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.clearImageControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.geoCodeGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.longitudeControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.latitudeControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.optionslGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vctpCapacityComboControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vctEquipMemo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.isdnNumberControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.isdnSpeedControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ipNumberControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ipSpeedControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.contactGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.technicalContactNameControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.technicalContactPhoneControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.technicalContactEmailControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.meetingGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.locationGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.addressLayoutControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.capacityMeetingRoomControlItem)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.roomLayout)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cateringGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cateringCheckList)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.managerGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.managerNameControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.managerPhoneControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.managerEmailControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.coordinatorGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.coordinatorEmailControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.coordinatorPhoneControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.coordinatorNameControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.descrGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.descrMemo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.equipGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.equipCheckList)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.notesGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.notesMemo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.speedLabelLayoutControlItem)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.speedLabelLayout)).BeginInit();
			this.SuspendLayout();
			// 
			// ContentPanel
			// 
			this.ContentPanel.Controls.Add(this.infoLable);
			this.ContentPanel.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
			this.ContentPanel.Size = new System.Drawing.Size(739, 557);
			// 
			// infoLable
			// 
			this.infoLable.Controls.Add(this.clearImageBn);
			this.infoLable.Controls.Add(this.labelControl1);
			this.infoLable.Controls.Add(this.meetingCapacityCombo);
			this.infoLable.Controls.Add(this.addressMemoEdit);
			this.infoLable.Controls.Add(this.roomLayoutComboBox);
			this.infoLable.Controls.Add(this.citySuburb);
			this.infoLable.Controls.Add(this.imagePictureEdit);
			this.infoLable.Controls.Add(this.imageButton);
			this.infoLable.Controls.Add(this.roomImageFilePath);
			this.infoLable.Controls.Add(this.ipSpeedCombo);
			this.infoLable.Controls.Add(this.vctpLocationName);
			this.infoLable.Controls.Add(this.isdnSpeedCombo);
			this.infoLable.Controls.Add(this.ipNumber);
			this.infoLable.Controls.Add(this.latitudeVal);
			this.infoLable.Controls.Add(this.longitudeVal);
			this.infoLable.Controls.Add(this.isdnNumber);
			this.infoLable.Controls.Add(this.technicalContactMail);
			this.infoLable.Controls.Add(this.technicalContactPhone);
			this.infoLable.Controls.Add(this.technicalContactName);
			this.infoLable.Controls.Add(this.vctpCapacityCombo);
			this.infoLable.Controls.Add(this.equipCheckedListBox);
			this.infoLable.Controls.Add(this.caiteringCheckedListBox);
			this.infoLable.Controls.Add(this.memoEdit2);
			this.infoLable.Controls.Add(this.memoEdit3);
			this.infoLable.Controls.Add(this.memoEdit1);
			this.infoLable.Controls.Add(this.managerMail);
			this.infoLable.Controls.Add(this.managerPhone);
			this.infoLable.Controls.Add(this.managerName);
			this.infoLable.Controls.Add(this.siteCoordinatorMail);
			this.infoLable.Controls.Add(this.siteCoordinatorPhone);
			this.infoLable.Controls.Add(this.siteCoordinatorName);
			this.infoLable.Controls.Add(this.meetingOnlyLocationName);
			this.infoLable.Controls.Add(this.stateCombo);
			this.infoLable.Controls.Add(this.countryCombo);
			this.infoLable.Controls.Add(this.vctpLocationInfoIcon);
			this.infoLable.Controls.Add(this.freeRoomsLable);
			this.infoLable.Controls.Add(this.roomTypeCombo);
			this.infoLable.Controls.Add(this.labelControl2);
			this.infoLable.Dock = System.Windows.Forms.DockStyle.Fill;
			this.infoLable.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6});
			this.infoLable.Location = new System.Drawing.Point(0, 0);
			this.infoLable.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.infoLable.Name = "infoLable";
			this.infoLable.Root = this.layoutControlGroup1;
			this.infoLable.Size = new System.Drawing.Size(739, 557);
			this.infoLable.TabIndex = 0;
			this.infoLable.Text = "layoutControl1";
			// 
			// clearImageBn
			// 
			this.clearImageBn.Location = new System.Drawing.Point(610, 105);
			this.clearImageBn.Name = "clearImageBn";
			this.clearImageBn.Size = new System.Drawing.Size(112, 23);
			this.clearImageBn.StyleController = this.infoLable;
			this.clearImageBn.TabIndex = 44;
			this.clearImageBn.Text = "Clear Image";
			this.clearImageBn.Click += new System.EventHandler(this.clearImageBn_Click);
			// 
			// labelControl1
			// 
			this.labelControl1.Location = new System.Drawing.Point(464, 94);
			this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.labelControl1.Name = "labelControl1";
			this.labelControl1.Size = new System.Drawing.Size(75, 16);
			this.labelControl1.StyleController = this.infoLable;
			this.labelControl1.TabIndex = 9;
			this.labelControl1.Text = "labelControl1";
			// 
			// meetingCapacityCombo
			// 
			this.meetingCapacityCombo.Location = new System.Drawing.Point(115, 282);
			this.meetingCapacityCombo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.meetingCapacityCombo.Name = "meetingCapacityCombo";
			this.meetingCapacityCombo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.meetingCapacityCombo.Properties.NullText = "[please select]";
			this.meetingCapacityCombo.Properties.NullValuePromptShowForEmptyValue = true;
			this.meetingCapacityCombo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
			this.meetingCapacityCombo.Size = new System.Drawing.Size(242, 22);
			this.meetingCapacityCombo.StyleController = this.infoLable;
			this.meetingCapacityCombo.TabIndex = 39;
			// 
			// addressMemoEdit
			// 
			this.addressMemoEdit.Location = new System.Drawing.Point(115, 226);
			this.addressMemoEdit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.addressMemoEdit.Name = "addressMemoEdit";
			this.addressMemoEdit.Size = new System.Drawing.Size(242, 22);
			this.addressMemoEdit.StyleController = this.infoLable;
			this.addressMemoEdit.TabIndex = 38;
			// 
			// roomLayoutComboBox
			// 
			this.roomLayoutComboBox.EditValue = "Boardroom";
			this.roomLayoutComboBox.Location = new System.Drawing.Point(115, 252);
			this.roomLayoutComboBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.roomLayoutComboBox.Name = "roomLayoutComboBox";
			this.roomLayoutComboBox.Properties.Appearance.Options.UseImage = true;
			this.roomLayoutComboBox.Properties.Appearance.Options.UseTextOptions = true;
			this.roomLayoutComboBox.Properties.AppearanceDropDown.Options.UseImage = true;
			this.roomLayoutComboBox.Properties.AppearanceDropDown.Options.UseTextOptions = true;
			serializableAppearanceObject1.Options.UseImage = true;
			this.roomLayoutComboBox.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleLeft, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
			this.roomLayoutComboBox.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Boardroom", "Boardroom", 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cabaret", "Cabaret", 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Classroom", "Classroom", 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Theatre", "Theatre", 3),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("U-Shaped", "UShaped", 4)});
			this.roomLayoutComboBox.Properties.LargeImages = this.roomLayoutList;
			this.roomLayoutComboBox.Properties.SmallImages = this.roomLayoutList;
			this.roomLayoutComboBox.Size = new System.Drawing.Size(242, 22);
			this.roomLayoutComboBox.StyleController = this.infoLable;
			this.roomLayoutComboBox.TabIndex = 41;
			// 
			// roomLayoutList
			// 
			this.roomLayoutList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("roomLayoutList.ImageStream")));
			this.roomLayoutList.TransparentColor = System.Drawing.Color.Transparent;
			this.roomLayoutList.Images.SetKeyName(0, "Boardroom2.PNG");
			this.roomLayoutList.Images.SetKeyName(1, "Cabaret2.PNG");
			this.roomLayoutList.Images.SetKeyName(2, "Classroom2.PNG");
			this.roomLayoutList.Images.SetKeyName(3, "Theatre3.PNG");
			this.roomLayoutList.Images.SetKeyName(4, "UShaped2.PNG");
			// 
			// citySuburb
			// 
			this.citySuburb.Location = new System.Drawing.Point(115, 196);
			this.citySuburb.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.citySuburb.Name = "citySuburb";
			this.citySuburb.Size = new System.Drawing.Size(242, 22);
			this.citySuburb.StyleController = this.infoLable;
			this.citySuburb.TabIndex = 37;
			// 
			// imagePictureEdit
			// 
			this.imagePictureEdit.Location = new System.Drawing.Point(430, 105);
			this.imagePictureEdit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.imagePictureEdit.Name = "imagePictureEdit";
			this.imagePictureEdit.Properties.Appearance.Options.UseImage = true;
			this.imagePictureEdit.Properties.AppearanceFocused.Options.UseImage = true;
			this.imagePictureEdit.Properties.InitialImage = null;
			this.imagePictureEdit.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
			this.imagePictureEdit.Size = new System.Drawing.Size(176, 181);
			this.imagePictureEdit.StyleController = this.infoLable;
			this.imagePictureEdit.TabIndex = 35;
			// 
			// imageButton
			// 
			this.imageButton.Location = new System.Drawing.Point(610, 290);
			this.imageButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.imageButton.Name = "imageButton";
			this.imageButton.Size = new System.Drawing.Size(112, 23);
			this.imageButton.StyleController = this.infoLable;
			this.imageButton.TabIndex = 34;
			this.imageButton.Text = "Load Image";
			this.imageButton.Click += new System.EventHandler(this.imageButton_Click);
			// 
			// roomImageFilePath
			// 
			this.roomImageFilePath.Enabled = false;
			this.roomImageFilePath.Location = new System.Drawing.Point(432, 290);
			this.roomImageFilePath.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.roomImageFilePath.Name = "roomImageFilePath";
			this.roomImageFilePath.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.roomImageFilePath.Properties.ReadOnly = true;
			this.roomImageFilePath.Size = new System.Drawing.Size(174, 22);
			this.roomImageFilePath.StyleController = this.infoLable;
			this.roomImageFilePath.TabIndex = 33;
			// 
			// ipSpeedCombo
			// 
			this.ipSpeedCombo.Location = new System.Drawing.Point(323, 257);
			this.ipSpeedCombo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.ipSpeedCombo.Name = "ipSpeedCombo";
			this.ipSpeedCombo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.ipSpeedCombo.Properties.Items.AddRange(new object[] {
            "384",
            "512",
            "768",
            "1024",
            "2048",
            "3072",
            "4096"});
			this.ipSpeedCombo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
			this.ipSpeedCombo.Size = new System.Drawing.Size(85, 22);
			this.ipSpeedCombo.StyleController = this.infoLable;
			this.ipSpeedCombo.TabIndex = 30;
			// 
			// vctpLocationName
			// 
			this.vctpLocationName.Location = new System.Drawing.Point(117, 107);
			this.vctpLocationName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.vctpLocationName.Name = "vctpLocationName";
			this.vctpLocationName.Size = new System.Drawing.Size(213, 22);
			this.vctpLocationName.StyleController = this.infoLable;
			this.vctpLocationName.TabIndex = 42;
			// 
			// isdnSpeedCombo
			// 
			this.isdnSpeedCombo.Location = new System.Drawing.Point(323, 227);
			this.isdnSpeedCombo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.isdnSpeedCombo.Name = "isdnSpeedCombo";
			this.isdnSpeedCombo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.isdnSpeedCombo.Properties.Items.AddRange(new object[] {
            "128",
            "256",
            "384",
            "512"});
			this.isdnSpeedCombo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
			this.isdnSpeedCombo.Size = new System.Drawing.Size(85, 22);
			this.isdnSpeedCombo.StyleController = this.infoLable;
			this.isdnSpeedCombo.TabIndex = 29;
			// 
			// ipNumber
			// 
			this.ipNumber.Location = new System.Drawing.Point(115, 257);
			this.ipNumber.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.ipNumber.Name = "ipNumber";
			this.ipNumber.Size = new System.Drawing.Size(100, 22);
			this.ipNumber.StyleController = this.infoLable;
			this.ipNumber.TabIndex = 28;
			// 
			// latitudeVal
			// 
			this.latitudeVal.Location = new System.Drawing.Point(528, 376);
			this.latitudeVal.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.latitudeVal.Name = "latitudeVal";
			this.latitudeVal.Size = new System.Drawing.Size(194, 22);
			this.latitudeVal.StyleController = this.infoLable;
			this.latitudeVal.TabIndex = 32;
			// 
			// longitudeVal
			// 
			this.longitudeVal.EditValue = "";
			this.longitudeVal.Location = new System.Drawing.Point(528, 345);
			this.longitudeVal.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.longitudeVal.Name = "longitudeVal";
			this.longitudeVal.Size = new System.Drawing.Size(194, 22);
			this.longitudeVal.StyleController = this.infoLable;
			this.longitudeVal.TabIndex = 31;
			this.longitudeVal.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.longitudeGrad_EditValueChanging);
			// 
			// isdnNumber
			// 
			this.isdnNumber.Location = new System.Drawing.Point(115, 227);
			this.isdnNumber.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.isdnNumber.Name = "isdnNumber";
			this.isdnNumber.Size = new System.Drawing.Size(100, 22);
			this.isdnNumber.StyleController = this.infoLable;
			this.isdnNumber.TabIndex = 27;
			// 
			// technicalContactMail
			// 
			this.technicalContactMail.Location = new System.Drawing.Point(115, 376);
			this.technicalContactMail.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.technicalContactMail.Name = "technicalContactMail";
			this.technicalContactMail.Size = new System.Drawing.Size(293, 22);
			this.technicalContactMail.StyleController = this.infoLable;
			this.technicalContactMail.TabIndex = 25;
			// 
			// technicalContactPhone
			// 
			this.technicalContactPhone.Location = new System.Drawing.Point(115, 346);
			this.technicalContactPhone.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.technicalContactPhone.Name = "technicalContactPhone";
			this.technicalContactPhone.Size = new System.Drawing.Size(293, 22);
			this.technicalContactPhone.StyleController = this.infoLable;
			this.technicalContactPhone.TabIndex = 24;
			this.technicalContactPhone.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.CheckPhoneInput_EditValueChanging);
			// 
			// technicalContactName
			// 
			this.technicalContactName.Location = new System.Drawing.Point(115, 315);
			this.technicalContactName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.technicalContactName.Name = "technicalContactName";
			this.technicalContactName.Size = new System.Drawing.Size(293, 22);
			this.technicalContactName.StyleController = this.infoLable;
			this.technicalContactName.TabIndex = 23;
			this.technicalContactName.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.CheckNameInput_EditValueChanging);
			// 
			// vctpCapacityCombo
			// 
			this.vctpCapacityCombo.Location = new System.Drawing.Point(117, 137);
			this.vctpCapacityCombo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.vctpCapacityCombo.Name = "vctpCapacityCombo";
			this.vctpCapacityCombo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.vctpCapacityCombo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.vctpCapacityCombo.Properties.NullText = "[please select]";
			this.vctpCapacityCombo.Properties.NullValuePromptShowForEmptyValue = true;
			this.vctpCapacityCombo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
			this.vctpCapacityCombo.Size = new System.Drawing.Size(291, 22);
			this.vctpCapacityCombo.StyleController = this.infoLable;
			this.vctpCapacityCombo.TabIndex = 21;
			// 
			// equipCheckedListBox
			// 
			this.equipCheckedListBox.CheckOnClick = true;
			this.equipCheckedListBox.Location = new System.Drawing.Point(379, 236);
			this.equipCheckedListBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.equipCheckedListBox.Name = "equipCheckedListBox";
			this.equipCheckedListBox.Size = new System.Drawing.Size(343, 118);
			this.equipCheckedListBox.StyleController = this.infoLable;
			this.equipCheckedListBox.TabIndex = 20;
			// 
			// caiteringCheckedListBox
			// 
			this.caiteringCheckedListBox.CheckOnClick = true;
			this.caiteringCheckedListBox.Location = new System.Drawing.Point(379, 105);
			this.caiteringCheckedListBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.caiteringCheckedListBox.Name = "caiteringCheckedListBox";
			this.caiteringCheckedListBox.Size = new System.Drawing.Size(343, 99);
			this.caiteringCheckedListBox.StyleController = this.infoLable;
			this.caiteringCheckedListBox.TabIndex = 19;
			// 
			// memoEdit2
			// 
			this.memoEdit2.Location = new System.Drawing.Point(379, 488);
			this.memoEdit2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.memoEdit2.Name = "memoEdit2";
			this.memoEdit2.Size = new System.Drawing.Size(343, 52);
			this.memoEdit2.StyleController = this.infoLable;
			this.memoEdit2.TabIndex = 18;
			// 
			// memoEdit3
			// 
			this.memoEdit3.Location = new System.Drawing.Point(19, 186);
			this.memoEdit3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.memoEdit3.Name = "memoEdit3";
			this.memoEdit3.Size = new System.Drawing.Size(389, 37);
			this.memoEdit3.StyleController = this.infoLable;
			this.memoEdit3.TabIndex = 22;
			// 
			// memoEdit1
			// 
			this.memoEdit1.Location = new System.Drawing.Point(379, 386);
			this.memoEdit1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.memoEdit1.Name = "memoEdit1";
			this.memoEdit1.Size = new System.Drawing.Size(343, 70);
			this.memoEdit1.StyleController = this.infoLable;
			this.memoEdit1.TabIndex = 17;
			// 
			// managerMail
			// 
			this.managerMail.Location = new System.Drawing.Point(115, 511);
			this.managerMail.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.managerMail.Name = "managerMail";
			this.managerMail.Size = new System.Drawing.Size(242, 22);
			this.managerMail.StyleController = this.infoLable;
			this.managerMail.TabIndex = 16;
			// 
			// managerPhone
			// 
			this.managerPhone.Location = new System.Drawing.Point(115, 479);
			this.managerPhone.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.managerPhone.Name = "managerPhone";
			this.managerPhone.Size = new System.Drawing.Size(242, 22);
			this.managerPhone.StyleController = this.infoLable;
			this.managerPhone.TabIndex = 15;
			this.managerPhone.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.CheckPhoneInput_EditValueChanging);
			// 
			// managerName
			// 
			this.managerName.Location = new System.Drawing.Point(115, 446);
			this.managerName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.managerName.Name = "managerName";
			this.managerName.Size = new System.Drawing.Size(242, 22);
			this.managerName.StyleController = this.infoLable;
			this.managerName.TabIndex = 14;
			this.managerName.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.CheckNameInput_EditValueChanging);
			// 
			// siteCoordinatorMail
			// 
			this.siteCoordinatorMail.Location = new System.Drawing.Point(115, 392);
			this.siteCoordinatorMail.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.siteCoordinatorMail.Name = "siteCoordinatorMail";
			this.siteCoordinatorMail.Size = new System.Drawing.Size(242, 22);
			this.siteCoordinatorMail.StyleController = this.infoLable;
			this.siteCoordinatorMail.TabIndex = 13;
			// 
			// siteCoordinatorPhone
			// 
			this.siteCoordinatorPhone.Location = new System.Drawing.Point(115, 366);
			this.siteCoordinatorPhone.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.siteCoordinatorPhone.Name = "siteCoordinatorPhone";
			this.siteCoordinatorPhone.Size = new System.Drawing.Size(242, 22);
			this.siteCoordinatorPhone.StyleController = this.infoLable;
			this.siteCoordinatorPhone.TabIndex = 12;
			this.siteCoordinatorPhone.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.CheckPhoneInput_EditValueChanging);
			// 
			// siteCoordinatorName
			// 
			this.siteCoordinatorName.Location = new System.Drawing.Point(115, 340);
			this.siteCoordinatorName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.siteCoordinatorName.Name = "siteCoordinatorName";
			this.siteCoordinatorName.Size = new System.Drawing.Size(242, 22);
			this.siteCoordinatorName.StyleController = this.infoLable;
			this.siteCoordinatorName.TabIndex = 11;
			this.siteCoordinatorName.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.CheckNameInput_EditValueChanging);
			// 
			// meetingOnlyLocationName
			// 
			this.meetingOnlyLocationName.Location = new System.Drawing.Point(115, 105);
			this.meetingOnlyLocationName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.meetingOnlyLocationName.Name = "meetingOnlyLocationName";
			this.meetingOnlyLocationName.Size = new System.Drawing.Size(146, 22);
			this.meetingOnlyLocationName.StyleController = this.infoLable;
			this.meetingOnlyLocationName.TabIndex = 8;
			// 
			// stateCombo
			// 
			this.stateCombo.Location = new System.Drawing.Point(115, 166);
			this.stateCombo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.stateCombo.Name = "stateCombo";
			this.stateCombo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.stateCombo.Properties.NullText = "[please select]";
			this.stateCombo.Properties.NullValuePromptShowForEmptyValue = true;
			this.stateCombo.Properties.Sorted = true;
			this.stateCombo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
			this.stateCombo.Size = new System.Drawing.Size(242, 22);
			this.stateCombo.StyleController = this.infoLable;
			this.stateCombo.TabIndex = 7;
			// 
			// countryCombo
			// 
			this.countryCombo.EditValue = "";
			this.countryCombo.Location = new System.Drawing.Point(115, 135);
			this.countryCombo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.countryCombo.Name = "countryCombo";
			this.countryCombo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.countryCombo.Properties.NullText = "[please select]";
			this.countryCombo.Properties.NullValuePromptShowForEmptyValue = true;
			this.countryCombo.Properties.Sorted = true;
			this.countryCombo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
			this.countryCombo.Size = new System.Drawing.Size(242, 22);
			this.countryCombo.StyleController = this.infoLable;
			this.countryCombo.TabIndex = 6;
			this.countryCombo.SelectedIndexChanged += new System.EventHandler(this.countryCombo_SelectedIndexChanged);
			// 
			// vctpLocationInfoIcon
			// 
			this.vctpLocationInfoIcon.Appearance.Image = global::MyVrm.Outlook.WinForms.Properties.Resources.information;
			this.vctpLocationInfoIcon.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.vctpLocationInfoIcon.Appearance.Options.UseImage = true;
			this.vctpLocationInfoIcon.Location = new System.Drawing.Point(334, 105);
			this.vctpLocationInfoIcon.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.vctpLocationInfoIcon.Name = "vctpLocationInfoIcon";
			this.vctpLocationInfoIcon.Size = new System.Drawing.Size(74, 26);
			this.vctpLocationInfoIcon.StyleController = this.infoLable;
			this.vctpLocationInfoIcon.TabIndex = 43;
			// 
			// freeRoomsLable
			// 
			this.freeRoomsLable.Appearance.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
			this.freeRoomsLable.Appearance.Options.UseFont = true;
			this.freeRoomsLable.Appearance.Options.UseTextOptions = true;
			this.freeRoomsLable.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.freeRoomsLable.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
			this.freeRoomsLable.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
			this.freeRoomsLable.Location = new System.Drawing.Point(514, 2);
			this.freeRoomsLable.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.freeRoomsLable.Name = "freeRoomsLable";
			this.freeRoomsLable.Size = new System.Drawing.Size(223, 26);
			this.freeRoomsLable.StyleController = this.infoLable;
			this.freeRoomsLable.TabIndex = 5;
			this.freeRoomsLable.Text = "4 Free Rooms Left";
			// 
			// roomTypeCombo
			// 
			this.roomTypeCombo.Location = new System.Drawing.Point(98, 2);
			this.roomTypeCombo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.roomTypeCombo.Name = "roomTypeCombo";
			this.roomTypeCombo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.roomTypeCombo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.roomTypeCombo.Properties.Items.AddRange(new object[] {
            "Video Conference or Telepresence",
            "Meeting Only"});
			this.roomTypeCombo.Properties.NullText = "<Select a room type>";
			this.roomTypeCombo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
			this.roomTypeCombo.Size = new System.Drawing.Size(292, 22);
			this.roomTypeCombo.StyleController = this.infoLable;
			this.roomTypeCombo.TabIndex = 4;
			this.roomTypeCombo.SelectedIndexChanged += new System.EventHandler(this.roomTypeCombo_SelectedIndexChanged);
			// 
			// labelControl2
			// 
			this.labelControl2.Appearance.Image = global::MyVrm.Outlook.WinForms.Properties.Resources.information;
			this.labelControl2.Appearance.Options.UseImage = true;
			this.labelControl2.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
			this.labelControl2.Location = new System.Drawing.Point(265, 105);
			this.labelControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.labelControl2.Name = "labelControl2";
			this.labelControl2.Size = new System.Drawing.Size(92, 26);
			this.labelControl2.StyleController = this.infoLable;
			toolTipItem3.Text = "Location name is the way you want to identify the room apart from city/suburb, st" +
				"ate/region or country. Eg \"DALLAS, Happy Street\", \"DALLAS, Level 5\".";
			superToolTip3.Items.Add(toolTipItem3);
			this.labelControl2.SuperTip = superToolTip3;
			this.labelControl2.TabIndex = 10;
			this.labelControl2.Click += new System.EventHandler(this.labelControl2_Click);
			// 
			// layoutControlItem6
			// 
			this.layoutControlItem6.Control = this.labelControl1;
			this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
			this.layoutControlItem6.Location = new System.Drawing.Point(462, 91);
			this.layoutControlItem6.MinSize = new System.Drawing.Size(79, 20);
			this.layoutControlItem6.Name = "layoutControlItem6";
			this.layoutControlItem6.Size = new System.Drawing.Size(79, 28);
			this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem6.Text = "layoutControlItem6";
			this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem6.TextToControlDistance = 0;
			this.layoutControlItem6.TextVisible = false;
			// 
			// layoutControlGroup1
			// 
			this.layoutControlGroup1.ContentImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
			this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
			this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
			this.layoutControlGroup1.GroupBordersVisible = false;
			this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.emptySpaceItem1,
            this.layoutControlItem2,
            this.emptySpaceItem4,
            this.roomTabbedGroup});
			this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup1.Name = "Root";
			this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup1.Size = new System.Drawing.Size(739, 557);
			this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup1.Text = "Root";
			this.layoutControlGroup1.TextVisible = false;
			// 
			// layoutControlItem1
			// 
			this.layoutControlItem1.Control = this.roomTypeCombo;
			this.layoutControlItem1.CustomizationFormText = "roomTypeCombo";
			this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem1.MinSize = new System.Drawing.Size(128, 26);
			this.layoutControlItem1.Name = "layoutControlItem1";
			this.layoutControlItem1.Size = new System.Drawing.Size(392, 30);
			this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem1.Text = "Room Type:";
			this.layoutControlItem1.TextSize = new System.Drawing.Size(92, 16);
			// 
			// emptySpaceItem1
			// 
			this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
			this.emptySpaceItem1.Location = new System.Drawing.Point(392, 0);
			this.emptySpaceItem1.MinSize = new System.Drawing.Size(54, 24);
			this.emptySpaceItem1.Name = "emptySpaceItem1";
			this.emptySpaceItem1.Size = new System.Drawing.Size(120, 30);
			this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.emptySpaceItem1.Text = "emptySpaceItem1";
			this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
			// 
			// layoutControlItem2
			// 
			this.layoutControlItem2.Control = this.freeRoomsLable;
			this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
			this.layoutControlItem2.Location = new System.Drawing.Point(512, 0);
			this.layoutControlItem2.MinSize = new System.Drawing.Size(79, 20);
			this.layoutControlItem2.Name = "layoutControlItem2";
			this.layoutControlItem2.Size = new System.Drawing.Size(227, 30);
			this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem2.Text = "layoutControlItem2";
			this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem2.TextToControlDistance = 0;
			this.layoutControlItem2.TextVisible = false;
			// 
			// emptySpaceItem4
			// 
			this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
			this.emptySpaceItem4.Location = new System.Drawing.Point(0, 30);
			this.emptySpaceItem4.Name = "emptySpaceItem4";
			this.emptySpaceItem4.Size = new System.Drawing.Size(739, 11);
			this.emptySpaceItem4.Text = "emptySpaceItem4";
			this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
			// 
			// roomTabbedGroup
			// 
			this.roomTabbedGroup.CustomizationFormText = "tabbedControlGroup1";
			this.roomTabbedGroup.Location = new System.Drawing.Point(0, 41);
			this.roomTabbedGroup.Name = "meetingOnlyGroup";
			this.roomTabbedGroup.SelectedTabPage = this.vctpGroup;
			this.roomTabbedGroup.SelectedTabPageIndex = 1;
			this.roomTabbedGroup.Size = new System.Drawing.Size(739, 516);
			this.roomTabbedGroup.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.meetingGroup,
            this.vctpGroup});
			this.roomTabbedGroup.Text = "roomTabbedGroup";
			// 
			// vctpGroup
			// 
			this.vctpGroup.CustomizationFormText = "Video Conferencing && Telepresence";
			this.vctpGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem6,
            this.emptySpaceItem11,
            this.roomImageGroup,
            this.geoCodeGroup,
            this.optionslGroup,
            this.contactGroup});
			this.vctpGroup.Location = new System.Drawing.Point(0, 0);
			this.vctpGroup.Name = "vctpGroup";
			this.vctpGroup.Size = new System.Drawing.Size(715, 467);
			this.vctpGroup.Text = "Video Conferencing && Telepresence";
			// 
			// emptySpaceItem6
			// 
			this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
			this.emptySpaceItem6.Location = new System.Drawing.Point(401, 0);
			this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 10);
			this.emptySpaceItem6.Name = "emptySpaceItem6";
			this.emptySpaceItem6.Size = new System.Drawing.Size(12, 330);
			this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.emptySpaceItem6.Text = "emptySpaceItem6";
			this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
			// 
			// emptySpaceItem11
			// 
			this.emptySpaceItem11.CustomizationFormText = "emptySpaceItem11";
			this.emptySpaceItem11.Location = new System.Drawing.Point(0, 330);
			this.emptySpaceItem11.MinSize = new System.Drawing.Size(104, 24);
			this.emptySpaceItem11.Name = "emptySpaceItem11";
			this.emptySpaceItem11.Size = new System.Drawing.Size(715, 137);
			this.emptySpaceItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.emptySpaceItem11.Text = "emptySpaceItem11";
			this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
			// 
			// roomImageGroup
			// 
			this.roomImageGroup.CustomizationFormText = "Room Image";
			this.roomImageGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem8,
            this.emptySpaceItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.clearImageControl});
			this.roomImageGroup.Location = new System.Drawing.Point(413, 0);
			this.roomImageGroup.Name = "roomImageGroup";
			this.roomImageGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
			this.roomImageGroup.Size = new System.Drawing.Size(302, 240);
			this.roomImageGroup.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
			this.roomImageGroup.Text = "Room Image";
			// 
			// layoutControlItem8
			// 
			this.layoutControlItem8.Control = this.imagePictureEdit;
			this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
			this.layoutControlItem8.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem8.Name = "layoutControlItem8";
			this.layoutControlItem8.Size = new System.Drawing.Size(180, 185);
			this.layoutControlItem8.Text = "layoutControlItem8";
			this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem8.TextToControlDistance = 0;
			this.layoutControlItem8.TextVisible = false;
			// 
			// emptySpaceItem8
			// 
			this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
			this.emptySpaceItem8.Location = new System.Drawing.Point(180, 27);
			this.emptySpaceItem8.MinSize = new System.Drawing.Size(1, 1);
			this.emptySpaceItem8.Name = "emptySpaceItem8";
			this.emptySpaceItem8.Size = new System.Drawing.Size(116, 158);
			this.emptySpaceItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.emptySpaceItem8.Text = "emptySpaceItem8";
			this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
			// 
			// layoutControlItem9
			// 
			this.layoutControlItem9.Control = this.roomImageFilePath;
			this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
			this.layoutControlItem9.Location = new System.Drawing.Point(0, 185);
			this.layoutControlItem9.MinSize = new System.Drawing.Size(56, 26);
			this.layoutControlItem9.Name = "layoutControlItem9";
			this.layoutControlItem9.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 2, 2, 2);
			this.layoutControlItem9.Size = new System.Drawing.Size(180, 27);
			this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem9.Text = "layoutControlItem9";
			this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem9.TextToControlDistance = 0;
			this.layoutControlItem9.TextVisible = false;
			// 
			// layoutControlItem10
			// 
			this.layoutControlItem10.Control = this.imageButton;
			this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
			this.layoutControlItem10.Location = new System.Drawing.Point(180, 185);
			this.layoutControlItem10.MinSize = new System.Drawing.Size(10, 10);
			this.layoutControlItem10.Name = "layoutControlItem10";
			this.layoutControlItem10.Size = new System.Drawing.Size(116, 27);
			this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem10.Text = "layoutControlItem10";
			this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem10.TextToControlDistance = 0;
			this.layoutControlItem10.TextVisible = false;
			// 
			// clearImageControl
			// 
			this.clearImageControl.Control = this.clearImageBn;
			this.clearImageControl.CustomizationFormText = "clearImageControl";
			this.clearImageControl.Location = new System.Drawing.Point(180, 0);
			this.clearImageControl.MinSize = new System.Drawing.Size(95, 27);
			this.clearImageControl.Name = "clearImageControl";
			this.clearImageControl.Size = new System.Drawing.Size(116, 27);
			this.clearImageControl.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.clearImageControl.Text = "clearImageControl";
			this.clearImageControl.TextSize = new System.Drawing.Size(0, 0);
			this.clearImageControl.TextToControlDistance = 0;
			this.clearImageControl.TextVisible = false;
			// 
			// geoCodeGroup
			// 
			this.geoCodeGroup.CustomizationFormText = "Geographical Values";
			this.geoCodeGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.longitudeControl,
            this.latitudeControl});
			this.geoCodeGroup.Location = new System.Drawing.Point(413, 240);
			this.geoCodeGroup.Name = "geoCodeGroup";
			this.geoCodeGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
			this.geoCodeGroup.Size = new System.Drawing.Size(302, 90);
			this.geoCodeGroup.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
			this.geoCodeGroup.Text = "Geographical Values";
			// 
			// longitudeControl
			// 
			this.longitudeControl.Control = this.longitudeVal;
			this.longitudeControl.CustomizationFormText = "Longitude";
			this.longitudeControl.Location = new System.Drawing.Point(0, 0);
			this.longitudeControl.MinSize = new System.Drawing.Size(152, 26);
			this.longitudeControl.Name = "longitudeControl";
			this.longitudeControl.Size = new System.Drawing.Size(296, 31);
			this.longitudeControl.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.longitudeControl.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 0, 0, 0);
			this.longitudeControl.Text = "Longitude:";
			this.longitudeControl.TextSize = new System.Drawing.Size(92, 16);
			// 
			// latitudeControl
			// 
			this.latitudeControl.Control = this.latitudeVal;
			this.latitudeControl.CustomizationFormText = "Latitude";
			this.latitudeControl.Location = new System.Drawing.Point(0, 31);
			this.latitudeControl.MinSize = new System.Drawing.Size(166, 26);
			this.latitudeControl.Name = "latitudeControl";
			this.latitudeControl.Size = new System.Drawing.Size(296, 31);
			this.latitudeControl.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.latitudeControl.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 0, 0, 0);
			this.latitudeControl.Text = "Latitude:";
			this.latitudeControl.TextSize = new System.Drawing.Size(92, 16);
			// 
			// optionslGroup
			// 
			this.optionslGroup.CustomizationFormText = "Options";
			this.optionslGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.vctpCapacityComboControl,
            this.vctEquipMemo,
            this.isdnNumberControl,
            this.isdnSpeedControl,
            this.ipNumberControl,
            this.ipSpeedControl,
            this.layoutControlItem12,
            this.layoutControlItem14});
			this.optionslGroup.Location = new System.Drawing.Point(0, 0);
			this.optionslGroup.Name = "optionslGroup";
			this.optionslGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
			this.optionslGroup.Size = new System.Drawing.Size(401, 210);
			this.optionslGroup.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
			this.optionslGroup.Text = "Options";
			// 
			// vctpCapacityComboControl
			// 
			this.vctpCapacityComboControl.Control = this.vctpCapacityCombo;
			this.vctpCapacityComboControl.CustomizationFormText = "Capacity";
			this.vctpCapacityComboControl.Location = new System.Drawing.Point(0, 30);
			this.vctpCapacityComboControl.MinSize = new System.Drawing.Size(147, 26);
			this.vctpCapacityComboControl.Name = "vctpCapacityComboControl";
			this.vctpCapacityComboControl.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 2, 4, 4);
			this.vctpCapacityComboControl.Size = new System.Drawing.Size(395, 30);
			this.vctpCapacityComboControl.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.vctpCapacityComboControl.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 0, 0, 0);
			this.vctpCapacityComboControl.Text = "Capacity:";
			this.vctpCapacityComboControl.TextSize = new System.Drawing.Size(92, 16);
			// 
			// vctEquipMemo
			// 
			this.vctEquipMemo.Control = this.memoEdit3;
			this.vctEquipMemo.CustomizationFormText = "Equipment Type";
			this.vctEquipMemo.Location = new System.Drawing.Point(0, 60);
			this.vctEquipMemo.MinSize = new System.Drawing.Size(112, 42);
			this.vctEquipMemo.Name = "vctEquipMemo";
			this.vctEquipMemo.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 2, 4, 2);
			this.vctEquipMemo.Size = new System.Drawing.Size(395, 62);
			this.vctEquipMemo.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.vctEquipMemo.Text = "Equipment Type";
			this.vctEquipMemo.TextLocation = DevExpress.Utils.Locations.Top;
			this.vctEquipMemo.TextSize = new System.Drawing.Size(92, 16);
			// 
			// isdnNumberControl
			// 
			this.isdnNumberControl.Control = this.isdnNumber;
			this.isdnNumberControl.CustomizationFormText = "ISDN Number";
			this.isdnNumberControl.Location = new System.Drawing.Point(0, 122);
			this.isdnNumberControl.MinSize = new System.Drawing.Size(166, 26);
			this.isdnNumberControl.Name = "isdnNumberControl";
			this.isdnNumberControl.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 2, 2, 2);
			this.isdnNumberControl.Size = new System.Drawing.Size(202, 30);
			this.isdnNumberControl.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.isdnNumberControl.Text = "ISDN Number:";
			this.isdnNumberControl.TextSize = new System.Drawing.Size(92, 16);
			// 
			// isdnSpeedControl
			// 
			this.isdnSpeedControl.Control = this.isdnSpeedCombo;
			this.isdnSpeedControl.ControlAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this.isdnSpeedControl.CustomizationFormText = "ISDN Speed";
			this.isdnSpeedControl.Location = new System.Drawing.Point(202, 122);
			this.isdnSpeedControl.MinSize = new System.Drawing.Size(28, 26);
			this.isdnSpeedControl.Name = "isdnSpeedControl";
			this.isdnSpeedControl.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 2, 2, 2);
			this.isdnSpeedControl.Size = new System.Drawing.Size(193, 30);
			this.isdnSpeedControl.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.isdnSpeedControl.Text = "Speed, k:";
			this.isdnSpeedControl.TextSize = new System.Drawing.Size(92, 16);
			// 
			// ipNumberControl
			// 
			this.ipNumberControl.Control = this.ipNumber;
			this.ipNumberControl.CustomizationFormText = "IP Number";
			this.ipNumberControl.Location = new System.Drawing.Point(0, 152);
			this.ipNumberControl.MinSize = new System.Drawing.Size(166, 26);
			this.ipNumberControl.Name = "ipNumberControl";
			this.ipNumberControl.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 2, 2, 2);
			this.ipNumberControl.Size = new System.Drawing.Size(202, 30);
			this.ipNumberControl.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.ipNumberControl.Text = "IP Number:";
			this.ipNumberControl.TextSize = new System.Drawing.Size(92, 16);
			// 
			// ipSpeedControl
			// 
			this.ipSpeedControl.Control = this.ipSpeedCombo;
			this.ipSpeedControl.CustomizationFormText = "IP Speed";
			this.ipSpeedControl.Location = new System.Drawing.Point(202, 152);
			this.ipSpeedControl.MinSize = new System.Drawing.Size(158, 26);
			this.ipSpeedControl.Name = "ipSpeedControl";
			this.ipSpeedControl.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 2, 2, 2);
			this.ipSpeedControl.Size = new System.Drawing.Size(193, 30);
			this.ipSpeedControl.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.ipSpeedControl.Text = "Speed, k:";
			this.ipSpeedControl.TextSize = new System.Drawing.Size(92, 16);
			// 
			// layoutControlItem12
			// 
			this.layoutControlItem12.Control = this.vctpLocationName;
			this.layoutControlItem12.CustomizationFormText = "Location Name:";
			this.layoutControlItem12.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem12.MinSize = new System.Drawing.Size(173, 26);
			this.layoutControlItem12.Name = "layoutControlItem12";
			this.layoutControlItem12.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 2, 4, 4);
			this.layoutControlItem12.Size = new System.Drawing.Size(317, 30);
			this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem12.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 0, 0, 0);
			this.layoutControlItem12.Text = "Location Name:";
			this.layoutControlItem12.TextSize = new System.Drawing.Size(92, 16);
			// 
			// layoutControlItem14
			// 
			this.layoutControlItem14.Control = this.vctpLocationInfoIcon;
			this.layoutControlItem14.CustomizationFormText = "layoutControlItem14";
			this.layoutControlItem14.Location = new System.Drawing.Point(317, 0);
			this.layoutControlItem14.MinSize = new System.Drawing.Size(10, 20);
			this.layoutControlItem14.Name = "layoutControlItem14";
			this.layoutControlItem14.Size = new System.Drawing.Size(78, 30);
			this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem14.Text = "layoutControlItem14";
			this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem14.TextToControlDistance = 0;
			this.layoutControlItem14.TextVisible = false;
			// 
			// contactGroup
			// 
			this.contactGroup.CustomizationFormText = "Technical Contact Details";
			this.contactGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.technicalContactNameControl,
            this.technicalContactPhoneControl,
            this.technicalContactEmailControl});
			this.contactGroup.Location = new System.Drawing.Point(0, 210);
			this.contactGroup.Name = "contactGroup";
			this.contactGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
			this.contactGroup.Size = new System.Drawing.Size(401, 120);
			this.contactGroup.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
			this.contactGroup.Text = "Technical Contact Details";
			// 
			// technicalContactNameControl
			// 
			this.technicalContactNameControl.Control = this.technicalContactName;
			this.technicalContactNameControl.CustomizationFormText = "layoutControlItem8";
			this.technicalContactNameControl.Location = new System.Drawing.Point(0, 0);
			this.technicalContactNameControl.MinSize = new System.Drawing.Size(150, 26);
			this.technicalContactNameControl.Name = "technicalContacttNameControl";
			this.technicalContactNameControl.Size = new System.Drawing.Size(395, 31);
			this.technicalContactNameControl.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.technicalContactNameControl.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 0, 0, 0);
			this.technicalContactNameControl.Text = "Name:";
			this.technicalContactNameControl.TextSize = new System.Drawing.Size(92, 16);
			// 
			// technicalContactPhoneControl
			// 
			this.technicalContactPhoneControl.Control = this.technicalContactPhone;
			this.technicalContactPhoneControl.CustomizationFormText = "Phone:";
			this.technicalContactPhoneControl.Location = new System.Drawing.Point(0, 31);
			this.technicalContactPhoneControl.MinSize = new System.Drawing.Size(150, 26);
			this.technicalContactPhoneControl.Name = "technicalContactPhoneControl";
			this.technicalContactPhoneControl.Size = new System.Drawing.Size(395, 30);
			this.technicalContactPhoneControl.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.technicalContactPhoneControl.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 0, 0, 0);
			this.technicalContactPhoneControl.Text = "Phone:";
			this.technicalContactPhoneControl.TextSize = new System.Drawing.Size(92, 16);
			// 
			// technicalContactEmailControl
			// 
			this.technicalContactEmailControl.Control = this.technicalContactMail;
			this.technicalContactEmailControl.CustomizationFormText = "Email:";
			this.technicalContactEmailControl.Location = new System.Drawing.Point(0, 61);
			this.technicalContactEmailControl.MinSize = new System.Drawing.Size(166, 26);
			this.technicalContactEmailControl.Name = "technicalContactEmailControl";
			this.technicalContactEmailControl.Size = new System.Drawing.Size(395, 31);
			this.technicalContactEmailControl.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.technicalContactEmailControl.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 0, 0, 0);
			this.technicalContactEmailControl.Text = "Email:";
			this.technicalContactEmailControl.TextSize = new System.Drawing.Size(92, 16);
			// 
			// meetingGroup
			// 
			this.meetingGroup.CustomizationFormText = "Meeting Only";
			this.meetingGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.locationGroup,
            this.cateringGroup,
            this.managerGroup,
            this.coordinatorGroup,
            this.descrGroup,
            this.equipGroup,
            this.notesGroup,
            this.layoutControlItem11});
			this.meetingGroup.Location = new System.Drawing.Point(0, 0);
			this.meetingGroup.Name = "meetingGroup";
			this.meetingGroup.Size = new System.Drawing.Size(715, 467);
			this.meetingGroup.Text = "Meeting Only";
			// 
			// locationGroup
			// 
			this.locationGroup.CustomizationFormText = "Location Details";
			this.locationGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem7,
            this.layoutControlItem4,
            this.layoutControlItem3,
            this.layoutControlItem13,
            this.addressLayoutControl,
            this.capacityMeetingRoomControlItem,
            this.roomLayout});
			this.locationGroup.Location = new System.Drawing.Point(0, 0);
			this.locationGroup.Name = "locationGroup";
			this.locationGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
			this.locationGroup.Size = new System.Drawing.Size(350, 235);
			this.locationGroup.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
			this.locationGroup.Text = "Location Details";
			// 
			// layoutControlItem5
			// 
			this.layoutControlItem5.Control = this.meetingOnlyLocationName;
			this.layoutControlItem5.CustomizationFormText = "Location Name:";
			this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem5.MinSize = new System.Drawing.Size(166, 26);
			this.layoutControlItem5.Name = "layoutControlItem5";
			this.layoutControlItem5.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 2, 2, 2);
			this.layoutControlItem5.Size = new System.Drawing.Size(248, 30);
			this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem5.Text = "Location Name:";
			this.layoutControlItem5.TextSize = new System.Drawing.Size(92, 16);
			// 
			// layoutControlItem7
			// 
			this.layoutControlItem7.AppearanceItemCaption.Image = global::MyVrm.Outlook.WinForms.Properties.Resources.information;
			this.layoutControlItem7.AppearanceItemCaption.Options.UseImage = true;
			this.layoutControlItem7.Control = this.labelControl2;
			this.layoutControlItem7.CustomizationFormText = "infoLable";
			this.layoutControlItem7.Location = new System.Drawing.Point(248, 0);
			this.layoutControlItem7.MinSize = new System.Drawing.Size(79, 20);
			this.layoutControlItem7.Name = "layoutControlItem7";
			this.layoutControlItem7.Size = new System.Drawing.Size(96, 30);
			this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem7.Text = "infoLable";
			this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem7.TextToControlDistance = 0;
			this.layoutControlItem7.TextVisible = false;
			// 
			// layoutControlItem4
			// 
			this.layoutControlItem4.Control = this.stateCombo;
			this.layoutControlItem4.CustomizationFormText = "State:";
			this.layoutControlItem4.Location = new System.Drawing.Point(0, 61);
			this.layoutControlItem4.MinSize = new System.Drawing.Size(166, 26);
			this.layoutControlItem4.Name = "layoutControlItem4";
			this.layoutControlItem4.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 2, 2, 2);
			this.layoutControlItem4.Size = new System.Drawing.Size(344, 30);
			this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem4.Text = "State:";
			this.layoutControlItem4.TextSize = new System.Drawing.Size(92, 16);
			// 
			// layoutControlItem3
			// 
			this.layoutControlItem3.Control = this.countryCombo;
			this.layoutControlItem3.CustomizationFormText = "Country:";
			this.layoutControlItem3.Location = new System.Drawing.Point(0, 30);
			this.layoutControlItem3.MinSize = new System.Drawing.Size(166, 26);
			this.layoutControlItem3.Name = "layoutControlItem3";
			this.layoutControlItem3.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 2, 2, 2);
			this.layoutControlItem3.Size = new System.Drawing.Size(344, 31);
			this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem3.Text = "Country:";
			this.layoutControlItem3.TextSize = new System.Drawing.Size(92, 16);
			// 
			// layoutControlItem13
			// 
			this.layoutControlItem13.Control = this.citySuburb;
			this.layoutControlItem13.CustomizationFormText = "City/Suburb:";
			this.layoutControlItem13.Location = new System.Drawing.Point(0, 91);
			this.layoutControlItem13.MinSize = new System.Drawing.Size(152, 26);
			this.layoutControlItem13.Name = "layoutControlItem13";
			this.layoutControlItem13.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 2, 2, 2);
			this.layoutControlItem13.Size = new System.Drawing.Size(344, 30);
			this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem13.Text = "City/Suburb:";
			this.layoutControlItem13.TextSize = new System.Drawing.Size(92, 16);
			// 
			// addressLayoutControl
			// 
			this.addressLayoutControl.Control = this.addressMemoEdit;
			this.addressLayoutControl.CustomizationFormText = "Address:";
			this.addressLayoutControl.Location = new System.Drawing.Point(0, 121);
			this.addressLayoutControl.MinSize = new System.Drawing.Size(133, 22);
			this.addressLayoutControl.Name = "addressLayoutControl";
			this.addressLayoutControl.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 2, 2, 2);
			this.addressLayoutControl.Size = new System.Drawing.Size(344, 26);
			this.addressLayoutControl.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.addressLayoutControl.Text = "Address:";
			this.addressLayoutControl.TextSize = new System.Drawing.Size(92, 16);
			// 
			// capacityMeetingRoomControlItem
			// 
			this.capacityMeetingRoomControlItem.Control = this.meetingCapacityCombo;
			this.capacityMeetingRoomControlItem.CustomizationFormText = "Capacity:";
			this.capacityMeetingRoomControlItem.Location = new System.Drawing.Point(0, 177);
			this.capacityMeetingRoomControlItem.MinSize = new System.Drawing.Size(173, 26);
			this.capacityMeetingRoomControlItem.Name = "capacityMeetingRoomControlItem";
			this.capacityMeetingRoomControlItem.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 2, 2, 2);
			this.capacityMeetingRoomControlItem.Size = new System.Drawing.Size(344, 30);
			this.capacityMeetingRoomControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.capacityMeetingRoomControlItem.Text = "Capacity:";
			this.capacityMeetingRoomControlItem.TextSize = new System.Drawing.Size(92, 16);
			// 
			// roomLayout
			// 
			this.roomLayout.Control = this.roomLayoutComboBox;
			this.roomLayout.CustomizationFormText = "layoutControlItem12";
			this.roomLayout.Location = new System.Drawing.Point(0, 147);
			this.roomLayout.MinSize = new System.Drawing.Size(173, 26);
			this.roomLayout.Name = "roomLayout";
			this.roomLayout.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 2, 2, 2);
			this.roomLayout.Size = new System.Drawing.Size(344, 30);
			this.roomLayout.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.roomLayout.Text = "Layout:";
			this.roomLayout.TextSize = new System.Drawing.Size(92, 16);
			// 
			// cateringGroup
			// 
			this.cateringGroup.CustomizationFormText = "Catering Options";
			this.cateringGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.cateringCheckList});
			this.cateringGroup.Location = new System.Drawing.Point(362, 0);
			this.cateringGroup.Name = "cateringGroup";
			this.cateringGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
			this.cateringGroup.Size = new System.Drawing.Size(353, 131);
			this.cateringGroup.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
			this.cateringGroup.Text = "Catering Options";
			// 
			// cateringCheckList
			// 
			this.cateringCheckList.Control = this.caiteringCheckedListBox;
			this.cateringCheckList.CustomizationFormText = "Catering Options";
			this.cateringCheckList.Location = new System.Drawing.Point(0, 0);
			this.cateringCheckList.MinSize = new System.Drawing.Size(112, 39);
			this.cateringCheckList.Name = "cateringCheckList";
			this.cateringCheckList.Size = new System.Drawing.Size(347, 103);
			this.cateringCheckList.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.cateringCheckList.Text = "Catering Options";
			this.cateringCheckList.TextLocation = DevExpress.Utils.Locations.Top;
			this.cateringCheckList.TextSize = new System.Drawing.Size(0, 0);
			this.cateringCheckList.TextToControlDistance = 0;
			this.cateringCheckList.TextVisible = false;
			// 
			// managerGroup
			// 
			this.managerGroup.CustomizationFormText = "Manager Details";
			this.managerGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.managerNameControl,
            this.managerPhoneControl,
            this.managerEmailControl});
			this.managerGroup.Location = new System.Drawing.Point(0, 341);
			this.managerGroup.Name = "managerGroup";
			this.managerGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
			this.managerGroup.Size = new System.Drawing.Size(350, 126);
			this.managerGroup.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
			this.managerGroup.Text = "Manager Details";
			// 
			// managerNameControl
			// 
			this.managerNameControl.Control = this.managerName;
			this.managerNameControl.CustomizationFormText = "Name:";
			this.managerNameControl.Location = new System.Drawing.Point(0, 0);
			this.managerNameControl.MinSize = new System.Drawing.Size(147, 26);
			this.managerNameControl.Name = "managerNameControl";
			this.managerNameControl.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 2, 2, 2);
			this.managerNameControl.Size = new System.Drawing.Size(344, 33);
			this.managerNameControl.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.managerNameControl.Text = "Name:";
			this.managerNameControl.TextSize = new System.Drawing.Size(92, 16);
			// 
			// managerPhoneControl
			// 
			this.managerPhoneControl.Control = this.managerPhone;
			this.managerPhoneControl.CustomizationFormText = "Phone:";
			this.managerPhoneControl.Location = new System.Drawing.Point(0, 33);
			this.managerPhoneControl.MinSize = new System.Drawing.Size(147, 26);
			this.managerPhoneControl.Name = "managerPhoneControl";
			this.managerPhoneControl.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 2, 2, 2);
			this.managerPhoneControl.Size = new System.Drawing.Size(344, 32);
			this.managerPhoneControl.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.managerPhoneControl.Text = "Phone:";
			this.managerPhoneControl.TextSize = new System.Drawing.Size(92, 16);
			// 
			// managerEmailControl
			// 
			this.managerEmailControl.Control = this.managerMail;
			this.managerEmailControl.CustomizationFormText = "Email:";
			this.managerEmailControl.Location = new System.Drawing.Point(0, 65);
			this.managerEmailControl.MinSize = new System.Drawing.Size(147, 26);
			this.managerEmailControl.Name = "managerEmailControl";
			this.managerEmailControl.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 2, 2, 1);
			this.managerEmailControl.Size = new System.Drawing.Size(344, 33);
			this.managerEmailControl.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.managerEmailControl.Text = "Email:";
			this.managerEmailControl.TextSize = new System.Drawing.Size(92, 16);
			// 
			// coordinatorGroup
			// 
			this.coordinatorGroup.CustomizationFormText = "Site Coordinator Contact Details";
			this.coordinatorGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.coordinatorEmailControl,
            this.coordinatorPhoneControl,
            this.coordinatorNameControl});
			this.coordinatorGroup.Location = new System.Drawing.Point(0, 235);
			this.coordinatorGroup.Name = "coordinatorGroup";
			this.coordinatorGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
			this.coordinatorGroup.Size = new System.Drawing.Size(350, 106);
			this.coordinatorGroup.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
			this.coordinatorGroup.Text = "Site Coordinator Contact Details";
			// 
			// coordinatorEmailControl
			// 
			this.coordinatorEmailControl.Control = this.siteCoordinatorMail;
			this.coordinatorEmailControl.CustomizationFormText = "Email:";
			this.coordinatorEmailControl.Location = new System.Drawing.Point(0, 52);
			this.coordinatorEmailControl.Name = "coordinatorEmailControl";
			this.coordinatorEmailControl.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 2, 2, 2);
			this.coordinatorEmailControl.Size = new System.Drawing.Size(344, 26);
			this.coordinatorEmailControl.Text = "Email:";
			this.coordinatorEmailControl.TextSize = new System.Drawing.Size(92, 16);
			// 
			// coordinatorPhoneControl
			// 
			this.coordinatorPhoneControl.Control = this.siteCoordinatorPhone;
			this.coordinatorPhoneControl.CustomizationFormText = "Phone:";
			this.coordinatorPhoneControl.Location = new System.Drawing.Point(0, 26);
			this.coordinatorPhoneControl.Name = "coordinatorPhoneControl";
			this.coordinatorPhoneControl.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 2, 2, 2);
			this.coordinatorPhoneControl.Size = new System.Drawing.Size(344, 26);
			this.coordinatorPhoneControl.Text = "Phone:";
			this.coordinatorPhoneControl.TextSize = new System.Drawing.Size(92, 16);
			// 
			// coordinatorNameControl
			// 
			this.coordinatorNameControl.Control = this.siteCoordinatorName;
			this.coordinatorNameControl.CustomizationFormText = "Name:";
			this.coordinatorNameControl.Location = new System.Drawing.Point(0, 0);
			this.coordinatorNameControl.Name = "coordinatorNameControl";
			this.coordinatorNameControl.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 2, 2, 2);
			this.coordinatorNameControl.Size = new System.Drawing.Size(344, 26);
			this.coordinatorNameControl.Text = "Name:";
			this.coordinatorNameControl.TextSize = new System.Drawing.Size(92, 16);
			// 
			// descrGroup
			// 
			this.descrGroup.CustomizationFormText = "Room Description";
			this.descrGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.descrMemo});
			this.descrGroup.Location = new System.Drawing.Point(362, 281);
			this.descrGroup.Name = "descrGroup";
			this.descrGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
			this.descrGroup.Size = new System.Drawing.Size(353, 102);
			this.descrGroup.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
			this.descrGroup.Text = "Room Description";
			// 
			// descrMemo
			// 
			this.descrMemo.Control = this.memoEdit1;
			this.descrMemo.CustomizationFormText = "Room Description";
			this.descrMemo.Location = new System.Drawing.Point(0, 0);
			this.descrMemo.MinSize = new System.Drawing.Size(14, 22);
			this.descrMemo.Name = "descrMemo";
			this.descrMemo.Size = new System.Drawing.Size(347, 74);
			this.descrMemo.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.descrMemo.Text = "Room Description";
			this.descrMemo.TextLocation = DevExpress.Utils.Locations.Top;
			this.descrMemo.TextSize = new System.Drawing.Size(0, 0);
			this.descrMemo.TextToControlDistance = 0;
			this.descrMemo.TextVisible = false;
			// 
			// equipGroup
			// 
			this.equipGroup.CustomizationFormText = "Aux Equipment Options";
			this.equipGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.equipCheckList});
			this.equipGroup.Location = new System.Drawing.Point(362, 131);
			this.equipGroup.Name = "equipGroup";
			this.equipGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
			this.equipGroup.Size = new System.Drawing.Size(353, 150);
			this.equipGroup.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
			this.equipGroup.Text = "Aux Equipment Options";
			// 
			// equipCheckList
			// 
			this.equipCheckList.Control = this.equipCheckedListBox;
			this.equipCheckList.CustomizationFormText = "Aux Equipment Options";
			this.equipCheckList.Location = new System.Drawing.Point(0, 0);
			this.equipCheckList.MinSize = new System.Drawing.Size(112, 40);
			this.equipCheckList.Name = "equipCheckList";
			this.equipCheckList.Size = new System.Drawing.Size(347, 122);
			this.equipCheckList.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.equipCheckList.Text = "Aux Equipment Options";
			this.equipCheckList.TextLocation = DevExpress.Utils.Locations.Top;
			this.equipCheckList.TextSize = new System.Drawing.Size(0, 0);
			this.equipCheckList.TextToControlDistance = 0;
			this.equipCheckList.TextVisible = false;
			// 
			// notesGroup
			// 
			this.notesGroup.CustomizationFormText = "Extra Notes";
			this.notesGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.notesMemo});
			this.notesGroup.Location = new System.Drawing.Point(362, 383);
			this.notesGroup.Name = "notesGroup";
			this.notesGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
			this.notesGroup.Size = new System.Drawing.Size(353, 84);
			this.notesGroup.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
			this.notesGroup.Text = "Extra Notes";
			// 
			// notesMemo
			// 
			this.notesMemo.Control = this.memoEdit2;
			this.notesMemo.CustomizationFormText = "Extra Notes";
			this.notesMemo.Location = new System.Drawing.Point(0, 0);
			this.notesMemo.MinSize = new System.Drawing.Size(126, 22);
			this.notesMemo.Name = "notesMemo";
			this.notesMemo.Size = new System.Drawing.Size(347, 56);
			this.notesMemo.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.notesMemo.Text = "Extra Notes";
			this.notesMemo.TextLocation = DevExpress.Utils.Locations.Top;
			this.notesMemo.TextSize = new System.Drawing.Size(0, 0);
			this.notesMemo.TextToControlDistance = 0;
			this.notesMemo.TextVisible = false;
			// 
			// layoutControlItem11
			// 
			this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
			this.layoutControlItem11.Location = new System.Drawing.Point(350, 0);
			this.layoutControlItem11.MinSize = new System.Drawing.Size(10, 10);
			this.layoutControlItem11.Name = "layoutControlItem11";
			this.layoutControlItem11.Size = new System.Drawing.Size(12, 467);
			this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem11.Text = "layoutControlItem11";
			this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem11.TextToControlDistance = 0;
			this.layoutControlItem11.TextVisible = false;
			// 
			// speedLabelLayoutControlItem
			// 
			this.speedLabelLayoutControlItem.CustomizationFormText = "layoutControlItem12";
			this.speedLabelLayoutControlItem.Location = new System.Drawing.Point(0, 448);
			this.speedLabelLayoutControlItem.Name = "layoutControlItem12";
			this.speedLabelLayoutControlItem.Size = new System.Drawing.Size(715, 24);
			this.speedLabelLayoutControlItem.Text = "layoutControlItem12";
			this.speedLabelLayoutControlItem.TextSize = new System.Drawing.Size(0, 0);
			this.speedLabelLayoutControlItem.TextToControlDistance = 0;
			this.speedLabelLayoutControlItem.TextVisible = false;
			// 
			// speedLabelLayout
			// 
			this.speedLabelLayout.CustomizationFormText = "speedLabelLayout";
			this.speedLabelLayout.Location = new System.Drawing.Point(0, 448);
			this.speedLabelLayout.Name = "speedLabelLayout";
			this.speedLabelLayout.Size = new System.Drawing.Size(715, 24);
			this.speedLabelLayout.Text = "speedLabelLayout";
			this.speedLabelLayout.TextSize = new System.Drawing.Size(0, 0);
			this.speedLabelLayout.TextToControlDistance = 0;
			this.speedLabelLayout.TextVisible = false;
			// 
			// CreateEditRoomDialog
			// 
			this.ApplyEnabled = true;
			this.ApplyVisible = true;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
			this.ClientSize = new System.Drawing.Size(763, 613);
			this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.Name = "CreateEditRoomDialog";
			this.Load += new System.EventHandler(this.CreateEditRoomDialog_Load);
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CreateEditRoomDialog_FormClosing);
			((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).EndInit();
			this.ContentPanel.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.infoLable)).EndInit();
			this.infoLable.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.meetingCapacityCombo.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.addressMemoEdit.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.roomLayoutComboBox.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.citySuburb.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imagePictureEdit.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.roomImageFilePath.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ipSpeedCombo.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vctpLocationName.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.isdnSpeedCombo.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ipNumber.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.latitudeVal.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.longitudeVal.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.isdnNumber.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.technicalContactMail.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.technicalContactPhone.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.technicalContactName.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vctpCapacityCombo.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.equipCheckedListBox)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.caiteringCheckedListBox)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.memoEdit2.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.memoEdit3.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.managerMail.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.managerPhone.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.managerName.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.siteCoordinatorMail.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.siteCoordinatorPhone.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.siteCoordinatorName.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.meetingOnlyLocationName.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.stateCombo.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.countryCombo.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.roomTypeCombo.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.roomTabbedGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vctpGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.roomImageGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.clearImageControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.geoCodeGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.longitudeControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.latitudeControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.optionslGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vctpCapacityComboControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vctEquipMemo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.isdnNumberControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.isdnSpeedControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ipNumberControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ipSpeedControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.contactGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.technicalContactNameControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.technicalContactPhoneControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.technicalContactEmailControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.meetingGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.locationGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.addressLayoutControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.capacityMeetingRoomControlItem)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.roomLayout)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cateringGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cateringCheckList)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.managerGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.managerNameControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.managerPhoneControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.managerEmailControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.coordinatorGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.coordinatorEmailControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.coordinatorPhoneControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.coordinatorNameControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.descrGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.descrMemo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.equipGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.equipCheckList)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.notesGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.notesMemo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.speedLabelLayoutControlItem)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.speedLabelLayout)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private DevExpress.XtraLayout.LayoutControl infoLable;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private DevExpress.XtraEditors.ComboBoxEdit roomTypeCombo;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
		private DevExpress.XtraEditors.LabelControl freeRoomsLable;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
		private DevExpress.XtraEditors.ComboBoxEdit stateCombo;
		private DevExpress.XtraEditors.ComboBoxEdit countryCombo;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
		private DevExpress.XtraEditors.TextEdit meetingOnlyLocationName;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
		private DevExpress.XtraEditors.LabelControl labelControl2;
		private DevExpress.XtraEditors.LabelControl labelControl1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
		private DevExpress.XtraLayout.LayoutControlGroup locationGroup;
		private DevExpress.XtraEditors.TextEdit siteCoordinatorName;
		private DevExpress.XtraEditors.TextEdit siteCoordinatorMail;
		private DevExpress.XtraEditors.TextEdit siteCoordinatorPhone;
		private DevExpress.XtraEditors.TextEdit managerMail;
		private DevExpress.XtraEditors.TextEdit managerPhone;
		private DevExpress.XtraEditors.TextEdit managerName;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
		private DevExpress.XtraEditors.MemoEdit memoEdit1;
		private DevExpress.XtraEditors.MemoEdit memoEdit2;
		private DevExpress.XtraEditors.CheckedListBoxControl equipCheckedListBox;
		private DevExpress.XtraEditors.CheckedListBoxControl caiteringCheckedListBox;
		private DevExpress.XtraLayout.LayoutControlItem cateringCheckList;
		private DevExpress.XtraLayout.LayoutControlItem equipCheckList;
		private DevExpress.XtraLayout.LayoutControlGroup cateringGroup;
		private DevExpress.XtraLayout.LayoutControlGroup equipGroup;
		private DevExpress.XtraEditors.ComboBoxEdit vctpCapacityCombo;
		private DevExpress.XtraEditors.MemoEdit memoEdit3;
		private DevExpress.XtraEditors.TextEdit technicalContactMail;
		private DevExpress.XtraEditors.TextEdit technicalContactPhone;
		private DevExpress.XtraEditors.TextEdit technicalContactName;
		private DevExpress.XtraEditors.ComboBoxEdit isdnSpeedCombo;
		private DevExpress.XtraEditors.TextEdit ipNumber;
		private DevExpress.XtraEditors.TextEdit isdnNumber;
		private DevExpress.XtraEditors.ComboBoxEdit ipSpeedCombo;
		private DevExpress.XtraEditors.TextEdit longitudeVal;
		private DevExpress.XtraEditors.TextEdit latitudeVal;
		private DevExpress.XtraEditors.TextEdit roomImageFilePath;
		private DevExpress.XtraEditors.SimpleButton imageButton;
		private DevExpress.XtraLayout.TabbedControlGroup roomTabbedGroup;
		private DevExpress.XtraLayout.LayoutControlGroup meetingGroup;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
		private DevExpress.XtraLayout.LayoutControlGroup vctpGroup;
		private DevExpress.XtraLayout.LayoutControlItem vctEquipMemo;
		private DevExpress.XtraLayout.LayoutControlItem vctpCapacityComboControl;
		private DevExpress.XtraLayout.LayoutControlItem ipNumberControl;
		private DevExpress.XtraLayout.LayoutControlItem isdnSpeedControl;
		private DevExpress.XtraLayout.LayoutControlItem ipSpeedControl;
		private DevExpress.XtraLayout.LayoutControlItem latitudeControl;
		private DevExpress.XtraLayout.LayoutControlItem longitudeControl;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
		private DevExpress.XtraLayout.LayoutControlGroup contactGroup;
		private DevExpress.XtraLayout.LayoutControlItem technicalContactNameControl;
		private DevExpress.XtraLayout.LayoutControlItem technicalContactPhoneControl;
		private DevExpress.XtraLayout.LayoutControlItem technicalContactEmailControl;
		private DevExpress.XtraLayout.LayoutControlGroup geoCodeGroup;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
		private DevExpress.XtraEditors.PictureEdit imagePictureEdit;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
		private DevExpress.XtraLayout.LayoutControlGroup roomImageGroup;
		private DevExpress.XtraLayout.LayoutControlGroup optionslGroup;
		private DevExpress.XtraLayout.LayoutControlGroup descrGroup;
		private DevExpress.XtraLayout.LayoutControlItem descrMemo;
		private DevExpress.XtraLayout.LayoutControlGroup notesGroup;
		private DevExpress.XtraLayout.LayoutControlItem notesMemo;
		private DevExpress.XtraLayout.LayoutControlGroup managerGroup;
		private DevExpress.XtraLayout.LayoutControlItem managerNameControl;
		private DevExpress.XtraLayout.LayoutControlItem managerPhoneControl;
		private DevExpress.XtraLayout.LayoutControlItem managerEmailControl;
		private DevExpress.XtraLayout.LayoutControlGroup coordinatorGroup;
		private DevExpress.XtraLayout.LayoutControlItem coordinatorEmailControl;
		private DevExpress.XtraLayout.LayoutControlItem coordinatorPhoneControl;
		private DevExpress.XtraLayout.LayoutControlItem coordinatorNameControl;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
		private DevExpress.XtraLayout.LayoutControlItem speedLabelLayoutControlItem;
		private DevExpress.XtraLayout.LayoutControlItem speedLabelLayout;
		private DevExpress.XtraEditors.TextEdit citySuburb;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
		private DevExpress.XtraEditors.MemoEdit addressMemoEdit;
		private DevExpress.XtraLayout.LayoutControlItem addressLayoutControl;
		private DevExpress.XtraEditors.ComboBoxEdit meetingCapacityCombo;
		private DevExpress.XtraLayout.LayoutControlItem capacityMeetingRoomControlItem;
		private DevExpress.XtraEditors.ImageComboBoxEdit roomLayoutComboBox;
		private DevExpress.XtraLayout.LayoutControlItem roomLayout;
		private System.Windows.Forms.ImageList roomLayoutList;
		private DevExpress.XtraEditors.TextEdit vctpLocationName;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
		private DevExpress.XtraLayout.LayoutControlItem isdnNumberControl;
		private DevExpress.XtraEditors.LabelControl vctpLocationInfoIcon;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
		private DevExpress.XtraEditors.SimpleButton clearImageBn;
		private DevExpress.XtraLayout.LayoutControlItem clearImageControl;
	}
}
