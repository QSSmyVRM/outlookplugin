﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;

namespace MyVrm.WebServices.Data
{
    internal class GetProviderWorkOrderDetailsResponse : ServiceResponse
    {
        internal CateringWorkOrder WorkOrder { get; private set; }

        internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
        {
            do
            {
                reader.Read();
                if (reader.LocalName == "Workorders")
                {
                    reader.Read();
                    if (reader.LocalName == "Workorder")
                    {
                        WorkOrder = new CateringWorkOrder(reader.Service);
                        WorkOrder.LoadFromXml(reader, true);
                    }
                }
                else
                {
                    reader.SkipCurrentElement();
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "GetProviderWorkorderDetails"));
        }
    }
}
