﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MyVrm.WebServices.Data;
using MyVrm.WebServices.Maps;
using Image=System.Drawing.Image;

namespace MyVrm.Outlook.WinForms.Conference
{
    public partial class RoomInformationDialog : Dialog
    {
        private Endpoint _endpoint;

        public RoomInformationDialog()
        {
            InitializeComponent();
            CancelVisible = false;
			ApplyEnabled = false;
			ApplyVisible = false;
        	roomNameLayoutControlItem.Text = Strings.RoomNameLableText;
			roomNameLayoutControlItem.CustomizationFormText = Strings.RoomNameLableText;
			layoutControlItem1.Text = Strings.TierInformationLableText;
			layoutControlItem3.Text = Strings.FloorLableText;
			layoutControlItem5.Text = Strings.TimeZoneLableText;
			layoutControlItem6.Text = Strings.PhoneNumberLableText;
			layoutControlItem7.Text = Strings.AddressLableText;
			layoutControlItem8.Text = Strings.ParkingDirectionsLableText;
			layoutControlItem9.Text = Strings.AssistantInChargeLableText;
			layoutControlItem10.Text = Strings.ApproversLableText;
			layoutControlItem11.Text = Strings.EndpointLableText;
			layoutControlItem2.Text = Strings.DepartmentsLableText;
			layoutControlItem12.Text = Strings.CateringFacilityLableText;
			layoutControlItem13.Text = Strings.ProjectorAvailableLableText;
			layoutControlItem4.Text = Strings.RoomNumberLableText;
			layoutControlItem16.Text = Strings.CapacityLabelText;
			layoutControlItem23.Text = Strings.RoomImagesLableText;
			layoutControlItem14.Text = Strings.MapImagesLableText;
			layoutControlItem17.Text = Strings.MiscImagesLableText;

			layoutControlItem1.CustomizationFormText = Strings.TierInformationLableText;
			layoutControlItem3.CustomizationFormText = Strings.FloorLableText;
			layoutControlItem5.CustomizationFormText = Strings.TimeZoneLableText;
			layoutControlItem6.CustomizationFormText = Strings.PhoneNumberLableText;
			layoutControlItem7.CustomizationFormText = Strings.AddressLableText;
			layoutControlItem8.CustomizationFormText = Strings.ParkingDirectionsLableText;
			layoutControlItem9.CustomizationFormText = Strings.AssistantInChargeLableText;
			layoutControlItem10.CustomizationFormText = Strings.ApproversLableText;
			layoutControlItem11.CustomizationFormText = Strings.EndpointLableText;
			layoutControlItem2.CustomizationFormText = Strings.DepartmentsLableText;
			layoutControlItem12.CustomizationFormText = Strings.CateringFacilityLableText;
			layoutControlItem13.CustomizationFormText = Strings.ProjectorAvailableLableText;
			layoutControlItem4.CustomizationFormText = Strings.RoomNumberLableText;
			layoutControlItem16.CustomizationFormText = Strings.CapacityLabelText;
			layoutControlItem23.CustomizationFormText = Strings.RoomImagesLableText;
			layoutControlItem14.CustomizationFormText = Strings.MapImagesLableText;
			layoutControlItem17.CustomizationFormText = Strings.MiscImagesLableText;
			Text = Strings.RoomInformationLableText;

            roomNameLabelControl.DataBindings.Add("Text", roomBindingSource, "Name", true, DataSourceUpdateMode.Never);
            floorLabelControl.DataBindings.Add("Text", roomBindingSource, "Floor", true, DataSourceUpdateMode.Never);
            roomNumberLabelControl.DataBindings.Add("Text", roomBindingSource, "Number", true,
                                                    DataSourceUpdateMode.Never);
            Binding timezoneBinding = timeZoneLabelControl.DataBindings.Add("Text", roomBindingSource, "Timezone", true,
                                                                            DataSourceUpdateMode.Never);
            timezoneBinding.Format += ((sender, e) =>
                                           {
                                               var timezoneInfo = (TimeZoneInfo) e.Value;
                                               if (timezoneInfo != null)
                                               {
                                                   e.Value = timezoneInfo.DisplayName;
                                               }
                                           });
            phoneLabelControl.DataBindings.Add("Text", roomBindingSource, "Phone", true,
                                               DataSourceUpdateMode.Never);

            Binding cateringBinding = cateringLabelControl.DataBindings.Add("Text", roomBindingSource,
                                                                            "CateringFacility", true,
                                                                            DataSourceUpdateMode.Never);
            cateringBinding.Format += ((sender, e) => e.Value = ((bool) e.Value) ? "Yes" : "No");
            Binding projectorBinding = projectorLabelControl.DataBindings.Add("Text", roomBindingSource, "Projector",
                                                                              true, DataSourceUpdateMode.Never);
            projectorBinding.Format += ((sender, e) => e.Value = ((bool) e.Value) ? "Yes" : "No");
            capacityLabelControl.DataBindings.Add("Text", roomBindingSource, "Capacity", true,
                                                  DataSourceUpdateMode.Never);
            assistantLabelControl.DataBindings.Add("Text", roomBindingSource, "AssistantName", true,
                                                   DataSourceUpdateMode.Never);
            Binding departmentsBinding = departmentsLabelControl.DataBindings.Add("Text", roomBindingSource,
                                                                                  "Departments", true,
                                                                                  DataSourceUpdateMode.Never);
            departmentsBinding.Format += ((sender, e) =>
                                              {
                                                  var departments = (IEnumerable<Department>) e.Value;
                                                  e.Value = string.Join(", ", departments.Select(department => department.ToString()).ToArray());
                                              });
            Binding endpointBinding = endpointLabelControl.DataBindings.Add("Text", roomBindingSource, "EndpointId", true,
                                                                            DataSourceUpdateMode.Never);
            endpointBinding.Format += ((sender, e) =>
                                           {
                                               if (_endpoint == null)
                                               {
                                                   var endpointId = (EndpointId)e.Value;
                                                   var cursor = Cursor.Current;
                                                   Cursor.Current = Cursors.WaitCursor;
                                                   try
                                                   {
                                                       _endpoint = MyVrmService.Service.GetEndpoint(endpointId);
                                                   }
                                                   finally
                                                   {
                                                       Cursor.Current = cursor;
                                                   }
                                               }
                                               e.Value = _endpoint.Name;
                                           });
            Binding approversBinding = approversLabelControl.DataBindings.Add("Text", roomBindingSource, "Approvers",
                                                                              true,
                                                                              DataSourceUpdateMode.Never);
            approversBinding.Format += ((sender, e) =>
                                            {
                                                var approvers = (RoomApprovers) e.Value;
                                                e.Value = approvers.ToString();
                                            });
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
         Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        public Room RoomProfile
        {
            set { roomBindingSource.DataSource = value; }
        }

        private void roomBindingSource_DataSourceChanged(object sender, EventArgs e)
        {
            var roomProfile = (Room) roomBindingSource.DataSource;
            if (roomProfile == null) 
                return;
            const string listSep = ", ";

            List<string> addressParts = new List<string>();
            if (!string.IsNullOrEmpty(roomProfile.StreetAddress1))
            {
                addressParts.Add(roomProfile.StreetAddress1);
            }
            else if (!string.IsNullOrEmpty(roomProfile.StreetAddress2))
            {
                addressParts.Add(roomProfile.StreetAddress2);
            }
            if (!string.IsNullOrEmpty(roomProfile.City))
            {
                addressParts.Add(roomProfile.City);
            }
            if (!string.IsNullOrEmpty(roomProfile.State))
            {
                addressParts.Add(roomProfile.State);
            }
            if (!string.IsNullOrEmpty(roomProfile.Country))
            {
                addressParts.Add(roomProfile.Country);
            }
            if (!string.IsNullOrEmpty(roomProfile.ZipCode))
            {
                addressParts.Add(roomProfile.ZipCode);
            }
            var address = string.Join(listSep, addressParts.ToArray());
            addressLabelControl.Text = address;
            tiersLabelControl.Text = string.Format("{0} > {1}", roomProfile.TopTierName, roomProfile.MiddleTierName);
            googleMapsHyperLink.Text = new GoogleMapsUri
                                           {
                                               Location = address,
                                               Latitude = roomProfile.Latitude,
                                               Longitude = roomProfile.Longitude
                                           }.ToString();
            yahooMapsHyperLink.Text = new YahooMapsUri()
                                          {
                                              Location = address,
                                              Latitude = roomProfile.Latitude,
                                              Longitude = roomProfile.Longitude
                                          }.ToString();
            bingMapsHyperLink.Text = new BingMapsUri()
                                         {
                                             Location = address,
                                             Latitude = roomProfile.Latitude,
                                             Longitude = roomProfile.Longitude
                                         }.ToString();
            if (roomProfile.RoomImages.Count > 0)
            {
                using(var imageStream = roomProfile.RoomImages[0].AsStream())
                {
                    roomPictureEdit.Image = Image.FromStream(imageStream);
                }
                AssignImagesToImageListControl(roomProfile.RoomImages.ToArray(), roomImagesControl);
            }
            AssignImagesToImageListControl(new [] {roomProfile.Images.Map1,  roomProfile.Images.Map2}, mapImagesControl);
            AssignImagesToImageListControl(new[] { roomProfile.Images.Misc1, roomProfile.Images.Misc2 }, miscImagesControl);
        }

        private void AssignImagesToImageListControl(WebServices.Data.Image[] imageArray, ImageListControl imageListControl)
        {
            imageListControl.Images = imageArray;
        }
    }
}