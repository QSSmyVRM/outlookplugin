﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.WebServices.Data
{
    internal class DeleteWorkOrderRequest : ServiceRequestBase<DeleteWorkOrderResponse>
    {
        public DeleteWorkOrderRequest(MyVrmService service) : base(service)
        {
        }

        internal ConferenceId ConferenceId { get; set; }
        internal WorkOrderId WorkOrderId { get; set; }

        #region Overrides of ServiceRequestBase<DeleteWorkOrderResponse>

        internal override string GetXmlElementName()
        {
            return "login";
        }

        internal override string GetCommandName()
        {
            return "DeleteWorkOrder";
        }

        internal override string GetResponseXmlElementName()
        {
            return "success";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            UserId.WriteToXml(writer);
            ConferenceId.WriteToXml(writer, "ConferenceID");
            WorkOrderId.WriteToXml(writer, "WorkorderID");
        }

        #endregion
    }
}
