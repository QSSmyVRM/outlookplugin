﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using CallWebApi.Classes;
using Microsoft.Office.Interop.Outlook;
using MyVrm.WebServices.Data.TrueDaybook;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Exception = System.Exception;

namespace MyVrm.WebServices.Data
{
	public class TrueDaybookService_v2
	{
		public const string VirtualConfIdPropName = "VConfId";
		public const string markerURL = "{conference-url}";
		public const string markerExtId = "{conference-external-id}";
		public const string markerId = "{conference-id}";

		public const string markerExtId_MyVRM = "{71}";
		public const string markerURL_MyVRM = "{72}";

		private Guid _sessionId; //per organization
		private int _maxAvailSeats = -1; //-1 - not obtained yet
		private TrueDaybookDefaults _trueDaybookDefaults;

		public enum UserState
		{
			Undefined = -1,
			RegularUser,
			PowerUser
		}

		public UserState LoggedUserState;

		private bool _isLogged;
		public bool IsLogged
		{
			get { return _isLogged; }
			set { _isLogged = value; }
		}
		private long _currSettingsVersion = -1;
		public long CurrSettingsVersion
		{
			get { return _currSettingsVersion; }
			set { _currSettingsVersion = value; }
		}

		private long _lastReadSettingsVersion = 0;
		public long LastReadSettingsVersion
		{
			get { return _lastReadSettingsVersion; }
			set { _lastReadSettingsVersion = value; }
		}

		public static object CallWebApi(object data, Uri webApiUrl, Type type, string method)
		{
			// Create a WebClient to POST the request
			WebClient client = new WebClient();

			// Set the header so it knows we are sending JSON
			client.Headers[HttpRequestHeader.ContentType] = "application/json";

			// Serialise the data we are sending in to JSON
			string serialisedData = JsonConvert.SerializeObject(data, new IsoDateTimeConverter());

			string response;
			// Make the request
			if (method == "GET")
				response = client.DownloadString(webApiUrl);
			else
				response = client.UploadString(webApiUrl, method, serialisedData);

			// Deserialise the response into a GUID
			return JsonConvert.DeserializeObject(response, type);
		}

		/// <summary>
		/// Call Web API service by GET method
		/// </summary>
		/// <param name="webApiUrl">Uri of request</param>
		/// <param name="type">Type of response for deserialization</param>
		/// <returns>Response object (deserialized)</returns>
		public static object GetWebApi(Uri webApiUrl, Type type)
		{
			return CallWebApi(null, webApiUrl, type, "GET");
		}

		/// <summary>
		/// Call Web API service by POST method
		/// </summary>
		/// <param name="data">Object - body of request</param>
		/// <param name="webApiUrl">Uri of request</param>
		/// <param name="type">Type of response for deserialization</param>
		/// <returns>Response object (deserialized)</returns>
		public static object PostWebApi(object data, Uri webApiUrl, Type type)
		{
			return CallWebApi(data, webApiUrl, type, "POST");
		}

		/// <summary>
		/// Call Web API service by PUT method
		/// </summary>
		/// <param name="data">Object - body of request</param>
		/// <param name="webApiUrl">Uri of request</param>
		/// <param name="type">Type of response for deserialization</param>
		/// <returns>Response object (deserialized)</returns>
		public static object PutWebApi(object data, Uri webApiUrl, Type type)
		{
			return CallWebApi(data, webApiUrl, type, "PUT");
		}

		/// <summary>
		/// Call Web API service by DELETE method
		/// </summary>
		/// <param name="data">Object - body of request</param>
		/// <param name="webApiUrl">Uri of request</param>
		/// <param name="type">Type of response for deserialization</param>
		/// <returns>Response object (deserialized)</returns>
		public static object DeleteWebApi(object data, Uri webApiUrl, Type type)
		{
			return CallWebApi(data, webApiUrl, type, "DELETE");
		}
		//---------
		/// <summary>
		/// Request to api/User/Authenticate
		/// </summary>
		/// <param name="name">User name</param>
		/// <param name="password">User password</param>
		/// <returns>Authentication ticket</returns>
		public bool Authenticate(bool forceAuthenticate)
		{
			bool bRet = false;

			if (_isLogged && !forceAuthenticate)
				return true;

			_isLogged = false;
			//UserId _UserId = MyVrmService.Service.UserId;

			AuthRequest request = new AuthRequest
			{
				Name = MyVrmService.Service.Credential != null ? MyVrmService.Service.Credential.UserName : string.Empty,
				Password = MyVrmService.Service.Credential != null ? MyVrmService.Service.Credential.Password : string.Empty
			};

			if (string.IsNullOrEmpty(request.Name) || string.IsNullOrEmpty(request.Password))
			{
				throw new NoCredentialsException();
			}

			Uri resURL = new Uri(string.Format("{0}/api/User", MyVrmService.Service.Url));

			AuthResponse response = PostWebApi(request, resURL, typeof(AuthResponse)) as AuthResponse;

			if (response.Success)
			{
				_sessionId = response.AuthenticationTicket;
				LoggedUserState = (UserState) response.UserType;
				LastReadSettingsVersion = response.SettingsVersion;
				bRet = true;
			}
			else
			{
				MyVrmService.TraceSource.TraceError(response.Code + response.Message);
				throw new ServiceRequestException(response.Message, new WebException());
			}

			_isLogged = bRet;

			return bRet;
		}

		public bool TestAuthenticate(string url, string name, string pwd)
		{
			bool bRet = false;

			//_isLogged = false;
			AuthRequest request = new AuthRequest
			{
				Name = name,
				Password = pwd
			};

			Uri resURL = new Uri(string.Format("{0}/api/User", string.IsNullOrEmpty(url) ? string.Empty : url));

			AuthResponse response = PostWebApi(request, resURL, typeof(AuthResponse)) as AuthResponse;

			if (response.Success)
			{
				//_sessionId = response.AuthenticationTicket;
				//LoggedUserState = (UserState)response.UserType;
				//LastReadSettingsVersion = response.SettingsVersion;
				bRet = true;
			}
			else
			{
				MyVrmService.TraceSource.TraceError(response.Code + response.Message);
				throw new ServiceRequestException(response.Message, new WebException());
			}

			//_isLogged = bRet;

			return bRet;
		}
		/// <summary>
		/// Request to api/Conference/PutConference
		/// </summary>
		/// <param name="ticket">Auth ticket</param>
		/// <param name="endDate">End date of conference</param>
		/// <param name="startDate">Start date of conference</param>
		/// <param name="conferenceId">Id of changed conference</param>
		public void UpdateVConf(int conferenceId, AppointmentItem appItem, bool forceAuthenticate)
		{
			try
			{
				Authenticate(forceAuthenticate);
			}
			catch (Exception ex)
			{
				throw ex;
			}

			DateTime start = appItem.Start.ToUniversalTime(); 
			DateTime end = appItem.End.ToUniversalTime();

			ConferenceRequest request = new ConferenceRequest
			{
				Id = conferenceId,
				Subject = appItem.Subject,
				StartDate = start,
				EndDate = end,
				InvitationBody = appItem.Body,
				Recurrence = "",
				Participants = TrueDayServices_v1.GetParticipantsList(appItem)
			};

			Uri resURL = new Uri( string.Format("{0}/api/Conference?authenticationTicket={1}", MyVrmService.Service.Url, _sessionId));

			ConferenceResponse response = PutWebApi(request, resURL, typeof(ConferenceResponse)) as ConferenceResponse;

			if (response != null)
			{
				switch ((WebApiCodes.PutConferenceCode) response.Code)
				{
					case WebApiCodes.PutConferenceCode.Success:
						LastReadSettingsVersion = response.SettingsVersion;
						break;
					case WebApiCodes.PutConferenceCode.ExpiredTicket:
						UpdateVConf(conferenceId, appItem, true);
						break;
					default:
						MyVrmService.TraceSource.TraceError(response.Code + response.Message);
						throw new ServiceRequestException(response.Message, new WebException());
						//break;
				}
			}
		}

		/*private*/public static string ResolveBodyText(string body, string marker, string replaceWith)
		{
			string ret = body;
			
			// Search for a first occurence of {conference-url} and replace it with conferenceUrl
			if (!string.IsNullOrEmpty(body))
			{
				int left = body.IndexOf(marker);
				while (left != -1)
				{					
					string next = string.Empty;
					if (left + marker.Length < body.Length)
						next = body.Substring(left + marker.Length);
					ret = body.Substring(0, left) + replaceWith + next;
					body = ret;
					left = body.IndexOf(marker);
				}
			}
			return ret;
		}

		/// <summary>
		/// Request to api/Conference/PostConference
		/// </summary>
		/// <param name="ticket">Auth ticket</param>
		/// <param name="startDate">Start date of conference</param>
		/// <param name="endDate">End date of conference</param>
		/// <returns>Id of created conference</returns>
		public void ScheduleNewVConf(AppointmentItem appItem, bool forceAuthenticate)
		{
			try
			{
				Authenticate(forceAuthenticate);
			}
			catch (Exception ex)
			{
				throw ex;
			}

			DateTime start = appItem.Start.ToUniversalTime();
			DateTime end = appItem.End.ToUniversalTime();

			ConferenceRequest request = new ConferenceRequest
			{
				Subject = appItem.Subject,
				StartDate = start,
				EndDate = end,
				InvitationBody = appItem.Body,
				Recurrence = null,
				Participants = TrueDayServices_v1.GetParticipantsList(appItem)
			};

			Uri resURL = new Uri(string.Format("{0}/api/Conference?authenticationTicket={1}", MyVrmService.Service.Url, _sessionId));

			ConferenceResponse response = PostWebApi(request, resURL, typeof(ConferenceResponse)) as ConferenceResponse;

			if (response != null)
			{
				switch ((WebApiCodes.PostConferenceCode)response.Code)
				{
					case WebApiCodes.PostConferenceCode.Success:
						if (appItem.UserProperties.Find(VirtualConfIdPropName, true) == null)
							appItem.UserProperties.Add(VirtualConfIdPropName, OlUserPropertyType.olText/*olInteger*/, false, OlFormatText.olFormatTextText);//olFormatNumberRaw);
						appItem.UserProperties[VirtualConfIdPropName].Value = response.Id;

						string body = appItem.Body;
						body = ResolveBodyText(body, markerURL, response.ConferenceUrl);
						body = ResolveBodyText(body, markerId, response.Id.ToString());
						body = ResolveBodyText(body, markerExtId, response.ExternalId.ToString());

						body = ResolveBodyText(body, markerURL, response.ConferenceUrl);
						body = ResolveBodyText(body, markerId, response.Id.ToString());
						body = ResolveBodyText(body, markerExtId, response.ExternalId.ToString());
						appItem.Body = body;
						
						LastReadSettingsVersion = response.SettingsVersion;
						break;
					case WebApiCodes.PostConferenceCode.ExpiredTicket:
						ScheduleNewVConf(appItem, true);
						break;
					default:
						MyVrmService.TraceSource.TraceError(response.Code + response.Message);
						throw new ServiceRequestException(response.Message, new WebException());
						//break;
				}
			}
		}


		/// <summary>
		/// Request to api/Settings/GetDefaultSettings
		/// </summary>
		/// <param name="ticket">Auth ticket</param>
		public TrueDaybookDefaults GetDefaultSettings(bool forceAuthenticate)
		{
			try
			{
				Authenticate(forceAuthenticate);
			}
			catch (Exception ex)
			{
				throw ex;
			}

			if (CurrSettingsVersion == LastReadSettingsVersion && _trueDaybookDefaults != null)
				return _trueDaybookDefaults;

			Uri resURL = new Uri(string.Format("{0}/api/Settings/GetDefaultSettings?authenticationTicket={1}", MyVrmService.Service.Url, _sessionId));
			DefaultSettingsResponse response = GetWebApi(resURL, typeof(DefaultSettingsResponse)) as DefaultSettingsResponse;

			if (response != null)
			{
				switch ((WebApiCodes.GetDefaultSettingsCode)response.Code)
				{
					case WebApiCodes.GetDefaultSettingsCode.Success:
						TrueDaybookDefaults.TxtTypeEnum fmt = TrueDaybookDefaults.TxtTypeEnum.Txt;
						_trueDaybookDefaults = new TrueDaybookDefaults(response.DefaultAppointmentSubject, response.DefaultAppointmentInvitation, fmt);
						_maxAvailSeats = response.MaximumAvailableSeats;
						CurrSettingsVersion = response.SettingsVersion;
						LastReadSettingsVersion = CurrSettingsVersion;
						break;
					case WebApiCodes.GetDefaultSettingsCode.ExpiredTicket:
						GetDefaultSettings(true);
						break;
					default:
						MyVrmService.TraceSource.TraceError(response.Code + response.Message);
						throw new ServiceRequestException(response.Message, new WebException());
						//break;
				}
			}

			return _trueDaybookDefaults;
		}

		/// <summary>
		/// Request to api/Settings/GetMaximumAvailableSeats
		/// </summary>
		/// <param name="ticket">Auth ticket</param>
		public int GetMaxAvail(bool forceAuthenticate)
		{
			try
			{
				Authenticate(forceAuthenticate);
			}
			catch (Exception ex)
			{
				throw ex;
			}

			if (_maxAvailSeats != -1 && CurrSettingsVersion == LastReadSettingsVersion)
				return _maxAvailSeats;

			Uri resURL = new Uri(string.Format("{0}/api/Settings/GetMaximumAvailableSeats?authenticationTicket={1}", MyVrmService.Service.Url, _sessionId));
			MaximumAvailableSeatsResponse response = GetWebApi(resURL, typeof(MaximumAvailableSeatsResponse)) as MaximumAvailableSeatsResponse;

			if (response != null)
			{
				switch ((WebApiCodes.GetMaximumAvailableSeatsCode)response.Code)
				{
					case WebApiCodes.GetMaximumAvailableSeatsCode.Success:
						_maxAvailSeats = response.MaximumAvailableSeats;
						CurrSettingsVersion = response.SettingsVersion;
						break;
					case WebApiCodes.GetMaximumAvailableSeatsCode.ExpiredTicket:
						GetMaxAvail(true);
						break;
					default:
						MyVrmService.TraceSource.TraceError(response.Code + response.Message);
						throw new ServiceRequestException(response.Message, new WebException());
						//break;
				}
			}
			return _maxAvailSeats;
		}

		/// <summary>
		/// Request to api/Settings/GetAvailableSeats
		/// </summary>
		/// <param name="ticket">Auth ticket</param>
		/// <param name="startDate">Start date of requested period</param>
		/// <param name="endDate">End date of requested period</param>
		public AvailSeatsCollection GetAvailSeats(DateTime start, DateTime end, int confId, bool forceAuthenticate)
		{
			try
			{
				Authenticate(forceAuthenticate);
			}
			catch (Exception ex)
			{
				throw ex;
			}

			AvailSeatsCollection ret = new AvailSeatsCollection();
			DateTime startUtc = start.ToUniversalTime();
			DateTime endUtc = end.ToUniversalTime();

			string id = confId > 0 ? string.Format("&id={0}", confId) : string.Empty;

			Uri resURL = new Uri(string.Format("{0}/api/Settings/GetAvailableSeats?authenticationTicket={1}&startDate={2}&endDate={3}{4}",
				MyVrmService.Service.Url, _sessionId, startUtc.ToString("s"), endUtc.ToString("s"), id));

			AvailableSeatsResponse response = GetWebApi(resURL, typeof(AvailableSeatsResponse)) as AvailableSeatsResponse;

			if (response != null)
			{
				switch ((WebApiCodes.GetAvailableSeatsCode)response.Code)
				{
					case WebApiCodes.GetAvailableSeatsCode.Success:
						CurrSettingsVersion = response.SettingsVersion;
						
						DateTime lastDate = new DateTime();
						int lastSeatsNum = 0;
						foreach (AvailableSeatsResponse.Reservation record in response.Reservations)
						{
							int seats = record.Seats;
							DateTime dt = record.Date;
							TimeSpan interval = dt.ToLocalTime() - start;
							lastDate = dt.ToLocalTime();
							lastSeatsNum = seats;
							ret.Add(new AvailSeats() { Seats = seats, TimeSpan = interval });
						}

						if (lastDate < end)
						{
							//int i = ret.Count;
							ret.Add(new AvailSeats() { Seats = lastSeatsNum, TimeSpan = end - start });
						}
						break;
					case WebApiCodes.GetAvailableSeatsCode.ExpiredTicket:
						GetAvailSeats(start, end, confId, true);
						break;
					default:
						MyVrmService.TraceSource.TraceError(response.Code + response.Message);
						throw new ServiceRequestException(response.Message, new WebException());
						//break;
				}
			}

			return ret;
		}

		/// <summary>
		/// Request to api/Conference/DeleteConference
		/// </summary>
		/// <param name="ticket">Auth ticket</param>
		/// <param name="conferenceId">Id of conference</param>
		public void DeleteVConf(int conferenceId, bool forceAuthenticate)
		{
			try
			{
				Authenticate(forceAuthenticate);
			}
			catch (Exception ex)
			{
				throw ex;
			}

			Uri resURL = new Uri(string.Format("{0}/api/Conference?authenticationTicket={1}&conferenceId={2}", MyVrmService.Service.Url, _sessionId, conferenceId)); ///DeleteConference

			BaseResponse response = DeleteWebApi(null, resURL, typeof(BaseResponse)) as BaseResponse;

			if (response != null)
			{
				switch ((WebApiCodes.DeleteConferenceCode)response.Code)
				{
					case WebApiCodes.DeleteConferenceCode.Success:
						CurrSettingsVersion = response.SettingsVersion;
						break;
					case WebApiCodes.DeleteConferenceCode.ExpiredTicket:
						DeleteVConf(conferenceId, true);
						break;
					default:
						MyVrmService.TraceSource.TraceError(response.Code + response.Message);
						throw new ServiceRequestException(response.Message, new WebException());
						//break;
				}
			}
		}
	}
}
