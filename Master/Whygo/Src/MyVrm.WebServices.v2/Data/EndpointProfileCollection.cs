﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Linq;

namespace MyVrm.WebServices.Data
{
    public sealed class EndpointProfileCollection : ComplexPropertyCollection<EndpointProfile>, IOwnedProperty
    {
        private Endpoint _owner;

		public override object Clone()
		{
			EndpointProfileCollection copy = new EndpointProfileCollection();
			foreach (var item in Items)
			{
				copy.Add((EndpointProfile)item.Clone());
			}
			copy.Owner = Owner; //?????

			return copy;
		}

        internal EndpointProfileCollection()
        {
            
        }

        public void Add(EndpointProfile endpointProfile)
        {
            InternalAdd(endpointProfile);
        }

        public EndpointProfile Add(string name)
        {
            var endpointProfile = new EndpointProfile {Name = name};
            InternalAdd(endpointProfile);
            return endpointProfile;
        }

        public EndpointProfile DefaultProfile
        {
            get
            {
                var id = _owner.DefaultProfileId;
                return Items.FirstOrDefault(match => Equals(match.Id, id));    
            }
        }

        #region Overrides of ComplexPropertyCollection<EndpointProfile>

        internal override EndpointProfile CreateComplexProperty(string xmlElementName)
        {
            if (xmlElementName == "Profile")
            {
                return new EndpointProfile();
            }
            return null;
        }

        internal override string GetCollectionItemXmlElementName(EndpointProfile complexProperty)
        {
            return "Profile";
        }

        #endregion

        #region Implementation of IOwnedProperty

        public ServiceObject Owner
        {
            get
            {
                return _owner;
            }
            set
            {
                var owner = value as Endpoint;
                if (owner == null)
                {
                    throw new ArgumentException("value is not an instance of Endpoint class");
                }
                _owner = owner;
            }
        }

        #endregion
    }
}
