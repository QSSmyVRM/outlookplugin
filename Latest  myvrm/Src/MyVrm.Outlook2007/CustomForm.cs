﻿using System;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Outlook;
using Microsoft.Vbe.Interop.Forms;
using Exception = System.Exception;

namespace MyVrm.Outlook
{
    public class CustomForm : IDisposable
    {
        private UserForm _form;
        private Inspector _inspector;
        private Control _controlHost;
        private string _internalName;

        public const string CustomFormControlHostProgId = "MyVrm.OutlookCustomFormControlHost";
        private const string CustomFormControlHostControlName = "customFormControlHost";

        public CustomForm(UserForm form, Inspector inspector)
        {
            if (form == null) 
                throw new ArgumentNullException("form");
            _form = form;
            _inspector = inspector;
            _form.Layout += FormLayout;
        }

        public void AssignControl(CustomFormControl control)
        {
            if (control == null) 
                throw new ArgumentNullException("control");
            control.OutlookForm = this;

            Control ctrl;
            try
            {
                ctrl = _form.Controls._GetItemByName(CustomFormControlHostControlName);
            }
            catch (Exception)
            {
                ctrl = null;
            }
            if (ctrl == null)
            {
                ctrl = _form.Controls.Add(CustomFormControlHostProgId, CustomFormControlHostControlName, true);
            }
            var controlHost = (ICustomFormControlHost) ctrl;
            if (controlHost == null)
            {
                throw new Exception("Host control is not valid");
            }
            _internalName = control.Name;
            controlHost.CustomFormControl = control;
            _controlHost = ctrl;
        }

        public object Item
        {
            get { return _inspector.CurrentItem; }
        }

        public string InternalName
        {
            get { return _internalName ?? string.Empty; }
        }

        public void OnStartup()
        {
            ((ICustomFormControlHost)_controlHost).OnStartup();
        }

        public void OnShutdown()
        {
            try
            {
                ((ICustomFormControlHost)_controlHost).OnShutdown();
            }
            finally
            {
                _form.Controls.Remove(CustomFormControlHostControlName);    
            }
        }

        public void Dispose()
        {
            if (_controlHost != null)
            {
                Marshal.ReleaseComObject(_controlHost);
                _controlHost = null;
            }
            if (_form != null)
            {
                Marshal.ReleaseComObject(_form);
                _form = null;
            }
            if (_inspector != null)
            {
                Marshal.ReleaseComObject(_inspector);
                _inspector = null;
            }
        }

        void FormLayout()
        {
            try
            {
                if (_controlHost != null)
                {
                    _controlHost.Width = _form.InsideWidth;
                    _controlHost.Height = _form.InsideHeight;
                }
            }
            catch
            {
            }
        }
    }
}
