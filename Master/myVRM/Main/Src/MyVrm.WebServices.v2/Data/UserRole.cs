﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    /// <summary>
    /// Represents a user role in myVRM.
    /// </summary>
    [ServiceObjectDefinition("role")]
    public class UserRole : ServiceObject
    {
        public UserRole(MyVrmService service) : base(service)
        {
        }

        /// <summary>
        /// The Role Id.
        /// </summary>
        public UserRoleId Id
        {
            get
            {
                return (UserRoleId) PropertyBag[UserRoleSchema.Id];
            }
        }
        /// <summary>
        /// Name of the role.
        /// </summary>
        public string Name
        {
            get
            {
                return (string) PropertyBag[UserRoleSchema.Name];
            }
        }
        /// <summary>
        /// The role is locked.
        /// </summary>
        public bool Locked
        {
            get
            {
				bool bRet = false;
				if (PropertyBag[UserRoleSchema.Locked] != null)
					bool.TryParse(PropertyBag[UserRoleSchema.Locked].ToString(), out bRet);
				return bRet;//(bool)PropertyBag[UserRoleSchema.Locked];
            }
        }
        /// <summary>
        /// The role is active.
        /// </summary>
        public bool IsActive
        {
            get
            {
				bool bRet = false;
				if (PropertyBag[UserRoleSchema.IsActive] != null)
					bool.TryParse(PropertyBag[UserRoleSchema.IsActive].ToString(), out bRet);
				return bRet;//(bool)PropertyBag[UserRoleSchema.IsActive];
            }
        }

        #region Overrides of ServiceObject

        internal override ServiceObjectSchema GetSchema()
        {
            return UserRoleSchema.Instance;
        }

        #endregion
    }
}
