﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace MyVrm.WebServices.Data
{
    internal class TimePropertyDefinition : TypedPropertyDefinition
    {
		private bool _assumeUniversal;

        internal TimePropertyDefinition(string xmlElementName) : base(xmlElementName)
        {
            PropertyType = typeof(DateTime);
        }

		internal TimePropertyDefinition(string xmlElementName, bool assumeUniversal)
			: base(xmlElementName)
		{
			PropertyType = typeof(DateTime);
			_assumeUniversal = assumeUniversal;
		}

        internal TimePropertyDefinition(string xmlElementName, PropertyDefinitionFlags flags) : base(xmlElementName, flags)
        {
            PropertyType = typeof(DateTime);
        }
		internal TimePropertyDefinition(string xmlElementName, PropertyDefinitionFlags flags, bool assumeUniversal)
			: base(xmlElementName, flags)
		{
			PropertyType = typeof(DateTime);
			_assumeUniversal = assumeUniversal;
		}


        #region Overrides of TypedPropertyDefinition

        internal override object Parse(string value)
        {
            return Utilities.TimeOfDayStringToTimeSpan(value);
        }

        internal override string ToString(object value)
        {
            return Utilities.TimeSpanToTimeOfDayString((TimeSpan)value);
        }

		internal override void LoadPropertyValueFromXml(MyVrmServiceXmlReader reader, PropertyBag propertyBag)
		{
			string s = reader.ReadElementValue(XmlNamespace.NotSpecified, XmlElementName);
			// Parse in UTC if only UTC is enabled on service object and UTC is assumed for this property
			propertyBag[this] = Utilities.TimeOfDayStringToTimeSpan(s, reader.Service.IsUtcEnabled && _assumeUniversal);
		}
        #endregion
    }
}
