﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Reflection;

namespace MyVrm.WebServices.Data
{
    [Serializable]
    public abstract class ServiceId : ComplexProperty
    {
        private string _id;

		public override object Clone()
		{
		    Type type = GetType();
			ConstructorInfo ctor = type.GetConstructor(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(string) }, null);
			return ctor != null ? ctor.Invoke(new object[] { _id }) : null;
		}

        internal ServiceId()
        {
        }

        internal ServiceId(string id) : this()
        {
            if (id == null)
            {
                throw new ArgumentNullException("id");                
            }
            _id = id.Trim();
        }

        public string Id
        {
            get { return _id; }
        }

        internal virtual bool IsValid
        {
            get
            {
                return !string.IsNullOrEmpty(Id);
            }
        }

        public override string ToString()
        {
            if (_id != null)
            {
                return _id;
            }
            return string.Empty;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj))
            {
                return true;
            }
            var id = obj as ServiceId;
            if (id == null)
            {
                return false;
            }
            return IsValid && id.IsValid && Id.Equals(id.Id);
        }

        public static bool operator ==(ServiceId id1, ServiceId id2)
        {
            return Equals(id1, id2);
        }

        public static bool operator !=(ServiceId id1, ServiceId id2)
        {
            return !Equals(id1, id2);
        }

        public override int GetHashCode()
        {
            if (!IsValid)
            {
                return base.GetHashCode();
            }
            return Id.GetHashCode();
        }

        internal void Assign(ServiceId source)
        {
            _id = source.Id;
        }
        internal abstract string GetXmlElementName();
    
        internal void LoadFromXml(MyVrmServiceXmlReader reader)
        {
            LoadFromXml(reader, GetXmlElementName());
        }

        internal void WriteToXml(MyVrmXmlWriter writer)
        {
            WriteToXml(writer, GetXmlElementName());
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            writer.WriteValue(Id);
        }

        internal override void ReadTextValueFromXml(MyVrmServiceXmlReader reader)
        {
            _id = reader.ReadValue();
        }
    }
}
