﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using Microsoft.VisualStudio.Tools.Applications.Runtime;
using MyVrm.DataImport.Excel;
using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Interop.Excel;

namespace MyVrmExcelImportTemplate
{
    public partial class MCUSheet
    {
        private BridgesTable _bridgesTable;
        private void MCUSheet_Startup(object sender, System.EventArgs e)
        {
            _bridgesTable = new BridgesTable(mcuTable, Globals.ThisWorkbook.Service, Globals.ThisWorkbook.ValidationListManager);
            Globals.ThisWorkbook.RegisterTable(mcuTable, _bridgesTable);
        }

        private void MCUSheet_Shutdown(object sender, System.EventArgs e)
        {
        }

        #region VSTO Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(MCUSheet_Startup);
            this.Shutdown += new System.EventHandler(MCUSheet_Shutdown);
        }

        #endregion
    }
}
