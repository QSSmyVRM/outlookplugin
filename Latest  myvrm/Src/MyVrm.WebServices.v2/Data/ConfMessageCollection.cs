﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	public class ConfMessageCollection : ComplexPropertyCollection<ConfMessage>
    {
		//public ConfMessageCollection()
		//{
			
		//}
		//public ConfMessageCollection(Collection<ConfMessage> messages)
		//{

		//}

		public override object Clone()
		{
			ConfMessageCollection copy = new ConfMessageCollection();
			foreach (var item in Items)
			{
				copy.InternalAdd((ConfMessage)item.Clone()); //???????
			}

			return copy;
		}
		
		public void Add(ConfMessage customAttribute)
        {
            InternalAdd(customAttribute);
        }

		public void Clear()
		{
			Items.Clear();
		}

        #region Overrides of ComplexPropertyCollection<CustomAttribute>

		internal override ConfMessage CreateComplexProperty(string xmlElementName)
        {
			return new ConfMessage();
        }

		internal override string GetCollectionItemXmlElementName(ConfMessage complexProperty)
		{
			return "ConfMessageList";
        }

		internal override void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
		{
			Items.Clear();
			do
			{
				reader.Read();
				if (reader.IsStartElement(XmlNamespace.NotSpecified, "ConfMessage"))
				{
					var msg = new ConfMessage();
					msg.LoadFromXml(reader, "ConfMessage");
					Items.Add(msg);
				}
				else
				{
					reader.SkipCurrentElement();
				}
			} while (!reader.IsEndElement(XmlNamespace.NotSpecified, "ConfMessageList"));
		}

		internal override void WriteToXml(MyVrmXmlWriter writer, string xmlElementName)
		{
			writer.WriteStartElement(XmlNamespace.NotSpecified, "ConfMessageList");
			foreach (ConfMessage msg in Items)
			{
				msg.WriteElementsToXml(writer);
			}
			writer.WriteEndElement();
		}
        #endregion
    }
}