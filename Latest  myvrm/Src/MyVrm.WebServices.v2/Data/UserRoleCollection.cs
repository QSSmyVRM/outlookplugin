﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System.Collections;
using System.Collections.Generic;

namespace MyVrm.WebServices.Data
{
    /// <summary>
    /// Represents a collection of user roles.
    /// </summary>
    public class UserRoleCollection : IEnumerable<UserRole>
    {
        private readonly List<UserRole> _userRoles;

        internal UserRoleCollection()
        {
            _userRoles = new List<UserRole>();
        }


        #region Implementation of IEnumerable

        public IEnumerator<UserRole> GetEnumerator()
        {
            return _userRoles.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        internal void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
        {
            reader.EnsureCurrentNodeIsStartElement(XmlNamespace.NotSpecified, xmlElementName);
            if (!reader.IsEmptyElement)
            {
                do
                {
                    reader.Read();
                    if (reader.IsStartElement())
                    {
                        var userRole = new UserRole(reader.Service);
                        userRole.LoadFromXml(reader, true);
                        _userRoles.Add(userRole);
                    }
                }
                while (!reader.IsEndElement(XmlNamespace.NotSpecified, xmlElementName));
            }
        }
    }
}
