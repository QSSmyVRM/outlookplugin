﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System.Collections.ObjectModel;

namespace MyVrm.WebServices.Data
{
    internal class GetTopTierListRequest : ServiceRequestBase<GetTopTierListResponse>
    {
        public GetTopTierListRequest(MyVrmService service) : base(service)
        {
        }

        #region Overrides of ServiceRequestBase

        internal override string GetXmlElementName()
        {
            return "GetLocations";
        }

        internal override string GetCommandName()
        {
            return "GetLocations";
        }

        internal override string GetResponseXmlElementName()
        {
            return "GetLocations";
        }

        internal override GetTopTierListResponse ParseResponse(MyVrmServiceXmlReader reader)
        {
            var response = new GetTopTierListResponse();
            response.LoadFromXml(reader, GetResponseXmlElementName());
            return response;
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            writer.WriteElementValue(XmlNamespace.NotSpecified, "UserID", UserId.Id);
            OrganizationId.WriteToXml(writer);
        }
        #endregion
    }

    internal class GetMiddleTierListRequest : ServiceRequestBase<GetMiddleTierListResponse>
    {
        public GetMiddleTierListRequest(MyVrmService service) : base(service)
        {
        }

        public TierId ParentId { get; set; }
        #region Overrides of ServiceRequestBase

        internal override string GetXmlElementName()
        {
            return "GetLocations2";
        }

        internal override string GetCommandName()
        {
            return "GetLocations2";
        }

        internal override string GetResponseXmlElementName()
        {
            return "GetLocations2";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            writer.WriteElementValue(XmlNamespace.NotSpecified, "UserID", UserId.Id);
            OrganizationId.WriteToXml(writer);
            ParentId.WriteToXml(writer, "Tier1ID");
        }
        #endregion
    }
}
