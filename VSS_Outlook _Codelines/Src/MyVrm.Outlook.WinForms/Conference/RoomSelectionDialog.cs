﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.ViewInfo;
using DevExpress.XtraLayout.Utils;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Nodes;
using MyVrm.Common.Collections;
using MyVrm.Common.ComponentModel;
using MyVrm.Outlook.WinForms.Calendar;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook.WinForms.Conference
{
    public partial class RoomSelectionDialog : Dialog
    {
        private readonly List<ConferenceRoom> _allRooms = new List<ConferenceRoom>();
        private readonly DataList<ConferenceRoom> _rooms = new DataList<ConferenceRoom>();
        private readonly DataList<ConferenceRoom> _selectedRooms = new DataList<ConferenceRoom>();
        private User _userProfile;
        private readonly RepositoryItemEnumComboBox _mediaTypeRepositoryItem;
        private readonly TextEdit _searchValueTextEdit;
        private readonly EnumComboBoxEdit _searchValueComboBox;
        private readonly XtraUserControl _searchValueControl;
        private ConfRoom[] _preselectedRoomIds;

        private readonly TreeListColumn[] _advancedViewColumns;
        private readonly TreeListColumn[] _basicViewColumns;

        private const string RoomsViewLayoutFile = "RoomSelectionDialog_RoomsViewLayout.xml";
        private TreeListColumn _searchColumn;
        // TreeList column over which context menu poped up
        private TreeListColumn _clickedColumn;

        private readonly DXPopupMenu _restoreViewMenu;

        private class SearchField
        {
            private readonly TreeListColumn _column;
            
            public SearchField(TreeListColumn column)
            {
                _column = column;
            }

            public TreeListColumn Column
            {
                get { return _column; }
            }

            public override string ToString()
            {
                return _column.Caption;
            }
        }

    	private ConferenceType _conferenceType;
		public ConferenceType ConferenceType
    	{
    		set { _conferenceType = value; }
    		get { return _conferenceType; }
    	}

		public RoomSelectionDialog()
        {
            InitializeComponent();
			ApplyEnabled = false;
			ApplyVisible = false;
            Text = Strings.RoomSelectionDialogCaption;
            searchByLayoutControlItem.Text = Strings.RoomSelectionDialogSearchByText;
			searchByLayoutControlItem.CustomizationFormText = Strings.RoomSelectionDialogSearchByText;
            showOnlyFavoritesCheckEdit.Text = Strings.RoomSelectionDialogShowOnlyFavoritesCheckEditText;
            searchValueLayoutControlItem.Text = Strings.RoomSelectionDialogSearchForText;
            dataLoadingLabelControl.Text = Strings.RoomSelectionDialogUpdatingLabelText;
            changeViewButton.Text = Strings.RoomSelectionDialogChangeViewButtonText;
            hotdeskingCheckEdit.Text = Strings.HotDeskingCheckBoxText;
            chkSelectAll.Text = Strings.SelectAllText;
            layoutControlItem2.Visibility = LayoutVisibility.Never;


            roomNameColumn.Caption = Strings.RoomNameLableText;
			addressColumn.Caption = Strings.AddressLableText;
			floorColumn.Caption = Strings.FloorLableText;
            cityColumn.Caption = Strings.RoomSelectionDialogCityColumnCaption;
            stateColumn.Caption = Strings.RoomSelectionDialogStateColumnCaption;
            zipCodeColumn.Caption = Strings.RoomSelectionDialogZipCodeColumnCaption;
            phoneNumberColumn.Caption = Strings.RoomSelectionDialogPhoneColumnCaption;
            mediaTypeColumn.Caption = Strings.RoomSelectionDialogMediaTypeColumnCaption;
            assistantColumn.Caption = Strings.RoomSelectionDialogAssistantColumnCaption;
            middleTierColumn.Caption = Strings.RoomSelectionDialogMiddleTierColumnCaption;
			capacityColumn.Caption = Strings.CapacityLabelText;
			approval2Column.Caption = Strings.RoomSelectionDialogApprovalColumnCaption;

            selectRoomButton.Text = Strings.RoomSelectionDialogSelectRoomButtonText;
            removeRoomButton.Text = Strings.RoomSelectionDialogRemoveRoomButtonText;
			calendarButton.Text = Strings.RoomsCalendar;
			//showOnlyAvailableRoomsCheckEdit.Properties.Caption = Strings.ShowOnlyAvailableRoomsCaptionText;
			showOnlyFavoritesCheckEdit.Properties.Caption = Strings.RoomSelectionDialogShowOnlyFavoritesCheckEditText;
			
			//There is only one tooltip at advancedInforepositoryItem
            if(advancedInforepositoryItem.Buttons != null && advancedInforepositoryItem.Buttons.Count > 0)
			advancedInforepositoryItem.Buttons[0].ToolTip = Strings.AdvancedInfoToolTipText;

			OkEnabled = false;
		    calendarButton.Enabled = false;

            _restoreViewMenu = new DXPopupMenu();
            _restoreViewMenu.Items.Add(new DXMenuItem(Strings.RoomSelectionDialogBasicView,
                                                      (sender, e) => SetAdvancedRoomViewLayout(false)));
            _restoreViewMenu.Items.Add(new DXMenuItem(Strings.RoomSelectionDialogAdvancedView,
                                                      (sender, e) => SetAdvancedRoomViewLayout(true)));

            changeViewButton.DropDownControl = _restoreViewMenu;

            dataLoadingLayoutItem.Visibility = LayoutVisibility.Never;

            _advancedViewColumns = new[]
                                       {
                                           advancedInfoColumn, approval2Column, middleTierColumn, roomNameColumn, mediaTypeColumn,
                                           capacityColumn, addressColumn, floorColumn, cityColumn, stateColumn,
                                           zipCodeColumn, phoneNumberColumn
                                       };
            _basicViewColumns = new[]
                                    {
                                        advancedInfoColumn, approval2Column, middleTierColumn, roomNameColumn, mediaTypeColumn,
                                        capacityColumn
                                    };

            advancedInfoColumn.Fixed = FixedStyle.Left;

            foreach (TreeListColumn column in roomsTreeList.Columns)
            {
				if (column != advancedInfoColumn && column != approval2Column)
                    searchByComboBox.Properties.Items.Add(new SearchField(column));
            }
            SetAdvancedRoomViewLayout(true);

            _searchValueControl = new XtraUserControl ();
            searchValueLayoutControlItem.Control = _searchValueControl;

            _searchValueTextEdit = new TextEdit {Dock = DockStyle.Fill};
			_searchValueTextEdit.EditValueChanged += new EventHandler(_searchValueTextEdit_EditValueChanged);
               // (sender, e) => _rooms.CopyFrom(FindRooms((SearchField)searchByComboBox.SelectedItem, _searchValueTextEdit.Text, showOnlyFavoritesCheckEdit.Checked));

            _searchValueComboBox = new EnumComboBoxEdit();
            _searchValueComboBox.Width = 410; //ALLBUGS-19
            _searchValueComboBox.Properties.Items.AddRange(new EnumListSource(typeof(MediaType)).GetList());
            _searchValueComboBox.EditValueChanged += 
                (sender, e) => _rooms.CopyFrom(FindRooms((SearchField)searchByComboBox.SelectedItem, _searchValueComboBox.EditValue,
					showOnlyFavoritesCheckEdit.Checked, hotdeskingCheckEdit.Checked));

            _mediaTypeRepositoryItem = new RepositoryItemEnumComboBox
                                           {
                                               TextEditStyle = TextEditStyles.DisableTextEditor
                                           };

            _mediaTypeRepositoryItem.Items.AddRange(new EnumListSource(typeof(MediaType)).GetList());

            roomsTreeList.RepositoryItems.AddRange(new RepositoryItem[] { _mediaTypeRepositoryItem});

            mediaTypeColumn.ColumnEdit = _mediaTypeRepositoryItem;

            roomsTreeList.DataSource = _rooms;

            selectedRoomsListBox.DataSource = _selectedRooms;
            selectedRoomsListBox.DisplayMember = "Name";
            selectedRoomsListBox.ValueMember = "Id";
            selectedRoomsListBox.SortOrder = SortOrder.Ascending;

            _updateRoomDetailsDelegate = new UpdateRoomDetailsDelegate(UpdateRoomDetails);
		    _selectedRooms.ListChanged += OnListChanged;
			_deleteRoomDelegate = new DeleteRoomDelegate(DeleteRoom);
            searchByComboBox.SelectedIndex = 0;

            
        }

        //ALLBUGS-117
        private void checkEdit1_MouseDown(object sender, MouseEventArgs e)
        {
            CheckEdit cEdit = (CheckEdit)sender;

            if ((e.X < 72 && cEdit.Text == "Hotdesking") || (e.X < 118 && cEdit.Text == "Show Only Favorites")
                || (e.X < 64 && cEdit.Text == "Select All"))
            {
                if (cEdit.Text == "Hotdesking")
                    checkEditHotdesking_CheckedChanged(sender, e);
                else if (cEdit.Text == "Show Only Favorites")
                    showOnlyFavoritesCheckEdit_CheckedChanged(sender, e);
                else if (cEdit.Text == "Select All")
                    chkSelectAll_CheckedChanged(sender, e);
            }
            else
                ((DXMouseEventArgs)e).Handled = true;
        }

		void _searchValueTextEdit_EditValueChanged(object sender, EventArgs e)
		{
			//(sender, e) => 
			_rooms.CopyFrom(FindRooms((SearchField)searchByComboBox.SelectedItem, _searchValueTextEdit.Text, showOnlyFavoritesCheckEdit.Checked,
				hotdeskingCheckEdit.Checked));
			if (_allRooms.Count > 0)
			{
				if (!roomDetailsWorker.IsBusy)
				{
					dataLoadingLayoutItem.Visibility = LayoutVisibility.Always;
					roomDetailsWorker.RunWorkerAsync();
				}
				roomsTreeList.FocusedNode = roomsTreeList.Nodes.FirstNode;
			}
		}

    	private void OnListChanged(object sender, ListChangedEventArgs e)
    	{
            OkEnabled = calendarButton.Enabled = _selectedRooms.Count > 0;
    	}


    	public ConfRoom[] SelectedRooms
        {
            get
            {
               
                var roomIds = new ConfRoom[_selectedRooms.Count];
                for (var i = 0; i < _selectedRooms.Count; i++)
                {
                    var room = _selectedRooms[i];
                    if(room.Id!=new RoomId("0"))
                    roomIds[i] = new ConfRoom(room.Id);
                }
                return roomIds;
            }
            set
            {
                _preselectedRoomIds = value;
            }
        }



        private User UserProfile
        {
            get { return _userProfile ?? (_userProfile = MyVrmService.Service.GetUser()); }
        }

        public delegate void UpdateRoomDetailsDelegate(ConferenceRoom conferenceRoom);

        private readonly UpdateRoomDetailsDelegate _updateRoomDetailsDelegate;

        private void UpdateRoomDetails(ConferenceRoom conferenceRoom)
        {
            var idx = _rooms.IndexOf(conferenceRoom);
            if (idx != -1)
            {
                _rooms.ResetItem(idx);
            }
        }
		public delegate void DeleteRoomDelegate(RoomId roomId);

		private readonly DeleteRoomDelegate _deleteRoomDelegate;

		private void DeleteRoom(RoomId roomId)
		{
			int ndx = _rooms.Find("Id", roomId);
			if (ndx != -1)
				_rooms.RemoveAt(ndx);
		}

		private IEnumerable<ConferenceRoom> FindRooms(SearchField field, object value, bool favoritesOnly, bool hotdesking)
        {
            Predicate<ConferenceRoom> searchPredicate;
            switch (field.Column.FieldName)
            {
                case "Name":
                    searchPredicate = ((ConferenceRoom match) =>
                                           {
                                               if (string.IsNullOrEmpty(match.Name) && string.IsNullOrEmpty((string)value))
                                               {
                                                   return true;
                                               }
                                               if (match.Name != null)
                                               {
                                                   if (match.Name.StartsWith((string)value,
                                                                             StringComparison.InvariantCultureIgnoreCase))
                                                       return true;
                                               }
                                               return false;
                                           });
                    break;
                case "Address":
                    searchPredicate = ((ConferenceRoom match) =>
                                           {
                                               if (string.IsNullOrEmpty(match.Address) && string.IsNullOrEmpty((string)value))
                                               {
                                                   return true;
                                               }
                                               if (match.Address != null)
                                                   if (match.Address.StartsWith((string)value,
                                                                                StringComparison.
                                                                                    InvariantCultureIgnoreCase))
                                                       return true;
                                               return false;
                                           });
                    break;
                case "Floor":
                    searchPredicate = ((ConferenceRoom match) =>
                    {
                        if (string.IsNullOrEmpty(match.Floor) && string.IsNullOrEmpty((string)value))
                        {
                            return true;
                        }
                        if (match.Floor != null)
                            if (match.Floor.StartsWith((string)value,
                                                         StringComparison.
                                                             InvariantCultureIgnoreCase))
                                return true;
                        return false;
                    });
                    break;
                case "City":
                    searchPredicate = ((ConferenceRoom match) =>
                                           {
                                               if (string.IsNullOrEmpty(match.City) && string.IsNullOrEmpty((string)value))
                                               {
                                                   return true;
                                               }
                                               if (match.City != null)
                                                   if (match.City.StartsWith((string)value,
                                                                             StringComparison.InvariantCultureIgnoreCase))
                                                       return true;
                                               return false;
                                           });
                    break;
                case "State":
                    searchPredicate = ((ConferenceRoom match) =>
                                           {
                                               if (string.IsNullOrEmpty(match.State) && string.IsNullOrEmpty((string)value))
                                               {
                                                   return true;
                                               }
                                               if (match.State != null)
                                                   if (match.State.StartsWith((string)value,
                                                                              StringComparison.
                                                                                  InvariantCultureIgnoreCase))
                                                       return true;
                                               return false;
                                           });
                    break;
                case "ZipCode":
                    searchPredicate = ((ConferenceRoom match) =>
                                           {
                                               if (string.IsNullOrEmpty(match.ZipCode) && string.IsNullOrEmpty((string)value))
                                               {
                                                   return true;
                                               }
                                               if (match.ZipCode != null)
                                                   if (match.ZipCode.StartsWith((string)value,
                                                                                StringComparison.
                                                                                    InvariantCultureIgnoreCase))
                                                       return true;
                                               return false;
                                           });
                    break;
                case "Phone":
                    searchPredicate = ((ConferenceRoom match) =>
                    {
                        if (string.IsNullOrEmpty(match.Phone) && string.IsNullOrEmpty((string)value))
                        {
                            return true;
                        }
                        if (match.Phone != null)
                            if (match.Phone.StartsWith((string)value,
                                                         StringComparison.
                                                             InvariantCultureIgnoreCase))
                                return true;
                        return false;
                    });
                    break;
                case "AssistantInCharge":
                    searchPredicate = ((ConferenceRoom match) =>
                    {
                        if (string.IsNullOrEmpty(match.AssistantInCharge) && string.IsNullOrEmpty((string)value))
                        {
                            return true;
                        }
                        if (match.AssistantInCharge != null)
                            if (match.AssistantInCharge.StartsWith((string)value,
                                                         StringComparison.
                                                             InvariantCultureIgnoreCase))
                                return true;
                        return false;
                    });
                    break;
                case "MediaType":
                    searchPredicate = ((ConferenceRoom match) =>
                    {
                        if (value != null)
                        {
                            return match.MediaType == (MediaType)value;
                        }
                        return true;
                    });
                    break;
                case "MiddleTier":
                    searchPredicate = ((ConferenceRoom match) =>
                    {
                        if (string.IsNullOrEmpty(match.MiddleTier) && string.IsNullOrEmpty((string)value))
                        {
                            return true;
                        }
                        if (match.MiddleTier != null)
                            if (match.MiddleTier.StartsWith((string)value,
                                                         StringComparison.
                                                             InvariantCultureIgnoreCase))
                                return true;
                        return false;
                    });
                    break;
                case "Capacity":
                    searchPredicate = ((ConferenceRoom match) =>
                    {
                        if (!string.IsNullOrEmpty((string)value))
                        {
                            int capacity;
                            if (Int32.TryParse((string)value, out capacity))
                            {
                                return match.Capacity >= capacity;
                            }
                            return false;
                        }
                        return true;
                    });
                    break;
                default:
                    throw new ArgumentOutOfRangeException("field");
            }
            return FindRooms(_allRooms, favoritesOnly, hotdesking).FindAll(searchPredicate);
        }

		private List<ConferenceRoom> FindRooms(List<ConferenceRoom> rooms, bool favoritesOnly, bool hotdesking)
        {
			List<ConferenceRoom> favRooms = favoritesOnly ? rooms.FindAll(match => UserProfile.FavoriteRooms.Contains(match.Id)) : rooms;
			return hotdesking ? favRooms.FindAll(a => a.RoomCategory == RoomCategoryType.HotdeskingRoom) : favRooms;
        }

		public void SaveRoomsViewLayoutToFile(string FileName)
		{
			roomsTreeList.SaveLayoutToXml(Path.Combine(MyVrmAddin.LocalUserAppDataPath, FileName));
		}

    	private void SaveRoomsViewLayout()
        {
    		SaveRoomsViewLayoutToFile(RoomsViewLayoutFile);
        }

		public void RestoreRoomsViewLayoutWithFileName(string FileName)
		{
			var layoutFile = Path.Combine(MyVrmAddin.LocalUserAppDataPath, FileName);
			if (File.Exists(layoutFile))
			{
				roomsTreeList.RestoreLayoutFromXml(layoutFile);
			}
			else
			{
				SetAdvancedRoomViewLayout(true);
			}
		}

    	private void RestoreRoomsViewLayout()
    	{
			RestoreRoomsViewLayoutWithFileName(RoomsViewLayoutFile);
    	}

        private void roomsTreeList_DoubleClick(object sender, EventArgs e)
        {
            if(roomsTreeList.FocusedNode != null)
            {
                var conferenceRoom = _rooms[roomsTreeList.FocusedNode.Id];
                AddRoomToSelectionList(conferenceRoom);
            }
        }

        private void AddRoomToSelectionList(ConferenceRoom conferenceRoom)
        {
            if (_selectedRooms.Find("Id", conferenceRoom.Id) == -1)
            {
                _selectedRooms.Add(conferenceRoom);
            }
        }

        private void roomDetailsWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            // Get modified rooms
            var dateTimeInUserTimeZone =
                TimeZoneInfo.ConvertTime(UIHelper.Instance.Settings.RoomProfileListLastUpdated, UserProfile.TimeZone);
            var modifiedRoomIds = MyVrmService.Service.FindModifiedRooms(null, dateTimeInUserTimeZone);

            foreach (var conferenceRoom in _allRooms)
            {
                if (roomDetailsWorker.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }
                try
                {

                    var roomDetails = MyVrmAddin.Instance.GetRoomFromCache(conferenceRoom.Id,
                                                                           modifiedRoomIds.Any(
                                                                            roomId => roomId == conferenceRoom.Id));
                    if (roomDetails != null)
                    {
                        conferenceRoom.Floor = roomDetails.Floor;
                        conferenceRoom.Phone = roomDetails.Phone;
                        conferenceRoom.City = roomDetails.City;
                        conferenceRoom.Address = roomDetails.StreetAddress1;
                        conferenceRoom.State = roomDetails.State;
                        conferenceRoom.ZipCode = roomDetails.ZipCode;
                        conferenceRoom.MediaType = roomDetails.Media;
                        conferenceRoom.RoomCategory = roomDetails.RoomCategory;

                        bool bIsValidRoom = false;

                        switch (roomDetails.Media)
                        {
                            case MediaType.NoAudioVideo:
                                conferenceRoom.ImageIndex = 0;
                                bIsValidRoom = ConferenceType != 0 /*ConferenceType = 0 in case of Favorits dlg*/? ConferenceType == ConferenceType.RoomConference : true;
                                break;
                            case MediaType.AudioOnly:
                                conferenceRoom.ImageIndex = 0;
                                bIsValidRoom = ConferenceType != 0 ? ConferenceType != ConferenceType.AudioVideo : true;
                                break;
                            case MediaType.AudioVideo:
                                conferenceRoom.ImageIndex = 2;
                                if (conferenceRoom.RoomCategory == RoomCategoryType.HotdeskingRoom)
                                    conferenceRoom.ImageIndex = 5;

                                if (Conf.VMRType == VMRConferenceType.None) //i.e. for an A/V or Audio conference must have an endpoint
                                {
                                    if (roomDetails.EndpointId != EndpointId.Empty)
                                        //assume that AV-room without an endpoint is invalid or corrupted
                                        bIsValidRoom = true;
                                }
                                else //it is a VMR conference
                                {
                                    bIsValidRoom = true;
                                }
                                break;
                        }

                        if (conferenceRoom.RoomCategory == RoomCategoryType.HotdeskingRoom && ConferenceType == ConferenceType.Hotdesking)
                        {
                            bIsValidRoom = true;
                        }

                        conferenceRoom.AssistantInCharge = roomDetails.AssistantName;

                        if (!bIsValidRoom)
                        {
                            Invoke(_deleteRoomDelegate, conferenceRoom.Id);
                            //int ndx = _rooms.Find("Id", conferenceRoom.Id);
                            //if (ndx != -1)
                            //    _rooms.RemoveAt(ndx);
                        }
                        else
                        {
                            Invoke(_updateRoomDetailsDelegate, conferenceRoom);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MyVrmAddin.TraceSource.TraceException(ex);
                }
            }
        }

        private void RoomSelectionDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveRoomsViewLayout();
        	OnFormClosing();
        }

		public void OnFormClosing()
		{
			if (roomDetailsWorker.IsBusy)
			{
				roomDetailsWorker.CancelAsync();
			}
			Cursor = Cursors.WaitCursor;
			try
			{
				while (roomDetailsWorker.IsBusy)
				{
					Thread.Sleep(100);
					Application.DoEvents();
				}
			}
			finally
			{
				Cursor = Cursors.Default;
			}
		}

    	private void RoomSelectionDialog_Load(object sender, EventArgs e)
        {
            var cursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            RestoreRoomsViewLayout();
            if (Conf != null)
                layoutControlItem2.Visibility = LayoutVisibility.Always;
			OnLoad(cursor);
        }

    	public ConferenceWrapper Conf;
		public virtual ReadOnlyCollection<ManagedRoom> GetRooms()
		{
			ReadOnlyCollection<ManagedRoom> availableRooms = MyVrmService.Service.GetAvailableRooms(Conf.Conference.StartDate, Conf.Conference.Duration, Conf.Conference.TimeZone, Conf.Conference.IsRecurring,
				MediaTypeFilter.All, Conf.Conference.Type, Conf.Conference.Id);

			return availableRooms;
		}
		public void OnLoad(Cursor cursor)
		{
            try
            {
                _rooms.Clear();
                if (_allRooms.Count == 0)
                {
                    // filter out deleted rooms
                    var managedRooms = GetRooms();
                //    MyVrmService.TraceSource.TraceInformation("...Added for 101478");
                    foreach (var room in managedRooms)
                    {
                     //   MyVrmService.TraceSource.TraceInformation(" " +"Name = "+ room.Name +" " + "ID=" + room.Id + "");
                        if (room.Name != null)
                        {
                            _allRooms.Add(new ConferenceRoom
                            {
                                Id = room.Id,
                                Name = room.Name,
                                MiddleTier = room.MiddleTier.Name,
                                Capacity = room.Capacity,
                                Approval = room.Approval //> 0 ? true : false 
                            });
                        }
                    }
                   // MyVrmService.TraceSource.TraceInformation("...End for 101478");
                    _allRooms.Sort((x, y) => x.Name.CompareTo(y.Name));//x.Name.CompareTo(y.Name));
                    if (_preselectedRoomIds != null)
                    {
                        foreach (var confRoom in
                            _preselectedRoomIds.Select(roomId => _allRooms.FirstOrDefault(room => room.Id == roomId.ConfEndPt.Id)).Where(confRoom => confRoom != null))
                        {
                            AddRoomToSelectionList(confRoom);
                        }
                    }
                    if (!roomDetailsWorker.IsBusy)
                    {
                        dataLoadingLayoutItem.Visibility = LayoutVisibility.Always;
                        roomDetailsWorker.RunWorkerAsync();
                    }
                }
                _rooms.CopyFrom(_allRooms);
                roomsTreeList.DataSource = _rooms;
                roomsTreeList.Refresh();
                //if (Conf.Conference.Type == ConferenceType.Hotdesking)
                //{
                //    hotdeskingCheckEdit.Checked = true;
                //    hotdeskingCheckEdit.Enabled = false;
                //}

            }
            catch(Exception ex)
            {

            }
			finally
			{
				Cursor.Current = cursor;
			}
			
		}
        private void selectRoomButton_Click(object sender, EventArgs e)
        {
            foreach (TreeListNode treeListNode in roomsTreeList.Selection)
            {
                AddRoomToSelectionList(_rooms[treeListNode.Id]);
            }
        }

        private void removeRoomButton_Click(object sender, EventArgs e)
        {
            var toRemove = selectedRoomsListBox.SelectedItems.Cast<ConferenceRoom>().ToList();
            foreach (var room in toRemove)
            {
                _selectedRooms.Remove(room);
            }
        }

        private void SetAdvancedRoomViewLayout(bool advancedView)
        {
            var visibleColumns = advancedView ? _advancedViewColumns : _basicViewColumns;
            if (visibleColumns == null)
            {
                return;
            }
            try
            {
                roomsTreeList.BeginUpdate();
                foreach (TreeListColumn column in roomsTreeList.Columns)
                {
                    column.VisibleIndex = -1;
                }
                for (var i = 0; i < visibleColumns.Length; i++)
                {
                    var column = visibleColumns[i];
                    var width = column.VisibleWidth;
                    column.VisibleIndex = i;
                    // Workaround for issue with VisibleWidth property that is increased by ViewInfo.FirstColumnIndent when
                    // columns became visible again
                    column.Width = width - roomsTreeList.ViewInfo.FirstColumnIndent;
                }
                roomsTreeList.BestFitColumns();
            }
            finally
            {
                roomsTreeList.EndUpdate();
            }
        }

        private void selectedRoomsListBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                removeRoomButton_Click(this, EventArgs.Empty);
            }
        }

        private void advancedInforepositoryItem_ButtonPressed(object sender, ButtonPressedEventArgs e)
        {
            if (roomsTreeList.FocusedNode != null)
            {
                var conferenceRoom = _rooms[roomsTreeList.FocusedNode.Id];
                var room = MyVrmAddin.Instance.GetRoomFromCache(conferenceRoom.Id);
                ShowRoomInformationDialog(room);
            }
        }

        protected virtual void ShowRoomInformationDialog(Room room)
        {
            using (var dlg = new RoomInformationDialog())
            {
                Cursor = Cursors.WaitCursor;
                try
                {
                    dlg.RoomProfile = room;
                }
                finally
                {
                    Cursor = Cursors.Default;
                }
                dlg.ShowDialog(this);
            }
        }

        private void roomDetailsWorker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (Conf != null && Conf.Conference != null)
            {
                if (Conf.Conference.Type == ConferenceType.Hotdesking)
                {
                    hotdeskingCheckEdit.Checked = true;
                    hotdeskingCheckEdit.Enabled = false;
                }
            }
            // Save current time to room profiles last updated setting
            UIHelper.Instance.Settings.RoomProfileListLastUpdated = DateTime.Now;
            UIHelper.Instance.Settings.Save();
            dataLoadingLayoutItem.Visibility = LayoutVisibility.Never;
            if (e.Error != null)
            {
                ShowError(e.Error.Message);
            }
        }

        private void searchByComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var searchField = (SearchField) searchByComboBox.SelectedItem;
            _searchColumn = searchField.Column;
            _searchColumn.Visible = true;
            _searchValueControl.Controls.Clear();
            if (_searchColumn == mediaTypeColumn)
            {
                _searchValueComboBox.EditValue = null;
                _searchValueControl.Controls.Add(_searchValueComboBox);
            }
            else
            {
                _searchValueTextEdit.Text = string.Empty;
                _searchValueControl.Controls.Add(_searchValueTextEdit);
            }
            _searchValueComboBox.SelectedIndex = 2;//ALLBUGS-19
        }

        private void changeViewButton_Click(object sender, EventArgs e)
        {
            changeViewButton.ShowDropDown();
        }

        private void showOnlyFavoritesCheckEdit_CheckedChanged(object sender, EventArgs e)
        {
            var searchField = (SearchField) searchByComboBox.SelectedItem;
            _searchColumn = searchField.Column;
            _rooms.CopyFrom(FindRooms((SearchField) searchByComboBox.SelectedItem,
                                      _searchColumn == mediaTypeColumn
                                          ? _searchValueComboBox.EditValue
                                          : _searchValueTextEdit.Text, showOnlyFavoritesCheckEdit.Checked,
										  hotdeskingCheckEdit.Checked));
			if (_allRooms.Count > 0)
			{
				if (!roomDetailsWorker.IsBusy)
				{
					dataLoadingLayoutItem.Visibility = LayoutVisibility.Always;
					roomDetailsWorker.RunWorkerAsync();
				}
				roomsTreeList.FocusedNode = roomsTreeList.Nodes.FirstNode;
			}
        }

        private void roomsTreeList_LayoutUpgrade(object sender, DevExpress.Utils.LayoutUpgradeEventArgs e)
        {
            if (e.PreviousVersion == "1" || e.PreviousVersion == "2" || e.PreviousVersion == "3")
            {
                if (e.PreviousVersion == "1" || e.PreviousVersion == "2")
                {
                    // Fix columns while restoring saved layout
                    advancedInfoColumn.Caption = " ";
                    advancedInfoColumn.FieldName = "MediaType";
                    advancedInfoColumn.OptionsColumn.AllowSort = true;
                }
                // rename "Media"
                mediaTypeColumn.Caption = Strings.RoomSelectionDialogMediaTypeColumnCaption;
            }
        }

        private void roomsTreeList_ShowTreeListMenu(object sender, DevExpress.XtraTreeList.TreeListMenuEventArgs e)
        {
            var hitInfo = roomsTreeList.CalcHitInfo(e.Point);
            _clickedColumn = null;
            if (hitInfo.HitInfoType == HitInfoType.Column)
            {
                _clickedColumn = hitInfo.Column;
                if (_clickedColumn.OptionsColumn.AllowMoveToCustomizationForm)
                {
                    e.Menu.Items.Add(new DXMenuItem(Strings.RoomSelectionDialogRemoveColumnMenuItemCaption, RemoveColumnMenuItemClick));
                }
            }
        }

        private void RemoveColumnMenuItemClick(object sender, EventArgs e)
        {
            if (_clickedColumn != null)
            {
                _clickedColumn.VisibleIndex = -1;
            }
        }

		public void AdditionalWorkForFavoriteRoomSelectionDialog()
		{
			layoutControlItem6.Visibility = LayoutVisibility.Never;
			//layoutControlItem7.Visibility = LayoutVisibility.Never;
		    layoutRoomsCalendarItem.Visibility = LayoutVisibility.Never;
			_selectedRooms.ListChanged -= OnListChanged;
			FormClosing -= RoomSelectionDialog_FormClosing;
			Load -= RoomSelectionDialog_Load;
		}

		private void calendarButton_Click(object sender, EventArgs e)
		{
			if( _selectedRooms.Count > 0 )
			{
				List<RoomId> roomList = new List<RoomId>();
				roomList.AddRange(_selectedRooms.Select(room => room.Id));
				//foreach (var room in _selectedRooms)
				//{
				//    roomList.Add(new RoomId(room.Id.Id)); 
				//}

				using(var dlg = new CalendarForSelectedRooms(roomList))
				{
					dlg.ShowDialog(this);
				}
			}
		}

		private void checkEditHotdesking_CheckedChanged(object sender, EventArgs e)
		{
			showOnlyFavoritesCheckEdit_CheckedChanged(sender, e);
		}

        private void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSelectAll.Checked)
            {
                System.Collections.ArrayList arrSelections = new System.Collections.ArrayList();
                System.Collections.IEnumerator en = roomsTreeList.Nodes.GetEnumerator();
                while (en.MoveNext())
                {
                    TreeListNode node = en.Current as TreeListNode;
                    AddRoomToSelectionList(_rooms[node.Id]);
                }
                
                    
                    
            }
            else
            {
                _selectedRooms.Clear();
            }
           
        }
    }
}
