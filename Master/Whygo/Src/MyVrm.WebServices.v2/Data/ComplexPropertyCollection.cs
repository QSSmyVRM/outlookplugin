﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections;
using System.Collections.Generic;

namespace MyVrm.WebServices.Data
{
    public abstract class ComplexPropertyCollection<TComplexProperty> : ComplexProperty, IEnumerable<TComplexProperty>  where TComplexProperty : ComplexProperty
    {
        private readonly List<TComplexProperty> _items;
        private readonly List<TComplexProperty> _addedItems;
        private readonly List<TComplexProperty> _modifiedItems;
        private readonly List<TComplexProperty> _removedItems;
        

        internal ComplexPropertyCollection()
        {
            _items = new List<TComplexProperty>();
            _addedItems = new List<TComplexProperty>();
            _modifiedItems = new List<TComplexProperty>();
            _removedItems = new List<TComplexProperty>();
        }

        public TComplexProperty this[int index]
        {
            get
            {
                if (index < 0 || index > _items.Count)
                {
                    throw new ArgumentOutOfRangeException("index");
                }
                return _items[index];
            }
            internal set
            {
                _items[index] = value;
            }
        }

        public int Count
        {
            get
            {
                return _items.Count;
            }
        }
 

        internal List<TComplexProperty> Items
        {
            get { return _items; }
        }

        internal List<TComplexProperty> AddedItems
        {
            get { return _addedItems; }
        }

        internal List<TComplexProperty> ModifiedItems
        {
            get { return _modifiedItems; }
        }

        internal List<TComplexProperty> RemovedItems
        {
            get { return _removedItems; }
        }

        #region Implementation of IEnumerable

        public IEnumerator<TComplexProperty> GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        public bool Contains(TComplexProperty complexProperty)
        {
            return _items.Contains(complexProperty);
        }

        public int IndexOf(TComplexProperty complexProperty)
        {
            return _items.IndexOf(complexProperty);
        }

        internal void InternalAdd(TComplexProperty complexProperty)
        {
            InternalAdd(complexProperty, false);
        }

        internal void InternalAddRange(IEnumerable<TComplexProperty> complexProperties)
        {
            foreach (var complexProperty in complexProperties)
            {
                InternalAdd(complexProperty, false);    
            }
        }

        internal void InternalClear()
        {
            while (Count > 0)
            {
                InternalRemoveAt(0);
            }
        }

        internal void InternalRemoveAt(int index)
        {
            InternalRemove(_items[index]);
        }

		internal void InternalInsert(int index, TComplexProperty complexProperty)
		{
			if (index >= 0 && index <= _items.Count && !_items.Contains(complexProperty))
			{
				_items.Insert(index, complexProperty);
				
				complexProperty.OnChange -= ItemChanged;
				if (!RemovedItems.Contains(complexProperty))
					RemovedItems.Remove(complexProperty);
				if (!AddedItems.Contains(complexProperty))
					AddedItems.Add(complexProperty);
				complexProperty.OnChange += ItemChanged;
				Changed();
			}
		}

        internal bool InternalRemove(TComplexProperty complexProperty)
        {
            if (!_items.Remove(complexProperty))
            {
                return false;
            }
            complexProperty.OnChange -= ItemChanged;
            if (!AddedItems.Contains(complexProperty))
            {
                RemovedItems.Add(complexProperty);
            }
            else
            {
                AddedItems.Remove(complexProperty);
            }
            ModifiedItems.Remove(complexProperty);
            Changed();
            return true;
        }


        internal void ItemChanged(ComplexProperty complexProperty)
        {
            var item = complexProperty as TComplexProperty;
            if (!AddedItems.Contains(item) && !ModifiedItems.Contains(item))
            {
                ModifiedItems.Add(item);
                Changed();
            }
        }

        internal abstract TComplexProperty CreateComplexProperty(string xmlElementName);

        internal abstract string GetCollectionItemXmlElementName(TComplexProperty complexProperty);

        internal override void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
        {
            reader.EnsureCurrentNodeIsStartElement(XmlNamespace.NotSpecified, xmlElementName);
            if (!reader.IsEmptyElement)
            {
                do
                {
                    reader.Read();
                    if (reader.IsStartElement())
                    {
                        var complexProperty = CreateComplexProperty(reader.LocalName);
                        if (complexProperty != null)
                        {
                            complexProperty.LoadFromXml(reader, reader.LocalName);
                            InternalAdd(complexProperty, true);
                        }
                        else
                        {
                            reader.SkipCurrentElement();
                        }
                    }
                }
                while (!reader.IsEndElement(XmlNamespace.NotSpecified, xmlElementName));
            }
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            foreach (var complexProperty in this)
            {
                complexProperty.WriteToXml(writer, GetCollectionItemXmlElementName(complexProperty));
            }
        }

        private void InternalAdd(TComplexProperty complexProperty, bool loading)
        {
            if (!_items.Contains(complexProperty))
            {
                _items.Add(complexProperty);
                if (!loading)
                {
                    RemovedItems.Remove(complexProperty);
                    AddedItems.Add(complexProperty);
                }
                complexProperty.OnChange += ItemChanged;
                Changed();
            }
        }
    }
}
