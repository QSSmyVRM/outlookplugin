﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Office.Interop.Outlook;
using Microsoft.Office.Interop.Outlook.Extensions.Linq;
using Microsoft.Office.Tools.Ribbon;
using MyVrm.Outlook;
using MyVrm.Outlook.WinForms;
using MyVrm.Outlook.WinForms.ApprovePendings;
using MyVrm.Outlook.WinForms.Calendar;
using MyVrm.Outlook.WinForms.Conference;
using MyVrm.Outlook.WinForms.Options;
using MyVrm.Outlook.WinForms.Templates;
using MyVrm.WebServices.Data;
using stdole;
using Exception = System.Exception;
using Microsoft.Office.Tools.Outlook;
using Image = System.Drawing.Image;

namespace MyVrmAddin2007
{
    public partial class ConferenceRibbon : RibbonBase
    {
    	private string selectedTemplateID;
        public ConferenceRibbon():base(Globals.Factory.GetRibbonFactory())
        {
            System.Threading.Thread.CurrentThread.CurrentUICulture = System.Threading.Thread.CurrentThread.CurrentCulture;
            InitializeComponent();
            
        	selectedTemplateID = string.Empty;

			//Texts localization
        	generalGroup.Label = Strings.General;
			roomsCalendarButton.Label = Strings.RoomsCalendar;
            roomsCalendarButton.SuperTip = Strings.RoomsCalendarButtonTip;
        	templatesGroup.Label = Strings.Templates;
        	templateMenu.Label = Strings.CreateFromTemplate;
            templateMenu.SuperTip = Strings.CreateFromTemplateButtonTip;
        	saveAsTemplateBn.Label = Strings.SaveAsTemplate;
            saveAsTemplateBn.SuperTip = Strings.SaveAsTeamplateButtonTip;
        	preferencesGroup.Label = Strings.Preferences;
        	favoriteRoomsButton.Label = Strings.FavoriteRooms;
            favoriteRoomsButton.SuperTip = Strings.FavoritRoomsButtonTip;
        	groupShow.Label = Strings.Show;
        	appointmentButton.Label = Strings.Appointment;
            appointmentButton.SuperTip = Strings.AppointmentButtonTip;
			conferenceButton.Label = Strings.Conference;
            conferenceButton.SuperTip = Strings.ConferenceButtonTip;
        	optionsGroup.Label = Strings.OptionsMenuPoint;
			optionsButton.Label = Strings.OptionsMenuPoint;
            optionsButton.SuperTip = Strings.OptionsButtonTip;
            //ZD 104780 starts
            appointmentButton.Enabled = false;
            conferenceButton.Enabled = false;
            templateMenu.Enabled = false;
            optionsButton.Enabled = false;
            roomsCalendarButton.Enabled = false;
            favoriteRoomsButton.Enabled = false;
            //ZD 104780 Ends
            saveAsTemplateBn.Click+=saveAsTemplateBn_Click;
        }

        private void ConferenceRibbon_Load(object sender, RibbonUIEventArgs e)
        {
			tabMyVRM.Label = MyVrmAddin.ProductDisplayName;
            tabMyVRM.Visible = MyVrmAddin.Instance.IsLicenseAgreementAccepted;
			conferenceButton.Image = (Image)MyVrmAddin.Logo65;
        	DisEnable();
        }

		public void DisEnable_AfterSave(object sender, EventArgs e)
		{
			DisEnable();

			var inspector = (Inspector)Context;
			var conferenceWrapper = Globals.ThisAddIn.GetConferenceWrapperForAppointment(
					(AppointmentItem)inspector.CurrentItem);
			if (conferenceWrapper != null)
				conferenceWrapper.AfterSave -= DisEnable_AfterSave;
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
		}

		private void DisEnable()
		{
			var inspector = (Inspector) Context;
			templateMenu.Enabled = !Globals.ThisAddIn.HasConferenceWrapper((AppointmentItem) inspector.CurrentItem);
			//saveAsTemplateBn.Enabled = !templateMenu.Enabled;
		}

    	private void roomsCalendarButton_Click(object sender, RibbonControlEventArgs e)
    	{
    		roomsCalendarButton_OnClick();
    	}

		public static void roomsCalendarButton_OnClick()
		{
			using (var calendarDialog = new RoomsCalendarDialog())
			{
				calendarDialog.ShowDialog();
			}
		}

    	public static void approvalConferencesButton_OnClick()
		{
			List<ConfInfo> FoundConfs = new List<ConfInfo>();
			ConferenceApprovalDialog.GetConfsForApprove(out FoundConfs);

			if (FoundConfs.Count > 0)
			{
				//Run approval dlg
				using (var dlg = new ConferenceApprovalDialog())
				{
					dlg.FoundConfs = FoundConfs;

					dlg.ShowDialog();
				}
			}
			else
			{
				UIHelper.ShowMessage(Strings.NoConferencesToApproveMsg, MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}
		private void saveAsTemplateBn_Click(object sender, RibbonControlEventArgs e)
		{
			var inspector = (Inspector) Context;
			//OutlookAppointmentImpl apptImpl;
			var conferenceWrapper = Globals.ThisAddIn.GetConferenceWrapperForAppointment((AppointmentItem)inspector.CurrentItem);
			using (var editTemplateDialog = new SaveAsConferenceTemplateDialog(conferenceWrapper))
			{
				editTemplateDialog.ShowDialog();
			}
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
		}
		public void templateMenu_OnItemsLoading(RibbonMenu menu, RibbonControlEventHandler handler, string selected)
		{
			GetTemplateListResponse ret = null;

			var cursor = Cursor.Current;
			Cursor.Current = Cursors.WaitCursor;

			try
			{
				ret = MyVrmService.Service.GetTemplateList(GetTemplateListResponse.SortByType.ByName);
			}
			catch (Exception exception)
			{
				MyVrmAddin.TraceSource.TraceException(exception, true);
				UIHelper.ShowMessage(exception.Message, MessageBoxButtons.OK,
										 MessageBoxIcon.Exclamation);
			}
			finally
			{
				Cursor.Current = cursor;
			}

			if (ret != null && ret.templateList != null)
			{
				menu.Items.Clear();

				foreach (TemplateInfo templateInfo in ret.templateList)
				{
                    RibbonToggleButton rt = this.Factory.CreateRibbonToggleButton();
					rt.Label = templateInfo.Name;
					rt.Visible = true;
					rt.Tag = templateInfo.ID;
					rt.Enabled = true;
					rt.Checked = selected == templateInfo.ID;
					rt.Click += handler;

					menu.Items.Add(rt);
				}
			}
		}
		private void templateMenu_ItemsLoading(object sender, RibbonControlEventArgs e)
		{
			templateMenu_OnItemsLoading(templateMenu, checkBox_Click, selectedTemplateID);
		}

		static public GetTemplateResponse checkBox_OnClick(object sender, string selected)
		{
			RibbonToggleButton rb = (RibbonToggleButton)sender;
			selected = string.Empty;
			if (rb.Checked)
				selected = rb.Tag.ToString();

			if (selected.Trim().Length == 0 || int.Parse(selected) <= 0)
				return null;

			GetTemplateResponse response = null;
			var cursor = Cursor.Current;
			Cursor.Current = Cursors.WaitCursor;

			try
			{
				response = MyVrmService.Service.GetTemplate(int.Parse(selected));
			}
			catch (Exception exception)
			{
				MyVrmAddin.TraceSource.TraceException(exception, true);
				UIHelper.ShowMessage(exception.Message, MessageBoxButtons.OK,
										 MessageBoxIcon.Exclamation);
			}
			finally
			{
				Cursor.Current = cursor;
			}
			return response;
		}
		private void checkBox_Click(object sender, RibbonControlEventArgs e)
		{
			GetTemplateResponse response = checkBox_OnClick(sender, selectedTemplateID);
			if (response != null)
			{
				var inspector = (Inspector) Context;
				//OutlookAppointmentImpl apptImpl;
				var conferenceWrapper = Globals.ThisAddIn.GetConferenceWrapperForAppointment(
					(AppointmentItem) inspector.CurrentItem);

				if (conferenceWrapper != null)
				{
					conferenceWrapper.AfterSave -= DisEnable_AfterSave;
					conferenceWrapper.FillFromTemplate(response.Conference);
					
					conferenceWrapper.AfterSave += DisEnable_AfterSave;
				}
			}
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
		}

		static public void favoriteRoomsButton_OnClick()
		{
			//string selectedRooms = string.Empty;
			var cursor = Cursor.Current;
			Cursor.Current = Cursors.WaitCursor;

			try
			{
				//Read current favorites
				GetPreferedRoomResponse response = MyVrmService.Service.GetPreferedRoom();

				//Run selection dlg
				using (var dlg = new FavoriteRoomSelectionDialog())
				{
                    RoomId[] roomid;
                    List<ConfRoom> confSelectedRoom = new List<ConfRoom>();
					if (response != null)
					//	dlg.SelectedRooms = response.RoomIds.ToArray();

                        for (int i = 0; i < response.RoomIds.Count; i++)
                        {
                        //    if (response.RoomIds[i] != new RoomId("0"))
                        //    dlg.SelectedRooms[i] = new ConfRoom(response.RoomIds[i]);
                            confSelectedRoom.Add(new ConfRoom(response.RoomIds[i]));
                        }
                    dlg.SelectedRooms = confSelectedRoom.ToArray();
                    roomid = response.RoomIds.ToArray();
					if (dlg.ShowDialog() == DialogResult.OK)
					{
						//Store setup favorites //104678 starts
                        RoomId[] r1ID;
                        List<RoomId> confSelectedRoom1 = new List<RoomId>();
                        foreach (ConfRoom Confr in dlg.SelectedRooms)
                        {
                            confSelectedRoom1.Add(Confr.Id);
                        }
                        r1ID = confSelectedRoom1.ToArray();
                        MyVrmService.Service.SetPreferedRoom(r1ID);
                        //104678 Ends
					}
				}
			}
			catch (Exception exception)
			{
				MyVrmAddin.TraceSource.TraceException(exception, true);
				UIHelper.ShowMessage(exception.Message, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}
			finally
			{
				Cursor.Current = cursor;
			}
		}

		private void favoriteRoomsButton_Click(object sender, RibbonControlEventArgs e)
		{
			favoriteRoomsButton_OnClick();
		}

		private void conferenceButton_Click(object sender, RibbonControlEventArgs e)
		{
			conferenceButton_OnClick((Inspector)Context);
		}

    	static public void conferenceButton_OnClick(Inspector externalContext)
		{
			try
			{
				var inspector = externalContext;
				if (inspector != null)
				{
					inspector.SetCurrentFormPage("MyVrmAddin2007.ConferenceFormRegion");
				}
			}
			catch (Exception exception)
			{
				MyVrmAddin.TraceSource.TraceException(exception);
				UIHelper.ShowError(exception.Message);
			}
		}

		private void appointmentButton_Click(object sender, RibbonControlEventArgs e)
		{
			try
			{
				var inspector = (Inspector)Context;
				if(inspector != null)
					inspector.SetCurrentFormPage("Appointment");
			}
			catch (Exception exception)
			{
				MyVrmAddin.TraceSource.TraceException(exception);
				UIHelper.ShowError(exception.Message);
			}
		}

		private void approveConferenceButton_Click(object sender, RibbonControlEventArgs e)
		{
			approvalConferencesButton_OnClick();
		}

		private void optionsButton_Click(object sender, RibbonControlEventArgs ev)
		{
			using (var optDlg = new OptionsAsDlg())
			{
				CancelEventArgs e = new CancelEventArgs();
				if (optDlg.ShowDialog() == DialogResult.OK)
					optDlg.optionsControl.External_OnApplying(e);
			}
		}
    }
}
