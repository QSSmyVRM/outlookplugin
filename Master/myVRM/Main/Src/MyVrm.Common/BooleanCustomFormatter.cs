﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace MyVrm.Common
{
    /// <summary>
    /// Custom formatter for <see cref="bool"/>.
    /// </summary>
    public class BooleanCustomFormatter : IFormatProvider, ICustomFormatter
    {
        public object GetFormat(Type formatType)
        {
            return formatType == typeof(ICustomFormatter) ? this : null;
        }

        public string Format(string format, object arg, IFormatProvider formatProvider)
        {
            if (format == null) throw new ArgumentNullException("format");
            if (arg == null) throw new ArgumentNullException("arg");

            if (format.ToLower() == "yn")
            {
                return ((bool)arg) ? Strings.BooleanYesText : Strings.BooleanNoText;
            }
            return arg.ToString();
        }
    }
}
