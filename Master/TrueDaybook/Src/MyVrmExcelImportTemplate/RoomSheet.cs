﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using Microsoft.VisualStudio.Tools.Applications.Runtime;
using MyVrm.DataImport.Excel;
using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;

namespace MyVrmExcelImportTemplate
{
    public partial class RoomSheet
    {
        private RoomsTable _roomsTable;

        private void RoomSheet_Startup(object sender, System.EventArgs e)
        {
            _roomsTable = new RoomsTable(roomTable, Globals.ThisWorkbook.Service,
                                         Globals.ThisWorkbook.ValidationListManager);
            Globals.ThisWorkbook.RegisterTable(roomTable, _roomsTable);
        }

        private void RoomSheet_Shutdown(object sender, System.EventArgs e)
        {
        }

        #region VSTO Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
	    private void InternalStartup()
        {
            this.Startup += new System.EventHandler(RoomSheet_Startup);
            this.Shutdown += new System.EventHandler(RoomSheet_Shutdown);
        }
        
        #endregion

    }
}
