﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.IO;
using System.Xml;

namespace MyVrm.WebServices.Data
{
    internal class MyVrmXmlReader
    {
        private readonly XmlReader _xmlReader;
        private XmlNodeType _prevNodeType;

        public MyVrmXmlReader(Stream stream)
        {
            var settings = new XmlReaderSettings
                               {
                                   ConformanceLevel = ConformanceLevel.Auto,
                                   IgnoreComments = true,
                                   IgnoreWhitespace = true
                               };
            var reader = new XmlTextReader(stream) {Normalization = false};
            _xmlReader = XmlReader.Create(reader, settings);
        }


        public bool HasAttributes
        {
            get
            {
                return _xmlReader.HasAttributes;
            }
        }

        public XmlNodeType PrevNodeType
        {
            get { return _prevNodeType; }
        }

        public string LocalName
        {
            get { return _xmlReader.LocalName; }
        }

        public string NamespacePrefix
        {
            get { return _xmlReader.Prefix; }
        }

        public string NamespaceUri
        {
            get { return _xmlReader.NamespaceURI; }
        }

        public XmlNodeType NodeType
        {
            get { return _xmlReader.NodeType; }
        }

        public bool IsEmptyElement
        {
            get { return _xmlReader.IsEmptyElement; }
        }

        public void EnsureCurrentNodeIsEndElement(XmlNamespace xmlNamespace, string localName)
        {
            if (!IsEndElement(xmlNamespace, localName) && (!IsStartElement(xmlNamespace, localName) || !IsEmptyElement))
            {
                throw new ServiceXmlDeserializationException(string.Format("The element '{0}' in namespace '{1}' was not found at the current position.", xmlNamespace, localName));
            }
        }

        public void EnsureCurrentNodeIsStartElement()
        {
            if (NodeType != XmlNodeType.Element)
            {
                throw new ServiceXmlDeserializationException(string.Format("Expected start element, but found node '{0}' of type {1}.", _xmlReader.Name, NodeType));
            }
        }

        public void EnsureCurrentNodeIsStartElement(XmlNamespace xmlNamespace, string localName)
        {
            if (!IsStartElement(xmlNamespace, localName))
            {
                throw new ServiceXmlDeserializationException(string.Format("The element '{0}' in namespace '{1}' was not found at the current position.", localName, xmlNamespace));
            }
        }

        public string ReadElementValue()
        {
            EnsureCurrentNodeIsStartElement();
            return ReadElementValue(NamespacePrefix, LocalName);
        }

        public string ReadElementValue(string namespacePrefix, string localName)
        {
            if (!IsStartElement(namespacePrefix, localName))
            {
                ReadStartElement(namespacePrefix, localName);
            }
            string value = null;
            if (!IsEmptyElement)
            {
                value = ReadValue();
            }
            return value;
        }


        public string ReadElementValue(XmlNamespace xmlNamespace, string localName)
        {
            if (!IsStartElement(xmlNamespace, localName))
            {
                ReadStartElement(xmlNamespace, localName);
            }
            string value = null;
            if (!IsEmptyElement)
            {
                value = ReadValue();
            }
            return value;
        }

        public T ReadElementValue<T>()
        {
            EnsureCurrentNodeIsStartElement();
            var value = default(T);
            if (!IsEmptyElement)
            {
                value = ReadValue<T>();
            }
            return value;
        }

        public T ReadElementValue<T>(XmlNamespace xmlNamespace, string localName)
        {
            if (!IsStartElement(xmlNamespace, localName))
            {
                ReadStartElement(xmlNamespace, localName);
            }
            var value = default(T);
            if (!IsEmptyElement)
            {
                value = ReadValue<T>();
            }
            return value;
        }

        public string ReadInnerXml()
        {
            if (!IsStartElement())
            {
                throw new ServiceXmlDeserializationException("The current position is not the start of an element.");
            }
            return _xmlReader.ReadInnerXml().Trim();
        }

        public string ReadOuterXml()
        {
            if (!IsStartElement())
            {
                throw new ServiceXmlDeserializationException("The current position is not the start of an element.");
            }
			return _xmlReader.ReadOuterXml().Trim();
        }

        public XmlReader ReadSubtree()
        {
            if (!IsStartElement())
            {
                throw new ServiceXmlDeserializationException("The current position is not the start of an element.");
            }
            return _xmlReader.ReadSubtree();
        }

        public T ReadValue<T>()
        {
            return Utilities.Parse<T>(ReadValue());
        }

        public string ReadValue()
        {
			return _xmlReader.ReadString().Trim();
        }

        public bool IsEndElement(string namespacePrefix, string localName)
        {
            string str = FormatElementName(namespacePrefix, localName);
            return ((NodeType == XmlNodeType.EndElement) && (_xmlReader.Name == str));
        }

        public bool IsEndElement(XmlNamespace xmlNamespace, string localName)
        {
            if (LocalName != localName || NodeType != XmlNodeType.EndElement)
            {
                return false;
            }
            if (NamespacePrefix != Utilities.GetNamespacePrefix(xmlNamespace))
            {
                return NamespaceUri == Utilities.GetNamespaceUri(xmlNamespace);
            }
            return true;
        }

        public bool IsStartElement(XmlNamespace xmlNamespace, string localName)
        {
            if (LocalName != localName || !IsStartElement())
            {
                return false;
            }
            if (NamespacePrefix != Utilities.GetNamespacePrefix(xmlNamespace))
            {
                return NamespaceUri == Utilities.GetNamespaceUri(xmlNamespace);
            }
            return true;
        }

        public bool IsStartElement(string namespacePrefix, string localName)
        {
            string str = FormatElementName(namespacePrefix, localName);
            return ((NodeType == XmlNodeType.Element) && (_xmlReader.Name == str));
        }

        public bool IsStartElement()
        {
            return _xmlReader.NodeType == XmlNodeType.Element;
        }

        public void Read()
        {
            _prevNodeType = _xmlReader.NodeType;
            if (!_xmlReader.Read())
            {
                throw new ServiceXmlDeserializationException(Strings.UnexpectedEndOfXmlDocument);
            }
        }

        public void Read(XmlNodeType nodeType)
        {
            Read();
            if (NodeType != nodeType)
            {
                throw new ServiceXmlDeserializationException(string.Format(Strings.UnexpectedElementType,
                                                                           new object[]
                                                                                   {
                                                                                       nodeType, NodeType
                                                                                   }));
            }
        }

        public byte[] ReadBase64ElementValue()
        {
            EnsureCurrentNodeIsStartElement();
            string value = ReadElementValue();

            return Convert.FromBase64String(value);
        }

        public void ReadBase64ElementValue(Stream outputStream)
        {
            int num;
            EnsureCurrentNodeIsStartElement();
            var buffer = new byte[4096];
            do
            {
                num = _xmlReader.ReadElementContentAsBase64(buffer, 0, 4096);
                if (num > 0)
                {
                    outputStream.Write(buffer, 0, num);
                }
            }
            while (num > 0);
            outputStream.Flush();
        }

        public void ReadEndElement(XmlNamespace xmlNamespace, string localName)
        {
            InternalReadElement(xmlNamespace, localName, XmlNodeType.EndElement);
        }

        public void ReadEndElementIfNecessary(XmlNamespace xmlNamespace, string localName)
        {
            if ((!IsStartElement(xmlNamespace, localName) || !IsEmptyElement) && !IsEndElement(xmlNamespace, localName))
            {
                ReadEndElement(xmlNamespace, localName);
            }
        }


        public void ReadStartElement(XmlNamespace xmlNamespace, string localName)
        {
            InternalReadStartElement(xmlNamespace, localName, XmlNodeType.Element);
        }

        public void ReadStartElement(string namespacePrefix, string localName)
        {
            InternalReadElement(namespacePrefix, localName, XmlNodeType.Element);
        }

        public void SkipCurrentElement()
        {
            SkipElement(NamespacePrefix, LocalName);
        }

        public void SkipElement(string namespacePrefix, string localName)
        {
            if (!IsEndElement(namespacePrefix, localName))
            {
                if (!IsStartElement(namespacePrefix, localName))
                {
                    ReadStartElement(namespacePrefix, localName);
                }
                if (!IsEmptyElement)
                {
                    do
                    {
                        Read();
                    }
                    while (!IsEndElement(namespacePrefix, localName));
                }
            }
        }

        private static string FormatElementName(string namespacePrefix, string localElementName)
        {
            if (!string.IsNullOrEmpty(namespacePrefix))
            {
                return (namespacePrefix + ":" + localElementName);
            }
            return localElementName;
        }

        private void InternalReadStartElement(XmlNamespace xmlNamespace, string localName, XmlNodeType nodeType)
        {
            if (xmlNamespace == XmlNamespace.NotSpecified)
            {
                InternalReadElement(string.Empty, localName, nodeType);
            }
            else
            {
                Read(nodeType);
                if (LocalName != localName || NamespaceUri != Utilities.GetNamespaceUri(xmlNamespace))
                {
                    throw new ServiceXmlDeserializationException(string.Format(Strings.UnexpectedElement,
                                                                               new object[]
                                                                                   {
                                                                                       Utilities.GetNamespacePrefix(
                                                                                           xmlNamespace), localName,
                                                                                       nodeType, _xmlReader.Name,
                                                                                       NodeType
                                                                                   }));
                }
            }
        }


        private void InternalReadElement(string namespacePrefix, string localName, XmlNodeType nodeType)
        {
            Read(nodeType);
            if (LocalName != localName || NamespacePrefix != namespacePrefix)
            {
                throw new ServiceXmlDeserializationException(string.Format(Strings.UnexpectedElement,
                                                                           new object[]
                                                                               {
                                                                                   namespacePrefix, localName, nodeType,
                                                                                   _xmlReader.Name, NodeType
                                                                               }));
            }
        }

        private void InternalReadElement(XmlNamespace xmlNamespace, string localName, XmlNodeType nodeType)
        {
            if (xmlNamespace == XmlNamespace.NotSpecified)
            {
                InternalReadElement(string.Empty, localName, nodeType);
            }
            else
            {
                Read(nodeType);
                if (LocalName != localName || NamespaceUri != Utilities.GetNamespaceUri(xmlNamespace))
                {
                    throw new ServiceXmlDeserializationException(string.Format(Strings.UnexpectedElement,
                                                                               new object[]
                                                                                   {
                                                                                       Utilities.GetNamespacePrefix(
                                                                                           xmlNamespace), localName,
                                                                                       nodeType, _xmlReader.Name,
                                                                                       NodeType
                                                                                   }));
                }
            }
        }
    }
}
