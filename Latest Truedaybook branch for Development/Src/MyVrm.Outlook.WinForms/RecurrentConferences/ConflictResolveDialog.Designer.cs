﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.ObjectModel;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.XtraScheduler;
using DevExpress.XtraScheduler.Drawing;
using DevExpress.XtraTreeList.Nodes;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook.WinForms.RecurrentConferences
{
	partial class ConflictResolveDialog
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			DevExpress.XtraScheduler.TimeRuler timeRuler1 = new DevExpress.XtraScheduler.TimeRuler();
			DevExpress.XtraScheduler.TimeScaleYear timeScaleYear1 = new DevExpress.XtraScheduler.TimeScaleYear();
			DevExpress.XtraScheduler.TimeScaleQuarter timeScaleQuarter1 = new DevExpress.XtraScheduler.TimeScaleQuarter();
			DevExpress.XtraScheduler.TimeScaleMonth timeScaleMonth1 = new DevExpress.XtraScheduler.TimeScaleMonth();
			DevExpress.XtraScheduler.TimeScaleWeek timeScaleWeek1 = new DevExpress.XtraScheduler.TimeScaleWeek();
			DevExpress.XtraScheduler.TimeScaleDay timeScaleDay1 = new DevExpress.XtraScheduler.TimeScaleDay();
			DevExpress.XtraScheduler.TimeScaleHour timeScaleHour1 = new DevExpress.XtraScheduler.TimeScaleHour();
			DevExpress.XtraScheduler.TimeScaleFixedInterval timeScaleFixedInterval1 = new DevExpress.XtraScheduler.TimeScaleFixedInterval();
			DevExpress.XtraScheduler.TimeRuler timeRuler2 = new DevExpress.XtraScheduler.TimeRuler();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConflictResolveDialog));
			DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
			this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
			this.schedulerControl1 = new DevExpress.XtraScheduler.SchedulerControl();
			this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
			this.viewNavigatorBar1 = new DevExpress.XtraScheduler.UI.ViewNavigatorBar();
			this.viewNavigatorBackwardItem1 = new DevExpress.XtraScheduler.UI.ViewNavigatorBackwardItem();
			this.viewNavigatorForwardItem1 = new DevExpress.XtraScheduler.UI.ViewNavigatorForwardItem();
			this.viewNavigatorTodayItem1 = new DevExpress.XtraScheduler.UI.ViewNavigatorTodayItem();
			this.viewNavigatorZoomInItem1 = new DevExpress.XtraScheduler.UI.ViewNavigatorZoomInItem();
			this.viewNavigatorZoomOutItem1 = new DevExpress.XtraScheduler.UI.ViewNavigatorZoomOutItem();
			this.viewSelectorBar1 = new DevExpress.XtraScheduler.UI.ViewSelectorBar();
			this.viewSelectorItem1 = new DevExpress.XtraScheduler.UI.ViewSelectorItem();
			this.viewSelectorItem2 = new DevExpress.XtraScheduler.UI.ViewSelectorItem();
			this.viewSelectorItem3 = new DevExpress.XtraScheduler.UI.ViewSelectorItem();
			this.viewSelectorItem4 = new DevExpress.XtraScheduler.UI.ViewSelectorItem();
			this.viewSelectorItem5 = new DevExpress.XtraScheduler.UI.ViewSelectorItem();
			this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
			this.repositoryItemWeekOfMonth1 = new DevExpress.XtraScheduler.UI.RepositoryItemWeekOfMonth();
			this.repositoryItemResourcesComboBox1 = new DevExpress.XtraScheduler.UI.RepositoryItemResourcesComboBox();
			this.schedulerStorage1 = new DevExpress.XtraScheduler.SchedulerStorage(this.components);
			this.treeList1 = new DevExpress.XtraTreeList.TreeList();
			this.treeListColumn1 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.repositoryItemTimeEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
			this.treeListColumn2 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.repositoryItemTimeEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
			this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
			this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
			this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
			this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
			this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
			//this.viewNavigator1 = new DevExpress.XtraScheduler.UI.ViewNavigator(this.components);
			//this.viewSelector1 = new DevExpress.XtraScheduler.UI.ViewSelector(this.components);
			((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).BeginInit();
			this.ContentPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
			this.layoutControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.schedulerControl1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.repositoryItemWeekOfMonth1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.repositoryItemResourcesComboBox1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.schedulerStorage1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.treeList1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
			//((System.ComponentModel.ISupportInitialize)(this.viewNavigator1)).BeginInit();
			//((System.ComponentModel.ISupportInitialize)(this.viewSelector1)).BeginInit();
			this.SuspendLayout();
			// 
			// ContentPanel
			// 
			this.ContentPanel.Controls.Add(this.layoutControl1);
			this.ContentPanel.Size = new System.Drawing.Size(1089, 644);
			// 
			// layoutControl1
			// 
			this.layoutControl1.Controls.Add(this.schedulerControl1);
			this.layoutControl1.Controls.Add(this.treeList1);
			this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.layoutControl1.Location = new System.Drawing.Point(0, 0);
			this.layoutControl1.Name = "layoutControl1";
			this.layoutControl1.Root = this.layoutControlGroup1;
			this.layoutControl1.Size = new System.Drawing.Size(1089, 644);
			this.layoutControl1.TabIndex = 0;
			this.layoutControl1.Text = "Instances with conflicts:";
			// 
			// schedulerControl1
			// 
			this.schedulerControl1.ActiveViewType = DevExpress.XtraScheduler.SchedulerViewType.Timeline;
			this.schedulerControl1.GroupType = DevExpress.XtraScheduler.SchedulerGroupType.Resource;
			this.schedulerControl1.Location = new System.Drawing.Point(12, 266);
			this.schedulerControl1.MenuManager = this.barManager1;
			this.schedulerControl1.Name = "schedulerControl1";
			this.schedulerControl1.OptionsBehavior.RecurrentAppointmentEditAction = DevExpress.XtraScheduler.RecurrentAppointmentAction.Series;
			this.schedulerControl1.OptionsCustomization.AllowAppointmentDelete = DevExpress.XtraScheduler.UsedAppointmentType.Custom;
			this.schedulerControl1.OptionsCustomization.AllowAppointmentMultiSelect = false;
			this.schedulerControl1.OptionsCustomization.AllowInplaceEditor = DevExpress.XtraScheduler.UsedAppointmentType.None;
			this.schedulerControl1.OptionsView.ResourceHeaders.RotateCaption = false;
			this.schedulerControl1.OptionsView.ToolTipVisibility = DevExpress.XtraScheduler.ToolTipVisibility.Always;
			this.schedulerControl1.ResourceNavigator.Visibility = DevExpress.XtraScheduler.ResourceNavigatorVisibility.Always;
			this.schedulerControl1.Size = new System.Drawing.Size(1065, 366);
			this.schedulerControl1.Start = new System.DateTime(2011, 3, 7, 0, 0, 0, 0);
			this.schedulerControl1.Storage = this.schedulerStorage1;
			this.schedulerControl1.TabIndex = 5;
			this.schedulerControl1.Text = "schedulerControl1";
			this.schedulerControl1.Views.DayView.AppointmentDisplayOptions.StatusDisplayType = DevExpress.XtraScheduler.AppointmentStatusDisplayType.Bounds;
			this.schedulerControl1.Views.DayView.TimeRulers.Add(timeRuler1);
			this.schedulerControl1.Views.TimelineView.AppointmentDisplayOptions.StatusDisplayType = DevExpress.XtraScheduler.AppointmentStatusDisplayType.Bounds;
			this.schedulerControl1.Views.TimelineView.NavigationButtonVisibility = DevExpress.XtraScheduler.NavigationButtonVisibility.Always;
			timeScaleYear1.Enabled = false;
			timeScaleQuarter1.Enabled = false;
			timeScaleMonth1.Enabled = false;
			timeScaleFixedInterval1.Enabled = false;
			this.schedulerControl1.Views.TimelineView.Scales.Add(timeScaleYear1);
			this.schedulerControl1.Views.TimelineView.Scales.Add(timeScaleQuarter1);
			this.schedulerControl1.Views.TimelineView.Scales.Add(timeScaleMonth1);
			this.schedulerControl1.Views.TimelineView.Scales.Add(timeScaleWeek1);
			this.schedulerControl1.Views.TimelineView.Scales.Add(timeScaleDay1);
			this.schedulerControl1.Views.TimelineView.Scales.Add(timeScaleHour1);
			this.schedulerControl1.Views.TimelineView.Scales.Add(timeScaleFixedInterval1);
			this.schedulerControl1.Views.WeekView.AppointmentDisplayOptions.StatusDisplayType = DevExpress.XtraScheduler.AppointmentStatusDisplayType.Bounds;
			this.schedulerControl1.Views.WorkWeekView.AppointmentDisplayOptions.StatusDisplayType = DevExpress.XtraScheduler.AppointmentStatusDisplayType.Bounds;
			this.schedulerControl1.Views.WorkWeekView.TimeRulers.Add(timeRuler2);
			this.schedulerControl1.EditRecurrentAppointmentFormShowing += new DevExpress.XtraScheduler.EditRecurrentAppointmentFormEventHandler(this.schedulerControl1_EditRecurrentAppointmentFormShowing);
			this.schedulerControl1.AllowAppointmentDelete += new DevExpress.XtraScheduler.AppointmentOperationEventHandler(this.schedulerControl1_AllowAppointmentDelete);
			this.schedulerControl1.AppointmentDrag += new DevExpress.XtraScheduler.AppointmentDragEventHandler(this.schedulerControl1_AppointmentDrag);
			this.schedulerControl1.PreparePopupMenu += new DevExpress.XtraScheduler.PreparePopupMenuEventHandler(this.schedulerControl1_PreparePopupMenu);
			this.schedulerControl1.EditAppointmentFormShowing += new DevExpress.XtraScheduler.AppointmentFormEventHandler(this.schedulerControl1_EditAppointmentFormShowing);
			this.schedulerControl1.AppointmentResized += new DevExpress.XtraScheduler.AppointmentResizeEventHandler(this.schedulerControl1_AppointmentResized);
			// 
			// barManager1
			// 
			this.barManager1.AllowCustomization = false;
			this.barManager1.AllowQuickCustomization = false;
			this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.viewNavigatorBar1,
            this.viewSelectorBar1});
			this.barManager1.DockControls.Add(this.barDockControlTop);
			this.barManager1.DockControls.Add(this.barDockControlBottom);
			this.barManager1.DockControls.Add(this.barDockControlLeft);
			this.barManager1.DockControls.Add(this.barDockControlRight);
			this.barManager1.Form = this;
			this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.viewNavigatorBackwardItem1,
            this.viewNavigatorForwardItem1,
            this.viewNavigatorTodayItem1,
            this.viewNavigatorZoomInItem1,
            this.viewNavigatorZoomOutItem1,
            this.viewSelectorItem1,
            this.viewSelectorItem2,
            this.viewSelectorItem3,
            this.viewSelectorItem4,
            this.viewSelectorItem5});
			this.barManager1.MaxItemId = 14;
			this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemWeekOfMonth1,
            this.repositoryItemResourcesComboBox1});
			// 
			// viewNavigatorBar1
			// 
			this.viewNavigatorBar1.CanDockStyle = ((DevExpress.XtraBars.BarCanDockStyle)((DevExpress.XtraBars.BarCanDockStyle.Floating | DevExpress.XtraBars.BarCanDockStyle.Bottom)));
			this.viewNavigatorBar1.DockCol = 0;
			this.viewNavigatorBar1.DockRow = 0;
			this.viewNavigatorBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
			this.viewNavigatorBar1.FloatLocation = new System.Drawing.Point(788, 197);
			this.viewNavigatorBar1.FloatSize = new System.Drawing.Size(478, 30);
			this.viewNavigatorBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.viewNavigatorBackwardItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewNavigatorForwardItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewNavigatorTodayItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewNavigatorZoomInItem1),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this.viewNavigatorZoomOutItem1, "", false, false, true, 0)});
			this.viewNavigatorBar1.OptionsBar.AllowQuickCustomization = false;
			this.viewNavigatorBar1.OptionsBar.DisableClose = true;
			this.viewNavigatorBar1.OptionsBar.DisableCustomization = true;
			this.viewNavigatorBar1.OptionsBar.DrawDragBorder = false;
			// 
			// viewNavigatorBackwardItem1
			// 
			this.viewNavigatorBackwardItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorBackwardItem1.Glyph")));
			this.viewNavigatorBackwardItem1.GroupIndex = 1;
			this.viewNavigatorBackwardItem1.Id = 2;
			this.viewNavigatorBackwardItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorBackwardItem1.LargeGlyph")));
			this.viewNavigatorBackwardItem1.Name = "viewNavigatorBackwardItem1";
			// 
			// viewNavigatorForwardItem1
			// 
			this.viewNavigatorForwardItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorForwardItem1.Glyph")));
			this.viewNavigatorForwardItem1.GroupIndex = 1;
			this.viewNavigatorForwardItem1.Id = 3;
			this.viewNavigatorForwardItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorForwardItem1.LargeGlyph")));
			this.viewNavigatorForwardItem1.Name = "viewNavigatorForwardItem1";
			// 
			// viewNavigatorTodayItem1
			// 
			this.viewNavigatorTodayItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorTodayItem1.Glyph")));
			this.viewNavigatorTodayItem1.GroupIndex = 1;
			this.viewNavigatorTodayItem1.Id = 4;
			this.viewNavigatorTodayItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorTodayItem1.LargeGlyph")));
			this.viewNavigatorTodayItem1.Name = "viewNavigatorTodayItem1";
			// 
			// viewNavigatorZoomInItem1
			// 
			this.viewNavigatorZoomInItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorZoomInItem1.Glyph")));
			this.viewNavigatorZoomInItem1.GroupIndex = 1;
			this.viewNavigatorZoomInItem1.Id = 5;
			this.viewNavigatorZoomInItem1.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Add));
			this.viewNavigatorZoomInItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorZoomInItem1.LargeGlyph")));
			this.viewNavigatorZoomInItem1.Name = "viewNavigatorZoomInItem1";
			// 
			// viewNavigatorZoomOutItem1
			// 
			this.viewNavigatorZoomOutItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorZoomOutItem1.Glyph")));
			this.viewNavigatorZoomOutItem1.GroupIndex = 1;
			this.viewNavigatorZoomOutItem1.Id = 6;
			this.viewNavigatorZoomOutItem1.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Subtract));
			this.viewNavigatorZoomOutItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorZoomOutItem1.LargeGlyph")));
			this.viewNavigatorZoomOutItem1.Name = "viewNavigatorZoomOutItem1";
			// 
			// viewSelectorBar1
			// 
			this.viewSelectorBar1.CanDockStyle = ((DevExpress.XtraBars.BarCanDockStyle)((DevExpress.XtraBars.BarCanDockStyle.Floating | DevExpress.XtraBars.BarCanDockStyle.Bottom)));
			this.viewSelectorBar1.DockCol = 0;
			this.viewSelectorBar1.DockRow = 0;
			this.viewSelectorBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
			this.viewSelectorBar1.FloatLocation = new System.Drawing.Point(1274, 202);
			this.viewSelectorBar1.FloatSize = new System.Drawing.Size(442, 30);
			this.viewSelectorBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.viewSelectorItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewSelectorItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewSelectorItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewSelectorItem4, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewSelectorItem5)});
			this.viewSelectorBar1.OptionsBar.AllowQuickCustomization = false;
			this.viewSelectorBar1.OptionsBar.DisableClose = true;
			this.viewSelectorBar1.OptionsBar.DisableCustomization = true;
			this.viewSelectorBar1.OptionsBar.DrawDragBorder = false;
			this.viewSelectorBar1.OptionsBar.MultiLine = true;
			// 
			// viewSelectorItem1
			// 
			this.viewSelectorItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem1.Glyph")));
			this.viewSelectorItem1.GroupIndex = 1;
			this.viewSelectorItem1.Id = 7;
			this.viewSelectorItem1.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt)
							| System.Windows.Forms.Keys.D1));
			this.viewSelectorItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem1.LargeGlyph")));
			this.viewSelectorItem1.Name = "viewSelectorItem1";
			this.viewSelectorItem1.SchedulerViewType = DevExpress.XtraScheduler.SchedulerViewType.Day;
			// 
			// viewSelectorItem2
			// 
			this.viewSelectorItem2.Glyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem2.Glyph")));
			this.viewSelectorItem2.GroupIndex = 1;
			this.viewSelectorItem2.Id = 8;
			this.viewSelectorItem2.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt)
							| System.Windows.Forms.Keys.D2));
			this.viewSelectorItem2.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem2.LargeGlyph")));
			this.viewSelectorItem2.Name = "viewSelectorItem2";
			this.viewSelectorItem2.SchedulerViewType = DevExpress.XtraScheduler.SchedulerViewType.WorkWeek;
			// 
			// viewSelectorItem3
			// 
			this.viewSelectorItem3.Glyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem3.Glyph")));
			this.viewSelectorItem3.GroupIndex = 1;
			this.viewSelectorItem3.Id = 9;
			this.viewSelectorItem3.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt)
							| System.Windows.Forms.Keys.D3));
			this.viewSelectorItem3.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem3.LargeGlyph")));
			this.viewSelectorItem3.Name = "viewSelectorItem3";
			this.viewSelectorItem3.SchedulerViewType = DevExpress.XtraScheduler.SchedulerViewType.Week;
			// 
			// viewSelectorItem4
			// 
			this.viewSelectorItem4.Glyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem4.Glyph")));
			this.viewSelectorItem4.GroupIndex = 1;
			this.viewSelectorItem4.Id = 10;
			this.viewSelectorItem4.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt)
							| System.Windows.Forms.Keys.D4));
			this.viewSelectorItem4.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem4.LargeGlyph")));
			this.viewSelectorItem4.Name = "viewSelectorItem4";
			this.viewSelectorItem4.SchedulerViewType = DevExpress.XtraScheduler.SchedulerViewType.Month;
			// 
			// viewSelectorItem5
			// 
			this.viewSelectorItem5.Glyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem5.Glyph")));
			this.viewSelectorItem5.GroupIndex = 1;
			this.viewSelectorItem5.Id = 11;
			this.viewSelectorItem5.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt)
							| System.Windows.Forms.Keys.D5));
			this.viewSelectorItem5.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem5.LargeGlyph")));
			this.viewSelectorItem5.Name = "viewSelectorItem5";
			this.viewSelectorItem5.SchedulerViewType = DevExpress.XtraScheduler.SchedulerViewType.Timeline;
			// 
			// barDockControlTop
			// 
			this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
			this.barDockControlTop.Size = new System.Drawing.Size(1113, 0);
			// 
			// barDockControlBottom
			// 
			this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.barDockControlBottom.Location = new System.Drawing.Point(0, 700);
			this.barDockControlBottom.Size = new System.Drawing.Size(1113, 0);
			// 
			// barDockControlLeft
			// 
			this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
			this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
			this.barDockControlLeft.Size = new System.Drawing.Size(0, 700);
			// 
			// barDockControlRight
			// 
			this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
			this.barDockControlRight.Location = new System.Drawing.Point(1113, 0);
			this.barDockControlRight.Size = new System.Drawing.Size(0, 700);
			// 
			// repositoryItemWeekOfMonth1
			// 
			this.repositoryItemWeekOfMonth1.AutoHeight = false;
			this.repositoryItemWeekOfMonth1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.repositoryItemWeekOfMonth1.Name = "repositoryItemWeekOfMonth1";
			// 
			// repositoryItemResourcesComboBox1
			// 
			this.repositoryItemResourcesComboBox1.AutoHeight = false;
			this.repositoryItemResourcesComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.repositoryItemResourcesComboBox1.Name = "repositoryItemResourcesComboBox1";
			// 
			// schedulerStorage1
			// 
			this.schedulerStorage1.Appointments.ResourceSharing = true;
			// 
			// treeList1
			// 
			this.treeList1.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn1,
            this.treeListColumn2});
			this.treeList1.CustomizationFormBounds = new System.Drawing.Rectangle(1404, 445, 218, 205);
			this.treeList1.Location = new System.Drawing.Point(12, 31);
			this.treeList1.Name = "treeList1";
			this.treeList1.OptionsView.ShowIndicator = false;
			this.treeList1.OptionsView.ShowRoot = false;
			this.treeList1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTimeEdit1,
            this.repositoryItemTimeEdit2});
			this.treeList1.ShowButtonMode = DevExpress.XtraTreeList.ShowButtonModeEnum.ShowAlways;
			this.treeList1.Size = new System.Drawing.Size(1065, 231);
			this.treeList1.TabIndex = 4;
			// 
			// treeListColumn1
			// 
			this.treeListColumn1.Caption = "Instance date/time";
			this.treeListColumn1.ColumnEdit = this.repositoryItemTimeEdit1;
			this.treeListColumn1.FieldName = "treeListColumn1";
			this.treeListColumn1.Name = "treeListColumn1";
			this.treeListColumn1.OptionsColumn.AllowEdit = false;
			this.treeListColumn1.OptionsColumn.AllowFocus = false;
			this.treeListColumn1.OptionsColumn.ReadOnly = true;
			this.treeListColumn1.Visible = true;
			this.treeListColumn1.VisibleIndex = 0;
			// 
			// repositoryItemTimeEdit1
			// 
			this.repositoryItemTimeEdit1.AllowFocused = false;
			this.repositoryItemTimeEdit1.AutoHeight = false;
			this.repositoryItemTimeEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
			this.repositoryItemTimeEdit1.Mask.EditMask = "g";
			this.repositoryItemTimeEdit1.Name = "repositoryItemTimeEdit1";
			this.repositoryItemTimeEdit1.ReadOnly = true;
			// 
			// treeListColumn2
			// 
			this.treeListColumn2.Caption = "New date/time";
			this.treeListColumn2.ColumnEdit = this.repositoryItemTimeEdit2;
			this.treeListColumn2.FieldName = "treeListColumn2";
			this.treeListColumn2.Name = "treeListColumn2";
			this.treeListColumn2.Visible = true;
			this.treeListColumn2.VisibleIndex = 1;
			// 
			// repositoryItemTimeEdit2
			// 
			this.repositoryItemTimeEdit2.AutoHeight = false;
			this.repositoryItemTimeEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)});
			this.repositoryItemTimeEdit2.Mask.EditMask = "g";
			this.repositoryItemTimeEdit2.Name = "repositoryItemTimeEdit2";
			this.repositoryItemTimeEdit2.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemTimeEdit2_ButtonClick);
			this.repositoryItemTimeEdit2.EditValueChanged += new System.EventHandler(this.repositoryItemTimeEdit2_EditValueChanged);
			// 
			// layoutControlGroup1
			// 
			this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
			this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
			this.layoutControlGroup1.GroupBordersVisible = false;
			this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2});
			this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup1.Name = "layoutControlGroup1";
			this.layoutControlGroup1.Size = new System.Drawing.Size(1089, 644);
			this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup1.Text = "layoutControlGroup1";
			this.layoutControlGroup1.TextVisible = false;
			// 
			// layoutControlItem1
			// 
			this.layoutControlItem1.Control = this.treeList1;
			this.layoutControlItem1.CustomizationFormText = "Instances with conflicts:";
			this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem1.Name = "layoutControlItem1";
			this.layoutControlItem1.Size = new System.Drawing.Size(1069, 254);
			this.layoutControlItem1.Text = "Instances with conflicts:";
			this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Top;
			this.layoutControlItem1.TextSize = new System.Drawing.Size(137, 16);
			// 
			// layoutControlItem2
			// 
			this.layoutControlItem2.Control = this.schedulerControl1;
			this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
			this.layoutControlItem2.Location = new System.Drawing.Point(0, 254);
			this.layoutControlItem2.Name = "layoutControlItem2";
			this.layoutControlItem2.Size = new System.Drawing.Size(1069, 370);
			this.layoutControlItem2.Text = "layoutControlItem2";
			this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem2.TextToControlDistance = 0;
			this.layoutControlItem2.TextVisible = false;
			// 
			// barDockControl1
			// 
			this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
			this.barDockControl1.Location = new System.Drawing.Point(0, 0);
			this.barDockControl1.Size = new System.Drawing.Size(1113, 0);
			// 
			// barDockControl2
			// 
			this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.barDockControl2.Location = new System.Drawing.Point(0, 700);
			this.barDockControl2.Size = new System.Drawing.Size(1113, 0);
			// 
			// barDockControl3
			// 
			this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
			this.barDockControl3.Location = new System.Drawing.Point(0, 0);
			this.barDockControl3.Size = new System.Drawing.Size(0, 700);
			// 
			// barDockControl4
			// 
			this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
			this.barDockControl4.Location = new System.Drawing.Point(1113, 0);
			this.barDockControl4.Size = new System.Drawing.Size(0, 700);
			// 
			// viewNavigator1
			// 
			//this.viewNavigator1.BarManager = this.barManager1;
			//this.viewNavigator1.SchedulerControl = this.schedulerControl1;
			// 
			// viewSelector1
			// 
			//this.viewSelector1.BarManager = this.barManager1;
			//this.viewSelector1.SchedulerControl = this.schedulerControl1;
			// 
			// ConflictResolveDialog
			// 
			this.ApplyEnabled = true;
			this.ApplyVisible = true;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
			this.ClientSize = new System.Drawing.Size(1113, 700);
			this.Controls.Add(this.barDockControl3);
			this.Controls.Add(this.barDockControl4);
			this.Controls.Add(this.barDockControl2);
			this.Controls.Add(this.barDockControl1);
			this.Controls.Add(this.barDockControlLeft);
			this.Controls.Add(this.barDockControlRight);
			this.Controls.Add(this.barDockControlBottom);
			this.Controls.Add(this.barDockControlTop);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "ConflictResolveDialog";
			this.ShowInTaskbar = true;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Conflict Resolution Dialog";
			this.Load += new System.EventHandler(this.ConflictResolveDialog_Load);
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnFormClosing);
			this.Controls.SetChildIndex(this.barDockControlTop, 0);
			this.Controls.SetChildIndex(this.barDockControlBottom, 0);
			this.Controls.SetChildIndex(this.barDockControlRight, 0);
			this.Controls.SetChildIndex(this.barDockControlLeft, 0);
			this.Controls.SetChildIndex(this.barDockControl1, 0);
			this.Controls.SetChildIndex(this.barDockControl2, 0);
			this.Controls.SetChildIndex(this.barDockControl4, 0);
			this.Controls.SetChildIndex(this.barDockControl3, 0);
			((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).EndInit();
			this.ContentPanel.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
			this.layoutControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.schedulerControl1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.repositoryItemWeekOfMonth1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.repositoryItemResourcesComboBox1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.schedulerStorage1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.treeList1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
			//((System.ComponentModel.ISupportInitialize)(this.viewNavigator1)).EndInit();
			//((System.ComponentModel.ISupportInitialize)(this.viewSelector1)).EndInit();
			this.ResumeLayout(false);

		}

		

		#endregion

		private DevExpress.XtraLayout.LayoutControl layoutControl1;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private DevExpress.XtraScheduler.SchedulerStorage schedulerStorage1;
		private DevExpress.XtraTreeList.TreeList treeList1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
		private DevExpress.XtraScheduler.SchedulerControl schedulerControl1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
		private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn1;
		private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit repositoryItemTimeEdit1;
		private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn2;
		private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit repositoryItemTimeEdit2;
		private DevExpress.XtraBars.BarManager barManager1;
		private DevExpress.XtraBars.BarDockControl barDockControlTop;
		private DevExpress.XtraBars.BarDockControl barDockControlBottom;
		private DevExpress.XtraBars.BarDockControl barDockControlLeft;
		private DevExpress.XtraBars.BarDockControl barDockControlRight;
		private DevExpress.XtraScheduler.UI.RepositoryItemWeekOfMonth repositoryItemWeekOfMonth1;
		private DevExpress.XtraBars.BarDockControl barDockControl3;
		private DevExpress.XtraBars.BarDockControl barDockControl4;
		private DevExpress.XtraBars.BarDockControl barDockControl2;
		private DevExpress.XtraBars.BarDockControl barDockControl1;
		private DevExpress.XtraScheduler.UI.ViewNavigatorBar viewNavigatorBar1;
		private DevExpress.XtraScheduler.UI.ViewNavigatorBackwardItem viewNavigatorBackwardItem1;
		private DevExpress.XtraScheduler.UI.ViewNavigatorForwardItem viewNavigatorForwardItem1;
		private DevExpress.XtraScheduler.UI.ViewNavigatorTodayItem viewNavigatorTodayItem1;
		private DevExpress.XtraScheduler.UI.ViewNavigatorZoomInItem viewNavigatorZoomInItem1;
		private DevExpress.XtraScheduler.UI.ViewNavigatorZoomOutItem viewNavigatorZoomOutItem1;
		private DevExpress.XtraScheduler.UI.ViewSelectorBar viewSelectorBar1;
		private DevExpress.XtraScheduler.UI.ViewSelectorItem viewSelectorItem1;
		private DevExpress.XtraScheduler.UI.ViewSelectorItem viewSelectorItem2;
		private DevExpress.XtraScheduler.UI.ViewSelectorItem viewSelectorItem3;
		private DevExpress.XtraScheduler.UI.ViewSelectorItem viewSelectorItem4;
		private DevExpress.XtraScheduler.UI.ViewSelectorItem viewSelectorItem5;
		//private DevExpress.XtraScheduler.UI.ViewNavigator viewNavigator1;
		//private DevExpress.XtraScheduler.UI.ViewSelector viewSelector1;
		private DevExpress.XtraScheduler.UI.RepositoryItemResourcesComboBox repositoryItemResourcesComboBox1;
	}
}
