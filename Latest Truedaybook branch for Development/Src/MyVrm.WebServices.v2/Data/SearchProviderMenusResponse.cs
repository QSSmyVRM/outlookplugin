﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace MyVrm.WebServices.Data
{
    internal class SearchProviderMenusResponse : ServiceResponse
    {
        private readonly List<CateringProviderMenu> _menus = new List<CateringProviderMenu>();

        internal ReadOnlyCollection<CateringProviderMenu> Menus
        {
            get
            {
                return new ReadOnlyCollection<CateringProviderMenu>(_menus);
            }
        }

        internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
        {
            _menus.Clear();
            base.ReadElementsFromXml(reader);
            if (!reader.IsEmptyElement)
            {
                do
                {
                    reader.Read();
                    if (reader.IsStartElement(XmlNamespace.NotSpecified, "Menu"))
                    {
                        var menu = new CateringProviderMenu();
                        menu.LoadFromXml(reader, "Menu");
                        _menus.Add(menu);
                    }
                    else
                    {
                        reader.SkipCurrentElement();
                    }
                } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "SearchProviderMenus"));
            }
        }
    }
}
