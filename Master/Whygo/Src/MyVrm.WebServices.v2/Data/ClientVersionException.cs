﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace MyVrm.WebServices.Data
{
    /// <summary>
    /// Represents an error that occurs when a request cannot be handled due to a service version mismatch.
    /// </summary>
    [Serializable]
    public class ClientVersionException : MyVrmServiceLocalException
    {
        public ClientVersionException()
        {
        }

        public ClientVersionException(string message) : base(message)
        {
        }

        public ClientVersionException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public ClientVersionException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
