﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace MyVrm.WebServices.Data
{
    internal class GetUserRequest : ServiceRequestBase<GetUserResponse>
    {
        internal GetUserRequest(MyVrmService service) : base(service)
        {
        }

        internal UserId RequestedUserId { get; set; }

        #region Overrides of ServiceRequestBase<GetUserResponse>

        internal override string GetXmlElementName()
        {
            return "login";
        }

        internal override string GetCommandName()
        {
            return "GetOldUser";
        }

        internal override string GetResponseXmlElementName()
        {
            return "oldUser";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            UserId.WriteToXml(writer);
            writer.WriteStartElement(XmlNamespace.NotSpecified, "user");
            RequestedUserId.WriteToXml(writer);
            writer.WriteEndElement();
        }

        #endregion
    }
}
