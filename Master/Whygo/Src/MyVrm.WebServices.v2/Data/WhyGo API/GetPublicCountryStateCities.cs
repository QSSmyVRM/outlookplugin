﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;

namespace MyVrm.WebServices.Data
{
	class GetPublicCountryStateCitiesRequest: ServiceRequestBase<GetPublicCountryStateCitiesResponse>
	{
		internal int _countryId;
		internal int _stateId;
		internal GetPublicCountryStateCitiesRequest(MyVrmService service)
			: base(service)
		{
		}

		#region Overrides of ServiceRequestBase

		internal override string GetXmlElementName()
		{
			return "GetPublicCountryStateCities";
		}

		internal override string GetCommandName()
		{
			return Constants.GetPublicCountryStateCitiesCommandName;
		}

		internal override string GetResponseXmlElementName()
		{
			return "GetPublicCountryStateCities";
		}

		internal override GetPublicCountryStateCitiesResponse ParseResponse(MyVrmServiceXmlReader reader)
		{
			var response = new GetPublicCountryStateCitiesResponse(); //{ Room = new Room(Service) };
			response.LoadFromXml(reader, "GetPublicCountryStateCities");// GetResponseXmlElementName());
			return response;
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			UserId.WriteToXml(writer, "userID");
			writer.WriteElementValue(XmlNamespace.NotSpecified, "CountryId", _countryId);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "StateId", _stateId);
		}

		#endregion
	}

	public class GetPublicCountryStateCitiesResponse : ServiceResponse
	{
		public Dictionary<int, string> Cities { get; internal set; }

		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{
			Cities = new Dictionary<int, string>();
			int id = 0;
			string name = string.Empty;
			while (!reader.IsEndElement(XmlNamespace.NotSpecified, "GetPublicCountryStateCities"))
			{
				switch (reader.LocalName)
				{
					case "ID":
						id = reader.ReadElementValue<int>();
						break;
					case "Name":
						name = reader.ReadElementValue<string>();
						break;
				}
				if (reader.IsEndElement(XmlNamespace.NotSpecified, "City"))
				{
					if (id > 0 && !string.IsNullOrEmpty(name))
					{
						Cities.Add(id, name);
						id = 0;
						name = string.Empty;
					}
				}
				reader.Read();
			}
		}
	}
}