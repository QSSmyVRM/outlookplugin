﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CallWebApi.Classes
{
    /// <summary>
    /// Request with maximum available seats of the company
    /// </summary>
    public class MaximumAvailableSeatsResponse : BaseResponse
    {
        /// <summary>
        /// Maximum available seats for this company
        /// </summary>
        public int MaximumAvailableSeats { set; get; }
    }
}