﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.WebServices.Data
{
    internal class GetAudioUsersRequest : ServiceRequestBase<GetAudioUsersResponse>
    {
        internal GetAudioUsersRequest(MyVrmService service) : base(service)
        {
        }

        #region Overrides of ServiceRequestBase<GetAudioUsersResponse>

        internal override string GetXmlElementName()
        {
            return "login";
        }

        internal override string GetCommandName()
        {
            return "GetAudioUserList";
        }

        internal override string GetResponseXmlElementName()
        {
            return "users";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            OrganizationId.WriteToXml(writer);
            UserId.WriteToXml(writer);
        }

        #endregion
    }
}
