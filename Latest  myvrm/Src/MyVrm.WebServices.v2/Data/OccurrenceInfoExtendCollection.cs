﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	public class OccurrenceInfoExtendCollection : ComplexPropertyCollection<OccurrenceInfoExtend>
	{
		private const string InstanceElementName = "instance";
		private const string AppointmentTimeElementName = "appointmentTime";

		public override object Clone()
		{
			OccurrenceInfoExtendCollection copy = new OccurrenceInfoExtendCollection();
			foreach (var item in Items)
			{
				copy.Add((OccurrenceInfoExtend)item.Clone());
			}
			return copy;
		}

		internal AppointmentTime AppointmentTime { get; set; }

		public void Add(OccurrenceInfoExtend occurrenceInfo)
		{
			InternalAdd(occurrenceInfo);
		}

		public bool Remove(OccurrenceInfoExtend occurrenceInfo)
		{
			return InternalRemove(occurrenceInfo);
		}

		public void Clear()
		{
			InternalClear();
		}

		#region Overrides of ComplexPropertyCollection<OccurrenceInfo>

		internal override OccurrenceInfoExtend CreateComplexProperty(string xmlElementName)
		{
			return xmlElementName == InstanceElementName ? new OccurrenceInfoExtend() : null;
		}

		internal override string GetCollectionItemXmlElementName(OccurrenceInfoExtend complexProperty)
		{
			return InstanceElementName;
		}

		internal override void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
		{
			reader.EnsureCurrentNodeIsStartElement(XmlNamespace.NotSpecified, xmlElementName);
			if (!reader.IsEmptyElement)
			{
				do
				{
					reader.Read();
					if (reader.IsStartElement())
					{
						var complexProperty = CreateComplexProperty(reader.LocalName);
						if (complexProperty != null)
						{
							complexProperty.LoadFromXml(reader, reader.LocalName);
							InternalAdd(complexProperty);
						}
						else if (reader.LocalName == AppointmentTimeElementName)
						{
							AppointmentTime = new AppointmentTime();
							AppointmentTime.LoadFromXml(reader, AppointmentTimeElementName);
						}
						else
						{
							reader.SkipCurrentElement();
						}
					}
				}
				while (!reader.IsEndElement(XmlNamespace.NotSpecified, xmlElementName));
			}
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			AppointmentTime.WriteToXml(writer, AppointmentTimeElementName);
			base.WriteElementsToXml(writer);
		}

		#endregion
	}
}
