﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    public sealed class UserStatus : ComplexProperty
    {
        internal UserStatus()
        {
        }

        public bool Deleted { get; set; }
        public bool Locked { get; set; }

		public override object Clone()
		{
			UserStatus copy = new UserStatus();
			copy.Deleted = Deleted;
			copy.Locked = Locked;

			return copy;
		}

        internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
        {
            switch(reader.LocalName)
            {
                case "deleted":
                {
                    Deleted = Utilities.BoolStringToBool(reader.ReadValue());
                    return true;
                }
                case "locked":
                {
                    Locked = Utilities.BoolStringToBool(reader.ReadValue());
                    return true;
                }
            }
            return false;
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            writer.WriteElementValue(XmlNamespace.NotSpecified, "deleted", Deleted);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "locked", Locked);
        }
    }
}
