﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace MyVrm.WebServices.Data
{
    internal class GetAudioUsersResponse : ServiceResponse
    {
        readonly List<AudioUser> _users = new List<AudioUser>();

        internal ReadOnlyCollection<AudioUser> Users
        {
            get
            {
                return new ReadOnlyCollection<AudioUser>(_users);
            }
        }

        internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
        {
            do
            {
                reader.Read();
                if (reader.LocalName == "user")
                {
                    var user = new AudioUser();
                    user.LoadFromXml(reader, reader.LocalName);
                    _users.Add(user);
                }
                else
                {
                    reader.SkipCurrentElement();
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "users"));
        }
    }
}
