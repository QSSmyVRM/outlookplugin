﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.Outlook.WinForms.Conference
{
    partial class ResourcesScheduleControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
			if (disposing && Conference != null)
			{
				Conference.PropertyChanged -= ConferencePropertyChanged;
                Conference.LocationIds.ListChanged -= LocationIdsListChanged;
                Conference.Participants.ListChanged -= ParticipantsListChanged;
			}
			if (disposing && _conferenceUsers != null)
				_conferenceUsers.ListChanged -= ConferenceUsersListChanged;
			if (disposing && _conferenceRooms != null)
				_conferenceRooms.ListChanged -= ConferenceRoomsListChanged;

        	if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraScheduler.SchedulerColorSchema schedulerColorSchema1 = new DevExpress.XtraScheduler.SchedulerColorSchema();
            DevExpress.XtraScheduler.TimeScaleYear timeScaleYear1 = new DevExpress.XtraScheduler.TimeScaleYear();
            DevExpress.XtraScheduler.TimeScaleQuarter timeScaleQuarter1 = new DevExpress.XtraScheduler.TimeScaleQuarter();
            DevExpress.XtraScheduler.TimeScaleMonth timeScaleMonth1 = new DevExpress.XtraScheduler.TimeScaleMonth();
            DevExpress.XtraScheduler.TimeScaleWeek timeScaleWeek1 = new DevExpress.XtraScheduler.TimeScaleWeek();
            DevExpress.XtraScheduler.TimeScaleDay timeScaleDay1 = new DevExpress.XtraScheduler.TimeScaleDay();
            DevExpress.XtraScheduler.TimeScaleHour timeScaleHour1 = new DevExpress.XtraScheduler.TimeScaleHour();
            DevExpress.XtraScheduler.TimeScaleFixedInterval timeScaleFixedInterval1 = new DevExpress.XtraScheduler.TimeScaleFixedInterval();
            DevExpress.XtraScheduler.TimeScaleFixedInterval timeScaleFixedInterval2 = new DevExpress.XtraScheduler.TimeScaleFixedInterval();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ResourcesScheduleControl));
            DevExpress.XtraScheduler.SchedulerColorSchema schedulerColorSchema2 = new DevExpress.XtraScheduler.SchedulerColorSchema();
            DevExpress.XtraScheduler.TimeScaleYear timeScaleYear2 = new DevExpress.XtraScheduler.TimeScaleYear();
            DevExpress.XtraScheduler.TimeScaleQuarter timeScaleQuarter2 = new DevExpress.XtraScheduler.TimeScaleQuarter();
            DevExpress.XtraScheduler.TimeScaleMonth timeScaleMonth2 = new DevExpress.XtraScheduler.TimeScaleMonth();
            DevExpress.XtraScheduler.TimeScaleWeek timeScaleWeek2 = new DevExpress.XtraScheduler.TimeScaleWeek();
            DevExpress.XtraScheduler.TimeScaleDay timeScaleDay2 = new DevExpress.XtraScheduler.TimeScaleDay();
            DevExpress.XtraScheduler.TimeScaleHour timeScaleHour2 = new DevExpress.XtraScheduler.TimeScaleHour();
            DevExpress.XtraScheduler.TimeScaleFixedInterval timeScaleFixedInterval3 = new DevExpress.XtraScheduler.TimeScaleFixedInterval();
            DevExpress.XtraScheduler.TimeScaleFixedInterval timeScaleFixedInterval4 = new DevExpress.XtraScheduler.TimeScaleFixedInterval();
            this.layoutControl2 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.usersSchedulerControl = new DevExpress.XtraScheduler.SchedulerControl();
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.resourcesBar = new DevExpress.XtraBars.Bar();
            this.selectRoomsBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.removeResourceBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.moveRoomUpBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.moveRoomDownBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.standaloneBarDockControl2 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.usersSchedulerStorage = new DevExpress.XtraScheduler.SchedulerStorage(this.components);
            this.roomsSchedulerControl = new DevExpress.XtraScheduler.SchedulerControl();
            this.roomsSchedulerStorage = new DevExpress.XtraScheduler.SchedulerStorage(this.components);
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.roomFreeBusyUpdater = new System.ComponentModel.BackgroundWorker();
            this.userFreeBusyUpdater = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.usersSchedulerControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersSchedulerStorage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.roomsSchedulerControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.roomsSchedulerStorage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl2
            // 
            this.layoutControl2.AllowCustomizationMenu = false;
            this.layoutControl2.Controls.Add(this.usersSchedulerControl);
            this.layoutControl2.Controls.Add(this.standaloneBarDockControl2);
            this.layoutControl2.Controls.Add(this.roomsSchedulerControl);
            this.layoutControl2.Controls.Add(this.groupControl1);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(895, 555);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // usersSchedulerControl
            // 
            this.usersSchedulerControl.ActiveViewType = DevExpress.XtraScheduler.SchedulerViewType.Timeline;
            this.usersSchedulerControl.GroupType = DevExpress.XtraScheduler.SchedulerGroupType.Resource;
            this.usersSchedulerControl.Location = new System.Drawing.Point(5, 64);
            this.usersSchedulerControl.MenuManager = this.barManager;
            this.usersSchedulerControl.Name = "usersSchedulerControl";
            this.usersSchedulerControl.OptionsCustomization.AllowAppointmentCopy = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.usersSchedulerControl.OptionsCustomization.AllowAppointmentCreate = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.usersSchedulerControl.OptionsCustomization.AllowAppointmentDelete = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.usersSchedulerControl.OptionsCustomization.AllowAppointmentDrag = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.usersSchedulerControl.OptionsCustomization.AllowAppointmentDragBetweenResources = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.usersSchedulerControl.OptionsCustomization.AllowAppointmentEdit = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.usersSchedulerControl.OptionsCustomization.AllowAppointmentMultiSelect = false;
            this.usersSchedulerControl.OptionsCustomization.AllowAppointmentResize = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.usersSchedulerControl.OptionsCustomization.AllowDisplayAppointmentForm = DevExpress.XtraScheduler.AllowDisplayAppointmentForm.Never;
            this.usersSchedulerControl.OptionsCustomization.AllowInplaceEditor = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.usersSchedulerControl.OptionsView.HideSelection = true;
            this.usersSchedulerControl.OptionsView.NavigationButtons.Visibility = DevExpress.XtraScheduler.NavigationButtonVisibility.Never;
            this.usersSchedulerControl.OptionsView.ResourceHeaders.Height = 150;
            this.usersSchedulerControl.OptionsView.ResourceHeaders.ImageSizeMode = DevExpress.XtraScheduler.HeaderImageSizeMode.Normal;
            this.usersSchedulerControl.OptionsView.ResourceHeaders.RotateCaption = false;
            this.usersSchedulerControl.OptionsView.ShowOnlyResourceAppointments = true;
            schedulerColorSchema1.Cell = System.Drawing.SystemColors.Control;
            schedulerColorSchema1.CellBorder = System.Drawing.SystemColors.ControlDark;
            schedulerColorSchema1.CellBorderDark = System.Drawing.SystemColors.ControlDark;
            schedulerColorSchema1.CellLight = System.Drawing.SystemColors.Window;
            schedulerColorSchema1.CellLightBorder = System.Drawing.SystemColors.ControlDark;
            schedulerColorSchema1.CellLightBorderDark = System.Drawing.SystemColors.ControlDark;
            this.usersSchedulerControl.ResourceColorSchemas.Add(schedulerColorSchema1);
            this.usersSchedulerControl.ResourceNavigator.Buttons.Append.Visible = false;
            this.usersSchedulerControl.ResourceNavigator.Buttons.Remove.Visible = false;
            this.usersSchedulerControl.Size = new System.Drawing.Size(885, 262);
            this.usersSchedulerControl.Start = new System.DateTime(2009, 10, 25, 0, 0, 0, 0);
            this.usersSchedulerControl.Storage = this.usersSchedulerStorage;
            this.usersSchedulerControl.TabIndex = 14;
            this.usersSchedulerControl.Views.DayView.Enabled = false;
            this.usersSchedulerControl.Views.MonthView.Enabled = false;
            this.usersSchedulerControl.Views.TimelineView.AppointmentDisplayOptions.AppointmentAutoHeight = true;
            this.usersSchedulerControl.Views.TimelineView.AppointmentDisplayOptions.StatusDisplayType = DevExpress.XtraScheduler.AppointmentStatusDisplayType.Time;
            this.usersSchedulerControl.Views.TimelineView.DateTimeScrollbarVisible = false;
            this.usersSchedulerControl.Views.TimelineView.GroupSeparatorWidth = 1;
            this.usersSchedulerControl.Views.TimelineView.NavigationButtonVisibility = DevExpress.XtraScheduler.NavigationButtonVisibility.Never;
            this.usersSchedulerControl.Views.TimelineView.ResourcesPerPage = 5;
            timeScaleYear1.Enabled = false;
            timeScaleQuarter1.Enabled = false;
            timeScaleMonth1.Enabled = false;
            timeScaleWeek1.Enabled = false;
            timeScaleDay1.DisplayFormat = "dddd, dd MMMM yyyy";
            timeScaleHour1.Enabled = false;
            timeScaleFixedInterval1.Value = System.TimeSpan.Parse("00:30:00");
            timeScaleFixedInterval2.Enabled = false;
            this.usersSchedulerControl.Views.TimelineView.Scales.Add(timeScaleYear1);
            this.usersSchedulerControl.Views.TimelineView.Scales.Add(timeScaleQuarter1);
            this.usersSchedulerControl.Views.TimelineView.Scales.Add(timeScaleMonth1);
            this.usersSchedulerControl.Views.TimelineView.Scales.Add(timeScaleWeek1);
            this.usersSchedulerControl.Views.TimelineView.Scales.Add(timeScaleDay1);
            this.usersSchedulerControl.Views.TimelineView.Scales.Add(timeScaleHour1);
            this.usersSchedulerControl.Views.TimelineView.Scales.Add(timeScaleFixedInterval1);
            this.usersSchedulerControl.Views.TimelineView.Scales.Add(timeScaleFixedInterval2);
            this.usersSchedulerControl.Views.TimelineView.SelectionBar.Visible = false;
            this.usersSchedulerControl.Views.TimelineView.ShowMoreButtons = false;
            this.usersSchedulerControl.Views.WeekView.Enabled = false;
            this.usersSchedulerControl.Views.WorkWeekView.Enabled = false;
            this.usersSchedulerControl.SelectionChanged += new System.EventHandler(this.usersSchedulerControl_SelectionChanged);
            this.usersSchedulerControl.VisibleIntervalChanged += new System.EventHandler(this.usersSchedulerControl_VisibleIntervalChanged);
            this.usersSchedulerControl.PreparePopupMenu += new DevExpress.XtraScheduler.PreparePopupMenuEventHandler(this.usersSchedulerControl_PreparePopupMenu);
            this.usersSchedulerControl.CustomDrawTimeCell += new DevExpress.XtraScheduler.CustomDrawObjectEventHandler(this.schedulerControl_CustomDrawTimeCell);
            this.usersSchedulerControl.CustomDrawResourceHeader += new DevExpress.XtraScheduler.CustomDrawObjectEventHandler(this.usersSchedulerControl_CustomDrawResourceHeader);
            this.usersSchedulerControl.Enter += new System.EventHandler(this.usersSchedulerControl_Enter);
            this.usersSchedulerControl.MouseDown += new System.Windows.Forms.MouseEventHandler(this.usersSchedulerControl_MouseDown);
            // 
            // barManager
            // 
            this.barManager.AllowCustomization = false;
            this.barManager.AllowQuickCustomization = false;
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.resourcesBar});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.DockControls.Add(this.standaloneBarDockControl2);
            this.barManager.Form = this;
            this.barManager.Images = this.imageCollection;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.removeResourceBarButtonItem,
            this.selectRoomsBarButtonItem,
            this.moveRoomUpBarButtonItem,
            this.moveRoomDownBarButtonItem});
            this.barManager.MaxItemId = 14;
            // 
            // resourcesBar
            // 
            this.resourcesBar.BarName = "Participants";
            this.resourcesBar.DockCol = 0;
            this.resourcesBar.DockRow = 0;
            this.resourcesBar.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.resourcesBar.FloatLocation = new System.Drawing.Point(102, 208);
            this.resourcesBar.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.selectRoomsBarButtonItem, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.removeResourceBarButtonItem, true),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.moveRoomUpBarButtonItem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.moveRoomDownBarButtonItem)});
            this.resourcesBar.OptionsBar.AllowQuickCustomization = false;
            this.resourcesBar.OptionsBar.DisableClose = true;
            this.resourcesBar.OptionsBar.DisableCustomization = true;
            this.resourcesBar.OptionsBar.DrawDragBorder = false;
            this.resourcesBar.OptionsBar.RotateWhenVertical = false;
            this.resourcesBar.OptionsBar.UseWholeRow = true;
            this.resourcesBar.StandaloneBarDockControl = this.standaloneBarDockControl2;
            this.resourcesBar.Text = "Resources";
            // 
            // selectRoomsBarButtonItem
            // 
            this.selectRoomsBarButtonItem.Caption = "Select rooms...";
            this.selectRoomsBarButtonItem.Hint = "Open room selection dialog";
            this.selectRoomsBarButtonItem.Id = 8;
            this.selectRoomsBarButtonItem.ImageIndex = 2;
            this.selectRoomsBarButtonItem.ItemAppearance.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.selectRoomsBarButtonItem.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold);
            this.selectRoomsBarButtonItem.ItemAppearance.Normal.ForeColor = System.Drawing.Color.Blue;
            this.selectRoomsBarButtonItem.ItemAppearance.Normal.Options.UseBackColor = true;
            this.selectRoomsBarButtonItem.ItemAppearance.Normal.Options.UseBorderColor = true;
            this.selectRoomsBarButtonItem.ItemAppearance.Normal.Options.UseFont = true;
            this.selectRoomsBarButtonItem.ItemAppearance.Normal.Options.UseForeColor = true;
            this.selectRoomsBarButtonItem.Name = "selectRoomsBarButtonItem";
            this.selectRoomsBarButtonItem.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.selectRoomsBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.selectRoomsBarButtonItem_ItemClick);
            // 
            // removeResourceBarButtonItem
            // 
            this.removeResourceBarButtonItem.Caption = "Remove";
            this.removeResourceBarButtonItem.Hint = "Remove selected resource";
            this.removeResourceBarButtonItem.Id = 7;
            this.removeResourceBarButtonItem.ImageIndex = 3;
            this.removeResourceBarButtonItem.Name = "removeResourceBarButtonItem";
            this.removeResourceBarButtonItem.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.removeResourceBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.removeResourceBarButtonItem_ItemClick);
            // 
            // moveRoomUpBarButtonItem
            // 
            this.moveRoomUpBarButtonItem.Caption = "Move room up";
            this.moveRoomUpBarButtonItem.GroupIndex = 1;
            this.moveRoomUpBarButtonItem.Hint = "Move selected room up";
            this.moveRoomUpBarButtonItem.Id = 9;
            this.moveRoomUpBarButtonItem.ImageIndex = 5;
            this.moveRoomUpBarButtonItem.Name = "moveRoomUpBarButtonItem";
            this.moveRoomUpBarButtonItem.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.moveRoomUpBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.moveRoomUpBarButtonItem_ItemClick);
            // 
            // moveRoomDownBarButtonItem
            // 
            this.moveRoomDownBarButtonItem.Caption = "Move room down";
            this.moveRoomDownBarButtonItem.Hint = "Move selected room down";
            this.moveRoomDownBarButtonItem.Id = 11;
            this.moveRoomDownBarButtonItem.ImageIndex = 4;
            this.moveRoomDownBarButtonItem.Name = "moveRoomDownBarButtonItem";
            this.moveRoomDownBarButtonItem.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.moveRoomDownBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.moveRoomDownBarButtonItem_ItemClick);
            // 
            // standaloneBarDockControl2
            // 
            this.standaloneBarDockControl2.AutoSizeInLayoutControl = false;
            this.standaloneBarDockControl2.CausesValidation = false;
            this.standaloneBarDockControl2.Location = new System.Drawing.Point(2, 2);
            this.standaloneBarDockControl2.MinimumSize = new System.Drawing.Size(0, 29);
            this.standaloneBarDockControl2.Name = "standaloneBarDockControl2";
            this.standaloneBarDockControl2.Size = new System.Drawing.Size(891, 36);
            this.standaloneBarDockControl2.Text = "standaloneBarDockControl2";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(895, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 555);
            this.barDockControlBottom.Size = new System.Drawing.Size(895, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 555);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(895, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 555);
            // 
            // imageCollection
            // 
            this.imageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection.ImageStream")));
            this.imageCollection.Images.SetKeyName(0, "Filter.png");
            this.imageCollection.Images.SetKeyName(1, "Add.png");
            this.imageCollection.Images.SetKeyName(2, "users.png");
            this.imageCollection.Images.SetKeyName(3, "clear.png");
            this.imageCollection.Images.SetKeyName(4, "Down.png");
            this.imageCollection.Images.SetKeyName(5, "Up.png");
            // 
            // usersSchedulerStorage
            // 
            this.usersSchedulerStorage.Appointments.ResourceSharing = true;
            this.usersSchedulerStorage.EnableReminders = false;
            // 
            // roomsSchedulerControl
            // 
            this.roomsSchedulerControl.ActiveViewType = DevExpress.XtraScheduler.SchedulerViewType.Timeline;
            this.roomsSchedulerControl.GroupType = DevExpress.XtraScheduler.SchedulerGroupType.Resource;
            this.roomsSchedulerControl.Location = new System.Drawing.Point(5, 326);
            this.roomsSchedulerControl.MenuManager = this.barManager;
            this.roomsSchedulerControl.Name = "roomsSchedulerControl";
            this.roomsSchedulerControl.OptionsCustomization.AllowAppointmentCopy = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.roomsSchedulerControl.OptionsCustomization.AllowAppointmentCreate = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.roomsSchedulerControl.OptionsCustomization.AllowAppointmentDelete = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.roomsSchedulerControl.OptionsCustomization.AllowAppointmentDrag = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.roomsSchedulerControl.OptionsCustomization.AllowAppointmentDragBetweenResources = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.roomsSchedulerControl.OptionsCustomization.AllowAppointmentEdit = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.roomsSchedulerControl.OptionsCustomization.AllowAppointmentMultiSelect = false;
            this.roomsSchedulerControl.OptionsCustomization.AllowAppointmentResize = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.roomsSchedulerControl.OptionsCustomization.AllowDisplayAppointmentForm = DevExpress.XtraScheduler.AllowDisplayAppointmentForm.Never;
            this.roomsSchedulerControl.OptionsCustomization.AllowInplaceEditor = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.roomsSchedulerControl.OptionsView.HideSelection = true;
            this.roomsSchedulerControl.OptionsView.NavigationButtons.Visibility = DevExpress.XtraScheduler.NavigationButtonVisibility.Never;
            this.roomsSchedulerControl.OptionsView.ResourceHeaders.Height = 150;
            this.roomsSchedulerControl.OptionsView.ResourceHeaders.ImageSizeMode = DevExpress.XtraScheduler.HeaderImageSizeMode.Normal;
            this.roomsSchedulerControl.OptionsView.ResourceHeaders.RotateCaption = false;
            this.roomsSchedulerControl.OptionsView.ShowOnlyResourceAppointments = true;
            schedulerColorSchema2.Cell = System.Drawing.SystemColors.Control;
            schedulerColorSchema2.CellBorder = System.Drawing.SystemColors.ControlDark;
            schedulerColorSchema2.CellBorderDark = System.Drawing.SystemColors.ControlDark;
            schedulerColorSchema2.CellLight = System.Drawing.SystemColors.Window;
            schedulerColorSchema2.CellLightBorder = System.Drawing.SystemColors.ControlDark;
            schedulerColorSchema2.CellLightBorderDark = System.Drawing.SystemColors.ControlDark;
            this.roomsSchedulerControl.ResourceColorSchemas.Add(schedulerColorSchema2);
            this.roomsSchedulerControl.ResourceNavigator.Buttons.Append.Visible = false;
            this.roomsSchedulerControl.ResourceNavigator.Buttons.Remove.Visible = false;
            this.roomsSchedulerControl.Size = new System.Drawing.Size(885, 224);
            this.roomsSchedulerControl.Start = new System.DateTime(2009, 10, 25, 0, 0, 0, 0);
            this.roomsSchedulerControl.Storage = this.roomsSchedulerStorage;
            this.roomsSchedulerControl.TabIndex = 4;
            this.roomsSchedulerControl.Views.DayView.Enabled = false;
            this.roomsSchedulerControl.Views.MonthView.Enabled = false;
            this.roomsSchedulerControl.Views.TimelineView.AppointmentDisplayOptions.AppointmentAutoHeight = true;
            this.roomsSchedulerControl.Views.TimelineView.AppointmentDisplayOptions.StatusDisplayType = DevExpress.XtraScheduler.AppointmentStatusDisplayType.Time;
            this.roomsSchedulerControl.Views.TimelineView.GroupSeparatorWidth = 1;
            this.roomsSchedulerControl.Views.TimelineView.NavigationButtonVisibility = DevExpress.XtraScheduler.NavigationButtonVisibility.Never;
            this.roomsSchedulerControl.Views.TimelineView.ResourcesPerPage = 5;
            timeScaleYear2.Enabled = false;
            timeScaleQuarter2.Enabled = false;
            timeScaleMonth2.Enabled = false;
            timeScaleWeek2.Enabled = false;
            timeScaleDay2.DisplayFormat = "dddd, dd MMMM yyyy";
            timeScaleDay2.Visible = false;
            timeScaleHour2.Enabled = false;
            timeScaleFixedInterval3.Value = System.TimeSpan.Parse("00:30:00");
            timeScaleFixedInterval3.Visible = false;
            timeScaleFixedInterval4.Enabled = false;
            this.roomsSchedulerControl.Views.TimelineView.Scales.Add(timeScaleYear2);
            this.roomsSchedulerControl.Views.TimelineView.Scales.Add(timeScaleQuarter2);
            this.roomsSchedulerControl.Views.TimelineView.Scales.Add(timeScaleMonth2);
            this.roomsSchedulerControl.Views.TimelineView.Scales.Add(timeScaleWeek2);
            this.roomsSchedulerControl.Views.TimelineView.Scales.Add(timeScaleDay2);
            this.roomsSchedulerControl.Views.TimelineView.Scales.Add(timeScaleHour2);
            this.roomsSchedulerControl.Views.TimelineView.Scales.Add(timeScaleFixedInterval3);
            this.roomsSchedulerControl.Views.TimelineView.Scales.Add(timeScaleFixedInterval4);
            this.roomsSchedulerControl.Views.TimelineView.SelectionBar.Visible = false;
            this.roomsSchedulerControl.Views.TimelineView.ShowMoreButtons = false;
            this.roomsSchedulerControl.Views.WeekView.Enabled = false;
            this.roomsSchedulerControl.Views.WorkWeekView.Enabled = false;
            this.roomsSchedulerControl.SelectionChanged += new System.EventHandler(this.roomsSchedulerControl_SelectionChanged);
            this.roomsSchedulerControl.VisibleIntervalChanged += new System.EventHandler(this.roomsSchedulerControl_VisibleIntervalChanged);
            this.roomsSchedulerControl.PreparePopupMenu += new DevExpress.XtraScheduler.PreparePopupMenuEventHandler(this.roomsSchedulerControl_PreparePopupMenu);
            this.roomsSchedulerControl.CustomDrawTimeCell += new DevExpress.XtraScheduler.CustomDrawObjectEventHandler(this.schedulerControl_CustomDrawTimeCell);
            this.roomsSchedulerControl.CustomDrawResourceHeader += new DevExpress.XtraScheduler.CustomDrawObjectEventHandler(this.roomsSchedulerControl_CustomDrawResourceHeader);
            this.roomsSchedulerControl.Click += new System.EventHandler(this.roomsSchedulerControl_Click);
            this.roomsSchedulerControl.Enter += new System.EventHandler(this.roomsSchedulerControl_Enter);
            this.roomsSchedulerControl.MouseDown += new System.Windows.Forms.MouseEventHandler(this.roomsSchedulerControl_MouseDown);
            this.roomsSchedulerControl.MouseMove += new System.Windows.Forms.MouseEventHandler(this.roomsSchedulerControl_MouseMove);
            // 
            // roomsSchedulerStorage
            // 
            this.roomsSchedulerStorage.EnableReminders = false;
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.groupControl1.Appearance.Options.UseBackColor = true;
            this.groupControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Location = new System.Drawing.Point(5, 42);
            this.groupControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.groupControl1.ShowCaption = false;
            this.groupControl1.Size = new System.Drawing.Size(885, 20);
            this.groupControl1.TabIndex = 18;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl1.Appearance.BorderColor = System.Drawing.SystemColors.ControlText;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl1.Location = new System.Drawing.Point(4, 2);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(877, 0);
            this.labelControl1.TabIndex = 16;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup7});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Size = new System.Drawing.Size(895, 555);
            this.layoutControlGroup2.Text = "Root";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.layoutControlGroup7.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup7.CustomizationFormText = "Conference Resources";
            this.layoutControlGroup7.GroupBordersVisible = false;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7,
            this.layoutControlItem10,
            this.layoutControlItem2,
            this.layoutControlItem1});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(895, 555);
            this.layoutControlGroup7.Text = "Conference Participants && Rooms";
            this.layoutControlGroup7.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.usersSchedulerControl;
            this.layoutControlItem7.CustomizationFormText = "Attendees:";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 64);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 0, 0);
            this.layoutControlItem7.Size = new System.Drawing.Size(895, 262);
            this.layoutControlItem7.Text = "Attendees:";
            this.layoutControlItem7.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.standaloneBarDockControl2;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(895, 40);
            this.layoutControlItem10.Text = "layoutControlItem10";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.roomsSchedulerControl;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 326);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 0, 5);
            this.layoutControlItem2.Size = new System.Drawing.Size(895, 229);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.groupControl1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 40);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 2, 2);
            this.layoutControlItem1.Size = new System.Drawing.Size(895, 24);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 169);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(701, 258);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // roomFreeBusyUpdater
            // 
            this.roomFreeBusyUpdater.WorkerSupportsCancellation = true;
            this.roomFreeBusyUpdater.DoWork += new System.ComponentModel.DoWorkEventHandler(this.roomFreeBusyUpdater_DoWork);
            this.roomFreeBusyUpdater.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.roomFreeBusyUpdater_RunWorkerCompleted);
            // 
            // userFreeBusyUpdater
            // 
            this.userFreeBusyUpdater.WorkerSupportsCancellation = true;
            this.userFreeBusyUpdater.DoWork += new System.ComponentModel.DoWorkEventHandler(this.userFreeBusyUpdater_DoWork);
            this.userFreeBusyUpdater.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.userFreeBusyUpdater_RunWorkerCompleted);
            // 
            // ResourcesScheduleControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.layoutControl2);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.DoubleBuffered = true;
            this.Name = "ResourcesScheduleControl";
            this.Size = new System.Drawing.Size(895, 555);
            this.Load += new System.EventHandler(this.ResourcesScheduleControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.usersSchedulerControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersSchedulerStorage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.roomsSchedulerControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.roomsSchedulerStorage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraScheduler.SchedulerControl usersSchedulerControl;
        private DevExpress.XtraScheduler.SchedulerStorage usersSchedulerStorage;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.Utils.ImageCollection imageCollection;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl2;
        private DevExpress.XtraBars.Bar resourcesBar;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraBars.BarButtonItem removeResourceBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem selectRoomsBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem moveRoomUpBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem moveRoomDownBarButtonItem;
        private DevExpress.XtraScheduler.SchedulerControl roomsSchedulerControl;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraScheduler.SchedulerStorage roomsSchedulerStorage;
        private System.ComponentModel.BackgroundWorker roomFreeBusyUpdater;
		private System.ComponentModel.BackgroundWorker userFreeBusyUpdater;
		private DevExpress.XtraEditors.LabelControl labelControl1;
		private DevExpress.XtraEditors.GroupControl groupControl1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
    }
}