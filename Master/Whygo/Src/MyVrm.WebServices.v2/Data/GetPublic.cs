﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	public class PublicRoom
	{
		public int LocationID { get; set; }
		public string LocationName { get; set; }
		public string CitySuburb { get; set; }
		public int ZipCode { get; set; }
		public string StateName { get; set; }
		public double GenericSellPrice { get; set; }
		public int Capacity { get; set; }
	}
	//Request
	class GetPublicRequest : ServiceRequestBase<GetPublicResponse>
	{
		internal GetPublicRequest(MyVrmService service) : base(service)
		{
		}

		#region Overrides of ServiceRequestBase<GetTemplateResponse>

		internal override string GetXmlElementName()
		{
			return "login";
		}

		internal override string GetCommandName()
		{
			return Constants.GetPublicCommandName;
		}

		internal override string GetResponseXmlElementName()
		{
			return "GetPublic";
		}

		internal override GetPublicResponse ParseResponse(MyVrmServiceXmlReader reader)
		{
			var response = new GetPublicResponse();
			response.ReadElementsFromXml(reader);
			return response;
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			if (UserId != null)
				UserId.WriteToXml(writer, "userID");
		}

		#endregion
	}

	//Response
	public class GetPublicResponse : ServiceResponse
	{
		//Result
		public List<PublicRoom> PublicRoomList = new List<PublicRoom>();

		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{
			
		}
	}
}