﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Globalization;
using System.Text.RegularExpressions;
using MyVrm.Common;
using MyVrm.Common.ComponentModel;

namespace MyVrm.WebServices.Data
{
    /// <summary>
    /// Specifies conference origin.
    /// </summary>
    public enum ConferenceOrigin
    {
        /// <summary>
        /// Conference has been created from Web.
        /// </summary>
        Web = 0,
        /// <summary>
        /// Conference has been created from Api. This is the default.
        /// </summary>
        Api = 1
    }

    public enum NetworkSwitchingOptions
    {
        /// <summary>
        /// Audio only conference.
        /// </summary>
        [LocalizedDescription("NATOSecret", typeof(Strings))]
        NATOSecret = 1,
        /// <summary>
        /// Audio/Video conference.
        /// </summary>
        [LocalizedDescription("NATOUnclassified", typeof(Strings))]
        NATOUnClassified =0 ,
    }

    /// <summary>
    /// Specifies conference type.
    /// </summary>
    public enum ConferenceType
    {
        /// <summary>
        /// Audio only conference.
        /// </summary>
        [LocalizedDescription("ConferenceTypeAudioOnly", typeof(Strings))]
        AudioOnly = 6,
        /// <summary>
        /// Audio/Video conference.
        /// </summary>
        [LocalizedDescription("ConferenceTypeAudioVideo", typeof(Strings))]
        AudioVideo = 2,
        /// <summary>
        /// Point-To-Point conference.
        /// </summary>
        [LocalizedDescription("ConferenceTypePointToPoint", typeof(Strings))]
        PointToPoint = 4,
        /// <summary>
        /// Room conference.
        /// </summary>
        [LocalizedDescription("ConferenceTypeRoomConference", typeof(Strings))]
        RoomConference = 7,
		/// <summary>
		/// Hotdesking conference.
		/// </summary>
		[LocalizedDescription("ConferenceTypeHotdesking", typeof(Strings))]
		Hotdesking = 8,
		/// <summary>
		/// VMR conference.
		/// </summary>
		[LocalizedDescription("ConferenceTypeVMRConference", typeof(Strings))]
		VMR = 10
    }

	public enum PCConferenceVendor
	{
		[LocalizedDescription("TxtNone", typeof(Strings))]
		None = 0,

		[LocalizedDescription("TxtBlueJeans", typeof(Strings))]
		BlueJeans = 1,
		
		[LocalizedDescription("TxtJabber", typeof(Strings))]
		Jabber = 2,

		[LocalizedDescription("TxtLinc", typeof(Strings))]
		Linc = 3,

		[LocalizedDescription("TxtVidtel", typeof(Strings))]
		Vidtel = 4
	}

	public enum VMRConferenceType
	{
		[LocalizedDescription("TxtNone", typeof(Strings))]
		None = 0,

		[LocalizedDescription("TxtPersonal", typeof(Strings))]
		Personal = 1,

		[LocalizedDescription("TxtRoom", typeof(Strings))]
		Room = 2,

		[LocalizedDescription("TxtExternal", typeof(Strings))]
		External = 3
	}

	public enum ConnectionTypes
	{
		[LocalizedDescription("ConnectionTypeDialinToMCU", typeof(Strings))]
		DialIn = 1,
		[LocalizedDescription("ConnectionTypeDialoutFromMCU", typeof(Strings))]
		DialOut = 2,
		[LocalizedDescription("ConnectionTypeDirectToMCU", typeof(Strings))]
		Direct = 3
	}

    /// <summary>
    /// Represents a conference in myVRM Web Service.
    /// </summary>
    [ServiceObjectDefinition("confInfo")]
    public abstract class ConferenceBase : ServiceObject
    {
    	public int MaxConfDuration = 24; //in hours

        private string _iCalendarUniqueId;
        private OccurrenceInfoCollection _modifiedOccurrences;
    	//public bool IsUtcEnabled { private set; get; } 

		void SetDefaults()
		{
			Origin = ConferenceOrigin.Api;
			IsOpenForPublicRegistration = Service.OrganizationOptions.IsDynamicInviteEnabled;
			IsPublic = Service.OrganizationOptions.IsDefaultConferenceAsPublic;
			Type = Service.OrganizationOptions.DefaultConferenceType;
			IsRecurring = false;
            Duration = TimeSpan.FromMinutes(0);//TimeSpan.FromMinutes(Service.OrganizationOptions.DefaultConfDuration); //TimeSpan.FromMinutes(0);  //ZD 102200
			TimeZone = TimeZoneInfo.FindSystemTimeZoneById(TimeZoneInfo.Local.Id);
		}
        /// <summary>
        /// Creates a new instance of the <see cref="Conference"/> class.
        /// </summary>
        protected ConferenceBase(MyVrmService service) : base(service)
        {
        	SetDefaults();
        	/* Origin = ConferenceOrigin.Api;
            IsOpenForPublicRegistration = Service.OrganizationOptions.IsDynamicInviteEnabled;
            IsPublic = Service.OrganizationOptions.IsDefaultConferenceAsPublic;
            Type = Service.OrganizationOptions.DefaultConferenceType;
            IsRecurring = false;
            Duration = TimeSpan.FromMinutes(0);
            TimeZone = TimeZoneInfo.FindSystemTimeZoneById(TimeZoneInfo.Local.Id);
        	//IsUtcEnabled = service.IsUtcEnabled;
		    * */
        }

		internal ConferenceBase(MyVrmService service, PropertyBag propertyBag)
			: base(service, propertyBag)
		{
		}

    	/// <summary>
        /// Binds to the existing conference.
        /// </summary>
        /// <param name="service">The instance of <see cref="MyVrmService"/>.</param>
        /// <param name="id">The identifier of the conference.</param>
        /// <returns>The instance of <see cref="Conference"/>.</returns>
		public static ConferenceBase Bind(MyVrmService service, ConferenceId id)
        {
            var conference = service.GetConference(id);
            if (conference.IsRecurring)
            {
				if (conference.RecurrenceRange != null)
					conference.RecurrenceRange.SetupRecurrence(conference.RecurrencePattern);
                conference.RecurrencePattern.StartTime = conference.AppointmentTime.StartTime;
                conference.RecurrencePattern.EndTime = conference.AppointmentTime.EndTime;
                conference.StartDate = conference.RecurrencePattern.StartDate +
                                       conference.RecurrencePattern.StartTime;
                conference.Duration = conference.AppointmentTime.Duration;
            }
            return conference;
        }

        /// <summary>
        /// Gets conference ID.
        /// </summary>
        public ConferenceId Id
        {
            get
            {
                return (ConferenceId)PropertyBag[GetIdPropertyDefinition()];
            }
            internal set
            {
                PropertyBag[GetIdPropertyDefinition()] = value;
            }
        }
        /// <summary>
        /// Gets conference unique ID. It can be searched on in the web site.
        /// </summary>
        public ConferenceId UniqueId
        {
            get
            {
                return (ConferenceId)PropertyBag[ConferenceBaseSchema.UniqueId];
            }
        }
        /// <summary>
        /// Gets or sets conference name.
        /// </summary>
        public string Name
        {
            get
            {
                return (string)PropertyBag[ConferenceBaseSchema.Name];
            }
            set
            {
                var str = RemoveInvalidCharacters(value);
                if (string.Compare(str, Name, StringComparison.InvariantCultureIgnoreCase) != 0)
                {
                    PropertyBag[ConferenceBaseSchema.Name] = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets conference DialIn.
        /// </summary>
        public string ConfDialIn
        {
            get
            {
                return (string)PropertyBag[ConferenceBaseSchema.ConfDialInNum];
            }
            set
            {

                PropertyBag[ConferenceBaseSchema.ConfDialInNum] = value;

            }
        }
        /// <summary>
        /// Gets or sets user id of the conference host.
        /// </summary>
        public UserId HostId
        {
            get
            {
                return (UserId)PropertyBag[ConferenceBaseSchema.HostId];
            }
            set
            {
                PropertyBag[ConferenceBaseSchema.HostId] = value;
            }
        }
        /// <summary>
        /// Gets name of the conference host.
        /// </summary>
        public string HostName
        {
            get
            {
                return (string)PropertyBag[ConferenceBaseSchema.HostName];
            }
        }
        /// <summary>
        /// Gets email of the conference host.
        /// </summary>
        public string HostEmail
        {
            get
            {
                return (string)PropertyBag[ConferenceBaseSchema.HostEmail];
            }
        }
        /// <summary>
        /// Conference origin.
        /// </summary>
        public ConferenceOrigin Origin
        {
            get
            {
                return (ConferenceOrigin)PropertyBag[ConferenceBaseSchema.Origin];
            }
            set
            {
                PropertyBag[ConferenceBaseSchema.Origin] = value;
            }
        }

    	/// <summary>
    	/// Gets or sets conference type.
    	/// </summary>
    	public abstract ConferenceType Type
    	{   
    		get;
    		set;
    	}
		
        /// <summary>
        /// Gets or sets conference password.
        /// </summary>
        public string Password
        {
            get
            {
                return (string)PropertyBag[ConferenceBaseSchema.Password];
            }
            set
            {
                PropertyBag[ConferenceBaseSchema.Password] = value;
            }
        }
        /// <summary>
        /// Gets or sets Boolean value that specifies whether conference is immediate.
        /// </summary>
        public bool IsImmediate
        {
            get
            {
            	bool bRet = false;
				if (PropertyBag[ConferenceBaseSchema.IsImmediate] != null)
            		bool.TryParse(PropertyBag[ConferenceBaseSchema.IsImmediate].ToString(), out bRet);
            	return bRet;
            	//PropertyBag[ConferenceBaseSchema.IsImmediate] == null ? false : (bool)(PropertyBag[ConferenceBaseSchema.IsImmediate]);
            }
            set
            {
                PropertyBag[ConferenceBaseSchema.IsImmediate] = value;
            }
        }
        /// <summary>
        /// Gets or sets Boolean value that specifies whether conference is recurring.
        /// </summary>
        public bool IsRecurring
        {
            get
            {
				bool bRet = false;
				if (PropertyBag[ConferenceBaseSchema.IsRecurring] != null)
					bool.TryParse(PropertyBag[ConferenceBaseSchema.IsRecurring].ToString(), out bRet);
				return bRet;//(bool)PropertyBag[ConferenceBaseSchema.IsRecurring];
            }
            set
            {
                if (!Service.OrganizationOptions.IsRecurringConferenceEnabled && value)
                {
                    throw new FeatureNotSupportedException(Strings.RecurringConferenceFeature);
                }
	            PropertyBag[ConferenceBaseSchema.IsRecurring] = value;
                if (!value)
                {
                    // Reset appointment time for non-recurring conference
                    AppointmentTime = null;
                    //??RecurrencePattern = null;
                    RecurrenceRange = null;
                }
            }
        }
        /// <summary>
        /// Gets or sets recurring text.
        /// </summary>
        public string RecurringText
        {
            get
            {
                return (string)PropertyBag[ConferenceBaseSchema.RecurringText];
            }
            set
            {
                PropertyBag[ConferenceBaseSchema.RecurringText] = value;
            }
        }

        /// <summary>
        /// Gets or sets recurrence pattern.
        /// </summary>
        public RecurrencePattern RecurrencePattern
        {
            get
            {
                return (RecurrencePattern)PropertyBag[ConferenceBaseSchema.RecurrencePattern];
            }
            set
            {
                IsRecurring = value != null;
                PropertyBag[ConferenceBaseSchema.RecurrencePattern] = value;
            }
        }

        /// <summary>
        /// Gets a list of modified occurrences for this meeting.
        /// </summary>
        public OccurrenceInfoCollection ModifiedOccurrences
        {
            get { return _modifiedOccurrences ?? (_modifiedOccurrences = new OccurrenceInfoCollection()); }
        }

        

        /// <summary>
        /// Gets or sets conference start date/time. For single instance conference only.
        /// </summary>
        public DateTime StartDate
        {
            get
            {

                var startDate = string.IsNullOrEmpty((string) PropertyBag[ConferenceBaseSchema.StartDate])
                                    ? Utilities.DateToString(DateTime.Today) : (string) PropertyBag[ConferenceBaseSchema.StartDate];
                var startHour = string.IsNullOrEmpty((string) PropertyBag[ConferenceBaseSchema.StartHour])
                                    ? "00" : (string) PropertyBag[ConferenceBaseSchema.StartHour];
                var startMinute = string.IsNullOrEmpty((string)PropertyBag[ConferenceBaseSchema.StartMin])
                                    ? "00" : (string)PropertyBag[ConferenceBaseSchema.StartMin];
                var startSet = string.IsNullOrEmpty((string)PropertyBag[ConferenceBaseSchema.StartSet])
                                    ? "AM" : (string)PropertyBag[ConferenceBaseSchema.StartSet];
                var format = string.Format("{0} {1}:{2} {3}", startDate, startHour, startMinute, startSet);
            	return DateTime.ParseExact(format,
            	                           "M/d/yyyy hh:mm tt", Utilities.CultureProvider, DateTimeStyles.AssumeLocal);// IsUtcEnabled ? DateTimeStyles.AssumeUniversal : DateTimeStyles.AssumeLocal);
            }
            set
            {
                PropertyBag[ConferenceBaseSchema.StartDate] = value.Date.ToString("M/d/yyyy", Utilities.CultureProvider);
                PropertyBag[ConferenceBaseSchema.StartHour] = (value.Hour > 12 ? value.Hour - 12 : value.Hour).ToString("D2");
                PropertyBag[ConferenceBaseSchema.StartMin] = value.Minute.ToString("D2");
                PropertyBag[ConferenceBaseSchema.StartSet] = value.ToString("tt", Utilities.CultureProvider);
                //PropertyBag[ConferenceBaseSchema.Duration] = TimeSpan.FromMinutes(Service.OrganizationOptions.DefaultConfDuration);
                //Duration = TimeSpan.FromMinutes(Service.OrganizationOptions.DefaultConfDuration); 
            }
        }
        /// <summary>
        /// Gets or sets conference end date/time. For single instance conference only.
        /// </summary>;
        public DateTime EndDate
        {
            get
            {
               // return StartDate + TimeSpan.FromMinutes(Service.OrganizationOptions.DefaultConfDuration); //Duration;
                //MyVrmService.Service.OrganizationOptions.DefaultConfDuration
                return StartDate + Duration;
            }
            set
            {
                 Duration = value - StartDate;    
                //Duration = TimeSpan.FromMinutes(Service.OrganizationOptions.DefaultConfDuration);  
            }
        }
        /// <summary>
        /// Gets or sets timezone.
        /// </summary>
        public TimeZoneInfo TimeZone
        {
            get
            {
                return (TimeZoneInfo)PropertyBag[ConferenceBaseSchema.TimeZone];
            }
            set
            {
                PropertyBag[ConferenceBaseSchema.TimeZone] = value;
            }
        }
        /// <summary>
        /// Gets or sets conference duration.
        /// </summary>
        public TimeSpan Duration
        {
            get
            {
               return (TimeSpan)PropertyBag[ConferenceBaseSchema.Duration];
            }
            set
            {
                //if( RecurrencePattern.TimeZone == "{(UTC-05:00) Eastern Time (US & Canada)}")
               //if(ConferenceBaseSchema.TimeZone.Name != null && ConferenceBaseSchema.TimeZone.Name == "(UTC-05:00) Eastern Time (US & Canada)" )
               // {
                    if (value < TimeSpan.Zero)
                    {
                        throw new Exception(Strings.ConferenceEndDateOccursBeforeStartDate);
                    } 
               // }//ZD 102305
				if (value.TotalHours > MaxConfDuration)
				{
					string str = string.Format(Strings.ConferenceDurationIsExceeded, MaxConfDuration);
					throw new Exception(str);
				}
                PropertyBag[ConferenceBaseSchema.Duration] = value;
            }
        }
        /// <summary>
        /// Gets or sets conference desscription.
        /// </summary>
        public string Description
        {
            get
            {
                return (string)PropertyBag[ConferenceBaseSchema.Description];
            }
            set
            {
                var str = RemoveInvalidCharacters(value);
                if (str == null && Description == "N/A")
                    return;
                if (string.Compare(str, Description, StringComparison.InvariantCultureIgnoreCase) != 0)
                {
                    PropertyBag[ConferenceBaseSchema.Description] = value;
                }
            }
        }

       
        /// <summary>
        /// Gets conference rooms.
        /// </summary>
        public RoomIdCollection Rooms
        {
            get
            {
                return (RoomIdCollection) PropertyBag[ConferenceBaseSchema.Rooms];
            }
        }
        /// <summary>
        /// Gets or sets Boolean value that specifies whether the conference is public.
        /// </summary>
        public bool IsPublic
        {
            get
            {
				bool bRet = false;
				if (PropertyBag[ConferenceBaseSchema.IsPublic] != null)
					bool.TryParse(PropertyBag[ConferenceBaseSchema.IsPublic].ToString(), out bRet);
				return bRet;//(bool)PropertyBag[ConferenceBaseSchema.IsPublic];
            }
            set
            {
                PropertyBag[ConferenceBaseSchema.IsPublic] = value;
            }
        }
        //ZD 102651 satrt
        /// <summary>
        /// Gets or sets Boolean value that specifies whether the MeetandGreetBuffer
        /// </summary>
        public int MeetandGreetBuffer
        {
            get
            {
                int iRet = 0;
                if (PropertyBag[ConferenceBaseSchema.MeetandGreetBuffer] != null)
                    int.TryParse(PropertyBag[ConferenceBaseSchema.MeetandGreetBuffer].ToString(), out iRet);
                return iRet; //(int)PropertyBag[ConferenceBaseSchema.ModifyType];
            }
            set
            {
                PropertyBag[ConferenceBaseSchema.MeetandGreetBuffer] = value;
            }
        }
        //ZD 102651 End
        /// <summary>
        /// Gets or sets Boolean value that specifies whether the conference is open for public registration.
        /// </summary>
        public bool IsOpenForPublicRegistration
        {
            get
            {
				bool bRet = false;
				if (PropertyBag[ConferenceBaseSchema.IsOpenForPublicRegistration] != null)
					bool.TryParse(PropertyBag[ConferenceBaseSchema.IsOpenForPublicRegistration].ToString(), out bRet);
				return bRet;//(bool)PropertyBag[ConferenceBaseSchema.IsOpenForPublicRegistration];
            }
            set
            {
                if(!Service.OrganizationOptions.IsDynamicInviteEnabled && value)
                {
                    throw new FeatureNotSupportedException(Strings.DynamicInviteFeature);
                }
                PropertyBag[ConferenceBaseSchema.IsOpenForPublicRegistration] = value;
            }
        }
        /// <summary>
        /// Gets conference participants.
        /// </summary>
        public ParticipantCollection Participants
        {
            get
            {
                return (ParticipantCollection) PropertyBag[ConferenceBaseSchema.Participants];
            }
        }
        // ZD 101224 starts
        /// <summary>
        /// GetConfroom collections
        /// </summary>
        public ConfGuestRoomCollection ConfGuestRooms
        {
            get
            {
                return (ConfGuestRoomCollection)PropertyBag[ConferenceBaseSchema.ConfGuestRooms];
            }
        }
        // ZD 101224 Ends

    	/// <summary>
    	/// Gets conference participants.
    	/// </summary>
		public ParticipantCollection ExternalParticipants = new ParticipantCollection();
      
		//{
		//    get
		//    {
		//        if
		//    //    ParticipantCollection ret = new ParticipantCollection(); ;
		//    //    foreach (var part in Participants)
		//    //    {
		//    //        if(part.InvitationMode == ParticipantInvitationMode.External)
		//    //            ret.Add(part);
		//    //    }
		//    //    return ret;
		//    }
		//    set; }
        /// <summary>
        /// Gets list of custom attributes.
        /// </summary>
        public ConfGuestRoomCollection externalConfRooms = new ConfGuestRoomCollection(); //101224 zd


        public BridgeSchema BridgesList
        {
            get
            {
                return (BridgeSchema)PropertyBag[ConferenceBaseSchema.BridgeList];
            }
        }
        public BridgeSchema BridgesLists = new BridgeSchema(); //101224 zd

        public CustomAttributeCollection CustomAttributes
        {
            get
            {
                return (CustomAttributeCollection) PropertyBag[ConferenceBaseSchema.CustomAttributes];
            }
        }

        /// <summary>
        /// Gets or sets advanced audio/video parameters.
        /// </summary>
        public AdvancedAudioVideoParameters AudioVideoParameters
        {
            get
            {
                return (AdvancedAudioVideoParameters)PropertyBag[ConferenceBaseSchema.AudioVideoParameters];
            }
            set
            {
                PropertyBag[ConferenceBaseSchema.AudioVideoParameters] = value;
            }
        }

        /// <summary>
        /// Deletes this conference.
        /// </summary>
        public virtual void Delete()
        {
			//InventoryWorkOrders.Clear();
			//InventoryWorkOrders.Save();
			//CateringWorkOrders.Clear();
			//CateringWorkOrders.Save();
			//HouseKeepingWorkOrders.Clear();
			//HouseKeepingWorkOrders.Save();
            Service.DeleteConference(Id);
        }

        internal AppointmentTime GetAppointmentTime()
        {
            var apptTime = new AppointmentTime
                               {
                                   StartTime = RecurrencePattern.StartTime,
                                   EndTime = RecurrencePattern.EndTime,
                                   TimeZone = RecurrencePattern.TimeZone,
                                   SetupDuration = TimeSpan.Zero,
                                   TeardownDuration = TimeSpan.Zero
                               };
            // Add modified occurrences, if any
            apptTime.ModifiedOccurrences.InternalAddRange(ModifiedOccurrences);
            return apptTime;
        }

        public void UpdateCalendarUniqueId(string uId)
        {
            if (!string.Equals(uId, _iCalendarUniqueId, StringComparison.Ordinal))
            {
                Service.UpdateConferenceICalUniqueId(Id, uId);
                _iCalendarUniqueId = uId;
            }
        }

        internal int ModifyType
        {
            get
            {
            	int iRet = 0;
				if (PropertyBag[ConferenceBaseSchema.ModifyType] != null)
					int.TryParse(PropertyBag[ConferenceBaseSchema.ModifyType].ToString(), out iRet);
            	return iRet; //(int)PropertyBag[ConferenceBaseSchema.ModifyType];
            }
            set
            {
                PropertyBag[ConferenceBaseSchema.ModifyType] = value;
            }
        }

        internal override ServiceObjectSchema GetSchema()
        {
            return ConferenceBaseSchema.Instance;
        }

        internal override PropertyDefinition GetIdPropertyDefinition()
        {
            return ConferenceBaseSchema.Id;
        }

        internal static string RemoveInvalidCharacters(string str)
        {
            return string.IsNullOrEmpty(str) ? str : Regex.Replace(str, Constants.InvalidConferenceNameRegularExpression, "");
        }

       public AppointmentTime AppointmentTime
        {
            get { return (AppointmentTime)PropertyBag[ConferenceBaseSchema.AppointmentTime]; }
            set { PropertyBag[ConferenceBaseSchema.AppointmentTime] = value; }
        }

        private RecurrenceRange RecurrenceRange
        {
            get { return (RecurrenceRange)PropertyBag[ConferenceBaseSchema.RecurrenceRange]; }
            set { PropertyBag[ConferenceBaseSchema.RecurrenceRange] = value; }
        }
		/**/
		public bool IsCloudConferencing
        {
            get
            {
				bool bRet = false;
				if (PropertyBag[ConferenceBaseSchema.IsCloudConferencing] != null)
					bool.TryParse(PropertyBag[ConferenceBaseSchema.IsCloudConferencing].ToString(), out bRet);
				return bRet;
            }
            set
            {
				PropertyBag[ConferenceBaseSchema.IsCloudConferencing] = value;
            }
        }

		public bool IsPCconference
        {
            get
            {
				bool bRet = false;
				if (PropertyBag[ConferenceBaseSchema.IsPCconference] != null)
					bool.TryParse(PropertyBag[ConferenceBaseSchema.IsPCconference].ToString(), out bRet);
				return bRet;
            }
            set
            {
                PropertyBag[ConferenceBaseSchema.IsPCconference] = value;
            }
        }

		public VMRConferenceType VMRType
        {
            get
            {
				VMRConferenceType ret = VMRConferenceType.None;
            	int iRet = 0;
				if (PropertyBag[ConferenceBaseSchema.VMRType] != null)
				{
					int.TryParse(PropertyBag[ConferenceBaseSchema.VMRType].ToString(), out iRet);
					ret = (VMRConferenceType) iRet;
				}
				return ret; 
            }
            set
            {
				PropertyBag[ConferenceBaseSchema.VMRType] = (int)value;
            }
        }
		public PCConferenceVendor PCVendorId
        {
            get
            {
            	PCConferenceVendor ret = PCConferenceVendor.None;
				int iRet = 0;
				if (PropertyBag[ConferenceBaseSchema.PCVendorId] != null)
				{
					int.TryParse(PropertyBag[ConferenceBaseSchema.PCVendorId].ToString(), out iRet);
					ret = (PCConferenceVendor)iRet;
				}
            	return ret; 
            }
            set
            {
				PropertyBag[ConferenceBaseSchema.PCVendorId] = (int)value;
            }
        }

		public NetworkSwitchingOptions IsSecured
		{
			get
			{
                NetworkSwitchingOptions nwkSwitchOption = NetworkSwitchingOptions.NATOSecret;
                int iRet = 0;
                if (PropertyBag[ConferenceBaseSchema.IsSecured] != null)
                {
                    int.TryParse(PropertyBag[ConferenceBaseSchema.IsSecured].ToString(), out iRet);
                    if (iRet == 1)
                        nwkSwitchOption = NetworkSwitchingOptions.NATOSecret;
                    else
                        nwkSwitchOption = NetworkSwitchingOptions.NATOUnClassified;
                }
                else
                    nwkSwitchOption = NetworkSwitchingOptions.NATOSecret;
                return nwkSwitchOption; 
				
			}
			set
			{

                PropertyBag[ConferenceBaseSchema.IsSecured] = (int)value ;
                
                
			}
		}

    	public bool IsSecuredEnabledForUser;

		public ConciergeSupportParams ConciergeSupportParams
        {
            get
            {
				return (ConciergeSupportParams)PropertyBag[ConferenceBaseSchema.ConciergeSupport];
            }
            set
            {
				PropertyBag[ConferenceBaseSchema.ConciergeSupport] = value;
            }
        }
		public ConfMessageCollection ConfMessageCollection
		{
			get
			{
				return (ConfMessageCollection)PropertyBag[ConferenceBaseSchema.ConfMessageCollection];
			}
			set
			{
				PropertyBag[ConferenceBaseSchema.ConfMessageCollection] = value;
			}
		}
    	/**/
     

    }
}
