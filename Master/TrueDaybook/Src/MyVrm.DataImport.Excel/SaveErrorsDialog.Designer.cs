﻿namespace MyVrm.DataImport.Excel
{
    partial class SaveErrorsDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.closeButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.errorTextLabel = new System.Windows.Forms.Label();
            this.errorsListView = new MyVrm.DataImport.Excel.WinForms.DataListView();
            this.itemNameColumn = new MyVrm.DataImport.Excel.WinForms.ColumnHeader();
            this.errorColumn = new MyVrm.DataImport.Excel.WinForms.ColumnHeader();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // closeButton
            // 
            this.closeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.closeButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.closeButton.Location = new System.Drawing.Point(547, 419);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(75, 23);
            this.closeButton.TabIndex = 4;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(198, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Review and resolve the following issues.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Unsaved items:";
            // 
            // errorTextLabel
            // 
            this.errorTextLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.errorTextLabel.AutoSize = true;
            this.errorTextLabel.Location = new System.Drawing.Point(12, 271);
            this.errorTextLabel.Name = "errorTextLabel";
            this.errorTextLabel.Size = new System.Drawing.Size(49, 13);
            this.errorTextLabel.TabIndex = 3;
            this.errorTextLabel.Text = "<details>";
            // 
            // errorsListView
            // 
            this.errorsListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.errorsListView.Columns.AddRange(new MyVrm.DataImport.Excel.WinForms.ColumnHeader[] {
            this.itemNameColumn,
            this.errorColumn});
            this.errorsListView.DataMember = "";
            this.errorsListView.DataSource = null;
            this.errorsListView.FullRowSelect = true;
            this.errorsListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.errorsListView.HideSelection = false;
            this.errorsListView.Location = new System.Drawing.Point(13, 61);
            this.errorsListView.MultiSelect = false;
            this.errorsListView.Name = "errorsListView";
            this.errorsListView.Size = new System.Drawing.Size(609, 184);
            this.errorsListView.TabIndex = 2;
            this.errorsListView.UseCompatibleStateImageBehavior = false;
            this.errorsListView.View = System.Windows.Forms.View.Details;
            this.errorsListView.SelectedIndexChanged += new System.EventHandler(this.errorsListView_SelectedIndexChanged);
            // 
            // itemNameColumn
            // 
            this.itemNameColumn.DisplayMember = "ItemName";
            this.itemNameColumn.Format = null;
            this.itemNameColumn.FormatInfo = null;
            this.itemNameColumn.IsSortable = false;
            this.itemNameColumn.NullText = "(null)";
            this.itemNameColumn.Text = "Name";
            // 
            // errorColumn
            // 
            this.errorColumn.DisplayMember = "ErrorText";
            this.errorColumn.Format = null;
            this.errorColumn.FormatInfo = null;
            this.errorColumn.IsSortable = false;
            this.errorColumn.NullText = "(null)";
            this.errorColumn.Text = "Error";
            this.errorColumn.Width = 200;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 248);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Details:";
            // 
            // SaveErrorsDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.closeButton;
            this.ClientSize = new System.Drawing.Size(634, 454);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.errorTextLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.errorsListView);
            this.Controls.Add(this.closeButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MinimizeBox = false;
            this.Name = "SaveErrorsDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Save Errors";
            this.Load += new System.EventHandler(this.SaveErrorsDialog_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button closeButton;
        private MyVrm.DataImport.Excel.WinForms.DataListView errorsListView;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private MyVrm.DataImport.Excel.WinForms.ColumnHeader itemNameColumn;
        private MyVrm.DataImport.Excel.WinForms.ColumnHeader errorColumn;
        private System.Windows.Forms.Label errorTextLabel;
        private System.Windows.Forms.Label label3;
    }
}