﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Office.Interop.Outlook;
using Microsoft.Office.Tools.Ribbon;
using MyVrm.Outlook;
using MyVrm.Outlook.WinForms;
using MyVrm.Outlook.WinForms.ApprovePendings;
using MyVrm.Outlook.WinForms.Calendar;
using MyVrm.Outlook.WinForms.CheckSeats;
using MyVrm.Outlook.WinForms.Conference;
using MyVrm.Outlook.WinForms.ManageRooms;
using MyVrm.Outlook.WinForms.Options;
using MyVrm.Outlook.WinForms.Templates;
using MyVrm.WebServices.Data;
using MyVrm.WebServices.Data.TrueDaybook;
using stdole;
using Exception = System.Exception;
using Microsoft.Office.Tools.Outlook;
using Application = Microsoft.Office.Interop.Outlook.Application;
using Document = Microsoft.Office.Interop.Outlook.Extensions.Linq.Document;
using Image = System.Drawing.Image;
using DevExpress.XtraRichEdit;

namespace MyVrmAddin2007
{
    public partial class ConferenceRibbon : RibbonBase
    {
		public const string VirtualConfPropName = "IsVConf";
		public const string VirtualConfIdPropName = "VConfId";
		public const string VirtualConfIsRevertedPropName = "VConfIsReverted";
		public const string VirtualConfiCalSent = "iCalSent";

    	private string selectedTemplateID;
        public ConferenceRibbon()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        	selectedTemplateID = string.Empty;

			//Texts localization
        	generalGroup.Label = Strings.General;
			roomsPrivateCalendarButton.Label = Strings.RoomsPrivateCalendar;
			roomsPrivateCalendarButton.SuperTip = Strings.RoomsPrivateCalendarButtonTip;
			roomsPublicCalendarButton.Label = Strings.RoomsPublicCalendar;
			roomsPublicCalendarButton.SuperTip = Strings.RoomsPublicCalendarButtonTip;
        	templatesGroup.Label = Strings.Templates;
        	templateMenu.Label = Strings.CreateFromTemplate;
			tdTemplateMenu.Label = Strings.CreateFromTemplate;
            templateMenu.SuperTip = Strings.CreateFromTemplateButtonTip;
			tdTemplateMenu.SuperTip = Strings.CreateFromTemplateButtonTip;
        	saveAsTemplateBn.Label = Strings.SaveAsTemplate;
            saveAsTemplateBn.SuperTip = Strings.SaveAsTeamplateButtonTip;
        	preferencesGroup.Label = Strings.Preferences;
        	favoriteRoomsButton.Label = Strings.FavoriteRooms;
            favoriteRoomsButton.SuperTip = Strings.FavoritRoomsButtonTip;
        	groupShow.Label = Strings.Show;
        	appointmentButton.Label = Strings.Appointment;
            appointmentButton.SuperTip = Strings.AppointmentButtonTip;
			conferenceButton.Label = Strings.Conference;
            conferenceButton.SuperTip = Strings.ConferenceButtonTip;
        	optionsGroup.Label = Strings.OptionsMenuPoint;
			optionsButton.Label = Strings.OptionsMenuPoint;
            optionsButton.SuperTip = Strings.OptionsButtonTip;
			manageRoomButton.Label = Strings.ManageRoomsMenuPoint;
			manageRoomButton.SuperTip = Strings.ManageRoomsMenuPointTip;

			bnVirtualConference.Label = Strings.Convert2VirtMenuItemTxt;
			bnVirtualConference.ScreenTip = Strings.Convert2VirtMenuItemTipTxt;
        }

        private void ConferenceRibbon_Load(object sender, RibbonUIEventArgs e)
        {
			tabMyVRM.Label = MyVrmAddin.ProductDisplayName;
            //tabMyVRM.Visible = MyVrmAddin.Instance.IsLicenseAgreementAccepted;
			conferenceButton.Image = (Image)MyVrmAddin.Logo65;
        	DisEnable();
			//**********
			//tabMyVRM.Label = "myVRM Virtual Conference";
			tabMyVRM.Visible = false;
			groupShow.Visible = false;
			templatesGroup.Visible = true;//false;
			generalGroup.Visible = false;
			preferencesGroup.Visible = false;
			optionsGroup.Visible = false;

        	AppointmentItem appItem = (AppointmentItem) ((Inspector) Context).CurrentItem;
			if (IsVirtualConference(appItem))
			{
				bnCheckSeats.Visible = true;
				bnVirtualConference.Label = Strings.Revert2VirtMenuItemTxt;
				bnVirtualConference.ScreenTip = Strings.Revert2VirtMenuItemTipTxt;
				bnVirtualConference.Image = Properties.Resources.RevertVConf32x32;
			}
			else
			{
				bnCheckSeats.Visible = false;
				bnVirtualConference.Label = Strings.Convert2VirtMenuItemTxt;
				bnVirtualConference.ScreenTip = Strings.Convert2VirtMenuItemTipTxt;
				bnVirtualConference.Image = Properties.Resources.virtualConf32x32;
			}
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }

		public void DisEnable_AfterSave(object sender, EventArgs e)
		{
			DisEnable();

			var inspector = (Inspector)Context;
			var conferenceWrapper = Globals.ThisAddIn.GetConferenceWrapperForAppointment(
					(AppointmentItem)inspector.CurrentItem,false,false);//ZD 101226
			if (conferenceWrapper != null)
				conferenceWrapper.AfterSave -= DisEnable_AfterSave;
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
		}

		private void DisEnable()
		{
			var inspector = (Inspector) Context;
			templateMenu.Enabled = !Globals.ThisAddIn.HasConferenceWrapper((AppointmentItem) inspector.CurrentItem);
			saveAsTemplateBn.Enabled = !templateMenu.Enabled;

			tdTemplateMenu.Enabled = !IsVirtualConference((AppointmentItem)inspector.CurrentItem); //!Globals.ThisAddIn.HasConferenceWrapper((AppointmentItem)inspector.CurrentItem);
			tdSaveAsTemplateBn.Enabled = false;//temporary commented!!!! = !tdTemplateMenu.Enabled;
			tdSaveAsTemplateBn.Visible = false;
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
		}

    	private void roomsCalendarButton_Click(object sender, RibbonControlEventArgs e)
    	{
			roomsCalendarButton_OnClick(sender == roomsPrivateCalendarButton);
    	}
		
		public static void roomsCalendarButton_OnClick(bool forPrivate)
		{
			using (var calendarDialog = new RoomsCalendarDialog())
			{
				calendarDialog.RoomMode = forPrivate ? RoomsCalendarDialog.RoomDisplayMode.Private : RoomsCalendarDialog.RoomDisplayMode.Public ;
				calendarDialog.ShowDialog();
			}
		}

    	public static void approvalConferencesButton_OnClick()
		{
			List<ConfInfo> FoundConfs = new List<ConfInfo>();
			ConferenceApprovalDialog.GetConfsForApprove(out FoundConfs);

			if (FoundConfs.Count > 0)
			{
				//Run approval dlg
				using (var dlg = new ConferenceApprovalDialog())
				{
					dlg.FoundConfs = FoundConfs;

					dlg.ShowDialog();
				}
			}
			else
			{
				UIHelper.ShowMessage(Strings.NoConferencesToApproveMsg, MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}
		private void saveAsTemplateBn_Click(object sender, RibbonControlEventArgs e)
		{
			return;

			var inspector = (Inspector) Context;
			//OutlookAppointmentImpl apptImpl;
			var conferenceWrapper = Globals.ThisAddIn.GetConferenceWrapperForAppointment((AppointmentItem)inspector.CurrentItem,false,false); //ZD 101226
			using (var editTemplateDialog = new SaveAsConferenceTemplateDialog(conferenceWrapper))
			{
				editTemplateDialog.ShowDialog();
			}
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
		}
        public  void templateMenu_OnItemsLoading(RibbonMenu menu, RibbonControlEventHandler handler, string selected)
        {
            GetTemplateListResponse ret = null;

            var cursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                ret = MyVrmService.Service.GetTemplateList(GetTemplateListResponse.SortByType.ByName);
            }
            catch (Exception exception)
            {
                MyVrmAddin.TraceSource.TraceException(exception, true);
                UIHelper.ShowMessage(exception.Message, MessageBoxButtons.OK,
                                         MessageBoxIcon.Exclamation);
            }
            finally
            {
                Cursor.Current = cursor;
            }

            if (ret != null && ret.templateList != null)
            {
                menu.Items.Clear();

                foreach (TemplateInfo templateInfo in ret.templateList)
                {
                    RibbonToggleButton rt = this.Factory.CreateRibbonToggleButton();
                    rt.Label = templateInfo.Name;
                    rt.Visible = true;
                    rt.Tag = templateInfo.ID;
                    rt.Enabled = true;
                    rt.Checked = selected == templateInfo.ID;
                    rt.Click += handler;

                    menu.Items.Add(rt);
                }
            }
        }
		private void templateMenu_ItemsLoading(object sender, RibbonControlEventArgs e)
		{
			templateMenu_OnItemsLoading(templateMenu, checkBox_Click, selectedTemplateID);
		}
		private void tdTemplateMenu_ItemsLoading(object sender, RibbonControlEventArgs e)
		{
			templateMenu_OnItemsLoading(tdTemplateMenu, tdCheckBox_Click, selectedTemplateID);
		}

		static public GetTemplateResponse checkBox_OnClick(object sender, string selected)
		{
			RibbonToggleButton rb = (RibbonToggleButton)sender;
			selected = string.Empty;
			if (rb.Checked)
				selected = rb.Tag.ToString();

			if (selected.Trim().Length == 0 || int.Parse(selected) <= 0)
				return null;

			GetTemplateResponse response = null;
			var cursor = Cursor.Current;
			Cursor.Current = Cursors.WaitCursor;

			try
			{
				response = MyVrmService.Service.GetTemplate(int.Parse(selected));
			}
			catch (Exception exception)
			{
				MyVrmAddin.TraceSource.TraceException(exception, true);
				UIHelper.ShowMessage(exception.Message, MessageBoxButtons.OK,
										 MessageBoxIcon.Exclamation);
			}
			finally
			{
				Cursor.Current = cursor;
			}
			return response;
		}

		private void checkBox_Click(object sender, RibbonControlEventArgs e)
		{
			GetTemplateResponse response = checkBox_OnClick(sender, selectedTemplateID);
			if (response != null)
			{
				var inspector = (Inspector) Context;
				//OutlookAppointmentImpl apptImpl;
				var conferenceWrapper = Globals.ThisAddIn.GetConferenceWrapperForAppointment(
					(AppointmentItem) inspector.CurrentItem,false,false);//ZD 101226

				if (conferenceWrapper != null)
				{
					conferenceWrapper.AfterSave -= DisEnable_AfterSave;
					conferenceWrapper.FillFromTemplate(response.Conference);
					
					conferenceWrapper.AfterSave += DisEnable_AfterSave;
				}
			}
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
		}

		private void tdCheckBox_Click(object sender, RibbonControlEventArgs e)
		{
			TDGetTemplateResponse response = tdCheckBox_OnClick(sender, selectedTemplateID);
			if (response != null)
			{
				//AppointmentItem appItem = (AppointmentItem)Explorer.Application.CreateItem(OlItemType.olAppointmentItem);
				AppointmentItem appItem = (AppointmentItem) (((Inspector) Context)).CurrentItem;
					//(AppointmentItem)Globals.ThisAddIn.Application.CreateItem(OlItemType.olAppointmentItem);
				MyVrmExplorer.Convert2TDBWrap(appItem,false);

				appItem.Body = string.IsNullOrEmpty(response.tdbTemplateInfo.Body) ? appItem.Body : response.tdbTemplateInfo.Body;
				appItem.Subject = string.IsNullOrEmpty(response.tdbTemplateInfo.Subj) ? appItem.Subject : response.tdbTemplateInfo.Subj;
				appItem.Duration = response.tdbTemplateInfo.Duration > 0 ? response.tdbTemplateInfo.Duration : appItem.Duration;
                //100337 - No need to reset start time to server time at all.
				//appItem.Start = response.tdbTemplateInfo.StartTime.TimeOfDay.Ticks > 0 ?
				//	appItem.Start.Date.AddTicks(response.tdbTemplateInfo.StartTime.TimeOfDay.Ticks) : appItem.Start;


                

                       

				while (appItem.Recipients.Count > 0)
					appItem.Recipients.Remove(appItem.Recipients[appItem.Recipients.Count].Index);

				if (response.tdbTemplateInfo.Participans != null)
				{
					var participantsEmialList = response.tdbTemplateInfo.Participans.Select(participant => participant.Email).ToList();
					foreach (string outlookRecipientMail in participantsEmialList)
					{
						Recipient recipient = appItem.Recipients.Add(outlookRecipientMail);
						bool bret = recipient.Resolve();
						if (!recipient.Resolved)
						{
							appItem.Recipients.Remove(recipient.Index);
						}
					}
				}

				bnCheckSeats.Visible = true;

				bnVirtualConference.Label = Strings.Revert2VirtMenuItemTxt;
				bnVirtualConference.ScreenTip = Strings.Revert2VirtMenuItemTipTxt;
				bnVirtualConference.Image = Properties.Resources.RevertVConf32x32;
				DisEnable();
				//appItem.Display(Type.Missing);


				/////////////////
				//var inspector = (Inspector)Context;
				////OutlookAppointmentImpl apptImpl;
				//var conferenceWrapper = Globals.ThisAddIn.GetConferenceWrapperForAppointment(
				//    (AppointmentItem)inspector.CurrentItem);

				//if (conferenceWrapper != null)
				//{
				//    conferenceWrapper.AfterSave -= DisEnable_AfterSave;
				//    conferenceWrapper.FillFromTemplate(response.Conference);

				//    conferenceWrapper.AfterSave += DisEnable_AfterSave;
				//}
			}
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
		}

		static public TDGetTemplateResponse tdCheckBox_OnClick(object sender, string selected)
		{
			RibbonToggleButton rb = (RibbonToggleButton)sender;
			selected = string.Empty;
			if (rb.Checked)
				selected = rb.Tag.ToString();

			if (selected.Trim().Length == 0 || int.Parse(selected) <= 0)
				return null;

			TDGetTemplateResponse response = null;
			var cursor = Cursor.Current;
			Cursor.Current = Cursors.WaitCursor;

			try
			{
				response = MyVrmService.Service.TDGetTemplateInfo(int.Parse(selected));
			}
			catch (Exception exception)
			{
				MyVrmAddin.TraceSource.TraceException(exception, true);
				UIHelper.ShowMessage(exception.Message, MessageBoxButtons.OK,
										 MessageBoxIcon.Exclamation);
			}
			finally
			{
				Cursor.Current = cursor;
			}
			return response;
		}
		static public void favoriteRoomsButton_OnClick()
		{
			//string selectedRooms = string.Empty;
			var cursor = Cursor.Current;
			Cursor.Current = Cursors.WaitCursor;

			try
			{
				//Read current favorites
				GetPreferedRoomResponse response = MyVrmService.Service.GetPreferedRoom();

				//Run selection dlg
				using (var dlg = new FavoriteRoomSelectionDialog())
				{
					if (response != null)
						dlg.SelectedRooms = response.RoomIds.ToArray();

					if (dlg.ShowDialog() == DialogResult.OK)
					{
						//Store setup favorites
						MyVrmService.Service.SetPreferedRoom(dlg.SelectedRooms);
					}
				}
			}
			catch (Exception exception)
			{
				MyVrmAddin.TraceSource.TraceException(exception, true);
				UIHelper.ShowMessage(exception.Message, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}
			finally
			{
				Cursor.Current = cursor;
			}
		}

		private void favoriteRoomsButton_Click(object sender, RibbonControlEventArgs e)
		{
			favoriteRoomsButton_OnClick();
		}

		private void conferenceButton_Click(object sender, RibbonControlEventArgs e)
		{
			conferenceButton_OnClick((Inspector)Context);
		}

    	

		private void appointmentButton_Click(object sender, RibbonControlEventArgs e)
		{
			try
			{
				var inspector = (Inspector)Context;
				if(inspector != null)
					inspector.SetCurrentFormPage("Appointment");
			}
			catch (Exception exception)
			{
				MyVrmAddin.TraceSource.TraceException(exception);
				UIHelper.ShowError(exception.Message);
			}
		}

		private void approveConferenceButton_Click(object sender, RibbonControlEventArgs e)
		{
			approvalConferencesButton_OnClick();
		}

		private void optionsButton_Click(object sender, RibbonControlEventArgs ev)
		{
            try
            {
                BindingList<UsersDatasource> lstOutlookContacts = new BindingList<UsersDatasource>();//Utils.GetOutlookContacts(Globals.ThisAddIn.Application);
                using (var optDlg = new NewOptionsDlg(lstOutlookContacts))
                {

                    optDlg.ShowDialog();

                }
            }
            catch (Exception ex)
            {
                UIHelper.ShowError(ex.Message);
                MyVrmAddin.TraceSource.TraceException(ex);
            }
		}

		public void manageRoomButton_Click(object sender, RibbonControlEventArgs e)
		{
			manageRoomButton_OnClick();
		}

		static public void manageRoomButton_OnClick()
		{
			GetPrivatePublicRoomIDResponse response = MyVrmService.Service.GetPrivatePublicRoomID();

			using (var manageDlg = new ManageRoomsDialog())
			{
				if (response != null && response.WGRoomList != null && response.WGRoomList.Count > 0)
					manageDlg.SetRoomList(response.WGRoomList);
				manageDlg.ShowDialog();
			}
		}

		public static bool IsVirtualConference(AppointmentItem appItem)
    	{
    		//AppointmentItem appItem = (AppointmentItem)Globals.ThisAddIn.Application.ActiveInspector().CurrentItem;
			//(AppointmentItem)((Inspector)Context).CurrentItem;
			UserProperty found = null;
			if(appItem != null)
				found = appItem.UserProperties.Find(VirtualConfPropName, true);
    		return (found != null ? (bool) found.Value : false);
    	}

    	public static string GetVirtualConferenceId(AppointmentItem appItem)
		{
			UserProperty found = null;
			if (appItem != null)
				found = appItem.UserProperties.Find(VirtualConfIdPropName, true);
			return (found != null ? ((string)found.Value).Trim() : string.Empty);
		}

		public static void SetICalSent(AppointmentItem appItem)
		{
			if (appItem.UserProperties.Find(VirtualConfiCalSent, true) == null)
				appItem.UserProperties.Add(VirtualConfiCalSent, OlUserPropertyType.olInteger, false, OlFormatNumber.olFormatNumberRaw);
			appItem.UserProperties[VirtualConfiCalSent].Value = 1;
		}
		public static int IsICalSent(AppointmentItem appItem)
		{
			UserProperty found = null;
			if (appItem != null)
				found = appItem.UserProperties.Find(VirtualConfiCalSent, true);
			return (found != null ? (int)found.Value : 0);
		}

//        const string rtfText = "{\\rtf1\\ansi\\ansicpg1252\\deff0\\deftab720{\\fonttbl" +
//                                 "{\\f0\\fswiss MS Sans Serif;}{\\f1\\froman\\fcharset2 Symbol;}" +
//                                 "{\\f2\\froman\\fprq2 Times New Roman;}}" +
//                                 "{\\colortbl\\red0\\green0\\blue0;\\red255\\green0\\blue0;}" +
//                                 "\\deflang1033\\horzdoc{\\*\\fchars }{\\*\\lchars }" +
//                                 "\\pard\\plain\\f2\\fs24 Line 1 of \\plain\\f2\\fs24\\cf1" +
//                                 "inserted\\plain\\f2\\fs24  file.\\par }";

		private const string htmlText = "<html><head><meta http-equiv=Content-Type content=\"text/html; charset=windows-1251\">" +
"</head><body >"+
"<P><FONT FACE=\"Times New Roman\"><b>Line 1</b> of</FONT> <FONT COLOR=\"#FF0000\" FACE=\"Times New Roman\">inserted</FONT><FONT FACE=\"Times New Roman\"> file.</FONT></P>" +
"<div><p ><span><o:p>&nbsp;</o:p></span></p><p ><o:p>&nbsp;</o:p></p>" +
"<p ><span ><span> </span></span><b><span >bold<span > <o:p></o:p></span></span></b></p>" +
"<p ><span >link: <a href=\"http://ya.ru/\">http://ya.ru/</a><o:p></o:p></span></p></div></body></html>"+
"<img src=\"c:\\temp\\111.jpg\" style=\"height: 247px; width: 336px\" />"+
"<p > <b style=\"color:Red\" >red bold text</b>  </p>";


		//"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 3.2//EN\">\r\n<HTML>\r\n<HEAD>\r\n<META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=windows-1251\">\r\n<META NAME=\"Generator\" CONTENT=\"MS Exchange Server version 08.00.0681.000\">\r\n<TITLE></TITLE>\r\n</HEAD>\r\n<BODY>\r\n<!-- Converted from text/rtf format -->\r\n\r\n<P ALIGN=LEFT><FONT SIZE=2 FACE=\"Arial\">red</FONT></P>\r\n\r\n<P ALIGN=LEFT><FONT SIZE=2 FACE=\"Arial\"> </FONT><B><FONT SIZE=2 FACE=\"Arial\">bold</FONT></B></P>\r\n\r\n<P ALIGN=LEFT><FONT SIZE=2 FACE=\"Arial\">link:</FONT><U> <FONT COLOR=\"#0000FF\" SIZE=2 FACE=\"Arial\">ya.ru</FONT></U></P>\r\n\r\n</BODY>\r\n</HTML>"

//        public static void ConvertBodyHtml2Rtf(AppointmentItem appItem, string htmlBody)
//        {
//            appItem.MeetingStatus = OlMeetingStatus.olMeeting;

//            RDOSession session = new RDOSession();
//            session.Logon(System.Reflection.Missing.Value, System.Reflection.Missing.Value, false, true, System.Reflection.Missing.Value, false);
//            RDOFolder calendar = session.GetDefaultFolder(rdoDefaultFolders.olFolderCalendar);
//            RDOAppointmentItem rdoAppointment = appItem.GlobalAppointmentID != null ?
//                        (RDOAppointmentItem)session.GetRDOObjectFromOutlookObject(appItem, false) :
//                        (RDOAppointmentItem)calendar.Items.Add(rdoItemType.olAppointmentItem);
//            rdoAppointment.HTMLBody = htmlBody;
////                "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 3.2//EN\"><HTML><HEAD><META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=koi8-r\">"+
////"<META NAME=\"Generator\" CONTENT=\"MS Exchange Server version 08.00.0681.000\"><TITLE></TITLE></HEAD><BODY><!-- Converted from text/rtf format -->" +
////"<P DIR=LTR><SPAN ></SPAN><A HREF=\"http://ya.ru\"><SPAN ><U></U></SPAN><U><SPAN ><FONT COLOR=\"#0000FF\" FACE=\"Calibri\">http://ya.ru</FONT></SPAN></U><SPAN ></SPAN></A><SPAN ></SPAN><SPAN ></SPAN></P>" +
////"<P DIR=LTR><SPAN ></SPAN></P></BODY></HTML>";

////                "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 3.2//EN\"><HTML><HEAD><META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=windows-1251\">" +
////"<META NAME=\"Generator\" CONTENT=\"MS Exchange Server version 08.00.0681.000\"><TITLE>Cloud Video discussion</TITLE></HEAD><BODY><!-- Converted from text/rtf format -->" +
////"<P ALIGN=LEFT><U><FONT COLOR=\"#0000FF\" SIZE=2 FACE=\"Arial\">ya.ru </FONT></U></P>" +
////"<P ALIGN=LEFT><B><FONT SIZE=2 FACE=\"Arial\">Template</FONT></B><FONT SIZE=2 FACE=\"Arial\"> invitation body </FONT></P>" +
////"<P ALIGN=LEFT><B><FONT SIZE=7 FONT COLOR=\"#FF0000\" FACE=\"New Roman\">text</FONT></B></P>" +
////"<H2 style=\"margin-bottom:40\"  ALIGN=LEFT><FONT SIZE=4 FACE=\"Arial\">obtained from the server:</FONT></H2><P> </P>" +
////"<P ALIGN=LEFT><FONT SIZE=2 FACE=\"Arial\">You have been invited to attend a TrueConnect Meeting. At the time of the meeting, please choose an access method described below: Desktop Link: To connect from your desktop, tablet or mobile device, go to <A HREF=\"http://connect.trueuc.com/scopia/mt/trueuc?ID=44500005&autojoin\">http://connect.trueuc.com/scopia/mt/trueuc?ID=44500005&autojoin</A></FONT></P>" +
////"<P ALIGN=LEFT><FONT SIZE=2 FACE=\"Arial\">; For other options (including connecting with presentation only or watching the webcast), go to <A HREF=\"http://connect.trueuc.com/scopia/mt/trueuc?ID=44500005\">http://connect.trueuc.com/scopia/mt/trueuc?ID=44500005</A> H.323 dial-in: To connect from an H.323 IP based videoconferencing device, dial by using one of the following numbers: 44500005@64.16.203.4 or 64.16.203.4##44500005 depending on the make and model of your videoconferencing system. Phone dial-in: To connect from a phone, please dial one of the following numbers: (888) 382-8784 - Toll-Free in the US, Canada and Puerto Rico (678) 894-3263 - Atlanta, GA (202) 684-7797 - Washington, DC At the voice prompt enter your Meeting ID 44500005 followed by the # sign Additional Access Numbers are available in 45 Countries - For a detailed list and dialing instructions go to www.scopiameeting.com/SPECS/SCOPIA_Global_Access_Numbers.pdf </FONT></P></BODY></HTML>";
			
//                //htmlText;
////            rdoAppointment.RTFBody = "{\\rtf1\\ansi\\ansicpg1251\\deff0\\deflang1049{\\fonttbl{\\f0\\fnil\\fcharset0 Calibri;}}"+
////"{\\colortbl ;\\red0\\green0\\blue255;}{\\*\\generator Msftedit 5.41.21.2510;}\\viewkind4\\uc1\\pard\\sa200\\sl276\\slmult1\\lang9\\f0\\fs22{\\field{\\*\\fldinst{HYPERLINK \"http://ya.ru\"}}{\\fldrslt{\\ul\\cf1 http://ya.ru}}}\\f0\\fs22\\par}";
//            object ind = 1;
//            Inspector inspector = appItem.GetInspector;
//            var wordDoc = (Microsoft.Office.Interop.Word.Document)inspector.WordEditor;
//            try
//            {
//                Microsoft.Office.Interop.Word.Selection selection = wordDoc.Windows.get_Item(ref ind).Selection;
				
//                Clipboard.Clear();
//                Clipboard.SetData(DataFormats.Rtf, rdoAppointment.RTFBody);//rtfText); //strRtf);//SetText(strRtf);
				
//                selection.PasteAndFormat(WdRecoveryType.wdFormatOriginalFormatting);
//            }
//            catch (Exception ex)
//            {
//                MyVrmAddin.TraceSource.TraceException(ex);
//            }
//        }
		
		//public static void Convert2()
		//{
		//    using (RichEditControl richEditControl = new RichEditControl())
		//    {
		//        richEditControl.HtmlText = htmlText;
		//            //LoadDocument((@".\teste.htm"), DocumentFormat.Html);
		//        //richEditControl.SaveDocument(@"c:\1\teste.rtf", DocumentFormat.Rtf);
		//    }

		//}

        public static void ConvertAppointment2Virtual(AppointmentItem appItem, bool IsStaticId,bool immediate)//ZD 101226
		{
			if (appItem != null && !IsVirtualConference(appItem))
			{
				/**/
				appItem.MeetingStatus = OlMeetingStatus.olMeeting;
				//var appointmentImpl = Globals.ThisAddIn.Appointments[appItem] ??
				//                      new OutlookAppointmentImpl(appItem);

               

				ConferenceWrapper conWrapper = Globals.ThisAddIn.InitConferenceWrapper(appItem.GetInspector, appItem,IsStaticId,immediate);//ZD 101226
                
                OutlookAppointment outlookAppt = Globals.ThisAddIn.Appointments[appItem] as OutlookAppointment;
                if (outlookAppt != null)
                    outlookAppt.RaiseSavingEvent = false;
                  


              
				/**/
				try
				{
					TrueDaybookService_v2 trueDayServices = MyVrmService.TrueDayServices;
					//TrueDaybookDefaults defaults = trueDayServices.GetDefaultSettings(false);
					TDGetDefaultSettingsResponse ret = MyVrmService.Service.TDGetDefaultSettings();


                    //appItem.Body = Strings.ConfDefaultBodyText_1; //ZD 102580
                    //appItem.Body += "\n"; //ZD 102580
                    //appItem.Body += Strings.ConfDefaultBodyText_2; //ZD 102580
					//appItem.Body = string.Empty;
					//appItem.Body = string.Empty;//ret.DefaultBody;//defaults.Body; //ConvertBodyHtml2Rtf(appItem, defaults.Body);
					//*** LAST WORKING VERSION!!! **/
					//using (RichEditControl richEditControl = new RichEditControl())
					//{
					//    richEditControl.HtmlText = defaults.Body; // htmlText;
					//    //appItem.Body = richEditControl.RtfText;
					//    //richEditControl.LoadDocument((@".\teste.htm"), DocumentFormat.Html);
					//    //richEditControl.SaveDocument(@"c:\1\teste.rtf", DocumentFormat.Rtf);

					//    object ind = 1;
					//    Inspector inspector = appItem.GetInspector;
					//    var wordDoc = (Microsoft.Office.Interop.Word.Document)inspector.WordEditor;
					//    try
					//    {
					//        Microsoft.Office.Interop.Word.Selection selection = wordDoc.Windows.get_Item(ref ind).Selection;

					//        Clipboard.Clear();
					//        Clipboard.SetData(DataFormats.Rtf, richEditControl.RtfText);//strRtf);//SetText(strRtf);

					//        selection.PasteAndFormat(Microsoft.Office.Interop.Word.WdRecoveryType.wdFormatOriginalFormatting);
					//    }
					//    catch (Exception ex)
					//    {
					//        MyVrmAddin.TraceSource.TraceException(ex);
					//    }
					//}
					appItem.Subject = ret.DefaultSubject;//defaults.Subject; // "[Template invitation subject text obtained from the server]"; 

					if (appItem.UserProperties.Find(VirtualConfPropName, true) == null)
						appItem.UserProperties.Add(VirtualConfPropName, OlUserPropertyType.olYesNo, false, OlFormatYesNo.olFormatYesNoTrueFalse);
					appItem.UserProperties[VirtualConfPropName].Value = true;

					if (appItem.UserProperties.Find(VirtualConfIdPropName, true) == null)
						appItem.UserProperties.Add(VirtualConfIdPropName, OlUserPropertyType.olText/*olInteger*/, false, OlFormatText.olFormatTextText);//OlFormatNumber.olFormatNumberRaw);
					appItem.UserProperties[VirtualConfIdPropName].Value = "";//0;

					if (appItem.UserProperties.Find(VirtualConfIsRevertedPropName, true) == null)
						appItem.UserProperties.Add(VirtualConfIsRevertedPropName, OlUserPropertyType.olYesNo, false, OlFormatYesNo.olFormatYesNoTrueFalse);
					appItem.UserProperties[VirtualConfIsRevertedPropName].Value = false;

					//appItem.Location = "[virtual room]";//defaults.Location;
					//Microsoft.Office.Interop.Outlook.RecurrencePattern recp = appItem.GetRecurrencePattern();
				}
				catch(NoCredentialsException ex)
				{
					UIHelper.ShowError(ex.Message);
					ConnectionOptionsDialog.ExternalCall();
					throw;
				}
				catch (Exception ex)
				{
					MyVrmAddin.TraceSource.TraceException(ex);
					UIHelper.ShowError(ex.Message);
					throw;
				}
                
			}
		}

        private static bool UserHasStaticID()
        {
            User user = MyVrmService.Service.GetUser();
            return user.IsStaticIDEnabled;
        }

        public static void CreateTDBProperties(AppointmentItem appItem)
        {
            if (appItem.UserProperties.Find(VirtualConfPropName, true) == null)
                appItem.UserProperties.Add(VirtualConfPropName, OlUserPropertyType.olYesNo, false, OlFormatYesNo.olFormatYesNoTrueFalse);
            appItem.UserProperties[VirtualConfPropName].Value = true;

            if (appItem.UserProperties.Find(VirtualConfIdPropName, true) == null)
                appItem.UserProperties.Add(VirtualConfIdPropName, OlUserPropertyType.olText/*olInteger*/, false, OlFormatText.olFormatTextText);//OlFormatNumber.olFormatNumberRaw);
            appItem.UserProperties[VirtualConfIdPropName].Value = "";//0;

            if (appItem.UserProperties.Find(VirtualConfIsRevertedPropName, true) == null)
                appItem.UserProperties.Add(VirtualConfIsRevertedPropName, OlUserPropertyType.olYesNo, false, OlFormatYesNo.olFormatYesNoTrueFalse);
            appItem.UserProperties[VirtualConfIsRevertedPropName].Value = false;
        }

    	public static void RevertVirtualConference2Regular(AppointmentItem appItem)
		{
			//AppointmentItem appItem = (AppointmentItem)Globals.ThisAddIn.Application.ActiveInspector().CurrentItem;
			if (appItem != null)
			{
				if (appItem.UserProperties.Find(VirtualConfIsRevertedPropName, true) == null)
					appItem.UserProperties.Add(VirtualConfIsRevertedPropName, OlUserPropertyType.olYesNo, false, OlFormatYesNo.olFormatYesNoTrueFalse);
				appItem.UserProperties[VirtualConfIsRevertedPropName].Value = true;

				if (appItem.UserProperties.Find("IsVConfDirty", true) == null)
					appItem.UserProperties.Add("IsVConfDirty", OlUserPropertyType.olYesNo, false, OlFormatYesNo.olFormatYesNoTrueFalse);
				appItem.UserProperties["IsVConfDirty"].Value = true;
				var vConfProp = appItem.UserProperties.Find(VirtualConfPropName, true);
				if (vConfProp != null)
				{
					vConfProp.Value = false;
					//vConfProp.Delete();
				}
				appItem.Subject = string.Empty;
				appItem.Body = string.Empty;
			}
		}
		
		public void bnVirtualConference_Click(object sender, RibbonControlEventArgs e)
		{
            try
            {
                AppointmentItem appItem = (AppointmentItem)((Inspector)Context).CurrentItem;
                if (appItem.Start < DateTime.Now)
                {
                    MessageBox.Show(null, Strings.CreateOrEditConfInPastMsgTxt,
                                    MyVrmAddin.ProductDisplayName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    if (!IsVirtualConference(appItem))
                    {
                        if (appItem.IsRecurring)
                        {
                            MessageBox.Show(null, Strings.VConfNotSupportRecurrenceMsgTxt,
                                            MyVrmAddin.ProductDisplayName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                        else
                        {
                            DialogResult dr = MessageBox.Show(null, Strings.Convert2VirtTxt,
                                //"Convert this appointment to a Virtual Conference? \n(The subject, the body text and location will be overwrtten.)",
                                                              MyVrmAddin.ProductDisplayName, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (dr == DialogResult.Yes)
                            {
                                //using (NewConfTypeDlg dlg = new NewConfTypeDlg())
                                {
                                    //	DialogResult drNewConfTypeDlg = dlg.ShowDialog();
                                    //	if (drNewConfTypeDlg == DialogResult.OK)
                                    {
                                        //dlg.ConfId;


                                        ConvertAppointment2Virtual(appItem, false, false);//ZD 101226
                                        bnCheckSeats.Visible = true;

                                        bnVirtualConference.Label = Strings.Revert2VirtMenuItemTxt;
                                        bnVirtualConference.ScreenTip = Strings.Revert2VirtMenuItemTipTxt;
                                        bnVirtualConference.Image = Properties.Resources.RevertVConf32x32;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        DialogResult dr = MessageBox.Show(null, Strings.Revert2VirtTxt,
                            //"Revert this Virtual Conference to a regular appointment? \n",
                                                          MyVrmAddin.ProductDisplayName, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dr == DialogResult.Yes)
                        {
                            bnCheckSeats.Visible = false;
                            RevertVirtualConference2Regular(appItem);
                            bnVirtualConference.Label = Strings.Convert2VirtMenuItemTxt;
                            bnVirtualConference.ScreenTip = Strings.Convert2VirtMenuItemTipTxt;
                            bnVirtualConference.Image = Properties.Resources.virtualConf32x32;
                        }
                    }
                    DisEnable();
                }
            }
            catch (Exception exception)
            {
                MyVrmAddin.TraceSource.TraceException(exception);
            }
            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
		}

		public static void CheckSeats_Click(Inspector Context)
		{
			var cursor = Cursor.Current;
			Cursor.Current = Cursors.WaitCursor;

			try
			{
				using (var checkXmlSeatsDlg = new XmlSeatsDlg())
				{
					AppointmentItem appItem = (AppointmentItem)(Context).CurrentItem;
					checkXmlSeatsDlg.ConfStart = appItem != null ? appItem.StartInStartTimeZone : DateTime.Now; //ZD 102582
					checkXmlSeatsDlg.ConfEnd = appItem != null ? appItem.EndInEndTimeZone : checkXmlSeatsDlg.ConfStart.AddHours(1);
					checkXmlSeatsDlg.ConfId = GetVirtualConferenceId(appItem);
                    checkXmlSeatsDlg.timeZTc = TimeZoneInfo.FindSystemTimeZoneById(appItem.StartTimeZone.ID);//appItem.StartTimeZone.ID; //TimeZoneInfo.FromSerializedString(appItem.StartTimeZone.Name);//(appItem.StartTimeZone.Name); ZD 102582
					
                    checkXmlSeatsDlg.ShowDialog();
					if (checkXmlSeatsDlg.OpenOptionsDlg)
						ConnectionOptionsDialog.ExternalCall();

				}
				/* unused
				using (var checkSeatsDlg = new CheckSeatsDlg())
				{
					AppointmentItem appItem = (AppointmentItem)(Context).CurrentItem;
					checkSeatsDlg.ConfStart = appItem != null ? appItem.Start.Date : DateTime.Now.Date;
					checkSeatsDlg.ConfId = GetVirtualConferenceId(appItem);
					checkSeatsDlg.ShowDialog();
					if(checkSeatsDlg.OpenOptionsDlg)
						ConnectionOptionsDialog.ExternalCall();
				}
				* */
			}
			catch (Exception exception)
			{
				MyVrmAddin.TraceSource.TraceException(exception);
				UIHelper.ShowError(exception.Message);
			}
			finally
			{
				Cursor.Current = cursor;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
			}
		}

    	public void bnCheckSeats_Click(object sender, RibbonControlEventArgs e)
		{
			CheckSeats_Click((Inspector)Context);
			//var cursor = Cursor.Current;
			//Cursor.Current = Cursors.WaitCursor;

			//try
			//{
			//    using (var checkSeatsDlg = new CheckSeatsDlg())
			//    {
			//        AppointmentItem appItem = (AppointmentItem)((Inspector)Context).CurrentItem;
			//        checkSeatsDlg.ConfStart = appItem != null ? appItem.Start.Date : DateTime.Now.Date;
			//        checkSeatsDlg.ShowDialog();
			//    }
			//}
			//catch (Exception exception)
			//{
			//    MyVrmAddin.TraceSource.TraceException(exception);
			//    UIHelper.ShowError(exception.Message);
			//}
			//finally
			//{
			//    Cursor.Current = cursor;
			//}
		}

        private void btnConference_Click(object sender, RibbonControlEventArgs e)
        {
            conferenceButton_OnClick((Inspector)Context);

        }
        static public void conferenceButton_OnClick(Inspector externalContext)
        {
            try
            {
                var inspector = externalContext;
                if (inspector != null)
                {
                    inspector.SetCurrentFormPage("TrueDayBookAddin2007.ConferenceFormRegion");
                }
            }
            catch (Exception exception)
            {
                MyVrmAddin.TraceSource.TraceException(exception);
                UIHelper.ShowError(exception.Message);
            }
        }

    }
}
