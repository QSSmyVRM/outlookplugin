﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	internal class GetLineRatesRequest : ServiceRequestBase<GetLineRatesResponse>
	{
		public GetLineRatesRequest(MyVrmService service)
			: base(service)
		{
		}

		#region Overrides of ServiceRequestBase

		internal override string GetXmlElementName()
		{
			return "GetLineRate";
		}

		internal override string GetCommandName()
		{
			return "GetLineRate";
		}

		internal override string GetResponseXmlElementName()
		{
			return "LineRate";
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			UserId.WriteToXml(writer, "userID");
		}

		#endregion
	}
	//
	public class GetLineRatesResponse : ServiceResponse
	{
		private readonly Dictionary<int, string> _lineRates = new Dictionary<int, string>();

		public Dictionary<int, string> LineRates
		{
			get
			{
				return new Dictionary<int, string>(_lineRates);
			}
		}

		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{
			base.ReadElementsFromXml(reader);
			_lineRates.Clear();
			do
			{
				reader.Read();
				if (reader.IsStartElement(XmlNamespace.NotSpecified, "Rate"))
				{
					int i = reader.ReadElementValue<int>(XmlNamespace.NotSpecified, "LineRateID");
					string name = reader.ReadElementValue(XmlNamespace.NotSpecified, "LineRateName");
					_lineRates.Add(i, name);
				}
				else
				{
					reader.SkipCurrentElement();
				}
			} while (!reader.IsEndElement(XmlNamespace.NotSpecified, "LineRate"));
		}
	}
}