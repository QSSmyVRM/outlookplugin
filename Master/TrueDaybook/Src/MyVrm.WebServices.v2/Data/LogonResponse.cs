﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.WebServices.Data
{
    internal class LogonResponse : ServiceResponse
    {
        internal UserId UserId { get; private set; }
        internal OrganizationId OrganizationId { get; private set; }
		internal bool IsAVEnabled { get; set; }
		internal TrueDaybookService_v2.UserState LoggedUserState { get; set; }
        internal string UserTDBpassword { get; set; }
        

        internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
        {
            do
            {
                reader.Read();
                if (reader.LocalName == "globalInfo")
                {
                    do
                    {
                        reader.Read();
                        if (reader.LocalName == "userID")
                        {
                            UserId = new UserId(reader.ReadValue());
                        }
                        else if (reader.LocalName == "organizationID")
                        {
                            OrganizationId = OrganizationId.Invalid;
                            OrganizationId.LoadFromXml(reader, reader.LocalName);
                        }
						else if(reader.LocalName == "enableAV")
						{
							IsAVEnabled = reader.ReadElementValue<bool>();
						}
                        else if (reader.LocalName == "UserTDBpassword")
						{
                            UserTDBpassword = reader.ReadValue();
						}
                        else if(reader.LocalName == "poweruser")
						{
							LoggedUserState = (TrueDaybookService_v2.UserState)reader.ReadElementValue<int>();
						}

                        else
                        {
                            reader.SkipCurrentElement();
                        }
                    } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "globalInfo"));
                }
                else
                {
                    reader.SkipCurrentElement();
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "user"));
        }
    }
}
