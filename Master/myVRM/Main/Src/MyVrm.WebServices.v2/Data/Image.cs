﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.IO;

namespace MyVrm.WebServices.Data
{
    /// <summary>
    /// Represents an image within myVRM web service
    /// </summary>
    [Serializable]
    public class Image
    {
        internal Image() : this(null, null)
        {
        }

        internal Image(string name, byte[] data)
        {
            Name = name;
            Data = data;
        }
        /// <summary>
        /// Name of image.
        /// </summary>
        public string Name { get; internal set; }
        /// <summary>
        /// Image data.
        /// </summary>
        public byte[] Data { get; internal set; }
        /// <summary>
        /// Indicates if <see cref="Image"/> is valid or not.
        /// </summary>
        public bool IsValid
        {
            get { return Data != null && Data.Length > 0; }
        }
        /// <summary>
        /// Returns image data as <see cref="Stream"/> object.
        /// </summary>
        /// <returns></returns>
        public Stream AsStream()
        {
            return new MemoryStream(Data, false);
        }
    }
}
