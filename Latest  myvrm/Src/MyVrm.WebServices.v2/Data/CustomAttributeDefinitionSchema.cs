﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    public class CustomAttributeDefinitionSchema : ServiceObjectSchema
    {
        public static readonly PropertyDefinition Id;
        public static readonly PropertyDefinition Title;
        public static readonly PropertyDefinition Mandatory;
        public static readonly PropertyDefinition Description;
        public static readonly PropertyDefinition Type;
        public static readonly PropertyDefinition Status;
        public static readonly PropertyDefinition IncludeInEmail;
        public static readonly PropertyDefinition CreateType;
        //public static readonly PropertyDefinition OptionList;


        internal static readonly CustomAttributeDefinitionSchema Instance;

        static CustomAttributeDefinitionSchema()
        {
            Id = new ComplexPropertyDefinition<CustomAttributeId>("CustomAttributeID", () => new CustomAttributeId());
            Title = new StringPropertyDefinition("Title", PropertyDefinitionFlags.CanSet);
            Mandatory = new BoolPropertyDefinition("Mandatory", PropertyDefinitionFlags.CanSet);
            Description = new StringPropertyDefinition("Description", PropertyDefinitionFlags.CanSet);
            Type = new GenericPropertyDefinition<CustomAttributeType>("Type", PropertyDefinitionFlags.CanSet);
            Status = new IntPropertyDefinition("Status", PropertyDefinitionFlags.CanSet);
            IncludeInEmail = new BoolPropertyDefinition("IncludeInEmail", PropertyDefinitionFlags.CanSet);
            CreateType = new GenericPropertyDefinition<CustomAttributeCreateType>("CreateType", PropertyDefinitionFlags.CanSet);

            //OptionList = new ComplexPropertyDefinition<ConciergeSupportParams>("OptionList",
            //                                                                PropertyDefinitionFlags.AutoInstantiateOnRead |
            //                                                                                   PropertyDefinitionFlags.CanSet,
            //                                                                                   () =>
            //                                                                                   new ConciergeSupportParams());
            Instance = new CustomAttributeDefinitionSchema();
        }

        internal override void RegisterProperties()
        {
            RegisterProperty(Id);
            RegisterProperty(Title);
            RegisterProperty(Mandatory);
            RegisterProperty(Description);
            RegisterProperty(Type);
            RegisterProperty(Status);
            RegisterProperty(IncludeInEmail);
            RegisterProperty(CreateType);
            //RegisterProperty(OptionList);
        }
    }
}
