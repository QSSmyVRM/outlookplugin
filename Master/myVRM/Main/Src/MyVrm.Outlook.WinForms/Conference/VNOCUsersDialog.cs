﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook.WinForms.Conference
{
	public partial class VNOCUsersDialog : MyVrm.Outlook.WinForms.Dialog
	{
		internal Dictionary<int, string> _selected = new Dictionary<int, string>();
		internal Collection<VNOCUser> _VNOCUsers = new Collection<VNOCUser>();
		public Dictionary<int, string> SelectedUsers
		{
			get { return _selected; }
		}
		public VNOCUsersDialog()
		{
			try
			{
				InitializeComponent();
				ApplyVisible = false;
				ApplyEnabled = false;
				OkEnabled = true;
				OkVisible = true;
				CancelEnabled = true;
				CancelVisible = true;
                colFirstName.Caption = Strings.FirstNameText;
                colLastName.Caption = Strings.LastNameText;
                colSelectedState.Caption = Strings.CheckedStateText;
                colUserEmail.Caption = Strings.UserEmailText;
                colOrganizationName.Caption = Strings.OrgNameText;
                Text = Strings.VNOCUsers;//ZD 102888

			}
			catch (Exception exception)
			{
				MyVrmAddin.TraceSource.TraceException(exception);
				UIHelper.ShowError(exception.Message);
			}
		}

		private void VNOCUsers_Load(object sender, EventArgs e)
		{
			try
			{
				//Collection<VNOCUser> users = MyVrmService.Service.GetVNOCUserList().VNOCUsers;
				foreach (var vnocUserId in _selected)
				{
					var found = FindVNOCUserById(vnocUserId.Key, _VNOCUsers);
					if (found != null)
						found.CheckedState = true;
				}
				vNOCUserBindingSource.DataSource = _VNOCUsers;
			}
			catch (Exception exception)
			{
				MyVrmAddin.TraceSource.TraceException(exception);
				UIHelper.ShowError(exception.Message);
			}
		}

		public static VNOCUser FindVNOCUserById(int id, Collection<VNOCUser> list)
		{
			foreach (var user in list)
			{
				if( user.UserID == id)
					return user;
			}
			return null;
		}

		public static Dictionary<int, string> TransformToIdName(Collection<VNOCUser> src, bool onlyChecked)
		{
			Dictionary<int, string> dest = new Dictionary<int, string>();
			if (src != null)
			{
				dest.Clear();
				foreach (var user in src)
				{
					if (onlyChecked && user.CheckedState || !onlyChecked)
						dest.Add(user.UserID, user.FirstName + " " + user.LastName);
				}
			}
			return dest;
		}

		private void VNOCUsers_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (((VNOCUsersDialog)sender).DialogResult == DialogResult.OK)
			{
				Collection<VNOCUser> users = (Collection<VNOCUser>) vNOCUserBindingSource.DataSource;
				_selected.Clear();
				_selected = TransformToIdName(users, true);
				//foreach (var user in users)
				//{
				//    _selected.Add(user.UserID, user.FirstName + " " + user.LastName);
				//}
			}
		}
	}
}
