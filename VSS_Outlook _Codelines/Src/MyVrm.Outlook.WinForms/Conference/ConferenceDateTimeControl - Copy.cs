﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System.ComponentModel;
using System.Windows.Forms;
using MyVrm.WebServices.Data; //ZD 101376
namespace MyVrm.Outlook.WinForms.Conference
{
    public partial class ConferenceDateTimeControl : ConferenceAwareControl
    {
        public ConferenceDateTimeControl()
        {
            InitializeComponent();
        	layoutControlItem14.Text = Strings.StartTimeLableText;
			layoutControlItem15.Text = Strings.EndTimeLableText;
            chkStartNow.Text = "";//Strings.StartNowLabelText;
            label1.Text = Strings.StartNowLabelText;

            // ZD 101376 starts
            publicConferenceCheckEdit.SuperTip.Items.Clear();
            publicConferenceCheckEdit.SuperTip.Items.Add(Strings.PublicConferenceLableText);
            publicConferenceCheckEdit.SuperTip.Items.Add(Strings.PublicConferenceCheckBoxText);
            publicConferenceCheckEdit.Properties.Caption = "";//Strings.PublicConferenceLableText;
           label2.Text = Strings.PublicConferenceLableText;
            // ZD 101376 Ends

        }


        protected override void OnReadOnlyChanged(ReadOnlyChangedEventArgs e)
        {
            publicConferenceCheckEdit.Enabled = !e.ReadOnly;
            startDateEdit.Enabled = startTimeEdit.Enabled = !e.ReadOnly;
            endDateEdit.Enabled = endTimeEdit.Enabled = !e.ReadOnly;
            if (chkStartNow.Visible == true) //ZD 102177 start
            {
                chkStartNow.Enabled = !e.ReadOnly;
            } //ZD 102177 End
        }

        protected override void OnConferenceBindingSourceDataSourceChanged(System.EventArgs e)
        {
            Conference.PropertyChanged += ConferencePropertyChanged;

            startDateEdit.Enabled = startTimeEdit.Enabled = !Conference.IsRecurring;
            endDateEdit.Enabled = endTimeEdit.Enabled = !Conference.IsRecurring;
            if (chkStartNow.Visible == true) //ZD 102177 start
            {
                chkStartNow.Enabled = !Conference.IsRecurring;
                if (Conference.IsRecurring)
                {
                    chkStartNow.Checked = false;
                    chkStartNow.Enabled = false;
                }
            } //ZD 102177 End
            
        }

        private void ConferencePropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch(e.PropertyName)
            {
                case "IsRecurring":
                {
                    startDateEdit.Enabled = startTimeEdit.Enabled = !Conference.IsRecurring;
                    endDateEdit.Enabled = endTimeEdit.Enabled = !Conference.IsRecurring;
                    if (chkStartNow.Visible == true) //ZD 102177 start
                    {
                        chkStartNow.Enabled = !Conference.IsRecurring;
                        if (Conference.IsRecurring)
                        {
                            chkStartNow.Checked = false;
                            chkStartNow.Enabled = false;
                        }
                    } //ZD 102177 End
                       
                    
                    break;
                }
            }
        }

        void endTimeBinding_BindingComplete(object sender, BindingCompleteEventArgs e)
        {
            if (e.BindingCompleteState != BindingCompleteState.Success && e.BindingCompleteContext == BindingCompleteContext.DataSourceUpdate)
            {
                    ShowError(e.ErrorText);
                    e.Binding.ReadValue();
                    e.Cancel = true;
            }
        }

        private void ConferenceDateTimeControl_Load(object sender, System.EventArgs e)
        {
            if (!DesignMode)
            {
                if (ConferenceBindingSource != null)
                {
                    startDateEdit.DataBindings.Add("EditValue", ConferenceBindingSource, "StartDate", true,
                                                   DataSourceUpdateMode.OnPropertyChanged);
                    startTimeEdit.DataBindings.Add("EditValue", ConferenceBindingSource, "StartDate", true,
                                                   DataSourceUpdateMode.OnPropertyChanged);
                    Binding endDateBinding = endDateEdit.DataBindings.Add("EditValue", ConferenceBindingSource, "EndDate",
                                                                          true, DataSourceUpdateMode.OnPropertyChanged);
                    endDateBinding.BindingComplete += endTimeBinding_BindingComplete;
                    Binding endTimeBinding = endTimeEdit.DataBindings.Add("EditValue", ConferenceBindingSource, "EndDate",
                                                                          true, DataSourceUpdateMode.OnPropertyChanged);
                    endTimeBinding.BindingComplete += endTimeBinding_BindingComplete;
                    publicConferenceCheckEdit.DataBindings.Add("Checked", ConferenceBindingSource, "IsPublic", true,
                                                               DataSourceUpdateMode.OnPropertyChanged);
                }
                var preference = MyVrmService.Service.OrganizationSettings.Preference;
                publicConferenceCheckEdit.Visible = preference.EnablePublicConferencing;
                if (!preference.EnablePublicConferencing)//ZD 101249 start
                {
                    //publicConferenceCheckEdit.Dispose();
                    publicConferenceCheckEdit.Visible = false;
                    layoutControlItem2.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;

                }
                //ZD 101836 starts 
                if (!preference.EnableImmConf)
                {
                    //chkStartNow.Dispose();
                    chkStartNow.Visible = false;
                    layoutControlItem1.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                }
                //ZD 101836 ends //ZD 101249 End
            }
            
        }

        

        private void chkStartNow_CheckedChanged(object sender, System.EventArgs e)
        {
            if (chkStartNow.Checked)
            {

                startDateEdit.DateTime = System.DateTime.Now.AddMinutes(1.0);
                endDateEdit.DateTime = System.DateTime.Now.AddMinutes(30.0);
                label1.Margin = new Padding(8, 0, 0, 2);
                label2.Margin = new Padding(8, 0, 0, 6);
                

                layoutControlItem15.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layoutControlItem14.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layoutControlItem8.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layoutControlItem9.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;

               
                Conference.IsImmediate = true;
                
            }
            else
            {
                layoutControlItem15.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                layoutControlItem14.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                layoutControlItem8.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                layoutControlItem9.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                Conference.IsImmediate = false;
            }
        }
    }
}