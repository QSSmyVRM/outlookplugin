﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	internal class GetAddressTypesRequest : ServiceRequestBase<GetAddressTypesResponse>
	{
		public GetAddressTypesRequest(MyVrmService service)
			: base(service)
		{
		}

		#region Overrides of ServiceRequestBase

		internal override string GetXmlElementName()
		{
			return "GetAddressType";
		}

		internal override string GetCommandName()
		{
			return "GetAddressType";
		}

		internal override string GetResponseXmlElementName()
		{
			return "GetAddressType";
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			UserId.WriteToXml(writer, "UserID");
		}

		#endregion
	}
	//
	public class GetAddressTypesResponse : ServiceResponse
	{
		private readonly Dictionary<int, string> _addressTypes = new Dictionary<int, string>();

		public Dictionary<int, string> AddressTypes
		{
			get
			{
				return new Dictionary<int, string>(_addressTypes);
			}
		}

		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{
			base.ReadElementsFromXml(reader);
			_addressTypes.Clear();
			while (!reader.IsStartElement(XmlNamespace.NotSpecified, "AddressType")) 
				reader.Read();
			do
			{
				reader.Read();
				if (reader.IsStartElement(XmlNamespace.NotSpecified, "Type"))
				{
					int i = reader.ReadElementValue<int>(XmlNamespace.NotSpecified, "ID");
					string name = reader.ReadElementValue(XmlNamespace.NotSpecified, "Name");
					_addressTypes.Add(i, name);
					reader.Read();
				}
				else
				{
					reader.SkipCurrentElement();
				}
			} while (!reader.IsEndElement(XmlNamespace.NotSpecified, "AddressType"));
		}
	}
}