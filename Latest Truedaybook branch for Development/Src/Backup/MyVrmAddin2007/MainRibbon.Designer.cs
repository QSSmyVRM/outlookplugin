﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrmAddin2007
{
	partial class MainRibbon
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.tabMyVrm = this.Factory.CreateRibbonTab();
            this.groupTrueDaybook = this.Factory.CreateRibbonGroup();
            this.bnVirtualConference = this.Factory.CreateRibbonButton();
            this.btnInstanceConference = this.Factory.CreateRibbonButton();
            this.separator1 = this.Factory.CreateRibbonSeparator();
            this.templateMenu = this.Factory.CreateRibbonMenu();
            this.separator2 = this.Factory.CreateRibbonSeparator();
            this.bnCheckSeats = this.Factory.CreateRibbonButton();
            this.optionsButton = this.Factory.CreateRibbonButton();
            this.tabMyVrm.SuspendLayout();
            this.groupTrueDaybook.SuspendLayout();
            // 
            // tabMyVrm
            // 
            this.tabMyVrm.Groups.Add(this.groupTrueDaybook);
            this.tabMyVrm.Label = "myVRM";
            this.tabMyVrm.Name = "tabMyVrm";
            // 
            // groupTrueDaybook
            // 
            this.groupTrueDaybook.Items.Add(this.bnVirtualConference);
            this.groupTrueDaybook.Items.Add(this.btnInstanceConference);
            this.groupTrueDaybook.Items.Add(this.separator1);
            this.groupTrueDaybook.Items.Add(this.templateMenu);
            this.groupTrueDaybook.Items.Add(this.separator2);
            this.groupTrueDaybook.Items.Add(this.bnCheckSeats);
            this.groupTrueDaybook.Items.Add(this.optionsButton);
            this.groupTrueDaybook.Label = "TrueDaybook";
            this.groupTrueDaybook.Name = "groupTrueDaybook";
            // 
            // bnVirtualConference
            // 
            this.bnVirtualConference.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.bnVirtualConference.Image = global::MyVrmAddin2007.Properties.Resources.virtualConf32x32;
            this.bnVirtualConference.Label = "New Conference";
            this.bnVirtualConference.Name = "bnVirtualConference";
            this.bnVirtualConference.ShowImage = true;
            this.bnVirtualConference.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.bnVirtualConference_Click);
            // 
            // btnInstanceConference
            // 
            this.btnInstanceConference.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnInstanceConference.Image = global::MyVrmAddin2007.Properties.Resources.instant_conference_icon_32x32;
            this.btnInstanceConference.Label = "Instant Conference";
            this.btnInstanceConference.Name = "btnInstanceConference";
            this.btnInstanceConference.ShowImage = true;
            this.btnInstanceConference.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnInstanceConference_Click);
            // 
            // separator1
            // 
            this.separator1.Name = "separator1";
            // 
            // templateMenu
            // 
            this.templateMenu.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.templateMenu.Dynamic = true;
            this.templateMenu.Label = "Create From Template";
            this.templateMenu.Name = "templateMenu";
            this.templateMenu.OfficeImageId = "CreateForm";
            this.templateMenu.ShowImage = true;
            this.templateMenu.ItemsLoading += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.tdTemplateMenu_ItemsLoading);
            // 
            // separator2
            // 
            this.separator2.Name = "separator2";
            // 
            // bnCheckSeats
            // 
            this.bnCheckSeats.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.bnCheckSeats.Image = global::MyVrmAddin2007.Properties.Resources.checkSeats32x32;
            this.bnCheckSeats.Label = "Check Available Seats";
            this.bnCheckSeats.Name = "bnCheckSeats";
            this.bnCheckSeats.OfficeImageId = "GroupTracking";
            this.bnCheckSeats.ShowImage = true;
            this.bnCheckSeats.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.bnCheckSeats_Click);
            // 
            // optionsButton
            // 
            this.optionsButton.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.optionsButton.Image = global::MyVrmAddin2007.Properties.Resources.Options32x32;
            this.optionsButton.Label = "Options";
            this.optionsButton.Name = "optionsButton";
            this.optionsButton.ShowImage = true;
            this.optionsButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.optionsButton_Click);
            // 
            // MainRibbon
            // 
            this.Name = "MainRibbon";
            this.RibbonType = "Microsoft.Outlook.Explorer";
            this.Tabs.Add(this.tabMyVrm);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.MainRibbon_Load);
            this.tabMyVrm.ResumeLayout(false);
            this.tabMyVrm.PerformLayout();
            this.groupTrueDaybook.ResumeLayout(false);
            this.groupTrueDaybook.PerformLayout();
			this.ResumeLayout(false);
		}

		#endregion

		private Microsoft.Office.Tools.Ribbon.RibbonTab tabMyVrm;
		internal Microsoft.Office.Tools.Ribbon.RibbonGroup groupTrueDaybook;
		internal Microsoft.Office.Tools.Ribbon.RibbonButton bnCheckSeats;
		internal Microsoft.Office.Tools.Ribbon.RibbonButton bnVirtualConference;
		internal Microsoft.Office.Tools.Ribbon.RibbonButton optionsButton;
		internal Microsoft.Office.Tools.Ribbon.RibbonMenu templateMenu;
		internal Microsoft.Office.Tools.Ribbon.RibbonSeparator separator1;
		internal Microsoft.Office.Tools.Ribbon.RibbonSeparator separator2;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnInstanceConference;

	}

	partial class ThisRibbonCollection : Microsoft.Office.Tools.Ribbon.RibbonReadOnlyCollection
	{
		internal MainRibbon MainRibbon
		{
			get { return this.GetRibbon<MainRibbon>(); }
		}
	}
}
