﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using MyVrm.Common.ComponentModel;
using MyVrm.WebServices.Data;
using System.Text.RegularExpressions;
using DevExpress.XtraEditors.DXErrorProvider;




namespace MyVrm.Outlook.WinForms.Conference
{
    public partial class AdditionalOptionsPage : ConferencePage
    {
		internal Collection<VNOCUser> _VNOCUsers = new Collection<VNOCUser>();
        internal Collection<EntityCode> _EntityCodes = new Collection<EntityCode>();
        AutoCompleteStringCollection collection = new AutoCompleteStringCollection();
        AutoCompleteStringCollection Emptycollection = new AutoCompleteStringCollection();
        //ErrorProvider errorProvider = new ErrorProvider();
        public User _userProfilelandID; //ZD 103181
        public User UserProfile
        {
            get { return _userProfilelandID ?? (_userProfilelandID = MyVrmService.Service.GetUser()); }
        }
        public void fnMessageHide()
        {
            if (MyVrmService.Service.OrganizationOptions.EnableActMsgDelivery)
            {
                layoutControlItem3.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                layoutControlItem11.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            }
        }
        public void fnMsgShow()
        {
            layoutControlItem3.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never ;
            layoutControlItem11.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
        }
        public AdditionalOptionsPage()
        {
            Text = Strings.AdditionalOptionsPageText;
            InitializeComponent();
            ConferenceBindingSource.DataSourceChanged += ConferenceBindingSource_DataSourceChanged;
        	RepositoryInit();
            emptySpaceItem1.Text = Strings.SpecialInstructionsLabelText;
            layoutControlItem2.Text = Strings.TypeInstructionsHereText;
            labelControl1.Text = Strings.ConferenceSupportLabelText;
            checkEditAVSupport.Text = Strings.EditAVSupportText;
            checkEditMeetGreet.Text = Strings.EditMeetGreetText;
            checkEditConciergeMonitoring.Text = Strings.EditConciergeText;
            checkEditDVNOC.Text = Strings.EditDVNOCText;
            labelControl2.Text = Strings.MessageOverLayText;
            colMessages.Caption = Strings.MessageText;
            colTime.Caption = Strings.TimeText;
            layoutControlItemEntitycode.Text = Strings.EntityCode;

            lblEntitycode.Visible = false;
        }

    	public ReadOnlyCollection<OverlayMessagesDropboxContent> MsgDropboxContent
    	{
    		get
    		{
    			if(_msgDropboxContent == null)
					_msgDropboxContent = MyVrmService.Service.GetConfMsg().DropboxContent;
    			return _msgDropboxContent;
    		}
    	}

    	private ReadOnlyCollection<OverlayMessagesDropboxContent> _msgDropboxContent;

		void RepositoryInit()
		{
			var msgDropboxContent = MsgDropboxContent;//MyVrmService.Service.GetConfMsg().DropboxContent;
			repositoryItemComboBoxMessages.Items.Clear();
			repositoryItemComboBox_1_4_7_5Min.Items.Clear();
			repositoryItemComboBox_2_5_8_2Min.Items.Clear();
			repositoryItemComboBox_3_6_9_30Sec.Items.Clear();
			_messages.Clear();
			foreach (var confMsg in msgDropboxContent)
			{
				repositoryItemComboBoxMessages.Items.Add(confMsg.Text);
			}
			//1,4,7 duration drop down will have - 5M duration values (5 to 30 minutes in 5 minute increments)
			for (int i = 5; i <= 30; i += 5)
			{
				repositoryItemComboBox_1_4_7_5Min.Items.Add(i + "M");
			}
			//2,5,8 duration drop down will have  - 2M duration values (2 to 20 minutes in 2 minute increments)
			for (int i = 2; i <= 20; i += 2)
			{
				repositoryItemComboBox_2_5_8_2Min.Items.Add(i + "M");
			}
			//3,6,9 duration drop down will have - 30s duration values (30 to 120 seconds in 30 second increments) 
			for (int i = 30; i <= 120; i += 30)
			{
				repositoryItemComboBox_3_6_9_30Sec.Items.Add(i + "s");
			}
		}

		public ConfMessageCollection DefaultConfMessages
    	{
    		get
    		{
    			if(_defaultConfMessages == null)
					_defaultConfMessages = MyVrmService.Service.GetNewConference().ConfMessages;
    			return _defaultConfMessages;
    		}
    	}

		private ConfMessageCollection _defaultConfMessages;

		

        protected override void OnApplying(System.ComponentModel.CancelEventArgs e)
        {
            base.OnApplying(e);
			if (Conference != null)
			{
                //ALLBUGS-138 start
                if (string.IsNullOrEmpty(textEntitycode.Text) && Conference.Entitycodestatusswitch == 0 && Conference.EntitycodeMandatory == true)
                {
                    errorProvider.SetError(textEntitycode, Strings.Entitycodeisrequired);
                }
                else
                {
                    errorProvider.SetError(textEntitycode, "");
                }

                if (string.IsNullOrEmpty(specialInstructionsEdit.Text) && Conference.SpecialInsMandatory == true)
                {
                    SpecerrorProvider.SetError(specialInstructionsEdit, Strings.SpecialInsisrequired);
                }
                else
                {
                    SpecerrorProvider.SetError(specialInstructionsEdit, "");
                }
                //ALLBUGS-138 End
				Conference.SpecialInstructions = specialInstructionsEdit.Text;
                int Languageid = 0;
                Languageid = UserProfile.languageID;
               _EntityCodes = MyVrmService.Service.GetEntityCodes(Languageid).EntityCodes;

               if (!string.IsNullOrEmpty(textEntitycode.Text))
               {
                   if (_EntityCodes.Count > 0)
                   {
                       foreach (var user in _EntityCodes)
                       {
                           if (!string.IsNullOrEmpty(user.Caption) && textEntitycode.Text.ToLower() == user.Caption.ToLower())
                           {
                               Conference.Entitycodeid = user.OptionID.ToString(); //ZD 103265
                               Conference.Entitycode = textEntitycode.Text;
                           }
                           else
                           {
                               Conference.Entitycodeid = "new";
                               Conference.Entitycode = textEntitycode.Text;
                           }
                       }
                   }
               }
               else
               {
                   Conference.Entitycodeid ="";
                   Conference.Entitycode = "";
               }
                
                Conference.ConciergeSupportParams.OnSiteAVSupport = checkEditAVSupport.Checked;
                Conference.ConciergeSupportParams.ConciergeMonitoring = checkEditConciergeMonitoring.Checked;
				Conference.ConciergeSupportParams.MeetandGreet = checkEditMeetGreet.Checked;
				Conference.ConciergeSupportParams.DedicatedVNOCOperator = checkEditDVNOC.Checked;
				Conference.ConciergeSupportParams.ConfVNOCOperators = VNOCUsersDialog.TransformToIdName(_VNOCUsers, true);
				//ConfMessageCollection mmm = (ConfMessageCollection)confMessageBindingSource.DataSource; //Commented for ZD 101358
				var res = _messages.Where(a => a.Checked);
				Conference.ConfMessageCollection.Clear();
				foreach (var confMessage in res)
				{
					Conference.ConfMessageCollection.Add(confMessage);
				}
				Conference.Conference.AdvancedAudioVideoSettings.ConfMessageCollection = Conference.ConfMessageCollection;
			}
        }

        protected override void OnReadOnlyChanged(ReadOnlyChangedEventArgs e)
        {
            base.OnReadOnlyChanged(e);
            specialInstructionsEdit.Enabled = !e.ReadOnly;

			checkEditConciergeMonitoring.Enabled = !e.ReadOnly;
			checkEditAVSupport.Enabled = !e.ReadOnly;
			checkEditMeetGreet.Enabled = !e.ReadOnly;
			checkEditDVNOC.Enabled = !e.ReadOnly;

			listBoxVNOCUsers.Enabled = !e.ReadOnly;
			bnAdd.Enabled = !e.ReadOnly;
			bnRemove.Enabled = !e.ReadOnly;
			treeListMessages.Enabled = !e.ReadOnly;
        }

        void ConferenceBindingSource_DataSourceChanged(object sender, System.EventArgs e)
        {


           
            //_EntityCodes = MyVrmService.Service.GetEntityCodes._VNOCUsers = MyVrmService.Service.GetVNOCUserList().VNOCUsers;
             int Languageid = 0;
             Languageid = UserProfile.languageID;

            // _EntityCodes = MyVrmService.Service.GetEntityCodes(Languageid).EntityCodes;

             _EntityCodes = MyVrmService.Service.GetEntityCodes(Languageid).EntityCodes;
             if (_EntityCodes.Count > 0)
             {
                 foreach (var user in _EntityCodes)
                 {
                     if (!string.IsNullOrEmpty(user.Caption))
                     {
                         //string DisplayName = user.Caption;
                         collection.Add(user.Caption);
                     }
                 }

                 textEntitycode.MaskBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                 textEntitycode.MaskBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
                 
             }
            //textEntitycode.MaskBox.AutoCompleteMode = AutoCompleteMode.None;

             

            specialInstructionsEdit.Text = Conference.SpecialInstructions;

            textEntitycode.Text = Conference.Entitycode;            
            if (Conference.Entitycodestatusswitch == 1)
            {
                layoutControlItemEntitycode.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                textEntitycode.Visible = false;
            }
            else
            {
                layoutControlItemEntitycode.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                textEntitycode.Visible = true;
            }

        	InitConsiergeSupport();
        }

		void InitConsiergeSupport()
		{
			try
			{

				SuspendLayout();
                if (Conference != null)
                {
                    checkEditAVSupport.Checked = Conference.ConciergeSupportParams.OnSiteAVSupport;// ZD 101307
                   checkEditConciergeMonitoring.Checked = Conference.ConciergeSupportParams.ConciergeMonitoring;// ZD 101307
                    checkEditMeetGreet.Checked = Conference.ConciergeSupportParams.MeetandGreet;
                    checkEditDVNOC.Checked = Conference.ConciergeSupportParams.DedicatedVNOCOperator;
                }
                bool isEnableDedicatedVNOC = true;
                if (MyVrmService.Service.OrganizationSettings != null && MyVrmService.Service.OrganizationSettings.Preference != null)
                {

                    bool isEnableOnsiteAV = MyVrmService.Service.OrganizationSettings.Preference.EnableOnsiteAV;
                    if (!isEnableOnsiteAV)
                    {


                        layoutControlItem4.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                    }

                    bool isEnableMeetGreet = MyVrmService.Service.OrganizationSettings.Preference.EnableMeetandGreet;

                    if (!isEnableMeetGreet)
                    {
                        layoutControlItem5.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                    }

                    bool isEnableConciergeMonitoring = MyVrmService.Service.OrganizationSettings.Preference.EnableConciergeMonitoring;

                    if (!isEnableConciergeMonitoring)
                    {
                        layoutControlItem6.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                    }


                    isEnableDedicatedVNOC = MyVrmService.Service.OrganizationSettings.Preference.EnableDedicatedVNOC;
                    if (!isEnableDedicatedVNOC)
                    {
                      //  layoutControlItem7.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never; commented for ZD 101277
                    }
                    if (!isEnableDedicatedVNOC && !isEnableMeetGreet && !isEnableOnsiteAV && !isEnableConciergeMonitoring)
                    {
                        layoutControlItem1.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                    }
                }


                User userProfile = MyVrmService.Service.GetUser();
                if (userProfile != null)
                {
                    bool isPropertySet = userProfile.IsVisibleVNOCSelection;
                    if (!isPropertySet)// ZD 101277
                    {
                        layoutControlItem7.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never; //ZD 101277
                        layoutControlItem8.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                        layoutControlItem9.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                        layoutControlItem10.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                    }
                }

				
				//var ret2 = MyVrmService.Service.GetVNOCUserList().VNOCUsers;
				ConfigurationSettings orgOptions = MyVrmService.Service.GetConfigurationSettings();
				//orgOptions.ConciergeSupport;
				listBoxVNOCUsers.Items.Clear();
				_VNOCUsers = MyVrmService.Service.GetVNOCUserList().VNOCUsers;
				var idName = VNOCUsersDialog.TransformToIdName(_VNOCUsers, false);
				if (Conference != null)
				{
					foreach (var user in Conference.ConciergeSupportParams.ConfVNOCOperators)
					{
						string name = string.Empty;
						if (idName.TryGetValue(user.Key, out name))
						{
							listBoxVNOCUsers.Items.Add(name);
							MyVrmAddin.TraceSource.TraceInformation("VNOCUser user: " + name);
						}
					}
				}
				InitMessages();

			}
			catch (Exception exception)
			{
				MyVrmAddin.TraceSource.TraceException(exception);
				
			}
			finally
			{
				ResumeLayout();
			}
		}

    	public const int MaxMsgNumber = 9; //????????

		ConfMessageCollection _messages = new ConfMessageCollection();
		List<RepositoryItemLookUpEdit> _durationEditors = new List<RepositoryItemLookUpEdit>();


		private RepositoryItemLookUpEdit CreateDurationRepositoryItem()
		{
			var lookUpEdit = new RepositoryItemLookUpEdit
			{
				DisplayMember = "Duartion",
				ValueMember = "Duartion",
				ShowHeader = false,
				ShowLines = false,
				PopupSizeable = false,
				TextEditStyle = TextEditStyles.DisableTextEditor,
				NullText = ""
			};
			lookUpEdit.Columns.Add(new LookUpColumnInfo("Duartion"));
			treeListMessages.RepositoryItems.Add(lookUpEdit);
			return lookUpEdit;
		}

		internal void SetDurationLookUpEdit(RepositoryItemLookUpEdit lookUpDurationEdit, int i)
		{
			if (lookUpDurationEdit != null)
			{
				switch (i%3)
				{
					case 1:
						lookUpDurationEdit.DataSource = repositoryItemComboBox_1_4_7_5Min.Items;
						break;
					case 2:
						lookUpDurationEdit.DataSource = repositoryItemComboBox_2_5_8_2Min.Items;
						break;
					case 0:
						lookUpDurationEdit.DataSource = repositoryItemComboBox_3_6_9_30Sec.Items;
						break;
				}
			}
		}

		void InitMessages()
		{
			if (MyVrmService.Service.OrganizationSettings.Preference.MCUEnchancedLimit > 0)
			{
				//var ret = MyVrmService.Service.GetConfMsg().DropboxContent;

				//repositoryItemComboBoxMessages.Items.Clear();
				//repositoryItemComboBox_1_4_7_5Min.Items.Clear();
				//repositoryItemComboBox_2_5_8_2Min.Items.Clear();
				//repositoryItemComboBox_3_6_9_30Sec.Items.Clear();
				//_messages.Clear();
				//foreach (var confMsg in ret)
				//{
				//    repositoryItemComboBoxMessages.Items.Add(confMsg.Text);
				//}
				////1,4,7 duration drop down will have - 5M duration values (5 to 30 minutes in 5 minute increments)
				//for (int i = 5; i <= 30; i += 5)
				//{
				//    repositoryItemComboBox_1_4_7_5Min.Items.Add(i + "M");
				//}
				////2,5,8 duration drop down will have  - 2M duration values (2 to 20 minutes in 2 minute increments)
				//for (int i = 2; i <= 20; i += 2)
				//{
				//    repositoryItemComboBox_2_5_8_2Min.Items.Add(i + "M");
				//}
				////3,6,9 duration drop down will have - 30s duration values (30 to 120 seconds in 30 second increments) 
				//for (int i = 30; i <= 120; i += 30)
				//{
				//    repositoryItemComboBox_3_6_9_30Sec.Items.Add(i + "s");
				//}
				_durationEditors.Clear();
				if (Conference != null)
				{
					ConfMessageCollection msgs = Conference.Conference.Id != null ?
						Conference.ConfMessageCollection : DefaultConfMessages; //MyVrmService.Service.GetNewConference().ConfMessages;
					for (int i = 1; i <= MaxMsgNumber; i++)
					{
						RepositoryItemLookUpEdit lookUpDurationEdit = CreateDurationRepositoryItem();
						SetDurationLookUpEdit(lookUpDurationEdit, i);
						_durationEditors.Insert(i - 1, lookUpDurationEdit);

						ConfMessage msg = new ConfMessage();
						msg.ControlID = i;
						var found = msgs.FirstOrDefault(a => a.ControlID == i);
						if( found != null)
						{
							msg = found;
						}
						else
						{
							msg.Checked = false;
							msg.TextMessage = (string) repositoryItemComboBoxMessages.Items[0];
							msg.Duartion = (string) ((ComboBoxItemCollection)lookUpDurationEdit.DataSource)[0];
						}
						_messages.Add(msg);
					}
					
					confMessageBindingSource.DataSource = _messages;
				}
			}

			treeListMessages.Enabled = MyVrmService.Service.OrganizationSettings.Preference.MCUEnchancedLimit > 0;
		}

    	private void AdditionalInfoPage_Load(object sender, System.EventArgs e)
    	{
           //ALLBUGS-138 start
                if (string.IsNullOrEmpty(textEntitycode.Text) && Conference.Entitycodestatusswitch == 0 && Conference.EntitycodeMandatory == true)
                {
                    errorProvider.SetError(textEntitycode, Strings.Entitycodeisrequired);
                }
                else
                {
                    errorProvider.SetError(textEntitycode, "");
                }

                if (string.IsNullOrEmpty(specialInstructionsEdit.Text) &&  Conference.SpecialInsMandatory == true)
                {
                    SpecerrorProvider.SetError(specialInstructionsEdit, Strings.SpecialInsisrequired);
                }
                else
                {
                    SpecerrorProvider.SetError(specialInstructionsEdit, "");
                }
                //ALLBUGS-138 End
    		InitConsiergeSupport();//MyVrmService.Service
            //MyVrmService.Service.GetCustomAttributeDefinition
            if (!MyVrmService.Service.OrganizationOptions.EnableActMsgDelivery)
            {
                layoutControlItem3.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layoutControlItem11.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            }
    	}

        private void specialInstructionsEdit_EditValueChanged(object sender, System.EventArgs e)
        {

            if (string.IsNullOrEmpty(specialInstructionsEdit.Text) && Conference.SpecialInsMandatory == true)
            {
                SpecerrorProvider.SetError(specialInstructionsEdit, Strings.SpecialInsisrequired);
            }
            else
            {
                SpecerrorProvider.SetError(specialInstructionsEdit, "");
            }
            //ZD 103423 start
            //if (!string.IsNullOrEmpty(specialInstructionsEdit.Text.Trim()))
            //{
            //    if (specialInstructionsEdit.Text.Length > 4000 )
            //    {
            //        //textEntitycode.ErrorText = Strings.EntitycodeContainsInvalidCharacters;
            //        SpecerrorProvider.SetError(specialInstructionsEdit, Strings.SpecialInstructionsLimit);
            //    }
            //    else
            //    {
            //        SpecerrorProvider.SetError(specialInstructionsEdit, "");
            //    }
            //}
            //ZD 103423 End 
        }

    	void specialInstructionsEdit_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
    	{
			if (e.OldValue != e.NewValue && e.OldValue != null)
			{
				if (Conference != null && Conference.Appointment != null)
					Conference.Appointment.SetNonOutlookProperty(true);
			}
    	}

		private void bnAdd_Click(object sender, System.EventArgs e)
		{
			Dictionary<int, string> selected = new Dictionary<int, string>();
			if (Conference != null && Conference.ConciergeSupportParams != null && Conference.ConciergeSupportParams.ConfVNOCOperators.Count > 0)
			{
				selected = Conference.ConciergeSupportParams.ConfVNOCOperators;
			}
			using (var dlg = new VNOCUsersDialog() { _VNOCUsers = _VNOCUsers,_selected = selected })
			{
				if( dlg.ShowDialog() == DialogResult.OK)
				{
					listBoxVNOCUsers.Items.Clear();
					foreach( var user in dlg.SelectedUsers)
					{
						listBoxVNOCUsers.Items.Add(user.Value);
					}
					if (Conference != null && Conference.Appointment != null)
						Conference.Appointment.SetNonOutlookProperty(true);
				}
			}
		}

		private void bnRemove_Click(object sender, System.EventArgs e)
		{
			int iNum = listBoxVNOCUsers.Items.Count;
			listBoxVNOCUsers.Items.Clear();
			foreach (var user in _VNOCUsers)
				user.CheckedState = false;

			if (iNum > 0 && Conference != null && Conference.Appointment != null)
				Conference.Appointment.SetNonOutlookProperty(true);
		}

		private void CheckStateChanged(object sender, System.EventArgs e)
		{
			if (Conference != null && Conference.Appointment != null)
				Conference.Appointment.SetNonOutlookProperty(true);
		}


		private void treeListMessages_CustomNodeCellEdit(object sender, DevExpress.XtraTreeList.GetCustomNodeCellEditEventArgs e)
		{
			if (e.Column == colTime )
			{
				//if (_durationEditors.Count > e.Node.Id)
				//    e.RepositoryItem = _durationEditors[e.Node.Id];
				if (_durationEditors.Count > e.Node.Id)
				{
					switch ((e.Node.Id + 1 ) % 3)
					{
						case 1:
							e.RepositoryItem = repositoryItemComboBox_1_4_7_5Min;
							break;
						case 2:
							e.RepositoryItem = repositoryItemComboBox_2_5_8_2Min;
							break;
						case 0:
							e.RepositoryItem = repositoryItemComboBox_3_6_9_30Sec;
							break;
					}
				}
			}
		}

		private void treeListMessages_CellValueChanging(object sender, DevExpress.XtraTreeList.CellValueChangedEventArgs e)
		{
			if (treeListMessages.FocusedNode != null)
			{
				ConfMessageCollection msgs = (ConfMessageCollection)confMessageBindingSource.DataSource;
				if (e.Column == colState)
				{
					if((bool)e.Value)
					{
						var res = _messages.Where(a => a.Checked && a.Duartion == msgs.ElementAt(treeListMessages.FocusedNode.Id).Duartion);
						if(res.Count() > 0)
						{
							UIHelper.ShowError(Strings.ConfSelectTimemsg);
							return;
						}
					}
					msgs.ElementAt(treeListMessages.FocusedNode.Id).Checked = (bool)e.Value;
					Conference.Appointment.SetNonOutlookProperty(true);
				}
				else
				{
					if (e.Column == colMessages)
					{
						msgs.ElementAt(treeListMessages.FocusedNode.Id).TextMessage = (string)e.Value;
						Conference.Appointment.SetNonOutlookProperty(true);
					}
					else
					{
						if (e.Column == colTime)
						{
							msgs.ElementAt(treeListMessages.FocusedNode.Id).Duartion = (string)e.Value;
							Conference.Appointment.SetNonOutlookProperty(true);
						}
					}
				}
			}
		}


        private void textEntitycode_EditValueChanged(object sender, System.EventArgs e)
        {
            if (string.IsNullOrEmpty(textEntitycode.Text) && Conference.Entitycodestatusswitch == 0 && Conference.EntitycodeMandatory == true)
            {
                errorProvider.SetError(textEntitycode, Strings.Entitycodeisrequired);
            }
            else
            {
                errorProvider.SetError(textEntitycode, "");
            }

            if (!string.IsNullOrEmpty(textEntitycode.Text.Trim()) && textEntitycode.Text.Length >= 3)
            {
                
                    textEntitycode.MaskBox.AutoCompleteCustomSource = collection;
            }
            else
            {
                textEntitycode.MaskBox.AutoCompleteCustomSource = Emptycollection;
            }

            if (!string.IsNullOrEmpty(textEntitycode.Text.Trim()))
            {
                if (Regex.IsMatch(textEntitycode.Text.Trim(), Strings.EntitycodeRegexPattern))
                {
                    //textEntitycode.ErrorText = Strings.EntitycodeContainsInvalidCharacters;
                    errorProvider.SetError(textEntitycode, Strings.EntitycodeContainsInvalidCharacters);
                }
                else
                {
                    errorProvider.SetError(textEntitycode, "");

                }
            }

        }
        
        //private void textEntitycode_OnDefocus(object sender, EventArgs e)
        //{
        //    if (!string.IsNullOrEmpty(textEntitycode.Text.Trim()))
        //    {
        //        if (Regex.IsMatch(textEntitycode.Text.Trim(), Strings.EntitycodeRegexPattern))
        //        {
        //            UIHelper.ShowError(Strings.EntitycodeContainsInvalidCharacters);
        //            //textEntitycode.Text = Regex.Replace(textEntitycode.Text.Trim(), Strings.EntitycodeRegexPattern, "");
        //        }
        //    }
        //}

        //private void textEntitycode_OnDefocus(object sender, EventArgs e)
        //{
        //    if (string.IsNullOrEmpty(textEntitycode.Text.Trim()))
        //    {
        //        lblEntitycode.Text = "";
        //        lblEntitycode.Visible = false;
        //    }

        //    if (!string.IsNullOrEmpty(textEntitycode.Text.Trim()))
        //    {
        //        if (_EntityCodes.Count > 0)
        //        {
        //            foreach (var user in _EntityCodes)
        //            {
        //                if (user.Caption.ToLower().Contains(textEntitycode.Text.ToLower()))
        //                {
        //                    if (textEntitycode.Text.ToLower() == user.Caption.ToLower())
        //                    {
        //                        lblEntitycode.Text = "";
        //                        lblEntitycode.Visible = false;
        //                        break;
        //                    }
        //                    else
        //                    {
        //                        if (textEntitycode.Text.Length > 2)
        //                        {
        //                            lblEntitycode.Text = Strings.InvalidEntityCode;
        //                            lblEntitycode.Visible = true;
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    if (textEntitycode.Text.ToLower() != user.Caption.ToLower() && textEntitycode.Text.Length > 2)
        //                    {
        //                        lblEntitycode.Text = Strings.InvalidEntityCode;
        //                        lblEntitycode.Visible = true;
        //                    }
        //                }
        //            }
        //        }
        //    }
        //}
    
    }
}
