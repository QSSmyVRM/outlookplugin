﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;

namespace MyVrm.WebServices.Data
{
    public class CateringServiceCollection : BaseCollection<CateringService>
    {
        internal CateringServiceCollection()
        {
        }

        #region Overrides of BaseCollection<CateringService>

        internal override void LoadFromXml(MyVrmServiceXmlReader reader)
        {
            do
            {
                reader.Read();
                if (reader.LocalName == "Service")
                {
                    var service = new CateringService();
                    do
                    {
                        reader.Read();
                        if (reader.LocalName == "ID")
                        {
                            service.Id = reader.ReadElementValue<int>();
                        }
                        else if(reader.LocalName == "Name")
                        {
                            service.Name = reader.ReadElementValue();
                        }
                        else
                        {
                            reader.SkipCurrentElement();
                        }
                    } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "Service"));
                    Items.Add(service);
                }
                else
                {
                    reader.SkipCurrentElement();
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "CateringServices"));
        }

        #endregion
    }
}
