﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections;
using System.Collections.Generic;

namespace MyVrm.WebServices.Data
{
    /// <summary>
    /// Represents organization options.
    /// </summary>
    public class OrganizationOptions
    {
        /// <summary>
        /// Represents contact person details.
        /// </summary>
        public class ContactDetails : ComplexProperty
        {
            /// <summary>
            /// Name of contact person appearing on the bottom of each window.
            /// </summary>
            public string Name { get; set; }
            /// <summary>
            /// Email for tech support contact appearing on the bottom of each window.
            /// </summary>
            public string Email { get; set; }
            /// <summary>
            /// Phone number for tech support contact appearing on the bottom of each window.
            /// </summary>
            public string Phone { get; set; }
            /// <summary>
            /// Any additional information.
            /// </summary>
            public string AdditionInfo { get; set; }

			public override object Clone()
			{
				ContactDetails copy = new ContactDetails();
				copy.AdditionInfo = string.Copy(AdditionInfo);
				copy.Email = string.Copy(Email);
				copy.Name = string.Copy(Name);
				copy.Phone = string.Copy(Phone);
				
				return copy;
			}

            internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
            {
                switch(reader.LocalName)
                {
                    case "Name":
                    {
                        Name = reader.ReadElementValue();
                        return true;
                    }
                    case "Email":
                    {
                        Email = reader.ReadElementValue();
                        return true;
                    }
                    case "Phone":
                    {
                        Phone = reader.ReadElementValue();
                        return true;
                    }
                    case "AdditionInfo":
                    {
                        AdditionInfo = reader.ReadElementValue();
                        return true;
                    }
                }
                return false;
            }
        }
        /// <summary>
        /// Represents system availablity options.
        /// </summary>
        public class SystemAvailableTime : ComplexProperty
        {
			private int _systemTimeZoneID;

			public override object Clone()
			{
				SystemAvailableTime copy = new SystemAvailableTime();
				copy._systemTimeZoneID = _systemTimeZoneID;
				copy.DaysClosed = DaysClosed;
				copy.EndTimeInLocalTimezone = new TimeSpan(EndTimeInLocalTimezone.Ticks);
				copy.EndTimeInServerTimezone = new TimeSpan(EndTimeInServerTimezone.Ticks);
				copy.StartTimeInLocalTimezone = new TimeSpan(StartTimeInLocalTimezone.Ticks);
				copy.StartTimeInServerTimezone = new TimeSpan(StartTimeInServerTimezone.Ticks);
				copy.IsOpen24Hours = IsOpen24Hours;

				return copy;
			}

			public void SetupSystemTimeZoneID(int systemTimeZoneID)
			{
				_systemTimeZoneID = systemTimeZoneID;
				TimeZoneInfo ti = TimeZoneConvertion.ConvertToTimeZoneInfo(_systemTimeZoneID);
				StartTimeInLocalTimezone = new TimeSpan(TimeZoneInfo.ConvertTimeBySystemTimeZoneId(new DateTime(StartTimeInServerTimezone.Ticks), ti.Id, TimeZoneInfo.Local.Id).Ticks);
				EndTimeInLocalTimezone = new TimeSpan(TimeZoneInfo.ConvertTimeBySystemTimeZoneId(new DateTime(EndTimeInServerTimezone.Ticks), ti.Id, TimeZoneInfo.Local.Id).Ticks);
			}
            /// <summary>
            /// If true, rooms can be booked any day and any hour of the day.
            /// </summary>
            public bool IsOpen24Hours { get; set; }
            /// <summary>
			/// The start time and during which conferences can run - server time zone.
            /// </summary>
			public TimeSpan StartTimeInServerTimezone { get; set; }
			/// <summary>
			/// The end time and during which conferences can run - server time zone.
			/// </summary>
			public TimeSpan EndTimeInServerTimezone { get; set; }
            /// <summary>
			/// The start time and during which conferences can run - local time zone.
			/// </summary>
			public TimeSpan StartTimeInLocalTimezone { get; set; }
			/// <summary>
            /// The end time and during which conferences can run - local time zone.
            /// </summary>
            public TimeSpan EndTimeInLocalTimezone { get; set; }
            /// <summary>
            /// Any days of the week for which conferences cannot be booked.
            /// </summary>
            public DaysOfWeek DaysClosed { get; set; }

            internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
            {
                switch(reader.LocalName)
                {
                    case "IsOpen24Hours":
                    {
                        IsOpen24Hours = reader.ReadElementValue<bool>();
                        return true;
                    }
                    case "StartTime":
                    {
						StartTimeInServerTimezone = Utilities.TimeOfDayStringToTimeSpan(reader.ReadElementValue());
                        return true;
                    }
                    case "EndTime":
                    {
						EndTimeInServerTimezone = Utilities.TimeOfDayStringToTimeSpan(reader.ReadElementValue());
                        return true;
                    }
                    case "DaysClosed":
                    {
                        DaysClosed = Utilities.StringToDaysOfWeek(reader.ReadElementValue());
                        return true;
                    }
                }
                return false;
            }
        }

        public OrganizationOptions()
        {
            Contact = new ContactDetails();
            AvailableTime = new SystemAvailableTime();
        }

        public ContactDetails Contact { get; private set; }
        /// <summary>
        /// If true, any approved conference modified in any way does not require reapproval.
        /// </summary>
        public bool AutoAcceptModifiedConference { get; set; }
        /// <summary>
        /// Allows users to schedule recurring conferences
        /// </summary>
        public bool IsRecurringConferenceEnabled { get; set; }
        /// <summary>
        /// If true, and if conference is labeled as public, then the Enable Open for Registration field is available. 
        /// This field enables anyone with access to the website to register for the conference.
        /// </summary>
        public bool IsDynamicInviteEnabled { get; set; }
        /// <summary>
        /// Enables point-to-point conferences.
        /// </summary>
        public bool IsPointToPointConferenceEnabled { get; set; }
        /// <summary>
        /// Legacy. Do not use.
        /// </summary>
        public bool IsRealtimeDisplayEnabled { get; set; }
        /// <summary>
        /// Enables user to set dial-out for a conference. If dial-out not enabled, users cannot set dial-out for any conferences.
        /// </summary>
        public bool IsDialoutEnabled { get; set; }
        /// <summary>
        /// Defaults all conferences to public. 
        /// </summary>
        public bool IsDefaultConferenceAsPublic { get; set; }
        /// <summary>
        /// Defaults conference type.
        /// </summary>
        public ConferenceType DefaultConferenceType { get; set; }

        public ConferenceType[] EnabledConferenceTypes
        {
            get
            {
                var enabledConferenceTypes = new List<ConferenceType>();
                if (IsRoomConferenceEnabled)
                {
                    enabledConferenceTypes.Add(ConferenceType.RoomConference);
                }
                if (IsPointToPointConferenceEnabled)
                {
                    enabledConferenceTypes.Add(ConferenceType.PointToPoint);
                }
                if (IsAudioOnlyConferenceEnabled)
                {
                    enabledConferenceTypes.Add(ConferenceType.AudioOnly);
                }
                if (IsAudioVideoConferenceEnabled)
                {
                    enabledConferenceTypes.Add(ConferenceType.AudioVideo);
					enabledConferenceTypes.Add(ConferenceType.VMR);
                }
                return enabledConferenceTypes.ToArray();
            }
        }
        /// <summary>
        /// Enables room conferences.
        /// </summary>
        public bool IsRoomConferenceEnabled { get; set; }
        /// <summary>
        /// Enables Audio-Video conferences.
        /// </summary>
        public bool IsAudioVideoConferenceEnabled { get; set; }
        /// <summary>
        /// Enables Audio-only conferences.
        /// </summary>
        public bool IsAudioOnlyConferenceEnabled { get; set; }
        public int DefaultCalendarToOfficeHours { get; set; }
        public string RoomTreeExpandLevel { get; set; }
        public bool IsCustomOptionEnabled { get; set; }
        public bool IsBufferZoneEnabled { get; set; }
        public string IsConferenceCodeEnabled { get; set; }
        public string IsLeaderPinEnabled { get; set; }
        public bool IsAdvancedAudioVideoParamsEnabled { get; set; }
        public bool IsAudioParamsEnabled { get; set; }
        public SystemAvailableTime AvailableTime { get; private set; }
        public byte[] MailLogoImage { get; set; }
       // public int DefaultConfDuration { get; set; }
        public int DefaultConfDuration { get; set; }//ZD 101749 starts
        internal void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElement)
        {
            do
            {
                reader.Read();
                switch(reader.LocalName)
                {
                    case "ContactDetails":
                    {
                        Contact.LoadFromXml(reader, reader.LocalName);
                        break;
                    }
                    case "AutoAcceptModifiedConference":
                    {
                        AutoAcceptModifiedConference = reader.ReadElementValue<bool>();
                        break;
                    }
                    case "EnableRecurringConference":
                    {
                        IsRecurringConferenceEnabled = reader.ReadElementValue<bool>();
                        break;
                    }
                    case "EnableDynamicInvite":
                    {
                        IsDynamicInviteEnabled = reader.ReadElementValue<bool>();
                        break;
                    }
                    case "EnableP2PConference":
                    {
                        IsPointToPointConferenceEnabled = reader.ReadElementValue<bool>();
                        break;
                    }
                    case "EnableRealtimeDisplay":
                    {
                        IsRealtimeDisplayEnabled = reader.ReadElementValue<bool>();
                        break;
                    }
                    case "EnableDialout":
                    {
                        IsDialoutEnabled = reader.ReadElementValue<bool>();
                        break;
                    }
                    case "DefaultConferencesAsPublic":
                    {
                        IsDefaultConferenceAsPublic = reader.ReadElementValue<bool>();
                        break;
                    }
                    case "DefaultConferenceType":
                    {
                        DefaultConferenceType = reader.ReadElementValue<ConferenceType>();
                        break;
                    }
                    case "EnableRoomConference":
                    {
                        IsRoomConferenceEnabled = reader.ReadElementValue<bool>();
                        break;
                    }
                    case "EnableAudioVideoConference":
                    {
                        IsAudioVideoConferenceEnabled = reader.ReadElementValue<bool>();
                        break;
                    }
                    case "EnableAudioOnlyConference":
                    {
                        IsAudioOnlyConferenceEnabled = reader.ReadElementValue<bool>();
                        break;
                    }
                    case "DefaultCalendarToOfficeHours":
                    {
                        DefaultCalendarToOfficeHours = reader.ReadElementValue<int>();
                        break;
                    }
                    case "RoomTreeExpandLevel":
                    {
                        RoomTreeExpandLevel = reader.ReadElementValue();
                        break;
                    }
                    case "EnableCustomOption":
                    {
                        IsCustomOptionEnabled = reader.ReadElementValue<bool>();
                        break;
                    }
                    case "EnableBufferZone":
                    {
                        IsBufferZoneEnabled = reader.ReadElementValue<bool>();
                        break;
                    }
                    case "ConferenceCode":
                    {
                        IsConferenceCodeEnabled = reader.ReadElementValue();
                        break;
                    }
                    case "LeaderPin":
                    {
                        IsLeaderPinEnabled = reader.ReadElementValue();
                        break;
                    }
                    case "AdvAvParams":
                    {
                        IsAdvancedAudioVideoParamsEnabled = reader.ReadElementValue<bool>();
                        break;
                    }
                    case "AudioParams":
                    {
                        IsAudioParamsEnabled = reader.ReadElementValue<bool>();
                        break;
                    }
                    case "SystemAvailableTime":
                    {
                        AvailableTime.LoadFromXml(reader, reader.LocalName);
                        break;
                    }
                    case "MailLogoImage":
                    {
                        MailLogoImage = reader.ReadBase64ElementValue();
                        break;
                    }
                    case "DefaultConfDuration"://ZD 101749 starts
                    {
                        DefaultConfDuration = reader.ReadElementValue<int>();
                        break;
                    }//ZD 101749 Ends
                    default:
                    {
                        reader.SkipCurrentElement();
                        break;
                    }
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, xmlElement));
        }
    }
}
