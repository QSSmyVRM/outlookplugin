﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	internal class TDGetTemplateRequest : ServiceRequestBase<TDGetTemplateResponse>
	{
		internal int _id;
		internal TDGetTemplateRequest(MyVrmService service)
			: base(service)
		{
		}

		#region Overrides of ServiceRequestBase

		internal override string GetXmlElementName()
		{
			return "TDGetTemplate";
		}

		internal override string GetCommandName()
		{
			return Constants.TDGetTemplateCommandName;
		}

		internal override string GetResponseXmlElementName()
		{
			return "TDGetTemplate";
		}

		internal override TDGetTemplateResponse ParseResponse(MyVrmServiceXmlReader reader)
		{
			var response = new TDGetTemplateResponse(); 
			response.LoadFromXml(reader, GetResponseXmlElementName());
			return response;
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			UserId.WriteToXml(writer, "userId");
			writer.WriteElementValue(XmlNamespace.NotSpecified, "templateId", _id);
		}
		#endregion
	}

	public class TDGetTemplateResponse : ServiceResponse
	{
		public TDBTemplateInfo tdbTemplateInfo = new TDBTemplateInfo();
		//public bool SetDefault {set; get;}
		public bool Public { set; get; }
		public string Description { set; get; }
		public string RetMessage { set; get; }
        public int RetCode { set; get; }
		public long SettingsVersion { set; get; }
		public XMLError XMLError { set; get; }
		public int Id { set; get; }
		public string Name { set; get; }

		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{
			bool bErrorTransferred = false;
			while (!reader.IsEndElement(XmlNamespace.NotSpecified, "templateInfo") && !bErrorTransferred) 
			{
				try
				{
					reader.Read();
					switch (reader.LocalName)
					{
						case "id":
							Id = reader.ReadElementValue<int>();
							break;
						case "name":
							Name = reader.ReadElementValue();
							break;
						//case "setDefault":
						//    {
						//        string val = reader.ReadElementValue();
						//        SetDefault = string.Compare(val, "false", true) == 0 ? false : true; 
						//        break;
						//    }
						case "public":
							Public = reader.ReadElementValue<bool>();
							break;
						case "description":
							Description = reader.ReadElementValue();
							break;
						case "retState":
							bErrorTransferred = true;
							break;
						default:
							reader.SkipCurrentElement();
							//reader.Read();
							break;
					}
				}
				catch (Exception ex)
				{
					MyVrmService.TraceSource.TraceError(reader.LocalName + ": " + ex.Message, null);
					reader.Read();
					//throw;
				}
			}

			while (!reader.IsEndElement(XmlNamespace.NotSpecified, "templateData") && !bErrorTransferred)
			{
				try
				{
					if (reader.LocalName == "templateData")
					{
						tdbTemplateInfo.LoadFromXml(reader, "templateData");
						break;
					}
					reader.Read();
				}
				catch (Exception ex)
				{
					MyVrmService.TraceSource.TraceError(reader.LocalName + ": " + ex.Message, null);
					reader.Read();
					//throw;
				}
			}
			
			while (!reader.IsEndElement(XmlNamespace.NotSpecified, "retState"))
			{
				try
				{
					switch (reader.LocalName)
					{
						case "retCode":
							RetCode = reader.ReadElementValue<int>();
							break;
						case "retMessage":
							RetMessage = reader.ReadElementValue();
							break;
						case "settingsVersion":
							SettingsVersion = reader.ReadElementValue<long>();
							break;
						case "error":
							XMLError = new XMLError();
							XMLError.LoadFromXml(reader, XmlNamespace.NotSpecified, "error");
							break;
					}
					reader.Read();
				}
				catch (Exception ex)
				{
					MyVrmService.TraceSource.TraceError(reader.LocalName + ": " + ex.Message, null);
					reader.Read();
					//throw;
				}
			}
		}
	}
}
