﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	public class TDBTemplate : ComplexProperty
	{
		public int Id;
		//public string Descr;
		public string Name;
		//public bool IsPublic;

		public override object Clone()
		{
			TDBTemplate copy = new TDBTemplate();
			copy.Id = Id;
			//copy.IsPublic = IsPublic;
			copy.Name = Name;
			//copy.Descr = Descr;
			return copy;
		}

		internal override void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
		{
			while (!reader.IsEndElement(XmlNamespace.NotSpecified, "template"))
			{
				reader.Read();
				switch (reader.LocalName)
				{
					case "name":
						Name = reader.ReadElementValue();
						break;
					//case "description":
					//    Descr = reader.ReadElementValue();
					//    break;
					case "id":
						Id = reader.ReadElementValue<int>();
						break;
					//case "public":
					//    IsPublic = reader.ReadElementValue<bool>();
					//    break;
					default: 
						reader.SkipCurrentElement();
						break;
				}
			}
		}

		//internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		//{
		//    writer.WriteStartElement(XmlNamespace.NotSpecified, "template");
		//    writer.WriteElementValue(XmlNamespace.NotSpecified, "name", Name);
		//    writer.WriteElementValue(XmlNamespace.NotSpecified, "Description", Descr);
		//    writer.WriteElementValue(XmlNamespace.NotSpecified, "Public", IsPublic);
		//    writer.WriteEndElement();
		//}
	}

	public class TDBTemplateCollection : ICollection<TDBTemplate>
	{
		private readonly List<TDBTemplate> _templateList = new List<TDBTemplate>();

		#region Implementation of IEnumerable

		public IEnumerator<TDBTemplate> GetEnumerator()
		{
			return _templateList.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		#endregion

		#region Implementation of ICollection<TDBTemplate>

		public void Add(TDBTemplate item)
		{
			_templateList.Add(item);
		}

		public void Clear()
		{
			_templateList.Clear();
		}

		public bool Contains(TDBTemplate item)
		{
			return _templateList.Contains(item);
		}

		public void CopyTo(TDBTemplate[] array, int arrayIndex)
		{
			_templateList.CopyTo(array, arrayIndex);
		}

		public bool Remove(TDBTemplate item)
		{
			return _templateList.Remove(item);
		}

		public int Count
		{
			get { return _templateList.Count; }
		}

		public bool IsReadOnly
		{
			get { return false; }
		}

		#endregion
	}

	public class TDBTemplateInfo : ComplexProperty
	{
		public string Subj { get; set; }
		public int Duration { get; set; }
		public DateTime StartTime { get; set; }
		public string Body;

		public ParticipantCollection Participans { get; set; }
		
		public override object Clone()
		{
			TDBTemplateInfo copy = new TDBTemplateInfo();
			copy.Body = string.Copy(Body);
			copy.Duration = Duration;
			copy.StartTime = StartTime;
			copy.Subj = Subj;
			copy.Participans = Participans;
			return copy;
		}

		internal override void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
		{
			while (!reader.IsEndElement(XmlNamespace.NotSpecified, "templateData"))
			{
				reader.Read();
				switch (reader.LocalName)
				{
					case "subject":
						Subj = reader.ReadElementValue();
						break;
					case "durationMin":
						Duration = reader.ReadElementValue<int>();
						break;
					case "startTime":
						StartTime = reader.ReadElementValue<DateTime>();
						StartTime = StartTime.ToLocalTime();
						break;
					case "body":
						Body = reader.ReadElementValue();
						break;
					case "partys":
						if (Participans == null)
							Participans = new ParticipantCollection();
						Participans.LoadFromXml(reader, "partys");
						break;
					default:
						reader.SkipCurrentElement();
						break;
				}
			}
		}
	}
}
