﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System.Text;

namespace MyVrm.Common.Extensions
{
    public static class ByteArrayExtensions
    {
        private static readonly byte[] NibbleToHex = Encoding.ASCII.GetBytes("0123456789ABCDEF");
        private const byte SpaceDelimiter = 0x20;

        public static string ToHexString(this byte[] array)
        {
            return ToHexString(array, false);
        }

        public static string ToHexString(this byte[] array, bool spaceDelimiter)
        {
            return ToHexString(array, 0, array.Length, spaceDelimiter);
        }

        public static string ToHexString(this byte[] array, int start, int count)
        {
            return ToHexString(array, start, count, false);
        }

        public static string ToHexString(this byte[] array, int start, int count, bool spaceDelimiter)
        {
            if (array == null)
            {
                return null;
            }
            var size = count * 2;
            if (count > 0 && spaceDelimiter)
            {
                size += (count - 1);
            }
            var bytes = new byte[size];
            var num = 0;
            for (var i = start; i < (start + count); i++)
            {
                if (i > start && spaceDelimiter)
                    bytes[num++] += SpaceDelimiter;
                bytes[num++] = NibbleToHex[array[i] >> 4];
                bytes[num++] = NibbleToHex[array[i] & 15];
            }
            return Encoding.ASCII.GetString(bytes);
        }
    }
}
