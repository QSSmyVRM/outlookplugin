﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace MyVrm.WebServices.Data
{
    internal class GetRoomSetsRequest : ServiceRequestBase<GetRoomSetsResponse>
    {
        internal GetRoomSetsRequest(MyVrmService service) : base(service)
        {
        }

        internal RoomId RoomId { get; set; }
        internal RoomSetType SetType { get; set; }

        #region Overrides of ServiceRequestBase

        internal override string GetXmlElementName()
        {
            return "login";
        }

        internal override string GetCommandName()
        {
            return "GetRoomSets";
        }

        internal override string GetResponseXmlElementName()
        {
            return "SetList";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            UserId.WriteToXml(writer);
            RoomId.WriteToXml(writer);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "Type", SetType);
        }

        #endregion
    }
}