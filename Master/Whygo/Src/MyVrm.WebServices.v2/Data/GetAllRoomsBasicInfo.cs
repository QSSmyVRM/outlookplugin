﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;

namespace MyVrm.WebServices.Data
{
	internal class GetAllRoomsBasicInfoRequest : ServiceRequestBase<GetAllRoomsBasicInfoResponse>
	{
		internal GetAllRoomsBasicInfoRequest(MyVrmService service)
			: base(service)
		{
		}

		internal RoomId RoomId { get; set; }

		#region Overrides of ServiceRequestBase

		internal override string GetXmlElementName()
		{
			return "GetAllRoomsBasicInfo";
		}

		internal override string GetCommandName()
		{
			return Constants.GetAllRoomsBasicInfoCommandName;
		}

		internal override string GetResponseXmlElementName()
		{
			return "Rooms";
		}

		internal override GetAllRoomsBasicInfoResponse ParseResponse(MyVrmServiceXmlReader reader)
		{
			var response = new GetAllRoomsBasicInfoResponse { Room = new Room(Service) };
			response.LoadFromXml(reader, "Room");// GetResponseXmlElementName());
			return response;
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			UserId.WriteToXml(writer, "UserID");
			if(RoomId != null)
				RoomId.WriteToXml(writer, "RoomID");
		}

		#endregion
	}

	internal class GetAllRoomsBasicInfoResponse : ServiceResponse
	{
		internal Room Room { get; set; }

		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{
			Room.LoadFromXml(reader, true, "Room");
		}
	}
}