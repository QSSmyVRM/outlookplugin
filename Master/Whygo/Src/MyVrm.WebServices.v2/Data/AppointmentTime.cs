﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;

namespace MyVrm.WebServices.Data
{
    /*internal*/public class AppointmentTime : ComplexProperty
    {
        private const string StartHourElementName = "startHour";
        private const string StartMinuteElementName = "startMin";
        private const string StartSetElementName = "startSet";
        private const string DurationElementName = "durationMin";
        private const string SetupDurationElementName = "setupDuration";
        private const string TeardownDurationElementName = "teardownDuration";
        private const string TimeZoneElementName = "timeZone";
        private const string CustomInstanceElementName = "customInstance";
        private const string InstancesElementName = "instances";
        private OccurrenceInfoCollection _modifiedOccurrences;
        private int _startHour;
        private int _startMinute;
        private string _startSet = Constants.BeforeNoonAbbr;
        private TimeSpan? _startTime;
        private TimeSpan? _endTime;
        private TimeSpan _duration;

        public TimeZoneInfo TimeZone { get; set; }
        
        public TimeSpan StartTime
        {
            get
            {
                if (_startTime == null)
                {
					_startTime = Utilities.BuildTimeOfDay(_startHour, _startMinute, _startSet, false /*because _startHour already comes in local system*/);//, MyVrmService.Service.UtcEnabled == 1);
                }
                return _startTime.Value;
            }
            set { _startTime = value; }
        }
        
        public TimeSpan EndTime
        {
            get
            {
                if (_endTime == null)
                {
                    _endTime = StartTime + _duration;
                }
                return _endTime.Value;
            }
            set
            {
                if (value < StartTime)
                {
                    throw new Exception(Strings.ConferenceEndDateOccursBeforeStartDate);
                }
                _endTime = value;
            }
        }

        public TimeSpan SetupDuration { get; set; }
        public TimeSpan TeardownDuration { get; set; }

        public TimeSpan Duration
        {
            get
            {
                return EndTime - StartTime; 
            }
            /*private*/ set { _duration = value; }
        }

        internal OccurrenceInfoCollection ModifiedOccurrences
        {
            get { return _modifiedOccurrences ?? (_modifiedOccurrences = new OccurrenceInfoCollection()); }
        }

        private bool IsCustom
        {
            get { return ModifiedOccurrences.Count > 0; }
        }

        internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
        {
            switch (reader.LocalName)
            {
                case TimeZoneElementName:
                {
                    TimeZone = TimeZoneConvertion.ConvertToTimeZoneInfo(reader.ReadElementValue<int>());
                    return true;
                }
                case StartHourElementName:
                {
                    _startHour = reader.ReadElementValue<int>();
                    return true;
                }
                case StartMinuteElementName:
                {
                    _startMinute = reader.ReadElementValue<int>();
                    return true;
                }
                case StartSetElementName:
                {
                    _startSet = reader.ReadElementValue();
                    return true;
                }
                case DurationElementName:
                {
                    Duration = TimeSpan.FromMinutes(reader.ReadElementValue<int>());
                    return true;
                }
                case SetupDurationElementName:
                {
                    SetupDuration = TimeSpan.FromMinutes(reader.ReadElementValue<int>());
                    return true;
                }
                case TeardownDurationElementName:
                {
                    TeardownDuration = TimeSpan.FromMinutes(reader.ReadElementValue<int>());
                    return true;
                }
                case CustomInstanceElementName:
                {
                    var isCustom = reader.ReadElementValue<bool>();
                    if (isCustom)
                    {
                        ModifiedOccurrences.LoadFromXml(reader, InstancesElementName);
                    }
                    return true;
                }
            }
            return false;
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            // This is custom appointment time so we need to write customized instances
            if (IsCustom)
            {
                writer.WriteElementValue(Namespace, CustomInstanceElementName, IsCustom);
                var apptTime = new AppointmentTime
                                   {
                                       StartTime = StartTime,
                                       EndTime = EndTime,
                                       TimeZone = TimeZone,
                                       SetupDuration = SetupDuration,
                                       TeardownDuration = TeardownDuration
                                   };
                ModifiedOccurrences.AppointmentTime = apptTime;
                ModifiedOccurrences.WriteToXml(writer, InstancesElementName);
            }
            else
            {
                writer.WriteElementValue(Namespace, TimeZoneElementName, TimeZoneConvertion.ConvertToTimeZoneId(TimeZone));
                writer.WriteElementValue(Namespace, StartHourElementName, (StartTime.Hours > 12 ? StartTime.Hours - 12 : StartTime.Hours).ToString("D2"));
                writer.WriteElementValue(Namespace, StartMinuteElementName, StartTime.Minutes.ToString("D2"));
                writer.WriteElementValue(Namespace, StartSetElementName, Utilities.GetNoonAbbr(StartTime));
                writer.WriteElementValue(Namespace, DurationElementName, Duration.TotalMinutes);
                writer.WriteElementValue(Namespace, SetupDurationElementName, SetupDuration.TotalMinutes);
                writer.WriteElementValue(Namespace, TeardownDurationElementName, TeardownDuration.TotalMinutes);
                base.WriteElementsToXml(writer);
            }
        }

		public override object Clone()//CopyTo(AppointmentTime dest)
		{
			AppointmentTime dest = new AppointmentTime();
			//if( _endTime != null)
			//    dest._endTime = new TimeSpan(_endTime.Value.Ticks) ;
			//dest._startHour = _startHour;
			//dest._startMinute = _startMinute;
			//dest._startSet = _startSet;
			//if (_startTime != null)
			//    dest._startTime = new TimeSpan(_startTime.Value.Ticks);
			dest.EndTime = EndTime;
			dest.StartTime = StartTime;
			dest.Duration = new TimeSpan(Duration.Ticks);
			dest.SetupDuration = new TimeSpan(SetupDuration.Ticks);
			dest.TeardownDuration = new TimeSpan(TeardownDuration.Ticks);
			dest.TimeZone = TimeZoneInfo.FindSystemTimeZoneById(TimeZone.Id);
			dest._modifiedOccurrences = new OccurrenceInfoCollection();
			foreach (var occur in _modifiedOccurrences)
			{
				OccurrenceInfo //ocCopy = new OccurrenceInfo();
				ocCopy = (OccurrenceInfo)occur.Clone();
				dest._modifiedOccurrences.Add(ocCopy);
			}
			return dest;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(this, obj))
				return true;

			AppointmentTime appointmentTime = obj as AppointmentTime;
			if (appointmentTime == null)
				return false;
			//if (appointmentTime._duration != _duration)
			//    return false;
			if (appointmentTime.Duration != Duration)
				return false;
			if (appointmentTime.ModifiedOccurrences != null && ModifiedOccurrences == null ||
				appointmentTime.ModifiedOccurrences == null && ModifiedOccurrences != null)
				return false;
			if (appointmentTime.ModifiedOccurrences != null && ModifiedOccurrences != null &&
					appointmentTime.ModifiedOccurrences.Count != ModifiedOccurrences.Count)
				return false;
			if (appointmentTime.ModifiedOccurrences != null && ModifiedOccurrences != null)
			{
				for (int i = 0; i < ModifiedOccurrences.Count; i++)
				{
					if (!ModifiedOccurrences[i].Equals(appointmentTime.ModifiedOccurrences[i]))
						return false;
				}
			}
			if (appointmentTime.StartTime != StartTime)
				  return false;
			if (appointmentTime.EndTime != EndTime)
				return false;
			if (appointmentTime.SetupDuration != SetupDuration)
				return false;
			if (appointmentTime.TeardownDuration != TeardownDuration)
				return false;
			if (!appointmentTime.TimeZone.Equals(TimeZone))
				return false;
			//if (appointmentTime._startHour != _startHour)
			//    return false;
			//if (appointmentTime._startMinute != _startMinute)
			//    return false;
			//if (appointmentTime._startSet != _startSet)
			//    return false;
			//if (appointmentTime._startTime != _startTime)
			//    return false;
			//if (appointmentTime.IsCustom != IsCustom)
			//    return false;
			
			return true;
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
    }
}
