using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing.Design;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace MyVrm.DataImport.Excel.WinForms
{
    /// <summary>
    /// Represents a Windows list view control, which displays a collection of items that can be displayed using one of four different views.
    /// </summary>
    [DesignTimeVisible(true)]
    [ToolboxItem(true)]
    public class DataListView : ListView
    {
        private CurrencyManager dataManager = null;
        private string dataMember = String.Empty;
        private object dataSource = null;
        private readonly WinForms.ColumnHeaderCollection columnHeaderCollection;
        private IBindingList bindingList;
        private string sortProperty;
        private ListSortDirection sortDirection;
        private HandleRef headerHandle;

        /// <summary>
        /// Initializes a new instance of the ListView class.
        /// </summary>
        public DataListView()
        {
            sortProperty = string.Empty;
            columnHeaderCollection = new WinForms.ColumnHeaderCollection(this);
            View = View.Details;
        }

        #region Public Properties

        /// <summary>
        /// Gets the collection of all column headers that appear in the control.
        /// </summary>
        /// <value>A ColumnHeaderCollection that represents the column headers that appear when the View property is set to View.Details.</value>
        [Category("Behavior")]
        [MergableProperty(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public new WinForms.ColumnHeaderCollection Columns
        {
            get { return columnHeaderCollection; }
        }

        /// <summary>
        /// Gets or sets the specific list in a DataSource for which the ListView control displays a list.
        /// </summary>
        /// <value>A list in a DataSource. The default is an empty string ("").</value>
        [Category("Data")]
        [Editor("System.Windows.Forms.Design.DataMemberListEditor,System.Design",
            typeof (UITypeEditor))]
        public string DataMember
        {
            get { return dataMember; }
            set
            {
                if (dataMember == value)
                {
                    return;
                }
                dataMember = value;
            }
        }

        /// <summary>
        /// Gets or sets the data source that the list is displaying data for.
        /// </summary>
        /// <value>An object that functions as a data source.</value>
        [Category("Data")]
        [RefreshProperties(RefreshProperties.Repaint)]
        [TypeConverter("System.Windows.Forms.Design.DataSourceConverter,System.Design")]
        public object DataSource
        {
            get { return dataSource; }
            set
            {
                if (value != null && (value as IList) == null)
                {
                    if ((value as IListSource) == null)
                    {
                        throw new Exception(Strings.BadDataSourceForComplexBinding);
                    }
                }
                if (value == dataSource)
                {
                    return;
                }
                SetDataConnection(value, true);
            }
        }

        [DefaultValue(ListSortDirection.Ascending), Category("Data")]
        public ListSortDirection SortDirection
        {
            get { return sortDirection; }
            set
            {
                if (SortDirection != value)
                {
                    sortDirection = value;
                    ApplySort();
                }
            }
        }
        [Editor("System.Windows.Forms.Design.DataMemberFieldEditor, System.Design", typeof(UITypeEditor)), Category("Data"), DefaultValue("")]
        public string SortProperty
        {
            get { return sortProperty; }
            set
            {
                if (value == null)
                {
                    value = "";
                }
                if (SortProperty != value)
                {
                    sortProperty = value;
                    ApplySort();
                }
            }
        }

        #endregion

        internal HandleRef HeaderHandle
        {
            get { return headerHandle; }
        }

        #region Public Events

        /// <summary>
        /// Occurs when the DataSource property value has changed.
        /// </summary>
        [Category("Behavior")]
        [Description("Occurs when the DataSource property value has changed")]
        public event EventHandler DataSourceChanged;

        #endregion

        protected virtual void OnDataSourceChanged(EventArgs e)
        {
            if (DataSourceChanged != null)
            {
                DataSourceChanged(this, e);
            }
        }

        public event ItemCheckedEventHandler UpdateItem;

        protected virtual void OnUpdateItem(ItemCheckedEventArgs e)
        {
            if (UpdateItem != null)
            {
                UpdateItem(this, e);
            }
        }

        protected override void OnBindingContextChanged(EventArgs e)
        {
            SetDataConnection(dataSource, true);
            base.OnBindingContextChanged(e);
        }

        protected CurrencyManager DataManager
        {
            get { return dataManager; }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                DataSource = null;
            }
            base.Dispose(disposing);
        }

        protected override void OnColumnClick(ColumnClickEventArgs e)
        {
            ColumnHeader header = Columns[e.Column];
            if (header != null)
            {
                if (SupportsSorting && header.IsSortable)
                {
                    PropertyDescriptor descriptor = DataManager.GetItemProperties()[header.DisplayMember];
                    if (descriptor != null && typeof(IComparable).IsAssignableFrom(descriptor.PropertyType))
                    {
                        ApplySort(descriptor.Name);
                    }
                }
            }
            base.OnColumnClick(e);
        }

        protected override void OnHandleCreated(EventArgs e)
        {
            HandleRef hWnd = new HandleRef(this, Handle);
            IntPtr handle = UnsafeNativeMethods.SendMessage(hWnd, NativeMethods.LVM_GETHEADER, IntPtr.Zero, IntPtr.Zero);
            headerHandle = new HandleRef(this, handle);
            Invalidate();
            UnsafeNativeMethods.InvalidateRect(hWnd, IntPtr.Zero, false);
            base.OnHandleCreated(e);
            UpdateHeaderSortArrow();
        }
        /// <summary>
        /// Sets a new datasource
        /// </summary>
        /// <param name="newDataSource">A new datasource</param>
        /// <param name="force">Force a new datasource</param>
        private void SetDataConnection(object newDataSource, bool force)
        {
            CurrencyManager newDataManager = null;
            bool dataSourceChanged = dataSource != newDataSource;
            if (force || dataSourceChanged)
            {
                if (dataSource as IComponent != null)
                {
                    ((IComponent) dataSource).Disposed -= DataSourceDisposed;
                }
                if (dataSource as IBindingList != null)
                {
                    ((IBindingList) dataSource).ListChanged -= DataSourceListChanged;
                }
                dataSource = newDataSource;
                if (dataSource as IComponent != null)
                {
                    ((IComponent) dataSource).Disposed += DataSourceDisposed;
                }
                if (dataSource as IBindingList != null)
                {
                    ((IBindingList) dataSource).ListChanged += DataSourceListChanged;
                }
                if (newDataSource != null && BindingContext != null && newDataSource != Convert.DBNull)
                {
                    newDataManager = (CurrencyManager) BindingContext[newDataSource, DataMember];
                }
                if (dataManager != newDataManager)
                {
//                    if (dataManager != null)
//                    {
//                        dataManager.ItemChanged -= new ItemChangedEventHandler(DataManager_ItemChanged);
//                        dataManager.PositionChanged -= new EventHandler(DataManager_PositionChanged);
//                    }
                    bindingList = null;
                    dataManager = newDataManager;
//                    if (dataManager != null)
//                    {
//                        dataManager.ItemChanged += new ItemChangedEventHandler(DataManager_ItemChanged);
//                        dataManager.PositionChanged += new EventHandler(DataManager_PositionChanged);
//                    }
                }
                if (dataManager != null)
                {
                    bindingList = dataManager.List as IBindingList;
                    AddItems(dataManager.List);
                    ApplySort();
                }
            }
            if (dataSourceChanged)
            {
                OnDataSourceChanged(EventArgs.Empty);
            }
        }

        private void DataSourceDisposed(object sender, EventArgs e)
        {
            SetDataConnection(null, true);
        }

        private void DataSourceListChanged(object sender, ListChangedEventArgs e)
        {
            switch (e.ListChangedType)
            {
                case ListChangedType.ItemAdded:
                {
                    AddItem(InnerDataSource, e.NewIndex);
                    break;
                }
                case ListChangedType.ItemChanged:
                {
                    ChangeItem(InnerDataSource, e.NewIndex);
                    break;
                }
                case ListChangedType.ItemDeleted:
                {
                    DeleteItem(e.NewIndex);
                    break;
                }
                case ListChangedType.Reset:
                {
                    if (dataManager != null)
                    {
                        AddItems(dataManager.List);
                    }
                    break;
                }
            }
        }

        /// <summary>
        /// Fills listview from innerSource
        /// </summary>
        /// <param name="innerSource"></param>
        private void AddItems(IList innerSource)
        {
            // clear items
            Items.Clear();
            base.Columns.Clear();
            // load column headers
            foreach (ColumnHeader column in Columns)
            {
                base.Columns.Add(column);
            }
            if (innerSource != null)
            {
                BeginUpdate();
                try
                {
                    // load data into the control
                    for (int row = 0; row < innerSource.Count; row++)
                    {
                        AddItem(innerSource, row);
                    }
                }
                finally
                {
                    EndUpdate();
                }
            }
        }

        /// <summary>
        /// Adds an item to Items collection from IList by specified index
        /// </summary>
        /// <param name="innerSource"></param>
        /// <param name="index"></param>
        private void AddItem(IList innerSource, int index)
        {
            ListViewItem item = new ListViewItem();
            // load primary field
            item.Text = GetDisplayMemberValue(innerSource[index], Columns[0]);
            // load all subfields
            for (int subItem = 1; subItem < Columns.Count; subItem++)
            {
                item.SubItems.Add(GetDisplayMemberValue(innerSource[index], Columns[subItem]));
            }
            Items.Insert(index, item);
            OnUpdateItem(new ItemCheckedEventArgs(item));
        }

        private void ChangeItem(IList innerSource, int index)
        {
            ListViewItem item = Items[index];
            item.SubItems.Clear();
            // load primary field
            item.Text = GetDisplayMemberValue(innerSource[index], Columns[0]);
            // load all subfields
            for (int subItem = 1; subItem < Columns.Count; subItem++)
            {
                item.SubItems.Add(GetDisplayMemberValue(innerSource[index], Columns[subItem]));
            }
            OnUpdateItem(new ItemCheckedEventArgs(item));
        }

        private void DeleteItem(int index)
        {
            Items.RemoveAt(index);
        }

        private IList InnerDataSource
        {
            get
            {
                if (dataSource is DataSet)
                {
                    if (dataMember != null && dataMember.Length > 0)
                    {
                        return ((IListSource) ((DataSet) dataSource).Tables[dataMember]).GetList();
                    }
                    else
                    {
                        return ((IListSource) ((DataSet) dataSource).Tables[0]).GetList();
                    }
                }
                else if (dataSource is IListSource)
                {
                    return ((IListSource) dataSource).GetList();
                }
                else
                {
                    return (IList) dataSource;
                }
            }
        }

        /// <summary>
        /// Returns string value of and obj for specified column
        /// </summary>
        private string GetDisplayMemberValue(object obj, ColumnHeader column)
        {
            if (obj == null)
            {
                return column.GetDisplayText(obj);
            }
            if (column == null)
            {
                throw new ArgumentNullException("column");
            }

            if (obj is DataRowView)
            {
                try
                {
                    // this is DataRowView from a DataView
                    return column.GetDisplayText((((DataRowView) obj)[column.DisplayMember]));
                }
                catch (Exception e)
                {
                    return e.Message;
                }
            }
            else if (obj is ValueType && obj.GetType().IsPrimitive)
            {
                // this is a primitive value type
                return column.GetDisplayText(obj);
            }
            else if (obj is String)
            {
                // this is a simple string
                return column.GetDisplayText(obj);
            }
            else
            {
                // this is an object or structure
                try
                {
                    Type sourceType = obj.GetType();
                    // see if the field is a property
                    PropertyInfo prop = sourceType.GetProperty(column.DisplayMember);
                    if (prop == null || !prop.CanRead)
                    {
                        // no readable property of that name exists - check for field
                        FieldInfo field = sourceType.GetField(column.DisplayMember);
                        if (field == null)
                        {
                            // no field exists either, return the field name as a debugging indicator
                            return
                                string.Format(Strings.Culture, Strings.ListViewWrongDisplayMember,
                                              column.DisplayMember);
                        }
                        else
                        {
                            // got a field, return its value
                            return column.GetDisplayText(field.GetValue(obj));
                        }
                    }
                    else
                    {
                        // found a property, return its value
                        return column.GetDisplayText(prop.GetValue(obj, null));
                    }
                }
                catch (Exception e)
                {
                    return e.Message;
                }
            }
        }

        private void UpdateHeaderSortArrow()
        {
//            if (IsHandleCreated && HeaderStyle == ColumnHeaderStyle.Clickable)
//            {
//                NativeMethods.HDITEM lparam = new NativeMethods.HDITEM();
//                lparam.mask = NativeMethods.HDI_FORMAT;
//                for(int iColumn = 0; iColumn < Columns.Count; iColumn++)
//                {
//                    UnsafeNativeMethods.SendMessage(HeaderHandle, NativeMethods.HDM_GETITEM, (IntPtr) iColumn,
//                                                    ref lparam);
//                }
//            }
        }
        #region Sorting support
        protected bool SupportsSorting
        {
            get
            {
                if (bindingList != null)
                {
                    return bindingList.SupportsSorting;
                }
                return false;
            }
        }

        private void ApplySort()
        {
            if (SupportsSorting && !string.IsNullOrEmpty(SortProperty))
            {
                Cursor = Cursors.WaitCursor;
                try
                {
                    PropertyDescriptorCollection itemProperties = DataManager.GetItemProperties();
                    PropertyDescriptor property = itemProperties[SortProperty];
                    bindingList.ApplySort(property, SortDirection);
                }
                finally
                {
                    Cursor = Cursors.Default;
                }
            }
        }

        private void ApplySort(string property)
        {
            if (property == SortProperty)
            {
                if (SortDirection == ListSortDirection.Ascending)
                {
                    SortDirection = ListSortDirection.Descending;
                }
                else
                {
                    SortDirection = ListSortDirection.Ascending;
                }
            }
            else
            {
                SortProperty = property;
            }
        }
        #endregion
    }
}