﻿#if _DYNAMIC_XMLSERIALIZER_COMPILATION
[assembly:System.Security.AllowPartiallyTrustedCallers()]
[assembly:System.Security.SecurityTransparent()]
[assembly:System.Security.SecurityRules(System.Security.SecurityRuleSet.Level1)]
#endif
[assembly:System.Reflection.AssemblyVersionAttribute("2.9.0.0")]
[assembly:System.Xml.Serialization.XmlSerializerVersionAttribute(ParentAssemblyId=@"6d3a1df4-6ce7-403a-b559-4b5de39fe092,", Version=@"4.0.0.0")]
namespace Microsoft.Xml.Serialization.GeneratedAssembly {

    public class XmlSerializationWriterVRMNewWebService : System.Xml.Serialization.XmlSerializationWriter {

        public void Write1_InvokeWebservice(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"InvokeWebservice", @"http://www.myvrm.com/webservices/", null, false);
            if (pLength > 0) {
                WriteElementString(@"inputXML", @"http://www.myvrm.com/webservices/", ((global::System.String)p[0]));
            }
            WriteEndElement();
        }

        public void Write2_InvokeWebserviceInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        protected override void InitCallbacks() {
        }
    }

    public class XmlSerializationReaderVRMNewWebService : System.Xml.Serialization.XmlSerializationReader {

        public object[] Read1_InvokeWebserviceResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations0 = 0;
            int readerCount0 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id1_InvokeWebserviceResponse, id2_Item)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations1 = 0;
                    int readerCount1 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id3_InvokeWebserviceResult && (object) Reader.NamespaceURI == (object)id2_Item)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"http://www.myvrm.com/webservices/:InvokeWebserviceResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"http://www.myvrm.com/webservices/:InvokeWebserviceResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations1, ref readerCount1);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"http://www.myvrm.com/webservices/:InvokeWebserviceResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations0, ref readerCount0);
            }
            return p;
        }

        public object[] Read2_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations2 = 0;
            int readerCount2 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations2, ref readerCount2);
            }
            return p;
        }

        protected override void InitCallbacks() {
        }

        string id3_InvokeWebserviceResult;
        string id1_InvokeWebserviceResponse;
        string id2_Item;

        protected override void InitIDs() {
            id3_InvokeWebserviceResult = Reader.NameTable.Add(@"InvokeWebserviceResult");
            id1_InvokeWebserviceResponse = Reader.NameTable.Add(@"InvokeWebserviceResponse");
            id2_Item = Reader.NameTable.Add(@"http://www.myvrm.com/webservices/");
        }
    }

    public abstract class XmlSerializer1 : System.Xml.Serialization.XmlSerializer {
        protected override System.Xml.Serialization.XmlSerializationReader CreateReader() {
            return new XmlSerializationReaderVRMNewWebService();
        }
        protected override System.Xml.Serialization.XmlSerializationWriter CreateWriter() {
            return new XmlSerializationWriterVRMNewWebService();
        }
    }

    public sealed class ArrayOfObjectSerializer : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"InvokeWebservice", @"http://www.myvrm.com/webservices/");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriterVRMNewWebService)writer).Write1_InvokeWebservice((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer1 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"InvokeWebserviceResponse", @"http://www.myvrm.com/webservices/");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReaderVRMNewWebService)reader).Read1_InvokeWebserviceResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer2 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"InvokeWebserviceInHeaders", @"http://www.myvrm.com/webservices/");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriterVRMNewWebService)writer).Write2_InvokeWebserviceInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer3 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"InvokeWebserviceResponseOutHeaders", @"http://www.myvrm.com/webservices/");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReaderVRMNewWebService)reader).Read2_Item();
        }
    }

    public class XmlSerializerContract : global::System.Xml.Serialization.XmlSerializerImplementation {
        public override global::System.Xml.Serialization.XmlSerializationReader Reader { get { return new XmlSerializationReaderVRMNewWebService(); } }
        public override global::System.Xml.Serialization.XmlSerializationWriter Writer { get { return new XmlSerializationWriterVRMNewWebService(); } }
        System.Collections.Hashtable readMethods = null;
        public override System.Collections.Hashtable ReadMethods {
            get {
                if (readMethods == null) {
                    System.Collections.Hashtable _tmp = new System.Collections.Hashtable();
                    _tmp[@"MyVrm.WebServices.Vrmws.VRMNewWebService:System.String InvokeWebservice(System.String):Response"] = @"Read1_InvokeWebserviceResponse";
                    _tmp[@"MyVrm.WebServices.Vrmws.VRMNewWebService:System.String InvokeWebservice(System.String):OutHeaders"] = @"Read2_Item";
                    if (readMethods == null) readMethods = _tmp;
                }
                return readMethods;
            }
        }
        System.Collections.Hashtable writeMethods = null;
        public override System.Collections.Hashtable WriteMethods {
            get {
                if (writeMethods == null) {
                    System.Collections.Hashtable _tmp = new System.Collections.Hashtable();
                    _tmp[@"MyVrm.WebServices.Vrmws.VRMNewWebService:System.String InvokeWebservice(System.String)"] = @"Write1_InvokeWebservice";
                    _tmp[@"MyVrm.WebServices.Vrmws.VRMNewWebService:System.String InvokeWebservice(System.String):InHeaders"] = @"Write2_InvokeWebserviceInHeaders";
                    if (writeMethods == null) writeMethods = _tmp;
                }
                return writeMethods;
            }
        }
        System.Collections.Hashtable typedSerializers = null;
        public override System.Collections.Hashtable TypedSerializers {
            get {
                if (typedSerializers == null) {
                    System.Collections.Hashtable _tmp = new System.Collections.Hashtable();
                    _tmp.Add(@"MyVrm.WebServices.Vrmws.VRMNewWebService:System.String InvokeWebservice(System.String):InHeaders", new ArrayOfObjectSerializer2());
                    _tmp.Add(@"MyVrm.WebServices.Vrmws.VRMNewWebService:System.String InvokeWebservice(System.String):OutHeaders", new ArrayOfObjectSerializer3());
                    _tmp.Add(@"MyVrm.WebServices.Vrmws.VRMNewWebService:System.String InvokeWebservice(System.String)", new ArrayOfObjectSerializer());
                    _tmp.Add(@"MyVrm.WebServices.Vrmws.VRMNewWebService:System.String InvokeWebservice(System.String):Response", new ArrayOfObjectSerializer1());
                    if (typedSerializers == null) typedSerializers = _tmp;
                }
                return typedSerializers;
            }
        }
        public override System.Boolean CanSerialize(System.Type type) {
            if (type == typeof(global::MyVrm.WebServices.Vrmws.VRMNewWebService)) return true;
            return false;
        }
        public override System.Xml.Serialization.XmlSerializer GetSerializer(System.Type type) {
            return null;
        }
    }
}
