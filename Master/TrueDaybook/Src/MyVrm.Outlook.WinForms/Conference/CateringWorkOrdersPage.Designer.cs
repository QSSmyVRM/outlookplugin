﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.Outlook.WinForms.Conference
{
    partial class CateringWorkOrdersPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CateringWorkOrdersPage));
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.assignWorkOrderBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.editWorkOrderBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.removeWorkOrderBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.workOrderList = new DevExpress.XtraTreeList.TreeList();
            this.roomNameColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.quantityColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.roomIdColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.quantityRepositoryItemTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.workOrderList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.quantityRepositoryItemTextEdit)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager
            // 
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Images = this.imageList;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.editWorkOrderBarButtonItem,
            this.removeWorkOrderBarButtonItem,
            this.assignWorkOrderBarButtonItem});
            this.barManager.MaxItemId = 3;
            // 
            // bar1
            // 
            this.bar1.BarName = "Tools";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.assignWorkOrderBarButtonItem),
            new DevExpress.XtraBars.LinkPersistInfo(this.editWorkOrderBarButtonItem),
            new DevExpress.XtraBars.LinkPersistInfo(this.removeWorkOrderBarButtonItem)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.DisableCustomization = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Tools";
            // 
            // assignWorkOrderBarButtonItem
            // 
            this.assignWorkOrderBarButtonItem.Caption = "Assign";
            this.assignWorkOrderBarButtonItem.Id = 2;
            this.assignWorkOrderBarButtonItem.ImageIndex = 0;
            this.assignWorkOrderBarButtonItem.Name = "assignWorkOrderBarButtonItem";
            this.assignWorkOrderBarButtonItem.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.assignWorkOrderBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem3_ItemClick);
            // 
            // editWorkOrderBarButtonItem
            // 
            this.editWorkOrderBarButtonItem.Caption = "Edit";
            this.editWorkOrderBarButtonItem.Id = 0;
            this.editWorkOrderBarButtonItem.ImageIndex = 1;
            this.editWorkOrderBarButtonItem.Name = "editWorkOrderBarButtonItem";
            this.editWorkOrderBarButtonItem.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.editWorkOrderBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.editWorkOrderBarButtonItem_ItemClick);
            // 
            // removeWorkOrderBarButtonItem
            // 
            this.removeWorkOrderBarButtonItem.Caption = "Remove";
            this.removeWorkOrderBarButtonItem.Id = 1;
            this.removeWorkOrderBarButtonItem.ImageIndex = 2;
            this.removeWorkOrderBarButtonItem.Name = "removeWorkOrderBarButtonItem";
            this.removeWorkOrderBarButtonItem.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.removeWorkOrderBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.removeWorkOrderBarButtonItem_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(480, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 219);
            this.barDockControlBottom.Size = new System.Drawing.Size(480, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 193);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(480, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 193);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "Create.png");
            this.imageList.Images.SetKeyName(1, "Edit.png");
            this.imageList.Images.SetKeyName(2, "Remove.png");
            // 
            // workOrderList
            // 
            this.workOrderList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.roomNameColumn,
            this.quantityColumn,
            this.roomIdColumn});
            this.workOrderList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.workOrderList.Location = new System.Drawing.Point(0, 26);
            this.workOrderList.Name = "workOrderList";
            this.workOrderList.OptionsView.ShowHorzLines = false;
            this.workOrderList.OptionsView.ShowIndicator = false;
            this.workOrderList.OptionsView.ShowRoot = false;
            this.workOrderList.OptionsView.ShowVertLines = false;
            this.workOrderList.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.quantityRepositoryItemTextEdit});
            this.workOrderList.Size = new System.Drawing.Size(480, 193);
            this.workOrderList.TabIndex = 4;
            this.workOrderList.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.workOrderList_FocusedNodeChanged);
            this.workOrderList.DoubleClick += new System.EventHandler(this.workOrderList_DoubleClick);
            // 
            // roomNameColumn
            // 
            this.roomNameColumn.Caption = "Room";
            this.roomNameColumn.FieldName = "RoomName";
            this.roomNameColumn.Name = "roomNameColumn";
            this.roomNameColumn.OptionsColumn.AllowEdit = false;
            this.roomNameColumn.OptionsColumn.AllowFocus = false;
            this.roomNameColumn.OptionsColumn.AllowMove = false;
            this.roomNameColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.roomNameColumn.OptionsColumn.AllowSort = false;
            this.roomNameColumn.OptionsColumn.ReadOnly = true;
            this.roomNameColumn.OptionsColumn.ShowInCustomizationForm = false;
            this.roomNameColumn.Visible = true;
            this.roomNameColumn.VisibleIndex = 0;
            // 
            // quantityColumn
            // 
            this.quantityColumn.Caption = "Quantity";
            this.quantityColumn.FieldName = "Quantity";
            this.quantityColumn.Name = "quantityColumn";
            this.quantityColumn.OptionsColumn.AllowEdit = false;
            this.quantityColumn.OptionsColumn.AllowFocus = false;
            this.quantityColumn.OptionsColumn.AllowMove = false;
            this.quantityColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.quantityColumn.OptionsColumn.AllowSort = false;
            this.quantityColumn.OptionsColumn.ReadOnly = true;
            this.quantityColumn.OptionsColumn.ShowInCustomizationForm = false;
            this.quantityColumn.Visible = true;
            this.quantityColumn.VisibleIndex = 1;
            // 
            // roomIdColumn
            // 
            this.roomIdColumn.Caption = "RoomId";
            this.roomIdColumn.FieldName = "RoomId";
            this.roomIdColumn.Name = "roomIdColumn";
            this.roomIdColumn.OptionsColumn.AllowEdit = false;
            this.roomIdColumn.OptionsColumn.AllowFocus = false;
            this.roomIdColumn.OptionsColumn.AllowMove = false;
            this.roomIdColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.roomIdColumn.OptionsColumn.AllowSort = false;
            this.roomIdColumn.OptionsColumn.ReadOnly = true;
            this.roomIdColumn.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // quantityRepositoryItemTextEdit
            // 
            this.quantityRepositoryItemTextEdit.AutoHeight = false;
            this.quantityRepositoryItemTextEdit.Name = "quantityRepositoryItemTextEdit";
            this.quantityRepositoryItemTextEdit.NullText = "No assigned work order";
            this.quantityRepositoryItemTextEdit.ReadOnly = true;
            // 
            // CateringWorkOrdersPage
            // 
            this.Controls.Add(this.workOrderList);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "CateringWorkOrdersPage";
            this.Size = new System.Drawing.Size(480, 219);
            this.Load += new System.EventHandler(this.CateringWorkOrdersPage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.workOrderList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.quantityRepositoryItemTextEdit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem editWorkOrderBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem removeWorkOrderBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem assignWorkOrderBarButtonItem;
        private DevExpress.XtraTreeList.TreeList workOrderList;
        private DevExpress.XtraTreeList.Columns.TreeListColumn roomNameColumn;
        private DevExpress.XtraTreeList.Columns.TreeListColumn quantityColumn;
        private DevExpress.XtraTreeList.Columns.TreeListColumn roomIdColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit quantityRepositoryItemTextEdit;
        private System.Windows.Forms.ImageList imageList;
    }
}
