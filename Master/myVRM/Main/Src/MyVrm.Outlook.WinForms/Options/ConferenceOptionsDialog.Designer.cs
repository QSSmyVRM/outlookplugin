﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.Outlook.WinForms.Options
{
    partial class ConferenceOptionsDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.dataLayoutControl = new DevExpress.XtraDataLayout.DataLayoutControl();
			this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
			this.checkNotDeleteNoConfirm = new DevExpress.XtraEditors.CheckEdit();
			this.checkDeleteNoConfirm = new DevExpress.XtraEditors.CheckEdit();
			this.checkConfirmDeletion = new DevExpress.XtraEditors.CheckEdit();
			this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
			this.appendConferenceDetailsToMeetingCheckEdit = new DevExpress.XtraEditors.CheckEdit();
			this.showPopupConferenceIsScheduledCheckEdit = new DevExpress.XtraEditors.CheckEdit();
			this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlDelitionOptions = new DevExpress.XtraLayout.LayoutControlItem();
			((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).BeginInit();
			this.ContentPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl)).BeginInit();
			this.dataLayoutControl.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
			this.layoutControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.checkNotDeleteNoConfirm.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.checkDeleteNoConfirm.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.checkConfirmDeletion.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.appendConferenceDetailsToMeetingCheckEdit.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.showPopupConferenceIsScheduledCheckEdit.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlDelitionOptions)).BeginInit();
			this.SuspendLayout();
			// 
			// ContentPanel
			// 
			this.ContentPanel.Controls.Add(this.dataLayoutControl);
			this.ContentPanel.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
			this.ContentPanel.Size = new System.Drawing.Size(397, 278);
			// 
			// dataLayoutControl
			// 
			this.dataLayoutControl.Controls.Add(this.layoutControl1);
			this.dataLayoutControl.Controls.Add(this.appendConferenceDetailsToMeetingCheckEdit);
			this.dataLayoutControl.Controls.Add(this.showPopupConferenceIsScheduledCheckEdit);
			this.dataLayoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataLayoutControl.Location = new System.Drawing.Point(0, 0);
			this.dataLayoutControl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.dataLayoutControl.Name = "dataLayoutControl";
			this.dataLayoutControl.Root = this.layoutControlGroup1;
			this.dataLayoutControl.Size = new System.Drawing.Size(397, 278);
			this.dataLayoutControl.TabIndex = 0;
			this.dataLayoutControl.Text = "dataLayoutControl1";
			// 
			// layoutControl1
			// 
			this.layoutControl1.Controls.Add(this.checkNotDeleteNoConfirm);
			this.layoutControl1.Controls.Add(this.checkDeleteNoConfirm);
			this.layoutControl1.Controls.Add(this.checkConfirmDeletion);
			this.layoutControl1.Location = new System.Drawing.Point(5, 80);
			this.layoutControl1.Name = "layoutControl1";
			this.layoutControl1.Root = this.layoutControlGroup2;
			this.layoutControl1.Size = new System.Drawing.Size(390, 196);
			this.layoutControl1.TabIndex = 4;
			this.layoutControl1.Text = "layoutControl1";
			// 
			// checkNotDeleteNoConfirm
			// 
			this.checkNotDeleteNoConfirm.Location = new System.Drawing.Point(7, 32);
			this.checkNotDeleteNoConfirm.Name = "checkNotDeleteNoConfirm";
			this.checkNotDeleteNoConfirm.Properties.Caption = "Do not delete it from server by default";
			this.checkNotDeleteNoConfirm.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
			this.checkNotDeleteNoConfirm.Size = new System.Drawing.Size(376, 21);
			this.checkNotDeleteNoConfirm.StyleController = this.layoutControl1;
			this.checkNotDeleteNoConfirm.TabIndex = 5;
			this.checkNotDeleteNoConfirm.CheckedChanged += new System.EventHandler(this.CheckDeletionOption);
			// 
			// checkDeleteNoConfirm
			// 
			this.checkDeleteNoConfirm.Location = new System.Drawing.Point(7, 7);
			this.checkDeleteNoConfirm.Name = "checkDeleteNoConfirm";
			this.checkDeleteNoConfirm.Properties.Caption = "Delete it from server by default without confirmation";
			this.checkDeleteNoConfirm.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
			this.checkDeleteNoConfirm.Size = new System.Drawing.Size(376, 21);
			this.checkDeleteNoConfirm.StyleController = this.layoutControl1;
			this.checkDeleteNoConfirm.TabIndex = 4;
			this.checkDeleteNoConfirm.CheckedChanged += new System.EventHandler(this.CheckDeletionOption);
			// 
			// checkConfirmDeletion
			// 
			this.checkConfirmDeletion.EditValue = true;
			this.checkConfirmDeletion.Location = new System.Drawing.Point(7, 57);
			this.checkConfirmDeletion.Name = "checkConfirmDeletion";
			this.checkConfirmDeletion.Properties.Caption = "Need confirmation of deletion from server";
			this.checkConfirmDeletion.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
			this.checkConfirmDeletion.Size = new System.Drawing.Size(376, 21);
			this.checkConfirmDeletion.StyleController = this.layoutControl1;
			this.checkConfirmDeletion.TabIndex = 6;
			this.checkConfirmDeletion.CheckedChanged += new System.EventHandler(this.CheckDeletionOption);
			// 
			// layoutControlGroup2
			// 
			this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
			this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
			this.layoutControlGroup2.GroupBordersVisible = false;
			this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6,
            this.layoutControlItem5,
            this.layoutControlItem4});
			this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup2.Name = "layoutControlGroup2";
			this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
			this.layoutControlGroup2.Size = new System.Drawing.Size(390, 196);
			this.layoutControlGroup2.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup2.Text = "layoutControlGroup2";
			this.layoutControlGroup2.TextVisible = false;
			// 
			// layoutControlItem6
			// 
			this.layoutControlItem6.Control = this.checkConfirmDeletion;
			this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
			this.layoutControlItem6.Location = new System.Drawing.Point(0, 50);
			this.layoutControlItem6.MinSize = new System.Drawing.Size(87, 25);
			this.layoutControlItem6.Name = "layoutControlItem6";
			this.layoutControlItem6.Size = new System.Drawing.Size(380, 136);
			this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem6.Text = "layoutControlItem6";
			this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem6.TextToControlDistance = 0;
			this.layoutControlItem6.TextVisible = false;
			// 
			// layoutControlItem5
			// 
			this.layoutControlItem5.Control = this.checkNotDeleteNoConfirm;
			this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
			this.layoutControlItem5.Location = new System.Drawing.Point(0, 25);
			this.layoutControlItem5.Name = "layoutControlItem5";
			this.layoutControlItem5.Size = new System.Drawing.Size(380, 25);
			this.layoutControlItem5.Text = "layoutControlItem5";
			this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem5.TextToControlDistance = 0;
			this.layoutControlItem5.TextVisible = false;
			// 
			// layoutControlItem4
			// 
			this.layoutControlItem4.Control = this.checkDeleteNoConfirm;
			this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
			this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem4.Name = "layoutControlItem4";
			this.layoutControlItem4.Size = new System.Drawing.Size(380, 25);
			this.layoutControlItem4.Text = "layoutControlItem4";
			this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem4.TextToControlDistance = 0;
			this.layoutControlItem4.TextVisible = false;
			// 
			// appendConferenceDetailsToMeetingCheckEdit
			// 
			this.appendConferenceDetailsToMeetingCheckEdit.Location = new System.Drawing.Point(2, 27);
			this.appendConferenceDetailsToMeetingCheckEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.appendConferenceDetailsToMeetingCheckEdit.Name = "appendConferenceDetailsToMeetingCheckEdit";
			this.appendConferenceDetailsToMeetingCheckEdit.Properties.Caption = "Append conference details to a meeting request";
			this.appendConferenceDetailsToMeetingCheckEdit.Size = new System.Drawing.Size(393, 21);
			this.appendConferenceDetailsToMeetingCheckEdit.StyleController = this.dataLayoutControl;
			this.appendConferenceDetailsToMeetingCheckEdit.TabIndex = 1;
			// 
			// showPopupConferenceIsScheduledCheckEdit
			// 
			this.showPopupConferenceIsScheduledCheckEdit.Location = new System.Drawing.Point(2, 2);
			this.showPopupConferenceIsScheduledCheckEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.showPopupConferenceIsScheduledCheckEdit.Name = "showPopupConferenceIsScheduledCheckEdit";
			this.showPopupConferenceIsScheduledCheckEdit.Properties.Caption = "Show popup when a conference is successfully scheduled";
			this.showPopupConferenceIsScheduledCheckEdit.Size = new System.Drawing.Size(393, 21);
			this.showPopupConferenceIsScheduledCheckEdit.StyleController = this.dataLayoutControl;
			this.showPopupConferenceIsScheduledCheckEdit.TabIndex = 0;
			// 
			// layoutControlGroup1
			// 
			this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
			this.layoutControlGroup1.GroupBordersVisible = false;
			this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlDelitionOptions});
			this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup1.Name = "layoutControlGroup1";
			this.layoutControlGroup1.Size = new System.Drawing.Size(397, 278);
			this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup1.Text = "layoutControlGroup1";
			this.layoutControlGroup1.TextVisible = false;
			// 
			// layoutControlItem1
			// 
			this.layoutControlItem1.Control = this.showPopupConferenceIsScheduledCheckEdit;
			this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
			this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem1.MinSize = new System.Drawing.Size(356, 25);
			this.layoutControlItem1.Name = "layoutControlItem1";
			this.layoutControlItem1.Size = new System.Drawing.Size(397, 25);
			this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem1.Text = "layoutControlItem1";
			this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem1.TextToControlDistance = 0;
			this.layoutControlItem1.TextVisible = false;
			// 
			// layoutControlItem2
			// 
			this.layoutControlItem2.Control = this.appendConferenceDetailsToMeetingCheckEdit;
			this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
			this.layoutControlItem2.Location = new System.Drawing.Point(0, 25);
			this.layoutControlItem2.MinSize = new System.Drawing.Size(301, 25);
			this.layoutControlItem2.Name = "layoutControlItem2";
			this.layoutControlItem2.Size = new System.Drawing.Size(397, 31);
			this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem2.Text = "layoutControlItem2";
			this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem2.TextToControlDistance = 0;
			this.layoutControlItem2.TextVisible = false;
			// 
			// layoutControlDelitionOptions
			// 
			this.layoutControlDelitionOptions.Control = this.layoutControl1;
            this.layoutControlDelitionOptions.CustomizationFormText = "When deleting conference from Outlook";
			this.layoutControlDelitionOptions.Location = new System.Drawing.Point(0, 56);
			this.layoutControlDelitionOptions.MinSize = new System.Drawing.Size(216, 24);
			this.layoutControlDelitionOptions.Name = "layoutControlDelitionOptions";
			this.layoutControlDelitionOptions.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 2, 5, 2);
			this.layoutControlDelitionOptions.Size = new System.Drawing.Size(397, 222);
			this.layoutControlDelitionOptions.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlDelitionOptions.Text = "When delete conference or it\'s occurence from Outlook";
			this.layoutControlDelitionOptions.TextLocation = DevExpress.Utils.Locations.Top;
			this.layoutControlDelitionOptions.TextSize = new System.Drawing.Size(315, 16);
			// 
			// ConferenceOptionsDialog
			// 
			this.ApplyEnabled = true;
			this.ApplyVisible = true;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
			this.ClientSize = new System.Drawing.Size(421, 334);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
			this.Name = "ConferenceOptionsDialog";
			this.Text = "Conference Options";
			((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).EndInit();
			this.ContentPanel.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl)).EndInit();
			this.dataLayoutControl.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
			this.layoutControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.checkNotDeleteNoConfirm.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.checkDeleteNoConfirm.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.checkConfirmDeletion.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.appendConferenceDetailsToMeetingCheckEdit.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.showPopupConferenceIsScheduledCheckEdit.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlDelitionOptions)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private DevExpress.XtraEditors.CheckEdit showPopupConferenceIsScheduledCheckEdit;
		private DevExpress.XtraEditors.CheckEdit appendConferenceDetailsToMeetingCheckEdit;
		private DevExpress.XtraLayout.LayoutControl layoutControl1;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlDelitionOptions;
		private DevExpress.XtraEditors.CheckEdit checkConfirmDeletion;
		private DevExpress.XtraEditors.CheckEdit checkNotDeleteNoConfirm;
		private DevExpress.XtraEditors.CheckEdit checkDeleteNoConfirm;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
    }
}
