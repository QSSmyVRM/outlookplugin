﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.Outlook.WinForms.Options
{
    partial class NewOptionsDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.lblRequiredFields = new DevExpress.XtraEditors.LabelControl();
            this.ErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.lblRequiredFields1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnApply = new DevExpress.XtraEditors.SimpleButton();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.tbAbout = new DevExpress.XtraTab.XtraTabPage();
            this.lblContactEmailEdit = new System.Windows.Forms.LinkLabel();
            this.lblVersionEdit = new DevExpress.XtraEditors.LabelControl();
            this.additionalInfoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.lblAdditionalInformation = new DevExpress.XtraEditors.LabelControl();
            this.lblSupportPhoneEdit = new DevExpress.XtraEditors.LabelControl();
            this.lblSupportPhone = new DevExpress.XtraEditors.LabelControl();
            this.lblTechSupportEmail = new DevExpress.XtraEditors.LabelControl();
            this.lblContactNameEdit = new DevExpress.XtraEditors.LabelControl();
            this.lblTechSupportContact = new DevExpress.XtraEditors.LabelControl();
            this.lblCopyright = new DevExpress.XtraEditors.LabelControl();
            this.lblVersionLabel = new DevExpress.XtraEditors.LabelControl();
            this.lblProductName = new DevExpress.XtraEditors.LabelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.tbTemplates = new DevExpress.XtraTab.XtraTabPage();
            this.grpParticipants = new DevExpress.XtraEditors.GroupControl();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddNewParticipant = new DevExpress.XtraEditors.SimpleButton();
            this.gcParticipants = new DevExpress.XtraGrid.GridControl();
            this.gvParticipants = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.grpConferenceSettings = new DevExpress.XtraEditors.GroupControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.lblDoubleClickRooms = new DevExpress.XtraEditors.LabelControl();
            this.roomsList = new DevExpress.XtraEditors.ListBoxControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.lblMinutes = new DevExpress.XtraEditors.LabelControl();
            this.txtMins = new DevExpress.XtraEditors.TextEdit();
            this.lblHours = new DevExpress.XtraEditors.LabelControl();
            this.txtHours = new DevExpress.XtraEditors.TextEdit();
            this.lblDuration = new DevExpress.XtraEditors.LabelControl();
            this.txtConferenceName = new DevExpress.XtraEditors.TextEdit();
            this.lblConferenceName = new DevExpress.XtraEditors.LabelControl();
            this.grpCreateTemplate = new DevExpress.XtraEditors.GroupControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.chkPublic = new DevExpress.XtraEditors.CheckEdit();
            this.memoTemplateDescription = new DevExpress.XtraEditors.MemoEdit();
            this.lblTemplateDescription = new DevExpress.XtraEditors.LabelControl();
            this.txtTemplateName = new DevExpress.XtraEditors.TextEdit();
            this.lblTemplateName = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.tbAccount = new DevExpress.XtraTab.XtraTabPage();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.lnkChangeSettings = new System.Windows.Forms.LinkLabel();
            this.lblTimeZone = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.lblLanguage = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.linklblForgotPassword = new System.Windows.Forms.LinkLabel();
            this.btnTestConnection = new DevExpress.XtraEditors.SimpleButton();
            this.chkRememberMe = new DevExpress.XtraEditors.CheckEdit();
            this.txtPassword = new DevExpress.XtraEditors.TextEdit();
            this.lblPassword = new DevExpress.XtraEditors.LabelControl();
            this.txtUserName = new DevExpress.XtraEditors.TextEdit();
            this.lblUserName = new DevExpress.XtraEditors.LabelControl();
            this.txtServiceURL = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.lblSiteURL = new DevExpress.XtraEditors.LabelControl();
            this.lblTrueDayBookAccount = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.OptionsTabs = new DevExpress.XtraTab.XtraTabControl();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProvider)).BeginInit();
            this.tbAbout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.additionalInfoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.tbTemplates.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpParticipants)).BeginInit();
            this.grpParticipants.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcParticipants)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvParticipants)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpConferenceSettings)).BeginInit();
            this.grpConferenceSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.roomsList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMins.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHours.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtConferenceName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpCreateTemplate)).BeginInit();
            this.grpCreateTemplate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkPublic.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoTemplateDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTemplateName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.tbAccount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkRememberMe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceURL.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OptionsTabs)).BeginInit();
            this.OptionsTabs.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl11.Appearance.Options.UseForeColor = true;
            this.labelControl11.Location = new System.Drawing.Point(3, 634);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(6, 13);
            this.labelControl11.TabIndex = 0;
            this.labelControl11.Text = "*";
            // 
            // lblRequiredFields
            // 
            this.lblRequiredFields.Location = new System.Drawing.Point(14, 634);
            this.lblRequiredFields.Name = "lblRequiredFields";
            this.lblRequiredFields.Size = new System.Drawing.Size(73, 13);
            this.lblRequiredFields.TabIndex = 1;
            this.lblRequiredFields.Text = "Required Fields";
            // 
            // ErrorProvider
            // 
            this.ErrorProvider.ContainerControl = this;
            // 
            // lblRequiredFields1
            // 
            this.lblRequiredFields1.Location = new System.Drawing.Point(21, 639);
            this.lblRequiredFields1.Name = "lblRequiredFields1";
            this.lblRequiredFields1.Size = new System.Drawing.Size(73, 13);
            this.lblRequiredFields1.TabIndex = 1;
            this.lblRequiredFields1.Text = "Required Fields";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Location = new System.Drawing.Point(12, 640);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(6, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "*";
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(467, 646);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(81, 28);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "[Cancel]";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnApply
            // 
            this.btnApply.Location = new System.Drawing.Point(379, 646);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(81, 28);
            this.btnApply.TabIndex = 4;
            this.btnApply.Text = "[Apply]";
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(291, 646);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(81, 28);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "[OK]";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // tbAbout
            // 
            this.tbAbout.Controls.Add(this.lblContactEmailEdit);
            this.tbAbout.Controls.Add(this.lblVersionEdit);
            this.tbAbout.Controls.Add(this.additionalInfoEdit);
            this.tbAbout.Controls.Add(this.lblAdditionalInformation);
            this.tbAbout.Controls.Add(this.lblSupportPhoneEdit);
            this.tbAbout.Controls.Add(this.lblSupportPhone);
            this.tbAbout.Controls.Add(this.lblTechSupportEmail);
            this.tbAbout.Controls.Add(this.lblContactNameEdit);
            this.tbAbout.Controls.Add(this.lblTechSupportContact);
            this.tbAbout.Controls.Add(this.lblCopyright);
            this.tbAbout.Controls.Add(this.lblVersionLabel);
            this.tbAbout.Controls.Add(this.lblProductName);
            this.tbAbout.Controls.Add(this.panelControl3);
            this.tbAbout.Name = "tbAbout";
            this.tbAbout.Size = new System.Drawing.Size(544, 599);
            this.tbAbout.Text = "About";
            // 
            // lblContactEmailEdit
            // 
            this.lblContactEmailEdit.AutoSize = true;
            this.lblContactEmailEdit.Location = new System.Drawing.Point(153, 105);
            this.lblContactEmailEdit.Name = "lblContactEmailEdit";
            this.lblContactEmailEdit.Size = new System.Drawing.Size(53, 13);
            this.lblContactEmailEdit.TabIndex = 14;
            this.lblContactEmailEdit.TabStop = true;
            this.lblContactEmailEdit.Text = "linkLabel1";
            // 
            // lblVersionEdit
            // 
            this.lblVersionEdit.Location = new System.Drawing.Point(78, 32);
            this.lblVersionEdit.Name = "lblVersionEdit";
            this.lblVersionEdit.Size = new System.Drawing.Size(63, 13);
            this.lblVersionEdit.TabIndex = 13;
            this.lblVersionEdit.Text = "labelControl4";
            // 
            // additionalInfoEdit
            // 
            this.additionalInfoEdit.Location = new System.Drawing.Point(14, 172);
            this.additionalInfoEdit.Name = "additionalInfoEdit";
            this.additionalInfoEdit.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.additionalInfoEdit.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            //this.additionalInfoEdit.Properties.Appearance.Options.UseBackColor = true;
            //this.additionalInfoEdit.Properties.Appearance.Options.UseForeColor = true;
            this.additionalInfoEdit.Properties.ReadOnly = true;
            this.additionalInfoEdit.Size = new System.Drawing.Size(309, 172);
            this.additionalInfoEdit.TabIndex = 12;
            this.additionalInfoEdit.Enabled = false; //102821
            // 
            // lblAdditionalInformation
            // 
            this.lblAdditionalInformation.Location = new System.Drawing.Point(14, 152);
            this.lblAdditionalInformation.Name = "lblAdditionalInformation";
            this.lblAdditionalInformation.Size = new System.Drawing.Size(118, 13);
            this.lblAdditionalInformation.TabIndex = 11;
            this.lblAdditionalInformation.Text = "[Additional Information:]";
            // 
            // lblSupportPhoneEdit
            // 
            this.lblSupportPhoneEdit.Location = new System.Drawing.Point(153, 130);
            this.lblSupportPhoneEdit.Name = "lblSupportPhoneEdit";
            this.lblSupportPhoneEdit.Size = new System.Drawing.Size(63, 13);
            this.lblSupportPhoneEdit.TabIndex = 8;
            this.lblSupportPhoneEdit.Text = "labelControl4";
            // 
            // lblSupportPhone
            // 
            this.lblSupportPhone.Location = new System.Drawing.Point(14, 130);
            this.lblSupportPhone.Name = "lblSupportPhone";
            this.lblSupportPhone.Size = new System.Drawing.Size(109, 13);
            this.lblSupportPhone.TabIndex = 7;
            this.lblSupportPhone.Text = "[Tech Support Phone:]";
            // 
            // lblTechSupportEmail
            // 
            this.lblTechSupportEmail.Location = new System.Drawing.Point(14, 105);
            this.lblTechSupportEmail.Name = "lblTechSupportEmail";
            this.lblTechSupportEmail.Size = new System.Drawing.Size(103, 13);
            this.lblTechSupportEmail.TabIndex = 5;
            this.lblTechSupportEmail.Text = "[Tech Support Email:]";
            // 
            // lblContactNameEdit
            // 
            this.lblContactNameEdit.Location = new System.Drawing.Point(153, 82);
            this.lblContactNameEdit.Name = "lblContactNameEdit";
            this.lblContactNameEdit.Size = new System.Drawing.Size(63, 13);
            this.lblContactNameEdit.TabIndex = 4;
            this.lblContactNameEdit.Text = "labelControl2";
            // 
            // lblTechSupportContact
            // 
            this.lblTechSupportContact.Location = new System.Drawing.Point(14, 82);
            this.lblTechSupportContact.Name = "lblTechSupportContact";
            this.lblTechSupportContact.Size = new System.Drawing.Size(117, 13);
            this.lblTechSupportContact.TabIndex = 3;
            this.lblTechSupportContact.Text = "[Tech Support Contact:]";
            // 
            // lblCopyright
            // 
            this.lblCopyright.Location = new System.Drawing.Point(14, 57);
            this.lblCopyright.Name = "lblCopyright";
            this.lblCopyright.Size = new System.Drawing.Size(63, 13);
            this.lblCopyright.TabIndex = 2;
            this.lblCopyright.Text = "labelControl2";
            // 
            // lblVersionLabel
            // 
            this.lblVersionLabel.Location = new System.Drawing.Point(14, 32);
            this.lblVersionLabel.Name = "lblVersionLabel";
            this.lblVersionLabel.Size = new System.Drawing.Size(43, 13);
            this.lblVersionLabel.TabIndex = 1;
            this.lblVersionLabel.Text = "[Version]";
            // 
            // lblProductName
            // 
            this.lblProductName.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.lblProductName.Appearance.Options.UseBackColor = true;
            this.lblProductName.Location = new System.Drawing.Point(14, 8);
            this.lblProductName.Name = "lblProductName";
            this.lblProductName.Size = new System.Drawing.Size(63, 13);
            this.lblProductName.TabIndex = 0;
            this.lblProductName.Text = "labelControl2";
            // 
            // panelControl3
            // 
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(0, 0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(544, 599);
            this.panelControl3.TabIndex = 11;
            // 
            // tbTemplates
            // 
            this.tbTemplates.Controls.Add(this.grpParticipants);
            this.tbTemplates.Controls.Add(this.grpConferenceSettings);
            this.tbTemplates.Controls.Add(this.grpCreateTemplate);
            this.tbTemplates.Controls.Add(this.panelControl2);
            this.tbTemplates.Name = "tbTemplates";
            this.tbTemplates.Size = new System.Drawing.Size(544, 599);
            this.tbTemplates.Text = "Templates";
            // 
            // grpParticipants
            // 
            this.grpParticipants.Controls.Add(this.simpleButton4);
            this.grpParticipants.Controls.Add(this.btnAddNewParticipant);
            this.grpParticipants.Controls.Add(this.gcParticipants);
            this.grpParticipants.Location = new System.Drawing.Point(3, 401);
            this.grpParticipants.Name = "grpParticipants";
            this.grpParticipants.Size = new System.Drawing.Size(538, 196);
            this.grpParticipants.TabIndex = 2;
            this.grpParticipants.Text = "Participant Settings";
            // 
            // simpleButton4
            // 
            this.simpleButton4.Location = new System.Drawing.Point(306, 165);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(113, 23);
            this.simpleButton4.TabIndex = 2;
            this.simpleButton4.Text = "TrueDaybook Look Up";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // btnAddNewParticipant
            // 
            this.btnAddNewParticipant.Location = new System.Drawing.Point(425, 165);
            this.btnAddNewParticipant.Name = "btnAddNewParticipant";
            this.btnAddNewParticipant.Size = new System.Drawing.Size(108, 23);
            this.btnAddNewParticipant.TabIndex = 1;
            this.btnAddNewParticipant.Text = "Add New Participant";
            this.btnAddNewParticipant.Click += new System.EventHandler(this.btnAddNewParticipant_Click);
            // 
            // gcParticipants
            // 
            this.gcParticipants.Location = new System.Drawing.Point(3, 28);
            this.gcParticipants.MainView = this.gvParticipants;
            this.gcParticipants.Name = "gcParticipants";
            this.gcParticipants.Size = new System.Drawing.Size(530, 133);
            this.gcParticipants.TabIndex = 0;
            this.gcParticipants.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvParticipants,
            this.gridView1});
            // 
            // gvParticipants
            // 
            this.gvParticipants.ActiveFilterEnabled = false;
            this.gvParticipants.GridControl = this.gcParticipants;
            this.gvParticipants.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.Hidden;
            this.gvParticipants.Name = "gvParticipants";
            this.gvParticipants.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.True;
            this.gvParticipants.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.True;
            this.gvParticipants.OptionsCustomization.AllowGroup = false;
            this.gvParticipants.OptionsView.ColumnAutoWidth = false;
            this.gvParticipants.OptionsView.ShowGroupPanel = false;
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gcParticipants;
            this.gridView1.Name = "gridView1";
            // 
            // grpConferenceSettings
            // 
            this.grpConferenceSettings.Controls.Add(this.labelControl16);
            this.grpConferenceSettings.Controls.Add(this.lblDoubleClickRooms);
            this.grpConferenceSettings.Controls.Add(this.roomsList);
            this.grpConferenceSettings.Controls.Add(this.simpleButton1);
            this.grpConferenceSettings.Controls.Add(this.lblMinutes);
            this.grpConferenceSettings.Controls.Add(this.txtMins);
            this.grpConferenceSettings.Controls.Add(this.lblHours);
            this.grpConferenceSettings.Controls.Add(this.txtHours);
            this.grpConferenceSettings.Controls.Add(this.lblDuration);
            this.grpConferenceSettings.Controls.Add(this.txtConferenceName);
            this.grpConferenceSettings.Controls.Add(this.lblConferenceName);
            this.grpConferenceSettings.Location = new System.Drawing.Point(3, 193);
            this.grpConferenceSettings.Name = "grpConferenceSettings";
            this.grpConferenceSettings.Size = new System.Drawing.Size(538, 201);
            this.grpConferenceSettings.TabIndex = 1;
            this.grpConferenceSettings.Text = "Conference Settings";
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl16.Appearance.Options.UseForeColor = true;
            this.labelControl16.Location = new System.Drawing.Point(122, 33);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(6, 13);
            this.labelControl16.TabIndex = 10;
            this.labelControl16.Text = "*";
            // 
            // lblDoubleClickRooms
            // 
            this.lblDoubleClickRooms.Location = new System.Drawing.Point(178, 82);
            this.lblDoubleClickRooms.Name = "lblDoubleClickRooms";
            this.lblDoubleClickRooms.Size = new System.Drawing.Size(237, 13);
            this.lblDoubleClickRooms.TabIndex = 9;
            this.lblDoubleClickRooms.Text = "Double click on the room to remove it from the list";
            // 
            // roomsList
            // 
            this.roomsList.Appearance.BackColor = System.Drawing.Color.White;
            this.roomsList.Appearance.Options.UseBackColor = true;
            this.roomsList.Location = new System.Drawing.Point(133, 99);
            this.roomsList.Name = "roomsList";
            this.roomsList.Size = new System.Drawing.Size(349, 95);
            this.roomsList.TabIndex = 8;
            this.roomsList.DoubleClick += new System.EventHandler(this.roomsList_DoubleClick);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.BackColor = System.Drawing.Color.White;
            this.simpleButton1.Appearance.BackColor2 = System.Drawing.Color.White;
            this.simpleButton1.Appearance.Options.UseBackColor = true;
            this.simpleButton1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.simpleButton1.Location = new System.Drawing.Point(44, 99);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 7;
            this.simpleButton1.Text = "Add Rooms";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // lblMinutes
            // 
            this.lblMinutes.Location = new System.Drawing.Point(285, 62);
            this.lblMinutes.Name = "lblMinutes";
            this.lblMinutes.Size = new System.Drawing.Size(21, 13);
            this.lblMinutes.TabIndex = 6;
            this.lblMinutes.Text = "mins";
            // 
            // txtMins
            // 
            this.txtMins.EditValue = "0";
            this.txtMins.Location = new System.Drawing.Point(220, 59);
            this.txtMins.Name = "txtMins";
            this.txtMins.Size = new System.Drawing.Size(55, 20);
            this.txtMins.TabIndex = 5;
            // 
            // lblHours
            // 
            this.lblHours.Location = new System.Drawing.Point(196, 63);
            this.lblHours.Name = "lblHours";
            this.lblHours.Size = new System.Drawing.Size(15, 13);
            this.lblHours.TabIndex = 4;
            this.lblHours.Text = "hrs";
            // 
            // txtHours
            // 
            this.txtHours.EditValue = "1";
            this.txtHours.Location = new System.Drawing.Point(133, 59);
            this.txtHours.Name = "txtHours";
            this.txtHours.Size = new System.Drawing.Size(55, 20);
            this.txtHours.TabIndex = 3;
            // 
            // lblDuration
            // 
            this.lblDuration.Location = new System.Drawing.Point(74, 59);
            this.lblDuration.Name = "lblDuration";
            this.lblDuration.Size = new System.Drawing.Size(45, 13);
            this.lblDuration.TabIndex = 2;
            this.lblDuration.Text = "Duration:";
            // 
            // txtConferenceName
            // 
            this.txtConferenceName.Location = new System.Drawing.Point(133, 30);
            this.txtConferenceName.Name = "txtConferenceName";
            this.txtConferenceName.Size = new System.Drawing.Size(349, 20);
            this.txtConferenceName.TabIndex = 1;
            // 
            // lblConferenceName
            // 
            this.lblConferenceName.Location = new System.Drawing.Point(29, 30);
            this.lblConferenceName.Name = "lblConferenceName";
            this.lblConferenceName.Size = new System.Drawing.Size(90, 13);
            this.lblConferenceName.TabIndex = 0;
            this.lblConferenceName.Text = "Conference Name:";
            // 
            // grpCreateTemplate
            // 
            this.grpCreateTemplate.Controls.Add(this.labelControl15);
            this.grpCreateTemplate.Controls.Add(this.chkPublic);
            this.grpCreateTemplate.Controls.Add(this.memoTemplateDescription);
            this.grpCreateTemplate.Controls.Add(this.lblTemplateDescription);
            this.grpCreateTemplate.Controls.Add(this.txtTemplateName);
            this.grpCreateTemplate.Controls.Add(this.lblTemplateName);
            this.grpCreateTemplate.Location = new System.Drawing.Point(3, 8);
            this.grpCreateTemplate.Name = "grpCreateTemplate";
            this.grpCreateTemplate.Size = new System.Drawing.Size(538, 179);
            this.grpCreateTemplate.TabIndex = 0;
            this.grpCreateTemplate.Text = "Create Template";
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl15.Appearance.Options.UseForeColor = true;
            this.labelControl15.Location = new System.Drawing.Point(122, 30);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(6, 13);
            this.labelControl15.TabIndex = 5;
            this.labelControl15.Text = "*";
            // 
            // chkPublic
            // 
            this.chkPublic.Location = new System.Drawing.Point(133, 156);
            this.chkPublic.Name = "chkPublic";
            this.chkPublic.Properties.Caption = "Public";
            this.chkPublic.Size = new System.Drawing.Size(75, 19);
            this.chkPublic.TabIndex = 4;
            // 
            // memoTemplateDescription
            // 
            this.memoTemplateDescription.Location = new System.Drawing.Point(133, 53);
            this.memoTemplateDescription.Name = "memoTemplateDescription";
            this.memoTemplateDescription.Size = new System.Drawing.Size(349, 96);
            this.memoTemplateDescription.TabIndex = 3;
            // 
            // lblTemplateDescription
            // 
            this.lblTemplateDescription.Appearance.Options.UseTextOptions = true;
            this.lblTemplateDescription.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lblTemplateDescription.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTemplateDescription.Location = new System.Drawing.Point(45, 51);
            this.lblTemplateDescription.Name = "lblTemplateDescription";
            this.lblTemplateDescription.Size = new System.Drawing.Size(84, 28);
            this.lblTemplateDescription.TabIndex = 2;
            this.lblTemplateDescription.Text = "Template Description";
            // 
            // txtTemplateName
            // 
            this.txtTemplateName.Location = new System.Drawing.Point(132, 28);
            this.txtTemplateName.Name = "txtTemplateName";
            this.txtTemplateName.Size = new System.Drawing.Size(349, 20);
            this.txtTemplateName.TabIndex = 1;
            // 
            // lblTemplateName
            // 
            this.lblTemplateName.Location = new System.Drawing.Point(45, 27);
            this.lblTemplateName.Name = "lblTemplateName";
            this.lblTemplateName.Size = new System.Drawing.Size(74, 13);
            this.lblTemplateName.TabIndex = 0;
            this.lblTemplateName.Text = "Template Name";
            // 
            // panelControl2
            // 
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(544, 599);
            this.panelControl2.TabIndex = 11;
            // 
            // tbAccount
            // 
            this.tbAccount.Appearance.PageClient.BackColor = System.Drawing.Color.Transparent;
            this.tbAccount.Appearance.PageClient.Options.UseBackColor = true;
            this.tbAccount.Controls.Add(this.groupControl2);
            this.tbAccount.Controls.Add(this.groupControl1);
            this.tbAccount.Controls.Add(this.panelControl1);
            this.tbAccount.Name = "tbAccount";
            this.tbAccount.Size = new System.Drawing.Size(544, 599);
            this.tbAccount.Text = "Account";
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.simpleButton2);
            this.groupControl2.Controls.Add(this.labelControl10);
            this.groupControl2.Controls.Add(this.lnkChangeSettings);
            this.groupControl2.Controls.Add(this.lblTimeZone);
            this.groupControl2.Controls.Add(this.labelControl9);
            this.groupControl2.Controls.Add(this.lblLanguage);
            this.groupControl2.Controls.Add(this.labelControl6);
            this.groupControl2.Location = new System.Drawing.Point(3, 267);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(537, 166);
            this.groupControl2.TabIndex = 1;
            this.groupControl2.Text = "Language and Location";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(434, 118);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(98, 27);
            this.simpleButton2.TabIndex = 6;
            this.simpleButton2.Text = "Refresh";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(6, 124);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(362, 13);
            this.labelControl10.TabIndex = 5;
            this.labelControl10.Text = "To see the most recent changes on the TrueDaybook website,  click refresh";
            // 
            // lnkChangeSettings
            // 
            this.lnkChangeSettings.AllowDrop = true;
            this.lnkChangeSettings.AutoSize = true;
            this.lnkChangeSettings.Location = new System.Drawing.Point(97, 92);
            this.lnkChangeSettings.Name = "lnkChangeSettings";
            this.lnkChangeSettings.Size = new System.Drawing.Size(209, 13);
            this.lnkChangeSettings.TabIndex = 4;
            this.lnkChangeSettings.TabStop = true;
            this.lnkChangeSettings.Text = "Change settings for TrueDaybook website";
            this.lnkChangeSettings.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // lblTimeZone
            // 
            this.lblTimeZone.Location = new System.Drawing.Point(97, 65);
            this.lblTimeZone.Name = "lblTimeZone";
            this.lblTimeZone.Size = new System.Drawing.Size(46, 13);
            this.lblTimeZone.TabIndex = 3;
            this.lblTimeZone.Text = "TimeZone";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(39, 64);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(52, 13);
            this.labelControl9.TabIndex = 2;
            this.labelControl9.Text = "Time zone:";
            // 
            // lblLanguage
            // 
            this.lblLanguage.Location = new System.Drawing.Point(97, 39);
            this.lblLanguage.Name = "lblLanguage";
            this.lblLanguage.Size = new System.Drawing.Size(47, 13);
            this.lblLanguage.TabIndex = 1;
            this.lblLanguage.Text = "Language";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(39, 38);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(51, 13);
            this.labelControl6.TabIndex = 0;
            this.labelControl6.Text = "Language:";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.linklblForgotPassword);
            this.groupControl1.Controls.Add(this.btnTestConnection);
            this.groupControl1.Controls.Add(this.chkRememberMe);
            this.groupControl1.Controls.Add(this.txtPassword);
            this.groupControl1.Controls.Add(this.lblPassword);
            this.groupControl1.Controls.Add(this.txtUserName);
            this.groupControl1.Controls.Add(this.lblUserName);
            this.groupControl1.Controls.Add(this.txtServiceURL);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.lblSiteURL);
            this.groupControl1.Controls.Add(this.lblTrueDayBookAccount);
            this.groupControl1.Location = new System.Drawing.Point(3, 8);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(537, 249);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Login Information";
            this.groupControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.groupControl1_Paint);
            this.groupControl1.Click += new System.EventHandler(this.groupControl1_Click);
            // 
            // linklblForgotPassword
            // 
            this.linklblForgotPassword.AutoSize = true;
            this.linklblForgotPassword.Location = new System.Drawing.Point(95, 200);
            this.linklblForgotPassword.Name = "linklblForgotPassword";
            this.linklblForgotPassword.Size = new System.Drawing.Size(118, 13);
            this.linklblForgotPassword.TabIndex = 11;
            this.linklblForgotPassword.TabStop = true;
            this.linklblForgotPassword.Text = "Forgot your password?";
            this.linklblForgotPassword.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked_1);
            // 
            // btnTestConnection
            // 
            this.btnTestConnection.Location = new System.Drawing.Point(434, 217);
            this.btnTestConnection.Name = "btnTestConnection";
            this.btnTestConnection.Size = new System.Drawing.Size(98, 27);
            this.btnTestConnection.TabIndex = 10;
            this.btnTestConnection.Text = "Test Connection";
            this.btnTestConnection.Click += new System.EventHandler(this.btnTestConnection_Click);
            // 
            // chkRememberMe
            // 
            this.chkRememberMe.Location = new System.Drawing.Point(95, 174);
            this.chkRememberMe.Name = "chkRememberMe";
            this.chkRememberMe.Properties.Caption = "Remember my password";
            this.chkRememberMe.Size = new System.Drawing.Size(152, 19);
            this.chkRememberMe.TabIndex = 8;
            this.chkRememberMe.CheckedChanged += new System.EventHandler(this.checkEdit1_CheckedChanged);
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(95, 147);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Properties.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(253, 20);
            this.txtPassword.TabIndex = 7;
            // 
            // lblPassword
            // 
            this.lblPassword.Location = new System.Drawing.Point(39, 149);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(50, 13);
            this.lblPassword.TabIndex = 6;
            this.lblPassword.Text = "Password:";
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(95, 121);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(253, 20);
            this.txtUserName.TabIndex = 5;
            // 
            // lblUserName
            // 
            this.lblUserName.Location = new System.Drawing.Point(33, 123);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(56, 13);
            this.lblUserName.TabIndex = 4;
            this.lblUserName.Text = "User Name:";
            // 
            // txtServiceURL
            // 
            this.txtServiceURL.Location = new System.Drawing.Point(95, 68);
            this.txtServiceURL.Name = "txtServiceURL";
            this.txtServiceURL.Size = new System.Drawing.Size(338, 20);
            this.txtServiceURL.TabIndex = 3;
            this.txtServiceURL.Validating += new System.ComponentModel.CancelEventHandler(this.txtServiceURL_Validating);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(95, 97);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(197, 13);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "Example: http://schedule.ucanytime.com";
            // 
            // lblSiteURL
            // 
            this.lblSiteURL.Location = new System.Drawing.Point(45, 71);
            this.lblSiteURL.Name = "lblSiteURL";
            this.lblSiteURL.Size = new System.Drawing.Size(44, 13);
            this.lblSiteURL.TabIndex = 1;
            this.lblSiteURL.Text = "Site URL:";
            // 
            // lblTrueDayBookAccount
            // 
            this.lblTrueDayBookAccount.Location = new System.Drawing.Point(88, 37);
            this.lblTrueDayBookAccount.Name = "lblTrueDayBookAccount";
            this.lblTrueDayBookAccount.Size = new System.Drawing.Size(253, 13);
            this.lblTrueDayBookAccount.TabIndex = 0;
            this.lblTrueDayBookAccount.Text = "Please enter your TrueDaybook Account Information";
            this.lblTrueDayBookAccount.Click += new System.EventHandler(this.lblTrueDayBookAccount_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(544, 599);
            this.panelControl1.TabIndex = 11;
            // 
            // OptionsTabs
            // 
            this.OptionsTabs.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.OptionsTabs.Appearance.Options.UseBackColor = true;
            this.OptionsTabs.Location = new System.Drawing.Point(12, 12);
            this.OptionsTabs.Name = "OptionsTabs";
            this.OptionsTabs.PaintStyleName = "Skin";
            this.OptionsTabs.SelectedTabPage = this.tbAccount;
            this.OptionsTabs.Size = new System.Drawing.Size(551, 628);
            this.OptionsTabs.TabIndex = 0;
            this.OptionsTabs.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tbAccount,
            this.tbTemplates,
            this.tbAbout});
            // 
            // NewOptionsDlg
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(566, 689);
            this.VerticalScroll.Visible = true; //ZD 102238
            this.VerticalScroll.Enabled = true;
            this.HorizontalScroll.Visible = true; //ZD 102238
            this.HorizontalScroll.Enabled = true;
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.lblRequiredFields1);
            this.Controls.Add(this.OptionsTabs);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NewOptionsDlg";
            this.ShowIcon = false;
            this.Text = "TrueDaybook Options";
            this.Load += new System.EventHandler(this.NewOptionsDlg_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.NewOptionsDlg_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProvider)).EndInit();
            this.tbAbout.ResumeLayout(false);
            this.tbAbout.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.additionalInfoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.tbTemplates.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grpParticipants)).EndInit();
            this.grpParticipants.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcParticipants)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvParticipants)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpConferenceSettings)).EndInit();
            this.grpConferenceSettings.ResumeLayout(false);
            this.grpConferenceSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.roomsList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMins.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHours.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtConferenceName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpCreateTemplate)).EndInit();
            this.grpCreateTemplate.ResumeLayout(false);
            this.grpCreateTemplate.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkPublic.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoTemplateDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTemplateName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.tbAccount.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkRememberMe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceURL.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OptionsTabs)).EndInit();
            this.OptionsTabs.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl lblRequiredFields;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider ErrorProvider;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl lblRequiredFields1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnApply;
        private DevExpress.XtraTab.XtraTabControl OptionsTabs;
        private DevExpress.XtraTab.XtraTabPage tbAccount;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private System.Windows.Forms.LinkLabel lnkChangeSettings;
        private DevExpress.XtraEditors.LabelControl lblTimeZone;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl lblLanguage;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.LinkLabel linklblForgotPassword;
        private DevExpress.XtraEditors.SimpleButton btnTestConnection;
        private DevExpress.XtraEditors.CheckEdit chkRememberMe;
        private DevExpress.XtraEditors.TextEdit txtPassword;
        private DevExpress.XtraEditors.LabelControl lblPassword;
        private DevExpress.XtraEditors.TextEdit txtUserName;
        private DevExpress.XtraEditors.LabelControl lblUserName;
        private DevExpress.XtraEditors.TextEdit txtServiceURL;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl lblSiteURL;
        private DevExpress.XtraEditors.LabelControl lblTrueDayBookAccount;
        private DevExpress.XtraTab.XtraTabPage tbTemplates;
        private DevExpress.XtraEditors.GroupControl grpParticipants;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton btnAddNewParticipant;
        private DevExpress.XtraGrid.GridControl gcParticipants;
        private DevExpress.XtraGrid.Views.Grid.GridView gvParticipants;
        private DevExpress.XtraEditors.GroupControl grpConferenceSettings;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl lblDoubleClickRooms;
        private DevExpress.XtraEditors.ListBoxControl roomsList;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.LabelControl lblMinutes;
        private DevExpress.XtraEditors.TextEdit txtMins;
        private DevExpress.XtraEditors.LabelControl lblHours;
        private DevExpress.XtraEditors.TextEdit txtHours;
        private DevExpress.XtraEditors.LabelControl lblDuration;
        private DevExpress.XtraEditors.TextEdit txtConferenceName;
        private DevExpress.XtraEditors.LabelControl lblConferenceName;
        private DevExpress.XtraEditors.GroupControl grpCreateTemplate;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.CheckEdit chkPublic;
        private DevExpress.XtraEditors.MemoEdit memoTemplateDescription;
        private DevExpress.XtraEditors.LabelControl lblTemplateDescription;
        private DevExpress.XtraEditors.TextEdit txtTemplateName;
        private DevExpress.XtraEditors.LabelControl lblTemplateName;
        private DevExpress.XtraTab.XtraTabPage tbAbout;
        private System.Windows.Forms.LinkLabel lblContactEmailEdit;
        private DevExpress.XtraEditors.LabelControl lblVersionEdit;
        private DevExpress.XtraEditors.MemoEdit additionalInfoEdit;
        private DevExpress.XtraEditors.LabelControl lblAdditionalInformation;
        private DevExpress.XtraEditors.LabelControl lblSupportPhoneEdit;
        private DevExpress.XtraEditors.LabelControl lblSupportPhone;
        private DevExpress.XtraEditors.LabelControl lblTechSupportEmail;
        private DevExpress.XtraEditors.LabelControl lblContactNameEdit;
        private DevExpress.XtraEditors.LabelControl lblTechSupportContact;
        private DevExpress.XtraEditors.LabelControl lblCopyright;
        private DevExpress.XtraEditors.LabelControl lblVersionLabel;
        private DevExpress.XtraEditors.LabelControl lblProductName;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;

    }
}