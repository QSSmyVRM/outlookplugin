﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using MyVrm.WebServices.Data;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Columns;


namespace MyVrm.Outlook.WinForms.Options
{
    public partial class NewOptionsDlg : MyVrm.Outlook.WinForms.Form
    {
        bool UseDefaultCredentials = false;
        private const int ErrCode_SameNameExists = 222;
        BindingList<ConferenceParticipants> participants = new BindingList<ConferenceParticipants>();
        Conference.ConferenceWrapper conWrapper = null;
        
        BindingList<UsersDatasource> OutlookContacts = new BindingList<UsersDatasource>();
        
        public NewOptionsDlg(BindingList<UsersDatasource> outlookContacts)
        {
            try
            {
                InitializeComponent();
                OutlookContacts = outlookContacts;





                this.Text = Strings.OptionsDlgText;




                gcParticipants.DataSource = participants;
            }
            catch (Exception ex)
            {
                MyVrmAddin.TraceSource.TraceException(ex);
                UIHelper.ShowError(ex.Message);
            }
        }

        private void NewOptionsDlg_Load(object sender, EventArgs e)
        {

            LoadAccountSettings();
            LoadAboutDetails();
            LoadTemplates();
        }

        private void LoadTemplates()
        {
            //Localization
            lblRequiredFields1.Text = Strings.OptionsDlgRequiredFields;
            grpCreateTemplate.Text = Strings.OptionsDlgCreateTemplateGroup;
            lblTemplateName.Text = Strings.OptionsDlgTemplateName;
            lblTemplateDescription.Text = Strings.OptionsDlgTemplateDesc;
            chkPublic.Text = Strings.OptionsDlgIsPublic;
            grpConferenceSettings.Text = Strings.OptionsDlgConferenceSettings;
            lblConferenceName.Text = Strings.OptionsDlgConferenceName;
            lblDuration.Text = Strings.OptionsDlgConferenceDuration;
            lblHours.Text = Strings.OptionsDlgHours;
            lblMinutes.Text = Strings.OptionsDlgMinutes;
            lblDoubleClickRooms.Text = Strings.OptionsDlgDoubleClickRooms;
            simpleButton1.Text = Strings.OptionsDlgAddRooms;
            grpParticipants.Text = Strings.OptionsDlgParticipantSettings;
            simpleButton4.Text = Strings.OptionsDlgTrueDayBookLookup;
            btnAddNewParticipant.Text = Strings.OptionsDlgAddNewParticipant;
            btnOK.Text = MyVrm.Outlook.WinForms.Strings.DialogOkButtonText;
            btnApply.Text = MyVrm.Outlook.WinForms.Strings.ApplyBtnText;
            btnCancel.Text = MyVrm.Outlook.WinForms.Strings.DialogCancelButtonText;



            //Events
            gvParticipants.CustomUnboundColumnData += new CustomColumnDataEventHandler(gvParticipants_CustomUnboundColumnData);
            
            gvParticipants.CustomDrawColumnHeader += new ColumnHeaderCustomDrawEventHandler(gvParticipants_CustomDrawColumnHeader);
            gvParticipants.RowCellClick += new RowCellClickEventHandler(gvParticipants_RowCellClick);
            gvParticipants.ShowingEditor += new CancelEventHandler(gvParticipants_ShowingEditor);
            //Add one dummy row
            //ConferenceParticipants participant = new ConferenceParticipants("", "", "", 1, 0, 0, false, 1, 0);
            //participants.Add(participant);

            //Insert a column with icon in it
            GridColumn deleteColumn = new GridColumn();

            deleteColumn.VisibleIndex = 0;
            deleteColumn.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            deleteColumn.ColumnEdit = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.gcParticipants.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            deleteColumn.ColumnEdit});
            deleteColumn.FieldName = "Delete";


           
            // Disable editing.
            deleteColumn.OptionsColumn.AllowEdit = false;
            
            gvParticipants.Columns.Insert(0, deleteColumn);
            //Set some properties
            //No row separator
            gvParticipants.OptionsView.ShowIndicator = false;
            //gvParticipants.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.Click;
            gvParticipants.OptionsView.ShowHorzLines = false;
            gvParticipants.OptionsView.ShowVertLines = false;
            gvParticipants.OptionsView.NewItemRowPosition = NewItemRowPosition.None;
            //Showing first row as editabl;e
            //gvParticipants.ShowEditor();

            //Column properties
            gvParticipants.ColumnPanelRowHeight = 30;

            foreach (GridColumn col in gvParticipants.Columns)
            {
                col.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;

            }
            gvParticipants.Columns[6].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            gvParticipants.Columns[7].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            gvParticipants.Columns[8].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            gvParticipants.Columns[9].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            gvParticipants.Columns[0].Width = 50;
            gvParticipants.Columns[1].Width = 70;
            gvParticipants.Columns[2].Width = 70;
            gvParticipants.Columns[3].Width = 110;
            gvParticipants.Columns[4].Width = 50;
            gvParticipants.Columns[5].Width = 50;
            gvParticipants.Columns[6].Width = 20;
            gvParticipants.Columns[7].Width = 40;
            gvParticipants.Columns[8].Width = 30;
            gvParticipants.Columns[9].Width = 30;

            gvParticipants.Columns[10].Visible = false;
            //Special handling of Radio button
            CreateRadioButtonColumns();
        }

        void gvParticipants_ShowingEditor(object sender, CancelEventArgs e)
        {
            ConferenceParticipants rowParticipants =  gvParticipants.GetRow(gvParticipants.FocusedRowHandle) as ConferenceParticipants;
            if (rowParticipants != null && rowParticipants.ReadOnly && (gvParticipants.FocusedColumn.FieldName == "FirstName" || gvParticipants.FocusedColumn.FieldName == "LastName" || gvParticipants.FocusedColumn.FieldName == "Email"))
                e.Cancel = true;
        }

        void gvParticipants_RowCellClick(object sender, RowCellClickEventArgs e)
        {
            if(e.Column.FieldName =="Delete")
            {
                //Handle delete
                DialogResult dlgResult = MessageBox.Show(Strings.DeleteConfirmation, "TruedayBook", MessageBoxButtons.OKCancel);

                if (dlgResult == DialogResult.OK)
                {
                    gvParticipants.DeleteRow(e.RowHandle);
                }
                
                e.Handled=true;
            }
        }

        private void CreateRadioButtonColumns()
        {
            GridColumn gcAttendee = gvParticipants.Columns[4];
            DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup sAttendee = new DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup();
            DevExpress.XtraEditors.Controls.RadioGroupItem gAttendee = new DevExpress.XtraEditors.Controls.RadioGroupItem();
            sAttendee.SelectedIndexChanged += new EventHandler(sAttendee_SelectedIndexChanged);
            gAttendee.Value = 1;
            sAttendee.Items.Add(gAttendee);

            gcAttendee.ColumnEdit = sAttendee;
            this.gcParticipants.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            gcAttendee.ColumnEdit});

            GridColumn gcRoomAttendee = gvParticipants.Columns[5];
            DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup sRoomAttendee = new DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup();
            DevExpress.XtraEditors.Controls.RadioGroupItem gRoomAttendee = new DevExpress.XtraEditors.Controls.RadioGroupItem();
            sRoomAttendee.SelectedIndexChanged += new EventHandler(sRoomAttendee_SelectedIndexChanged);
            gRoomAttendee.Value = 1;
            sRoomAttendee.Items.Add(gAttendee);

            gcRoomAttendee.ColumnEdit = sRoomAttendee;
            this.gcParticipants.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            gcRoomAttendee.ColumnEdit});


            GridColumn gcCC = gvParticipants.Columns[6];
            DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup sCC = new DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup();
            DevExpress.XtraEditors.Controls.RadioGroupItem gCCAttendee = new DevExpress.XtraEditors.Controls.RadioGroupItem();
            sCC.SelectedIndexChanged += new EventHandler(sCC_SelectedIndexChanged);
            gCCAttendee.Value = 1;
            sCC.Items.Add(gCCAttendee);

            gcCC.ColumnEdit = sCC;
            this.gcParticipants.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            gcCC.ColumnEdit});

            GridColumn gcVideo = gvParticipants.Columns[8];
            DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup sVideo = new DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup();
            DevExpress.XtraEditors.Controls.RadioGroupItem gVideo = new DevExpress.XtraEditors.Controls.RadioGroupItem();
            sVideo.SelectedIndexChanged += new EventHandler(sVideo_SelectedIndexChanged);
            gVideo.Value = 1;
            sVideo.Items.Add(gVideo);

            gcVideo.ColumnEdit = sVideo;
            this.gcParticipants.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            gcVideo.ColumnEdit});

            GridColumn gcAudio = gvParticipants.Columns[9];
            DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup sAudio = new DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup();
            DevExpress.XtraEditors.Controls.RadioGroupItem gAudio = new DevExpress.XtraEditors.Controls.RadioGroupItem();

            gAudio.Value = 1;
            sAudio.Items.Add(gAudio);
            sAudio.SelectedIndexChanged += new EventHandler(sAudio_SelectedIndexChanged);
            gcAudio.ColumnEdit = sAudio;
            this.gcParticipants.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            gcAudio.ColumnEdit});
        }

        void sAudio_SelectedIndexChanged(object sender, EventArgs e)
        {
            gvParticipants.SetRowCellValue(gvParticipants.FocusedRowHandle, "Video", 0);
        }

        void sVideo_SelectedIndexChanged(object sender, EventArgs e)
        {
            gvParticipants.SetRowCellValue(gvParticipants.FocusedRowHandle, "Audio", 0);
        }

        void sCC_SelectedIndexChanged(object sender, EventArgs e)
        {
            gvParticipants.SetRowCellValue(gvParticipants.FocusedRowHandle, "ExternalAttendee", 0);
            gvParticipants.SetRowCellValue(gvParticipants.FocusedRowHandle, "RoomAttendee", 0);
        }

        void sRoomAttendee_SelectedIndexChanged(object sender, EventArgs e)
        {
            gvParticipants.SetRowCellValue(gvParticipants.FocusedRowHandle, "ExternalAttendee", 0);
            gvParticipants.SetRowCellValue(gvParticipants.FocusedRowHandle, "CC", 0);
        }

        void sAttendee_SelectedIndexChanged(object sender, EventArgs e)
        {
            gvParticipants.SetRowCellValue(gvParticipants.FocusedRowHandle, "RoomAttendee", 0);
            gvParticipants.SetRowCellValue(gvParticipants.FocusedRowHandle, "CC", 0);
        }


        /// <summary>
        /// This function handles the custom painting of cell headers
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void gvParticipants_CustomDrawColumnHeader(object sender, ColumnHeaderCustomDrawEventArgs e)
        {
            e.Appearance.DrawBackground(e.Cache, e.Bounds);

            
            foreach (DevExpress.Utils.Drawing.DrawElementInfo info in e.Info.InnerElements)
            {

                DevExpress.Utils.Drawing.ObjectPainter.DrawObject(e.Cache, info.ElementPainter,
                    info.ElementInfo);
            }
            e.Painter.DrawCaption(e.Info, e.Info.Caption, e.Appearance.Font, e.Appearance.GetBorderBrush(e.Cache), e.Bounds, e.Appearance.GetStringFormat());
            e.Handled = true;
        }

        
        void gvParticipants_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            if (e.Column.FieldName == "Delete" && e.IsGetData)
            {

                //Set the icon for Delete button
                e.Value = MyVrm.Outlook.WinForms.Properties.Resources.recycle;

            }
        }

        private void LoadAboutDetails()
        {
            lblProductName.BackColor = panelControl3.BackColor;
            lblVersionEdit.BackColor = panelControl3.BackColor;
            lblCopyright.BackColor = panelControl3.BackColor;

            lblVersionLabel.BackColor = panelControl3.BackColor;
            lblTechSupportContact.BackColor = panelControl3.BackColor;

            lblTechSupportEmail.BackColor = panelControl3.BackColor;

            lblSupportPhone.BackColor = panelControl3.BackColor;

            lblAdditionalInformation.BackColor = panelControl3.BackColor;



            //var contactInfo = MyVrmService.Service.OrganizationOptions.Contact;
            lblContactNameEdit.BackColor = panelControl3.BackColor;
            //contactEmailEdit.Text = "mailto:" + "support@myvrm.com"; //commented because from this dialog there is no possibility to open an e-mail
            lblContactEmailEdit.BackColor = panelControl3.BackColor;
            lblSupportPhoneEdit.BackColor = panelControl3.BackColor;

            lblProductName.Text = MyVrmAddin.ProductDisplayName;//ProductName;
            lblVersionEdit.Text = MyVrmAddin.ProductVersion;
            lblCopyright.Text = MyVrmAddin.Copyright;

            lblVersionLabel.Text = MyVrm.Outlook.WinForms.Strings.AboutDialogVersionText;
            lblTechSupportContact.Text = MyVrm.Outlook.WinForms.Strings.AboutDialogSupportContactText;
            
            lblTechSupportEmail.Text = MyVrm.Outlook.WinForms.Strings.AboutDialogSupportEmailText;
            
            lblSupportPhone.Text = MyVrm.Outlook.WinForms.Strings.AboutDialogSupportPhoneText;
            
            lblAdditionalInformation.Text = MyVrm.Outlook.WinForms.Strings.AboutDialogInfoText;
            

           // 102631 start
            var contactInfo = MyVrmService.Service.OrganizationOptions.Contact;
            lblContactNameEdit.Text = contactInfo.Name;//contactInfo.Name;
            //lblContactEmailEdit.Text = "mailto:" + contactInfo.Email; //commented because from this dialog there is no possibility to open an e-mail
            lblContactEmailEdit.Text = contactInfo.Email; //contactInfo.Email;
            lblSupportPhoneEdit.Text = contactInfo.Phone; //contactInfo.Phone;
            additionalInfoEdit.Text = contactInfo.AdditionInfo;//contactInfo.AdditionInfo;
            // 102631 End
            
        }

        private void LoadAccountSettings()
        {
            //Localization
            lblUserName.Text = WinForms.Strings.UserNameText;
            lblPassword.Text = WinForms.Strings.PasswordText;
            lblSiteURL.Text = WinForms.Strings.SiteURLText;
            btnTestConnection.Text = WinForms.Strings.TestConnectionLableText;
            linklblForgotPassword.Text = WinForms.Strings.ForgotPasswordText;
            chkRememberMe.Text = WinForms.Strings.RememberMeText;
            lblTrueDayBookAccount.Text = Strings.TrueDayBookAccountInformation;
            lnkChangeSettings.Text = Strings.ChangeSettingsText;


            var settings = MyVrmAddin.Instance.Settings;
            txtServiceURL.Text = settings.MyVrmServiceUrl;//"http://daybook.myvrm.com";
            UseDefaultCredentials = settings.UseDefaultCredentials;
            chkRememberMe.Checked = settings.RememberMe;
            txtUserName.Text = settings.UserName;
            if (settings.RememberMe)
                txtPassword.Text = settings.UserPassword;
            else
                txtPassword.Text = string.Empty;

            LoadTimeSettings();
            
        }

        private void LoadTimeSettings()
        {
            try
            {
                User us = MyVrmService.Service.GetUser();
                lblTimeZone.Text = us.TimeZone.DisplayName;
                lblLanguage.Text = us.Language.DisplayName;
            }
            catch
            {
                lblTimeZone.Text = string.Empty;
                lblLanguage.Text = string.Empty;
            }
        }

        private bool ValidateTextEdit(TextEdit textEdit, string errorText, CancelEventArgs e)
        {
            bool bValidated = true;
            if (string.IsNullOrEmpty(textEdit.Text) || string.IsNullOrEmpty(textEdit.Text.Trim()))
            {
                ErrorProvider.SetError(textEdit, errorText);
                bValidated = false;
            }
            else
            {
                ErrorProvider.SetError(textEdit, "");
            }
            return bValidated;
        }
        private void groupControl1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            
        }

        private void groupControl1_Click(object sender, EventArgs e)
        {

        }

        

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void linkLabel1_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (txtServiceURL.Text != null && txtServiceURL.Text.Length > 0)
            {
                string server = txtServiceURL.Text;

                if (!server.EndsWith("/"))
                    server += "/";
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo = new System.Diagnostics.ProcessStartInfo();
                p.StartInfo.FileName = server + "en/emaillogin.aspx";
                p.Start();
            }
        }

        private void btnTestConnection_Click(object sender, EventArgs e)
        {
            Cursor cursor = Cursor;
            try
            {
                Cursor = Cursors.WaitCursor;
                MyVrmService.TestConnection(txtServiceURL.Text, MyVrm.WebServices.Data.AuthenticationMode.Custom,
                                                  new NetworkCredential(txtUserName.Text, txtPassword.Text));//ZD 101226
                ShowMessage(WinForms.Strings.TestConnectionSucceeded);
                btnOK_Click(sender, e);//ZD 101544
            }
            catch (Exception exception)
            {
                ShowError(string.Format(WinForms.Strings.TestConnectionFailed, exception.Message));
            }
            finally
            {
                Cursor = cursor;
            }
        }

        private void NewOptionsDlg_FormClosing(object sender, FormClosingEventArgs e)
        {
           
        }

        private bool SaveData(ref bool bTemplateCreated)
        {
            bool bRet = false;
            if (!ValidateControls())
            {

                return bRet;
            }
            var settings = MyVrmAddin.Instance.Settings;
            //ZD 101226 Starts
            //SaveAccountOptions(settings);
            settings.MyVrmServiceUrl = txtServiceURL.Text;//"http://daybook.myvrm.com";
            settings.AuthenticationMode = MyVrm.WebServices.Data.AuthenticationMode.Custom;
            settings.UseDefaultCredentials = false;
            settings.RememberMe = chkRememberMe.Checked;
            settings.UserName = txtUserName.Text;
            if (chkRememberMe.Checked)
                settings.UserPassword = txtPassword.Text;
            else
                settings.UserPassword = string.Empty;
            
            settings.Save();
            //ZD 101226 Ends
            bRet = SaveTemplateData(ref bTemplateCreated);
            return bRet;

        }

        private bool SaveTemplateData(ref bool bTemplateCreated)
        {
            bool bSaved = true;
            bTemplateCreated = false;
            if(txtTemplateName != null && txtTemplateName.Text.Trim().Length >0 )
            {
                bSaved = false;

                //Validate Participants
                for (int i = 0; i < gvParticipants.RowCount; i++)
                {
                    int rowHandle = gvParticipants.GetVisibleRowHandle(i);

                    if (gvParticipants.IsDataRow(rowHandle))
                    {
                         ConferenceParticipants part = gvParticipants.GetRow(rowHandle) as ConferenceParticipants;
                         if (!part.ReadOnly)
                         {
                             for (int iColNumber = 1; iColNumber < 4; iColNumber++)
                             {
                                 object v = gvParticipants.GetRowCellValue(rowHandle, gvParticipants.VisibleColumns[iColNumber]);


                                 if (!ValidateColumnValue(iColNumber, v))
                                     return bSaved;

                             }
                         }
                    }
                }
            
                if (txtConferenceName.Text == null || txtConferenceName.Text.Trim().Length == 0)
                {
                    UIHelper.ShowMessage(MyVrm.Outlook.WinForms.Strings.ConferenceNameMandatory, MessageBoxButtons.OK,
                                    MessageBoxIcon.Exclamation);
                    return bSaved;
                }
                conWrapper = new MyVrm.Outlook.WinForms.Conference.ConferenceWrapper(MyVrmService.Service);
                int hours = 0;
                int mins = 0;
                if (txtHours.Text != null && txtHours.Text.Trim().Length > 0)
                    Int32.TryParse(txtHours.Text, out hours);
                if (txtMins.Text != null && txtMins.Text.Trim().Length > 0)
                    Int32.TryParse(txtMins.Text, out mins);
                TimeSpan duration = new TimeSpan(hours, mins, 0);
                if (duration.TotalHours < 0.15)
                {
                    UIHelper.ShowMessage(MyVrm.Outlook.WinForms.Strings.MinimumDuration, MessageBoxButtons.OK,
                                    MessageBoxIcon.Exclamation);
                    return bSaved;

                }

                conWrapper.Conference.Name = txtConferenceName.Text;
                conWrapper.Conference.Duration = duration;
                
                for(int i =0;i<roomsList.Items.Count;i++)
                {
                    ConferenceRoomsList room = roomsList.Items[i] as ConferenceRoomsList;
                    if (room != null)
                    {
                        RoomId newRoom = new RoomId(room.Id);
                        
                        conWrapper.Conference.Rooms.Add(newRoom);
                    }
                }

                foreach (ConferenceParticipants participant in participants)
                {
                    Participant newParticipant = new Participant(participant.FirstName, participant.LastName, participant.Email);
                    newParticipant.Notify = participant.Notify;
                    if (participant.ExternalAttendee == 1)
                        newParticipant.InvitationMode = ParticipantInvitationMode.External;
                    else if (participant.RoomAttendee == 1)
                        newParticipant.InvitationMode = ParticipantInvitationMode.Room;
                    else if (participant.CC == 1)
                        newParticipant.InvitationMode = ParticipantInvitationMode.CcOnly;

                    if (participant.Audio == 1)
                        newParticipant.AudioVideoMode = MediaType.AudioOnly;
                    else if (participant.Video == 1)
                        newParticipant.AudioVideoMode = MediaType.AudioVideo;


                    conWrapper.Participants.Add(newParticipant);
                }
                try
                {
                    MyVrmService.Service.ConferenceSetTemplate(conWrapper.Conference, txtTemplateName.Text.Trim(), memoTemplateDescription.Text.Trim(), chkPublic.Checked, false);
                    bSaved = true;
                    bTemplateCreated = true;
                }
                catch (MyVrmServiceException exception)
                {
                    if (exception.ErrorCode == ErrCode_SameNameExists) //Template with a such name is already exists
                    {
                        UIHelper.ShowMessage(MyVrm.Outlook.WinForms.Strings.TemplateAlreadyExists, MessageBoxButtons.OK,
                                     MessageBoxIcon.Exclamation);
                        
                        txtTemplateName.Select();
                    }
                    else
                    {
                        MyVrmAddin.TraceSource.TraceException(exception, true);
                        UIHelper.ShowMessage(exception.Message, MessageBoxButtons.OK,
                                     MessageBoxIcon.Exclamation);
                        
                    }
                }
                catch (Exception exception)
                {
                    MyVrmAddin.TraceSource.TraceException(exception, true);
                    UIHelper.ShowMessage(exception.Message, MessageBoxButtons.OK,
                                     MessageBoxIcon.Exclamation);
                 
                }
            
            }
            return bSaved;
        }

        private bool ValidateControls()
        {
            bool bValidated = true;
            CancelEventArgs e = new CancelEventArgs();
            bValidated = ValidateTextEdit(txtServiceURL, Strings.WebServiceUrlCannotBeEmpty, e);
            if (!bValidated)
            {
                ShowError(Strings.WebServiceUrlCannotBeEmpty);
                return bValidated;
            }

            bValidated = ValidateTextEdit(txtUserName, Strings.UserNameCannotBeEmpty, e);
            if (!bValidated)
            {
                ShowError(Strings.UserNameCannotBeEmpty);
                return bValidated;
            }

            bValidated = ValidateTextEdit(txtPassword, Strings.PasswordCannotBeEmpty, e);
            if (!bValidated)
            {
                ShowError(Strings.PasswordCannotBeEmpty);
                return bValidated;
            }

            return bValidated;
        }

        private void SaveAccountOptions(Settings settings)
        {
            
            settings.MyVrmServiceUrl = txtServiceURL.Text ;//"http://daybook.myvrm.com";
            settings.UseDefaultCredentials = false; //ZD 101226
            settings.RememberMe = chkRememberMe.Checked;
            settings.UserName = txtUserName.Text;
            if (chkRememberMe.Checked)
                settings.UserPassword = txtPassword.Text;
            else
                settings.UserPassword = string.Empty;
            
        }

        private void txtServiceURL_Validating(object sender, CancelEventArgs e)
        {
            var textEdit = (TextEdit)sender;
            ValidateTextEdit(textEdit, Strings.WebServiceUrlCannotBeEmpty, e);
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            LoadTimeSettings();
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process p = new System.Diagnostics.Process();
            p.StartInfo = new System.Diagnostics.ProcessStartInfo();
            p.StartInfo.FileName = txtServiceURL.Text;
            p.Start();

        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            ShowSelectRoomsDialog();
        }
        protected virtual void ShowSelectRoomsDialog()
        {
            using (var dlg = new MyVrm.Outlook.WinForms.Conference.RoomSelectionDialog())
            {
                User us = MyVrmService.Service.GetUser();
                
                List<RoomId> lstRooms = new List<RoomId>();
                dlg.SelectedRooms = lstRooms.ToArray() ;
                dlg.Conf = new MyVrm.Outlook.WinForms.Conference.ConferenceWrapper(MyVrmService.Service);
                
                dlg.Conf.Conference.StartDate = DateTime.Now;
                int nHours = 1;
                int nMins = 0;
                if (txtHours != null && txtHours.Text.Trim().Length > 0)
                    Int32.TryParse(txtHours.Text, out nHours);
                if (txtMins != null && txtMins.Text.Trim().Length > 0)
                    Int32.TryParse(txtMins.Text, out nMins);
                TimeSpan duration = new TimeSpan(nHours, nMins, 0);
                dlg.Conf.Conference.EndDate = DateTime.Now.Add(new TimeSpan(System.Convert.ToInt32(txtHours.Text), System.Convert.ToInt32(txtMins.Text), 0));
                dlg.Conf.Conference.Duration = duration;

                if (dlg.ShowDialog(this) == DialogResult.OK)
                {
                    RoomId[] rooms = dlg.SelectedRooms;
                    
                    foreach (RoomId room in rooms)
                    {
                        ConferenceRoomsList confRoom = new ConferenceRoomsList();
                        confRoom.Id = room.Id;
                        confRoom.DisplayMember = room.RoomName;
                        roomsList.Items.Add(confRoom);

                    }
                    roomsList.Refresh();
                    
                }


                
            }

            //using (var dlg = new RoomSelectionDialog())
            //{
            //    dlg.SelectedRooms = Conference.LocationIds.ToArray();
            //    dlg.ConferenceType = Conference.Type;
            //    dlg.Conf = Conference;
            //    if (dlg.ShowDialog(this) == DialogResult.OK)
            //    {
            //        AddRoomResources(dlg.SelectedRooms);
            //    }
            //}
        }

        private void roomsList_DoubleClick(object sender, EventArgs e)
        {
            object selItem = roomsList.SelectedItem;
            if (selItem != null)
                roomsList.Items.Remove(selItem);
        }

        private void btnAddNewParticipant_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < gvParticipants.RowCount; i++)
            {
                int rowHandle = gvParticipants.GetVisibleRowHandle(i);
               
                if (gvParticipants.IsDataRow(rowHandle))
                {
                    ConferenceParticipants part = gvParticipants.GetRow(rowHandle) as ConferenceParticipants;
                    if (!part.ReadOnly)
                    {
                        for (int iColNumber = 1; iColNumber < 4; iColNumber++)
                        {
                            object v = gvParticipants.GetRowCellValue(rowHandle, gvParticipants.VisibleColumns[iColNumber]);


                            if (!ValidateColumnValue(iColNumber, v))
                                return;

                        }
                    }
                }
            }
            
            for (int i = 0; i < gvParticipants.RowCount; i++)
            {
                int rowHandle = gvParticipants.GetVisibleRowHandle(i);

                if (gvParticipants.IsDataRow(rowHandle))
                {
                    ConferenceParticipants part =  gvParticipants.GetRow(rowHandle) as ConferenceParticipants;
                    if (part != null)
                        part.ReadOnly = true;
                }
            }
            ConferenceParticipants participant = new ConferenceParticipants("", "", "", 1, 0, 0, false, 1, 0);
            participants.Insert(0,participant);

            gvParticipants.FocusedRowHandle = 0;
            gvParticipants.FocusedColumn = gvParticipants.Columns[1];
            gvParticipants.ShowEditor();


        }

        private bool ValidateColumnValue(int iColNumber, object value)
        {
            bool bValidated = true;
            switch (iColNumber)
            {
                case 1:
                    if (value == null || value.ToString().Trim().Length == 0)
                    {
                        bValidated = false;
                        MessageBox.Show(MyVrm.Outlook.WinForms.Options.Strings.FirstNameEmpty);
                    }
                    break;
                case 2:
                    if (value == null || value.ToString().Trim().Length == 0)
                    {
                        MessageBox.Show(MyVrm.Outlook.WinForms.Options.Strings.LastNameEmpty);
                        bValidated = false;
                    }
                    break;
                case 3:
                    if (value == null || value.ToString().Trim().Length == 0)
                    {
                        MessageBox.Show(MyVrm.Outlook.WinForms.Options.Strings.EmailEmpty);
                        bValidated = false;
                    }

                    break;
                default:
                    break;
            }


            return bValidated;
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < gvParticipants.RowCount; i++)
            {
                int rowHandle = gvParticipants.GetVisibleRowHandle(i);

                if (gvParticipants.IsDataRow(rowHandle))
                {
                     ConferenceParticipants part = gvParticipants.GetRow(rowHandle) as ConferenceParticipants;
                     if (!part.ReadOnly)
                     {
                         for (int iColNumber = 1; iColNumber < 4; iColNumber++)
                         {
                             object v = gvParticipants.GetRowCellValue(rowHandle, gvParticipants.VisibleColumns[iColNumber]);



                             if (!ValidateColumnValue(iColNumber, v))
                                 return;

                         }
                     }
                }
            }

            for (int i = 0; i < gvParticipants.RowCount; i++)
            {
                int rowHandle = gvParticipants.GetVisibleRowHandle(i);

                if (gvParticipants.IsDataRow(rowHandle))
                {
                    ConferenceParticipants part = gvParticipants.GetRow(rowHandle) as ConferenceParticipants;
                    if (part != null)
                        part.ReadOnly = true;
                }
            }
            UsersDialog uDlg = new UsersDialog(OutlookContacts);
            if (uDlg.ShowDialog() == DialogResult.OK)
            {
                List<UsersDatasource> selectedUsers =  uDlg.SelectedUsers;
                int i = 0;
                int iFirstRowHandle = -1;
                foreach (UsersDatasource us in selectedUsers)
                {
                    ConferenceParticipants confParticipant = new ConferenceParticipants(us.FirstName, us.LastName, us.Email, 0, 1, 0, true, 1, 0);
                    confParticipant.ReadOnly = true;
                    participants.Add(confParticipant);
                    
                }

                object objRow = gvParticipants.GetRow(iFirstRowHandle);
                uDlg.ClearSelectedUsers();
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            bool bTemplateCreated = false;
            if (SaveData(ref bTemplateCreated))
            {
                if(bTemplateCreated)
                    MessageBox.Show(Strings.TemplateCreatedSuccessfully);
                this.Close();
            }
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            bool bTemplateCreated = false;
            if (SaveData(ref bTemplateCreated))
            {
                if(bTemplateCreated)
                    MessageBox.Show(Strings.TemplateCreatedSuccessfully);
                ClearTemplateControls();
            }
        }

        private void ClearTemplateControls()
        {
            txtTemplateName.Text = string.Empty;
            txtConferenceName.Text = string.Empty;
            roomsList.Items.Clear();
            memoTemplateDescription.Text = string.Empty;
            chkPublic.Checked = false;
            participants.Clear();

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblTrueDayBookAccount_Click(object sender, EventArgs e)
        {

        }

       

       
    }


}
