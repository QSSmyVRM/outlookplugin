﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraTreeList.Nodes;
using MyVrm.Common.ComponentModel;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook.WinForms.Conference
{
    public partial class CateringWorkOrdersPage : ConferencePage
    {
        private class WorkOrderRecord
        {
            private Room _room;
            private CateringWorkOrder _workOrder;

            internal WorkOrderRecord(Room room, CateringWorkOrder workOrder)
            {
                _room = room;
                _workOrder = workOrder;
            }

            public RoomId RoomId
            {
                get
                {
                    return _room.Id;
                }
            }
            public string RoomName
            {
                get
                {
                    return _room.Name;
                }
            }
            public WorkOrderId Id
            {
                get
                {
                    return _workOrder != null ? _workOrder.Id : null;
                }
            }
            public string Name
            {
                get
                {
                    return _workOrder != null ? _workOrder.Name : null;
                }
            }

            public uint? Quantity
            {
                get
                {
                    if (_workOrder != null)
                    {
                        long quantity = _workOrder.Menus.Sum(menu => menu.Quantity);
                        return Convert.ToUInt32(quantity);
                    }
                    return null;
                }
            }

            internal CateringWorkOrder WorkOrder
            {
                get
                {
                    return _workOrder;
                }
                set
                {
                    _workOrder = value;
                }
            }

            internal void DetachWorkOrder()
            {
                _workOrder = null;
            }
        }

        private readonly DataList<WorkOrderRecord> _workOrderRecords = new DataList<WorkOrderRecord>();

        public CateringWorkOrdersPage()
        {
            InitializeComponent();
            Text = Strings.CateringWorkOrdersPageText;
			bar1.Text = Strings.ToolsBarText;
        	roomNameColumn.Caption = Strings.RoomLableText;
			quantityColumn.Caption = Strings.QuantityColumnText;

            workOrderList.DataSource = _workOrderRecords;
			assignWorkOrderBarButtonItem.Caption = Strings.AssignMenuItemText;
			editWorkOrderBarButtonItem.Caption = Strings.EditMenuItemText;
			removeWorkOrderBarButtonItem.Caption = Strings.RemoveMenuItemText;
        }

        protected override void OnReadOnlyChanged(ReadOnlyChangedEventArgs e)
        {
            base.OnReadOnlyChanged(e);
            bar1.Visible = !e.ReadOnly;
            workOrderList.Enabled = !e.ReadOnly;
        }

        void WorkOrderRecordsListChanged(object sender, System.ComponentModel.ListChangedEventArgs e)
        {
            switch (e.ListChangedType)
            {
                case ListChangedType.Reset:
                    break;
                case ListChangedType.ItemAdded:
                    break;
                case ListChangedType.ItemDeleted:
                    break;
                case ListChangedType.ItemMoved:
                    break;
                case ListChangedType.ItemChanged:
                    {
                        EnableBarButtons(_workOrderRecords[e.NewIndex].WorkOrder);
                        break;
                    }
                case ListChangedType.PropertyDescriptorAdded:
                    break;
                case ListChangedType.PropertyDescriptorDeleted:
                    break;
                case ListChangedType.PropertyDescriptorChanged:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void EnableBarButtons(CateringWorkOrder workOrder)
        {
            var isWorkOrderAssigned = workOrder != null;
            assignWorkOrderBarButtonItem.Enabled = !isWorkOrderAssigned;
            editWorkOrderBarButtonItem.Enabled = isWorkOrderAssigned;
            removeWorkOrderBarButtonItem.Enabled = isWorkOrderAssigned;
        }

        private void EnableBarButtons(TreeListNode node)
        {
            if (node != null)
            {
                EnableBarButtons(_workOrderRecords[node.Id].WorkOrder);
            }
            else
            {
                assignWorkOrderBarButtonItem.Enabled = false;
                editWorkOrderBarButtonItem.Enabled = false;
                removeWorkOrderBarButtonItem.Enabled = false;
            }
        }

        void ConferenceBindingSource_DataSourceChanged(object sender, EventArgs e)
        {
            ResetData();
        }

        private void ResetData()
        {
            if (Conference != null)
            {
                Conference.LocationIds.ListChanged += LocationIds_ListChanged;
                ResetWorkOrdersRecords();
            }
        }

        private void CateringWorkOrdersPage_Load(object sender, EventArgs e)
        {
            ConferenceBindingSource.DataSourceChanged += ConferenceBindingSource_DataSourceChanged;
            _workOrderRecords.ListChanged += WorkOrderRecordsListChanged;
            ResetData();
        }

        void LocationIds_ListChanged(object sender, ListChangedEventArgs e)
        {
            switch (e.ListChangedType)
            {
                case ListChangedType.Reset:
                    {
                        ResetWorkOrdersRecords();
                        break;
                    }
                case ListChangedType.ItemAdded:
                    {
                        var room = MyVrmAddin.Instance.GetRoomFromCache(Conference.LocationIds[e.NewIndex]);
                        CateringWorkOrder workorder = Conference.Conference.CateringWorkOrders.FirstOrDefault(order => order.RoomId.Equals(room.Id));
                        _workOrderRecords.Add(new WorkOrderRecord(room, workorder));
                        break;
                    }
                case ListChangedType.ItemDeleted:
                    {
                        _workOrderRecords.RemoveAt(e.NewIndex);
                        break;
                    }
                case ListChangedType.ItemMoved:
                    {
                        var workOrderRecord = _workOrderRecords[e.OldIndex];
                        _workOrderRecords.Move(workOrderRecord, e.NewIndex);
                        break;
                    }
                case ListChangedType.ItemChanged:
                    break;
                case ListChangedType.PropertyDescriptorAdded:
                    break;
                case ListChangedType.PropertyDescriptorDeleted:
                    break;
                case ListChangedType.PropertyDescriptorChanged:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            workOrderList.RefreshDataSource();
        }

        private void ResetWorkOrdersRecords()
        {
            _workOrderRecords.Clear();
            var query = from roomId in Conference.LocationIds
                        join workOrder in Conference.Conference.CateringWorkOrders on roomId equals
                            workOrder.RoomId into workOrders
                        from wo in workOrders.DefaultIfEmpty()
                        select new WorkOrderRecord(MyVrmAddin.Instance.GetRoomFromCache(roomId), wo);

            _workOrderRecords.AddRange(query);
        }
        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            AssignWorkOrder();
        }

        private void AssignWorkOrder()
        {
            if (workOrderList.FocusedNode == null)
            {
                return;
            }
            using (var dialog = new CateringWorkOrderDialog())
            {
                var workOrderRecord = _workOrderRecords[workOrderList.FocusedNode.Id];
                dialog.WorkOrder = new CateringWorkOrder(MyVrmService.Service)
                {
                    Name = string.Format("{0}_CAT", Conference.Conference.Name),
                    RoomId = workOrderRecord.RoomId,
                    DeliveryTime = Conference.StartDate,
                    Price = 0
                };
                if (dialog.ShowDialog(this) == DialogResult.OK)
                {
                    Conference.Conference.CateringWorkOrders.Add(dialog.WorkOrder);
                    var record = _workOrderRecords.FirstOrDefault(rec => rec.RoomId == dialog.WorkOrder.RoomId);
                    record.WorkOrder = dialog.WorkOrder;
                    _workOrderRecords.ResetItem(workOrderList.FocusedNode.Id);
					if (Conference != null)
						Conference.Appointment.SetNonOutlookProperty(true); 
                }
            }
        }
        private void editWorkOrderBarButtonItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EditWorkOrder();
        }

        private void EditWorkOrder()
        {
            if (workOrderList.FocusedNode == null)
            {
                return;
            }
            using (var dialog = new CateringWorkOrderDialog())
            {
                var workOrderRecord = _workOrderRecords[workOrderList.FocusedNode.Id];
                dialog.WorkOrder = workOrderRecord.WorkOrder;
                if (dialog.ShowDialog(this) == DialogResult.OK)
                {
                    _workOrderRecords.ResetItem(workOrderList.FocusedNode.Id);
					if (Conference != null)
						Conference.Appointment.SetNonOutlookProperty(true);
                }
            }
        }

        private void removeWorkOrderBarButtonItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (workOrderList.FocusedNode != null)
            {
                var workOrderRecord = _workOrderRecords[workOrderList.FocusedNode.Id];
                Conference.Conference.CateringWorkOrders.Remove(workOrderRecord.WorkOrder);
                workOrderRecord.DetachWorkOrder();
                _workOrderRecords.ResetItem(workOrderList.FocusedNode.Id);
				if (Conference != null)
					Conference.Appointment.SetNonOutlookProperty(true);
            }
        }

        private void workOrderList_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            EnableBarButtons(e.Node);
        }

        private void workOrderList_DoubleClick(object sender, EventArgs e)
        {
            if (workOrderList.FocusedNode != null)
            {
                if (_workOrderRecords[workOrderList.FocusedNode.Id].WorkOrder == null)
                {
                    AssignWorkOrder();
                }
                else
                {
                    EditWorkOrder();
                }
            }
        }
    }
}
