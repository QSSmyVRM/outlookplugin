﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;


namespace MyVrm.WebServices.Data
{
    internal class GetoldRecurranceRequest : ServiceRequestBase<GetoldRecurranceResponse>
    {
        public GetoldRecurranceRequest(MyVrmService service)
            : base(service)
        {
        }

       
      internal ConferenceId ConferenceId { get; set; }

        #region Overrides of ServiceRequestBase

        internal override string GetXmlElementName()
        {
            return "login";
        }

        internal override string GetCommandName()
        {
            return "GetOldConference";
        }

        internal override string GetResponseXmlElementName()
        {
            return "conference";
        }

        internal override GetoldRecurranceResponse ParseResponse(MyVrmServiceXmlReader reader)
        {

            var response = new GetoldRecurranceResponse();
            response.LoadFromXml(reader, GetResponseXmlElementName());
            return response;
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            UserId.WriteToXml(writer, "userID");
            ConferenceId.WriteToXml(writer, "selectID");
			writer.WriteElementValue(XmlNamespace.NotSpecified, "utcEnabled", Service.IsUtcEnabled);
        }

        #endregion
    }

    public class GetoldRecurranceResponse : ServiceResponse
    {
        internal Conference Conference { get; set; }
        public string endtype { get; set; }
        public string startDate { get; set; }
        public string occurrence { get; set; }
        public string recurType { get; set; }
        public string dailyType { get; set; }
        public string dayGap { get; set; }
        public string weekGap { get; set; }
        public string weekDay { get; set; }
        public string monthlyType {get;set ;}
        public string monthDayNo {get;set ;}
        public string monthGap {get;set;}
        public string monthWeekDayNo {get;set;}
        public string  monthWeekDay {get;set;}
        
        public string yearlyType {get;set;}
        public string yearMonth {get;set;}
        public string yearMonthDay {get;set;}
        public string yearMonthWeekDayNo {get;set;}
        public string yearMonthWeekDay {get;set;} 


        internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
        {
             do
            {
                    reader.Read();
                    if (reader.LocalName != "" && reader.LocalName != null)
                    {
                                switch (reader.LocalName)
                                {
                                    case "confInfo":
                                        LoadFromXml(reader, "confInfo");
                                        //reader.Read();
                                        break;
                                    //case "recurrencePattern":
                                    //    LoadFromXml(reader, "recurrencePattern");
                                    //    //reader.Read();
                                    //    break;
                                    case "recurType":
                                        recurType = reader.ReadElementValue();
                                        //reader.Read();
                                        break;
                                    case "monthGap":
                                        monthGap = reader.ReadElementValue();
                                        break;
                                    case "monthWeekDayNo":
                                        monthWeekDayNo = reader.ReadElementValue();
                                        break;
                                    case "monthWeekDay":
                                        monthWeekDay = reader.ReadElementValue();
                                        break;
                                    case "yearlyType":
                                        yearlyType = reader.ReadElementValue();
                                        break;
                                    case "yearMonth":
                                        yearMonth = reader.ReadElementValue();
                                        break;
                                    case "yearMonthDay":
                                        yearMonthDay = reader.ReadElementValue();
                                        break;
                                    case "yearMonthWeekDayNo":
                                        yearMonthWeekDayNo = reader.ReadElementValue();
                                        break;
                                    case "yearMonthWeekDay":
                                        yearMonthWeekDay = reader.ReadElementValue();
                                        break;
                                    case "dailyType":
                                        dailyType = reader.ReadElementValue();
                                        break;
                                    case "dayGap":
                                        dayGap = reader.ReadElementValue();
                                        break;
                                    case "weekGap":
                                        weekGap = reader.ReadElementValue();
                                        break;
                                    case "weekDay":
                                        weekDay = reader.ReadElementValue();
                                        break;
                                    case "monthlyType":
                                        monthlyType = reader.ReadElementValue();
                                        break;
                                    case "monthDayNo":
                                        monthDayNo = reader.ReadElementValue();
                                        break;
                                    case "recurrenceRange":
                                        LoadFromXml(reader, "recurrenceRange");
                                        //reader.Read();
                                        break;
                                    case "startDate":
                                        startDate = reader.ReadElementValue();
                                        //reader.Read();
                                        break;
                                    case "endType":
                                        endtype = reader.ReadElementValue();
                                        break;
                                    case "occurrence":
                                        occurrence = reader.ReadElementValue();
                                        break;
                                    default:
                                        reader.SkipCurrentElement();
                                        break;
                                }
                        }

                  } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "conference"));
        }
           
           
        
    }



}