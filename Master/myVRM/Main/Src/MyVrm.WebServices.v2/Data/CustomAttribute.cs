﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace MyVrm.WebServices.Data
{
    public enum CustomAttributeType
    {
        TextBox = 4,
        ListBox = 5,
        DropDownList = 6,
        Url = 7,
        RadioButtonList = 8,
        MultilineText = 10
    }

    public enum CustomAttributeCreateType
    {
        User = 0,
        System = 1
    }
    /// <summary>
    /// Represents a custom attribute.
    /// </summary>
    public class CustomAttribute : ComplexProperty
    {
		public override object Clone()//CopyTo(CustomAttribute attr)
		{
			//if (attr == null)
			CustomAttribute attr = new CustomAttribute();
			attr.Description = Description;
			if (Id != null)
				attr.Id = new CustomAttributeId(Id.Id);
			attr.IncludeInEmail = IncludeInEmail;
			attr.Mandatory = Mandatory;
			attr.OptionId = OptionId;
			attr.Status = Status;
			attr.Title = Title;
			attr.Type = Type ;
			attr.Value = Value;
            attr.FieldController = FieldController;
			return attr;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(this, obj))
				return true;

			CustomAttribute attr = obj as CustomAttribute;
			if (attr == null)
				return false;

			if (attr.Description != Description)
				return false;
			if (attr.Id.Id != Id.Id)
				return false;
			if (attr.IncludeInEmail != IncludeInEmail)
				return false;
			if (attr.Mandatory != Mandatory)
				return false;
			if (attr.OptionId != OptionId)
				return false;
			if (attr.Status != Status)
				return false;
			if (attr.Title != Title)
				return false;
			if (attr.Type != Type )
				return false;
			if (attr.Value != Value)
				return false;
            if (attr.FieldController != FieldController)
                return false;

			return true;
		}

        public CustomAttribute()
        {
            OptionId = "-1";
        }

        public CustomAttributeId Id { get; set; }
        public string Title { get; private set; }
        public bool Mandatory { get; private set; }
        public string Description { get; private set; }
        public CustomAttributeType Type { get; set; }
        public int Status { get; private set; }
        public bool IncludeInEmail { get; private set; }
        public string OptionId { get; set; }
        private string _value;
        string EntitycodeoptionValue = "";
        int EntitycodeoptionID = 0;
        bool entitycodechecked = false;
        public string FieldController { get; private set; }
        public string Value
        {
            get { return _value; }
            set
            {
                if (!string.Equals(_value, value, StringComparison.InvariantCultureIgnoreCase))
                {
                    _value = value;
                    Changed();
                }
            }
        }

        internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
        {
            switch (reader.LocalName)
            {
                case "CustomAttributeID":
                    {
                        Id = new CustomAttributeId();
                        Id.LoadFromXml(reader);
                        return true;
                    }
                case "Title":
                    {
                        Title = reader.ReadElementValue();
                        return true;
                    }
                case "Mandatory":
                    {
                        Mandatory = reader.ReadElementValue<bool>();
                        return true;
                    }
                case "Description":
                    {
                        Description = reader.ReadElementValue();
                        return true;
                    }
                case "FieldController":
                    {
                        FieldController = reader.ReadElementValue();
                        return true;
                    }
                case "Type":
                    {
                        Type = reader.ReadElementValue<CustomAttributeType>();
                        return true;
                    }
                case "Status":
                    {
                        Status = reader.ReadElementValue<int>();
                        return true;
                    }
                case "IncludeInEmail":
                    {
                        IncludeInEmail = reader.ReadElementValue<bool>();
                        return true;
                    }
                case "Selected":
                    {
                        OptionId = reader.ReadElementValue();
                        if (FieldController == "Special Instructions")
                            OptionId = "-1";
                        if (FieldController == "Entity Code" && OptionId == "1" && EntitycodeoptionID > 0)
                        {
                            Value = EntitycodeoptionValue;
                            OptionId = EntitycodeoptionID.ToString();
                            entitycodechecked = true;
                            //LoadFromXml(reader, "CustomAttribute");
                        }
                        return true;
                    }
                case "SelectedValue":
                    {
                        Value = reader.ReadElementValue();
                        return true;
                    }
                case "OptionList":
                    {
                        LoadFromXml(reader, "OptionList");
                        return true;
                    }
                case "Option":
                    {
                        LoadFromXml(reader, "Option");
                        return true;
                    }
                case "DisplayCaption":
                    {
                        if (FieldController == "Entity Code" && entitycodechecked == false)
                        {
                            EntitycodeoptionValue = reader.ReadElementValue();
                        }
                        return true;
                    }
                case "OptionID":
                    {
                        if (FieldController == "Entity Code" && entitycodechecked == false)
                        {
                            EntitycodeoptionID = reader.ReadElementValue<int>();
                            //OptionId = 1;
                            //EntitycodeoptionID = reader.ReadElementValue<int>();
                        }
                        return true;
                    }
            }

            return false;
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            Id.WriteToXml(writer);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "Type", Type);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "OptionID", OptionId);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "OptionValue", Value);
        }

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
    }
}
