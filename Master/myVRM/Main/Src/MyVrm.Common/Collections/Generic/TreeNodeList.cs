﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;

namespace MyVrm.Common.Collections.Generic
{
    public class TreeNodeList<T> : List<TreeNode<T>>
    {
        private readonly TreeNode<T> _parent;

        internal TreeNodeList(TreeNode<T> parent)
        {
            _parent = parent;
        }

        public new TreeNode<T> Add(TreeNode<T> node)
        {
            base.Add(node);
            node.Parent = _parent;
            return node;
        }

        public TreeNode<T> Add(T value)
        {
            return Add(new TreeNode<T>(value));
        }

        public T Find(Predicate<T> match)
        {
            return Find(match, false);
        }

        public T Find(Predicate<T> match, bool deepSearch)
        {
            foreach (var node in this)
            {
                if (match(node.Value))
                {
                    return node.Value;
                }
                if (deepSearch)
                {
                    return node.Nodes.Find(match, true);
                }
            }
            return default(T);
        }

        public List<T> FindAll(Predicate<T> match)
        {
            return FindAll(match, false);
        }

        public List<T> FindAll(Predicate<T> match, bool deepSearch)
        {
            var values = new List<T>();
            foreach (var node in this)
            {
                if (match(node.Value))
                {
                    values.Add(node.Value);
                }
                if (deepSearch)
                {
                    values.AddRange(node.Nodes.FindAll(match, true));
                }
            }
            return values;
        }

        public List<TreeNode<T>> FindAll(Predicate<TreeNode<T>> match, bool deepSearch)
        {
            var nodes = new List<TreeNode<T>>();
            foreach (var node in this)
            {
                if (match(node))
                {
                    nodes.Add(node);
                }
                if (deepSearch)
                {
                    nodes.AddRange(node.Nodes.FindAll(match, true));
                }
            }
            return nodes;
        }
    }
}
