﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace MyVrm.Outlook
{
    public class CommandBarReadOnlyCollection : ReadOnlyCollection<OfficeCommandBar>
    {
        internal class CommandBarList : IList<OfficeCommandBar>
        {
            public CommandBarList()
            {
            }

            public CommandBarList(IList<OfficeCommandBar> list)
            {
                InnerList = list;
            }
            public IList<OfficeCommandBar> InnerList { get; set; }

            public IEnumerator<OfficeCommandBar> GetEnumerator()
            {
                return InnerList.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            public void Add(OfficeCommandBar item)
            {
                InnerList.Add(item);
            }

            public void Clear()
            {
                InnerList.Clear();
            }

            public bool Contains(OfficeCommandBar item)
            {
                return InnerList.Contains(item);
            }

            public void CopyTo(OfficeCommandBar[] array, int arrayIndex)
            {
                InnerList.CopyTo(array, arrayIndex);
            }

            public bool Remove(OfficeCommandBar item)
            {
                return InnerList.Remove(item);
            }

            public int Count
            {
                get { return InnerList.Count; }
            }

            public bool IsReadOnly
            {
                get { return InnerList.IsReadOnly; }
            }

            public int IndexOf(OfficeCommandBar item)
            {
                return InnerList.IndexOf(item);
            }

            public void Insert(int index, OfficeCommandBar item)
            {
                InnerList.Insert(index, item);
            }

            public void RemoveAt(int index)
            {
                InnerList.RemoveAt(index);
            }

            public OfficeCommandBar this[int index]
            {
                get { return InnerList[index]; }
                set { InnerList[index] = value; }
            }
        }
        public CommandBarReadOnlyCollection() : base(new CommandBarList(new OfficeCommandBar[]{}))
        {
        }
        public CommandBarReadOnlyCollection(IList<OfficeCommandBar> list)
            : base(new CommandBarList(list))
        {
        }

        public void InitInnerList(IList<OfficeCommandBar> list)
        {
            var items = (CommandBarList)Items;
            items.InnerList = list;
        }

        protected TCommandBar FindFirst<TCommandBar>() where TCommandBar : OfficeCommandBar
        {
            return Items.OfType<TCommandBar>().FirstOrDefault();
        }
    }
}
