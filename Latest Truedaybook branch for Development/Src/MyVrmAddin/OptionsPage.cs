﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using DevExpress.XtraEditors;
using Microsoft.Office.Interop.Outlook;
using MyVrm.Outlook;
using MyVrmAddin2003.Properties;
using Form=System.Windows.Forms.Form;

namespace MyVrmAddin2003
{
    [ComVisible(true)]
    [Guid("66EFE48E-672D-4b33-AB43-7BDF17F76D8E"), ProgId("myVRMAddin.OptionsPage")]
    public partial class OptionsPage : UserControl, PropertyPage
    {
        private bool _dirty;
        private PropertyPageSite _propertyPageSite;

        public OptionsPage()
        {
            InitializeComponent();
        }

        #region PropertyPage Members

        public void Apply()
        {
            try
            {
                var cancelEventArgs = new CancelEventArgs(false);
                optionsControl.Apply(cancelEventArgs);
                _dirty = cancelEventArgs.Cancel;
            }
            catch (System.Exception e)
            {
                XtraMessageBox.Show(this, e.Message, MyVrmAddin.ProductDisplayName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw;
            }
        }

        public bool Dirty
        {
            get { return _dirty; }
        }

        public void GetPageInfo(ref string helpFile, ref int helpContext)
        {
        }

        #endregion

        [DispId(-518)]
        public string TabCaption
        {
            get
            {
                return Strings.SchedulerOptionsPageTitle;
            }
        }

        protected override void OnHandleDestroyed(EventArgs e)
        {
            // Since Outlook doesn't call Dispose method of IDispose this control never release resources.
            // So we catch OnHandleDestroyed event and explicitly call Dispose method of Component class.
            Dispose();
            base.OnHandleDestroyed(e);
        }

        void optionsControl_OptionsChanged(object sender, EventArgs args)
        {
            _dirty = true;
            if (_propertyPageSite != null)
            {
                _propertyPageSite.OnStatusChange();
            }
        }

        private void OptionsPage_Load(object sender, EventArgs e)
        {
            _propertyPageSite = GetPropertyPageSite();
            optionsControl.OptionsChanged += optionsControl_OptionsChanged;
        }

        PropertyPageSite GetPropertyPageSite()
        {
            string windowsFormsStrongName = typeof(Form).Assembly.FullName;
            Type oleObjectType =
                Type.GetType(Assembly.CreateQualifiedName(windowsFormsStrongName,
                                                                            "System.Windows.Forms.UnsafeNativeMethods"))
                    .GetNestedType("IOleObject");
            MethodInfo getClientSetMethodInfo = oleObjectType.GetMethod("GetClientSite");
            return getClientSetMethodInfo.Invoke(this, null) as PropertyPageSite;
        }
    }
}