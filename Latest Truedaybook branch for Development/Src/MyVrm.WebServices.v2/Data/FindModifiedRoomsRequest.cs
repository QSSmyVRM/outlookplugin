﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace MyVrm.WebServices.Data
{
    internal class FindModifiedRoomsRequest : ServiceRequestBase<FindModifiedRoomsResponse>
    {
        internal FindModifiedRoomsRequest(MyVrmService service) : base(service)
        {
        }

        internal UserId LastModifiedBy { get; set; }
        internal DateTime LastModifiedDate { get; set; }

        #region Overrides of ServiceRequestBase

        internal override string GetXmlElementName()
        {
            return "SearchRooms";
        }

        internal override string GetCommandName()
        {
            return Constants.SearchRoomsCommandName;
        }

        internal override string GetResponseXmlElementName()
        {
            return "SearchRooms";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            if (LastModifiedBy != null)
            {
                LastModifiedBy.WriteToXml(writer, "LastModifiedBy");    
            }
            writer.WriteElementValue(XmlNamespace.NotSpecified, "LastModifiedDate", LastModifiedDate.ToString(Constants.DateTimeSecFormat));
        }

        #endregion
    }
}
