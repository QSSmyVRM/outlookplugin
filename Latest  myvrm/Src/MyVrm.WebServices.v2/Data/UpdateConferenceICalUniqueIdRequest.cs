﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    internal class UpdateConferenceICalUniqueIdRequest : ServiceRequestBase<UpdateConferenceICalUniqueIdResponse>
    {
        public UpdateConferenceICalUniqueIdRequest(MyVrmService service) : base(service)
        {
        }

        public ConferenceId ConferenceId { get; set; }
        public string ICalId { get; set; }

        #region Overrides of ServiceRequestBase<UpdateConferenceICalUniqueIdResponse>

        internal override string GetXmlElementName()
        {
            return "UpdateIcalID";
        }

        internal override string GetCommandName()
        {
            return "UpdateICalID";
        }

        internal override string GetResponseXmlElementName()
        {
            return "success";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            OrganizationId.WriteToXml(writer);
            UserId.WriteToXml(writer, "UserID");
            ConferenceId.WriteToXml(writer, "ConfID");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "IcalID", ICalId);
        }

        #endregion
    }
}
