﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop.Excel.Extensions;
using MyVrm.Common;
using MyVrm.Common.Collections;
using MyVrm.WebServices.Data;

namespace MyVrm.DataImport.Excel
{
    public class ValidationListManager
    {
        private readonly Workbook _workbook;
        private readonly MyVrmService _service;
        private readonly List<HiddenWorksheet> _hiddenWorksheets;
        private const string RangeNamePrefix = "myVRM_ValidationRange_";
        private readonly Dictionary<string, string> _listToNamedRange;

        public ValidationListManager(Workbook workbook, MyVrmService service)
        {
            _workbook = workbook;
            _service = service;
            _hiddenWorksheets = new List<HiddenWorksheet>();
            _listToNamedRange = new Dictionary<string, string>();

            // Find hidden worksheets
            for (int i = 1; i < workbook.Worksheets.Count; i++ )
            {
                var worksheet = workbook.Worksheets[i] as Worksheet;
                if (worksheet != null && HiddenWorksheet.IsHiddenWorksheet(worksheet))
                {
                    _hiddenWorksheets.Add(new HiddenWorksheet(worksheet));
                }
            }
            // Find named ranges
            List<Name> hiddenNames = new List<Name>();
            Names names = workbook.Names;
            foreach (Name name in names)
            {
                var str = name.Name;
                if (str.StartsWith(RangeNamePrefix, StringComparison.OrdinalIgnoreCase))
                {
                    Range refersToRange = name.RefersToRange;
                    if (refersToRange != null)
                    {
                        string specificName = GetSpecificName(refersToRange);
                        if (!string.IsNullOrEmpty(specificName))
                        {
                            _listToNamedRange[specificName] = str;
                        }
                        else
                        {
                            hiddenNames.Add(name);
                        }
                    }
                    else
                    {
                        hiddenNames.Add(name);
                    }
                }
            }
        }

        public string GetYesNoRange()
        {
            var namedRange = Get("YesNo");
            if (string.IsNullOrEmpty(namedRange))
            {
                List<string> yesNo = new List<string>();
                yesNo.Add("Yes");
                yesNo.Add("No");
                namedRange = Add("YesNo", yesNo, "General");
            }
            return namedRange;
        }

        public string GetBridgeTypesRange()
        {
            var namedRange = Get("BridgeTypeNames");
            if (string.IsNullOrEmpty(namedRange))
            {
                List<string> bridgeTypeNames = new List<string>();
                var bridgeTypes = Service.GetBridgeTypes();
                foreach (var bridgeType in bridgeTypes)
                {
                    bridgeTypeNames.Add(bridgeType.Name);
                }
                namedRange = Add("BridgeTypeNames", bridgeTypeNames, "General");
            }
            return namedRange;
        }

        public string GetTimezoneRange()
        {
            var namedRange = Get("TimeZoneNames");
            if (string.IsNullOrEmpty(namedRange))
            {
                List<string> timeZoneNames = new List<string>();
                var timeZones = TimeZoneInfo.GetSystemTimeZones();
                foreach (var timeZoneInfo in timeZones)
                {
                    timeZoneNames.Add(timeZoneInfo.Id);
                }
                timeZoneNames.Sort();
                namedRange = Add("TimeZoneNames", timeZoneNames, "General");
            }
            return namedRange;
        }

        public string GetVideoEquipmentRange()
        {
            var namedRange = Get("VideoEquipments");
            if (string.IsNullOrEmpty(namedRange))
            {
                List<string> equipmentNames = new List<string>();
                var videoEquipments = Service.GetVideoEquipment();
                foreach (var videoEquipment in videoEquipments)
                {
                    equipmentNames.Add(videoEquipment.Name);
                }
                namedRange = Add("VideoEquipments", equipmentNames, "General");
            }
            return namedRange;
        }

        public string GetLineRateRange()
        {
            var namedRange = Get("LineRates");
            if (string.IsNullOrEmpty(namedRange))
            {
                List<string> lineRateNames = new List<string>();
                var lineRates = Service.GetLineRate();
                foreach (var lineRate in lineRates)
                {
                    lineRateNames.Add(lineRate.Name);
                }
                namedRange = Add("LineRates", lineRateNames, "General");
            }
            return namedRange;
        }

        public string GetConnectionTypeRange()
        {
            var namedRange = Get("ConnectionTypes");
            if (string.IsNullOrEmpty(namedRange))
            {
                List<string> names = new List<string>();
                EnumListSource  enumListSource = new EnumListSource(typeof(ConnectionType));
                foreach (LocalizedEnum localizedEnum in enumListSource.GetList())
                {
                    names.Add(localizedEnum.Text);
                }
                namedRange = Add("ConnectionTypes", names, "General");
            }
            return namedRange;
        }

        public string GetAddressTypeRange()
        {
            var namedRange = Get("AddressTypes");
            if (string.IsNullOrEmpty(namedRange))
            {
                List<string> names = new List<string>();
                EnumListSource enumListSource = new EnumListSource(typeof(AddressType));
                foreach (LocalizedEnum localizedEnum in enumListSource.GetList())
                {
                    names.Add(localizedEnum.Text);
                }
                namedRange = Add("AddressTypes", names, "General");
            }
            return namedRange;
        }

        public string GetBridgeNamesRange()
        {
            var namedRange = Get("BridgeNames");
            if (string.IsNullOrEmpty(namedRange))
            {
                List<string> bridgeNames = new List<string>();
                var bridges = Service.GetBridges();
                foreach (var bridge in bridges)
                {
                    bridgeNames.Add(bridge.Name);
                }
                namedRange = Add("BridgeNames", bridgeNames, "General");
            }
            return namedRange;
        }

        public string GetMediaTypeRange()
        {
            var namedRange = Get("MediaTypes");
            if (string.IsNullOrEmpty(namedRange))
            {
                List<string> names = new List<string>();
                EnumListSource enumListSource = new EnumListSource(typeof(MediaType));
                foreach (LocalizedEnum localizedEnum in enumListSource.GetList())
                {
                    names.Add(localizedEnum.Text);
                }
                namedRange = Add("MediaTypes", names, "General");
            }
            return namedRange;
        }

        public string GetUserRoleNamesRange()
        {
            var namedRange = Get("UserRoles");
            if (string.IsNullOrEmpty(namedRange))
            {
                List<string> names = new List<string>();
                var userRoles = Service.GetUserRoles();
                foreach (var bridge in userRoles)
                {
                    names.Add(bridge.Name);
                }
                namedRange = Add("UserRoles", names, "General");
            }
            return namedRange;
        }

        public string Add(string specificName, ICollection values, string numberFormat)
        {
            if (specificName == null) 
                throw new ArgumentNullException("specificName");
            if (values == null) 
                throw new ArgumentNullException("values");
            return Add(values, specificName, numberFormat);
        }


        public string Get(string specificName)
        {
            if (specificName == null) 
                throw new ArgumentNullException("specificName");
            if (_listToNamedRange.ContainsKey(specificName))
            {
                return _listToNamedRange[specificName];
            }
            return null;
        }

        private string Add(ICollection values, string specificName, string numberFormat)
        {
            if (values == null || values.Count == 0)
            {
                //throw new ArgumentException("There are not enough entries in the list. The list must contain at least one entry.");
                return null;
            }
            var worksheet = FindFreeWorksheet();
            Range range = worksheet.Add(values, numberFormat);
            if (range == null)
            {
                return null;
            }
            var rangeName = RangeNamePrefix + specificName;
            _workbook.Names.Add(rangeName, range, false, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                               Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            if (!string.IsNullOrEmpty(specificName))
            {
                GetSpecificNameRange(worksheet.Worksheet, range).set_Value(Type.Missing, specificName);
                _listToNamedRange[specificName] = rangeName;
                return rangeName;
            }
            return rangeName;
        }

        private HiddenWorksheet FindFreeWorksheet()
        {
            HiddenWorksheet freeWorksheet = null;
            foreach (var worksheet in _hiddenWorksheets)
            {
                if (worksheet.HasFreeColumns)
                {
                    freeWorksheet = worksheet;
                    break;
                }
            }    
            if (freeWorksheet == null)
            {
                freeWorksheet = new HiddenWorksheet(_workbook);
                _hiddenWorksheets.Add(freeWorksheet);
            }
            return freeWorksheet;
        }

        private string GetSpecificName(Range range)
        {
            string specificName = string.Empty;
            Worksheet ws = range.Worksheet;
            Range specificNameRange = GetSpecificNameRange(ws, range);
            if (specificNameRange != null)
            {
                object obj2 = specificNameRange.get_Value(Type.Missing);
                specificName = (obj2 != null) ? obj2.ToString() : null;
            }
            return specificName;
        }


        private Range GetSpecificNameRange(Worksheet ws, Range range)
        {
            if (range != null)
            {
                return ws.Cells.Item(range.Rows.Count + 1, range.Column);
            }
            return null;
        }

        private MyVrmService Service
        {
            get { return _service; }
        }
    }
}
