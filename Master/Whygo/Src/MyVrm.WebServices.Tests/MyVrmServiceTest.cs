﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using MyVrm.Common.Diagnostics;
using MyVrm.WebServices.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyVrm.WebServices.Vrmws;

namespace MyVrm.WebServices.Tests
{
    /// <summary>
    ///This is a test class for MyVrmServiceTest and is intended
    ///to contain all MyVrmServiceTest Unit Tests
    ///</summary>
    [TestClass()]
    public class MyVrmServiceTest
    {
		const string constURL = "http://myvrm01/en/myvrmws.asmx";
		const string constAcount = "mkleinerman@logic-lab.com";
		const string constPassword = "Maria2011";
		const string constUserID = "17";
		const string constTemplateName = "4 UNIT TEST";
		const string constFromTemplateConfName = "4 UNIT TEST From Template Conference";
		const string constFromTemplateConfDescr = "N/A";
		internal DateTime constStartDate = new DateTime( 2011, 09, 13);

		static VRMNewWebService nakedService = new VRMNewWebService();
		static MyVrmService wrappedService = MyVrmService.Service;

    	private static IDictionary<object, object> Params = new Dictionary<object, object>();

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

		static void Init()
		{
			Params.Clear();
			Params.Add("URL", constURL);
			Params.Add("account", constAcount);
			Params.Add("password", constPassword);
			Params.Add("userID", constUserID);

			NetworkCredential currCred = new NetworkCredential(Params["account"].ToString(), Params["password"].ToString());
			nakedService.Credentials = currCred;
			nakedService.Url = Params["URL"].ToString();

			wrappedService.Credential = currCred;
			wrappedService.AuthenticationMode = AuthenticationMode.Custom;
			wrappedService.Url = Params["URL"].ToString();

			wrappedService.ClientVersion = new ClientVersion(2, 0, 58);
			wrappedService.ClientType = "02";
			//wrappedService.UnitTestExternalSet( new UserId(constUserID));
			MyVrmService.TraceSource = new MyVrmTraceSource("myVRM");
		}

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
		[ClassInitialize()]
		public static void MyClassInitialize(TestContext testContext)
		{
			Init();
		}
        //
        //Use ClassCleanup to run code after all tests in a class have run
		[ClassCleanup()]
		public static void MyClassCleanup()
		{
			CleanUpTemplate();
		}
        //
        //Use TestInitialize to run code before running each test
		//[TestInitialize()]
		//public void MyTestInitialize()
		//{
			
		//}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for FullUrl
        ///</summary>
        [TestMethod()]
        public void FullUrlTest()
        {
            var target = new MyVrmService_Accessor();
            
            var fullUrl = "http://myvrm/en/myvrmws.asmx";
            target.Url = "http://myvrm/en/myvrmws.asmx";
            Assert.AreEqual(fullUrl, target.FullUrl);
            
            var customUrl1 = "http://myvrm/en/myvrmws.asmx?WSDL";
            target.Url = customUrl1;
            Assert.AreEqual(customUrl1, target.FullUrl);
            
            var customUrl2 = "http://myvrm/en/alternate.asmx";
            target.Url = customUrl2;
            Assert.AreEqual(customUrl2, target.FullUrl);

            target.Url = "http://myvrm/en";
            Assert.AreEqual(fullUrl, target.FullUrl);
            
            target.Url = "http://myvrm";
            Assert.AreEqual(fullUrl, target.FullUrl);
        }

		string CreateTestTemplate()
		{
			StringBuilder sbBuilder = new StringBuilder();
			sbBuilder.Append("<myVRM>");
			sbBuilder.Append(string.Format("<login>{0}</login>", Params["account"]));
			sbBuilder.Append(string.Format("<password>{0}</password>", Params["password"]));
			sbBuilder.Append(string.Format("<version>{0}</version>", wrappedService.ClientVersion));
			sbBuilder.Append(string.Format("<client>{0}</client>", wrappedService.ClientType));
			sbBuilder.Append("<commandname>SetTemplate</commandname>");
			sbBuilder.Append("<command>");
			sbBuilder.Append("<template>");
			sbBuilder.Append("<userInfo>");
			sbBuilder.Append(string.Format("<userID>{0}</userID>", Params["userID"]));
			sbBuilder.Append("</userInfo>");
			sbBuilder.Append("<templateInfo>");
			sbBuilder.Append("<ID>new</ID>");
			sbBuilder.Append(string.Format("<name>{0}</name>", constTemplateName));
			sbBuilder.Append("<public>1</public>");
			sbBuilder.Append("<owner>");
			sbBuilder.Append("<firstName></firstName>");
			sbBuilder.Append("<lastName></lastName>");
			sbBuilder.Append("</owner>");
			sbBuilder.Append("<setDefault>true</setDefault>");
			sbBuilder.Append("<description>4 UNIT TEST description</description>");
			sbBuilder.Append("</templateInfo>");

			sbBuilder.Append("<confInfo>");
			sbBuilder.Append("<confID>52</confID>");
			sbBuilder.Append(string.Format("<confName>{0}</confName>", constFromTemplateConfName));
			sbBuilder.Append("<confType>2</confType>"); //new!
			sbBuilder.Append("<confHost>17</confHost>");
			sbBuilder.Append("<confOrigin>1</confOrigin>");
			sbBuilder.Append("<createBy>2</createBy>");
			sbBuilder.Append("<confPassword></confPassword>");
			sbBuilder.Append("<immediate>0</immediate>");
			sbBuilder.Append("<publicConf>0</publicConf>");
			sbBuilder.Append("<dynamicInvite>1</dynamicInvite>");
			sbBuilder.Append("<recurring>0</recurring>");
			sbBuilder.Append("<recurringText></recurringText>");
			sbBuilder.Append("<startDate>4/13/2011</startDate>");
			sbBuilder.Append("<startHour>05</startHour>");
			sbBuilder.Append("<startMin>00</startMin>");
			sbBuilder.Append("<startSet>PM</startSet>");
			sbBuilder.Append("<timeZone>53</timeZone>");
			sbBuilder.Append("<durationMin>30</durationMin>");
			sbBuilder.Append(string.Format("<description>{0}</description>", constFromTemplateConfDescr));
			sbBuilder.Append("<partys>");
			sbBuilder.Append("<party>");
			sbBuilder.Append("<partyID>17</partyID>");
			sbBuilder.Append("<partyFirstName>Maria</partyFirstName>");
			sbBuilder.Append("<partyLastName>Kleinerman</partyLastName>");
			sbBuilder.Append("<partyEmail>mkleinerman@logic-lab.com</partyEmail>");
			sbBuilder.Append("<partyInvite>2</partyInvite>");
			sbBuilder.Append("<partyNotify>0</partyNotify>");
			sbBuilder.Append("<partyAudVid>0</partyAudVid>");
			sbBuilder.Append("</party>");

			sbBuilder.Append("<party>");
			sbBuilder.Append("<partyID>16</partyID>");
			sbBuilder.Append("<partyFirstName>Anton</partyFirstName>");
			sbBuilder.Append("<partyLastName>Chochia</partyLastName>");
			sbBuilder.Append("<partyEmail>achochia@logic-lab.com</partyEmail>");
			sbBuilder.Append("<partyInvite>2</partyInvite>");
			sbBuilder.Append("<partyNotify>1</partyNotify>");
			sbBuilder.Append("<partyAudVid>0</partyAudVid>");
			sbBuilder.Append("</party>");

			sbBuilder.Append("</partys>");
			sbBuilder.Append("<locationList>");
			sbBuilder.Append("<selected>");
			sbBuilder.Append("<level1ID>16</level1ID>");
			sbBuilder.Append("<level1ID>14</level1ID>");
			sbBuilder.Append("</selected>");
			sbBuilder.Append("</locationList>");
			sbBuilder.Append("<fileUpload></fileUpload>");
			sbBuilder.Append("<CustomAttributesList>");
			sbBuilder.Append("<CustomAttribute>");
			sbBuilder.Append("<CustomAttributeID>1</CustomAttributeID>");
			sbBuilder.Append("<Type>4</Type>");
			sbBuilder.Append("<OptionID>0</OptionID>");
			sbBuilder.Append("<OptionValue></OptionValue>");
			sbBuilder.Append("</CustomAttribute>");
			sbBuilder.Append("<CustomAttribute>");
			sbBuilder.Append("<CustomAttributeID>2</CustomAttributeID>");
			sbBuilder.Append("<Type>4</Type>");
			sbBuilder.Append("<OptionID>0</OptionID>");
			sbBuilder.Append("<OptionValue></OptionValue>");
			sbBuilder.Append("</CustomAttribute>");
			sbBuilder.Append("<CustomAttribute>");
			sbBuilder.Append("<CustomAttributeID>3</CustomAttributeID>");
			sbBuilder.Append("<Type>10</Type>");
			sbBuilder.Append("<OptionID>0</OptionID>");
			sbBuilder.Append("<OptionValue></OptionValue>");
			sbBuilder.Append("</CustomAttribute>");
			sbBuilder.Append("</CustomAttributesList>");
			sbBuilder.Append("<advAVParam>");
			sbBuilder.Append("<maxAudioPart>0</maxAudioPart>");
			sbBuilder.Append("<maxVideoPart>0</maxVideoPart>");
			sbBuilder.Append("<restrictProtocol>1</restrictProtocol>");
			sbBuilder.Append("<restrictAV>3</restrictAV>");
			sbBuilder.Append("<videoLayout>1</videoLayout>");
			sbBuilder.Append("<maxLineRateID>384</maxLineRateID>");
			sbBuilder.Append("<audioCodec>0</audioCodec>");
			sbBuilder.Append("<videoCodec>0</videoCodec>");
			sbBuilder.Append("<dualStream>0</dualStream>");
			sbBuilder.Append("<confOnPort>0</confOnPort>");
			sbBuilder.Append("<encryption>0</encryption>");
			sbBuilder.Append("<lectureMode>0</lectureMode>");
			sbBuilder.Append("<VideoMode>2</VideoMode>");
			sbBuilder.Append("<SingleDialin>0</SingleDialin>");
			sbBuilder.Append("</advAVParam>");
			sbBuilder.Append("<ModifyType></ModifyType>");
			sbBuilder.Append("</confInfo>");
			sbBuilder.Append("</template>");
			sbBuilder.Append("</command>");
			sbBuilder.Append("</myVRM>");
			string inXml = sbBuilder.ToString();
			return nakedService.InvokeWebservice(inXml);
		}

    	[TestMethod()]
		public void TemplateFunctionalTest()
		{
			int iID = 0;

			GetTemplateListResponse getTemplateListResponse = wrappedService.GetTemplateList(GetTemplateListResponse.SortByType.ByName);
			Assert.IsNotNull(getTemplateListResponse); //Check get response

    		int foundNdx = 0;
			while (getTemplateListResponse != null && getTemplateListResponse.templateList != null && foundNdx != -1) //Until defined template exists
			{
				foundNdx = getTemplateListResponse.templateList.FindIndex(template => template.Name.Trim() == constTemplateName);
				if (foundNdx != -1)
				{
					int.TryParse(getTemplateListResponse.templateList[foundNdx].ID, out iID);
					ConferenceDeleteTemplateResponse deletedResponse = wrappedService.ConferenceDeleteTemplate(iID);
					Assert.IsTrue(deletedResponse.Success); //Check delete template
					
					getTemplateListResponse = wrappedService.GetTemplateList(GetTemplateListResponse.SortByType.ByName);
					Assert.IsNotNull(getTemplateListResponse); //Check get response
				}
			}

			if( foundNdx == -1) //template doesn't exist
			{
				string outResponse = CreateTestTemplate(); //Create template "manually"
				ConferenceSetTemplateResponse setTemplateResponse = new ConferenceSetTemplateResponse(); //Simulate "response read" 

				Stream stream = new MemoryStream();
				for (int i = 0; i < outResponse.Trim().Length; i++)
					stream.WriteByte((byte)outResponse[i]);
				stream.Position = 0;

				MyVrmServiceXmlReader reader = new MyVrmServiceXmlReader(stream, wrappedService);
				setTemplateResponse.ReadElementsFromXml(reader);
				Assert.IsNotNull(setTemplateResponse.TemplateInfo);
				Assert.AreEqual(setTemplateResponse.TemplateInfo.Name, constTemplateName); //Check if read made correctly
				Assert.AreEqual(setTemplateResponse.Conference.Description, constFromTemplateConfDescr); //Check template conference descr
				Assert.AreEqual(setTemplateResponse.TemplateInfo.Description, "4 UNIT TEST description"); //Check
				Assert.AreEqual(setTemplateResponse.TemplateInfo.IsPublic, 1); //Check
				Assert.AreEqual(setTemplateResponse.TemplateInfo.Name, constTemplateName); //Check
				Assert.AreEqual(setTemplateResponse.Conference.Description, constFromTemplateConfDescr); //Check
				Assert.AreEqual(setTemplateResponse.Conference.Duration, new TimeSpan(0,30,0)); //Check
				Assert.AreEqual(setTemplateResponse.Conference.Name, constFromTemplateConfName); //Check
				Assert.AreEqual(setTemplateResponse.Conference.StartDate, constStartDate); //Check
				
				int.TryParse(setTemplateResponse.TemplateInfo.ID, out iID);
				GetTemplateResponse getTemplateResponse = wrappedService.GetTemplate(iID);

				Assert.AreEqual(getTemplateResponse.Conference.Name, constFromTemplateConfName); //Check get template by ID
				Assert.AreEqual(getTemplateResponse.Conference.Description, constFromTemplateConfDescr);
				Assert.AreEqual(getTemplateResponse.Conference.Duration, new TimeSpan(0, 30, 0));
				Assert.AreEqual(getTemplateResponse.Conference.IsPublic, false);
				Assert.AreEqual(getTemplateResponse.Conference.Name, constFromTemplateConfName);
				Assert.IsTrue(getTemplateResponse.Conference.Participants.Items.Exists(var => var.UserId.Id == "17"));
				Assert.IsTrue(getTemplateResponse.Conference.Participants.Items.Exists(var => var.UserId.Id == "16"));

				Assert.IsTrue(getTemplateResponse.Conference.Rooms.Items.Exists(var => var.Id == "16"));
				Assert.IsTrue(getTemplateResponse.Conference.Rooms.Items.Exists(var => var.Id == "14"));
				Assert.AreEqual(getTemplateResponse.Conference.StartDate, constStartDate);

				//Clean up
				ConferenceDeleteTemplateResponse deletedResponse = wrappedService.ConferenceDeleteTemplate(iID);
				Assert.IsTrue(deletedResponse.Success); //Check delete template
			}
		}

		[TestMethod()]
		public void CheckOutXMLOfCreateTemplate()
		{
			CleanUpTemplate();
			string outResponse = CreateTestTemplate();
			Assert.IsTrue(outResponse.Trim().Contains("<template"));
			Assert.IsTrue(outResponse.Trim().Contains("<userInfo"));
			Assert.IsTrue(outResponse.Trim().Contains("<userName"));
			Assert.IsTrue(outResponse.Trim().Contains("<templateInfo"));
			Assert.IsTrue(outResponse.Trim().Contains("<ID"));
			Assert.IsTrue(outResponse.Trim().Contains("<name"));
			Assert.IsTrue(outResponse.Trim().Contains("<public"));
			Assert.IsTrue(outResponse.Trim().Contains("<owner"));
			Assert.IsTrue(outResponse.Trim().Contains("<firstName"));
			Assert.IsTrue(outResponse.Trim().Contains("<lastName"));
			Assert.IsTrue(outResponse.Trim().Contains("<description"));
			Assert.IsTrue(outResponse.Trim().Contains("<confInfo"));
			Assert.IsTrue(outResponse.Trim().Contains("<confName"));
			Assert.IsTrue(outResponse.Trim().Contains("<confPassword"));
			Assert.IsTrue(outResponse.Trim().Contains("<durationMin"));
			Assert.IsTrue(outResponse.Trim().Contains("<description"));
			Assert.IsTrue(outResponse.Trim().Contains("<mainLocation"));
			Assert.IsTrue(outResponse.Trim().Contains("<location"));
			Assert.IsTrue(outResponse.Trim().Contains("<locationName"));
			Assert.IsTrue(outResponse.Trim().Contains("<publicConf"));
			Assert.IsTrue(outResponse.Trim().Contains("<confType"));
			Assert.IsTrue(outResponse.Trim().Contains("<numberParticipants"));
			Assert.IsTrue(outResponse.Trim().Contains("<invited"));
			Assert.IsTrue(outResponse.Trim().Contains("<invitee"));
			Assert.IsTrue(outResponse.Trim().Contains("<cc"));
			Assert.IsTrue(outResponse.Trim().Contains("<party"));
			Assert.IsTrue(outResponse.Trim().Contains("<partyFirstName"));
			Assert.IsTrue(outResponse.Trim().Contains("<partyLastName"));
			Assert.IsTrue(outResponse.Trim().Contains("<partyEmail"));
		}

		[TestMethod()]
		public void CheckOutXMLOfGetTemplateList()
		{
			//Ensure at least 1 template exists
			CleanUpTemplate();
			CreateTestTemplate();

			//Check itself
			StringBuilder sbBuilder = new StringBuilder();
			sbBuilder.Append("<myVRM>");
			sbBuilder.Append(string.Format("<login>{0}</login>", Params["account"]));
			sbBuilder.Append(string.Format("<password>{0}</password>", Params["password"]));
			sbBuilder.Append(string.Format("<version>{0}</version>", wrappedService.ClientVersion));
			sbBuilder.Append(string.Format("<client>{0}</client>", wrappedService.ClientType));
			sbBuilder.Append("<commandname>GetTemplateList</commandname>");
			sbBuilder.Append("<command>");
			sbBuilder.Append("<login>");
		    sbBuilder.Append(string.Format("<userID>{0}</userID>", Params["userID"]));
			sbBuilder.Append("<sortBy></sortBy>");
			sbBuilder.Append("</login>");
			sbBuilder.Append("</command>");
			sbBuilder.Append("</myVRM>");
			string inXml = sbBuilder.ToString();
			string outResponse = nakedService.InvokeWebservice(inXml);
			Assert.IsTrue(outResponse.Trim().Contains("<templates"));
			Assert.IsTrue(outResponse.Trim().Contains("<sortBy"));
			Assert.IsTrue(outResponse.Trim().Contains("<template"));
			Assert.IsTrue(outResponse.Trim().Contains("<ID"));
			Assert.IsTrue(outResponse.Trim().Contains("<name"));
			Assert.IsTrue(outResponse.Trim().Contains("<public"));
			Assert.IsTrue(outResponse.Trim().Contains("<owner"));
			Assert.IsTrue(outResponse.Trim().Contains("<ownerID"));
			Assert.IsTrue(outResponse.Trim().Contains("<firstName"));
			Assert.IsTrue(outResponse.Trim().Contains("<lastName"));
			Assert.IsTrue(outResponse.Trim().Contains("<description"));
		}

		[TestMethod()]
		public void CheckOutXMLOfGetOldTemplate()
		{
			//Make a template
			CleanUpTemplate();
			CreateTestTemplate();

			GetTemplateListResponse getTemplateListResponse = wrappedService.GetTemplateList(GetTemplateListResponse.SortByType.ByName);

    		int foundNdx = 0;
			int iID = 0;
			if (getTemplateListResponse != null && getTemplateListResponse.templateList != null && foundNdx != -1)
			{
				foundNdx = getTemplateListResponse.templateList.FindIndex(template => template.Name.Trim() == constTemplateName);
				if (foundNdx != -1)
				{
					int.TryParse(getTemplateListResponse.templateList[foundNdx].ID, out iID);
				}
			}
			Assert.IsTrue(foundNdx != -1);

			//Check itself
			StringBuilder sbBuilder = new StringBuilder();
			sbBuilder.Append("<myVRM>");
			sbBuilder.Append(string.Format("<login>{0}</login>", Params["account"]));
			sbBuilder.Append(string.Format("<password>{0}</password>", Params["password"]));
			sbBuilder.Append(string.Format("<version>{0}</version>", wrappedService.ClientVersion));
			sbBuilder.Append(string.Format("<client>{0}</client>", wrappedService.ClientType));
			sbBuilder.Append("<commandname>GetOldTemplate</commandname>");
			sbBuilder.Append("<command>");
			sbBuilder.Append("<login>");
			sbBuilder.Append(string.Format("<userID>{0}</userID>", Params["userID"]));
			sbBuilder.Append(string.Format("<templateID>{0}</templateID>", iID));
			sbBuilder.Append("</login>");
			sbBuilder.Append("</command>");
			sbBuilder.Append("</myVRM>");
			string inXml = sbBuilder.ToString();
			string outResponse = nakedService.InvokeWebservice(inXml);
			Assert.IsTrue(outResponse.Trim().Contains("<template"));
			Assert.IsTrue(outResponse.Trim().Contains("<userInfo"));
			Assert.IsTrue(outResponse.Trim().Contains("<userFirstName"));
			Assert.IsTrue(outResponse.Trim().Contains("<userLastName"));
			Assert.IsTrue(outResponse.Trim().Contains("<emailClient"));
			Assert.IsTrue(outResponse.Trim().Contains("<setDefault"));
			Assert.IsTrue(outResponse.Trim().Contains("<templateInfo"));
			Assert.IsTrue(outResponse.Trim().Contains("<ID"));
			Assert.IsTrue(outResponse.Trim().Contains("<name"));
			Assert.IsTrue(outResponse.Trim().Contains("<public"));
			Assert.IsTrue(outResponse.Trim().Contains("<owner"));
			Assert.IsTrue(outResponse.Trim().Contains("<firstName"));
			Assert.IsTrue(outResponse.Trim().Contains("<lastName"));
			Assert.IsTrue(outResponse.Trim().Contains("<description"));
			Assert.IsTrue(outResponse.Trim().Contains("<confInfo"));
			Assert.IsTrue(outResponse.Trim().Contains("<confName"));
			Assert.IsTrue(outResponse.Trim().Contains("<confPassword"));
			Assert.IsTrue(outResponse.Trim().Contains("<durationMin"));
			Assert.IsTrue(outResponse.Trim().Contains("<description"));
			Assert.IsTrue(outResponse.Trim().Contains("<confType"));
			Assert.IsTrue(outResponse.Trim().Contains("<advAVParam"));
			Assert.IsTrue(outResponse.Trim().Contains("<maxAudioPart"));
			Assert.IsTrue(outResponse.Trim().Contains("<maxVideoPart"));
			Assert.IsTrue(outResponse.Trim().Contains("<restrictProtocol"));
			Assert.IsTrue(outResponse.Trim().Contains("<restrictAV"));
			Assert.IsTrue(outResponse.Trim().Contains("<videoLayout"));
			Assert.IsTrue(outResponse.Trim().Contains("<maxLineRateID"));
			Assert.IsTrue(outResponse.Trim().Contains("<audioCodec"));
			Assert.IsTrue(outResponse.Trim().Contains("<videoCodec"));
			Assert.IsTrue(outResponse.Trim().Contains("<dualStream"));
			Assert.IsTrue(outResponse.Trim().Contains("<confOnPort"));
			Assert.IsTrue(outResponse.Trim().Contains("<encryption"));
			Assert.IsTrue(outResponse.Trim().Contains("<lectureMode"));
			Assert.IsTrue(outResponse.Trim().Contains("<VideoMode"));
			Assert.IsTrue(outResponse.Trim().Contains("<SingleDialin"));
			Assert.IsTrue(outResponse.Trim().Contains("<locationList"));
			Assert.IsTrue(outResponse.Trim().Contains("<selected"));
			Assert.IsTrue(outResponse.Trim().Contains("<level1ID"));
			Assert.IsTrue(outResponse.Trim().Contains("<level3List"));
			Assert.IsTrue(outResponse.Trim().Contains("<level3"));
			Assert.IsTrue(outResponse.Trim().Contains("<level3ID"));
			Assert.IsTrue(outResponse.Trim().Contains("<level3Name"));
			Assert.IsTrue(outResponse.Trim().Contains("<level2List"));
			Assert.IsTrue(outResponse.Trim().Contains("<level2"));
			Assert.IsTrue(outResponse.Trim().Contains("<level2ID"));
			Assert.IsTrue(outResponse.Trim().Contains("<level2Name"));
			Assert.IsTrue(outResponse.Trim().Contains("<level1List"));
			Assert.IsTrue(outResponse.Trim().Contains("<level1"));
			Assert.IsTrue(outResponse.Trim().Contains("<level1ID"));
			Assert.IsTrue(outResponse.Trim().Contains("<level1Name"));
			Assert.IsTrue(outResponse.Trim().Contains("<capacity"));
			Assert.IsTrue(outResponse.Trim().Contains("<projector"));
			Assert.IsTrue(outResponse.Trim().Contains("<maxNumConcurrent"));
			Assert.IsTrue(outResponse.Trim().Contains("<videoAvailable"));
			Assert.IsTrue(outResponse.Trim().Contains("<publicConf"));
			Assert.IsTrue(outResponse.Trim().Contains("<continuous"));
			Assert.IsTrue(outResponse.Trim().Contains("<videoLayout"));
			Assert.IsTrue(outResponse.Trim().Contains("<videoSessionID"));
			Assert.IsTrue(outResponse.Trim().Contains("<manualVideoLayout"));
			Assert.IsTrue(outResponse.Trim().Contains("<lectureMode"));
			Assert.IsTrue(outResponse.Trim().Contains("<lecturer"));
			Assert.IsTrue(outResponse.Trim().Contains("<dynamicInvite"));
			Assert.IsTrue(outResponse.Trim().Contains("<lineRateID"));
			Assert.IsTrue(outResponse.Trim().Contains("<audioAlgorithmID"));
			Assert.IsTrue(outResponse.Trim().Contains("<videoProtocolID"));
			Assert.IsTrue(outResponse.Trim().Contains("<videoSessionName"));
			Assert.IsTrue(outResponse.Trim().Contains("<lineRate"));
			Assert.IsTrue(outResponse.Trim().Contains("<rate"));
			Assert.IsTrue(outResponse.Trim().Contains("<lineRateID"));
			Assert.IsTrue(outResponse.Trim().Contains("<lineRateName"));
			Assert.IsTrue(outResponse.Trim().Contains("<videoEquipment"));
			Assert.IsTrue(outResponse.Trim().Contains("<equipment"));
			Assert.IsTrue(outResponse.Trim().Contains("<videoEquipmentID"));
			Assert.IsTrue(outResponse.Trim().Contains("<videoEquipmentName"));
			Assert.IsTrue(outResponse.Trim().Contains("<audioAlgorithm"));
			Assert.IsTrue(outResponse.Trim().Contains("<audio"));
			Assert.IsTrue(outResponse.Trim().Contains("<audioAlgorithmID"));
			Assert.IsTrue(outResponse.Trim().Contains("<audioAlgorithmName"));
			Assert.IsTrue(outResponse.Trim().Contains("<audioUsage"));
			Assert.IsTrue(outResponse.Trim().Contains("<audio"));
			Assert.IsTrue(outResponse.Trim().Contains("<audioUsageID"));
			Assert.IsTrue(outResponse.Trim().Contains("<audioUsageName"));
			Assert.IsTrue(outResponse.Trim().Contains("<videoProtocol"));
			Assert.IsTrue(outResponse.Trim().Contains("<video"));
			Assert.IsTrue(outResponse.Trim().Contains("<videoProtocolID"));
			Assert.IsTrue(outResponse.Trim().Contains("<videoProtocolName"));
			Assert.IsTrue(outResponse.Trim().Contains("<defaultAGroups"));
			Assert.IsTrue(outResponse.Trim().Contains("<defaultCGroups"));
			Assert.IsTrue(outResponse.Trim().Contains("<groups"));
			Assert.IsTrue(outResponse.Trim().Contains("<group"));
			Assert.IsTrue(outResponse.Trim().Contains("<groupID"));
			//Assert.IsTrue(outResponse.Trim().Contains("<groupName"));
			//Assert.IsTrue(outResponse.Trim().Contains("<user"));
			//Assert.IsTrue(outResponse.Trim().Contains("<userID"));
			//Assert.IsTrue(outResponse.Trim().Contains("<userFirstName"));
			//Assert.IsTrue(outResponse.Trim().Contains("<userLastName"));
			//Assert.IsTrue(outResponse.Trim().Contains("<userEmail"));
			Assert.IsTrue(outResponse.Trim().Contains("<partys"));
			Assert.IsTrue(outResponse.Trim().Contains("<party"));
			Assert.IsTrue(outResponse.Trim().Contains("<partyID"));
			Assert.IsTrue(outResponse.Trim().Contains("<partyFirstName"));
			Assert.IsTrue(outResponse.Trim().Contains("<partyLastName"));
			Assert.IsTrue(outResponse.Trim().Contains("<partyEmail"));
			Assert.IsTrue(outResponse.Trim().Contains("<partyInvite"));
			Assert.IsTrue(outResponse.Trim().Contains("<partyNotify"));
			Assert.IsTrue(outResponse.Trim().Contains("<partyLevel"));
			Assert.IsTrue(outResponse.Trim().Contains("<partyTitle"));
			Assert.IsTrue(outResponse.Trim().Contains("<partyAudVid"));
			Assert.IsTrue(outResponse.Trim().Contains("<partyProtocol"));
			Assert.IsTrue(outResponse.Trim().Contains("<partyAddress"));
			Assert.IsTrue(outResponse.Trim().Contains("<partyAddressType"));
			Assert.IsTrue(outResponse.Trim().Contains("<partyConnectionType"));
			Assert.IsTrue(outResponse.Trim().Contains("<partyStatus"));
			Assert.IsTrue(outResponse.Trim().Contains("<partyIsOutside"));
		}

    	static void CleanUpTemplate()
		{
			int iID = 0;
			GetTemplateListResponse getTemplateListResponse = wrappedService.GetTemplateList(GetTemplateListResponse.SortByType.ByName);
			int foundNdx = 0;
			while (getTemplateListResponse != null && getTemplateListResponse.templateList != null && foundNdx != -1) //Until defined template exists
			{
				foundNdx = getTemplateListResponse.templateList.FindIndex(template => template.Name.Trim() == constTemplateName);
				if (foundNdx != -1)
				{
					int.TryParse(getTemplateListResponse.templateList[foundNdx].ID, out iID);
					wrappedService.ConferenceDeleteTemplate(iID);
					getTemplateListResponse = wrappedService.GetTemplateList(GetTemplateListResponse.SortByType.ByName);
				}
			}
		}
		// *****************
		// CONFERENCE TESTS
		// *****************
		[TestMethod()]
		[ExpectedException(typeof(MyVrmServiceException))]
		public void CreateConf_WithoutName_Exception()
		{
			Conference conf = new Conference(wrappedService);
			wrappedService.SaveConference(conf);
		}

		[TestMethod()]
		public void CreateAndDeleteConf()
		{
			Conference conf = new Conference(wrappedService);
			conf.Name = "UNIT TEST Conf";
			conf.StartDate = new DateTime( 2012, 9, 28, 9, 0, 0, DateTimeKind.Local); 
			conf.Duration = new TimeSpan(0, 0, 30, 0);
			conf.HostId = new UserId(constUserID);
			wrappedService.SaveConference(conf);
			wrappedService.DeleteConference(conf.Id);
		}
    }
}
