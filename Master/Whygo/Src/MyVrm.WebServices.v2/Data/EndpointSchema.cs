﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.WebServices.Data
{
    public class EndpointSchema : ServiceObjectSchema
    {
        public static readonly PropertyDefinition Id;
        public static readonly PropertyDefinition Name;
        public static readonly PropertyDefinition Type;
        public static readonly PropertyDefinition UserId;
        public static readonly PropertyDefinition Profiles;
        public static readonly PropertyDefinition DefaultProfileId;

        internal static EndpointSchema Instance;

        static EndpointSchema()
        {
            Id = new ComplexPropertyDefinition<EndpointId>("ID", "EndpointID", PropertyDefinitionFlags.None, ()=> new EndpointId());
            Name = new StringPropertyDefinition("Name", "EndpointName", PropertyDefinitionFlags.CanSet);
            Type = new EndpointTypePropertyDefinition("EntityType", PropertyDefinitionFlags.CanSet);
            UserId = new ComplexPropertyDefinition<UserId>("UserID", PropertyDefinitionFlags.CanSet, ()=> new UserId());
            Profiles = new ComplexPropertyDefinition<EndpointProfileCollection>("Profiles",
                                                                                PropertyDefinitionFlags.
                                                                                    AutoInstantiateOnRead |
                                                                                PropertyDefinitionFlags.CanSet,
                                                                                () => new EndpointProfileCollection());
            DefaultProfileId = new ComplexPropertyDefinition<EndpointProfileId>("DefaultProfileID",
                                                                                PropertyDefinitionFlags.CanSet,
                                                                                () => new EndpointProfileId());

            Instance = new EndpointSchema();
        }

        internal override void RegisterProperties()
        {
            base.RegisterProperties();
            RegisterProperty(Id);
            RegisterProperty(Name);
            RegisterProperty(Type);
            RegisterProperty(UserId);
            RegisterProperty(Profiles);
            RegisterProperty(DefaultProfileId);
        }
    }
}
