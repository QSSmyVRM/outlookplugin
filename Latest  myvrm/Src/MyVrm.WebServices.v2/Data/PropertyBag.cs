﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace MyVrm.WebServices.Data
{
    /// <summary>
    /// Represents a property bag keyed on <see cref="PropertyDefinition"/> objects.
    /// </summary>
    [Serializable]

	internal class PropertyBag : ICloneable
    {
        [NonSerialized]
        private readonly Dictionary<PropertyDefinition, object> _properties = new Dictionary<PropertyDefinition, object>();
        [NonSerialized]
        private bool _isDirty;
        [NonSerialized]
        private readonly ServiceObject _owner;
        [NonSerialized]
        private readonly List<PropertyDefinition> _loadedProperties = new List<PropertyDefinition>();
        [NonSerialized]
        private readonly List<PropertyDefinition> _modifiedProperties = new List<PropertyDefinition>();
        [NonSerialized]
        private readonly Dictionary<PropertyDefinition, object> _deletedProperties = new Dictionary<PropertyDefinition, object>();
        [NonSerialized]
        private readonly List<PropertyDefinition> _addedProperties = new List<PropertyDefinition>();
        [NonSerialized]
        private bool _loading;


        internal PropertyBag(ServiceObject owner)
        {
        	if (owner == null) throw new ArgumentNullException("owner");
        	_owner = owner;
        }

    	internal bool IsDirty
        {
            get
            {
                var num = _modifiedProperties.Count + _deletedProperties.Count + _addedProperties.Count;
                //if (_modifiedProperties.Count == 1)
                //    if (_modifiedProperties[0].XmlElementName == "ParticipantList")
                //    {
                         
                //        return false;
                //    }
                //    else
                //        return true; 
                //else
                    return num > 0 || _isDirty;
            }
            set
            {
                _isDirty = value;
                _modifiedProperties.Clear();
            }
        }

        internal ServiceObject Owner
        {
            get { return _owner; }
        }

        internal object this[PropertyDefinition propertyDefinition]
        {
            get
            {
                MyVrmServiceLocalException exception;
                var propertyValue = GetPropertyValueOrException(propertyDefinition, out exception);
                if (exception != null)
                {
                    throw exception;
                }
                return propertyValue;
            }
            set
            {
                if (!_loading)
                {
                    if (Owner.IsNew && !propertyDefinition.HasFlag(PropertyDefinitionFlags.CanSet))
                    {
                        //throw new ServiceObjectPropertyException(Strings.PropertyIsReadOnly, propertyDefinition);
                    }   
                    if (!Owner.IsNew)
                    {
                        if (value == null && !propertyDefinition.HasFlag(PropertyDefinitionFlags.CanDelete))
                        {
                            //throw new ServiceObjectPropertyException(Strings.PropertyCannotBeDeleted, propertyDefinition);
                        }
                        if(!propertyDefinition.HasFlag(PropertyDefinitionFlags.CanUpdate))
                        {
                            //throw new ServiceObjectPropertyException(Strings.PropertyCannotBeUpdated, propertyDefinition);
                        }
                    }
                }
                if (value == null)
                {
                    DeleteProperty(propertyDefinition);
                }
                else
                {
                    object propVal;
                    if (_properties.TryGetValue(propertyDefinition, out propVal))
                    {
                        var complexProperty = propVal as ComplexProperty;
                        if (complexProperty != null)
                        {
                            complexProperty.OnChange -= PropertyChanged;
                        }
                    }

                	bool doesValueExist = _properties.ContainsKey(propertyDefinition);
					bool isValueChanged = !doesValueExist;
					if (doesValueExist && _properties[propertyDefinition] != value)
						isValueChanged = true;

                    if (_deletedProperties.Remove(propertyDefinition))
                    {
						if (isValueChanged)
							AddToChangeList(propertyDefinition, _modifiedProperties);
                    }
                    else if (!_properties.ContainsKey(propertyDefinition))
                    {
						if (isValueChanged)
							AddToChangeList(propertyDefinition, _addedProperties);
                    }
                    else if (!_modifiedProperties.Contains(propertyDefinition))
                    {
						if (isValueChanged)
							AddToChangeList(propertyDefinition, _modifiedProperties);
                    }
                    InitComplexProperty(value as ComplexProperty);
                	
                    _properties[propertyDefinition] = value;
					if (isValueChanged)
						Changed();
                }
            }
        }

        internal void DeleteProperty(PropertyDefinition propertyDefinition)
        {
            if (!_deletedProperties.ContainsKey(propertyDefinition))
            {
                object value;
                _properties.TryGetValue(propertyDefinition, out value);
                _properties.Remove(propertyDefinition);
                _modifiedProperties.Remove(propertyDefinition);
                _deletedProperties.Add(propertyDefinition, value);
                var complexProperty = value as ComplexProperty;
                if (complexProperty != null)
                {
                    complexProperty.OnChange -= PropertyChanged;
                }
            }
        }

        internal void PropertyChanged(ComplexProperty complexProperty)
        {
            foreach (var pair in
                _properties.Where(pair => (pair.Value == complexProperty) && !_deletedProperties.ContainsKey(pair.Key)))
            {
                AddToChangeList(pair.Key, _modifiedProperties);
                Changed();
            }
        }


        internal static void AddToChangeList(PropertyDefinition propertyDefinition, List<PropertyDefinition> changeList)
        {
            if (!changeList.Contains(propertyDefinition))
            {
                changeList.Add(propertyDefinition);
            }
        }

        internal void Changed()
        {
            _isDirty = true;
            Owner.Changed();
        }

        internal void LoadFromXml(MyVrmServiceXmlReader reader, bool clearPropertyBag)
        {
            LoadFromXml(reader, clearPropertyBag, Owner.GetXmlElementName());
        }

        internal void LoadFromXml(MyVrmServiceXmlReader reader, bool clearPropertyBag, string xmlElementName)
        {
            if (clearPropertyBag)
            {
                Clear();
            }
            _loading = true;
            try
            {
                do
                {
                    reader.Read();
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        PropertyDefinition definition;
                        if (Owner.Schema.TryGetPropertyDefinition(reader.LocalName, out definition))
                        {
                            definition.LoadPropertyValueFromXml(reader, this);
                            _loadedProperties.Add(definition);
                        }
                        else
                        {
                            reader.SkipCurrentElement();
                        }
                    }
                } while (!reader.IsEndElement(XmlNamespace.NotSpecified, xmlElementName));
                ClearChangeLog();
            }
            finally
            {
                _loading = false;
            }
        }
        
        internal void Clear()
        {
            ClearChangeLog();
            _properties.Clear();
            _loadedProperties.Clear();
        }

        internal void ClearChangeLog()
        {
            _deletedProperties.Clear();
            _modifiedProperties.Clear();
            _addedProperties.Clear();
            foreach (var property in _properties.Select(pair => pair.Value).OfType<ComplexProperty>())
            {
                property.ClearChangeLog();
            }
            _isDirty = false;
        }


        internal bool Contains(PropertyDefinition propertyDefinition)
        {
            return _properties.ContainsKey(propertyDefinition);
        }

        internal bool TryGetValue(PropertyDefinition propertyDefinition, out object propertyValue)
        {
            return _properties.TryGetValue(propertyDefinition, out propertyValue);
        }

        /// <summary>
        /// Writes bag's properties to XML.
        /// </summary>
        /// <param name="writer">The writer to write the properties to.</param>
        internal void WriteToXml(MyVrmXmlWriter writer)
        {
            WriteToXml(writer, true);
        }

        internal void WriteToXml(MyVrmXmlWriter writer, bool withServiceObjectElement)
        {
            if (withServiceObjectElement)
                writer.WriteStartElement(XmlNamespace.NotSpecified, Owner.GetXmlElementName());

            // Write special id for a new oject
            var idPropertyDefinition = Owner.GetIdPropertyDefinition();
            if (idPropertyDefinition != null)
            {
                if (Contains(idPropertyDefinition))
                {
                    idPropertyDefinition.WritePropertyValueToXml(writer, this);
                }
                else
                {
                    writer.WriteElementValue(XmlNamespace.NotSpecified, idPropertyDefinition.XmlElementNameForWrite,
                                             Constants.NewServiceId);
                }
            }
            // Write properties
            foreach (var propertyDefinition in
                Owner.Schema.Where(propertyDefinition => propertyDefinition != idPropertyDefinition && propertyDefinition.HasFlag(PropertyDefinitionFlags.CanSet)))
            {
                try
                {
                    propertyDefinition.WritePropertyValueToXml(writer, this);

                }
                catch (Exception UE)
                {
                    
                    // DO nontihgn
                }

               
            }
            if (withServiceObjectElement)
                writer.WriteEndElement();
        }

        private object  GetPropertyValueOrException(PropertyDefinition propertyDefinition, out MyVrmServiceLocalException exception)
        {
            object propertyValue;
            exception = null;
            if (!TryGetValue(propertyDefinition, out propertyValue))
            {
                if (propertyDefinition.HasFlag(PropertyDefinitionFlags.AutoInstantiateOnRead))
                {
                    var complexPropertyDefinition =
                        propertyDefinition as ComplexPropertyDefinitionBase;
                    propertyValue = complexPropertyDefinition.CreatePropertyInstance(Owner);
                    if (propertyValue != null)
                    {
                        InitComplexProperty(propertyValue as ComplexProperty);
                        _properties[propertyDefinition] = propertyValue;
                    }
                    return propertyValue;
                }
                if (propertyDefinition == Owner.GetIdPropertyDefinition())
                {
                    return propertyValue;
                }

            }
            return propertyValue;
        }

        private void InitComplexProperty(ComplexProperty complexProperty)
        {
            if (complexProperty != null)
            {
                complexProperty.OnChange += PropertyChanged;
                var property = complexProperty as IOwnedProperty;
                if (property != null)
                {
                    property.Owner = Owner;
                }
            }
        }

    	public Object Clone()
		{
    		return Clone(Owner);
		}

		public PropertyBag Clone(ServiceObject newOwner)
		{
			var _propertiesCopy = new PropertyBag(newOwner);

			foreach (var property in _properties)
			{
				if (!property.Value.GetType().IsPrimitive)
				{
					if (property.Value.GetType().Name == "String")
					{
						_propertiesCopy._properties.Add(property.Key, property.Value != null ? string.Copy((string)property.Value) : null);
					}
					else
					{
						var ob = property.Value as ICloneable;
						if (ob != null)
							_propertiesCopy._properties.Add(property.Key, ((ICloneable)property.Value).Clone());
						else
						{
							_propertiesCopy._properties.Add(property.Key, property.Value);
						}
					}
				}
				else
				{
					_propertiesCopy._properties.Add(property.Key, property.Value);
				}
			}
			return _propertiesCopy;
		}
    }
}
