﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Globalization;

namespace MyVrm.WebServices.Data
{
    internal static class Utilities
    {
        private static readonly CultureInfo _cultureProvider = CultureInfo.InvariantCulture;

        internal static CultureInfo CultureProvider
        {
            get { return _cultureProvider; }
        }

        internal static XmlNamespace GetNamespaceFromUri(string uri)
        {
            return XmlNamespace.NotSpecified;
        }

        internal static string GetNamespacePrefix(XmlNamespace xmlNamespace)
        {
            return string.Empty;
        }

        internal static string GetNamespaceUri(XmlNamespace xmlNamespace)
        {
            return string.Empty;
        }

        internal static string SerializeEnum(Enum value)
        {
            return value.ToString("d");
        }

        internal static string DateTimeToString(DateTime value)
        {
            return value.ToString(Constants.DateTimeFormat, CultureProvider);
        }


        internal static DateTime StringToDate(string value)
        {
            return StringToDate(value, false);
        }

        internal static DateTime StringToDate(string value, bool assumeUniversal)
        {
            return DateTime.ParseExact(value, Constants.DateFormat, CultureProvider, assumeUniversal ? DateTimeStyles.AssumeUniversal : DateTimeStyles.None);
        }

		internal static DateTime StringToDateTime(string value, bool assumeUniversal)
		{
			return DateTime.ParseExact(value, Constants.DateTimeFormat, CultureProvider, assumeUniversal ? DateTimeStyles.AssumeUniversal : DateTimeStyles.None);
		}
		internal static DateTime StringToDateTime(string value)
		{
			return DateTime.Parse(value, CultureProvider, DateTimeStyles.AssumeLocal);
		}

        internal static string DateToString(DateTime value)
        {
            return value.ToString(Constants.DateFormat, CultureProvider);
        }

        internal static DateTime BuildDateTime(DateTime date, int hour, int minute, string timeSet)
        {
			return date.Date + BuildTimeOfDay(hour, minute, timeSet, MyVrmService.Service.UtcEnabled == 1);
        }

        internal static string GetNoonAbbr(TimeSpan timeOfDay)
        {
            return timeOfDay.Hours >= 12 && timeOfDay.Hours < 24 ? Constants.AfterNoonAbbr : Constants.BeforeNoonAbbr;
        }

		internal static string HourToString(int hour24)
		{
			return hour24 > 12 && hour24 <= 23 ? (hour24 - 12).ToString() : hour24.ToString();
		}

        internal static string TimeSpanToTimeOfDayString(TimeSpan value)
        {
            DateTime dateTime = DateTime.Today;
            dateTime += value;
            return dateTime.ToString(Constants.TimeFormat, CultureProvider);
        }

        internal static TimeSpan TimeOfDayStringToTimeSpan(string value)
        {
            return TimeOfDayStringToTimeSpan(value, false);
        }

        internal static TimeSpan TimeOfDayStringToTimeSpan(string value, bool assumeUniversal)
        {
            return DateTime.ParseExact(value, Constants.TimeFormat, CultureProvider, assumeUniversal ? DateTimeStyles.AssumeUniversal : DateTimeStyles.None).TimeOfDay;
        }

		internal static TimeSpan BuildTimeOfDay(int hour, int minute, string timeSet, bool assumeUniversal)
        {
            var format = string.Format("{0:D2}:{1:D2} {2}", hour, minute, timeSet);
			var dt = DateTime.ParseExact(format, "hh:mm tt", CultureProvider, assumeUniversal ? DateTimeStyles.AssumeUniversal : DateTimeStyles.AssumeLocal);
            return dt.TimeOfDay;
        }

        internal static string BoolToBoolString(bool value)
        {
            if (value)
            {
                return "1";
            }
            return "0";
        }

        internal static bool BoolStringToBool(string value)
        {
            if (string.IsNullOrEmpty(value))
                return false;
            return value.Trim() != "0";
        }

        internal static T Parse<T>(string value)
        {
            if (!typeof(T).IsEnum)
            {
                if (typeof(T) == typeof(bool))
                {
                    return (T)(object)BoolStringToBool(value);
                }
                return (T) Convert.ChangeType(value, typeof (T), CultureProvider);
            }
            return (T) Enum.Parse(typeof (T), value, false);
        }

        internal static MyVrmServiceErrorLevel ErrorLevelFromString(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return MyVrmServiceErrorLevel.Unknown;
            }
            switch(value.Trim())
            {
                case "E":
                    return MyVrmServiceErrorLevel.User;
                case "S":
                    return MyVrmServiceErrorLevel.System;
                case "C":
                    return MyVrmServiceErrorLevel.Custom;
                default:
                    return MyVrmServiceErrorLevel.Unknown;
            }
        }

        internal static string DaysOfWeekToString(DaysOfWeek daysOfWeek)
        {
            var days = new List<int>(7);
            if ((daysOfWeek & DaysOfWeek.Sunday) != 0)
            {
                days.Add(1);
            }
            if ((daysOfWeek & DaysOfWeek.Monday) != 0)
            {
                days.Add(2);
            }
            if ((daysOfWeek & DaysOfWeek.Tuesday) != 0)
            {
                days.Add(3);
            }
            if ((daysOfWeek & DaysOfWeek.Wednesday) != 0)
            {
                days.Add(4);
            }
            if ((daysOfWeek & DaysOfWeek.Thursday) != 0)
            {
                days.Add(5);
            }
            if ((daysOfWeek & DaysOfWeek.Friday) != 0)
            {
                days.Add(6);
            }
            if ((daysOfWeek & DaysOfWeek.Saturday) != 0)
            {
                days.Add(7);
            }
            return string.Join(",", Array.ConvertAll(days.ToArray(), day => Convert.ToString(day)));
        }

        internal static DaysOfWeek StringToDaysOfWeek(string value)
        {
            DaysOfWeek daysOfWeek = DaysOfWeek.None;
            if (string.IsNullOrEmpty(value))
            {
                return daysOfWeek;
            }
            var days = value.Split(new []{','});
            var intDays = Array.ConvertAll(days, day => Convert.ToInt32(day));
            
            foreach (var intDay in intDays)
            {
                switch (intDay)
                {
                    case 1:
                        daysOfWeek |= DaysOfWeek.Sunday;
                        break;
                    case 2:
                        daysOfWeek |= DaysOfWeek.Monday;
                        break;
                    case 3:
                        daysOfWeek |= DaysOfWeek.Tuesday;
                        break;
                    case 4:
                        daysOfWeek |= DaysOfWeek.Wednesday;
                        break;
                    case 5:
                        daysOfWeek |= DaysOfWeek.Thursday;
                        break;
                    case 6:
                        daysOfWeek |= DaysOfWeek.Friday;
                        break;
                    case 7:
                        daysOfWeek |= DaysOfWeek.Saturday;
                        break;
                }
            }
            return daysOfWeek;
        }

        internal static string WeekDayToString(DaysOfWeek daysOfWeek)
        {
            int weekDay;
            switch (daysOfWeek)
            {
                case DaysOfWeek.AllDays:
                    weekDay = 1;
                    break;
                case DaysOfWeek.Weekdays:
                    weekDay = 2;
                    break;
                case DaysOfWeek.WeekendDays:
                    weekDay = 3;
                    break;
                case DaysOfWeek.Sunday:
                    weekDay = 4;
                    break;
                case DaysOfWeek.Monday:
                    weekDay = 5;
                    break;
                case DaysOfWeek.Tuesday:
                    weekDay = 6;
                    break;
                case DaysOfWeek.Wednesday:
                    weekDay = 7;
                    break;
                case DaysOfWeek.Thursday:
                    weekDay = 8;
                    break;
                case DaysOfWeek.Friday:
                    weekDay = 9;
                    break;
                case DaysOfWeek.Saturday:
                    weekDay = 10;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("daysOfWeek");
            }
            return weekDay.ToString();
        }

        internal static DaysOfWeek StringToWeekDay(string value)
        {
            DaysOfWeek daysOfWeek = DaysOfWeek.None;
            if (string.IsNullOrEmpty(value))
            {
                return daysOfWeek;
            }
            var weekDay = Int32.Parse(value);
            switch(weekDay)
            {
                case 1:
                    daysOfWeek = DaysOfWeek.AllDays;
                    break;
                case 2:
                    daysOfWeek = DaysOfWeek.Weekdays;
                    break;
                case 3:
                    daysOfWeek = DaysOfWeek.WeekendDays;
                    break;
                case 4:
                    daysOfWeek = DaysOfWeek.Sunday;
                    break;
                case 5:
                    daysOfWeek = DaysOfWeek.Monday;
                    break;
                case 6:
                    daysOfWeek = DaysOfWeek.Tuesday;
                    break;
                case 7:
                    daysOfWeek = DaysOfWeek.Wednesday;
                    break;
                case 8:
                    daysOfWeek = DaysOfWeek.Thursday;
                    break;
                case 9:
                    daysOfWeek = DaysOfWeek.Friday;
                    break;
                case 10:
                    daysOfWeek = DaysOfWeek.Saturday;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("value");
            }
            return daysOfWeek;
        }

        internal static string ReplaceOutXMLSpecialCharacters(string str) //ZD 104771 start
        {
            return string.IsNullOrEmpty(str) ? str : str.Replace(Constants.LessThan, "<").Replace(Constants.GreaterThan, ">").Replace(Constants.ampersand, "&"); ;
        } //ZD 104771 End
    }
}
