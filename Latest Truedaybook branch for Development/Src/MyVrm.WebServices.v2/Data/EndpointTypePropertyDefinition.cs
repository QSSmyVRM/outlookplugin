﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    internal class EndpointTypePropertyDefinition : TypedPropertyDefinition
    {
        internal EndpointTypePropertyDefinition(string xmlElementName) : base(xmlElementName)
        {
            PropertyType = typeof(EndpointType);
        }


        internal EndpointTypePropertyDefinition(string xmlElementName, PropertyDefinitionFlags flags) : base(xmlElementName, flags)
        {
            PropertyType = typeof(EndpointType);
        }

        #region Overrides of TypedPropertyDefinition

        internal override object Parse(string value)
        {
            switch(value)
            {
                case "":
                {
                    return EndpointType.Endpoint;
                }
                case "U":
                {
                    return EndpointType.UserEndpoint;
                }
                case "R":
                {
                    return EndpointType.RoomEndpoint;
                }
            }
            return null;
        }

        internal override string ToString(object value)
        {
            switch((EndpointType)value)
            {
                case EndpointType.Endpoint:
                    return "";
                case EndpointType.UserEndpoint:
                    return "U";
                case EndpointType.RoomEndpoint:
                    return "R";
            }
            return null;
        }

        internal override void LoadPropertyValueFromXml(MyVrmServiceXmlReader reader, PropertyBag propertyBag)
        {
            string value = reader.ReadElementValue(XmlNamespace.NotSpecified, XmlElementName);
            propertyBag[this] = Parse(value);
        }

        internal override void WritePropertyValueToXml(MyVrmXmlWriter writer, PropertyBag propertyBag)
        {
            object value = propertyBag[this] ?? EndpointType.Endpoint;
            writer.WriteElementValue(XmlNamespace.NotSpecified, XmlElementName, ToString(value));
        }

        #endregion
    }
}
