﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.ComponentModel;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Registrator;
using DevExpress.XtraEditors.Repository;
using MyVrm.Common;

namespace MyVrm.Outlook.WinForms
{
    [UserRepositoryItem("Register")]
    public class RepositoryItemEnumComboBox : RepositoryItemComboBox
    {
        static RepositoryItemEnumComboBox()
        {
            Register();
        }

        internal const string EditorName = "EnumComboBoxEdit";

        public static void Register()
        {
            EditorRegistrationInfo.Default.Editors.Add(new EditorClassInfo(EditorName, typeof(EnumComboBoxEdit),
                            typeof(RepositoryItemEnumComboBox), typeof(DevExpress.XtraEditors.ViewInfo.ComboBoxViewInfo),
                            new DevExpress.XtraEditors.Drawing.ButtonEditPainter(), true, null, typeof(DevExpress.Accessibility.ComboBoxEditAccessible)));
        }

        public override string EditorTypeName
        {
            get { return EditorName; }
        }

        public override string GetDisplayText(DevExpress.Utils.FormatInfo format, object editValue)
        {
            if (editValue is Enum)
            {
                return LocalizedDescriptionAttribute.FromEnum(editValue.GetType(), editValue);
            }
            return base.GetDisplayText(format, editValue);
        }

        public override string GetDisplayText(object editValue)
        {
            if (editValue is Enum)
            {
                return LocalizedDescriptionAttribute.FromEnum(editValue.GetType(), editValue);
            }
            return base.GetDisplayText(editValue);
        }
    }

    public class EnumComboBoxEdit : ComboBoxEdit
    {
        public EnumComboBoxEdit()
        {
            Properties.TextEditStyle = TextEditStyles.DisableTextEditor;    
        }

        static EnumComboBoxEdit()
        {
            RepositoryItemEnumComboBox.Register();
        }
        [Browsable(false)]
        public override string EditorTypeName
        {
            get { return RepositoryItemEnumComboBox.EditorName; }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public new RepositoryItemEnumComboBox Properties
        {
            get { return base.Properties as RepositoryItemEnumComboBox; }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override object SelectedItem
        {
            get
            {
                foreach (LocalizedEnum localizedEnum in Properties.Items)
                {
                    if (localizedEnum.Value.Equals(EditValue))
                    {
                        return localizedEnum;
                    }
                }
                return base.SelectedItem;
            }
            set
            {
                if (value == null)
                {
                    EditValue = null;
                    return;
                }
                if (Properties.Items.Contains(value))
                {
                    var localizedEnum = value as LocalizedEnum;
                    if (localizedEnum != null)
                    {
                        EditValue = localizedEnum.Value;
                    }
                }
                base.SelectedItem = value;
            }
        }
        [Browsable(false)]
        public override object EditValue
        {
            get
            {
                return base.EditValue;
            }
            set
            {
                if (IsLoading)
                {
                    base.EditValue = value;
                    return;
                }
                var localizedEnum = value as LocalizedEnum;
                base.EditValue = localizedEnum != null ? localizedEnum.Value : value;
            }
        }
    }
}
