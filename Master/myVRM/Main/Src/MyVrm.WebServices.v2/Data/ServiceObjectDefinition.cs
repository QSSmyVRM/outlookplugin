﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace MyVrm.WebServices.Data
{
    /// <summary>
    /// Decorates classes that map to myVRM service objects.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    internal sealed class ServiceObjectDefinitionAttribute : Attribute
    {
        private readonly string _xmlElementName;

        internal ServiceObjectDefinitionAttribute(string xmlElementName)
        {
            _xmlElementName = xmlElementName;
        }

        internal string XmlElementName
        {
            get { return _xmlElementName; }
        }
    }
}
