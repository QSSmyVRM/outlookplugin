﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace MyVrm.Outlook.WinForms.Conference
{
    public partial class ConferenceAwareControl : BaseControl
    {
        private bool _readOnly;
        private BindingSource _conferenceBindingSource;

        public ConferenceAwareControl()
        {
            InitializeComponent();
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false),
         EditorBrowsable(EditorBrowsableState.Never)]
        public ConferenceWrapper Conference
        {
            get
            {
                return _conferenceBindingSource != null ? (ConferenceWrapper) _conferenceBindingSource.DataSource : null;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false),
         EditorBrowsable(EditorBrowsableState.Never)]
        public BindingSource ConferenceBindingSource
        {
            get
            {
                return _conferenceBindingSource;
            }
            set
            {
                if (_conferenceBindingSource != value)
                {
                    if (_conferenceBindingSource != null)
                    {
                        _conferenceBindingSource.DataSourceChanged -= ConferenceBindingSourceDataSourceChanged;
                    }
                    _conferenceBindingSource = null;
                }
                if (value != null)
                {
                    _conferenceBindingSource = value;
                    _conferenceBindingSource.DataSourceChanged += ConferenceBindingSourceDataSourceChanged;
                }
            }
        }

        public bool ReadOnly
        {
            get
            {
                return _readOnly;
            }
            set
            {
                if (value != _readOnly)
                {
                    _readOnly = value;
                    OnReadOnlyChanged(new ReadOnlyChangedEventArgs(_readOnly));
                }
            }
        }

        protected virtual void OnReadOnlyChanged(ReadOnlyChangedEventArgs e)
        {
        }

        protected virtual void OnConferenceBindingSourceDataSourceChanged(EventArgs e)
        {
        }

        private void ConferenceBindingSourceDataSourceChanged(object sender, EventArgs e)
        {
            OnConferenceBindingSourceDataSourceChanged(e);
        }
    }
}
