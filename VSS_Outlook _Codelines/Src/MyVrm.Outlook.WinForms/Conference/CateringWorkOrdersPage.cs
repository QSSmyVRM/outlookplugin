﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/


using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraTreeList.Nodes;
using MyVrm.Common.ComponentModel;
using MyVrm.WebServices.Data;
using System.Collections.Generic;


namespace MyVrm.Outlook.WinForms.Conference
{
    public partial class CateringWorkOrdersPage : ConferencePage
    {
        private class WorkOrderRecord
        {
            private Room _room;
            private CateringWorkOrder _workOrder;

            internal WorkOrderRecord(Room room, CateringWorkOrder workOrder)
            {
                _room = room;
                _workOrder = workOrder;
            }

            public RoomId RoomId
            {
                get
                {
                    return _room.Id;
                }
            }
            public string RoomName
            {
                get
                {
                    return _room.Name;
                }
            }
            public WorkOrderId Id
            {
                get
                {
                    return _workOrder != null ? _workOrder.Id : null;
                }
            }
            public string Name
            {
                get
                {
                    return _workOrder != null ? _workOrder.Name : null;
                }
            }

            public uint? Quantity
            {
                get
                {
                    if (_workOrder != null)
                    {
                        long quantity = _workOrder.Menus.Sum(menu => menu.Quantity);
                        return Convert.ToUInt32(quantity);
                    }
                    return null;
                }
            }

            internal CateringWorkOrder WorkOrder
            {
                get
                {
                    return _workOrder;
                }
                set
                {
                    _workOrder = value;
                }
            }

            internal void DetachWorkOrder()
            {
                _workOrder = null;
            }
        }

        private readonly DataList<WorkOrderRecord> _workOrderRecords = new DataList<WorkOrderRecord>();

        public CateringWorkOrdersPage()
        {
            InitializeComponent();
            Text = Strings.CateringWorkOrdersPageText;
			bar1.Text = Strings.ToolsBarText;
        	roomNameColumn.Caption = Strings.RoomLableText;
			quantityColumn.Caption = Strings.QuantityColumnText;

            workOrderList.DataSource = _workOrderRecords;
			assignWorkOrderBarButtonItem.Caption = Strings.AssignMenuItemText;
			editWorkOrderBarButtonItem.Caption = Strings.EditMenuItemText;
			removeWorkOrderBarButtonItem.Caption = Strings.RemoveMenuItemText;
        }

        protected override void OnReadOnlyChanged(ReadOnlyChangedEventArgs e)
        {
            base.OnReadOnlyChanged(e);
            bar1.Visible = !e.ReadOnly;
            workOrderList.Enabled = !e.ReadOnly;
        }

        void WorkOrderRecordsListChanged(object sender, System.ComponentModel.ListChangedEventArgs e)
        {
            switch (e.ListChangedType)
            {
                case ListChangedType.Reset:
                    break;
                case ListChangedType.ItemAdded:
                    break;
                case ListChangedType.ItemDeleted:
                    break;
                case ListChangedType.ItemMoved:
                    break;
                case ListChangedType.ItemChanged:
                    {
                        EnableBarButtons(_workOrderRecords[e.NewIndex].WorkOrder);
                        break;
                    }
                case ListChangedType.PropertyDescriptorAdded:
                    break;
                case ListChangedType.PropertyDescriptorDeleted:
                    break;
                case ListChangedType.PropertyDescriptorChanged:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void EnableBarButtons(CateringWorkOrder workOrder)
        {
            var isWorkOrderAssigned = workOrder != null;
            //assignWorkOrderBarButtonItem.Enabled = !isWorkOrderAssigned; //ZD 103771 
            editWorkOrderBarButtonItem.Enabled = isWorkOrderAssigned;
           // removeWorkOrderBarButtonItem.Enabled = isWorkOrderAssigned;
        }

        private void EnableBarButtons(TreeListNode node)
        {
            if (node != null)
            {
                EnableBarButtons(_workOrderRecords[node.Id].WorkOrder);
            }
            else
            {
                assignWorkOrderBarButtonItem.Enabled = false;  
                editWorkOrderBarButtonItem.Enabled = false;
                removeWorkOrderBarButtonItem.Enabled = false;
            }
        }

        void ConferenceBindingSource_DataSourceChanged(object sender, EventArgs e)
        {
            ResetData();
        }

        private void ResetData()
        {
            if (Conference != null)
            {
                Conference.LocationIds.ListChanged += LocationIds_ListChanged;
                ResetWorkOrdersRecords();
            }
        }

        private void CateringWorkOrdersPage_Load(object sender, EventArgs e)
        {
            ConferenceBindingSource.DataSourceChanged += ConferenceBindingSource_DataSourceChanged;
            _workOrderRecords.ListChanged += WorkOrderRecordsListChanged;
            ResetData();
        }

        void LocationIds_ListChanged(object sender, ListChangedEventArgs e)
        {
            switch (e.ListChangedType)
            {
                case ListChangedType.Reset:
                    {
                        ResetWorkOrdersRecords();
                        break;
                    }
                case ListChangedType.ItemAdded:
                    {
                        var room = MyVrmAddin.Instance.GetRoomFromCache(new RoomId(Conference.LocationIds[e.NewIndex].ConfEndPt.Id.Id));
                         CateringWorkOrder workorder = Conference.Conference.CateringWorkOrders.FirstOrDefault(order => order.RoomId.Equals(room.Id));
                        _workOrderRecords.Add(new WorkOrderRecord(room, workorder));
                        break;
                    }
                case ListChangedType.ItemDeleted:
                    {
                        _workOrderRecords.RemoveAt(e.NewIndex);
                        break;
                    }
                case ListChangedType.ItemMoved:
                    {
                        var workOrderRecord = _workOrderRecords[e.OldIndex];
                        _workOrderRecords.Move(workOrderRecord, e.NewIndex);
                        break;
                    }
                case ListChangedType.ItemChanged:
                    break;
                case ListChangedType.PropertyDescriptorAdded:
                    break;
                case ListChangedType.PropertyDescriptorDeleted:
                    break;
                case ListChangedType.PropertyDescriptorChanged:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            workOrderList.RefreshDataSource();
        }

        private void ResetWorkOrdersRecords()
        {
            _workOrderRecords.Clear();
            DataList<RoomId> roomID1;
            roomID1 = new DataList<RoomId>();
            List<string> Roomlist = new List<string>(); string Roomid = "";//ALLBUGS-123


            //DataList<ConfRoom> cfroom = new DataList<ConfRoom>();
           
            for (int i = 0; i < Conference.LocationIds.Count; i++)
            {
                RoomId RoomDummy = new RoomId("0");
                if (RoomDummy != new RoomId(Conference.LocationIds[i].ConfEndPt.Id.Id))
                    roomID1.Add(new RoomId(Conference.LocationIds[i].ConfEndPt.Id.Id));
                var roomid = MyVrmAddin.Instance.GetRoomFromCache(new RoomId(Conference.LocationIds[i].ConfEndPt.Id.Id));//ALLBUGS-123
                Roomlist.Add(roomid.Id.ToString());
            }
            var query = from roomId in roomID1
                        join workOrder in Conference.Conference.CateringWorkOrders on roomId equals
                            workOrder.RoomId into workOrders
                        from wo in workOrders.DefaultIfEmpty()
                        select new WorkOrderRecord(MyVrmAddin.Instance.GetRoomFromCache(roomId), wo);
            //ALLBUGS-123 start
            for (int i = 0; i < Conference.Conference.CateringWorkOrders.Count; i++)
            {
                Roomid = Conference.Conference.CateringWorkOrders[i].RoomId.ToString();
                if (!Roomlist.Contains(Roomid))
                {
                    Conference.Conference.CateringWorkOrders.Remove(Conference.Conference.CateringWorkOrders.FirstOrDefault(order => order.RoomId.ToString() == Roomid));
                }
            }
            //ALLBUGS-123 End
            _workOrderRecords.AddRange(query);
        }
        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            AssignWorkOrder();
        }

        private void AssignWorkOrder()
        {
            bool newWorkorder = false; //103771 start
            if (workOrderList.FocusedNode == null)
            {
                return;
            }
            using (var dialog = new CateringWorkOrderDialog())
            {
                var workOrderRecord = _workOrderRecords[workOrderList.FocusedNode.Id];
                dialog.WorkOrder = new CateringWorkOrder(MyVrmService.Service)
                {
                    Name = string.Format("{0}_CAT", Conference.Conference.Name),
                    RoomId = workOrderRecord.RoomId,
                    DeliveryTime = Conference.StartDate,
                    Price = 0
                };
                if (_workOrderRecords[workOrderList.FocusedNode.Id].WorkOrder == null) //103771
                    newWorkorder = true;//103771
                     
                if (dialog.ShowDialog(this) == DialogResult.OK)
                {
                    Conference.Conference.CateringWorkOrders.Add(dialog.WorkOrder);
                    //var record = _workOrderRecords.LastOrDefault(rec => rec.RoomId == dialog.WorkOrder.RoomId);
                    //record.WorkOrder = dialog.WorkOrder;
                    if (newWorkorder) //103771 start
                    {
                        var record = _workOrderRecords.FirstOrDefault(rec => rec.RoomId == dialog.WorkOrder.RoomId);
                        record.WorkOrder = dialog.WorkOrder;
                        _workOrderRecords.ResetItem(workOrderList.FocusedNode.Id);
                    }
                    else
                    {
                        var room = MyVrmAddin.Instance.GetRoomFromCache(dialog.WorkOrder.RoomId);
                        CateringWorkOrder workorder = Conference.Conference.CateringWorkOrders.LastOrDefault(order => order.RoomId.Equals(room.Id));

                        WorkOrderRecord woID = new WorkOrderRecord(room, dialog.WorkOrder);
                        _workOrderRecords.Add(woID);
                        
                    }
                      //103771 End
					if (Conference != null)
						Conference.Appointment.SetNonOutlookProperty(true); 
                }
            }
        }
        private void editWorkOrderBarButtonItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EditWorkOrder();
        }

        private void EditWorkOrder()
        {
            if (workOrderList.FocusedNode == null)
            {
                return;
            }
            using (var dialog = new CateringWorkOrderDialog())
            {
                var workOrderRecord = _workOrderRecords[workOrderList.FocusedNode.Id];
                dialog.WorkOrder = workOrderRecord.WorkOrder;
                if (dialog.ShowDialog(this) == DialogResult.OK)
                {
                    _workOrderRecords.ResetItem(workOrderList.FocusedNode.Id);
					if (Conference != null)
						Conference.Appointment.SetNonOutlookProperty(true);
                }
            }
        }

        //ZD 103771 start
        private void removeWorkOrderBarButtonItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            List<string> Roomlist = new List<string>();
            string Roomid = "";
            int count = 0; 
            if (workOrderList.FocusedNode != null)
            {
                for (int i = 0; i < Conference.Conference.CateringWorkOrders.Count; i++)
                {
                    Roomid = Conference.Conference.CateringWorkOrders[i].RoomId.ToString();
                    Roomlist.Add(Roomid);
                }
                var workOrderRecord = _workOrderRecords[workOrderList.FocusedNode.Id];
                for (int i = 0; i < Roomlist.Count; i++)
                {
                    if (Roomlist.Contains(workOrderRecord.RoomId.ToString()))
                    {
                        count = count + 1;
                    }
                }
                if (count > 1)
                {
                    Conference.Conference.CateringWorkOrders.Remove(workOrderRecord.WorkOrder);
                    _workOrderRecords.Remove(workOrderRecord);
                    workOrderRecord.DetachWorkOrder();
                    _workOrderRecords.ResetItem(workOrderList.FocusedNode.Id);
                }
                else
                {
                    Conference.Conference.CateringWorkOrders.Remove(workOrderRecord.WorkOrder);
                    workOrderRecord.DetachWorkOrder();
                    _workOrderRecords.ResetItem(workOrderList.FocusedNode.Id);
                }

                if (Conference != null)
                    Conference.Appointment.SetNonOutlookProperty(true);

                //var workOrderRecord = _workOrderRecords[workOrderList.FocusedNode.Id];
                //Conference.Conference.CateringWorkOrders.Remove(workOrderRecord.WorkOrder);
               
                //workOrderRecord.DetachWorkOrder();
                //_workOrderRecords.ResetItem(workOrderList.FocusedNode.Id); //ZD 103771 End
                if (Conference != null)
                    Conference.Appointment.SetNonOutlookProperty(true);
            }
        }

        private void workOrderList_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            EnableBarButtons(e.Node);
        }

        private void workOrderList_DoubleClick(object sender, EventArgs e)
        {
            if (workOrderList.FocusedNode != null)
            {
                if (_workOrderRecords[workOrderList.FocusedNode.Id].WorkOrder == null)
                {
                    AssignWorkOrder();
                }
                else
                {
                    EditWorkOrder();
                }
            }
        }
    }
}
