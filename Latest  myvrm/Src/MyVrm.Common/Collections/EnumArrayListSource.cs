/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace MyVrm.Common.Collections
{
    /// <summary>
    /// Represents an array of enum values
    /// </summary>
    public class EnumArrayListSource : EnumListSource
    {
        private Array _enumValues;
        /// <summary>
        /// Initialize a new instance of <see cref="EnumArrayListSource"/>
        /// </summary>
        /// <param name="values">Array of enumeration members</param>
        /// <param name="enumType">Type of enumeration</param>
        public EnumArrayListSource(Array values, Type enumType)
        {
            EnumValues = values;
            EnumType = enumType;
        }
        /// <summary>
        /// Gets or sets array of enumeration members
        /// </summary>
        public Array EnumValues
        {
            get { return _enumValues; }
            set { _enumValues = value; }
        }
        /// <summary>
        /// Returns array of enumeration members
        /// </summary>
        /// <returns>Array of enumeration members</returns>
        protected override Array GetValues()
        {
            return _enumValues;
        }
    }
}