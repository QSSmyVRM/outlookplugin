﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

namespace MyVrm.Outlook.WinForms.Conference
{
	partial class AudioAddInPage
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AudioAddInPage));
			this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
			this.bar1 = new DevExpress.XtraBars.Bar();
			this.barAddEntry = new DevExpress.XtraBars.BarButtonItem();
			this.barDelEntry = new DevExpress.XtraBars.BarButtonItem();
			this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
			this.imageList = new System.Windows.Forms.ImageList(this.components);
			this.treeList = new DevExpress.XtraTreeList.TreeList();
			this.treeListAudioConferenceBridgeColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.repositoryItemEnumComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
			this.treeListLeaderPinColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
			this.treeListParticipantCodeColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.treeList)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.repositoryItemEnumComboBox1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
			this.SuspendLayout();
			// 
			// barManager1
			// 
			this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
			this.barManager1.DockControls.Add(this.barDockControlTop);
			this.barManager1.DockControls.Add(this.barDockControlBottom);
			this.barManager1.DockControls.Add(this.barDockControlLeft);
			this.barManager1.DockControls.Add(this.barDockControlRight);
			this.barManager1.Form = this;
			this.barManager1.Images = this.imageList;
			this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barAddEntry,
            this.barDelEntry});
			this.barManager1.MaxItemId = 2;
			// 
			// bar1
			// 
			this.bar1.BarName = "Tools";
			this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
			this.bar1.DockCol = 0;
			this.bar1.DockRow = 0;
			this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
			this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barAddEntry, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barDelEntry, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
			this.bar1.OptionsBar.AllowQuickCustomization = false;
			this.bar1.OptionsBar.DisableClose = true;
			this.bar1.OptionsBar.DisableCustomization = true;
			this.bar1.OptionsBar.DrawDragBorder = false;
			this.bar1.OptionsBar.UseWholeRow = true;
			this.bar1.Text = "Tools";
			// 
			// barAddEntry
			// 
			this.barAddEntry.Caption = "Add Entry";
			this.barAddEntry.Id = 0;
			this.barAddEntry.ImageIndex = 0;
			this.barAddEntry.Name = "barAddEntry";
			this.barAddEntry.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
			this.barAddEntry.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barAddEntry_ItemClick);
			// 
			// barDelEntry
			// 
			this.barDelEntry.Caption = "Delete Entry";
			this.barDelEntry.Id = 1;
			this.barDelEntry.ImageIndex = 1;
			this.barDelEntry.Name = "barDelEntry";
			this.barDelEntry.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
			this.barDelEntry.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barDelEntry_ItemClick);
			// 
			// barDockControlTop
			// 
			this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
			this.barDockControlTop.Size = new System.Drawing.Size(449, 30);
			// 
			// barDockControlBottom
			// 
			this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.barDockControlBottom.Location = new System.Drawing.Point(0, 240);
			this.barDockControlBottom.Size = new System.Drawing.Size(449, 0);
			// 
			// barDockControlLeft
			// 
			this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
			this.barDockControlLeft.Location = new System.Drawing.Point(0, 30);
			this.barDockControlLeft.Size = new System.Drawing.Size(0, 210);
			// 
			// barDockControlRight
			// 
			this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
			this.barDockControlRight.Location = new System.Drawing.Point(449, 30);
			this.barDockControlRight.Size = new System.Drawing.Size(0, 210);
			// 
			// imageList
			// 
			this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
			this.imageList.TransparentColor = System.Drawing.Color.Transparent;
			this.imageList.Images.SetKeyName(0, "Create.png");
			this.imageList.Images.SetKeyName(1, "Remove.png");
			// 
			// treeList
			// 
			this.treeList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListAudioConferenceBridgeColumn,
            this.treeListLeaderPinColumn,
            this.treeListParticipantCodeColumn});
			this.treeList.Dock = System.Windows.Forms.DockStyle.Fill;
			this.treeList.Location = new System.Drawing.Point(0, 30);
			this.treeList.Name = "treeList";
			this.treeList.OptionsView.ShowHorzLines = false;
			this.treeList.OptionsView.ShowIndicator = false;
			this.treeList.OptionsView.ShowRoot = false;
			this.treeList.OptionsView.ShowVertLines = false;
			this.treeList.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemEnumComboBox1,
            this.repositoryItemSpinEdit1});
			this.treeList.Size = new System.Drawing.Size(449, 210);
			this.treeList.TabIndex = 4;
			this.treeList.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.treeList_FocusedNodeChanged);
			this.treeList.FocusedColumnChanged += new DevExpress.XtraTreeList.FocusedColumnChangedEventHandler(this.treeList_FocusedColumnChanged);
			// 
			// treeListAudioConferenceBridgeColumn
			// 
			this.treeListAudioConferenceBridgeColumn.Caption = "Audio Conference Bridge";
			this.treeListAudioConferenceBridgeColumn.ColumnEdit = this.repositoryItemEnumComboBox1;
			this.treeListAudioConferenceBridgeColumn.FieldName = "Audio Conference Bridge";
			this.treeListAudioConferenceBridgeColumn.Name = "treeListAudioConferenceBridgeColumn";
			this.treeListAudioConferenceBridgeColumn.OptionsColumn.AllowMove = false;
			this.treeListAudioConferenceBridgeColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
			this.treeListAudioConferenceBridgeColumn.OptionsColumn.AllowSort = false;
			this.treeListAudioConferenceBridgeColumn.Visible = true;
			this.treeListAudioConferenceBridgeColumn.VisibleIndex = 0;
			this.treeListAudioConferenceBridgeColumn.Width = 157;
			// 
			// repositoryItemEnumComboBox1
			// 
			this.repositoryItemEnumComboBox1.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.repositoryItemEnumComboBox1.AutoHeight = false;
			this.repositoryItemEnumComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.repositoryItemEnumComboBox1.Name = "repositoryItemEnumComboBox1";
			this.repositoryItemEnumComboBox1.NullText = "<Select bridge>";
			this.repositoryItemEnumComboBox1.NullValuePrompt = "<Select bridge>";
			this.repositoryItemEnumComboBox1.NullValuePromptShowForEmptyValue = true;
			this.repositoryItemEnumComboBox1.Tag = "<Null>";
			this.repositoryItemEnumComboBox1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
			this.repositoryItemEnumComboBox1.ValidateOnEnterKey = true;
			this.repositoryItemEnumComboBox1.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.repositoryItemEnumComboBox1_EditValueChanging);
			// 
			// treeListLeaderPinColumn
			// 
			this.treeListLeaderPinColumn.Caption = "Leader Pin";
			this.treeListLeaderPinColumn.ColumnEdit = this.repositoryItemSpinEdit1;
			this.treeListLeaderPinColumn.FieldName = "Leader Pin";
			this.treeListLeaderPinColumn.Name = "treeListLeaderPinColumn";
			this.treeListLeaderPinColumn.OptionsColumn.AllowSort = false;
			this.treeListLeaderPinColumn.Visible = true;
			this.treeListLeaderPinColumn.VisibleIndex = 1;
			// 
			// repositoryItemSpinEdit1
			// 
			this.repositoryItemSpinEdit1.AutoHeight = false;
			this.repositoryItemSpinEdit1.IsFloatValue = false;
			this.repositoryItemSpinEdit1.Mask.EditMask = "D";
			this.repositoryItemSpinEdit1.Mask.UseMaskAsDisplayFormat = true;
			this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
			this.repositoryItemSpinEdit1.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.LeaderPinAndParticipantCode_EditValueChanging);
			// 
			// treeListParticipantCodeColumn
			// 
			this.treeListParticipantCodeColumn.Caption = "Participant Code";
			this.treeListParticipantCodeColumn.ColumnEdit = this.repositoryItemSpinEdit1;
			this.treeListParticipantCodeColumn.FieldName = "Participant Code";
			this.treeListParticipantCodeColumn.Name = "treeListParticipantCodeColumn";
			this.treeListParticipantCodeColumn.OptionsColumn.AllowSort = false;
			this.treeListParticipantCodeColumn.Visible = true;
			this.treeListParticipantCodeColumn.VisibleIndex = 2;
			this.treeListParticipantCodeColumn.Width = 206;
			// 
			// AudioAddInPage
			// 
			this.AutoSize = true;
			this.Controls.Add(this.treeList);
			this.Controls.Add(this.barDockControlLeft);
			this.Controls.Add(this.barDockControlRight);
			this.Controls.Add(this.barDockControlBottom);
			this.Controls.Add(this.barDockControlTop);
			this.Name = "AudioAddInPage";
			this.Size = new System.Drawing.Size(449, 240);
			this.Validating += new System.ComponentModel.CancelEventHandler(this.AudioAddInPage_Validating);
			((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.treeList)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.repositoryItemEnumComboBox1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private DevExpress.XtraBars.BarManager barManager1;
		private DevExpress.XtraBars.Bar bar1;
		private DevExpress.XtraBars.BarDockControl barDockControlTop;
		private DevExpress.XtraBars.BarDockControl barDockControlBottom;
		private DevExpress.XtraBars.BarDockControl barDockControlLeft;
		private DevExpress.XtraBars.BarDockControl barDockControlRight;
		private DevExpress.XtraBars.BarButtonItem barAddEntry;
		private DevExpress.XtraBars.BarButtonItem barDelEntry;
		private System.Windows.Forms.ImageList imageList;
		private DevExpress.XtraTreeList.TreeList treeList;
		private DevExpress.XtraTreeList.Columns.TreeListColumn treeListLeaderPinColumn;
		private DevExpress.XtraTreeList.Columns.TreeListColumn treeListParticipantCodeColumn;
		private DevExpress.XtraTreeList.Columns.TreeListColumn treeListAudioConferenceBridgeColumn;
		private RepositoryItemComboBox repositoryItemEnumComboBox1;
		private RepositoryItemSpinEdit repositoryItemSpinEdit1;
	}
}
