/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System.Collections.Generic;

namespace MyVrm.WebServices.Data
{
    public class GetCustomAttributeDefinitionsResponse : ServiceResponse
    {
        private readonly List<CustomAttributeDefinition> _attributes = new List<CustomAttributeDefinition>();

        public List<CustomAttributeDefinition> Attributes
        {
            get { return _attributes; }
        }

        internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
        {
            base.ReadElementsFromXml(reader);
            do
            {
                reader.Read();
                if (reader.IsStartElement(XmlNamespace.NotSpecified, "CustomAttribute"))
                {
                    var attrib = new CustomAttributeDefinition(reader.Service);
                    attrib.LoadFromXml(reader, true);
                    _attributes.Add(attrib);
                }
                else
                {
                    reader.SkipCurrentElement();
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "CustomAttributesList"));
        }
    }
}