﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace MyVrm.Outlook
{
    public class CustomFormControl : UserControl
    {
        [Browsable(false)]
        public object OutlookItem
        {
            get
            {
                if (OutlookForm != null)
                {
                    return OutlookForm.Item;
                }
                return null;
            }
        }

        internal CustomForm OutlookForm { get; set; }

        public event EventHandler CustomFormShowing;
        public void OnShowing()
        {
            if (CustomFormShowing != null)
            {
                CustomFormShowing(this, EventArgs.Empty);
            }
        }

        public event EventHandler CustomFormClosed;
        public void OnClose()
        {
            if (CustomFormClosed != null)
            {
                CustomFormClosed(this, EventArgs.Empty);
            }
        }

    }
}
