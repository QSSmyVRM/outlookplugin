﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraLayout;
using DevExpress.XtraLayout.Utils;
using DevExpress.XtraPrinting.Native;
using MyVrm.Common;
using MyVrm.Common.ComponentModel;
using MyVrm.Outlook.WinForms.Calendar;
using MyVrm.Outlook.WinForms.ManageRooms;
using MyVrm.Outlook.WinForms.Properties;
using MyVrm.WebServices.Data;
using Image = System.Drawing.Image;

namespace MyVrm.Outlook.WinForms.Conference
{
	public partial class RoomSelectionNew : Dialog
	{
		public ConferenceWrapper Conf;
		private ConferenceType _conferenceType;
		public ConferenceType ConferenceType
		{
			set { _conferenceType = value; }
			get { return _conferenceType; }
		}

		public enum CloseDlgReason
		{
			None,
			BaseView,
			AdvView,
			CloseCancel,
			CloseOK
		}
		public List<RoomFlat> RoomList = new List<RoomFlat>();
		public RoomSelectionDialog.ViewType ViewType;
		public CloseDlgReason CloseReason;
		ToolTipController _toolTipController = new ToolTipController();
		public bool IsFavoriteChecked
		{
			set { checkEditShowFavorites.Checked = value; }
			get { return checkEditShowFavorites.Checked; }
		}
		public bool IsOnlyPublicChecked
		{
			set { checkEditPublicOnly.Checked = value; }
			get { return checkEditPublicOnly.Checked; }
		}

		public string SearchForText
		{
			set { textEditSearchFor.Text = value; }
			get { return textEditSearchFor.Text; }
		}
		public void SetComboBoxSearchColumnValue(string colName, string value)
		{
			int i = comboBoxSearchByColumn.Properties.Items.IndexOf(colName);
			if (i != -1)
			{
				comboBoxSearchByColumn.SelectedIndex = i;
				SearchForText = value;
			}
			else
			{
				SearchForText = string.Empty;
			}
		}

		private User _userProfile;
		private User UserProfile
		{
			get { return _userProfile ?? (_userProfile = MyVrmService.Service.GetUser()); }
		}

		private RoomId[] _preselectedRoomIds;
		public RoomId[] SelectedRooms
		{
			get
			{
				return GetSelected();
			}
			set
			{
				_preselectedRoomIds = new RoomId[value.Length];
				value.CopyTo(_preselectedRoomIds, 0);
			}
		}

		private enum PublicViewMode
		{
			Basic = 0,
			Graphic
		}
		private enum PrivateViewMode
		{
			Basic = 0,
			Advanced
		}

		private readonly DXPopupMenu _viewPublicPopupMenu;
		private readonly DXPopupMenu _viewPrivatePopupMenu;

		private int _currRecPageNo;
		private int _totalRecPages;
		//private int _totalPagesNumber = NotAssigned;
		//private int _readPagesNumber = 0;
		private const int NotAssigned = -1;
		private int _totalRecNumber = NotAssigned;
		private string _currRecFilter;
		private int _rowNumberPerPage;
		private int _currFocusedRowNumber;
		private int _currTopRowNumber;

		Dictionary<int, string> PublicCountriesDict = new Dictionary<int, string>();
		//Dictionary<int, string> PublicCountryStatesDict = new Dictionary<int, string>();
		Dictionary<Pair<int, string>, int> PublicCountryStatesDict = new Dictionary<Pair<int, string>, int>();
		Dictionary<int, string> PublicCountryStateCitiesDict = new Dictionary<int, string>();

		void CheckAndRestoreLayout(ColumnView columnView, string filePath)
		{
			FileInfo info = new FileInfo(filePath);

			if (info.Exists)
				columnView.RestoreLayoutFromXml(filePath);
		}

		public RoomSelectionNew()
		{
			MyVrmAddin.TraceSource.TraceInfoEx("RoomSelectionNew In");
			InitializeComponent();
			ApplyEnabled = false;
			ApplyVisible = false;

			colRoomType.DisplayFormat.Format = new CustomRoomTypeFormatter();
			colPublicRoomType.DisplayFormat.Format = new CustomRoomTypeFormatter();
			gridControl_RoomsInfoNew.DataSource = RoomList;

			/**/
			_viewPublicPopupMenu = new DXPopupMenu();
			_viewPublicPopupMenu.Items.Add(new DXMenuItem("Basic", SetPublicBasicLayout));
			_viewPublicPopupMenu.Items.Add(new DXMenuItem("Graphic", SetPublicGraphicLayout));
			_viewPrivatePopupMenu = new DXPopupMenu();
			_viewPrivatePopupMenu.Items.Add(new DXMenuItem("Basic", SetPrivateBasicLayout));
			_viewPrivatePopupMenu.Items.Add(new DXMenuItem("Advanced", SetPrivateAdvancedLayout));

			CheckAndRestoreLayout(layoutPublicBasic, Path.Combine(MyVrmAddin.LocalUserAppDataPath, "RoomSelectionNew_layoutPublicBasic.xml"));
			CheckAndRestoreLayout(layoutPublicGraphic, Path.Combine(MyVrmAddin.LocalUserAppDataPath, "RoomSelectionNew_layoutPublicGraphic.xml"));
			CheckAndRestoreLayout(layoutPrivateBasicView, Path.Combine(MyVrmAddin.LocalUserAppDataPath, "RoomSelectionNew_layoutPrivateBasicView.xml"));
			CheckAndRestoreLayout(layoutPrivateAdvancedView, Path.Combine(MyVrmAddin.LocalUserAppDataPath, "RoomSelectionNew_layoutPrivateAdvancedView.xml"));

			gridControl_RoomsInfoNew.MainView = layoutPrivateBasicView;
			SetPrivateBasicLayout(null, null);

			comboBoxSearchByColumn.Properties.Items.AddRange(new object[] { "Capacity", "City", "Name", "Phone", "Site", "State", "Zip Code" });
			//{ "Capacity",  "Name", "Phone", "Site", "State", "Zip Code" });
			
			dropBnChangeView.DropDownControl = _viewPrivatePopupMenu;

			_updPageDelegate = new UpdDelegate(UpdateRoomPage);
			_displayProgress = new UpdDelegate(DisplayProgress);

			MyVrmAddin.TraceSource.TraceInfoEx("RoomSelectionNew Out");
		}

		void ShowDetails()
		{
			MyVrmAddin.TraceSource.TraceInfoEx("ShowDetails In");

			object room = gridControl_RoomsInfoNew.MainView.GetRow(((ColumnView)gridControl_RoomsInfoNew.MainView).FocusedRowHandle) as RoomFlat;
			if (room != null)
			{
				RoomId roomId = ((RoomFlat)room).Id;
				if (((RoomFlat)room).IsPublic)
				{
					using (var dialog = new RoomInfo(roomId))
					{
						dialog.ShowDialog(this);
					}
				}
				else //Private - show old info
				{
					using (var dialog = new RoomInformationDialog())
					{
						var roomProfile = MyVrmAddin.Instance.GetRoomFromCache(roomId);
						dialog.RoomProfile = roomProfile;
						dialog.ShowDialog(this);
					}
				}
			}
			MyVrmAddin.TraceSource.TraceInfoEx("ShowDetails Out");
		}

		private void ShowDetails(object sender, EventArgs e)
		{
			ShowDetails();
		}

		private void AddRoomToConference()
		{
			RoomFlat room = gridControl_RoomsInfoNew.MainView.GetRow(((ColumnView)gridControl_RoomsInfoNew.MainView).FocusedRowHandle) as RoomFlat;
			if (room != null)
			{
				bool bFound = false;
				foreach (RoomFlat aRoom in roomListBoxControl.Items)
				{
					if (aRoom.Id == (room).Id)
					{
						bFound = true;
						break;
					}
				}
				if(!bFound)
					roomListBoxControl.Items.Add(room);
				//if (!roomListBoxControl.Items.Contains(room)
					//    roomListBoxControl.Items.Add(room);
				//string roomName = ((RoomFlat)room).Name;
				//if (!roomListBoxControl.Items.Contains(roomName))
				//    roomListBoxControl.Items.Add(((RoomFlat)room).Name);

				DataList<RoomId> added = new DataList<RoomId>();
				added.AddRange(_preselectedRoomIds.ToArray());
				added.Add(room.Id);
				_preselectedRoomIds = added.ToArray();
			}
		}
		private void RemoveRoomFromConference()
		{
			if (roomListBoxControl.SelectedItem != null)
				roomListBoxControl.Items.Remove(roomListBoxControl.SelectedItem);
		}

		private void repositoryAddRoom_Click(object sender, EventArgs e)
		{
			AddRoomToConference();
		}

		private void roomListBoxControl_DoubleClick(object sender, EventArgs e)
		{
			RemoveRoomFromConference();
		}

		private void gridControl_RoomsInfo_DoubleClick(object sender, EventArgs e)
		{
			AddRoomToConference();
		}

		private void comboBoxSearchColumn_SelectedIndexChanged(object sender, EventArgs e)
		{
			textEditSearchFor.Text = string.Empty;
			switch (comboBoxSearchByColumn.SelectedItem.ToString())
			{
				case "Capacity" :
					textEditSearchFor.Properties.NullValuePrompt = "Enter capacity value here, then click Search";
					break;
				case "City":
					textEditSearchFor.Properties.NullValuePrompt = "Enter location here, then click Search";
					break;
				case "Name":
					textEditSearchFor.Properties.NullValuePrompt = "Enter room name here, then click Search";
					break;
				case "Phone":
					textEditSearchFor.Properties.NullValuePrompt = "Enter phone number here, then click Search";
					break;
				case "Site":
					textEditSearchFor.Properties.NullValuePrompt = "Enter site name here, then click Search";
					break;
				case "State":
					textEditSearchFor.Properties.NullValuePrompt = "Enter state name here, then click Search";
					break;
				case "Zip Code":
					textEditSearchFor.Properties.NullValuePrompt = "Enter zip code here, then click Search";
					break;
			}
			textEditSearchFor.Properties.NullValuePromptShowForEmptyValue = true;
		}


		private void textEditSearchFor_EditValueChanged(object sender, EventArgs e)
		{
			/* Temporary?
			_currRecPageNo = 0;
			GetNextPage();
			*/
		}

		void ProcessSearch()
		{
			List<RoomFlat> subsetPublicRooms = null;

			bool bIsFavorite = checkEditShowFavorites.Checked;
			bool bIsPublicOnly = checkEditPublicOnly.Checked;

			if (string.IsNullOrEmpty(textEditSearchFor.Text))
			{
				subsetPublicRooms =
							new List<RoomFlat>(RoomList.FindAll(room => room.Name.StartsWith(textEditSearchFor.Text, StringComparison.InvariantCultureIgnoreCase)));
			}
			else
			{
				GridColumn selectedColumn = null;
				foreach (GridColumn column in ((ColumnView)gridControl_RoomsInfoNew.MainView).Columns)
				{
					if (column.FieldName != "Picture" && !string.IsNullOrEmpty(column.FieldName))
					{
						if (column.GetTextCaption() == comboBoxSearchByColumn.SelectedItem.ToString())
						{
							selectedColumn = column;
							break;
						}
					}
				}
				if (selectedColumn != null)
				{
					switch (selectedColumn.FieldName)
					{
						case "Room Name":
						case "Name":
							subsetPublicRooms =
								new List<RoomFlat>(RoomList.FindAll(room => !string.IsNullOrEmpty(room.Name) &&
									room.Name.StartsWith(textEditSearchFor.Text, StringComparison.InvariantCultureIgnoreCase)));
							break;
						case "ID":
							subsetPublicRooms =
								new List<RoomFlat>(RoomList.FindAll(room => room.Id.ToString().Equals(textEditSearchFor.Text, StringComparison.InvariantCultureIgnoreCase)));
							break;
						case "Capacity":
							int val = 0;
							int.TryParse(textEditSearchFor.Text, out val);
							subsetPublicRooms =
								new List<RoomFlat>(RoomList.FindAll(room => room.Capacity >= val));
							break;
						case "Address":
							subsetPublicRooms =
								new List<RoomFlat>(RoomList.FindAll(room => !string.IsNullOrEmpty(room.Address) &&
									room.Address.StartsWith(textEditSearchFor.Text, StringComparison.InvariantCultureIgnoreCase)));
							break;
						case "State":
							subsetPublicRooms =
								new List<RoomFlat>(RoomList.FindAll(room => !string.IsNullOrEmpty(room.State) &&
									room.State.StartsWith(textEditSearchFor.Text, StringComparison.InvariantCultureIgnoreCase)));
							break;
						//
						case "ZipCode":
						case "Zip Code":
							subsetPublicRooms =
								new List<RoomFlat>(RoomList.FindAll(room => !string.IsNullOrEmpty(room.ZipCode) &&
									room.ZipCode.StartsWith(textEditSearchFor.Text, StringComparison.InvariantCultureIgnoreCase)));
							break;
						case "City":
							subsetPublicRooms =
								new List<RoomFlat>(RoomList.FindAll(room => !string.IsNullOrEmpty(room.City) &&
									room.City.StartsWith(textEditSearchFor.Text, StringComparison.InvariantCultureIgnoreCase)));
							break;
						case "Phone":
							subsetPublicRooms =
								new List<RoomFlat>(RoomList.FindAll(room => !string.IsNullOrEmpty(room.Phone) &&
									room.Phone.StartsWith(textEditSearchFor.Text, StringComparison.InvariantCultureIgnoreCase)));
							break;
						case "Site":
							subsetPublicRooms =
								new List<RoomFlat>(RoomList.FindAll(room => !string.IsNullOrEmpty(room.Site) &&
									room.Site.StartsWith(textEditSearchFor.Text, StringComparison.InvariantCultureIgnoreCase)));
							break;
						case "Assistant":
							subsetPublicRooms =
								new List<RoomFlat>(RoomList.FindAll(room => !string.IsNullOrEmpty(room.Assistant) &&
									room.Assistant.StartsWith(textEditSearchFor.Text, StringComparison.InvariantCultureIgnoreCase)));
							break;
						case "Floor":
							subsetPublicRooms =
								new List<RoomFlat>(RoomList.FindAll(room => !string.IsNullOrEmpty(room.Floor) &&
									room.Floor.StartsWith(textEditSearchFor.Text, StringComparison.InvariantCultureIgnoreCase)));
							break;
					}
				}
			}
			if (bIsFavorite)
			{
				subsetPublicRooms = subsetPublicRooms.FindAll(room => room.IsFavorite);
			}
			else
			{
				if (bIsPublicOnly)
				{
					subsetPublicRooms = subsetPublicRooms.FindAll(room => room.IsPublic);
				}
				else
				{
					subsetPublicRooms = subsetPublicRooms.FindAll(room => room.IsPublic == false);
				}
			}

			gridControl_RoomsInfoNew.DataSource = subsetPublicRooms;
		}

		private void AdjustUI()
		{
			SetPopupMenu(checkEditPublicOnly.Checked);

			//If Public popup menu is not set yet
			if (_viewPublicPopupMenu.Items[(int)PublicViewMode.Basic].Image == null &&
				_viewPublicPopupMenu.Items[(int)PublicViewMode.Graphic].Image == null)
			{
				_viewPublicPopupMenu.Items[(int)PublicViewMode.Basic].Image = Resources.Checkbox;
			}

			//Setup view
			if (checkEditPublicOnly.Checked)
			{
				if (_viewPublicPopupMenu.Items[(int)PublicViewMode.Basic].Image != null)
				{
					SetPublicBasicLayout(null, null);
				}
				else
				{
					if (_viewPublicPopupMenu.Items[(int)PublicViewMode.Graphic].Image != null)
					{
						SetPublicGraphicLayout(null, null);
					}
				}
			}
			else
			{
				if (_viewPrivatePopupMenu.Items[(int)PrivateViewMode.Basic].Image != null)
				{
					SetPrivateBasicLayout(null, null);
				}
				else
				{
					if (_viewPrivatePopupMenu.Items[(int)PrivateViewMode.Advanced].Image != null)
					{
						SetPrivateAdvancedLayout(null, null);
					}
				}
			}
		}

		void SetSearchParams(bool forPublic)
		{
			LayoutVisibility setShow = forPublic ? LayoutVisibility.Always : LayoutVisibility.Never;

			countryControlItem.Visibility = setShow;
			stateControlItem.Visibility = setShow;
			countryStateLayoutGroup.Visibility = setShow;
			countryStateLayoutControl.Visible = forPublic;
			countryStateLayoutControlItem.Visibility = setShow;

			searchByControlItem.TextLocation = forPublic ? Locations.Left : Locations.Top;
			searchForControlItem.TextLocation = forPublic ? Locations.Left : Locations.Top;
			//searchByControlItem.AppearanceItemCaption.TextOptions.VAlignment = forPublic
			//                                                                    ? VertAlignment.Default
			//                                                                    : VertAlignment.Top;
			//layoutControlItem5.AppearanceItemCaption.TextOptions.VAlignment = forPublic
			//                                                                    ? VertAlignment.Default
			//                                                                    : VertAlignment.Top;

			if(forPublic)
			{
				PublicCountryStateCitiesDict.Clear();
				searchByControlItem.Text = "City:";
				comboBoxSearchByColumn.Properties.Items.Clear();
				comboBoxSearchByColumn.EditValue = string.Empty;
				comboBoxSearchByColumn.Enabled = false;
				//Set not to expand comboBoxSearchByColumn
				//comboBoxSearchByColumn.Width = countryComboBox.Width;
				//comboBoxSearchByColumn.MaximumSize = new Size(countryComboBox.Width, comboBoxSearchByColumn.Height);
				
				searchForControlItem.Visibility = LayoutVisibility.Never;
			}
			else
			{
				searchByControlItem.Text = "Search by:";
				comboBoxSearchByColumn.EditValue = string.Empty;
				comboBoxSearchByColumn.Properties.Items.Clear();
				comboBoxSearchByColumn.Properties.Items.AddRange(new object[] { "Capacity", "City", "Name", "Phone", "Site", "State", "Zip Code" });
				searchForControlItem.Visibility = LayoutVisibility.Always;
				comboBoxSearchByColumn.Enabled = true;
				comboBoxSearchByColumn.SelectedIndex = //0;
										comboBoxSearchByColumn.Properties.Items.IndexOf(
				((ColumnView)gridControl_RoomsInfoNew.MainView).Columns.ColumnByFieldName("Name").GetTextCaption());
			}
		}

		private void checkEditXX_CheckedChanged(object sender, EventArgs e)
		{
			//UIForANewSearch();
			if (sender.Equals(checkEditShowFavorites))
			{
				if (checkEditShowFavorites.Checked)
				{
					if (comboBoxSearchByColumn.Properties.Items.IndexOf("Site") == -1)
						comboBoxSearchByColumn.Properties.Items.Add("Site");
					if (comboBoxSearchByColumn.Properties.Items.IndexOf("State") == -1)
						comboBoxSearchByColumn.Properties.Items.Add("State");
					checkEditPublicOnly.Checked = !checkEditShowFavorites.Checked;
					SetSearchParams(false);
				}
			}
			else
			{
				if (sender.Equals(checkEditPublicOnly))
				{
					if (checkEditPublicOnly.Checked)
					{
						//Public rooms additional work
						/*if (comboBoxSearchByColumn.Properties.Items.IndexOf("Site") != -1)
							comboBoxSearchByColumn.Properties.Items.Remove("Site");
						if (comboBoxSearchByColumn.Properties.Items.IndexOf("State") != -1)
							comboBoxSearchByColumn.Properties.Items.Remove("State");

						comboBoxSearchByColumn.SelectedIndex = comboBoxSearchByColumn.Properties.Items.IndexOf(
								((ColumnView)gridControl_RoomsInfoNew.MainView).Columns.ColumnByFieldName("City").GetTextCaption());
						 */
						//emptySpaceItem1.AppearanceItemCaption.BackColor = SystemColors.Info;
						//emptySpaceItem1.AppearanceItemCaption.ForeColor = Color.DarkBlue;
						//textEditSearchFor.Properties.NullValuePrompt = "Enter location here, then click Search";
						//textEditSearchFor.Properties.NullValuePromptShowForEmptyValue = true;
						//emptySpaceItem1.Text = " " + "Enter location here, then click Search";

						checkEditShowFavorites.Checked = !checkEditPublicOnly.Checked;
						SetSearchParams(true);
						if(PublicCountriesDict.Count == 0)
						{
							GetPublicCountriesResponse publicCountries = MyVrmService.Service.GetPublicCountries();
							PublicCountriesDict = publicCountries.Countries;
							foreach (var item in PublicCountriesDict)
							{
								countryComboBox.Properties.Items.Add(item.Value);
							}
						}
					}
					else //for Private
					{
						/*
						if (comboBoxSearchByColumn.Properties.Items.IndexOf("Site") == -1)
							comboBoxSearchByColumn.Properties.Items.Add("Site");
						if (comboBoxSearchByColumn.Properties.Items.IndexOf("State") == -1)
							comboBoxSearchByColumn.Properties.Items.Add("State");
						 * */
						SetSearchParams(false);
					}
				}
			}

			//stateComboBox.Properties.NullText = checkEditPublicOnly.Checked ? "[please select]" : string.Empty;
			countryComboBox.Properties.NullText = checkEditPublicOnly.Checked ? "[please select]" : string.Empty;
			if (!checkEditPublicOnly.Checked)
			{
				stateComboBox.EditValue = string.Empty;
				countryComboBox.EditValue = null;//string.Empty;

				stateComboBox.SelectedIndex = -1;
				stateComboBox.Enabled = false;
			}

			countryComboBox.Enabled = checkEditPublicOnly.Checked;
			countryComboBox.Properties.NullValuePromptShowForEmptyValue = checkEditPublicOnly.Checked;
			//stateComboBox.Enabled = checkEditPublicOnly.Checked;
			UIForANewSearch();

            if (!checkEditPublicOnly.Checked )
            {
                textEditSearchFor.Properties.NullValuePromptShowForEmptyValue = false;
                GetNextPage();
            }

			if (checkEditPublicOnly.Checked)
				Invoke(_displayProgress, "Select your desired location from the drop lists above and click Search.");
		}

		private void roomsCalendarButton_Click(object sender, EventArgs e)
		{
			List<RoomId> roomList = new List<RoomId>();

			foreach (/*string*/RoomFlat room in roomListBoxControl.Items)
			{
				roomList.Add(new RoomId(room.Id.ToString()));
				////RoomFlat found = RoomList.Find(aRoom => aRoom == room);// .Name
				//if (RoomList.Contains(room))//found != null)
				//{
				//    roomList.Add(new RoomId(room.Id.ToString()));// found.Id.ToString()));
				//}
				//else
				//{
				//    foreach (RoomId pre in _preselectedRoomIds)
				//    {
				//        Room cachedRoom = MyVrmAddin.Instance.GetRoomFromCache(new RoomId(pre.Id));
				//        if (cachedRoom != null )//&& cachedRoom.Id/*Name*/ == room.Id)
				//        {
				//            roomList.Add(new RoomId(cachedRoom.Id.ToString()));
				//            break;
				//        }
				//    }
				//}
			}
			if (roomList.Count > 0)
			{
				using (var dlg = new CalendarForSelectedRooms(roomList))
				{
					dlg.RoomMode = RoomsCalendarDialog.RoomDisplayMode.All;
					dlg.ShowDialog(this);
				}
			}
		}

		public virtual ReadOnlyCollection<ManagedRoom> GetRooms()
		{
			MyVrmAddin.TraceSource.TraceInfoEx("GetAvailableRooms In");
			ReadOnlyCollection<ManagedRoom> availableRooms = MyVrmService.Service.GetAvailableRooms(Conf.Conference.StartDate, Conf.Conference.Duration, Conf.Conference.TimeZone, Conf.Conference.IsRecurring,
				MediaTypeFilter.All, Conf.Conference.Type, Conf.Conference.Id);
			MyVrmAddin.TraceSource.TraceInfoEx("GetAvailableRooms Out");
			return availableRooms;
		}

		RoomFlat TransformRoomListWithCache(Room room)
		{
			RoomFlat roomFlat = new RoomFlat();
			Room cachedRoom = MyVrmAddin.Instance.GetRoomFromCache(room.Id);

			if (cachedRoom != null)
			{
				if (cachedRoom.LastModifiedDate != room.LastModifiedDate)
					cachedRoom = MyVrmService.Service.GetRoom(room.Id);
				roomFlat = TransformRoomList(cachedRoom);
			}
			return roomFlat;
		}

		RoomFlat TransformRoomList(Room room)
		{
			MyVrmAddin.TraceSource.TraceInfoEx("TransformRoomList In {0}", room.Name);
			RoomFlat roomFlat = new RoomFlat();
			roomFlat.Id = room.Id;
			roomFlat.Name = room.Name;
			roomFlat.ZipCode = room.ZipCode;
			roomFlat.City = room.City;
			roomFlat.Capacity = room.Capacity;

			if (room.IsPublic)
			{
				roomFlat.Country = room.PublicRoomData.Country == 0 ? string.Empty :
					/*ManageRooms.ManageRoomStaticData.CountriesDict*/PublicCountriesDict.FirstOrDefault(a => a.Key == room.PublicRoomData.Country).Value;

				roomFlat.State = string.Empty;
				if (room.PublicRoomData.State != 0)
				{
					var state = /*ManageRooms.ManageRoomStaticData.StatesDict*/PublicCountryStatesDict.FirstOrDefault(
						a => a.Value == room.PublicRoomData.Country && a.Key.First == room.PublicRoomData.State);
					if (state.Key != null)
						roomFlat.State = state.Key.Second;
				}
			}
			else
			{
				roomFlat.State = room.State;
				roomFlat.Country = room.Country;
			}

			roomFlat.Phone = room.Phone;
			roomFlat.Approval = (room.Approval != 0);
			roomFlat.Site = room.MiddleTierName;
			roomFlat.Type = LocalizedDescriptionAttribute.FromEnum(room.Media.GetType(), room.Media);
			roomFlat.Address = room.StreetAddress1;
			roomFlat.Assistant = room.AssistantName;
			roomFlat.Floor = room.Floor;

			if(room.PublicRoomData.IsISDNCapable == 1)
				roomFlat.pubISDNIP = "ISDN";
			if(room.PublicRoomData.IsIPCapable == 1)
			{
				if (room.PublicRoomData.IsISDNCapable == 1)
					roomFlat.pubISDNIP += " & "; 
				roomFlat.pubISDNIP += "IP";
			}
			//if (!string.IsNullOrEmpty(room.PublicRoomData.isdnAddress))
			//    roomFlat.pubISDNIP = "ISDN";
			//if (!string.IsNullOrEmpty(room.PublicRoomData.ipAddress))
			//{
			//    if (!string.IsNullOrEmpty(roomFlat.pubISDNIP))
			//        roomFlat.pubISDNIP += " or ";
			//    roomFlat.pubISDNIP += "IP";
			//}
			roomFlat.pubSDHD = room.PublicRoomData.IsHDCapable == 1 ? "HD" : "SD"; //???
			roomFlat.pubBusinessHrsPrice = room.PublicRoomData.OfficeHoursCost;
			roomFlat.pubCurrency = room.PublicRoomData.CurrencyType;
			/*temporary?????*/
			if (room.IsPublic)
			{
				
				if (room.RoomImages.Count > 0)
				{
					using (var imageStream = room.RoomImages[0].AsStream())
					{
						roomFlat.Picture = Image.FromStream(imageStream);
					}
				}
				else
				{
					//room.PublicRoomData.ImgLink = "http://yandex.st/www/1.301/yaru/i/logo.png";
					
					if (!string.IsNullOrEmpty(room.PublicRoomData.ImgLink))
					{
						try
						{
							Room cachedRoom = MyVrmAddin.Instance.GetRoomOnlyFromCache(room.Id);
							if (cachedRoom != null && cachedRoom.RoomImages.Count > 0) //get cached image
							{
								using (Stream imageStream = cachedRoom.RoomImages[0].AsStream())
								{
									roomFlat.Picture = Image.FromStream(imageStream);
									imageStream.Close();
									room.RoomImages = cachedRoom.RoomImages;
								}
							}
							else
							{
								MyVrmAddin.TraceSource.TraceInfoEx("Get {0}", room.PublicRoomData.ImgLink);
								WebRequest req = WebRequest.Create(room.PublicRoomData.ImgLink);
								using (WebResponse response = req.GetResponse())
								{
									if (response != null)
									{
										using (Stream stream = response.GetResponseStream())
										{
											if (stream != null)
											{
												roomFlat.Picture = Image.FromStream(stream);
												stream.Close();
												using (MemoryStream ms = new MemoryStream())
												{
													roomFlat.Picture.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);

													//Set image to put it in the cache 
													var images = new List<MyVrm.WebServices.Data.Image>();
													var image = new WebServices.Data.Image(room.PublicRoomData.ImgLink,
																						   ms.ToArray());
													images.Add(image);
													room.RoomImages.SetImages(images);
													ms.Close();
												}
											}
										}
										response.Close();
									}
								}
								MyVrmAddin.TraceSource.TraceInfoEx("Received {0}", room.PublicRoomData.ImgLink);
							}
						}
						catch (Exception exception)
						{
							MyVrmAddin.TraceSource.TraceWarning("{0} image assign is failed : {1}", room.PublicRoomData.ImgLink, exception.Message);
						}
					}
				}
			}/**/
			roomFlat.pubAutomated = room.PublicRoomData.IsAutomatic == 1 ? "Automated" : "Non-Automated";
			roomFlat.IsPublic = room.IsPublic;
			roomFlat.IsFavorite = UserProfile.FavoriteRooms.FirstOrDefault(r => r.Id == room.Id.Id) != null;

			MyVrmAddin.TraceSource.TraceInfoEx("TransformRoomList Out {0}", room.Name);

			return roomFlat;
		}

		//
		

		void GetNextPage()
		{
			GetNextPage_FirstAndOthers();
			return;
		}

		private void RoomSelectionNew_Load(object sender, EventArgs e)
		{
			//init
			MyVrmAddin.TraceSource.TraceInfoEx("RoomSelectionNew_Load In");
			RoomList = new List<RoomFlat>();
			_currRecFilter = string.Empty;
			_currRecPageNo = 0;
			_currTopRowNumber = 0;
			_currFocusedRowNumber = 0;
			_rowNumberPerPage = (gridControl_RoomsInfoNew.MainView).ViewRect.Height / ((ColumnView)gridControl_RoomsInfoNew.MainView).Columns[0].AppearanceCell.FontHeight - 1;
			if (_rowNumberPerPage <= 0)
				_rowNumberPerPage = 15;
			comboBoxSearchByColumn.SelectedIndex = //0;
										comboBoxSearchByColumn.Properties.Items.IndexOf(
				((ColumnView)gridControl_RoomsInfoNew.MainView).Columns.ColumnByFieldName("Name").GetTextCaption());

			foreach (RoomId pre in _preselectedRoomIds)
			{

				RoomFlat found = RoomList.Find(aRoom => aRoom.Id.ToString() == pre.Id);
				if (found != null)
				{
					roomListBoxControl.Items.Add(found);//.Name);
				}
				else
				{
					Room cachedRoom = MyVrmAddin.Instance.GetRoomFromCache(new RoomId(pre.Id));
					if (cachedRoom != null)
					{
						roomListBoxControl.Items.Add(TransformRoomList(cachedRoom));//cachedRoom.Name);
					}
				}
			}

			gridControl_RoomsInfoNew.ToolTipController = _toolTipController;

			foreach (EditorButton btn in repositoryItemButtonEdit1.Buttons)
			{
				btn.ToolTip = Strings.AdvancedInfoToolTipText;
			}
			foreach (EditorButton btn in repositoryItemButtonEdit2.Buttons)
			{
				btn.ToolTip = Strings.AdvancedInfoToolTipText;
			}

			graphicColPublicPicture.LayoutViewField.SizeConstraintsType = SizeConstraintsType.Custom;
			graphicColPublicShowDetails.LayoutViewField.SizeConstraintsType = SizeConstraintsType.Custom;
			graphicColPublicName.LayoutViewField.SizeConstraintsType = SizeConstraintsType.Custom;
			graphicColPublicziipCode.LayoutViewField.SizeConstraintsType = SizeConstraintsType.Custom;
			graphicColPublicCity.LayoutViewField.SizeConstraintsType = SizeConstraintsType.Custom;
			graphicColPublicBusinessHrs.LayoutViewField.SizeConstraintsType = SizeConstraintsType.Custom;
			graphicColPublicISDNIP.LayoutViewField.SizeConstraintsType = SizeConstraintsType.Custom;
			graphicColPublicSDHD.LayoutViewField.SizeConstraintsType = SizeConstraintsType.Custom;
			graphicColPublicCapacity.LayoutViewField.SizeConstraintsType = SizeConstraintsType.Custom;
			graphicColPublicAdd.LayoutViewField.SizeConstraintsType = SizeConstraintsType.Custom;
			emptySpaceItem4.SizeConstraintsType = SizeConstraintsType.Custom;
			emptySpaceItem5.SizeConstraintsType = SizeConstraintsType.Custom;
			emptySpaceItem6.SizeConstraintsType = SizeConstraintsType.Custom;
			emptySpaceItem4.MinSize = new Size(1, 1);
			emptySpaceItem5.MinSize = new Size(1, 1);
			emptySpaceItem6.MinSize = new Size(1, 1);

			graphicColPublicName.LayoutViewField.Size = new Size(graphicColPublicName.LayoutViewField.Size.Width, 25);
			graphicColPublicziipCode.LayoutViewField.Size = new Size(graphicColPublicName.LayoutViewField.Size.Width, 25);
			graphicColPublicCity.LayoutViewField.Size = new Size(graphicColPublicName.LayoutViewField.Size.Width, 25);
			graphicColPublicBusinessHrs.LayoutViewField.Size = new Size(graphicColPublicName.LayoutViewField.Size.Width, 25);
			graphicColPublicISDNIP.LayoutViewField.Size = new Size(graphicColPublicName.LayoutViewField.Size.Width, 25);
			graphicColPublicSDHD.LayoutViewField.Size = new Size(graphicColPublicName.LayoutViewField.Size.Width, 25);
			graphicColPublicCapacity.LayoutViewField.Size = new Size(graphicColPublicName.LayoutViewField.Size.Width, 25);

			/**/

			//foreach (var item in ManageRoomStaticData.CountriesDict)
			//{
			//    countryComboBox.Properties.Items.Add(item.Value);
			//}
			//countryComboBox.SelectedIndex = -1;
			/**/
			textEditSearchFor.Properties.NullValuePromptShowForEmptyValue = false;

			stateComboBox.Properties.NullText = string.Empty; // checkEditPublicOnly.Checked ? "[please select]" : string.Empty;
			countryComboBox.Properties.NullText = string.Empty; // checkEditPublicOnly.Checked ? "[please select]" : string.Empty;

			countryComboBox.Enabled = checkEditPublicOnly.Checked;
			stateComboBox.Enabled = checkEditPublicOnly.Checked;
			SetSearchParams(false);
			GetNextPage();
			MyVrmAddin.TraceSource.TraceInfoEx("RoomSelectionNew_Load Out");
			//searchBn.Enabled = false;
			//searchBn.Focus();
		}

		private RoomId[] GetSelected()
		{
			RoomId[] ret = new RoomId[roomListBoxControl.Items.Count];
			for (var i = 0; i < roomListBoxControl.Items.Count; i++)
			{
				//RoomFlat found = RoomList.Find(aRoom => aRoom == roomListBoxControl.Items[i]);//.Name == (string)roomListBoxControl.Items[i]);
				if (RoomList.Contains<RoomFlat>((RoomFlat) roomListBoxControl.Items[i])) //(found != null)
				{
					ret[i] = new RoomId(((RoomFlat)roomListBoxControl.Items[i]).Id.ToString());//found.Id.ToString());
				}
				else
				{
					foreach (RoomId pre in _preselectedRoomIds)
					{
						Room cachedRoom = MyVrmAddin.Instance.GetRoomFromCache(new RoomId(pre.Id));
						if (cachedRoom != null )//&& cachedRoom.Id == ((RoomFlat)(roomListBoxControl.Items[i])).Id) //.Name == (string)roomListBoxControl.Items[i])
						{
							ret[i] = new RoomId(cachedRoom.Id.ToString());
							break;
						}
					}
				}
			}
			return ret;
		}

		void StopWorkers()
		{
			MyVrmAddin.TraceSource.TraceInfoEx("StopWorkers In");
			emptySpaceItem1.AppearanceItemCaption.ForeColor = Color.Empty;
			emptySpaceItem1.AppearanceItemCaption.BackColor = SystemColors.Info;
			if (backgroundWorkerFirstPage.IsBusy)
				emptySpaceItem1.Text = " Cancelling previous search request..."; //"Server request is cancelling. Please wait!";

			while (backgroundWorkerFirstPage.IsBusy)
			{
				backgroundWorkerFirstPage.CancelAsync();
				Thread.Sleep(100);
				Application.DoEvents();
			}
			
			emptySpaceItem1.AppearanceItemCaption.BackColor = Color.Empty;
			emptySpaceItem1.Text = " ";
			MyVrmAddin.TraceSource.TraceInfoEx("StopWorkers Out");
		}


		void RoomSelectionNew_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			layoutPublicBasic.SaveLayoutToXml(Path.Combine(MyVrmAddin.LocalUserAppDataPath, "RoomSelectionNew_layoutPublicBasic.xml"));
			layoutPublicGraphic.SaveLayoutToXml(Path.Combine(MyVrmAddin.LocalUserAppDataPath, "RoomSelectionNew_layoutPublicGraphic.xml"));
			layoutPrivateBasicView.SaveLayoutToXml(Path.Combine(MyVrmAddin.LocalUserAppDataPath, "RoomSelectionNew_layoutPrivateBasicView.xml"));
			layoutPrivateAdvancedView.SaveLayoutToXml(Path.Combine(MyVrmAddin.LocalUserAppDataPath, "RoomSelectionNew_layoutPrivateAdvancedView.xml"));

			StopWorkers();
		}

		void RoomSelectionNew_Closed(object sender, EventArgs e)
		{
			if (CloseReason == CloseDlgReason.None)
				CloseReason = DialogResult == DialogResult.OK ? CloseDlgReason.CloseOK : CloseDlgReason.CloseCancel;
		}

		void SetPublicBasicLayout(object sender, EventArgs e)
		{
			_viewPublicPopupMenu.Items[(int)PublicViewMode.Basic].Image = Resources.Checkbox;
			_viewPublicPopupMenu.Items[(int)PublicViewMode.Graphic].Image = null;
			layoutPublicBasic.OptionsView.ShowGroupPanel = false;
			gridControl_RoomsInfoNew.MainView = layoutPublicBasic;
		}
		void SetPublicGraphicLayout(object sender, EventArgs e)
		{
			_viewPublicPopupMenu.Items[(int)PublicViewMode.Basic].Image = null;
			_viewPublicPopupMenu.Items[(int)PublicViewMode.Graphic].Image = Resources.Checkbox;
			gridControl_RoomsInfoNew.MainView = layoutPublicGraphic;
		}
		void SetPrivateBasicLayout(object sender, EventArgs e)
		{
			_viewPrivatePopupMenu.Items[(int)PrivateViewMode.Basic].Image = Resources.Checkbox;
			_viewPrivatePopupMenu.Items[(int)PrivateViewMode.Advanced].Image = null;
			gridControl_RoomsInfoNew.MainView = layoutPrivateBasicView;
		}
		void SetPrivateAdvancedLayout(object sender, EventArgs e)
		{
			_viewPrivatePopupMenu.Items[(int)PrivateViewMode.Basic].Image = null;
			_viewPrivatePopupMenu.Items[(int)PrivateViewMode.Advanced].Image = Resources.Checkbox;
			gridControl_RoomsInfoNew.MainView = layoutPrivateAdvancedView;
		}

		private void SetPopupMenu(bool forPublic)
		{
			if (forPublic)
				dropBnChangeView.DropDownControl = _viewPublicPopupMenu;
			else
				dropBnChangeView.DropDownControl = _viewPrivatePopupMenu;
		}

		private void dropBnChangeView_ArrowButtonClick(object sender, EventArgs e)
		{
			SetPopupMenu(checkEditPublicOnly.Checked);
		}

		private void dropBnChangeView_MouseClick(object sender, MouseEventArgs e)
		{
			SetPopupMenu(checkEditPublicOnly.Checked);
			dropBnChangeView.ShowDropDown();
		}

		private void bnRemove_Click(object sender, EventArgs e)
		{
			RemoveRoomFromConference();
		}

		private void gridControl_RoomsInfoNew_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
		{
		}

		
		void layoutPrivateAdvancedView_TopRowChanged(object sender, System.EventArgs e)
		{
		}

		private void layoutPrivateBasicView_TopRowChanged(object sender, EventArgs e)
		{
		}

		private void layoutPublicBasic_TopRowChanged(object sender, EventArgs e)
		{
		}

		private void layoutPublicGraphic_VisibleRecordIndexChanged(object sender, DevExpress.XtraGrid.Views.Layout.Events.LayoutViewVisibleRecordIndexChangedEventArgs e)
		{
		}

		private void bnAdd_Click(object sender, EventArgs e)
		{
			AddRoomToConference();
		}

		void UIForANewSearch()
		{
			RoomList.Clear();
			gridControl_RoomsInfoNew.DataSource = RoomList;
			_currRecPageNo = 0;
			_currRecFilter = textEditSearchFor.Text;
			_currTopRowNumber = 0;
			_currFocusedRowNumber = 0;

			StopWorkers();
			AdjustUI();
			
		}
		private void searchBn_Click(object sender, EventArgs e)
		{
			textEditSearchFor.Properties.NullValuePromptShowForEmptyValue = false;
			UIForANewSearch();
			GetNextPage();
		}

		public delegate void UpdDelegate(string roomName);
		public delegate void UpdCompleteDelegate();

		private readonly UpdDelegate _updPageDelegate;
		private readonly UpdDelegate _displayProgress;

		private void UpdateRoomPage(string text) //delegate
		{
			Application.DoEvents();
			if (gridControl_RoomsInfoNew.MainView != layoutPublicGraphic)
			{
				_currFocusedRowNumber = ((GridView)(gridControl_RoomsInfoNew.MainView)).FocusedRowHandle ;
				_currTopRowNumber = ((GridView)(gridControl_RoomsInfoNew.MainView)).TopRowIndex ;
			}
			else
			{
				_currFocusedRowNumber = layoutPublicGraphic.FocusedRowHandle ;
				_currTopRowNumber = layoutPublicGraphic.VisibleRecordIndex ;
			}
			gridControl_RoomsInfoNew.DataSource = RoomList;

			int i = 0;
			int.TryParse(text, out i);
			if (text.Trim().Length > 0 && i > 0 && _totalRecPages > 0)
			{
				emptySpaceItem1.AppearanceItemCaption.ForeColor = Color.Empty;
				emptySpaceItem1.Text = " " + "Searching..." + //WinForms.Strings.ProgressBarMsgTxt +
										string.Format(" Page {0} of {1} has been displayed (room(s) {2} of {3})", text,
															   _totalRecPages, ((List<RoomFlat>)gridControl_RoomsInfoNew.DataSource).Count,
															   _totalRecNumber);
				Application.DoEvents();
			}
			ProcessSearch();
			if (gridControl_RoomsInfoNew.MainView != layoutPublicGraphic)
			{
				((GridView)(gridControl_RoomsInfoNew.MainView)).FocusedRowHandle = _currFocusedRowNumber;
				((GridView)(gridControl_RoomsInfoNew.MainView)).TopRowIndex = _currTopRowNumber;
			}
			else
			{
				layoutPublicGraphic.FocusedRowHandle = _currFocusedRowNumber;
				layoutPublicGraphic.VisibleRecordIndex = _currTopRowNumber;
			}
		}

		private void DisplayProgress(string text) //delegate
		{
			emptySpaceItem1.AppearanceItemCaption.BackColor = text.Trim().Length == 0 ? Color.Empty : SystemColors.Info;
			emptySpaceItem1.Text = " " + text;
			Application.DoEvents();
		}
		//-------------------------------
		void GetNextPage_FirstAndOthers()
		{
			MyVrmAddin.TraceSource.TraceInfoEx("GetNextPage_FirstAndOthers In");
			Cursor cursor = Cursor.Current;
			Cursor.Current = Cursors.WaitCursor;

			try
			{
				while (backgroundWorkerFirstPage.IsBusy)
				{
					backgroundWorkerFirstPage.CancelAsync();
					Thread.Sleep(100);
					Application.DoEvents();
				}
				_currRecPageNo = 1;
				Invoke(_displayProgress, "Searching...");//string.Format(WinForms.Strings.ProgressBarMsgTxt, _currRecPageNo));
				backgroundWorkerFirstPage.RunWorkerAsync(_currRecPageNo);
			}
			finally
			{
				Cursor.Current = cursor;
				gridControl_RoomsInfoNew.DataSource = RoomList;
				MyVrmAddin.TraceSource.TraceInfoEx("GetNextPage_FirstAndOthers Out");
			}
		}
		//--
		//enum DisplayMode
		//{
		//    PerRecord,
		//    PerPage
		//}

		//private DisplayMode displayMode = DisplayMode.PerPage;

		private void backgroundWorkerFirstPage_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
		{
			MyVrmAddin.TraceSource.TraceInfoEx("backgroundWorkerFirstPage_DoWork() : GetConfAvailableRoom call");
			_currRecPageNo = (int)e.Argument;
			if (!backgroundWorkerFirstPage.CancellationPending )
			{
				bool _GetAllData = true;
				string searchBy = comboBoxSearchByColumn.SelectedItem.ToString();
				string sortBy = ((ColumnView)gridControl_RoomsInfoNew.MainView).SortInfo.View.SortedColumns[0].FieldName;
				string country = countryComboBox.SelectedIndex == -1 || !checkEditPublicOnly.Checked ? string.Empty : countryComboBox.Properties.Items[countryComboBox.SelectedIndex].ToString();
				string state = stateComboBox.SelectedIndex == -1 || !checkEditPublicOnly.Checked ? string.Empty : stateComboBox.Properties.Items[stateComboBox.SelectedIndex].ToString();
				int countryId = 0;
				
				if( checkEditPublicOnly.Checked )
				{
					if (/*ManageRoomStaticData.CountriesDict*/PublicCountriesDict.ContainsValue(country))
					{
						KeyValuePair<int, string> found = /*ManageRoomStaticData.CountriesDict*/PublicCountriesDict.FirstOrDefault(a => a.Value == country);
						countryId = found.Key;
					}
					searchBy = "City";
					_currRecFilter = comboBoxSearchByColumn.SelectedIndex > 0 ? comboBoxSearchByColumn.SelectedItem.ToString() : string.Empty;
				}

				int stateId = 0;
				if(countryId != 0)
				{
					if(checkEditPublicOnly.Checked )
					{
						var found = /*ManageRoomStaticData.StatesDict*/PublicCountryStatesDict.FirstOrDefault(
							a => a.Value == countryId && a.Key.Second == state);
						if (found.Key != null)
						{
							stateId = found.Key.First;
						}
					}
				}

				GetConfAvailableRoomResponse ret =
					MyVrmService.Service.GetConfAvailableRoom(Conf.Conference.StartDate, Conf.Conference.Duration,
															  Conf.Conference.TimeZone, Conf.Conference.IsRecurring,
															  MediaTypeFilter.All, Conf.Conference.Type, Conf.Conference.Id,
															  _currRecFilter, _currRecPageNo, searchBy, sortBy,
															  checkEditPublicOnly.Checked ? 1 : 0, _GetAllData,
															  country, state, countryId, stateId);

				_currRecPageNo = ret.PageNo;
				_totalRecPages = ret.TotalPages;
				_totalRecNumber = ret.TotalNumber;
				List<Room> Rooms = ret.Rooms;
				if (backgroundWorkerFirstPage.CancellationPending )
					e.Cancel = true;
				if (Rooms != null && Rooms.Count > 0 && !backgroundWorkerFirstPage.CancellationPending )
				{
					foreach (var room in Rooms)
					{
						if (backgroundWorkerFirstPage.CancellationPending)
						{
							e.Cancel = true;
							break;
						}
						if (RoomList.FirstOrDefault(a => a.Id == room.Id) == null)
						{
							RoomList.Add(_GetAllData ? TransformRoomList(room) : TransformRoomListWithCache(room));
							
							if (room.IsPublic && _GetAllData)
							{
								MyVrmAddin.TraceSource.TraceInfoEx("backgroundWorker_DoWork() : UpdateRoomCache call");
								MyVrmAddin.Instance.UpdateRoomCache(room);
								MyVrmAddin.TraceSource.TraceInfoEx("backgroundWorker_DoWork() : UpdateRoomCache ret");
							}
						}
						//if(displayMode == DisplayMode.PerRecord)
						//    Invoke(_updPageDelegate, _currRecPageNo.ToString());
					}
				}
			}
			else
			{
				e.Cancel = true;
			}
			MyVrmAddin.TraceSource.TraceInfoEx("backgroundWorkerFirstPage_DoWork() : GetConfAvailableRoom ret");
		}

		private void backgroundWorkerFirstPage_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
		{
			MyVrmAddin.TraceSource.TraceInfoEx("backgroundWorkerFirstPage_RunWorkerCompleted In");
			if (!backgroundWorkerFirstPage.CancellationPending && !e.Cancelled)
			{
				Invoke(_updPageDelegate, _currRecPageNo.ToString());

				if (_totalRecPages > 1 && _currRecPageNo < _totalRecPages && !backgroundWorkerFirstPage.CancellationPending &&
					!e.Cancelled ) 
				{
					backgroundWorkerFirstPage.RunWorkerAsync(++_currRecPageNo);
				}
				else
				{
					if (_totalRecPages == 0)
					{
						Invoke(_displayProgress, "No rooms found.");
					}
					else
					{
						if (_currRecPageNo == _totalRecPages)
						{
							Invoke(_displayProgress, "Search complete.");
						}
					}
				}
			}
			MyVrmAddin.TraceSource.TraceInfoEx("backgroundWorkerFirstPage_RunWorkerCompleted Out");
		}

		void FillCities()
		{
			PublicCountryStateCitiesDict.Clear();
			comboBoxSearchByColumn.Properties.Items.Clear();
			comboBoxSearchByColumn.Enabled = false;
			comboBoxSearchByColumn.EditValue = string.Empty;
			KeyValuePair<int, string> country = PublicCountriesDict.FirstOrDefault(a => a.Value == (string)countryComboBox.EditValue);
			int countryId = country.Key;
			
			var found = PublicCountryStatesDict.FirstOrDefault(a => a.Value == countryId && a.Key.Second ==
				(string)stateComboBox.EditValue);
			//KeyValuePair<int, string> state = found.Key != null ? found.Key.First : 0;
			int stateId = found.Key != null ? found.Key.First : 0;//state.Key;
			if (stateId == 0 && countryId != 1 /*Australia*/ && countryId != 3 /*USA*/  && countryId != 248 /*England*/ || stateId > 0)
			{
				GetPublicCountryStateCitiesResponse publicCities = MyVrmService.Service.GetPublicCountryStateCities(countryId,
				                                                                                                    stateId);
				PublicCountryStateCitiesDict = publicCities.Cities;

				foreach (var city in PublicCountryStateCitiesDict)
				{
					comboBoxSearchByColumn.Properties.Items.Add(city.Value);
				}
				comboBoxSearchByColumn.Properties.Items.Add("[not specified]");
				comboBoxSearchByColumn.SelectedIndex = 0;
				comboBoxSearchByColumn.Enabled = true;
				comboBoxSearchByColumn.Focus();
				comboBoxSearchByColumn.ShowPopup();
			}
		}

		//private static string NoItems = "[no items]";
		private void countryComboBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			ComboBoxEdit combo = sender as ComboBoxEdit;
			if (combo != null && checkEditPublicOnly.Checked)
			{
				KeyValuePair<int, string> found = /*ManageRoomStaticData.CountriesDict*/PublicCountriesDict.FirstOrDefault(a => a.Value == (string)combo.EditValue);
				int iNdx = found.Key;//: -1?
				stateComboBox.Enabled = true;
				stateComboBox.EditValue = string.Empty;
				stateComboBox.Properties.Items.Clear();
				int countryId = iNdx;
				if (countryId != 1 /*Australia*/ && countryId != 3 /*USA*/  && countryId != 248 /*England*/ )
				{
					stateComboBox.Enabled = false;
					FillCities();
					if (PublicCountryStatesDict.Count == 0 || !PublicCountryStatesDict.Values.Contains(iNdx))
					{
						GetPublicCountryStatesResponse publicStates = MyVrmService.Service.GetPublicCountryStates(iNdx);
						foreach (var state in publicStates.States)
						{
							PublicCountryStatesDict.Add(new Pair<int, string>(state.Key, state.Value), iNdx);
						}
					}
				}
				else
				{
					if (PublicCountryStatesDict.Count == 0 || !PublicCountryStatesDict.Values.Contains(iNdx))
					{
						GetPublicCountryStatesResponse publicStates = MyVrmService.Service.GetPublicCountryStates(iNdx);
						foreach (var state in publicStates.States)
						{
							PublicCountryStatesDict.Add(new Pair<int, string>(state.Key, state.Value), iNdx);
						}
					}

					var extracted = /*ManageRoomStaticData.StatesDict*/ PublicCountryStatesDict.Where(a => a.Value == iNdx).ToList();
					foreach (var state in extracted)
					{
						stateComboBox.Properties.Items.Add(state.Key.Second);
					}
					stateComboBox.Properties.Items.Add("[not specified]");
					if (stateComboBox.Properties.Items.Count == 0)
					{
						//stateComboBox.EditValue = NoItems; //for this moment - only for Jamaica
						stateComboBox.Properties.NullValuePromptShowForEmptyValue = false;
					}
					else
					{
						stateComboBox.SelectedIndex = 0; //-1;
						stateComboBox.Properties.NullValuePromptShowForEmptyValue = true;
						//countryComboBox.Focused 
						stateComboBox.Focus();
						stateComboBox.ShowPopup();
					}
					searchBn.Enabled = true;
				}
			}
		}

		private void stateComboBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (checkEditPublicOnly.Checked)
				FillCities();
		}
	}
}
