﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.WebServices.Data
{
    internal class GetCustomAttributeDefinitionsRequest : ServiceRequestBase<GetCustomAttributeDefinitionsResponse>
    {
        public GetCustomAttributeDefinitionsRequest(MyVrmService service) : base(service)
        {
        }

        internal override string GetXmlElementName()
        {
            return "CustomAttribute";
        }

        internal override string GetCommandName()
        {
            return "GetCustomAttributes";
        }

        internal override string GetResponseXmlElementName()
        {
            return "CustomAttributesList";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            // DeptID=0 return all custom attributes
            writer.WriteElementValue(XmlNamespace.NotSpecified, "DeptID", 0);
        }
    }
}