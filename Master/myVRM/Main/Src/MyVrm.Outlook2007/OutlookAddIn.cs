﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Office.Interop.Outlook;
using Microsoft.Office.Tools;
using Microsoft.VisualStudio.Tools.Applications.Runtime;

namespace MyVrm.Outlook
{
    public abstract class OutlookAddIn : AddIn
    {
        private CustomFormManager _customFormManager;
        private CommandBarManager _commandBarManager;


        protected OutlookAddIn(IHostItemProvider hostItemProvider, IRuntimeServiceProvider runtimeServiceProvider, string primaryCookie, object container, string identifier)
            : base(hostItemProvider, runtimeServiceProvider, primaryCookie, container, identifier)
        {
            
        }
        [EditorBrowsable(EditorBrowsableState.Never)]
        public CustomFormReadOnlyCollection GetCustomForms()
        {
            var forms = new CustomFormReadOnlyCollection();
            forms.InitInnerList(CustomFormManager.CustomForms);
            return forms;
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public TCustomFormCollection GetCustomForms<TCustomFormCollection>(Inspector inspector) where TCustomFormCollection : CustomFormReadOnlyCollection, new()
        {
            IList<CustomFormControl> list;
            var forms = Activator.CreateInstance<TCustomFormCollection>();
            if (_customFormManager != null && CustomFormManager.CustomForms.TryGetValue(inspector, out list))
            {
                forms.InitInnerList(list);
            }
            return forms;
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public CommandBarReadOnlyCollection GetCommandBars()
        {
            var commandBars = new CommandBarReadOnlyCollection();
            commandBars.InitInnerList(CommandBarManager.CommandBars);
            return commandBars;
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public TCommandBarCollection GetCommandBars<TCommandBarCollection>(Inspector inspector) where TCommandBarCollection : CommandBarReadOnlyCollection, new()
        {
            IList<OfficeCommandBar> list;
            var commandBars = Activator.CreateInstance<TCommandBarCollection>();
            if (_commandBarManager != null && CommandBarManager.CommandBars.TryGetValue(inspector, out list))
            {
                commandBars.InitInnerList(list);
            }
            return commandBars;
        }

        protected CustomFormManager CustomFormManager
        {
            get { return _customFormManager ?? (_customFormManager = new CustomFormManager(this)); }
        }

        protected CommandBarManager CommandBarManager
        {
            get { return _commandBarManager ?? (_commandBarManager = new CommandBarManager(this)); }
        }
    }
}
