﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;


namespace MyVrm.WebServices.Data
{
      internal class GetEmailContentRequest : ServiceRequestBase<GetEmailContentResponse>
      {
        public GetEmailContentRequest(MyVrmService service) : base(service)
        {
        }
        public GetEmailContentRequest(MyVrmService service, ConferenceType confType)
            : base(service)
        {
            ConfTyp1 = confType;//104556
        }
        public ConferenceId contentid { get; set; }
       // public string ICalId { get; set; }
       
        public string subject { get; set; }
        public string placeholders { get; set; }
        public string emailtype { get; set; }
        public string emaillanguage { get; set; }
        public string body { get; set; }
        public string emailcontent { get; set; }
        public string Conforgin { get; set; }
        public ConferenceType ConfTyp1 { get; set; }//104556
        //public string  { get; set; }

        #region Overrides of ServiceRequestBase<GetEmailContentResponse>

        internal override string GetXmlElementName()
        {
            return "emailcontent";
        }

        internal override string GetCommandName()
        {
            return Constants.GetEmailContentCommandName;
        }

        internal override string GetResponseXmlElementName()
        {
            return "emailcontent";
        }

        internal override GetEmailContentResponse ParseResponse(MyVrmServiceXmlReader reader)
        {
            var response = new GetEmailContentResponse();
            response.LoadFromXml(reader, GetResponseXmlElementName());
            return response;
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            UserId.WriteToXml(writer, "UserID");
            MyVrmService.Service.OrganizationId.WriteToXml(writer, "organizationID");
            //OrganizationId.WriteToXml(writer, "OrganizationId");
            //writer.WriteElementValue(XmlNamespace.NotSpecified, "emailcontent", emailcontent);
            
            //writer.WriteStartElement(XmlNamespace.NotSpecified, "emailcontent");
           
            writer.WriteElementValue(XmlNamespace.NotSpecified, "Conforgin", 2);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "ConfType", ConfTyp1);//104556
            //writer.WriteStartElement(XmlNamespace.NotSpecified, "Profile");
            //writer.WriteElementValue(XmlNamespace.NotSpecified, "subject", subject);
            //writer.WriteElementValue(XmlNamespace.NotSpecified, "body", body);
           // writer.WriteElementValue(XmlNamespace.NotSpecified, "emailtype", emailtype);
           // writer.WriteElementValue(XmlNamespace.NotSpecified, "emaillanguage", emaillanguage);
           // writer.WriteEndElement();
        }

        #endregion
      }

        public class GetEmailContentResponse : ServiceResponse
        {
            public string body { get; internal set; }
            public string placeholders { get; internal set; }
            public string ConferenceUrl { get; internal set; }
            public int contentid { get; internal set; }
            public string subject { get; internal set; }
            public string emailcontent { get; internal set; }
            public int  emailtype { get; internal set; }
            
            internal void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
            {
                try
				{
                    do{
                    reader.Read();
                    if(reader.LocalName != "" && reader.LocalName != null){
					switch (reader.LocalName)
					{
                        case "body":
                                body = reader.ReadElementValue();
                            break;
                        case "subject":
                                subject = reader.ReadElementValue();
                            break;
                        case "contentid":
                            contentid = reader.ReadElementValue<int>();
                           break;
                        case "emailtype":
                           emailtype = reader.ReadElementValue<int>();
                           break;
                        default:
                           reader.SkipCurrentElement();
                           break;
					   }
                     }
                    } while (!reader.IsEndElement(XmlNamespace.NotSpecified, xmlElementName));
				}
                catch (Exception ex)
                {
                    MyVrmService.TraceSource.TraceError(reader.LocalName + ": " + ex.Message, null);
                    //reader.Read();
                    //throw;
                }
            }
           
        }

    
  
     
}
