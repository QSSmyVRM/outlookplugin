﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.WebServices.Data
{
    internal class SetEndpointRequest : ServiceRequestBase<SetEndpointResponse>
    {
        internal SetEndpointRequest(MyVrmService service) : base(service)
        {
        }

        internal Endpoint Endpoint { get; set; }

        #region Overrides of ServiceRequestBase

        internal override string GetXmlElementName()
        {
            return "SetEndpoint";
        }

        internal override string GetCommandName()
        {
            return "SetEndpoint";
        }

        internal override string GetResponseXmlElementName()
        {
            return "SetEndpoint";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            UserId.WriteToXml(writer, "UserID");
            OrganizationId.WriteToXml(writer);
            Endpoint.WriteToXmlPlain(writer);
        }

        #endregion
    }
}
