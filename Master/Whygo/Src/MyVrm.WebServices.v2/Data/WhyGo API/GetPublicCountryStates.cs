﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;

namespace MyVrm.WebServices.Data
{
	class GetPublicCountryStatesRequest: ServiceRequestBase<GetPublicCountryStatesResponse>
	{
		internal int _countryId;
		internal GetPublicCountryStatesRequest(MyVrmService service)
			: base(service)
		{
		}

		#region Overrides of ServiceRequestBase

		internal override string GetXmlElementName()
		{
			return "GetPublicCountryStates";
		}

		internal override string GetCommandName()
		{
			return Constants.GetPublicCountryStatesCommandName;
		}

		internal override string GetResponseXmlElementName()
		{
			return "GetPublicCountryStates";
		}

		internal override GetPublicCountryStatesResponse ParseResponse(MyVrmServiceXmlReader reader)
		{
			var response = new GetPublicCountryStatesResponse(); //{ Room = new Room(Service) };
			response.LoadFromXml(reader, "GetPublicCountryStates");// GetResponseXmlElementName());
			return response;
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			UserId.WriteToXml(writer, "userID");
			writer.WriteElementValue(XmlNamespace.NotSpecified, "CountryId", _countryId);
		}

		#endregion
	}

	public class GetPublicCountryStatesResponse : ServiceResponse
	{
		public Dictionary<int, string> States { get; internal set; }

		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{
			States = new Dictionary<int, string>();
			int id = 0;
			string name = string.Empty;
			while (!reader.IsEndElement(XmlNamespace.NotSpecified, "GetPublicCountryStates"))
			{
				switch (reader.LocalName)
				{
					case "ID":
						id = reader.ReadElementValue<int>();
						break;
					case "Name":
						name = reader.ReadElementValue<string>();
						break;
				}
				if (reader.IsEndElement(XmlNamespace.NotSpecified, "State"))
				{
					if (id > 0 && !string.IsNullOrEmpty(name))
					{
						States.Add(id, name);
						id = 0;
						name = string.Empty;
					}
				}
				reader.Read();
			}
		}
	}
}