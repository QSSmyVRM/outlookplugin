﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using DevExpress.LookAndFeel;
using DevExpress.UserSkins;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Outlook;
using Microsoft.Office.Tools.Ribbon;
using MyVrm.Outlook;
using MyVrm.Outlook.WinForms;
using MyVrm.Outlook.WinForms.Conference;
using MyVrm.Outlook.WinForms.Options;
using MyVrm.WebServices.Data;
using MyVrm.WebServices.Data.TrueDaybook;
using MyVrmAddin2007.Properties;
using Application = Microsoft.Office.Interop.Outlook.Application;
using Exception=System.Exception;
using RecurrencePattern = Microsoft.Office.Interop.Outlook.RecurrencePattern;
using MyVrm.Outlook.Configuration;
using System.Security.Cryptography;

namespace MyVrmAddin2007
{
    public partial class ThisAddIn
    {
        private Explorer _currentExplorer;

        private Inspectors _inspectors;
    	private Explorers _openExplorers;
        private readonly Dictionary<Guid, InspectorWrapper> _wrappedInspectors = new Dictionary<Guid, InspectorWrapper>();

	    private readonly OutlookAppointmentImplCollection _appointments = new OutlookAppointmentImplCollection();

        private readonly List<AppointmentCoreTriats> _shadowAppointments = new List<AppointmentCoreTriats>();

        private readonly Dictionary<OutlookAppointmentImpl, InspectorWrapper> _appointmentToWrappers = new Dictionary<OutlookAppointmentImpl, InspectorWrapper>();
        private readonly Dictionary<OutlookAppointmentImpl, ConferenceWrapper> _appointmentToConferenceWrappers = new Dictionary<OutlookAppointmentImpl, ConferenceWrapper>();

        private readonly ExplorerWrapperCollection _wrappedExplorers = new ExplorerWrapperCollection();

        private DefaultLookAndFeel _defaultLookAndFeel;

        private MAPIFolder _deletedItemsFolder;

        static readonly byte[] SAditionalEntropy = { 0x9b, 0xce, 0x54, 0x71, 0x34, 0xf2, 0xdf, 0xb0 };
        private string strUserFname;
        private string strUserLname;
        private string streMailid;
        [EditorBrowsable(EditorBrowsableState.Never)]
        public OutlookAppointmentImplCollection Appointments
        {
            get { return _appointments; }
        }

        //Already closed appointments - just for Live Meeting co-existance
        [EditorBrowsable(EditorBrowsableState.Never)]
        public List<AppointmentCoreTriats> ShadowAppointments
        {
            get { return _shadowAppointments; }
        }

        //List of converted from myVRM to Live Meeting conferences:
        // Key      - LM GlobalAppointmentId, which accept ConferenceId
        // Value    - ConferenceId of the converted myVRM conference
        public Dictionary<string, ConferenceId> ConvertedConfs = new Dictionary<string, ConferenceId>();        

        public Dictionary<OutlookAppointmentImpl, ConferenceWrapper> AppointmentToConferenceWrappers
        {
            get { return _appointmentToConferenceWrappers; }
        }

        internal Dictionary<OutlookAppointmentImpl, InspectorWrapper> AppointmentToWrappers
        {
            get { return _appointmentToWrappers; }
        }

        public bool HasConferenceWrapper(AppointmentItem appointmentItem)
        {
			if (appointmentItem == null)
			{
				MyVrmAddin.TraceSource.TraceWarning("HasConferenceWrapper() : appointmentItem = null");
				return false;
			}

            var outlookAppointmentImpl = Appointments[appointmentItem];
            if (outlookAppointmentImpl != null)
            {
                ConferenceWrapper conferenceWrapper;
                return AppointmentToConferenceWrappers.TryGetValue(outlookAppointmentImpl, out conferenceWrapper);
            }
            return false;
        }

        public ConferenceWrapper GetConferenceWrapperForAppointment(AppointmentItem appointmentItem,bool isStaticId,bool isimmediate)
        {
			if (appointmentItem == null)
			{
				MyVrmAddin.TraceSource.TraceWarning("GetConferenceWrapperForAppointment() : appointmentItem = null");
				return null;
			}
        	ConferenceWrapper conferenceWrapper;

            var outlookAppointmentImpl = Appointments[appointmentItem];
            if (outlookAppointmentImpl == null)
            {
                outlookAppointmentImpl = new OutlookAppointmentImpl(appointmentItem);
                Appointments.Add(outlookAppointmentImpl);
            }
            if (!AppointmentToConferenceWrappers.TryGetValue(outlookAppointmentImpl, out conferenceWrapper))
            {
                conferenceWrapper = new ConferenceWrapper(outlookAppointmentImpl, MyVrmService.Service);
                conferenceWrapper.Conference.IsStaticConference = isStaticId;
                conferenceWrapper.Conference.IsImmediateconference = isimmediate;
                AppointmentToConferenceWrappers.Add(outlookAppointmentImpl, conferenceWrapper);
            }
            return conferenceWrapper;
        }

        private void ThisAddIn_Startup(object sender, EventArgs e)
        {
            try
            {
                MyVrmAddin.TraceSource.TraceInformation("Add-in is starting up...");
                var thisAssembly = Assembly.GetExecutingAssembly();
                foreach (Attribute attribute in thisAssembly.GetCustomAttributes(true))
                {
                    if (attribute is AssemblyFileVersionAttribute)
                    {
                        MyVrmAddin.ProductVersion = ((AssemblyFileVersionAttribute)attribute).Version;
                    }
                    else if (attribute is AssemblyProductAttribute)
                    {
                        MyVrmAddin.ProductName = ((AssemblyProductAttribute)attribute).Product;
                    }
                    else if (attribute is AssemblyCompanyAttribute)
                    {
                        MyVrmAddin.CompanyName = ((AssemblyCompanyAttribute)attribute).Company;
                    }
                    else if (attribute is AssemblyCopyrightAttribute)
                    {
                        MyVrmAddin.Copyright = ((AssemblyCopyrightAttribute)attribute).Copyright;
                    }
                }

				//Properties.Settings settings = new Properties.Settings();
				string settingValue = GetSettingValue("ProductDisplayName") as string;//settings["ProductDisplayName"] as string;
            	MyVrmAddin.ProductDisplayName = "myVRM"; //default
				if (!string.IsNullOrEmpty(settingValue))
					MyVrmAddin.ProductDisplayName = settingValue;

				settingValue = GetSettingValue("Logo16x16") as string;//settings["Logo16x16"] as string;
            	MyVrmAddin.Logo16 = Resources.Logo; //default
				if(!string.IsNullOrEmpty(settingValue))
					MyVrmAddin.Logo16 = Resources.ResourceManager.GetObject(settingValue) as Bitmap;

                settingValue = GetSettingValue("InstanceConference16x16") as string;//settings["Logo16x16"] as string;
                MyVrmAddin.Logo16 = Resources.instant_conference_icon_16x16; //default
                if (!string.IsNullOrEmpty(settingValue))
                    MyVrmAddin.InstantConference16 = Resources.ResourceManager.GetObject(settingValue) as Bitmap;

				settingValue = GetSettingValue("Logo65x65") as string;//settings["Logo65x65"] as string;
				MyVrmAddin.Logo65 = Resources.tdb_32x32; //default
				if (!string.IsNullOrEmpty(settingValue))
					MyVrmAddin.Logo65 = Resources.ResourceManager.GetObject(settingValue) as Bitmap;

				settingValue = GetSettingValue("Copyright") as string;//settings["Copyright"] as string;
				if (!string.IsNullOrEmpty(settingValue))
					MyVrmAddin.Copyright = settingValue;
				
				settingValue = GetSettingValue("BuildType") as string;//settings["Copyright"] as string;
				if (!string.IsNullOrEmpty(settingValue))
					MyVrmAddin.BuildType = settingValue;

                settingValue = GetSettingValue("ForgotPasswordURL") as string;//settings["Copyright"] as string;
                if (!string.IsNullOrEmpty(settingValue))
                    MyVrmAddin.ForgotPasswordURL = settingValue;
                settingValue = GetSettingValue("WebSettingsURL") as string;//settings["Copyright"] as string;
                if (!string.IsNullOrEmpty(settingValue))
                    MyVrmAddin.WebSettingsURL = settingValue;


                MyVrmAddin.TraceSource.TraceInformation("OS version: {0}", Environment.OSVersion.ToString());
                MyVrmAddin.TraceSource.TraceInformation("Local timezone: {0}", TimeZoneInfo.Local.StandardName);
                MyVrmAddin.TraceSource.TraceInformation("OS language: {0}", CultureInfo.InstalledUICulture.LCID);
                MyVrmAddin.TraceSource.TraceInformation(".NET Framework version: {0}", Environment.Version.ToString()); 
                MyVrmAddin.TraceSource.TraceInformation("Outlook version: {0}", Application.Version);
                MyVrmAddin.TraceSource.TraceInformation("Add-in: {0}", MyVrmAddin.ProductName);
                MyVrmAddin.TraceSource.TraceInformation("Add-in version: {0}", MyVrmAddin.ProductVersion);
                
                MyVrmAddin.ProductStorageVersion = "1.0";
				MyVrmService.Service.UtcEnabled = 1; //??
                MyVrmService.TraceSource = MyVrmAddin.TraceSource;
                // Outlook Generic client
                MyVrmService.Service.ClientType = "02"; 
                // Client version
                MyVrmService.Service.ClientVersion = ClientVersion.Parse(MyVrmAddin.ProductVersion);
				UIService.DefaultCaption = MyVrmAddin.ProductDisplayName;

                OfficeSkins.Register();
                _defaultLookAndFeel = new DefaultLookAndFeel();
                _defaultLookAndFeel.LookAndFeel.SetSkinStyle(UIHelper.GetCurrentSkinName(Application.Version));
                // Display EULA dialog if neccessary
				//if (!MyVrmAddin.Instance.IsLicenseAgreementAccepted)
				//{
				//    string licenseAgreementFile;
				//    using (var eulaDlg = new LicenseAgreementDialog())
				//    {
				//        eulaDlg.Text = MyVrmAddin.ProductDisplayName; 
				//        licenseAgreementFile = MyVrmAddin.DefaultLicenseAgreementFile;
				//        eulaDlg.LicenseAgreementFile = licenseAgreementFile;
				//        if (eulaDlg.ShowDialog() == DialogResult.Cancel)
				//        {
				//            return;
				//        }
				//    }
				//    MyVrmAddin.Instance.AcceptLicenseAgreement(licenseAgreementFile);
				//}
				//don't need: Application.OptionsPagesAdd += ApplicationOptionsPagesAdd;
                //don't need: Application.ItemSend += ApplicationItemSend;
                _inspectors = Application.Inspectors;
                _inspectors.NewInspector += OnNewInspector;
                _currentExplorer = Application.ActiveExplorer();

                _openExplorers = Application.Explorers;
				_openExplorers.NewExplorer += openExplorers_NewExplorer;

                foreach (Explorer explorer in _openExplorers)
                {
                    _wrappedExplorers.Add(new MyVrmExplorer(explorer));
                }
                MyVrmAddin.TraceSource.TraceInformation("Add-in is starting before explorer switch...");
                _currentExplorer.BeforeFolderSwitch += ExplorerBeforeFolderSwitch;
                MyVrmAddin.TraceSource.TraceInformation("Add-in is starting before explorer switch 1234...");
                _deletedItemsFolder = Application.Session.GetDefaultFolder(OlDefaultFolders.olFolderDeletedItems);
                MyVrmAddin.Instance.Settings.Reload();
                ShadowAppointments.Clear();
				ConvertedConfs.Clear();

                MyVrmService.Service.FirstName = "";
                MyVrmService.Service.SecondName = "";
                MyVrmService.Service.isImportUser = false;
                var settings = MyVrmAddin.Instance.Settings;
                settings.MyVrmServiceUrl = "http://schedule.ucanytime.com/";
                //settings.MyVrmServiceUrl = "http://159.54.57.13/";
                ChkUser();
            }
            catch (Exception exception)
            {
                MyVrmAddin.TraceSource.TraceException(exception);
                Marshal.FinalReleaseComObject(this);
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
               // UIHelper.ShowError(exception.Message);
            }
        }

        private MAPIFolder _currentFolder;
        private Items _currentFolderItems;

        public void ChkUser()
        {
            var settings = MyVrmAddin.Instance.Settings;
               string sttrUsername = Globals.ThisAddIn.Application.ActiveExplorer().Session.CurrentUser.Name;
                string[] strName = sttrUsername.Split(' ');
                string TDBUserPasword = "";
            try
            {
                //ZD 101226 Starts
                //SaveAccountOptions(settings);
                MyVrmService.Service.isImportUser = true;

                //settings.MyVrmServiceUrl = "http://192.168.1.15/myvrm/en/myvrmws.asmx";
                settings.MyVrmServiceUrl = "http://schedule.ucanytime.com/"; //159.54.57.13
                //settings.MyVrmServiceUrl = "http://159.54.57.13/";
                settings.AuthenticationMode = MyVrm.WebServices.Data.AuthenticationMode.Custom;
                settings.UseDefaultCredentials = false;
                settings.RememberMe = true;
                if (strName.Length == 2)
                {
                    strUserFname = sttrUsername.Split(' ')[0];
                    strUserLname = sttrUsername.Split(' ')[1];
                }
                else if (strName.Length == 3)
                {
                    strUserFname = sttrUsername.Split(' ')[0];
                    strUserLname = sttrUsername.Split(' ')[2];
                }
                else
                {
                    strUserFname = sttrUsername.Split(' ')[0];
                    strUserLname = sttrUsername.Split(' ')[0];
                }
                if (Globals.ThisAddIn.Application.ActiveExplorer().Session.CurrentUser.AddressEntry.GetExchangeUser() != null)
                {
                    streMailid = Globals.ThisAddIn.Application.ActiveExplorer().Session.CurrentUser.AddressEntry.GetExchangeUser().PrimarySmtpAddress;

                }
                else
                {
                    streMailid = Globals.ThisAddIn.Application.ActiveExplorer().Session.CurrentUser.Address;
                }
                MyVrmService.Service.FirstName = strUserFname;
                MyVrmService.Service.SecondName = strUserLname;
               
                settings.UserName = streMailid; //txtUserName.Text;
                settings.UserPassword = "";
                settings.Save();

                TDBUserPasword = MyVrmService.Service.LogonPwd();

                settings.UserPassword = TDBUserPasword;
                settings.Save();
                if (strUserLname != "Unknown")
                    MyVrmService.Service.ExternalCallLogon();
                else
                {
                    MyVrmService.Service.isImportUser = false;
                    UIHelper.ShowMessage("Mail not configured, cannot fetch user details");
                }
            }
            catch (Exception ex)
            {
                    MyVrmAddin.TraceSource.TraceException(ex);
                    UIHelper.ShowError(ex.Message);
            }
            //MyVrmService.Service.ChkNewUser(strUserFname, strUserLname, streMailid);
        }
        void ExplorerBeforeFolderSwitch(object newFolder, ref bool cancel)
        {
            var folder = (MAPIFolder) newFolder;
            if (folder == null)
                return;
            if (_currentFolderItems != null)
            {
                _currentFolderItems.ItemChange -= ItemsItemChange;
                _currentFolderItems = null;
            }
            if (_currentFolder != null)
            {
                ((MAPIFolderEvents_12_Event)_currentFolder).BeforeItemMove -= FolderBeforeItemMove;
                _currentFolder = null;
            }
            // Ignore non-calendar folders
            if (folder.DefaultItemType != OlItemType.olAppointmentItem)
                return;
            _currentFolder = folder;
            ((MAPIFolderEvents_12_Event)_currentFolder).BeforeItemMove += FolderBeforeItemMove;

            _currentFolderItems = _currentFolder.Items;
            _currentFolderItems.ItemChange += ItemsItemChange;
        }

        private bool IsDeletedItemsFolder(MAPIFolder aFolder) { return (aFolder.EntryID == _deletedItemsFolder.EntryID); }

        public bool IsItLiveMeetingAppointment(AppointmentItem item)
        {
            try
            {
                if (item != null && item.MessageClass.Contains("IPM.Appointment.Live Meeting Request"))
                    return true;
            }
            catch
            {
            }
            return false;
        }

    	//public Dictionary<string, AppointmentItem> ChangedAppItems = new Dictionary<string, AppointmentItem>();

        private void ItemsItemChange(object item)
        {
        	//return;

            try
            {
                var appointmentItem = item as AppointmentItem;
                if (appointmentItem == null)
                    return;

                string vconfId = ConferenceRibbon.GetVirtualConferenceId(appointmentItem);
                int IsICalSent = ConferenceRibbon.IsICalSent(appointmentItem);
                if (!string.IsNullOrEmpty(vconfId) && !string.IsNullOrEmpty(appointmentItem.GlobalAppointmentID) &&
                    IsICalSent == 0)
                {
                    MyVrmService.Service.UpdateConferenceICalUniqueId(new ConferenceId(vconfId), appointmentItem.GlobalAppointmentID);

                    ConferenceRibbon.SetICalSent(appointmentItem);
                    //AppointmentItem nn = (AppointmentItem)appointmentItem.Copy();
                    //ChangedAppItems[vconfId] = nn;
                }

                if (!string.IsNullOrEmpty(vconfId))
                {
                    if (GetConferenceWrapperForAppointment(appointmentItem, false, false) == null)//ZD 101226
                        Globals.ThisAddIn.InitConferenceWrapper(appointmentItem.GetInspector, appointmentItem, false, false);//ZD 101226
                }
                //if (!string.IsNullOrEmpty(vconfId))
                //{
                //    var found = ChangedAppItems.FirstOrDefault(a => a.Key == vconfId);

                //    AppointmentItem foundApp = found.Value;

                //    if (foundApp == null || foundApp.Subject != appointmentItem.Subject || foundApp.Start != appointmentItem.Start ||
                //        foundApp.End != appointmentItem.End )
                //    {
                //        MyVrmService.Service.TDUpdateConference(vconfId, appointmentItem.Subject, appointmentItem.Body,
                //           appointmentItem.Start, appointmentItem.End, TrueDayServices_v1.GetParticipantsList(appointmentItem), false);
                //        //UIHelper.ShowConferenceIsSuccessfullyCreatedPopup(null);
                //        AppointmentItem nn = (AppointmentItem)appointmentItem.Copy();
                //        ChangedAppItems[vconfId] = nn;
                //        ChangedAppItems[vconfId] = appointmentItem;
                //    }
                //}

                return;

                var appointmentImpl = Globals.ThisAddIn.Appointments[appointmentItem] ?? new OutlookAppointmentImpl(appointmentItem);
                {
                    if (appointmentImpl.ConferenceId == null && IsItLiveMeetingAppointment((AppointmentItem)item))
                    {
                        SetOverriddenLiveMeetingConfId(appointmentImpl);
                        //ConferenceId closedConfId = GetOverriddenLiveMeetingConfId(appointmentImpl);
                        //if(closedConfId != null)
                        //    appointmentImpl.m_ConvertedConfs.Add(appointmentImpl.GlobalAppointmentId, closedConfId);                    
                    }

                    if (!appointmentImpl.LinkedWithConference)
                    {
                        return;
                    }
                    var conferenceId = appointmentImpl.ConferenceId;
                    if (conferenceId != null)
                    {
                        Conference conference = null;
                        conference = Conference.Bind(MyVrmService.Service, conferenceId);
                        if (conference != null)
                        {
                            DateTime dt = conference.StartDate.Date + appointmentImpl.StartInStartTimeZone.TimeOfDay;
                            conference.StartDate = dt;
                            conference.EndDate = dt + appointmentImpl.Duration;

                            if (conference.RecurrencePattern != null)
                            {
                                conference.RecurrencePattern.StartTime = appointmentImpl.StartInStartTimeZone.TimeOfDay;
                                conference.RecurrencePattern.EndTime = appointmentImpl.EndInEndTimeZone.TimeOfDay;
                            }
                            if (conference.AppointmentTime != null)
                            {
                                conference.AppointmentTime.StartTime = appointmentImpl.StartInStartTimeZone.TimeOfDay;
                                conference.AppointmentTime.EndTime = appointmentImpl.EndInEndTimeZone.TimeOfDay;
                            }
                        }

                        switch (appointmentItem.RecurrenceState)
                        {
                            case OlRecurrenceState.olApptMaster:
                                if (conference != null)
                                {
                                    var recurrencePattern = appointmentItem.GetRecurrencePattern();
                                    foreach (var exception in recurrencePattern.Exceptions.Cast<Microsoft.Office.Interop.Outlook.
                                        Exception>().Where(except => except.Deleted))
                                    {
                                        if (conference.RecurrencePattern != null)
                                        {
                                            if (conference.RecurrencePattern.RecurrenceType == RecurrenceType.Custom)
                                            {
                                                //conference.RecurrencePattern.InitialOriginalDates ??
                                                int ndx = conference.RecurrencePattern.CustomDates.FindIndex(
                                                    customDate => customDate == exception.OriginalDate.Date);
                                                if (ndx != -1)
                                                {
                                                    conference.DeleteOccurrance(exception.OriginalDate);
                                                }
                                            }
                                            else
                                            {
                                                conference.DeleteOccurrance(exception.OriginalDate);
                                            }
                                        }
                                    }

                                    var changed = recurrencePattern.Exceptions.Cast<Microsoft.Office.Interop.Outlook.
                                        Exception>().Where(except => except.Deleted == false);
                                    bool bIsChanged = false;

                                    //If it was regular recurrent conf and we moved one occurence - transform to CustomDates
                                    if (changed.Count() > 0)
                                    {
                                        conference.ModifiedOccurrences.Clear();
                                        if (conference.RecurrencePattern != null)
                                        {
                                            conference.RecurrencePattern.CustomDates.Clear();
                                            conference.RecurrencePattern.RecurrenceType = RecurrenceType.Custom;
                                            conference.RecurrencePattern.StartDate = recurrencePattern.PatternStartDate;
                                            conference.RecurrencePattern.EndDate = recurrencePattern.PatternEndDate;
                                            RecurrencePattern rr = recurrencePattern;
                                            for (DateTime dt = rr.PatternStartDate; dt <= rr.PatternEndDate; dt = dt.AddDays(1))
                                            {
                                                try
                                                {
                                                    //Get regular instances and transform them into CustomDates
                                                    DateTime curt = dt;
                                                    curt = curt.AddHours(rr.StartTime.Hour);
                                                    curt = curt.AddMinutes(rr.StartTime.Minute);
                                                    curt = curt.AddSeconds(rr.StartTime.Second);

                                                    AppointmentItem ouItem = rr.GetOccurrence(curt);
                                                    if (ouItem != null)
                                                    {
                                                        conference.RecurrencePattern.CustomDates.Add(ouItem.Start);
                                                        OccurrenceInfo anOccurence = new OccurrenceInfo(ouItem.Start, ouItem.End);
                                                        conference.ModifiedOccurrences.Add(anOccurence);
                                                    }
                                                }
                                                catch (COMException coe)
                                                {
                                                    MyVrmAddin.TraceSource.TraceException(coe);
                                                }
                                                catch (Exception) //get an exception in case of date has already been changed
                                                {
                                                }
                                            }
                                            //bIsChanged = true;
                                        }

                                        foreach (var change in changed)
                                        {
                                            if (conference.RecurrencePattern != null)
                                                conference.RecurrencePattern.CustomDates.Add(change.AppointmentItem.Start);
                                            OccurrenceInfo anOccurence = new OccurrenceInfo(change.AppointmentItem.Start, change.AppointmentItem.End);
                                            conference.ModifiedOccurrences.Add(anOccurence);
                                        }
                                        if (conference.RecurrencePattern != null)
                                            conference.RecurrencePattern.CustomDates.Sort();

                                        //re-oreder ModifiedOccurrences
                                        OccurrenceInfoCollection orderedModifiedOccurrences = new OccurrenceInfoCollection();
                                        if (conference.RecurrencePattern != null)
                                        {
                                            foreach (DateTime dt in conference.RecurrencePattern.CustomDates)
                                            {
                                                OccurrenceInfo anOccurence = conference.ModifiedOccurrences.FirstOrDefault(occInfo => occInfo.Start == dt);
                                                orderedModifiedOccurrences.Add(anOccurence);
                                            }
                                        }
                                        conference.ModifiedOccurrences.Clear();
                                        foreach (var orderedModifiedOccurrence in orderedModifiedOccurrences)
                                        {
                                            conference.ModifiedOccurrences.Add(orderedModifiedOccurrence);
                                        }

                                        if (conference.RecurrencePattern != null)
                                            bIsChanged = conference.RecurrencePattern.CustomDates.Count > 0;
                                    }
                                    /* 
                                     * Commented only for check in - to build 2.0.58 - MUST BE RE-WORKED!! 
                                     * 
                                    var changed = recurrencePattern.Exceptions.Cast<Microsoft.Office.Interop.Outlook.
                                        Exception>().Where(except => except.Deleted == false);
                                    bool bIsChanged = false;

                                    //If it was regular recurrent conf and we moved one occurence - transform to CustomDates
                                    if (changed.Count() > 0 && conference.RecurrencePattern.CustomDates.Count == 0)
                                    {
                                        var occurrences = conference.RecurrencePattern.GetOccurrenceInfoList(conference.StartDate, //WRONG!!
                                                                                                             conference.EndDate); //WRONG!!
                                        foreach (var occurrenceInfo in occurrences)
                                        {
                                            conference.RecurrencePattern.CustomDates.Add(occurrenceInfo.Start); //+??
                                            conference.RecurrencePattern.RecurrenceType = RecurrenceType.Custom; //???
                                        }
                                        conference.RecurrencePattern.InitialOriginalDates.Clear();
                                        conference.RecurrencePattern.InitialOriginalDates.AddRange(conference.RecurrencePattern.CustomDates);
                                    }
                                    //Find changed occurence and change its' date
                                    foreach (var change in changed)
                                    {
                                        var ndx = conference.RecurrencePattern.InitialOriginalDates.FindIndex(a => a.Date == change.OriginalDate.Date);
                                        if (ndx != -1)
                                        {
                                            if (conference.RecurrencePattern.CustomDates[ndx].Date != change.AppointmentItem.StartInStartTimeZone.Date)
                                            {
                                                conference.RecurrencePattern.CustomDates[ndx] = change.AppointmentItem.Start;
                                                bIsChanged = true;
                                            }
                                        }
                                    }*/
                                    if (bIsChanged)
                                        conference.Save();

                                }
                                break;
                            case OlRecurrenceState.olApptNotRecurring:
                                if (conference != null)
                                {
                                    if (conference.StartDate.Date != appointmentImpl.StartInStartTimeZone.Date)
                                    {
                                        conference.StartDate = appointmentImpl.StartInStartTimeZone;
                                        conference.Save();
                                    }
                                }
                                break;
                        }
                    }
                }
            }
            catch (COMException COE)
            {
                MyVrmAddin.TraceSource.TraceException(COE);
            }
            catch (Exception e)
            {
                MyVrmAddin.TraceSource.TraceException(e);
                UIHelper.ShowError(e.Message);
            }
        }

		bool DoSkipDeleteFromServer()//bool forConference)
		{
			bool bSkipDeleteFromServer = false;
			switch (UIHelper.DeletionOptionValue)
			{
				case ConferenceOptionsDialog.DeletionOption.NoConfirmNoDelete:
					bSkipDeleteFromServer = true;
					break;
				case ConferenceOptionsDialog.DeletionOption.NoConfirmDelete:
					bSkipDeleteFromServer = false;
					break;
				case ConferenceOptionsDialog.DeletionOption.NeedConfirm:
                    //ZD 100569 12/16/2013 Inncrewin
					//if (UIHelper.ShowMessage(Strings.DeleteConferenceFromServerQuestion,//forConference ? Strings.DeleteConferenceFromServerQuestion : Strings.DeleteConferenceOccurenceFromServerQuestion, 
					//	MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
					//{
						bSkipDeleteFromServer = false;
					//}
                    //ZD 100569 12/16/2013 Inncrewin
					break;
			}
			return bSkipDeleteFromServer;
		}

        void FolderBeforeItemMove(object item, MAPIFolder moveTo, ref bool cancel)
        {
        	bool bDoForOutlook = true;
			//if (!IsDeletedItemsFolder(moveTo))
			//    return;

			var appointmentItem = item as AppointmentItem;
			if (appointmentItem == null)
				return;

			TrueDaybookService_v2 trueDayServices = MyVrmService.TrueDayServices; //new TrueDayServices();
			if (ConferenceRibbon.IsVirtualConference(appointmentItem) && trueDayServices != null)
			{
				string id = ConferenceRibbon.GetVirtualConferenceId(appointmentItem);
				if (!string.IsNullOrEmpty(id)) //id != 0)
				{
					if (IsDeletedItemsFolder(moveTo))
					{
						if (!DoSkipDeleteFromServer())
						{
							try
							{
								//trueDayServices.DeleteVConf(id, false);
								MyVrmService.Service.TDDeleteConference(id);
							}
                            catch (COMException coe)
                            {
                                MyVrmAddin.TraceSource.TraceException(coe);
                            }
							catch (Exception exception)
							{
								if (
									UIHelper.ShowMessage(exception.Message + ".\n" + Strings.DeleteOUConferenceQuestion, MessageBoxButtons.YesNo,
									                     MessageBoxIcon.Question) == DialogResult.No)
								{
									bDoForOutlook = false;
									cancel = true;
								}
								else
								{
									cancel = false;
								}
							}
						}
					}
					else
					{
						//VConf vConf = new VConf()
						//{
						//    Start = appointmentItem.Start,
						//    End = appointmentItem.End,
						//    Body = appointmentItem.Body,
						//    Subject = appointmentItem.Subject,
						//};
						//vConf.Envitees = new List<string>();
						//foreach (Recipient recipient in appointmentItem.Recipients)
						//{
						//    vConf.Envitees.Add(recipient.Address);
						//}
						//vConf.Id = id.ToString(); //????????
						
						//trueDayServices.UpdateVConf(id, appointmentItem); //vConf);
						//UIHelper.ShowConferenceIsSuccessfullyCreatedPopup(null);

						//CheckVCSave(ref Cancel, ref bOver);
						MyVrmService.Service.TDUpdateConference(id, appointmentItem.Subject, appointmentItem.Body, appointmentItem.Start,
						                                        appointmentItem.End,
						                                        TrueDayServices_v1.GetParticipantsList(appointmentItem), false);
						UIHelper.ShowConferenceIsSuccessfullyCreatedPopup(null);
					}
				}
			}
        	using (var appointmentImpl = Globals.ThisAddIn.Appointments[appointmentItem] ??
			                      new OutlookAppointmentImpl(appointmentItem))
			{
			//    try
			//    {
			//        if (!appointmentImpl.LinkedWithConference)
			//            return;
			//        var conferenceId = appointmentImpl.ConferenceId;
			//        if (conferenceId != null)
			//        {
			//            Conference conference = null;
			//            switch (appointmentItem.RecurrenceState)
			//            {
			//                    // Delete the occurrence if the occurrence or exception of recurring appointment is being deleted
			//                case OlRecurrenceState.olApptOccurrence:
			//                case OlRecurrenceState.olApptException:
			//                    {
			//                        conference = Conference.Bind(MyVrmService.Service, conferenceId, appointmentImpl.Start,
			//                                                     appointmentImpl.StartInStartTimeZone, appointmentImpl.EndInEndTimeZone);
			//                        conference.DeleteOccurrance(appointmentImpl.Start);
			//                        break;
			//                    }
			//                    // Delete the conference if master or not recurring appointment is being deleted
			//                case OlRecurrenceState.olApptNotRecurring:
			//                case OlRecurrenceState.olApptMaster:
			//                    {
			//                        if (!DoSkipDeleteFromServer())
			//                        {
			//                        conference = Conference.Bind(MyVrmService.Service, conferenceId);
			//                        conference.Delete();
			//                        }
			//                        break;
			//                    }
			//            }
			//            //appointmentImpl.UnlinkConference();
			//            //Appointments.Remove(appointmentImpl);
			//        }
			//    }
			//    catch (Exception e)
			//    {
			//        MyVrmAddin.TraceSource.TraceException(e);
			//        UIHelper.ShowError(e.Message);
					
			//        if (UIHelper.ShowMessage(Strings.DeleteOUConferenceQuestion, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
			//        {
			//            bDoForOutlook = false;
			//            cancel = true;
			//        }
			//        else
			//        {
			//            cancel = false;
			//        }
			//    }
                appointmentImpl.RaiseSavingEvent = false;
				if (bDoForOutlook)
				{
                    appointmentImpl.ClearIsVConfDirtyProperty();
					Appointments.Remove(appointmentImpl);
                    
				}
                
			}
        }

        
		void openExplorers_NewExplorer(Explorer explorer)
		{
            _wrappedExplorers.Add(new MyVrmExplorer(explorer));
		}

		//Tool bar items' tags:

        public static void Sub_SetAppointmentParamsFromTemplate(AppointmentItem appItem, Conference template, object asMissing)
		{
			//AppointmentItem appItem = (AppointmentItem)Application.CreateItem(OlItemType.olAppointmentItem);

			var conferenceWrapper = Globals.ThisAddIn.GetConferenceWrapperForAppointment(appItem,false,false);//ZD 101226

			conferenceWrapper.Host = MyVrmService.Service.UserId;
			conferenceWrapper.Conference.Name = template.Name;
			//+ Outlook assign 
			conferenceWrapper.Appointment.Subject = template.Name;

			conferenceWrapper.Conference.Password = template.Password;

			//Fake assign indeed
			conferenceWrapper.Conference.Description = template.Description;
			//Real assign
			conferenceWrapper.Appointment.Body = template.Description;

			conferenceWrapper.Conference.Duration = template.Duration;
			//+ Outlook assign 
			conferenceWrapper.Appointment.Duration = template.Duration;

			//conferenceWrapper.template.Type = template.Type;

			//Fake assign indeed
			conferenceWrapper.Conference.Rooms.Clear();
			conferenceWrapper.Conference.Rooms.AddRange(template.Rooms);
			//Real assign
			conferenceWrapper.LocationIds.Clear();
			conferenceWrapper.LocationIds.AddRange(template.Rooms);

			//Assign participants
			conferenceWrapper.Participants.Clear();
			//Outlook assign - for mail notification sending
			var participantsEmialList = template.Participants.Select(participant => participant.Email).ToList();
			conferenceWrapper.Appointment.SetRecipients(participantsEmialList);

			conferenceWrapper.Conference.IsPublic = template.IsPublic;

			appItem.Display(asMissing);
		}

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
            try
            {
                MyVrmAddin.TraceSource.TraceInformation("Add-in is shutting down...");
				//don't need: Application.OptionsPagesAdd -= ApplicationOptionsPagesAdd;
				//don't need: Application.ItemSend -= ApplicationItemSend;
                _inspectors.NewInspector -= OnNewInspector;
            	_openExplorers.NewExplorer -= openExplorers_NewExplorer;
                //if (Application.Version.StartsWith("12."))
                //    ((ExplorerEvents_Event)_currentExplorer).Deactivate/*Close*/ += OnClose;
                foreach (var appointmentImpl in Appointments.Where(appointmentImpl => appointmentImpl != null))
                {
                    appointmentImpl.Dispose();
                }
                Appointments.Clear();
                ShadowAppointments.Clear();
				ConvertedConfs.Clear();
            }
            catch (Exception exception)
            {
                MyVrmAddin.TraceSource.TraceException(exception);
            }
        }

        private void ApplicationItemSend(object item, ref bool cancel)
        {
            AppointmentItem apptItem = null;
            try
            {

			  //  var meetingItem = (MeetingItem)item;
			  //  apptItem = meetingItem.GetAssociatedAppointment(false);
			  //// apptItem = (AppointmentItem) item;
			  //  if (apptItem != null && apptItem.MeetingStatus != OlMeetingStatus.olMeetingCanceled)
			  //  {
			  //      CheckVCSave(ref cancel);

			  //      meetingItem.Save();
			  //  }

				//if (item is MeetingItem)
				//{
				//    var meetingItem = (MeetingItem) item;
				//    apptItem = meetingItem.GetAssociatedAppointment(false);
				//    // Ignore cancelled meetings
				//    //if (apptItem.MeetingStatus == OlMeetingStatus.olMeetingCanceled)
				//    //{
				//    //    return;
				//    //}
				//    if (apptItem == null)
				//    {
				//        MyVrmAddin.TraceSource.TraceInformation("ApplicationItemSend() : apptItem = null.");
				//    }
				//    var conferenceId = apptItem != null ? OutlookAppointmentImpl.GetConferenceId(apptItem) : null;
				//    if (conferenceId != null)
				//    {
				//        var appointmentItem = Appointments[conferenceId];
				//        if (appointmentItem != null)
				//        {
				//            if( appointmentItem.Item.MeetingStatus == OlMeetingStatus.olMeetingCanceled )
				//                return;

				//            var conferenceWrapper = AppointmentToConferenceWrappers[appointmentItem];
				//            conferenceWrapper.Conference.UpdateCalendarUniqueId(appointmentItem.GlobalAppointmentId);
				//            if (UIHelper.AppendConferenceDetailsToMeeting)
				//            {
				//                var conference = conferenceWrapper.Conference;
				//                if (conference != null)
				//                {
				//                    var body = meetingItem.Body;
				//                    var sb = new StringBuilder(body);
				//                    sb.AppendLine();
				//                    sb.AppendLine();
				//                    sb.AppendLine("You have been invited to a meeting with the following information");
				//                    sb.AppendLine();
				//                    sb.AppendFormat("Conference Name: {0}", conference.Name);
				//                    sb.AppendLine();
				//                    sb.AppendFormat("Unique ID: {0}", conference.UniqueId);
				//                    sb.AppendLine();
				//                    sb.AppendFormat("Date and Time: {0} {1}", conference.StartDate.ToString("g"),
				//                                    conference.TimeZone.DisplayName);
				//                    sb.AppendLine();
				//                    sb.AppendFormat("Duration: {0}", conference.Duration);
				//                    var hostProfile = MyVrmService.Service.GetUser(conference.HostId);
				//                    if (hostProfile != null)
				//                    {
				//                        sb.AppendLine();
				//                        sb.AppendFormat("Host: {0} ({1})", hostProfile.UserName, hostProfile.Email);
				//                    }
				//                    var roomNames = new string[conference.Rooms.Count];
				//                    for (var i = 0; i < conference.Rooms.Count; i++)
				//                    {
				//                        var roomId = conference.Rooms[i];
				//                        var roomProfile = MyVrmAddin.Instance.GetRoomFromCache(roomId);
				//                        if (roomProfile != null)
				//                        {
				//                            roomNames[i] = roomProfile.Name;
				//                        }
				//                    }
				//                    sb.AppendLine();
				//                    sb.AppendFormat("Location(s): {0}", string.Join("; ", roomNames));
				//                    meetingItem.Body = sb.ToString();
				//                    meetingItem.Save();
				//                }
				//            }
				//        }
				//    }
				//}
            }
            catch (Exception e)
            {
                MyVrmAddin.TraceSource.TraceException(e);
                UIHelper.ShowError(e.Message);
                cancel = true;
            }
            finally
            {
                if (apptItem != null)
                {
                    Marshal.ReleaseComObject(apptItem);
                }
            }
        }
		//private AppointmentItem _itemSave;
        private void OnNewInspector(Inspector inspector)
        {
            try
            {
                var item = inspector.CurrentItem as AppointmentItem;
                if (item != null && (item.MeetingStatus != OlMeetingStatus.olMeetingReceived && item.MeetingStatus != OlMeetingStatus.olMeetingCanceled))
                {
                    if (!string.IsNullOrEmpty(OutlookAppointmentImpl.GetVirtualConferenceId(item)))// != 0)) //GetConferenceId(item) != null)
                    {
                        var appointmentImpl = Globals.ThisAddIn.Appointments[item] ??
                                      new OutlookAppointmentImpl(item);
                        InitConferenceWrapper(inspector, item, false, false); //ZD 101226
                    }
                    else
                    {
                        var inspectorWrapper = InspectorWrapper.GetWrapperFor(inspector);
                        if (inspectorWrapper != null)
                        {
                            inspectorWrapper.Closed += InspectorWrapperClose;
                            _wrappedInspectors.Add(inspectorWrapper.Id, inspectorWrapper);
                            //  AppointmentToWrappers.Add(Appointments[item], inspectorWrapper);
                        }
                        else
                            GC.Collect();
                        GC.WaitForPendingFinalizers();
                        GC.Collect();
                    }
                }
                else if (item == null)
                {
                    
                   // InitConferenceWrapper(inspector, item, false, false); //ZD 101226
                   // var inspectorWrapper = InspectorWrapper.GetWrapperFor(inspector);
                   // if (inspectorWrapper != null)
                   // {
                    //    inspectorWrapper.Closed += InspectorWrapperClose;
                    //    _wrappedInspectors.Add(inspectorWrapper.Id, inspectorWrapper);
                        //  AppointmentToWrappers.Add(Appointments[item], inspectorWrapper);
                  //  }
                    Marshal.FinalReleaseComObject(inspector.CurrentItem);
                    item = null;
                  //  GC.Collect(); //104516
                   // GC.WaitForPendingFinalizers(); //104516
                   // GC.Collect(); //104516
                }
              
                ////Unsubscribe handlers for a virtual conference processing
                //if (_itemSave != null)
                //{
                //    _itemSave.Write -= VCItemWriteHandler;
                //    ((ItemEvents_10_Event) _itemSave).Close -= VCItemCloseHandler;
                //}
                //_itemSave = item;


                if (item != null && (item.MeetingStatus != OlMeetingStatus.olMeetingReceived && item.MeetingStatus != OlMeetingStatus.olMeetingCanceled))

                {
                    ThisRibbonCollection ribbonCollection = Globals.Ribbons[Globals.ThisAddIn.Application.ActiveInspector()];
                    //item.Write += VCItemWriteHandler;
                    //((ItemEvents_10_Event)item).Close += VCItemCloseHandler;
                    foreach (RibbonBase ribbon in ribbonCollection)
                    {
                        UserProperty propIsVConf = item.UserProperties.Find(ConferenceRibbon.VirtualConfPropName, true);
                        bool enableCheckSeats = false;
                        if (propIsVConf != null)
                        {
                            if (propIsVConf.Type == OlUserPropertyType.olYesNo)

                            {
                                enableCheckSeats = (bool)propIsVConf.Value;
                            }
                        }
                        var tabAppiontment = ribbon.Tabs.FirstOrDefault(a => a.ControlId.OfficeId == "TabAppointment");
                        if (tabAppiontment != null)
                        {
                            var tabItem =
                                tabAppiontment.Groups.FirstOrDefault(b => b.Items.FirstOrDefault(c => c.Id == "bnCheckSeats") != null);
                            if (tabItem != null)
                            {
                                tabItem.Items.FirstOrDefault(c => c.Id == "bnCheckSeats").Visible = enableCheckSeats;
                                break;
                            }
                        }
                    }
                  //  if (item.MeetingStatus == OlMeetingStatus.olMeetingReceived || item.MeetingStatus == OlMeetingStatus.olMeetingCanceled)
                   // Marshal.FinalReleaseComObject(inspector.CurrentItem);
                }
                else if (item != null && (item.MeetingStatus == OlMeetingStatus.olMeetingReceived || item.MeetingStatus == OlMeetingStatus.olMeetingCanceled))
                // Marshal.FinalReleaseComObject(item);
                {
                    var inspectorWrapper = InspectorWrapper.GetWrapperFor(inspector);
                    if (inspectorWrapper != null)
                    {
                        inspectorWrapper.Closed += InspectorWrapperClose;
                        _wrappedInspectors.Add(inspectorWrapper.Id, inspectorWrapper);
                        //  AppointmentToWrappers.Add(Appointments[item], inspectorWrapper);
                    }
                    else
                        GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();
                }

            }
            catch (COMException COE)
            {
                MyVrmAddin.TraceSource.TraceException(COE);
            }
            catch (Exception e)
            {
                MyVrmAddin.TraceSource.TraceException(e);
               // UIHelper.ShowError(e.Message);
            }
            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }


        internal bool ConvertToTDBConference(AppointmentItem item)
        {
            bool bResult = false;
            DialogResult dr = MessageBox.Show(null, Strings.Convert2VirtTxt,
                //"Convert this appointment to a Virtual Conference? \n(The subject, the body text and location will be overwrtten.)",
                                                              MyVrmAddin.ProductDisplayName, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.Yes)
            {
                ConferenceRibbon.ConvertAppointment2Virtual(item,false,false);//ZD 101226
                bResult = true;
            }
            

            return bResult;
        }


        internal ConferenceWrapper InitConferenceWrapper(Inspector inspector, AppointmentItem item,bool isStaticId,bool isimmediate)
        {
			if (item == null)
				MyVrmAddin.TraceSource.TraceWarning("InitConferenceWrapper() : item = null");

            var conferenceWrapper = GetConferenceWrapperForAppointment(item,isStaticId,isimmediate);
            
            var apptImpl = item != null ? Appointments[item] : null;
            if (apptImpl != null && !AppointmentToWrappers.ContainsKey(apptImpl))
            {
                var inspectorWrapper = InspectorWrapper.GetWrapperFor(inspector);
                if (inspectorWrapper != null)
                {
                    inspectorWrapper.Closed += InspectorWrapperClose;
                    _wrappedInspectors.Add(inspectorWrapper.Id, inspectorWrapper);
                    AppointmentToWrappers.Add(Appointments[item], inspectorWrapper);
                }
            }
			if (conferenceWrapper != null)
			{
				conferenceWrapper.InitialBodyTxt = item != null ? item.Body : string.Empty;
				conferenceWrapper.ClearConfCopy();
			}
            return conferenceWrapper;
        }

        private void InspectorWrapperClose(object sender, EventArgs e)
        {
            try
            {
                var wrapper = (InspectorWrapper)sender;
                //Unsubscribe from appointment Saving event
                OutlookAppointmentImpl appointmentImpl = (from appointmentToWrapper in AppointmentToWrappers
                                                          where appointmentToWrapper.Value == wrapper
                                                          select appointmentToWrapper.Key).FirstOrDefault();
                if (appointmentImpl != null)
                {
                    if (appointmentImpl.ConferenceId != null)
                    {
                        if (ShadowAppointments.FirstOrDefault(item => item.GlobalAppointmentId == appointmentImpl.GlobalAppointmentId) == null)
                        {
                            ShadowAppointments.Add(new AppointmentCoreTriats()
                            {
                                ConfId = appointmentImpl.ConferenceId,
                                Subject = appointmentImpl.Subject,
                                Start = appointmentImpl.Start,
                                End = appointmentImpl.End,
                                Location = appointmentImpl.Location,
                                IsRecurring = appointmentImpl.IsRecurring,
                                GlobalAppointmentId = appointmentImpl.GlobalAppointmentId,
                            });
                        }
                        ////If LM conference is already saved - remove converting record from list
                        //KeyValuePair<string, ConferenceId> entry = new KeyValuePair<string,ConferenceId>(appointmentImpl.GlobalAppointmentId, appointmentImpl.ConferenceId);
                        //if (ConvertedConfs.Contains(entry))
                        //{
                        //    ConvertedConfs.Remove(appointmentImpl.GlobalAppointmentId);
                        //}
                    }

                    AppointmentToWrappers.Remove(appointmentImpl);
					ConferenceWrapper conference;
					if (AppointmentToConferenceWrappers.TryGetValue(appointmentImpl, out conference))
					{

						//????????????????????
						//bool nonOutlookPropsChanged = conference.Appointment.GetNonOutlookProperty();
						////appointmentImpl.Item.MeetingStatus
						//if ((/*conference.Conference.IsDirty*/conference.Appointment.GetIsVConfDirtyProperty() || nonOutlookPropsChanged) && conference.IsConfChanged())
						//{
						//    bool Cancel = false;
						//    conference.Appointment.VC_CheckAndSave(ref Cancel);
						//    //conference.Save();
						//}
					}
                    AppointmentToConferenceWrappers.Remove(appointmentImpl);
                    appointmentImpl.Dispose();
                }
                //unsubscribe from inspector Close event
                wrapper.Closed -= InspectorWrapperClose;
                // remove from opened inspector collection
                _wrappedInspectors.Remove(wrapper.Id);
                // remove from appointments collection
                Appointments.Remove(appointmentImpl);
            }
            catch (COMException COE)
            {
                MyVrmAddin.TraceSource.TraceException(COE);
            }
            catch (Exception exception)
            {
                MyVrmAddin.TraceSource.TraceException(exception);
                UIHelper.ShowError(exception.Message);
            }
        }

        private void ApplicationOptionsPagesAdd(PropertyPages pages)
        {
            try
            {
                var ctrl2 = new OptionsPage();
                pages.Add(ctrl2, Strings.SchedulerOptionsPageTitle);
            }
            catch (COMException ex)
            {
                MyVrmAddin.TraceSource.TraceException(ex);
            }
            catch (Exception e)
            {
                MyVrmAddin.TraceSource.TraceException(e);
                UIHelper.ShowError(e.Message);
            }
        }

		private object GetSettingValue(string settingName)
		{
			Properties.Settings settings = new Properties.Settings();
			try
			{
				return settings[settingName];
            }
            catch (Exception )
			{
				return null;	
			}
		}

       // ConferenceId GetOverriddenLiveMeetingConfId(OutlookAppointmentImpl appointmentItem)
        void SetOverriddenLiveMeetingConfId(OutlookAppointmentImpl appointmentItem)
        {
            //ConferenceId confId = null;

            try
            {
                if (appointmentItem.ConferenceId == null && ShadowAppointments.Count > 0)
                {
                    AppointmentCoreTriats ret = ShadowAppointments.FirstOrDefault(
                        item => item.Subject == appointmentItem.Subject &&
                        item.Start == appointmentItem.Start &&
                        item.End == appointmentItem.End &&
                        item.Location == appointmentItem.Location &&
                        item.IsRecurring == appointmentItem.IsRecurring
                         );
                    if (ret != null)
                    {
                        //confId = ret.ConfId;

                        //Add converted to LM conference and according ConfId to list
                        if (string.IsNullOrEmpty(ConvertedConfs.FirstOrDefault(it => it.Key == appointmentItem.GlobalAppointmentId).Key))
                        {
                            ConvertedConfs.Add(appointmentItem.GlobalAppointmentId, ret.ConfId);
                            appointmentItem.m_ConvertedConfs.Add(appointmentItem.GlobalAppointmentId, ret.ConfId);
                        }

                        ShadowAppointments.Remove(ret);
                    }
                }
            }
            catch (Exception ex)
            {
                MyVrmAddin.TraceSource.TraceException(ex);
            }

            //return confId;
        }
		//VCRibbon _vcRibbon = new VCRibbon();
		//MainRibbon _mainRibbon = new MainRibbon();

		//protected override Microsoft.Office.Core.IRibbonExtensibility CreateRibbonExtensibilityObject()
		//{
		//    return new RibbonManager(new OfficeRibbon[] { _mainRibbon, _vcRibbon });
		//}


        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            Startup += ThisAddIn_Startup;
            Shutdown += ThisAddIn_Shutdown;
        }
        
        #endregion
    }
    
    public class AppointmentCoreTriats
    {
        public string Subject {get; set;}
        public  DateTime Start{get; set;}
        public  DateTime End {get; set;}
        public string Location {get; set;}
        public bool IsRecurring {get; set;}
        public ConferenceId ConfId {get; set;}
        public string GlobalAppointmentId { get; set; }
    }
}
