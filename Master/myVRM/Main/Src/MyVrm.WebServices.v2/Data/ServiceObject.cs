﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Reflection;

namespace MyVrm.WebServices.Data
{
    internal delegate T GetObjectInstanceDelegate<T>(MyVrmService service, string xmlElementName) where T : ServiceObject;

    /// <summary>
    /// Represents the base abstract class for all service types.
    /// </summary>
    [Serializable]
    public abstract class ServiceObject : ISerializable, ICloneable
    {
        private readonly PropertyBag _propertyBag;
        private string _xmlElementName;
        private readonly object _lockObject = new object();

        internal ServiceObject(MyVrmService service)
        {
            Service = service;
            _propertyBag = new PropertyBag(this);
        }

		internal ServiceObject(MyVrmService service, PropertyBag propertyBag)
		{
			Service = service;
			_propertyBag = propertyBag;
		}

    	//public abstract object Clone();
		public virtual object Clone()
		{
			ConstructorInfo ctor = GetType().GetConstructor(BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(MyVrmService), typeof(PropertyBag) }, null);
			var bag = _propertyBag.Clone(this);
			var copy = ctor.Invoke(new object[] {Service, bag});
			return copy;
		}
		
        protected ServiceObject(SerializationInfo info, StreamingContext context)
        {
            if (info == null) 
                throw new ArgumentNullException("info");
            _propertyBag = new PropertyBag(this);
            foreach (var value in info)
            {
                var propertyDefinition =
                    Schema.FirstOrDefault(
                        propDef =>
                        propDef.XmlElementName.Equals(value.Name, StringComparison.InvariantCultureIgnoreCase));
                if (propertyDefinition != null)
                {
                    _propertyBag[propertyDefinition] = value.Value;
                }
            }
        }

        public object this[PropertyDefinition propertyDefinition]
        {
            get
            {
                return PropertyBag[propertyDefinition];
            }
        }

        public bool IsDirty
        {
            get
            {
                return PropertyBag.IsDirty;
            }
        }

        public virtual bool IsNew
        {
            get
            {
                var id = GetId();
                if (id != null)
                {
                    return !id.IsValid;
                }
                return true;
            }
        }

        public ServiceObjectSchema Schema
        {
            get
            {
                return GetSchema();
            }
        }

        private MyVrmService _service;

        public MyVrmService Service
        {
            get { return _service; }
            internal set { _service = value; }
        }

        public event EventHandler OnChange;

        internal void Changed()
        {
            if (OnChange != null)
            {
                OnChange(this, EventArgs.Empty);
            }
        }

        internal PropertyBag PropertyBag
        {
            get { return _propertyBag; }
        }

		public void ClearChangeLog()
        {
            PropertyBag.ClearChangeLog();
        }
		
        internal ServiceId GetId()
        {
            var idPropertyDefinition = GetIdPropertyDefinition();
            object propertyValue = null;
            if (idPropertyDefinition != null)
            {
                PropertyBag.TryGetValue(idPropertyDefinition, out propertyValue);
            }
            return (ServiceId)propertyValue;
        }

        internal virtual PropertyDefinition GetIdPropertyDefinition()
        {
            return null;
        }

        internal abstract ServiceObjectSchema GetSchema();

        internal string GetXmlElementName()
        {
            if (string.IsNullOrEmpty(_xmlElementName))
            {
                _xmlElementName = GetXmlElementNameOverride();
                if (string.IsNullOrEmpty(_xmlElementName))
                {
                    lock (_lockObject)
                    {
                        foreach (var attribute2 in
                            GetType().GetCustomAttributes(false).OfType<ServiceObjectDefinitionAttribute>())
                        {
                            _xmlElementName = attribute2.XmlElementName;
                        }
                    }
                }
            }
            if (string.IsNullOrEmpty(_xmlElementName))
            {
                throw new Exception(string.Format("The class {0} does not have an associated XML element name.", GetType().Name));
            }
            return _xmlElementName;
        }
        /// <summary>
        /// This methods lets subclasses of ServiceObject override the default mechanism by which the XML element name associated with their type is retrieved. 
        /// </summary>
        /// <returns>The XML element name associated with this type.</returns>
        internal virtual string GetXmlElementNameOverride()
        {
            return null;
        }

        internal void LoadFromXml(MyVrmServiceXmlReader reader, bool clearPropertyBag)
        {
            PropertyBag.LoadFromXml(reader, clearPropertyBag);
        }

        internal void LoadFromXml(MyVrmServiceXmlReader reader, bool clearPropertyBag, string xmlElementName)
        {
            PropertyBag.LoadFromXml(reader, clearPropertyBag, xmlElementName);
        }

        internal void WriteToXml(MyVrmXmlWriter writer)
        {
            PropertyBag.WriteToXml(writer);
        }

        internal void WriteToXmlPlain(MyVrmXmlWriter writer)
        {
            PropertyBag.WriteToXml(writer, false);
        }

        #region Implementation of ISerializable
        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null) 
                throw new ArgumentNullException("info");
            foreach (var propertyDefinition in Schema)
            {
                object propValue;
                if (PropertyBag.TryGetValue(propertyDefinition, out propValue))
                {
                    info.AddValue(propertyDefinition.XmlElementName, propValue, propertyDefinition.PropertyType);
                }
            }
        }

        #endregion
    }
}
