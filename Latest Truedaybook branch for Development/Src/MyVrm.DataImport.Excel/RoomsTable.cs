﻿using System;
using System.Linq;
using Microsoft.Office.Interop.Excel.Extensions;
using MyVrm.Common;
using MyVrm.Common.Collections;
using MyVrm.DataImport.Excel.Extensions;
using MyVrm.WebServices.Data;
using ListObject=Microsoft.Office.Tools.Excel.ListObject;

namespace MyVrm.DataImport.Excel
{
    public class RoomsTable : ExcelTable
    {
        private readonly ExcelTableColumn _idColumn;
        private readonly ExcelTableColumn _nameColumn;
        private readonly ExcelTableColumn _topTierColumn;
        private readonly ExcelTableColumn _middleTierColumn;
        private readonly ExcelTableColumn _timeZoneColumn;
        private readonly ExcelTableColumn _floorColumn;
        private readonly ExcelTableColumn _roomNumberColumn;
        private readonly ExcelTableColumn _phoneNumberColumn;
        private readonly ExcelTableColumn _administratorColumn;
        private readonly ExcelTableColumn _projectorAvailableColumn;
        private readonly ExcelTableColumn _mediaColumn;
        private readonly ExcelTableColumn _endpointColumn;

        public RoomsTable(ListObject listObject, MyVrmService service, ValidationListManager validationListManager) : base(listObject, service, validationListManager)
        {
            var listColumns = ListObject.ListColumns;
            _idColumn = new ExcelTableColumn(listColumns[1]){IsReadOnly = true};
            _nameColumn = new ExcelTableColumn(listColumns[2]){IsRequired = true};
            _topTierColumn = new ExcelTableColumn(listColumns[3]){IsRequired = true};
            _middleTierColumn = new ExcelTableColumn(listColumns[4]){IsRequired = true};
            _timeZoneColumn = new ExcelTableColumn(listColumns[5])
                                  {
                                      IsRequired = true,
                                      HasFixedSetOfValues = true,
                                      ValuesRangeName = ValidationListManager.GetTimezoneRange()
                                  };
            _floorColumn = new ExcelTableColumn(listColumns[6]);
            _roomNumberColumn = new ExcelTableColumn(listColumns[7]);
            _phoneNumberColumn = new ExcelTableColumn(listColumns[8]);
            _administratorColumn = new ExcelTableColumn(listColumns[9]);
            _projectorAvailableColumn = new ExcelTableColumn(listColumns[10])
                                            {
                                                HasFixedSetOfValues = true,
                                                ValuesRangeName = ValidationListManager.GetYesNoRange()
                                            };
            _mediaColumn = new ExcelTableColumn(listColumns[11])
                               {
                                   HasFixedSetOfValues = true, 
                                   ValuesRangeName = ValidationListManager.GetMediaTypeRange()
                               };
            _endpointColumn = new ExcelTableColumn(listColumns[12]);

            TableColumns.AddRange(new[]
                                      {
                                          _idColumn, _nameColumn, _topTierColumn, _middleTierColumn, _timeZoneColumn,
                                          _floorColumn,
                                          _roomNumberColumn, _phoneNumberColumn, _administratorColumn,
                                          _projectorAvailableColumn,
                                          _mediaColumn, _endpointColumn
                                      });

            Initialize();
        }

        #region Overrides of ExcelTable

        protected override void Save(SaveErrorCollection errors)
        {
            var mediaTypeEnumList = new EnumListSource(typeof (MediaType));
            var topTiers = Service.GetTopTierList();

            foreach (var listRow in ListObject.ListRows.Items())
            {
                var room = new Room(Service)
                               {
                                   Capacity = 0,
                                   PhoneLinesCount = 0,
                                   AssistantId = new UserId("11"),
                                   SetupTime = 0,
                                   TimeZone = TimeZoneInfo.FindSystemTimeZoneById(TimeZoneInfo.Local.Id),
                                   TeardownTime = 0,
                                   HandicappedAccess = false,
                                   EndpointId = EndpointId.Default,
                                   Media = MediaType.NoAudioVideo,
                                   Projector = false,
                                   DynamicLayout = false,
                                   CateringFacility = false,
                                   Images = new RoomImages(),
                                   Approvers = new RoomApprovers()
                               };

                try
                {
                    var range = listRow.Range;
                    room.Name = range.Item(1, _nameColumn.Index).ValueAsString();

                    var topTierName = range.Item(1, _topTierColumn.Index).ValueAsString();
                    TopTier topTier = null;
                    while (topTier == null)
                    {
                        topTier =
                            topTiers.FirstOrDefault(match => match.Name.Equals(topTierName, StringComparison.OrdinalIgnoreCase));
                        if (topTier == null)
                        {
                            var newTier = new TopTier(Service) { Name = topTierName };
                            newTier.Save();
                            topTiers = Service.GetTopTierList();
                        }
                    }
                    room.TopTierId = topTier.Id;

                    var middleTierName = range.Item(1, _middleTierColumn.Index).ValueAsString();
                    MiddleTier middleTier = null;
                    while (middleTier == null)
                    {
                        var middleTiers = Service.GetMiddleTierList(topTier.Id);
                        middleTier =
                            middleTiers.FirstOrDefault(
                                match => match.Name.Equals(middleTierName, StringComparison.OrdinalIgnoreCase));
                        if (middleTier == null)
                        {
                            var newTier = new MiddleTier(Service) { Name = middleTierName, ParentId = topTier.Id};
                            newTier.Save();
                        }
                    }
                    room.MiddleTierId = middleTier.Id;
                    
                    room.TimeZone = range.Item(1, _timeZoneColumn.Index).ValueAsTimeZoneInfo();

                    room.Floor = range.Item(1, _floorColumn.Index).ValueAsString();
                    room.Number = range.Item(1, _roomNumberColumn.Index).ValueAsString();
                    room.Phone = range.Item(1, _phoneNumberColumn.Index).ValueAsString();

                    var projector = range.Item(1, _projectorAvailableColumn.Index).ValueAsString();
                    room.Projector = "Yes".Equals(projector, StringComparison.OrdinalIgnoreCase);

                    var mediaType = range.Item(1, _mediaColumn.Index).ValueAsString();

                    var list = mediaTypeEnumList.GetList();
                    foreach (LocalizedEnum localizedEnum in list)
                    {
                        if (localizedEnum.Text.Equals(mediaType, StringComparison.OrdinalIgnoreCase))
                        {
                            room.Media = (MediaType)localizedEnum.Value;
                            break;
                        }
                    }

                    room.Save();
                }
                catch (Exception e)
                {
                    errors.Add(new SaveError(room.Name, e));
                }
            }
        }

        #endregion
    }
}
