﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	class CurrencyAbbr
	{
		public enum CurrencyType
		{
			None = -1,
			AUD,
			USD
		}
		
		static public string GetCurrencyAbbr(CurrencyType _currencyType)
		{
			switch (_currencyType)
			{
				case CurrencyType.AUD:
					return "AUD";
				case CurrencyType.USD:
					return "USD";
			}
			return string.Empty;
		}

		static public CurrencyType GetCurrencyByAbbr(string currencyAbbr)
		{
			if (currencyAbbr != null)
			{
				switch (currencyAbbr)
				{
					case "AUD":
						return CurrencyType.AUD;
					case "USD":
						return CurrencyType.USD;
				}
			}
			return CurrencyType.None;
		}
	}
}
