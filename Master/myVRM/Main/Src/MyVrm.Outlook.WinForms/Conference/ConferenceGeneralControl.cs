﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System.Windows.Forms;
using DevExpress.Utils;
using MyVrm.Common.Collections;
using MyVrm.Common.ComponentModel;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook.WinForms.Conference
{
    public partial class ConferenceGeneralControl : ConferenceAwareControl
    {

        public ConferenceGeneralControl()
        {
            InitializeComponent();
			
			conferencePasswordEdit.SuperTip.Items.Clear();
			conferencePasswordEdit.SuperTip.Items.Add(Strings.ConferencePasswordLableText);
			// commented for ZD 101376
            //publicConferenceCheckEdit.SuperTip.Items.Clear();
            //publicConferenceCheckEdit.SuperTip.Items.Add(Strings.PublicConferenceLableText);
            //publicConferenceCheckEdit.SuperTip.Items.Add(Strings.PublicConferenceCheckBoxText);
            if (MyVrmService.Service.OrganizationOptions.EnableConferencePassword)//ZD 101836
               layoutControlItem16.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always ;
            else
              layoutControlItem16.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;

            conferenceTypeEdit.SuperTip.Items.Clear();
        	conferenceTypeEdit.SuperTip.Items.Add(Strings.ConferenceTypeLableText);
			conferenceTypeEdit.SuperTip.Items.Add(Strings.SelectConferenceTypeToolTipText);

			layoutControlItem1.Text = Strings.ConferenceTypeLableText + ":";
			layoutControlItem1.CustomizationFormText = Strings.ConferenceTypeLableText + ":";
			layoutControlItem16.Text = Strings.ConferencePasswordLableText + ":";
			layoutControlItem16.CustomizationFormText = Strings.ConferencePasswordLableText + ":";
            // commented for ZD 101376
            //publicConferenceCheckEdit.Properties.Caption = Strings.PublicConferenceLableText;
			conferenceTypeEdit.EditValueChanging += conferenceTypeEdit_EditValueChanging;
			pCConferencingComboBoxEdit.EditValueChanging += conferenceTypeEdit_EditValueChanging;
			vMRComboBoxEdit.EditValueChanging += conferenceTypeEdit_EditValueChanging;
			vidyoCheckEdit.EditValueChanging += conferenceTypeEdit_EditValueChanging;


            layoutControlItem8.Text = Strings.DesktopVideoLabelText+":";//zd 101376
            layoutControlItem4.Text = Strings.VMRText + ":";//zd 101376
			//checkEditSecure.EditValueChanging += conferenceTypeEdit_EditValueChanging;
        }

		void conferenceTypeEdit_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
		{
			if (e.NewValue != e.OldValue && e.OldValue != null)
			{
				if (Conference != null)
				{
					Conference.Appointment.SetNonOutlookProperty(true);
					if (sender.Equals(pCConferencingComboBoxEdit) && (PCConferenceVendor)e.NewValue != PCConferenceVendor.None && Conference.IsCloudConferencing ||
						sender.Equals(vidyoCheckEdit) && (bool)e.NewValue && Conference.PCVendorId != PCConferenceVendor.None)
					{
						UIHelper.ShowMessage(Strings.VidyoAndPCConferenceError, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
						e.Cancel = true;
					}
					else
					{
						if (sender.Equals(conferenceTypeEdit) && (ConferenceType)e.NewValue == ConferenceType.Hotdesking)
						{
							DataList<RoomId> nonHotdeskingRooms = new DataList<RoomId>();
							foreach (var roomId in Conference.LocationIds)
							{
								Room room = MyVrmAddin.Instance.GetRoomFromCache(roomId);
								if( room.RoomCategory != RoomCategoryType.HotdeskingRoom)
								{
									nonHotdeskingRooms.Add(roomId);
								}
							}
							foreach (var nonHotdeskingRoom in nonHotdeskingRooms)
							{
								Conference.LocationIds.Remove(nonHotdeskingRoom);
							}
						}

						if (sender.Equals(conferenceTypeEdit))
						{
							if((ConferenceType) e.NewValue == ConferenceType.Hotdesking ||
								 (ConferenceType) e.NewValue == ConferenceType.PointToPoint ||
								 (ConferenceType) e.NewValue == ConferenceType.RoomConference)
							{
								Conference.VMRType = VMRConferenceType.None;
								vMRComboBoxEdit.Enabled = false;
							}
							else
							{
								vMRComboBoxEdit.Enabled = true;
							}
						}

						if(sender.Equals(vMRComboBoxEdit))
						{
							Conference.VMRType = (VMRConferenceType)e.NewValue;
						}
					}
				}
			}
			
		}

        protected override void OnReadOnlyChanged(ReadOnlyChangedEventArgs e)
        {
            conferenceTypeEdit.Enabled = !e.ReadOnly;
            conferencePasswordEdit.Enabled = !e.ReadOnly;
           // publicConferenceCheckEdit.Enabled = !e.ReadOnly;//101376 
			pCConferencingComboBoxEdit.Enabled = !e.ReadOnly;
			vMRComboBoxEdit.Enabled = !e.ReadOnly;
			vidyoCheckEdit.Enabled = !e.ReadOnly;
			//checkEditSecure.Enabled = !e.ReadOnly;
        }

        private void ConferenceGeneralControl_Load(object sender, System.EventArgs e)
        {
            if (!DesignMode)
            {
                conferenceTypeEdit.Properties.Items.AddRange(
                    new EnumArrayListSource(MyVrmService.Service.OrganizationOptions.EnabledConferenceTypes,
                                            typeof (ConferenceType)).GetList());

                conferencePasswordEdit.DataBindings.Add("EditValue", ConferenceBindingSource, "Password", true,
                                                        DataSourceUpdateMode.OnPropertyChanged);
                conferenceTypeEdit.DataBindings.Add("EditValue", ConferenceBindingSource, "Type", true,
                                                    DataSourceUpdateMode.OnPropertyChanged);
              //  publicConferenceCheckEdit.DataBindings.Add("Checked", ConferenceBindingSource, "IsPublic", true,
                                                          // DataSourceUpdateMode.OnPropertyChanged);//commented for 101376
				
				var preference = MyVrmService.Service.OrganizationSettings.Preference;
				//PC Conferencing combo
				pCConferencingComboBoxEdit.Properties.Items.Clear();
				//pCConferencingComboBoxEdit.Properties.Items.Add(WinForms.Strings.TxtNone);
				//if(preference.IsBlueJeansEnabled)
				//    pCConferencingComboBoxEdit.Properties.Items.Add(WinForms.Strings.TxtBlueJeans);
				//if (preference.IsJabberEnabled)
				//    pCConferencingComboBoxEdit.Properties.Items.Add(WinForms.Strings.TxtJabber);
				//if (preference.IsLyncEnabled)
				//    pCConferencingComboBoxEdit.Properties.Items.Add(WinForms.Strings.TxtLinc);
				//if (preference.IsVidtelEnabled)
				//    pCConferencingComboBoxEdit.Properties.Items.Add(WinForms.Strings.TxtVidtel);
                if (MyVrmService.Service.enableDesktopVideo)
                {

                    pCConferencingComboBoxEdit.Properties.Items.AddRange(
                        new EnumArrayListSource(MyVrmService.Service.OrganizationOptions.EnabledPCConferenceTypes,
                                                typeof(PCConferenceVendor)).GetList());

                    pCConferencingComboBoxEdit.DataBindings.Add("EditValue", ConferenceBindingSource, "PCVendorId", true,
                                                        DataSourceUpdateMode.OnPropertyChanged);
                }
                else {
                    layoutControlItem8.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                    pCConferencingComboBoxEdit.Visible = false;
                }
                ConfigureNetworkSwitching(preference.NetworkSwitching);

               
            	//Cloud conf
				vidyoCheckEdit.Checked = false;
            	vidyoCheckEdit.Visible = preference.IsCloudEnabled;
                if (!preference.IsCloudEnabled)
                    vidyoCheckEdit.Dispose();

                //publicConferenceCheckEdit.Visible = preference.EnablePublicConferencing;
                //if (!preference.EnablePublicConferencing)
                //    publicConferenceCheckEdit.Dispose();

				vidyoCheckEdit.DataBindings.Add("Checked", ConferenceBindingSource, "IsCloudConferencing", true,
                                                           DataSourceUpdateMode.OnPropertyChanged);

				//VMR combo
				vMRComboBoxEdit.Properties.Items.Clear();
				//vMRComboBoxEdit.Properties.Items.Add(WinForms.Strings.TxtNone);
				//vMRComboBoxEdit.Properties.Items.Add("Personal"); //WinForms.Strings.);
				//vMRComboBoxEdit.Properties.Items.Add("Room");//WinForms.Strings.);
				//vMRComboBoxEdit.Properties.Items.Add("External");//WinForms.Strings.);
                if (MyVrmService.Service.OrganizationOptions.ViewVMR)
                {
                    vMRComboBoxEdit.Properties.Items.AddRange(
                        new EnumArrayListSource(MyVrmService.Service.OrganizationOptions.EnabledVMRConferenceTypes,
                                                typeof(VMRConferenceType)).GetList());

                    vMRComboBoxEdit.DataBindings.Add("EditValue", ConferenceBindingSource, "VMRType", true,
                                                               DataSourceUpdateMode.OnPropertyChanged);
                    if (conferenceTypeEdit.SelectedIndex == 0 || conferenceTypeEdit.SelectedIndex == 1 || conferenceTypeEdit.SelectedIndex == 4)
                    {
                        Conference.VMRType = VMRConferenceType.None;
                        vMRComboBoxEdit.Enabled = false;
                    }
                    else
                        vMRComboBoxEdit.Enabled = true;

                }
                else
                {
                    vMRComboBoxEdit.Visible = false;
                    layoutControlItem4.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                }
				checkEditSecure.Enabled = false;
				checkEditSecure.Visible = false;
				checkEditSecure.Dispose();
				/*Requirement is closed
//if GetAllOrgSettings command/<SecureSwitch> is 1 :
//GetNewConference /<userInfo>/<Secured> - 1 ,  then in Default Secure check box will be enabled and checked in conference scheduling screen.User can check/uncheck the "Secure" checkbox.
//GetNewConference /<userInfo>/<Secured> - 0 ,  then in Default Secure check box will be enabled and user can not make the selection 

//GetOldConference/<userInfo>/<Secured> - 1 and /<confInfo>/<Secured> - 1,  then Secure check box is enabled and checked for the conference.User can check/uncheck the "Secure" checkbox.
//GetOldConference/<userInfo>/<Secured> - 0 and /<confInfo>/<Secured> - 1,  then Secure check box is enabled and checked for the conference. User cannot make selection of check box.
//if GetAllOrgSettings command/<SecureSwitch> is 0 :
//Secure check box will be hid from application. 
				ConferenceWrapper conf = ((ConferenceWrapper)ConferenceBindingSource.DataSource);
				if(preference.IsSecureSwitchEnabled)
				{
					if (conf.Id == null) //new conf
					{
						if (MyVrmService.Service.GetNewConference().Secured)
						{
							conf.IsSecured = true;
							checkEditSecure.Enabled = true;
							checkEditSecure.Checked = conf.IsSecured;
						}
						else
						{
							conf.IsSecured = false; 
							checkEditSecure.Enabled = false;
							checkEditSecure.Checked = conf.IsSecured;
						}
					}
					else //existing conference
					{
						checkEditSecure.Checked = conf.IsSecured;
						if (conf.IsSecuredEnabledForUser)
						{
							checkEditSecure.Enabled = true;
						}
						else
						{
							checkEditSecure.Enabled = false;
						}
					}
				}
				else
				{
					conf.IsSecured = false;
					checkEditSecure.Enabled = false;
					checkEditSecure.Checked = conf.IsSecured; 
					checkEditSecure.Visible = false;
				}
				
            	checkEditSecure.DataBindings.Add("Checked", ConferenceBindingSource, "IsSecured", true,
														   DataSourceUpdateMode.OnPropertyChanged);
				*/

                if (MyVrmService.Service.OrganizationOptions.DefaultConferenceType == ConferenceType.AudioVideo || MyVrmService.Service.OrganizationOptions.DefaultConferenceType == ConferenceType.AudioOnly)
                    MyVrmService.Service.isMessageOverlay = true;
                else
                    MyVrmService.Service.isMessageOverlay = false;

            }
        }

        private void ConfigureNetworkSwitching(int iNetworkSwitch)
        {
            //Network switching
            

            networkingSwitchingComboEdit.Properties.Items.Clear();
            networkingSwitchingComboEdit.Properties.Items.AddRange(
                new EnumArrayListSource(MyVrmService.Service.OrganizationOptions.NetworkingOptions,
                                        typeof(NetworkSwitchingOptions)).GetList());

            
            //IsSecured
            //2 means NATO supported server
            if (iNetworkSwitch != 2)
            {
                layoutControlItem6.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                ConferenceWrapper conf = ((ConferenceWrapper)ConferenceBindingSource.DataSource);
                conf.IsSecured = NetworkSwitchingOptions.NATOUnClassified;
               
            }
            else
            {
                //Put it to IsSecured
                ConferenceWrapper conf = ((ConferenceWrapper)ConferenceBindingSource.DataSource);
                if (conf.Id == null)
                    conf.IsSecured = NetworkSwitchingOptions.NATOSecret;

                networkingSwitchingComboEdit.DataBindings.Add("EditValue", ConferenceBindingSource, "IsSecured", true,
                                                           DataSourceUpdateMode.OnPropertyChanged);
            }
        }
    }
}