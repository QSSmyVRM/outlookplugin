﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using MyVrm.WebServices.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.ObjectModel;

namespace MyVrm.WebServices.Tests
{
    
    
    /// <summary>
    ///This is a test class for RecurrencePatternTest and is intended
    ///to contain all RecurrencePatternTest Unit Tests
    ///</summary>
    [TestClass()]
    public class RecurrencePatternTest
    {
        private TestContext testContextInstance;
        private TimeZoneInfo _timeZoneInfo = TimeZoneInfo.Local;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        [TestMethod]
        public void DailyRecurrencePatternTest()
        {
            var startDate = new DateTime(2011, 1, 1);
            var endDate = new DateTime(2011, 1, 10, 23, 0, 0);
            var pattern = new RecurrencePattern(_timeZoneInfo)
                              {
                                  RecurrenceType = RecurrenceType.Daily,
                                  StartDate = startDate,
                                  EndDate = endDate,
                                  StartTime = new TimeSpan(10, 0, 0),
                                  EndTime = new TimeSpan(12, 0, 0),
                                  DaysOfWeek = DaysOfWeek.AllDays,
                                  Interval = 1
                              };
            var occurrenceInfos = pattern.GetOccurrenceInfoList(startDate, endDate);
            Assert.AreEqual(occurrenceInfos.Count, 10);
        }
        [TestMethod]
        public void MonthlyRecurrencePatternTest()
        {
            var startDate = new DateTime(2011, 1, 1);
            var endDate = new DateTime(2011, 12, 10, 23, 0, 0);
            var pattern = new RecurrencePattern(_timeZoneInfo)
            {
                RecurrenceType = RecurrenceType.Monthly,
                StartDate = startDate,
                EndDate = endDate,
                StartTime = new TimeSpan(10, 0, 0),
                EndTime = new TimeSpan(12, 0, 0),
                DaysOfWeek = DaysOfWeek.AllDays,
                DayOfMonth = 1, 
                Interval = 1 // of every N month(s)
            };
            var occurrenceInfos = pattern.GetOccurrenceInfoList(startDate, endDate);
            Assert.AreEqual(occurrenceInfos.Count, 12);
        }
        [TestMethod]
        public void MonthNthRecurrencePatternTest()
        {
            var startDate = new DateTime(2011, 1, 1);
            var endDate = new DateTime(2011, 12, 10, 23, 0, 0);
            var pattern = new RecurrencePattern(_timeZoneInfo)
            {
                RecurrenceType = RecurrenceType.MonthNth,
                StartDate = startDate,
                EndDate = endDate,
                StartTime = new TimeSpan(10, 0, 0),
                EndTime = new TimeSpan(12, 0, 0),
                DaysOfWeek = DaysOfWeek.AllDays,
                Instance = 1, // first, second, third, fourth, last
                Interval = 1 // of every N month(s)
            };
            var occurrenceInfos = pattern.GetOccurrenceInfoList(startDate, endDate);
            Assert.AreEqual(occurrenceInfos.Count, 12);
        }
        [TestMethod]
        public void YearlyRecurrencePatternTest()
        {
            var startDate = new DateTime(2011, 1, 1);
            var endDate = new DateTime(2012, 12, 10, 23, 0, 0);
            var pattern = new RecurrencePattern(_timeZoneInfo)
            {
                RecurrenceType = RecurrenceType.Yearly,
                StartDate = startDate,
                EndDate = endDate,
                StartTime = new TimeSpan(10, 0, 0),
                EndTime = new TimeSpan(12, 0, 0),
                DayOfMonth = 1,
                MonthOfYear = 1,
                Interval = 1 // of every N year(s)
            };
            var occurrenceInfos = pattern.GetOccurrenceInfoList(startDate, endDate);
            Assert.AreEqual(occurrenceInfos.Count, 2);
        }
        [TestMethod]
        public void YearNthRecurrencePatternTest()
        {
            var startDate = new DateTime(2011, 1, 1);
            var endDate = new DateTime(2012, 12, 10, 23, 0, 0);
            var pattern = new RecurrencePattern(_timeZoneInfo)
            {
                RecurrenceType = RecurrenceType.YearNth,
                StartDate = startDate,
                EndDate = endDate,
                StartTime = new TimeSpan(10, 0, 0),
                EndTime = new TimeSpan(12, 0, 0),
                DaysOfWeek = DaysOfWeek.AllDays,
                MonthOfYear = 1,
                Instance = 1, // first, second, third, fourth, last
                Interval = 1 // of every N month(s)
            };
            var occurrenceInfos = pattern.GetOccurrenceInfoList(startDate, endDate);
            Assert.AreEqual(occurrenceInfos.Count, 2);

            startDate = new DateTime(2011, 6, 1);
            pattern = new RecurrencePattern(_timeZoneInfo)
            {
                RecurrenceType = RecurrenceType.YearNth,
                StartDate = startDate,
                Occurrences = 10,
                StartTime = new TimeSpan(10, 0, 0),
                EndTime = new TimeSpan(12, 0, 0),
                DaysOfWeek = DaysOfWeek.Friday,
                MonthOfYear = 6,
                Instance = 2, // first, second, third, fourth, last
                Interval = 1 // of every N month(s)
            };
            occurrenceInfos = pattern.GetOccurrenceInfoList(startDate, endDate);
            Assert.AreEqual(occurrenceInfos.Count, 2);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void DayOfMonthRangeTest()
        {
            var startDate = new DateTime(2011, 1, 1);
            var pattern = new RecurrencePattern(_timeZoneInfo)
                              {
                                  RecurrenceType = RecurrenceType.Monthly,
                                  StartDate = startDate,
                                  DayOfMonth = 0
                              };
        }
    }
}
