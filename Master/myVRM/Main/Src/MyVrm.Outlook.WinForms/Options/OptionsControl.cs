/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Windows.Forms;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook.WinForms.Options
{
    public partial class OptionsControl : Page
    {
        public delegate void OptionsChangedHandler(object sender, EventArgs args);

        public OptionsControl()
        {
            InitializeComponent();
            labelControl3.Text = WinForms.Strings.ClearRoomLocalCacheLableText;
            labelControl2.Text = WinForms.Strings.ChangeConferenceOptionsLableText;
			labelControl1.Text = WinForms.Strings.ConferenceLableText;
            layoutControlItem14.CustomizationFormText = WinForms.Strings.LocalCacheCustomizationFormText;
            conferenceOptionsButton.Text = WinForms.Strings.ConferenceOptionsLableText + "...";
            connectionOptionsLabel.Text = WinForms.Strings.SetUpConnectionSettingsLableText;
            connectionOptionsGroupLabel.Text = WinForms.Strings.WebServicesLableText;
            connectionOptionsButton.Text = WinForms.Strings.ConnectionOptionsLableText + "...";
            clearCacheButton.Text = WinForms.Strings.ClearCacheLableText;
            aboutButton.Text = WinForms.Strings.AboutLableText;
        }


		public void External_OnApplying(System.ComponentModel.CancelEventArgs e)
		{
			OnApplying(e);
		}
        protected override void OnApplying(System.ComponentModel.CancelEventArgs e)
        {
            MyVrmAddin.Instance.Settings.Save();
            UIHelper.Instance.Settings.Save();
            e.Cancel = false;
        }

        public event OptionsChangedHandler OptionsChanged;

        void InvokeOptionsChanged(EventArgs args)
        {
            OptionsChangedHandler changed = OptionsChanged;
            if (changed != null) changed(this, args);
        }

        private void aboutButton_Click(object sender, EventArgs e)
        {
            using(var dlg = new AboutDialog())
            {
                dlg.ShowDialog(this);
            }
        }

        private void OptionsControl_Load(object sender, EventArgs e)
        {
        }

        private void clearCacheButton_Click(object sender, EventArgs e)
        {
            try
            {
                var cursor = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                try
                {
                    MyVrmAddin.Instance.ClearRoomCache();
                }
                finally
                {
                    Cursor.Current = cursor;    
                }
            }
            catch (Exception exception)
            {
                ShowError(exception.Message);
            }
        }

		private void connectionOptionsButton_Click(object sender, EventArgs e)
		{
			using (var dialog = new ConnectionOptionsDialog())
			{
				var settings = MyVrmAddin.Instance.Settings;
				dialog.ServiceUrl = settings.MyVrmServiceUrl;
				dialog.AuthenticationMode = settings.AuthenticationMode;
				dialog.UseDefaultCredential = settings.UseDefaultCredentials;
			    dialog.UserDomain = settings.UserDomain;
				dialog.UserName = settings.UserName;
				dialog.UserPassword = settings.UserPassword;
				if (dialog.ShowDialog(this) == DialogResult.OK)
				{
					settings.MyVrmServiceUrl = dialog.ServiceUrl;
					settings.AuthenticationMode = dialog.AuthenticationMode;
					settings.UseDefaultCredentials = dialog.UseDefaultCredential;
					settings.UserDomain = dialog.UserDomain;
					settings.UserName = dialog.UserName;
					settings.UserPassword = dialog.UserPassword;
					InvokeOptionsChanged(EventArgs.Empty);
                    //101652 starts
                    System.ComponentModel.CancelEventArgs ek = new System.ComponentModel.CancelEventArgs();
                    OnApplying(ek);
                    //101652 Ends
				}   
			}
		}

		private void conferenceOptionsButton_Click(object sender, EventArgs e)
		{
            using(var dialog = new ConferenceOptionsDialog())
            {
				dialog.ShowPopupConferenceIsScheduled = UIHelper.Instance.Settings.ShowPopupWhenConferenceIsScheduled;
				dialog.AppendConferenceDetailsToMeeting = UIHelper.Instance.Settings.AppendConferenceDetailsToMeeting;
				dialog.DeletionOptionValue = UIHelper.Instance.Settings.DeletionOptionValue;

                if (dialog.ShowDialog(this) == DialogResult.OK)
                {
                    UIHelper.Instance.Settings.ShowPopupWhenConferenceIsScheduled =
                        dialog.ShowPopupConferenceIsScheduled;
                    UIHelper.Instance.Settings.AppendConferenceDetailsToMeeting =
                        dialog.AppendConferenceDetailsToMeeting;
					UIHelper.Instance.Settings.DeletionOptionValue = dialog.DeletionOptionValue;
                    InvokeOptionsChanged(EventArgs.Empty);
                }
            }
        }
    }
}