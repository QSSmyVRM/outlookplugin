﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using MyVrm.Common.Caching.BackingStore;

namespace MyVrm.Common.Caching
{
    public class CacheManager : ICacheManager, IDisposable
    {
        private readonly object _lockObject = new object();
        private Dictionary<string, CacheItem> _items;
        private readonly IBackingStore _backingStore;

        internal CacheManager(string cachePath)
        {
            _backingStore = new SqliteBackingStore(cachePath);
        }
        
        #region Implementation of ICacheManager

        public void Add(string key, object value)
        {
            lock (_lockObject)
            {
                var cacheItem = new CacheItem(key, value);
                Items.Add(key, cacheItem);
                _backingStore.Add(cacheItem);
            }
        }

        public void Remove(string key)
        {
            lock (_lockObject)
            {
                Items.Remove(key);
                _backingStore.Remove(key);
            }
        }

        public object GetData(string key)
        {
            lock (_lockObject)
            {
                return Items[key].Value;
            }
        }

        public bool Contains(string key)
        {
            lock (_lockObject)
            {
                return Items.ContainsKey(key);
            }
        }

        public void Flush()
        {
            lock (_lockObject)
            {
                Items.Clear();
                _backingStore.Flush();
            }
        }

        public object this[string key]
        {
            get 
            {
                lock (_lockObject)
                {
                    return Items[key].Value;
                }
            }
        }

        public int Count
        {
            get
            {
                lock (_lockObject)
                {
                    return Items.Count;
                }
            }
        }

        private Dictionary<string, CacheItem> Items
        {
            get
            {
                if (_items == null)
                {
                    _items = new Dictionary<string, CacheItem>(100);
                    Dictionary<string, object> loadedItems = _backingStore.Load();
                    if (loadedItems != null)
                    {
                        foreach (var loadedItem in loadedItems)
                        {
                            _items.Add(loadedItem.Key, new CacheItem(loadedItem.Key, loadedItem.Value));
                        }
                    }
                }
                return _items;
            }
        }

        #endregion

        #region Implementation of IDisposable

        public void Dispose()
        {
            ((IDisposable)_backingStore).Dispose();
            Items.Clear();
        }

        #endregion
    }
}
