﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

//using System.Runtime.Serialization.Json;
 

namespace MyVrm.WebServices.Data.TrueDaybook
{
	public class TrueDaybookErrCode
	{
		private JObject _json;
		public JObject Json
		{
			set
			{
				_json = value;
				_intErrorCode = _json["Code"].Value<int>();
				_strErrorCode = _json["Success"].Value<string>();
				_strErrorMsg = _json["Message"].Value<string>();
			}
		}

		private int _intErrorCode;
		public int ErrorCodeInt
		{
			get { return _intErrorCode; }
		}

		private string _strErrorCode;
		public string ErrorCodeString
		{
			get { return _strErrorCode; }
		}

		private string _strErrorMsg;
		public string ErrorMsg
		{
			get { return _strErrorMsg; }
		}

		public TrueDaybookErrCode(JObject json)
		{
			Json = json;
		}
	}
}
