﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;

namespace MyVrm.WebServices.Data
{
    /// <summary>
    /// Defines how a complex property behaves. 
    /// </summary>
    [Flags]
    internal enum PropertyDefinitionFlags
    {
        /// <summary>
        /// No specific behavior. 
        /// </summary>
        None = 0,
        /// <summary>
        /// The property is automatically instantiated when it is read.
        /// </summary>
        AutoInstantiateOnRead = 1,
        /// <summary>
        /// The existing instance of the property is reusable.
        /// </summary>
        ReuseInstance = 2,
        /// <summary>
        /// The property can be set.
        /// </summary>
        CanSet = 4,
        /// <summary>
        /// The property can be updated.
        /// </summary>
        CanUpdate = 8,
        /// <summary>
        /// The property can be deleted.
        /// </summary>
        CanDelete = 0x10,
        /// <summary>
        /// The property can be searched.
        /// </summary>
        CanFind = 0x20,
        /// <summary>
        /// The property must be loaded explicitly
        /// </summary>
        MustBeExplicitlyLoaded = 0x40
    }

    public abstract class PropertyDefinition
    {
        private string _name;
        private readonly string _xmlElementName;
        private readonly string _xmlElementNameForWrite;
        private readonly PropertyDefinitionFlags _flags;

        internal PropertyDefinition(string xmlElementName)
        {
            if (xmlElementName == null) 
                throw new ArgumentNullException("xmlElementName");
            _xmlElementName = _xmlElementNameForWrite = xmlElementName;
            _flags = PropertyDefinitionFlags.None;
        }

        internal PropertyDefinition(string xmlElementName, PropertyDefinitionFlags flags)
        {
            if (xmlElementName == null) 
                throw new ArgumentNullException("xmlElementName");
            _xmlElementName = _xmlElementNameForWrite = xmlElementName;
            _flags = flags;
        }

        internal PropertyDefinition(string xmlElementName, string xmlElementNameForWrite, PropertyDefinitionFlags flags)
        {
            if (xmlElementName == null) 
                throw new ArgumentNullException("xmlElementName");
            if (xmlElementNameForWrite == null) 
                throw new ArgumentNullException("xmlElementNameForWrite");
            _xmlElementName = xmlElementName;
            _xmlElementNameForWrite = xmlElementNameForWrite;
            _flags = flags;
        }

        public string Name
        {
            get
            {
                if (string.IsNullOrEmpty(_name))
                {
                    ServiceObjectSchema.InitializeSchemaPropertyNames();
                }
                return _name;
            }
            internal set
            {
                _name = value;
            }
        }

        public string XmlElementName
        {
            get { return _xmlElementName; }
        }

        public string XmlElementNameForWrite
        {
            get { return _xmlElementNameForWrite; }
        }

        public Type PropertyType { get; protected set; }

        internal virtual bool IsNullable
        {
            get
            {
                return true;
            }
        }

        internal bool HasFlag(PropertyDefinitionFlags flag)
        {
            return (_flags & flag) == flag;
        }

        internal abstract void LoadPropertyValueFromXml(MyVrmServiceXmlReader reader, PropertyBag propertyBag);
        internal abstract void WritePropertyValueToXml(MyVrmXmlWriter writer, PropertyBag propertyBag);
    }
}
