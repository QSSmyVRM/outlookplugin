﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.Reflection;
using System.Resources;
using System.Windows.Forms;
using Microsoft.Office.Interop.Outlook;
using Microsoft.Office.Tools.Ribbon;
using MyVrm.Outlook;
using MyVrm.Outlook.WinForms;
using MyVrm.Outlook.WinForms.Options;
using MyVrm.WebServices.Data;
using Application = Microsoft.Office.Interop.Outlook.Application;
using Exception = System.Exception;
using Image = System.Drawing.Image;

namespace MyVrmAddin2007
{
	public partial class MainRibbon : RibbonBase
	{
		private string selectedTemplateID;
		public MainRibbon():base(Globals.Factory.GetRibbonFactory())
		{
            System.Threading.Thread.CurrentThread.CurrentUICulture = System.Threading.Thread.CurrentThread.CurrentCulture;
			InitializeComponent();
			selectedTemplateID = string.Empty;
			//Texts localization
			generalGroup.Label = Strings.General;
			roomsCalendarButton.Label = Strings.RoomsCalendar;
		    roomsCalendarButton.SuperTip = Strings.RoomsCalendarButtonTip;
			templatesGroup.Label = Strings.Templates;
			templateMenu.Label = Strings.CreateFromTemplate;
		    templateMenu.SuperTip = Strings.CreateFromTemplateButtonTip;
			preferencesGroup.Label = Strings.Preferences;
			favoriteRoomsButton.Label = Strings.FavoriteRooms;
		    favoriteRoomsButton.SuperTip = Strings.FavoritRoomsButtonTip;
			groupShow.Label = Strings.Show;
			conferenceButton.Label = Strings.Conference;
		    conferenceButton.SuperTip = Strings.ConferenceButtonTip;
			optionsGroup.Label = Strings.OptionsMenuPoint;
			optionsButton.Label = Strings.OptionsMenuPoint;
		    optionsButton.SuperTip = Strings.OptionsButtonTip;
		    approveConferenceButton.Label = Strings.ConferenceApproval;
		    approveConferenceButton.SuperTip = Strings.ConferenceApprovalButtonTip;
		}

		private void MainRibbon_Load(object sender, RibbonUIEventArgs e)
		{
            try
            {

                MyVrmAddin.ProductDisplayName = Properties.Settings.Default["ProductDisplayName"] as string;
                tabMyVrm.Label = MyVrmAddin.ProductDisplayName;

                if (MyVrmAddin.Logo65 == null)
                {
                    string settingValue = Properties.Settings.Default["Logo65x65"] as string;
                    MyVrmAddin.Logo65 = Properties.Resources.Logo65x65; //default
                    if (!string.IsNullOrEmpty(settingValue))
                        MyVrmAddin.Logo65 = Properties.Resources.ResourceManager.GetObject(settingValue) as Bitmap;
                }

                conferenceButton.Image = (Image)MyVrmAddin.Logo65;

                if (String.IsNullOrEmpty(MyVrmAddin.BuildType))
                {
                    MyVrmAddin.BuildType = Properties.Settings.Default["BuildType"] as string;
                }
                //For York version - hide all points except "New Conference" and "Options")
                if (MyVrmAddin.BuildType == "03")
                {
                    generalGroup.Visible = false;
                    roomsCalendarButton.Visible = false;
                    approveConferenceButton.Visible = false;
                    templatesGroup.Visible = false;
                    templateMenu.Visible = false;
                    preferencesGroup.Visible = false;
                    favoriteRoomsButton.Visible = false;
                }
            }
            catch (Exception exception)
            {
                MyVrmAddin.TraceSource.TraceException(exception, true);
            }
		}

		private void favoriteRoomsButton_Click(object sender, RibbonControlEventArgs e)
		{
			ConferenceRibbon.favoriteRoomsButton_OnClick();
		}

		private void roomsCalendarButton_Click(object sender, RibbonControlEventArgs e)
		{
			ConferenceRibbon.roomsCalendarButton_OnClick();
		}

		private void approveConferenceButton_Click(object sender, RibbonControlEventArgs e)
		{
			ConferenceRibbon.approvalConferencesButton_OnClick();
		}

		private void templateMenu_ItemsLoading(object sender, RibbonControlEventArgs e)
		{
			templateMenu_OnItemsLoading(templateMenu, checkBox_Click, selectedTemplateID);
		}
        public void templateMenu_OnItemsLoading(RibbonMenu menu, RibbonControlEventHandler handler, string selected)
        {
            GetTemplateListResponse ret = null;

            var cursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                ret = MyVrmService.Service.GetTemplateList(GetTemplateListResponse.SortByType.ByName);
            }
            catch (Exception exception)
            {
                MyVrmAddin.TraceSource.TraceException(exception, true);
                UIHelper.ShowMessage(exception.Message, MessageBoxButtons.OK,
                                         MessageBoxIcon.Exclamation);
            }
            finally
            {
                Cursor.Current = cursor;
            }

            if (ret != null && ret.templateList != null)
            {
                menu.Items.Clear();

                foreach (TemplateInfo templateInfo in ret.templateList)
                {
                    RibbonToggleButton rt = this.Factory.CreateRibbonToggleButton();
                    rt.Label = templateInfo.Name;
                    rt.Visible = true;
                    rt.Tag = templateInfo.ID;
                    rt.Enabled = true;
                    rt.Checked = selected == templateInfo.ID;
                    rt.Click += handler;

                    menu.Items.Add(rt);
                }
            }
        }

		public void DisEnable_AfterSave(object sender, EventArgs e)
		{
			var inspector = (Inspector)Context;
			var conferenceWrapper = Globals.ThisAddIn.GetConferenceWrapperForAppointment(
					(AppointmentItem)inspector.CurrentItem);
			if (conferenceWrapper != null)
				conferenceWrapper.AfterSave -= DisEnable_AfterSave;
		}
		private void checkBox_Click(object sender, RibbonControlEventArgs e)
		{
			GetTemplateResponse response = ConferenceRibbon.checkBox_OnClick(sender, selectedTemplateID);
			if (response != null)
			{
				//OutlookAppointmentImpl apptImpl;

				AppointmentItem appItem = (AppointmentItem)Globals.ThisAddIn.Application.CreateItem(OlItemType.olAppointmentItem);
                appItem.MeetingStatus = OlMeetingStatus.olMeeting;//102148

				ThisAddIn.Sub_SetAppointmentParamsFromTemplate(appItem, response.Conference, Type.Missing);
			}
		}

		private void optionsButton_Click(object sender, RibbonControlEventArgs ev)
		{
			using (var optDlg = new OptionsAsDlg())
			{
				CancelEventArgs e = new CancelEventArgs();
				if (optDlg.ShowDialog() == DialogResult.OK)
					optDlg.optionsControl.External_OnApplying(e);
			}
		}

		private void conferenceButton_Click(object sender, RibbonControlEventArgs e)
		{
			try
			{
				AppointmentItem appItem = (AppointmentItem) Globals.ThisAddIn.Application.CreateItem(OlItemType.olAppointmentItem);
				appItem.Display(Type.Missing);
                appItem.MeetingStatus = OlMeetingStatus.olMeeting;//102148
				appItem.GetInspector.SetCurrentFormPage("MyVrmAddin2007.ConferenceFormRegion");
			}
			catch (Exception exception)
			{
				MyVrmAddin.TraceSource.TraceException(exception);
				UIHelper.ShowError(exception.Message);
			}
		}
	}
}
