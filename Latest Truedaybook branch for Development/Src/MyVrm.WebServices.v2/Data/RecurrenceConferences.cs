﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace MyVrm.WebServices.Data
{
	public class Occurrence
	{
		public int Conflict { set; get; }
		public DateTime StartDate { set; get; }
		public int StartHour { set; get; }
		public int StartMin { set; get; }
		public string StartSet { set; get; }
		public TimeSpan DurationMin { set; get; } // int??
		public Occurrence()
		{
		}
	}

	public class RecurrenceConferencesResponse : ServiceResponse
	{
		//private const string conflictPathFilter = "./dateTime[conflict!=\"0\" and conflict!=\"\"]";
		private const string DateListTagName = "dateList";
		private const string DateTimeTagName = "dateTime";

		private const string ConflictTagName = "conflict";
		private const string StartDateTagName = "startDate";
		private const string StartHourTagName = "startHour";
		private const string StartMinTagName = "startMin";
		private const string StartSetTagName = "startSet";
		private const string DurationMinTagName = "durationMin";
		
		public bool HasConflicts { private set; get; }

	    public IEnumerable<Occurrence> Occurences
	    {
	        get { return _occurences; }
	    }

	    private readonly List<Occurrence> _occurences = new List<Occurrence>();
        
        internal void ReadElementsFromXml_Only_Check_Conflicts_Presence(MyVrmServiceXmlReader reader)
        {
			//XElement root = XElement.Parse("<dateList>"+ reader.ReadInnerXml() + "</dateList>");
			//IEnumerable<XElement> conflicts =
			//    ////Just LINQ
			//    //from elem in root.Elements("dateTime")
			//    //where elem.Element(conflictTagName).Value != "" && elem.Element("conflict").Value != "0"

			//    //with XPath
			//    from elem in root.XPathSelectElements(conflictPathFilter)
			//    select elem;
			//hasConflicts = conflicts.Count() > 0 ? true : false;


			do
			{
				//Forward till "conflict" element (if none of conflicts has already been found) or 
				//till the end of "dateList" node
				while (!reader.IsEndElement(XmlNamespace.NotSpecified, DateListTagName))
				{
					if (HasConflicts == false && reader.IsStartElement(XmlNamespace.NotSpecified, ConflictTagName))
						break;

					reader.Read();
				}
				//Check "conflict" value if conflict presence is not already found and "dateList" node end is not achieved
				if (HasConflicts == false && !reader.IsEndElement(XmlNamespace.NotSpecified, DateListTagName))
				{
					if (reader.LocalName == ConflictTagName && !reader.IsEmptyElement)
					{
						int iRet = 0;
						int.TryParse(reader.ReadElementValue(), out iRet);
						HasConflicts |= iRet > 0;
					}
					else
					{
						reader.SkipCurrentElement();
					}
				}
			} while (!reader.IsEndElement(XmlNamespace.NotSpecified, DateListTagName)); //read till the "dateList" node end 
        }

		//Read occurences and also check conflicts 
		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
        {
			int iRet = 0;

			do
			{
				//Forward till "conflict" element (if none of conflicts has already been found) or 
				//till the end of "dateList" node
				while (!reader.IsEndElement(XmlNamespace.NotSpecified, DateListTagName))
				{
					Occurrence anOccurence = null;
                    //while (!reader.IsStartElement(XmlNamespace.NotSpecified, DateTimeTagName)) //ZD 102047  start
                    while (!reader.IsStartElement(XmlNamespace.NotSpecified, DateTimeTagName) && !reader.IsEndElement(XmlNamespace.NotSpecified, DateListTagName))
                        reader.Read();
                    if (reader.IsEndElement(XmlNamespace.NotSpecified, DateListTagName))
                        continue; //ZD 102047 End.

					if (reader.LocalName == DateTimeTagName)
					{
						anOccurence = new Occurrence();
						reader.Read();
					}
					do
					{
						if (anOccurence != null)
						{
							if (reader.LocalName == StartDateTagName)
							{
								anOccurence.StartDate = Utilities.StringToDate(reader.ReadValue()).Date + anOccurence.StartDate.TimeOfDay;
							}
							else if (reader.LocalName == StartHourTagName)
							{
								int.TryParse(reader.ReadElementValue(), out iRet);
								anOccurence.StartHour = iRet;
							}
							else if (reader.LocalName == StartMinTagName)
							{
								int.TryParse(reader.ReadElementValue(), out iRet);
								anOccurence.StartMin = iRet;
							}
							else if (reader.LocalName == DurationMinTagName)
							{
								anOccurence.DurationMin = TimeSpan.FromMinutes(reader.ReadValue<int>());
							}
							else if (reader.LocalName == StartSetTagName)
							{
								anOccurence.StartSet = reader.ReadElementValue();
							}
							else if (reader.LocalName == ConflictTagName && !reader.IsEmptyElement)
							{
								int.TryParse(reader.ReadElementValue(), out iRet);
								anOccurence.Conflict = iRet;
							}
						}
						reader.Read();
					} while (!reader.IsEndElement(XmlNamespace.NotSpecified, DateTimeTagName)); 

					if(HasConflicts == false && anOccurence != null )
						HasConflicts |= anOccurence.Conflict > 0 ;
					if (anOccurence != null)
						_occurences.Add(anOccurence);

					reader.Read();
				}
				////Check "conflict" value if conflict presence is not already found and "dateList" node end is not achieved
				//if (hasConflicts == false && !reader.IsEndElement(XmlNamespace.NotSpecified, DateListTagName))
				//{
				//    if (reader.LocalName == ConflictTagName && !reader.IsEmptyElement)
				//    {
				//        int.TryParse(reader.ReadElementValue(), out iRet);
				//        hasConflicts |= iRet > 0;
				//    }
				//    else
				//    {
				//        reader.SkipCurrentElement();
				//    }
				//}
			} while (!reader.IsEndElement(XmlNamespace.NotSpecified, DateListTagName)); //read till the "dateList" node end 
        }
    }


	internal class RecurrenceConferencesRequest : ServiceRequestBase<RecurrenceConferencesResponse>
	{
        private readonly RoomIdCollectionBase _roomIds = new RoomIDsForRecurrenceCollection();
		internal RecurrencePattern RecurrencePattern{ get; set; }
		internal AppointmentTime AppointmentTime{ get; set; }
	    
        
	    internal RoomIdCollectionBase RoomIds
	    {
	        get { return _roomIds; }
	    }

	    internal ConferenceId ConferenceId { get; set; }
		// userID - contained in base class

		internal RecurrenceConferencesRequest(MyVrmService service)
			: base(service)
        {
		}
		#region Overrides of ServiceRequestBase<RecurrenceConferencesResponse>

		internal override string GetXmlElementName()
		{
			return "getRecurDateList";  
		}

		internal override string GetCommandName()
		{
			return Constants.GetRecurDateListCommandName;//"GetRecurDateList";
		}

		internal override string GetResponseXmlElementName()
		{
			return "dateList";
		}

		internal override RecurrenceConferencesResponse ParseResponse(MyVrmServiceXmlReader reader)
		{
			var response = new RecurrenceConferencesResponse();
			response.ReadElementsFromXml(reader);
			return response;
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			if (UserId != null)
				UserId.WriteToXml(writer, "userID");
			if (ConferenceId != null && ConferenceId.Id != null)
				ConferenceId.WriteToXml(writer, "confID");
			if (AppointmentTime != null)
				AppointmentTime.WriteToXml(writer, "appointmentTime");
			if (RecurrencePattern != null)
				RecurrencePattern.WriteToXml(writer, "recurrencePattern");
			if (RoomIds != null)
				RoomIds.WriteToXml(writer, "rooms");
		}

		#endregion
	}
}
