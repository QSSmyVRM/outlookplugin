﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	internal class TDGetTemplateListRequest : ServiceRequestBase<TDGetTemplateListResponse>
	{
		internal TDGetTemplateListResponse.SortBy _sortBy = TDGetTemplateListResponse.SortBy.ByName;
		internal TDGetTemplateListRequest(MyVrmService service) : base(service)
		{
		}

		#region Overrides of ServiceRequestBase

		internal override string GetXmlElementName()
		{
			return "TDGetTemplateList";
		}

		internal override string GetCommandName()
		{
			return Constants.TDGetTemplateListCommandName;
		}

		internal override string GetResponseXmlElementName()
		{
			return "TDGetTemplateList";
		}

		internal override TDGetTemplateListResponse ParseResponse(MyVrmServiceXmlReader reader)
		{
			var response = new TDGetTemplateListResponse(); 
			response.LoadFromXml(reader, GetResponseXmlElementName());
			return response;
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			UserId.WriteToXml(writer, "userId");
			//writer.WriteElementValue(XmlNamespace.NotSpecified, "sortBy", _sortBy);
		}
		#endregion
	}

	public class TDGetTemplateListResponse : ServiceResponse
	{
		public enum SortBy
		{
			ByName = 1,
			ByOwner = 2,
			ByPublic = 3,
			ByOrderId = 4
		}

		public TDBTemplateCollection TemplateCollection { get; internal set; }
		public string RetMessage { set; get; }
        public int RetCode { set; get; }
		public long SettingsVersion { set; get; }
		public XMLError XMLError { set; get; }

		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{
			bool bErrorTransferred = false;
			while (!reader.IsEndElement(XmlNamespace.NotSpecified, "templates") && !bErrorTransferred)
			{
				try
				{
					if(reader.IsStartElement(XmlNamespace.NotSpecified, "retState"))
					{
						bErrorTransferred = true;
						break;
					}

					if(reader.IsStartElement(XmlNamespace.NotSpecified, "template"))
					{
						TDBTemplate tdbTemplate = new TDBTemplate();
						tdbTemplate.LoadFromXml(reader, "template");
						if (TemplateCollection == null)
							TemplateCollection = new TDBTemplateCollection();
						TemplateCollection.Add(tdbTemplate);
					}
					reader.Read();
				}
				catch (Exception ex)
				{
					MyVrmService.TraceSource.TraceError(reader.LocalName + ": " + ex.Message, null);
					reader.Read();
					//throw;
				}
			}
			
			while (!reader.IsEndElement(XmlNamespace.NotSpecified, "retState"))
			{
				try
				{
					switch (reader.LocalName)
					{
						case "retCode":
							RetCode = reader.ReadElementValue<int>();
							break;
						case "retMessage":
							RetMessage = reader.ReadElementValue();
							break;
						case "settingsVersion":
							SettingsVersion = reader.ReadElementValue<long>();
							break;
						case "error":
							XMLError = new XMLError();
							XMLError.LoadFromXml(reader, XmlNamespace.NotSpecified, "error");
							break;
					}
					reader.Read();
				}
				catch (Exception ex)
				{
					MyVrmService.TraceSource.TraceError(reader.LocalName + ": " + ex.Message, null);
					reader.Read();
					//throw;
				}
			}
		}
	}
}
