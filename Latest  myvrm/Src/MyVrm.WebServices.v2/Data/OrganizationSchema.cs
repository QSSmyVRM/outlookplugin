﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    internal class OrganizationSchema : ServiceObjectSchema
    {
        internal static readonly PropertyDefinition Id;
        internal static readonly PropertyDefinition Name;

        public static readonly OrganizationSchema Instance;

        static OrganizationSchema()
        {
            Id = new ComplexPropertyDefinition<OrganizationId>("OrgId", () => new OrganizationId());
            Name = new StringPropertyDefinition("OrganizationName", PropertyDefinitionFlags.CanSet);

            Instance = new OrganizationSchema();
        }

        internal override void RegisterProperties()
        {
            base.RegisterProperties();
            RegisterProperty(Id);
            RegisterProperty(Name);
        }
    }
}
