﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Linq;
using Microsoft.Office.Interop.Outlook;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook
{
    public class OutlookAppointmentImplCollection : OutlookAppointmentBaseCollection<OutlookAppointmentImpl>
    {
        #region Overrides of OutlookAppointmentBaseCollection

        public OutlookAppointmentImpl this[Guid id]
        {
            get
            {
                return this.FirstOrDefault(appointment => appointment.Id == id);
            }
        }

        public override OutlookAppointmentImpl this[object item]
        {
            get
            {
                var appointmentItem = item as AppointmentItem;
                if (appointmentItem == null)
                {
                    throw new ArgumentException("", "item");
                }
                return this.FirstOrDefault(appointment => appointment.Item == appointmentItem);
            }
        }

        public override OutlookAppointmentImpl this[ConferenceId conferenceId]
        {
            get 
            { 
                if (conferenceId == null) 
                    throw new ArgumentNullException("conferenceId");
                return this.FirstOrDefault(appointment => appointment.ConferenceId == conferenceId);
            }
        }

        #endregion
    }
}