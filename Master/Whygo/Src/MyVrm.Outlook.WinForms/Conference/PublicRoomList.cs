﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Text;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Card;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Views.Layout;
using DevExpress.XtraGrid.Views.Layout.ViewInfo;
using DevExpress.XtraLayout;
using DevExpress.XtraLayout.Utils;
using MyVrm.Outlook.WinForms.Calendar;
using MyVrm.WebServices.Data;
using Image = System.Drawing.Image;

namespace MyVrm.Outlook.WinForms.Conference
{
	public partial class PublicRoomList : Dialog
	{
		private int _storedAddRemoveWidth;
		//private int _emptySpaceCalendarLeftWidth;
		public enum CloseDlgReason
		{
			None,
			BaseView,
			AdvView,
			CloseCancel,
			CloseOK
		}
		public  List<PublicRoom> PublicRooms = new List<PublicRoom>();
		public RoomSelectionDialog.ViewType ViewType;
		public CloseDlgReason CloseReason;
		ToolTipController _toolTipController = new ToolTipController();
		public bool IsFavoriteChecked
		{
			set { checkEditShowFavorites.Checked = value; }
			get { return checkEditShowFavorites.Checked; }
		}
		public bool IsOnlyPublicChecked
		{
			set { checkEditPublicOnly.Checked = value; }
			get { return checkEditPublicOnly.Checked; }
		}

		public string SearchForText
		{
			set { textEditSearchFor.Text = value; }
			get { return textEditSearchFor.Text; }
		}
		public void SetComboBoxSearchColumnValue(string colName, string value)
		{
			int i = comboBoxSearchColumn.Properties.Items.IndexOf(colName);
			if (i != -1)
			{
				comboBoxSearchColumn.SelectedIndex = i;
				SearchForText = value;
			}
			else
			{
				SearchForText = string.Empty;
			}
		}
		
		private RoomId[] _preselectedRoomIds;
		public RoomId[] SelectedRooms
		{
			get
			{
				//var roomIds = new RoomId[_selectedRooms.Count];
				//for (var i = 0; i < _selectedRooms.Count; i++)
				//{
				//    var room = _selectedRooms[i];
				//    roomIds[i] = room.Id;
				//}
				//return roomIds;
				//return _preselectedRoomIds;
				return GetSelected();
			}
			set
			{
				_preselectedRoomIds = new RoomId[value.Length];
				value.CopyTo(_preselectedRoomIds, 0 );
				//_preselectedRoomIds = value;
			}
		}
		
		public  List<PublicRoom> GetStubData()
		{
			return PublicRooms;
		}

		public PublicRoomList()
		{
			InitializeComponent();
			ApplyEnabled = false;
			ApplyVisible = false;
			_storedAddRemoveWidth = emptySpaceBelowAddRemoveBn.Width;
			//_emptySpaceCalendarLeftWidth = emptySpaceCalendarLeft.Width;
			checkEditCardMode.Checked = true;
			checkEditGridMode.Checked = false;
			//PublicRooms = new List<PublicRoom>
			//                {
			//                    new PublicRoom(1, "room 1", PublicRoom.ERoomType.PrivateRoom, Properties.Resources.room23,
			//                        "MOSCOW, Smolensky Passage", "Russian Federation", 10), //"ISDN OR IP"),
			//                        //Images.User.ToBitmap()),
			//                    new PublicRoom(2, "room 2", PublicRoom.ERoomType.PrivateRoom, Images.Video.ToBitmap(),
			//                        "MOSCOW, Nordstar Tower", "Russian Federation", 5), // "IP"),
			//                    new PublicRoom(3, "room 3", PublicRoom.ERoomType.PublicRoom, null,//Images.Add,
			//                        "MOSCOW, Embankment Tower", "Russian Federation", 8), // "ISDN")
			//                    new PublicRoom(25, "Masha's room", PublicRoom.ERoomType.PrivateRoom,  null,
			//                        "MOSCOW, somewhere", "RF", 7 ),
			//                    new PublicRoom(16, "294", PublicRoom.ERoomType.PrivateRoom,  null,
			//                        "MOSCOW, somewhere 2", "RF", 17 )
			//                };
			//PublicRooms[0].IsFavorite = true;

			colRoomType.DisplayFormat.Format = new CustomRoomTypeFormatter();
			colRoomType1.DisplayFormat.Format = new CustomRoomTypeFormatter();

			publicRoomBindingSource.DataSource = PublicRooms;
			foreach (var view in gridControl_RoomsInfo.ViewCollection)
			{
				if( view.GetType() == typeof(LayoutView))
				{
					gridControl_RoomsInfo.MainView = (BaseView)view;
					break;
				}
			}
			
			foreach (GridColumn column in ((ColumnView)gridControl_RoomsInfo.MainView).Columns)
            {
				if (column.FieldName != "Picture" && column.FieldName != "ID" &&
					column.FieldName != "IsFavorite" && column.FieldName != "RoomType" &&
					!string.IsNullOrEmpty(column.FieldName))
                    comboBoxSearchColumn.Properties.Items.Add(column.GetTextCaption());
            }
			comboBoxSearchColumn.SelectedIndex = 0;
		}

		private void checkEditGridMode_CheckedChanged(object sender, EventArgs e)
		{
			if (checkEditGridMode.Checked) //current state
			{
				checkEditCardMode.Checked = false;
				
//				emptySpaceItem3.Visibility = LayoutVisibility.Always;
				emptySpaceBelowAddRemoveBn.Width = _storedAddRemoveWidth;
				emptySpaceAboveAddRemoveBn.Visibility = LayoutVisibility.Always;
				layoutControlItemBnAdd.Visibility = LayoutVisibility.Always;
				layoutControlItemBnRemove.Visibility = LayoutVisibility.Always;

				foreach (var view in gridControl_RoomsInfo.ViewCollection)
				{
					if (view.GetType() == typeof(GridView))
					{
						gridControl_RoomsInfo.MainView = (BaseView)view;
						break;
					}
				}
				//emptySpaceCalendarLeft.Width = (layoutControlGroup3.Width - calendarLayoutControlItem.Width - 10) / 2;
				//emptySpaceCalendarRight.Width = emptySpaceCalendarLeft.Width;
			}
		}

		private void checkEditCardMode_CheckedChanged(object sender, EventArgs e)
		{
			if (checkEditCardMode.Checked)
			{
				checkEditGridMode.Checked = false;

				emptySpaceBelowAddRemoveBn.Width = emptySpaceBelowAddRemoveBn.MinSize.Width;
				//emptySpaceItem3.Visibility = LayoutVisibility.Never;
				emptySpaceAboveAddRemoveBn.Visibility = LayoutVisibility.Never;
				layoutControlItemBnAdd.Visibility = LayoutVisibility.Never;
				layoutControlItemBnRemove.Visibility = LayoutVisibility.Never;

				//gridControl_RoomsInfo.MainView = gridControl_RoomsInfo.ViewCollection[1];
				foreach (var view in gridControl_RoomsInfo.ViewCollection)
				{
					if (view.GetType() == typeof(LayoutView))
					{
						gridControl_RoomsInfo.MainView = (BaseView)view;
						break;
					}
				}

				//emptySpaceCalendarLeft.Width = (layoutControlGroup3.Width - calendarLayoutControlItem.Width - 10)/2;
				//emptySpaceCalendarRight.Width = emptySpaceCalendarLeft.Width;
			}
		}

		void ShowDetails()
		{
			object room = gridControl_RoomsInfo.MainView.GetRow(((ColumnView)gridControl_RoomsInfo.MainView).FocusedRowHandle) as PublicRoom;
			if (room != null)
			{
				RoomId roomId = new RoomId(((PublicRoom) room).ID.ToString());
				if (((PublicRoom)room).RoomType == PublicRoom.ERoomType.PublicRoom)
				{
					using (var dialog = new RoomInfo(roomId))
					{
						dialog.ShowDialog(this);
					}
				}
				else //Private - show old info
				{
					using (var dialog = new RoomInformationDialog())
					{
						var roomProfile = MyVrmAddin.Instance.GetRoomFromCache(roomId);
						dialog.RoomProfile = roomProfile;
						dialog.ShowDialog(this);
					}
				}
			}
		}
		
		private void ShowDetails(object sender, EventArgs e)
		{
			ShowDetails();
		}

		private void AddRoomToConference()
		{
			object room = gridControl_RoomsInfo.MainView.GetRow(((ColumnView)gridControl_RoomsInfo.MainView).FocusedRowHandle) as PublicRoom;
			if (room != null)
			{
				string roomName = ((PublicRoom) room).Name;
				if (!roomListBoxControl.Items.Contains(roomName))
					roomListBoxControl.Items.Add(((PublicRoom) room).Name);
			}
			EnableCalendarButton();
		}
		private void RemoveRoomFromConference()
		{
			if (roomListBoxControl.SelectedItem != null)
				roomListBoxControl.Items.Remove(roomListBoxControl.SelectedItem);
			EnableCalendarButton();
		}

		private void repositoryAddRoom_Click(object sender, EventArgs e)
		{
			AddRoomToConference();
		}

		private void roomListBoxControl_DoubleClick(object sender, EventArgs e)
		{
			RemoveRoomFromConference();
		}

		private void gridControl_RoomsInfo_DoubleClick(object sender, EventArgs e)
		{
			AddRoomToConference();
		}

		private void comboBoxSearchColumn_SelectedIndexChanged(object sender, EventArgs e)
		{
			textEditSearchFor.Text = "";
		}

		private void textEditSearchFor_EditValueChanged(object sender, EventArgs e)
		{
			List<PublicRoom> subsetPublicRooms = null;
			
			bool bIsFavorite = checkEditShowFavorites.Checked;
			bool bIsPublicOnly = checkEditPublicOnly.Checked;
			//bool bIsPublicAndPrivate = checkEditPublicAndPrivate.Checked;

			if(string.IsNullOrEmpty(textEditSearchFor.Text))
			{
				//publicRoomBindingSource.DataSource =
					subsetPublicRooms =
								new List<PublicRoom>(PublicRooms.FindAll(room => room.Name.StartsWith(textEditSearchFor.Text, StringComparison.InvariantCultureIgnoreCase)));
			}
			else
			{
				GridColumn selectedColumn = null;
				foreach (GridColumn column in ((ColumnView)gridControl_RoomsInfo.MainView).Columns)
				{
					if (column.FieldName != "Picture" && !string.IsNullOrEmpty(column.FieldName))
					{
						if (column.GetTextCaption() == comboBoxSearchColumn.SelectedItem.ToString())
						{
							selectedColumn = column;
							break;
						}
					}
				}
				if (selectedColumn != null)
				{
					switch(selectedColumn.FieldName)
					{
						case "Room Name":
						case "Name":
							subsetPublicRooms =
								new List<PublicRoom>(PublicRooms.FindAll(room => !string.IsNullOrEmpty(room.Name) &&
									room.Name.StartsWith(textEditSearchFor.Text, StringComparison.InvariantCultureIgnoreCase)));
							break;
						case "ID":
							subsetPublicRooms =
								new List<PublicRoom>(PublicRooms.FindAll(room => room.ID.ToString().Equals(textEditSearchFor.Text, StringComparison.InvariantCultureIgnoreCase)));
							break;
						case "Capacity":
							int val = 0;
							int.TryParse(textEditSearchFor.Text, out val);
							subsetPublicRooms =
								new List<PublicRoom>(PublicRooms.FindAll(room => room.Capacity >= val));
//								new List<PublicRoom>(PublicRooms.FindAll(room => room.Capacity.ToString().Equals(textEditSearchFor.Text, StringComparison.InvariantCultureIgnoreCase)));
							break;
						case "Address":
							subsetPublicRooms =
								new List<PublicRoom>(PublicRooms.FindAll(room => !string.IsNullOrEmpty(room.Address) &&
									room.Address.StartsWith(textEditSearchFor.Text, StringComparison.InvariantCultureIgnoreCase)));
							break;
						case "State":
							subsetPublicRooms =
								new List<PublicRoom>(PublicRooms.FindAll(room => !string.IsNullOrEmpty(room.State) &&
									room.State.StartsWith(textEditSearchFor.Text, StringComparison.InvariantCultureIgnoreCase)));
							break;
					}
				}
			}
			if (bIsFavorite)
			{
				subsetPublicRooms = subsetPublicRooms.FindAll(room => room.IsFavorite);
			}
			else
			{
				if (bIsPublicOnly)
				{
					subsetPublicRooms = subsetPublicRooms.FindAll(room => room.RoomType == PublicRoom.ERoomType.PublicRoom);
				}
				else
				{
					//if (!bIsPublicAndPrivate)
						subsetPublicRooms = subsetPublicRooms.FindAll(room => room.RoomType == PublicRoom.ERoomType.PrivateRoom);
				}
			}

			gridControl_RoomsInfo.DataSource = subsetPublicRooms;
		}

		private void checkEditXX_CheckedChanged(object sender, EventArgs e)
		{
			if(sender.Equals(checkEditShowFavorites) )
			{
				if (checkEditShowFavorites.Checked)
				{
					checkEditPublicOnly.Checked = !checkEditShowFavorites.Checked;
					//checkEditPublicAndPrivate.Checked = !checkEditShowFavorites.Checked;
				}
			}
			else
			{
				if (sender.Equals(checkEditPublicOnly))
				{
					if (checkEditPublicOnly.Checked)
					{
						checkEditShowFavorites.Checked = !checkEditPublicOnly.Checked;
						//checkEditPublicAndPrivate.Checked = !checkEditPublicOnly.Checked;
					}
				}
				else
				{
					//if (sender.Equals(checkEditPublicAndPrivate))
					//{
					//    if (checkEditPublicAndPrivate.Checked)
					//    {
					//        checkEditPublicOnly.Checked = !checkEditPublicAndPrivate.Checked;
					//        checkEditShowFavorites.Checked = !checkEditPublicAndPrivate.Checked;
					//    }
					//}
				}
			}
			textEditSearchFor_EditValueChanged(sender, e);
		}

		void EnableCalendarButton()
		{
			//bool bRet = false;
			//List<RoomId> roomList = new List<RoomId>();

			//foreach (string room in roomListBoxControl.Items)
			//{
			//    PublicRoom found = PublicRooms.Find(aRoom => aRoom.Name == room);
			//    if (found != null && found.RoomType == PublicRoom.ERoomType.PrivateRoom)
			//    {
			//        bRet = true;
			//        break;
			//    }
			//}
			//roomsCalendarButton.Enabled = bRet;
			roomsCalendarButton.Enabled = true;
		}

		private void roomsCalendarButton_Click(object sender, EventArgs e)
		{
			List<RoomId> roomList = new List<RoomId>();

			foreach (string room in roomListBoxControl.Items)
			{
				PublicRoom found = PublicRooms.Find(aRoom => aRoom.Name == room);
				if(found != null /*&& found.RoomType == PublicRoom.ERoomType.PrivateRoom*/)
				{
					roomList.Add(new RoomId(found.ID.ToString())); 
				}
			}
			if (roomList.Count > 0)
			{
				using (var dlg = new CalendarForSelectedRooms(roomList))
				{
					dlg.ShowDialog(this);
				}
			}
		}

		private void PublicRoomList_Load(object sender, EventArgs e)
		{
			if(!checkEditShowFavorites.Checked && !checkEditPublicOnly.Checked)
				gridControl_RoomsInfo.DataSource = PublicRooms;
			textEditSearchFor_EditValueChanged(sender, e);
			foreach (RoomId pre in _preselectedRoomIds)
			{
				PublicRoom found = PublicRooms.Find(aRoom => aRoom.ID.ToString() == pre.Id);
				if(found != null)
				{
					roomListBoxControl.Items.Add(found.Name);
				}
			}
			//if (!checkEditShowFavorites.Checked)
			//	checkEditPublicAndPrivate.Checked = true;
			
			//_toolTipController.SetToolTip(gridControl_RoomsInfo, "Double click on a room to add it to the list of Selected rooms");
			gridControl_RoomsInfo.ToolTipController = _toolTipController;

			foreach (EditorButton btn in repositoryItemButtonEdit1.Buttons)
			{
				btn.ToolTip = Strings.AdvancedInfoToolTipText;
			}
			foreach (EditorButton btn in repositoryItemButtonEdit2.Buttons)
			{
				btn.ToolTip = Strings.AdvancedInfoToolTipText;
			}
			EnableCalendarButton();
			layoutViewColumn4.LayoutViewField.SizeConstraintsType = SizeConstraintsType.Custom;
			layoutViewColumn8.LayoutViewField.SizeConstraintsType = SizeConstraintsType.Custom;
			
			//layoutViewColumn4.BestFit();
			//layoutViewColumn8.BestFit();

			//layoutViewColumn4.LayoutViewField.Height = 16;
			//layoutViewField4.Height = 16;
		}

		private RoomId [] GetSelected()
		{
			RoomId[] ret = new RoomId[roomListBoxControl.Items.Count];
			for (var i = 0; i < roomListBoxControl.Items.Count; i++)
			{
				PublicRoom found = PublicRooms.Find(aRoom => aRoom.Name == (string) roomListBoxControl.Items[i]);
				if (found != null)
				{
					ret[i] = new RoomId(found.ID.ToString());
				}
			}
			return ret;
		}

		private void gridControl_RoomsInfo_MouseHover(object sender, EventArgs e)
		{
			if (checkEditGridMode.Checked)
			{
				gridControl_RoomsInfo.ToolTipController.ShowHint("Double click on a room to add it to the list of Selected rooms");
			}
		}

		private void addButton_Click(object sender, EventArgs e)
		{
			AddRoomToConference(); 
		}

		private void removeButton_Click(object sender, EventArgs e)
		{
			RemoveRoomFromConference(); 
		}

		private void checkEditBaseMode_CheckedChanged(object sender, EventArgs e)
		{
			ViewType = RoomSelectionDialog.ViewType.Basic;
			CloseReason = CloseDlgReason.BaseView;
			Close();
		}

		private void checkEditAdvMode_CheckedChanged(object sender, EventArgs e)
		{
			ViewType = RoomSelectionDialog.ViewType.Advanced;
			CloseReason = CloseDlgReason.AdvView;
			Close();
		}

		void PublicRoomList_Closed(object sender, System.EventArgs e)
		{
			if( CloseReason == CloseDlgReason.None)
				CloseReason = DialogResult == DialogResult.OK ? CloseDlgReason.CloseOK : CloseDlgReason.CloseCancel;
		}
	}
	/**/
	public class PublicRoom
	{
		public enum ERoomType
		{
			PrivateRoom,
			PublicRoom
		}

		public int ID { get; set; }
		public string Name { get; set; }
		public string Address { get; set; }
		public string State { get; set; }
		public int Capacity { get; set; }
		//public string CallType { get; set; }
		public Image Picture { get; set; }
		public ERoomType RoomType { get; set; }
		public bool IsFavorite { get; set; }
		public PublicRoom(int id, string name, ERoomType type,
			Image pic, string address, string state, int capacity, bool isFavorite)//, string callType)
		{
			ID = id;
			Name = name;
			Picture = pic;
			RoomType = type;
			Address = address;
			State = state;
			Capacity = capacity;
			IsFavorite = isFavorite;
			//CallType = callType;
		}
		public PublicRoom()
		{
		}
		public PublicRoom Clone()
		{
			PublicRoom cpy = new PublicRoom();
			cpy.ID = ID;
			cpy.Name = Name;
			cpy.Picture = Picture;
			cpy.RoomType = RoomType;
			cpy.Address = Address;
			cpy.State = State;
			cpy.Capacity = Capacity;
			cpy.IsFavorite = IsFavorite;
			return cpy;
		}
	}
	/**/
	class CustomRoomTypeFormatter : IFormatProvider, ICustomFormatter
	{
		public object GetFormat(System.Type type)
		{
			return this;
		}
		public string Format(string format, object arg, IFormatProvider formatProvider)
		{
			string formatValue = arg.ToString();
			if (formatValue.Contains("PrivateRoom"))
				return "Private Room";
			if (formatValue.Contains("PublicRoom"))
				return "Public Room";
			return formatValue;
		}
	}
}
