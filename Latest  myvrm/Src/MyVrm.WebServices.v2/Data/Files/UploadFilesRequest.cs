﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;

namespace MyVrm.WebServices.Data.Files
{
    internal class UploadFilesRequest : ServiceRequestBase<UploadFilesResponse>
    {
        public UploadFilesRequest(MyVrmService service) : base(service)
        {
            FileInfos = new List<UploadFileInfo>();
        }

        public List<UploadFileInfo> FileInfos { get; private set; }
        internal override string GetXmlElementName()
        {
            return "SaveFiles";
        }

        internal override string GetCommandName()
        {
            return "SaveFiles";
        }

        internal override string GetResponseXmlElementName()
        {
            return "SaveFiles";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            writer.WriteStartElement(XmlNamespace.NotSpecified, "Files");
            foreach (var fileInfo in FileInfos)
            {
                writer.WriteStartElement(XmlNamespace.NotSpecified, "File");
                writer.WriteElementValue(XmlNamespace.NotSpecified, "FileName", fileInfo.Name);
                writer.WriteElementValue(XmlNamespace.NotSpecified, "FilePath", string.Empty);
                writer.WriteElementValue(XmlNamespace.NotSpecified, "FileAppendStr", string.Empty);
                writer.WriteElementValue(XmlNamespace.NotSpecified, "FileasString", Convert.ToBase64String(fileInfo.Data));
                writer.WriteEndElement();

            }
            writer.WriteEndElement();
        }
    }
}