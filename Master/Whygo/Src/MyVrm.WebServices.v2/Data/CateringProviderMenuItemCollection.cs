﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.WebServices.Data
{
    public class CateringProviderMenuItemCollection : ComplexPropertyCollection<CateringProviderMenuItem>
    {
        #region Overrides of ComplexPropertyCollection<CateringProviderMenuItem>

		public override object Clone()
		{
			CateringProviderMenuItemCollection copy = new CateringProviderMenuItemCollection();
			foreach (var item in Items)
			{
				copy.InternalAdd((CateringProviderMenuItem)item.Clone()); //???????
			}

			return copy;
		}

        internal override CateringProviderMenuItem CreateComplexProperty(string xmlElementName)
        {
            return new CateringProviderMenuItem();
        }

        internal override string GetCollectionItemXmlElementName(CateringProviderMenuItem complexProperty)
        {
            return "Item";
        }

        #endregion
    }
}
