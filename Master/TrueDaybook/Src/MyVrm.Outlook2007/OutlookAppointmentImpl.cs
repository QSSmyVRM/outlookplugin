﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Microsoft.Office.Interop.Outlook;
using MyVrm.WebServices.Data;
using Exception = System.Exception;
using RecurrencePattern=MyVrm.WebServices.Data.RecurrencePattern;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace MyVrm.Outlook
{
    public partial class OutlookAppointmentImpl : OutlookAppointment
    {
		public const string VirtualConfPropName = "IsVConf";
		public const string VirtualConfIdPropName = "VConfId";
		public const string VirtualConfIsRevertedPropName = "VConfIsReverted";
    	public const string VirtualConfiCalSent = "iCalSent";

		private const string ConferenceIdPropertyName = "myVRMConfID";

        private bool _isStaticAppointment = false;
        public static string GetConferenceIdPropertyName
        {
            get{ return ConferenceIdPropertyName; }
        }

        private AppointmentItem _item;
        private OutlookRecipientImpl _currentUserRecipient;

        public OutlookAppointmentImpl(AppointmentItem  item)
        {
            Id = Guid.NewGuid();
            _item = item;
            Item.PropertyChange += ItemPropertyChanged;
            Item.Write += ItemWrite;
            ((ItemEvents_10_Event)Item).Send += ItemSend;
        }

        //List of converted from myVRM to Live Meeting conferences:
        // Key      - LM GlobalAppointmentId, which accept ConferenceId
        // Value    - ConferenceId of the converted myVRM conference
        public Dictionary<string, ConferenceId> m_ConvertedConfs = new Dictionary<string, ConferenceId>();

        public Guid Id { get; private set; }

        public override string Subject
        {
            get { return Item.Subject; }
            set 
            { 
                if (Item.Subject != value)
                {
                    Item.Subject = value;
                    NotifyPropertyChanged("Subject");
                }
            }
        }

        public override DateTime Start
        {
            get { return Item.Start; }
            set
            {
                if (Item.Start != value)
                {
                    Item.Start = value;
                    NotifyPropertyChanged("Start");
                }
            }
        }

        public override DateTime End
        {
            get { return Item.End; }
            set
            {
                if (Item.End != value)
                {
                    Item.End = value;
                    NotifyPropertyChanged("End");
                }
            }
        }

        public override TimeSpan Duration
        {
            get { return TimeSpan.FromMinutes(Item.Duration); }
            set
            {
                if (Duration != value)
                {
                    Item.Duration = value.Minutes;
                    NotifyPropertyChanged("Duration");
                }
            }
        }


        public override string Location
        {
            get { return Item.Location; }
            set
            {
                if (Item.Location != value)
                {
                    Item.Location = value;
                    NotifyPropertyChanged("Location");
                }
            }
        }

        public override ConferenceId ConferenceId
        {
            get
            {
                try
                {
                    UserProperty confIdProp = Item.UserProperties.Find(ConferenceIdPropertyName, true);
                    if (confIdProp != null && confIdProp.Type == OlUserPropertyType.olText)
                    {
                        return new ConferenceId((string)confIdProp.Value);
                    }
                }
                catch
                {
                }
                return null;
            }
            set
            {
                var confId = ConferenceId;
                if (ConferenceId != value)
                {
                    UserProperty property = confId == null
                                                ? Item.UserProperties.Add(ConferenceIdPropertyName, OlUserPropertyType.olText,
                                                                          false, null)
                                                : Item.UserProperties[ConferenceIdPropertyName];
                    property.Value = value.ToString();
                }
            }
        }

        public override event PropertyChangedEventHandler PropertyChanged;
        
        public override event CancelEventHandler Saving;

        public override event CancelEventHandler Sending;
        
        public override event CancelEventHandler BeforeDelete;

        public override ReadOnlyCollection<OutlookRecipient> Recipients
        {
            get
            {
                var recipients = new List<OutlookRecipient>();
                foreach (Recipient recipient in Item.Recipients)
                {
                    if (!recipient.Resolved)
                    {
                        recipient.Resolve();
                    }
                    if (recipient.Resolved)
                    {
                        recipients.Add(new OutlookRecipientImpl(recipient));
                    }
                }
                return new ReadOnlyCollection<OutlookRecipient>(recipients);
            }
        }

		public override void SetRecipients(IEnumerable<string> arg)
		{
			ClearRecipients();

			foreach (string outlookRecipientMail in arg)
			{
				Recipient recipient = _item.Recipients.Add(outlookRecipientMail);
				bool bret = recipient.Resolve();
				if( !recipient.Resolved )
				{
					_item.Recipients.Remove(recipient.Index);
				}
			}
		}

		public override void ClearRecipients()
		{
			try
			{
				while (_item.Recipients.Count > 0)
					_item.Recipients.Remove(_item.Recipients[_item.Recipients.Count].Index);
			}
			catch (Exception e)
			{
				MyVrmService.TraceSource.TraceInformation("Recipients.Remove Exception {0} ", e.ToString());
				throw;
			}
		}

    	public override OutlookRecipient CurrentUser
        {
            get {
                return _currentUserRecipient ??
                       (_currentUserRecipient = new OutlookRecipientImpl(Item.Session.CurrentUser));
            }
        }

        public override string Body
        {
            get { return Item.Body; }
            set
            {
                if (Item.Body != value)
                {
                    Item.Body = value;
                    NotifyPropertyChanged("Body");
                }
            }
        }

        public override string HtmlBody
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public override string RtfBody
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public override bool IsRecurring
        {
            get { return Item.IsRecurring; }
            set {}
        }

        public override RecurrenceState RecurrenceState
        {
            get
            {
                switch(Item.RecurrenceState)
                {
                    case OlRecurrenceState.olApptNotRecurring:
                        return RecurrenceState.NotRecurring;
                    case OlRecurrenceState.olApptMaster:
                        return RecurrenceState.Master;
                    case OlRecurrenceState.olApptOccurrence:
                        return RecurrenceState.Occurrence;
                    case OlRecurrenceState.olApptException:
                        return RecurrenceState.Exception;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            set {  }
        }

        public AppointmentItem Item
        {
            get { return _item; }
        }


        public override FreeBusyStatus BusyStatus
        {
            get
            {
                switch(_item.BusyStatus)
                {
                    case OlBusyStatus.olFree:
                        return FreeBusyStatus.Free;
                    case OlBusyStatus.olTentative:
                        return FreeBusyStatus.Tentative;
                    case OlBusyStatus.olBusy:
                        return FreeBusyStatus.Busy;
                    case OlBusyStatus.olOutOfOffice:
                        return FreeBusyStatus.OutOfOffice;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            set
            {
            }
        }

        public override RecurrencePattern GetRecurrencePattern(TimeZoneInfo timeZone)
        {
            var recurrencePattern = new RecurrencePattern(timeZone);
            var outlookPattern = Item.GetRecurrencePattern();
            try
            {
                recurrencePattern.RecurrenceType = (RecurrenceType)outlookPattern.RecurrenceType;
                switch (outlookPattern.RecurrenceType)
                {
                    case OlRecurrenceType.olRecursDaily:
                    {
                        recurrencePattern.DaysOfWeek = (DaysOfWeek)outlookPattern.DayOfWeekMask;
                        break;
                    }
                    case OlRecurrenceType.olRecursWeekly:
                    {
                        recurrencePattern.DaysOfWeek = (DaysOfWeek)outlookPattern.DayOfWeekMask;
                        break;
                    }
                    case OlRecurrenceType.olRecursMonthly:
                    {
                        recurrencePattern.DayOfMonth = outlookPattern.DayOfMonth;
                        break;
                    }
                    case OlRecurrenceType.olRecursMonthNth:
                    {
                        recurrencePattern.DaysOfWeek = (DaysOfWeek)outlookPattern.DayOfWeekMask;
                        break;
                    }
                    case OlRecurrenceType.olRecursYearly:
                    {
                        recurrencePattern.MonthOfYear = outlookPattern.MonthOfYear;
                        recurrencePattern.DayOfMonth = outlookPattern.DayOfMonth;
                        break;
                    }
                    case OlRecurrenceType.olRecursYearNth:
                    {
                        recurrencePattern.MonthOfYear = outlookPattern.MonthOfYear;
                        recurrencePattern.DaysOfWeek = (DaysOfWeek)outlookPattern.DayOfWeekMask;
                        break;
                    }
                }
                recurrencePattern.StartTime = outlookPattern.StartTime.TimeOfDay;
                recurrencePattern.EndTime = outlookPattern.EndTime.TimeOfDay;
                if (outlookPattern.Interval > 0)
                {
                    recurrencePattern.Interval = outlookPattern.RecurrenceType == OlRecurrenceType.olRecursYearly ||
                                                 outlookPattern.RecurrenceType == OlRecurrenceType.olRecursYearNth
                                                     ? outlookPattern.Interval/12
                                                     : outlookPattern.Interval;
                }
                recurrencePattern.Instance = outlookPattern.Instance;
                recurrencePattern.StartDate = outlookPattern.PatternStartDate;
                if (outlookPattern.NoEndDate)
                {
                    recurrencePattern.NeverEnds();
                }
                else
                {
                    recurrencePattern.EndDate = outlookPattern.PatternEndDate;
                    recurrencePattern.Occurrences = outlookPattern.Occurrences;
                }
                return recurrencePattern;
            }
            finally
            {
                if (outlookPattern != null)
                {
                    Marshal.ReleaseComObject(outlookPattern);
                }
            }
        }


        /// <summary>
        /// Removes link (user's property) from the appointment.
        /// </summary>
        /// <param name="item">Appointment item.</param>
        public static void UnlinkConference(AppointmentItem item)
        {
            if (item == null) 
                throw new ArgumentNullException("item");
            var confIdProp = item.UserProperties.Find(ConferenceIdPropertyName, true);
            if (confIdProp != null)
            {
                confIdProp.Delete();
            }
        }

        /// <summary>
        /// Removes link (user's property) from the appointment.
        /// </summary>
        public void UnlinkConference()
        {
            UnlinkConference(Item);
        }

        public static ConferenceId GetConferenceId(AppointmentItem item)
        {
            if (item == null) 
                throw new ArgumentNullException("item");
            try
            {
                UserProperty confIdProp = item.UserProperties.Find(ConferenceIdPropertyName, true);
                if (confIdProp != null && confIdProp.Type == OlUserPropertyType.olText)
                {
                    return new ConferenceId((string)confIdProp.Value);
                }
            }
            catch
            {
            }
            return null;
        }

		public static string GetVirtualConferenceId(AppointmentItem item)
		{
			if (item == null)
				throw new ArgumentNullException("item");
			try
			{
				UserProperty confIdProp = item.UserProperties.Find(VirtualConfIdPropName, true);
				if (confIdProp != null && confIdProp.Type == OlUserPropertyType.olText)// olInteger)
				{
					return (string)confIdProp.Value;
				}
			}
			catch
			{
			}
			return string.Empty;
		}
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        private void NotifySaving(ref bool cancel)
        {
            // Remove one-offed form properties to avoid displaying custom forms on higher versions of Outlook 2007,2010.
            // For security and storage reasons, Microsoft Office Outlook 2007 ignores form definitions saved with any item.
            RemoveOneOff(false);

            /**/
            if (ConferenceId == null && Item.MessageClass.Contains("IPM.Appointment.Live Meeting Request"))
            {
                if (m_ConvertedConfs.ContainsKey(Item.GlobalAppointmentID))
                {
                    ConferenceId = m_ConvertedConfs[Item.GlobalAppointmentID];
                    //???? 
                    ////We have assigned the LM conference with myVRM ConfId - remove converting record from list
                    //m_ConvertedConfs.Remove(Item.GlobalAppointmentID);
                }
            }
            /**/
            // Ignore cancelled meetings
            if (Item.MeetingStatus == OlMeetingStatus.olMeetingCanceled)
            {
                return;
            }
            if (Saving != null && RaiseSavingEvent)
            {
                var args = new CancelEventArgs();
                Saving(this, args);
                cancel = args.Cancel;
            }
        }

        /// <summary>
        /// Removes custom form definition (one-off)
        /// </summary>
        /// <param name="removePropDef"></param>
        partial void RemoveOneOff(bool removePropDef);

        partial void ItemPropertyChanged(string name);

        private void ItemWrite(ref bool cancel)
        {
            NotifySaving(ref cancel);
        }

        private void ItemSend(ref bool cancel)
        {
            NotifySending(ref cancel);
        }

        private void NotifySending(ref bool cancel)
        {
            if (Sending != null)
            {
                var args = new CancelEventArgs();
                Sending(this, args);
                cancel = args.Cancel;
            }
        }

        private void OnBeforeDelete(object item, ref bool cancel)
        {
            NotifyBeforeDelete(ref cancel);
        }

        private void NotifyBeforeDelete(ref bool cancel)
        {
            if (BeforeDelete != null)
            {
                var args = new CancelEventArgs();
                BeforeDelete(this, args);
                cancel = args.Cancel;
            }
        }

        #region Implementation of IDisposable

        public override OutlookAppointment GetOccurrence(DateTime occurrenceTime)
        {
            var outlookPattern = Item.GetRecurrencePattern();
            return new OutlookAppointmentImpl(outlookPattern.GetOccurrence(occurrenceTime));
        }

        public override void Save()
        {
            Item.Save();
        }

        public override void Delete()
        {
            Item.Delete();
        }

        public override void Dispose()
        {
            if (_item != null)
            {
                ((ItemEvents_10_Event)Item).Send -= ItemSend;
                Item.PropertyChange -= ItemPropertyChanged;
                Item.Write -= ItemWrite;
                Marshal.ReleaseComObject(_item);
                _item = null;
            }
        }

        #endregion

		public override void SetNonOutlookProperty(bool value)
		{
			if (Item != null)
			{
				if (Item.UserProperties.Find("nonOutlookPropChanged", true) == null)
					Item.UserProperties.Add("nonOutlookPropChanged", OlUserPropertyType.olYesNo, false,
																	  OlFormatYesNo.olFormatYesNoTrueFalse);
				Item.UserProperties["nonOutlookPropChanged"].Value = value;
			}
		}

		public override bool GetNonOutlookProperty()
		{
			bool flag = false;

			if (Item != null)
			{
				if (Item.UserProperties.Find("nonOutlookPropChanged", true) != null)
				{
					bool.TryParse(Item.UserProperties["nonOutlookPropChanged"].Value.ToString(), out flag);
				}
			}
			return flag;
		}

		public override bool GetIsVConfDirtyProperty()
		{
			bool flag = false;

			if (Item != null)
			{
				if (Item.UserProperties.Find("IsVConfDirty", true) != null)
				{
					bool.TryParse(Item.UserProperties["IsVConfDirty"].Value.ToString(), out flag);
				}
			}
			return flag;
		}

		public override void ClearIsVConfDirtyProperty()
		{
			if (Item.UserProperties.Find("IsVConfDirty", true) == null)
				Item.UserProperties.Add("IsVConfDirty", OlUserPropertyType.olYesNo, false, OlFormatYesNo.olFormatYesNoTrueFalse);
			Item.UserProperties["IsVConfDirty"].Value = false;
		}

		public override bool GetVirtualConfIsReverted()
		{
			bool flag = false;

			if (Item != null)
			{
				if (Item.UserProperties.Find(VirtualConfIsRevertedPropName, true) != null)
				{
					bool.TryParse(Item.UserProperties[VirtualConfIsRevertedPropName].Value.ToString(), out flag);
				}
			}
			return flag;
		}

		public override bool GetIsVirtualConf()
		{
			bool flag = false;

			if (Item != null)
			{
				if (Item.UserProperties.Find(VirtualConfPropName, true) != null)
				{
					bool.TryParse(Item.UserProperties[VirtualConfPropName].Value.ToString(), out flag);
				}
			}
			return flag;
		}

		public override void VC_CheckAndSave(ref bool Cancel,Conference conference)
		{
            
			//TrueDaybookService_v2 trueDayServices = MyVrmService.TrueDayServices;
			UserProperty vconfId = Item.UserProperties.Find(VirtualConfIdPropName, true);
			bool bOver = false;

			if (GetVirtualConfIsReverted() )
			{
				string _vconfId = vconfId != null ? ((string)(vconfId.Value)).Trim() : string.Empty;
				if ( !string.IsNullOrEmpty(_vconfId))
				{
					//trueDayServices.DeleteVConf((int)vconfId.Value, false);
					MyVrmService.Service.TDDeleteConference(_vconfId);
				}

				//Clear all virtual conference properties
				var vConfProp = Item.UserProperties.Find(VirtualConfPropName, true);
				if (vConfProp != null)
					vConfProp.Delete();
				vConfProp = Item.UserProperties.Find(VirtualConfIdPropName, true);
				if (vConfProp != null)
					vConfProp.Delete();
				vConfProp = Item.UserProperties.Find(VirtualConfIsRevertedPropName, true);
				if (vConfProp != null)
					vConfProp.Delete();
				vConfProp = Item.UserProperties.Find(VirtualConfiCalSent, true);
				if (vConfProp != null)
					vConfProp.Delete();
			}
			else
			{
				if (Item.IsRecurring)
				{
					MessageBox.Show(null, Strings.VConfCannotBeRecurringMsgTxt,
												MyVrmAddin.ProductDisplayName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
					Cancel = true;
				}

				if (Cancel != true)
					CheckVCSave(ref Cancel, ref bOver,conference.IsImmediateconference); //ZD 101226

				if (Cancel != true)
				{
					try
					{
						if (GetIsVirtualConf())
						{
							string _vconfId = vconfId != null ? ((string)(vconfId.Value)).Trim() : string.Empty;

                    //        else if (Regex.IsMatch(Name, Strings.ConferenceNameRegexPattern))
                    //{
                    //    info.ErrorText = Strings.ConferenceNameContainsInvalidCharacters;
                    //}


							if (vconfId != null && !string.IsNullOrEmpty(_vconfId))//(int)(vconfId.Value) != 0)
							{
                                ////trueDayServices.UpdateVConf((int)(vconfId.Value), Item, false);

                                //ZD 102246 start
                                TDUpdateConferenceResponse response = MyVrmService.Service.TDUpdateExternalConference((string)(vconfId.Value), Item.Subject, Item.Body, Item.Start, Item.End,
                                                                              TrueDayServices_v1.GetParticipantsList(Item), bOver);


                                TDGetDefaultSettingsResponse ret = MyVrmService.Service.TDGetDefaultSettings();
                                string body = ret.DefaultBody;
                                string[] Description;
                                string bodydes = "" ;

                                if (Item.Body.Contains(Strings.ConfDefaultBodyText_1))
                                {
                                    //Description = Item.Body.Split(stringSeparators, StringSplitOptions.None);
                                    //bodydes = Description[0];
                                    Description = Regex.Split(Item.Body, "DO NOT DELETE OR CHANGE ANY OF THE TEXT BELOW THIS LINE");
                                    //Description[0].TrimEnd("*");//Trim( new Char[] { ' ', '*', '.' } ));
                                    bodydes = Description[0].TrimEnd('*');
                                }
                                else if (Body.Contains("You have been invited to attend a SCOPIA Meeting"))
                                {
                                    Description = Regex.Split(Body, "You have been invited to attend a SCOPIA Meeting.");
                                    bodydes = Description[0].TrimEnd('\r');
                                    bodydes = Description[0].TrimEnd('\n');
                                }

                                body = TrueDaybookService_v2.ResolveBodyText(body, TrueDaybookService_v2.markerURL, response.ConferenceUrl);
                                body = TrueDaybookService_v2.ResolveBodyText(body, TrueDaybookService_v2.markerId, response.ConferenceId);
                                body = TrueDaybookService_v2.ResolveBodyText(body, TrueDaybookService_v2.markerExtId, response.ExternalId.ToString());

                                body = TrueDaybookService_v2.ResolveBodyText(body, TrueDaybookService_v2.markerExtId_MyVRM, response.ExternalId.ToString());
                                body = TrueDaybookService_v2.ResolveBodyText(body, TrueDaybookService_v2.markerURL_MyVRM, response.ConferenceUrl);

                                if (!string.IsNullOrEmpty(bodydes))
                                {
                                    Item.Body = bodydes;
                                    Item.Body += "\n";
                                    Item.Body += Strings.ConfDefaultBodyText_1;
                                    Item.Body += "\n"; //ZD 102580
                                    Item.Body += Strings.ConfDefaultBodyText_2; //ZD 102580
                                    Item.Body += "\n\n";
                                    Item.Body += body;
                                }
                                else
                                {
                                    Item.Body = "\n";
                                    Item.Body += Strings.ConfDefaultBodyText_1;
                                    Item.Body += "\n"; //ZD 102580
                                    Item.Body += Strings.ConfDefaultBodyText_2; //ZD 102580
                                    Item.Body += "\n\n";
                                    Item.Body += body;
                                }
                                
                                //ZD 102246 End

								//MyVrmService.Service.TDUpdateConference((string)(vconfId.Value), Item.Subject, Item.Body, Item.Start, Item.End,
																		//TrueDayServices_v1.GetParticipantsList(Item), bOver);
                               
							}
							else
							{

                                //Regex regex = new Regex();
                                //102237
                                string regexex = @"^[^<>&]*$";  
                                Regex checkSP = new Regex(regexex);
                                      //ZD 102684
                                    if (Item.Subject != null && !checkSP.IsMatch(Item.Subject, 0))
                                    {
                                        MessageBox.Show(null, Strings.ConferenceNameContainsInvalid,
                                                    MyVrmAddin.ProductDisplayName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                        Cancel = true;
                                    }
                                    else
                                    {
                                    //trueDayServices.ScheduleNewVConf(Item, false);
                                     //string emaildescrip= "*********DO NOT DELETE OR CHANGE ANY OF THE TEXT BELOW THIS LINE********* TrueDaybook conference information will replace this section after you save and send this invitation.";
                                      //string empty = "";
                                      // if (Item.Subject.Contains(emaildescrip))
                                        //{
                                        //    Item.Subject.Replace(emaildescrip, empty);
                                        //}

                                    TDScheduleNewConferenceResponse response = MyVrmService.Service.TDScheduleNewConference(Item.Subject, Item.Body, Item.Start, Item.End,
                                                                                 TrueDayServices_v1.GetParticipantsList(Item), bOver, conference.Rooms, conference.IsStaticConference, conference.IsImmediateconference);//ZD 101226
                                
                                    if (Item.UserProperties.Find(VirtualConfIdPropName, true) == null)
                                        Item.UserProperties.Add(VirtualConfIdPropName, OlUserPropertyType.olText/*olInteger*/, false, OlFormatText.olFormatTextText);//olFormatNumberRaw);
                                    Item.UserProperties[VirtualConfIdPropName].Value = response.ConferenceId;

                                    TDGetDefaultSettingsResponse ret = MyVrmService.Service.TDGetDefaultSettings();
                                    string body = ret.DefaultBody;
                                    body = TrueDaybookService_v2.ResolveBodyText(body, TrueDaybookService_v2.markerURL, response.ConferenceUrl);
                                    body = TrueDaybookService_v2.ResolveBodyText(body, TrueDaybookService_v2.markerId, response.ConferenceId);
                                    body = TrueDaybookService_v2.ResolveBodyText(body, TrueDaybookService_v2.markerExtId, response.ExternalId.ToString());

                                    body = TrueDaybookService_v2.ResolveBodyText(body, TrueDaybookService_v2.markerExtId_MyVRM, response.ExternalId.ToString());
                                    body = TrueDaybookService_v2.ResolveBodyText(body, TrueDaybookService_v2.markerURL_MyVRM, response.ConferenceUrl);
                                    if (!string.IsNullOrEmpty(Item.Body) && Item.Body.Contains(Strings.ConfDefaultBodyText_1)) //ZD 102580
                                      Item.Body = TrueDaybookService_v2.ResolveBodyText(Item.Body, Strings.ConfDefaultBodyText_1, string.Empty); //ZD 102580
                                    if (!string.IsNullOrEmpty(Item.Body) && Item.Body.Contains(Strings.ConfDefaultBodyText_2)) //ZD 102580
                                      Item.Body = TrueDaybookService_v2.ResolveBodyText(Item.Body, Strings.ConfDefaultBodyText_2, string.Empty); //ZD 102580
                                    if (Item.Body != null)
                                        Item.Body = Item.Body.TrimEnd();
                                    if (!string.IsNullOrEmpty(Item.Body))
                                        Item.Body += "\n\n";

                                    Item.Body += Strings.ConfDefaultBodyText_1;
                                    Item.Body += "\n"; //ZD 102580
                                    Item.Body += Strings.ConfDefaultBodyText_2; //ZD 102580
                                    Item.Body += "\n\n";

                                    Item.Body += body;
                                    ConferenceId = new ConferenceId(response.ConferenceId);
                                    }
								//MyVrmService.Service.UpdateConferenceICalUniqueId(new ConferenceId(response.ConferenceId), Item.GlobalAppointmentID);
							}
						}
					}
					catch (Exception exception)
					{
						MessageBox.Show(null, exception.Message, MyVrmAddin.ProductDisplayName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
						Cancel = true;
					}
				}
			}
		}

		internal void CheckVCSave(ref bool Cancel, ref bool Over,bool immediate)
		{
			//AppointmentItem appItem = Application.ActiveInspector().CurrentItem as AppointmentItem;
			//Microsoft.Office.Interop.Word.Document doc = Item.GetInspector.WordEditor as Microsoft.Office.Interop.Word.Document;
			//doc.Content.FormattedText
			//doc.Content.Text = "doc";

			if (Item != null)
			{
				//UserProperty found = null;
				//int foundId = 0;
				string foundId = string.Empty;
				//found = Item.UserProperties.Find(VirtualConfPropName, true);
				if (/*found != null && (bool)found.Value*/GetIsVirtualConf() && Item.Recipients.Count > 0)
				{
					foundId = GetVirtualConferenceId(Item);
					//found = Item.UserProperties.Find(VirtualConfIdPropName, true);
					//if (found != null && ((string)found.Value).Trim() (int)found.Value != 0)
					//{
					//    foundId = (int)found.Value;
					//}
					//if(foundId != 0)
                    if(!immediate)
					{
						if (Item.Start < DateTime.Now)
						{
							MessageBox.Show(null, Strings.CreateOrEditConfInPastMsgTxt,
												MyVrmAddin.ProductDisplayName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
							Cancel = true;
						}
					}
					if (!Cancel)
					{
						//TrueDaybookService_v2 trueDayServices = MyVrmService.TrueDayServices; //new TrueDayServices();
						bool bOverLapped = false;
						int //limit = trueDayServices.GetMaxAvail(false);
						limit = MyVrmService.Service.TDGetMaximumAvailableSeats();
						AvailSeatsCollection2 //ret = trueDayServices.GetAvailSeats(Item.Start, Item.End, foundId, false);
						ret = MyVrmService.Service.TDGetAvailSeats(Item.Start, Item.End, foundId);

						List<string> uniqueRecipients = new List<string>();

						string smtpAddress = string.Empty;
						foreach (Recipient recipient in Item.Recipients)
						{
							if (recipient.Resolved) //found in Exchange
							{
								var exchangeUser = recipient.AddressEntry.GetExchangeUser();
								smtpAddress = exchangeUser != null ? exchangeUser.PrimarySmtpAddress : recipient.Address;
							}
							else
							{
								smtpAddress = recipient.Name; //not found - take it's name as an e-mail address
							}

							if( uniqueRecipients.IndexOf(smtpAddress) == -1 )
							{
								uniqueRecipients.Add(smtpAddress);
							}
						}

						////Get a schedule
						//DateTime aDateTime = Item.Start;//.Date;
						//Dictionary<DateTime, int> schedule = new Dictionary<DateTime, int>();
						//foreach (AvailSeats2 data in ret)
						//{
						//    //aDateTime = Item.Start.AddTicks(data.TimeSpan.Ticks);
						//    //    //aDateTime.AddTicks(data.TimeSpan.Ticks);
						//    //schedule.Add(aDateTime, data.Seats);

						//    schedule.Add(data.Start, data.Seats);
						//}

						////Check seats number - exceed or not
						//
						//if (schedule.Count > 0)
						//{
						//    Dictionary<DateTime, int>.Enumerator en = schedule.GetEnumerator();
						//    en.MoveNext();
						//    var prevPoint = en.Current;
							
						//    do
						//    {
						//        if ((prevPoint.Key <= Item.Start && Item.Start <= en.Current.Key ||
						//             Item.Start <= prevPoint.Key && prevPoint.Key <= Item.End) &&
						//            uniqueRecipients.Count + prevPoint.Value > limit)
						//        {
						//            bOverLapped = true;
						//            break;
						//        }

						//        prevPoint = en.Current;
						//    } while (en.MoveNext());
						//}
						////if (!bOverLapped)
						////{
						////    aDateTime = Item.Start.Date;
						////    foreach (AvailSeats data in ret)
						////    {
						////        aDateTime = aDateTime.AddTicks(data.TimeSpan.Ticks);
						////        if (Item.Start <= aDateTime && aDateTime <= Item.End && uniqueRecipients.Count + data.Seats > limit || bOverLapped)
						////        {
						////            bOverLapped = true;
						////            break;
						////        }
						////    }
						////}

						//new
						foreach (AvailSeats2 data in ret)
					    {
							if ((  Item.Start <= data.Start && data.Start <= Item.End ||
								   data.Start <= Item.Start && Item.Start <= data.End )
								 && uniqueRecipients.Count + data.Seats > limit)
					        {
					            bOverLapped = true;
					            break;
					        }
					    }

						if (bOverLapped)
						{
							if (MyVrmService.Service.LoggedUserState == TrueDaybookService_v2.UserState.PowerUser)
								//trueDayServices.LoggedUserState == TrueDaybookService_v2.UserState.PowerUser)
							{
                                //ZD 100568
								Over = false;
                                Cancel = true;
                                MyVrm.Outlook.WinForms.Conference.MaxSeatExceededDialog dialog = new MyVrm.Outlook.WinForms.Conference.MaxSeatExceededDialog();
                                dialog.StartDateTime = Item != null ? Item.Start : DateTime.Now;
                                dialog.EndDateTime = Item!= null ? Item.End : dialog.StartDateTime.AddHours(1.0);
                                if (dialog.ShowDialog() == DialogResult.OK)
                                {
                                    MyVrm.Outlook.WinForms.XmlSeatsDlg dlg = new MyVrm.Outlook.WinForms.XmlSeatsDlg();
                                    dlg.ConfStart = dialog.StartDateTime;
                                    dlg.ConfEnd = dialog.EndDateTime;
                                    dlg.ConfId = string.Empty;
                                    dlg.ShowDialog();
                                }
                                //ZD 100568
								
                                //if (res == DialogResult.No)
                                //{
                                //    Cancel = true;
                                //    Over = false;
                                //}
							}
							else
							{
								//MessageBox.Show(null, Strings.RegularUserLimitExceededMsgTxt,
								//				MyVrmAddin.ProductDisplayName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                //ZD 100568
                                Over = false;
                                Cancel = true;
                                MyVrm.Outlook.WinForms.Conference.MaxSeatExceededDialog dialog = new MyVrm.Outlook.WinForms.Conference.MaxSeatExceededDialog();
                                dialog.StartDateTime = Item.Start != null ? Item.Start : DateTime.Now;
                                dialog.EndDateTime = Item.End != null ? Item.End : dialog.StartDateTime.AddHours(1.0);
                                if (dialog.ShowDialog() == DialogResult.OK)
                                {
                                    MyVrm.Outlook.WinForms.XmlSeatsDlg dlg = new MyVrm.Outlook.WinForms.XmlSeatsDlg();
                                    dlg.ConfStart = dialog.StartDateTime;
                                    dlg.ConfEnd = dialog.EndDateTime;
                                    dlg.ConfId = string.Empty;
                                    dlg.ShowDialog();
                                }
                                //ZD 100568
								Cancel = true;
							}
						}
					}
				}
			}
		}

        public override bool IsStaticId
        {
            get
            {
                return _isStaticAppointment;
            }
            set
            {
                _isStaticAppointment = value;
            }
        }
    }
}
