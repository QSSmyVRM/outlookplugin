﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
    public class InventoryCharges
    {
        public decimal ServiceCharge { get; set; }
        public int DeliveryType { get; set; }
        public decimal DeliveryCost { get; set; }
    }
}
