﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    internal abstract class TierSchema : ServiceObjectSchema
    {
        internal static readonly PropertyDefinition Id;
        internal static readonly PropertyDefinition Name;

        static TierSchema()
        {
            Id = new ComplexPropertyDefinition<TierId>("ID", () => new TierId());
            Name = new StringPropertyDefinition("Name", PropertyDefinitionFlags.CanSet);

        }

        internal override void RegisterProperties()
        {
            base.RegisterProperties();
            RegisterProperty(Id);
            RegisterProperty(Name);
        }
    }

    internal class TopTierSchema : TierSchema
    {
        public static readonly TierSchema Instance;

        static TopTierSchema()
        {
            Instance = new TopTierSchema();
        }

    }

    internal class MiddleTierSchema : TierSchema
    {
        internal static readonly PropertyDefinition ParentId;

        public static readonly MiddleTierSchema Instance;

        static MiddleTierSchema()
        {
            ParentId = new ComplexPropertyDefinition<TierId>("Tier1ID", PropertyDefinitionFlags.CanSet, () => new TierId());

            Instance = new MiddleTierSchema();
        }

        internal override void RegisterProperties()
        {
            base.RegisterProperties();
            RegisterProperty(ParentId);
        }
    }
}
