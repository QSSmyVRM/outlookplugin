﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	public enum Level
	{
		NotSet = 0,
		Room = 1,
		MCU = 2,
		System = 4
	}

	public enum Decision
	{
		Undecided = 0,
		Approved = 1,
		Denied = 2
	}

	public class ApprovalUnit : ComplexProperty
	{
		private Decision _decision;
		private string _comment;
		private int _id;
		private string _name;

		public Decision Decision
		{
			get { return _decision; }
			set { _decision = value; }
		}
		public string Comment
		{
			get { return _comment; }
			set { _comment = value; }
		}
		public string Name
		{
			get { return _name; }
			set { _name = value; }
		}
		public int ID
		{
			get { return _id; }
			set { _id = value; }
		}

		public override object Clone()
		{
			ApprovalUnit copy = new ApprovalUnit();
			copy._comment = string.Copy(_comment);
			copy._name = string.Copy(_name);
			copy._decision = _decision;
			copy._id = _id;

			return copy;
		}

		internal ApprovalUnit()
		{
			_comment = string.Empty;
			_name = string.Empty;
			_id = 0;
			_decision = Decision.Undecided;
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			if( _id != 0 )
				writer.WriteElementValue(XmlNamespace.NotSpecified, "ID", _id);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "decision", _decision);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "message", _comment);
		}
	}
	
	public class ApprovalRoom : ApprovalUnit
	{
		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			writer.WriteStartElement(XmlNamespace.NotSpecified, "room");
			base.WriteElementsToXml(writer);
			writer.WriteEndElement();
		}
	}

	public class ApprovalMCU : ApprovalUnit
	{
		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			writer.WriteStartElement(XmlNamespace.NotSpecified, "MCU");
			base.WriteElementsToXml(writer);
			writer.WriteEndElement();
		}
	}
	public class ApprovalSystem : ApprovalUnit
	{
		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			writer.WriteStartElement(XmlNamespace.NotSpecified, "system");
			base.WriteElementsToXml(writer);
			writer.WriteEndElement();
		}
	}

	public class ApprovedEntry : ComplexProperty
	{
		private Level _level;
		private List<ApprovalUnit> _approvalUnitList;

		public Level Level
		{
			set { _level = value; }
			get { return _level; }
		}
		public List<ApprovalUnit> ApprovalUnitList
		{
			set { _approvalUnitList = value; }
			get { return _approvalUnitList; }
		}

		public override object Clone()
		{
			ApprovedEntry copy = new ApprovedEntry();
			copy._approvalUnitList = new List<ApprovalUnit>(_approvalUnitList);
			copy._level = _level;

			return copy;
		}

		public ApprovedEntry()
		{
			_level = Level.NotSet;
			_approvalUnitList = new List<ApprovalUnit>();
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			writer.WriteStartElement(XmlNamespace.NotSpecified, "partyInfo");
			writer.WriteStartElement(XmlNamespace.NotSpecified, "response");
			writer.WriteElementValue(XmlNamespace.NotSpecified, "level", _level);
			switch(_level)
			{
				case Level.Room:
					writer.WriteStartElement(XmlNamespace.NotSpecified, "rooms");
					foreach (ApprovalUnit approvalUnit in _approvalUnitList)
					{
						approvalUnit.WriteElementsToXml(writer);
					}
					writer.WriteEndElement();
					break;
				case Level.MCU:
					writer.WriteStartElement(XmlNamespace.NotSpecified, "MCUs");
					foreach (ApprovalUnit approvalUnit in _approvalUnitList)
					{
						approvalUnit.WriteElementsToXml(writer);
					}
					writer.WriteEndElement();
					break;
				case Level.System:
					writer.WriteStartElement(XmlNamespace.NotSpecified, "systems");
					foreach (ApprovalUnit approvalUnit in _approvalUnitList)
					{
						approvalUnit.WriteElementsToXml(writer);
					}
					writer.WriteEndElement();
					break;
			}
              
			writer.WriteEndElement();
			writer.WriteEndElement();
		}


		//    - <Entities>
		//        <Level>1</Level> 
		//        - <Entity>
		//            <Name>Room 294</Name> 
		//            <ID>16</ID> 
		//            <Message /> 
		//        </Entity>
		//        - <Entity>
		//            <Name>Room 268</Name> 
		//            <ID>17</ID> 
		//            <Message /> 
		//        </Entity>
		//    </Entities>
		internal void LoadFromXml(MyVrmServiceXmlReader reader)
		{
			while( !reader.IsEndElement(XmlNamespace.NotSpecified, "Entities"))
			{
				reader.Read();
				if (reader.LocalName == "Level" && !reader.IsEndElement(XmlNamespace.NotSpecified, "Level"))
				{
					_level = reader.ReadElementValue<Level>();
					reader.Read();
				}
				
				while (reader.LocalName == "Entity" && !reader.IsEndElement(XmlNamespace.NotSpecified, "Entity"))
				{
					if (reader.LocalName == "Entity" && reader.IsStartElement(XmlNamespace.NotSpecified, "Entity"))
					{
						ApprovalUnit unit = new ApprovalUnit();
						reader.Read();
						if (reader.LocalName == "Name") //can be empty
							unit.Name = reader.ReadElementValue();

						reader.Read();
						if (reader.LocalName == "ID") //can be empty
						{
							int iRet = 0;
							int.TryParse(reader.ReadElementValue(), out iRet);
							unit.ID = iRet;
						}
						reader.Read();
						if (reader.LocalName == "Message") //can be empty
							unit.Comment = reader.ReadElementValue();

						_approvalUnitList.Add(unit);
						reader.Read();
					}
				}
			}
		}
	}
}
