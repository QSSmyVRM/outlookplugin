﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraScheduler;
using DevExpress.XtraScheduler.Drawing;
using DevExpress.XtraScheduler.Native;
using DevExpress.XtraTreeList.Nodes;
using MyVrm.WebServices.Data;
using OccurrenceInfo = MyVrm.WebServices.Data.OccurrenceInfo;
using RecurrenceType = DevExpress.XtraScheduler.RecurrenceType;

namespace MyVrm.Outlook.WinForms.RecurrentConferences
{
	public partial class ConflictResolveDialog : MyVrm.Outlook.WinForms.Dialog
	{
		public static readonly DateTime SchedulerMaxDateTime = new DateTime(9998, 12, 31);
		public static readonly DateTime SchedulerMinDateTime = new DateTime(1753, 1, 1);

		public enum ResolveDlgAppointmentType 
		{
			OtherAppointment,
			EditedConflictingAppointment,
			TimeZoneConflictingAppointment,
			NonEditedNonConflictingAppointment
		}

		void Init()
		{
			InitializeComponent();
			ApplyEnabled = false;
			ApplyVisible = false;
			layoutControl1.Text = Strings.InstancesWithConflictsLableText;
			layoutControlItem1.Text = Strings.InstancesWithConflictsLableText;
			layoutControlItem1.CustomizationFormText = Strings.InstancesWithConflictsLableText;
			Text = Strings.ConflictResolutionDialogLableText;
			_rightBorder = SchedulerMaxDateTime;
			_leftBorder = SchedulerMinDateTime;
		}
		internal ConflictResolveDialog()
		{
			Init();
		}

		private List<RoomTimetable> _roomTimetable = new List<RoomTimetable>(); //Each room (=resource) timetable
		private List<Occurrence> _conflictOccurences = new List<Occurrence>(); //Conflicting occurences' descriptions
		private ConferenceId _currConferenceId = new ConferenceId("new");
		private string _confName = string.Empty;
		private OrganizationOptions.SystemAvailableTime _workingTime = new OrganizationOptions.SystemAvailableTime();
		private DateTime _leftBorder;
		private DateTime _rightBorder;

		//UI-style for conferences' occurences
		private int _busyStatus = 0;

		//Lables for distinguished occurences' types 
		private AppointmentLabel _normalLabel = new AppointmentLabel(Color.Cyan, "No Conflict");
		private AppointmentLabel _roomConflictLabel = new AppointmentLabel(Color.Red, "Conflict");
		private AppointmentLabel _timeZoneConflictLabel = new AppointmentLabel(Color.Salmon, "Conflict");
		private AppointmentLabel _otherConfLabel = new AppointmentLabel(Color.White, "Other conferences");
		private AppointmentConflictsCalculator _conflictsCalculator;

		//Lables' indexes
		private int _normalLabelIndex = 0;
		private int _roomConflictLabelIndex = 0;
		private int _timeZoneConflictLabelIndex = 0;
		private int _otherLabelIndex = 0;

		//Returns arranged (i.e. non-conflicting) appointment collection
		public OccurrenceInfoExtendCollection ResultAppointments { get { return GetResultAppointments(); } }
		internal OccurrenceInfoExtendCollection GetResultAppointments() 
		{
			OccurrenceInfoExtendCollection ret = new OccurrenceInfoExtendCollection();

			for (int i = 0; i < treeList1.Nodes.Count; i++)
			{
				DateTime origDateTime = new DateTime(((DateTime)treeList1.Nodes[i][treeListColumn1]).Ticks);
				DateTime dateTime = new DateTime(((DateTime)treeList1.Nodes[i][treeListColumn2]).Ticks);

				Appointment appointment = schedulerStorage1.Appointments.Items.Find(
						entry => entry.CustomFields["appID"] == treeList1.Nodes[i].Tag);

				OccurrenceInfoExtend entryInfo = new OccurrenceInfoExtend(origDateTime, dateTime, appointment.End);
				ret.Add(entryInfo);
			}

			if( treeList1.Nodes.Count < _conflictOccurences.Count) //i.e. if some instances were deleted
			{
				foreach (Occurrence confInst in _conflictOccurences)
				{
					DateTime dt = confInst.StartDate;
					string time = string.Format("{0:D2}:{1:D2}:00 {2}", confInst.StartHour, confInst.StartMin,
						confInst.StartSet);
					DateTime hours = DateTime.Parse(time);
					dt = dt.AddHours(hours.Hour);
					dt = dt.AddMinutes(hours.Minute);
					
					bool bFound = false;
					foreach( OccurrenceInfoExtend occur in ret)
					{
						if (occur.OrigStart.Date == dt.Date) //already exists
						{
							bFound = true;
							break;
						}
					}
					if (bFound == false)
					{
						OccurrenceInfoExtend entryInfo = new OccurrenceInfoExtend(dt, dt, dt + confInst.DurationMin);
						entryInfo.IsDeleted = confInst.Conflict > 0 ;
						ret.Add(entryInfo);
					}
				}
			}
			
			return ret;
		}
		public ConflictResolveDialog(List<RoomTimetable> roomTimetable, List<Occurrence> conflictOccurences,
			ConferenceId Id, string confName, OrganizationOptions.SystemAvailableTime workingTime,
			DateTime leftBorder, DateTime rightBorder)
		{
			Init();
			_roomTimetable = roomTimetable;
			_conflictOccurences = conflictOccurences;
			_currConferenceId = Id;
			_confName = confName;
			_workingTime = workingTime;
			_rightBorder = rightBorder;
			_leftBorder = leftBorder;	
			
			schedulerControl1.BeginUpdate();
			schedulerControl1.ToolTipController = new ToolTipController();
			schedulerControl1.ToolTipController.GetActiveObjectInfo += ToolTipController_GetActiveObjectInfo;
			schedulerControl1.EndUpdate();

            schedulerStorage1.Appointments.CustomFieldMappings.Add(new AppointmentCustomFieldMapping("AppointmentCustomField", "AppointmentCustomField"));
            schedulerStorage1.Appointments.CustomFieldMappings.Add(new AppointmentCustomFieldMapping("appID", "appID"));

			//Setup visibile working/close hours
			schedulerControl1.Views.DayView.WorkTime.Start = _workingTime.StartTimeInLocalTimezone;
			schedulerControl1.Views.DayView.WorkTime.End = _workingTime.EndTimeInLocalTimezone;
			schedulerControl1.Views.WorkWeekView.WorkTime.Start = _workingTime.StartTimeInLocalTimezone;
			schedulerControl1.Views.WorkWeekView.WorkTime.End = _workingTime.EndTimeInLocalTimezone;
			schedulerControl1.Views.TimelineView.WorkTime.Start = _workingTime.StartTimeInLocalTimezone;
			schedulerControl1.Views.TimelineView.WorkTime.End = _workingTime.EndTimeInLocalTimezone;

			_busyStatus = schedulerStorage1.Appointments.Statuses.GetStandardStatusId(AppointmentStatusType.Busy);
			_normalLabelIndex = schedulerStorage1.Appointments.Labels.Add(_normalLabel);
			_roomConflictLabelIndex = schedulerStorage1.Appointments.Labels.Add(_roomConflictLabel);
			_timeZoneConflictLabelIndex = schedulerStorage1.Appointments.Labels.Add(_timeZoneConflictLabel);
			_otherLabelIndex = schedulerStorage1.Appointments.Labels.Add(_otherConfLabel);

			foreach (RoomTimetable room in _roomTimetable)
			{
				if (room != null) //extra indeed
				{
					var foundRoom = schedulerStorage1.Resources.Items.Find(entry => (RoomId) entry.Id == room._room.Id &&
					                                                                entry.Caption == room._room.Name);
					if (foundRoom == null) //if not in list - add it
						schedulerStorage1.Resources.Add(new Resource(room._room.Id, room._room.Name));
				}
			}
			_conflictsCalculator = new AppointmentConflictsCalculator(schedulerStorage1.Appointments.Items);
		}

		private void ConflictResolveDialog_Load(object sender, EventArgs e)
		{
			//Set conflicting appointments
			treeList1.BeginUnboundLoad();
			schedulerStorage1.BeginUpdate();
			schedulerControl1.LimitInterval.Start = _leftBorder;
			schedulerControl1.LimitInterval.End = _rightBorder;
			int iAppIDCounter = 0;
			foreach (Occurrence confInst in _conflictOccurences)
			{
				DateTime dt = confInst.StartDate;
				string time = string.Format("{0:D2}:{1:D2}:00 {2}", confInst.StartHour, confInst.StartMin, 
					confInst.StartSet);
				DateTime hours = DateTime.Parse(time);
				dt = dt.AddHours(hours.Hour);
				dt = dt.AddMinutes(hours.Minute);

				Appointment appointment = schedulerStorage1.CreateAppointment(AppointmentType.Normal);
				appointment.Start = dt;
				appointment.Duration = confInst.DurationMin;
				appointment.Subject = _confName;

				appointment.StatusId = _busyStatus;

				//Set "id" to bind the treelist entry and the corresponding appointment in scheduler
				appointment.CustomFields["appID"] = iAppIDCounter++; 

				if (confInst.Conflict > 0)
				{
					//Mark currenly edited conflicting conference appointments
					switch(confInst.Conflict)
					{
							//Room conflict
						case 1: appointment.CustomFields["AppointmentCustomField"] = ResolveDlgAppointmentType.EditedConflictingAppointment;
							//add to scheduler
							appointment.LabelId = _roomConflictLabelIndex;
							appointment.Description = Strings.RoomConflict;
							break;
							//time Zone conflict
						case 2: appointment.CustomFields["AppointmentCustomField"] = ResolveDlgAppointmentType.TimeZoneConflictingAppointment;
							//add to scheduler
							appointment.LabelId = _timeZoneConflictLabelIndex;
							appointment.Description = Strings.TimeZoneConflict;
							break;
					}

					//add to list
					TreeListNode node = treeList1.AppendNode(new object[] { dt, dt }, -1);
					node.Tag = appointment.CustomFields["appID"];
					
				}
				else
				{
					//Mark currenly edited conflicting conference appointments
					appointment.CustomFields["AppointmentCustomField"] = ResolveDlgAppointmentType.NonEditedNonConflictingAppointment;
					appointment.LabelId = _normalLabelIndex;
				}
				schedulerControl1.Storage.Appointments.Add(appointment);
			}
			treeList1.EndUnboundLoad();
			schedulerStorage1.EndUpdate();

			//Set timetable appointments
			schedulerStorage1.BeginUpdate();
			foreach (RoomTimetable room in _roomTimetable)
			{
				int iResNdx = 0;
				Resource res = schedulerStorage1.Resources.GetResourceById(room._room.Id);
				if (res == Resource.Empty)
					iResNdx = schedulerStorage1.Resources.Add(new Resource(room._room.Id, room._room.Name));
				else
					iResNdx = schedulerStorage1.Resources.Items.IndexOf(res);

				foreach (ConferenceOccurrence confInst in room._occurences)
				{
					//Skip itself
					string confID = confInst.ConferenceId.Id;
					int iCommaNdx = confInst.ConferenceId.Id.IndexOf(",");

					if (iCommaNdx != -1)
						confID = confInst.ConferenceId.Id.Substring(0, iCommaNdx);
					if (confID == _currConferenceId.Id) // process complex recurrent ID, e.q. 23,1
						continue;

					//Check if GetRoomMonthlyCalendar() returned duplications 
					//(can return instances of 30th of prev month in case as well as for requested month)
					if (schedulerControl1.Storage.Appointments.Items.Find(ap => ap.Start == confInst.Date &&
							ap.Duration == confInst.Duration && ap.Subject == confInst.ConferenceName &&
							ap.ResourceId == res.Id) != null)
						continue;

					Appointment appointment = schedulerStorage1.CreateAppointment(AppointmentType.Normal);
					appointment.Start = confInst.Date;
					appointment.Duration = confInst.Duration;
					appointment.Subject = confInst.ConferenceName;
					appointment.LabelId = _otherLabelIndex;
					appointment.ResourceId = res.Id;
					appointment.StatusId = _busyStatus;
					appointment.CustomFields["AppointmentCustomField"] = ResolveDlgAppointmentType.OtherAppointment; //Other appointment
					appointment.CustomFields["appID"] = iAppIDCounter++;
					schedulerControl1.Storage.Appointments.Add(appointment);
				}
				
			}
			schedulerStorage1.EndUpdate();

			treeList1.FocusedNode = treeList1.Nodes.FirstNode;
			//Open scheduler at the first conflict
			DateTime dateTime = new DateTime(((DateTime)treeList1.Nodes.FirstNode[treeListColumn1]).Ticks);
			Wrap_GoToDate(dateTime);
		}

		//On appointment date/time (treelist entry, 2nd colomn) click
		private void repositoryItemTimeEdit2_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
		{
			DateTime dateTime = new DateTime( ((DateTime)treeList1.FocusedNode[treeListColumn2]).Ticks);
			switch( e.Button.Index )
			{
				case 0: //Setup new date/time
					break;
				case 1: //View occurence
					Wrap_GoToDate(dateTime);
					break;
				case 2: //Delete occurence
					Appointment appointment = schedulerStorage1.Appointments.Items.Find(
						entry => entry.CustomFields["appID"] == treeList1.FocusedNode.Tag); 
					treeList1.DeleteNode(treeList1.FocusedNode);
					if (appointment != null)
						schedulerStorage1.Appointments.Remove(appointment);
					break;
			}
		}

		//On appointment date/time change
		void repositoryItemTimeEdit2_EditValueChanged(object sender, System.EventArgs e)
		{
			Appointment appointment = schedulerStorage1.Appointments.Items.Find(
						entry => entry.CustomFields["appID"] == treeList1.FocusedNode.Tag);
			DevExpress.XtraEditors.TimeEdit dt = ((DevExpress.XtraEditors.TimeEdit)(sender));
			if (appointment != null)
			{
				appointment.BeginUpdate();
				appointment.Start = dt.Time;
				appointment.EndUpdate();
				schedulerControl1.Update();

				CalculateConflicts(appointment);
				Wrap_GoToDate(appointment.Start);
			}
		}

		private void DeleteAppointment_Click(object sender, EventArgs e)
		{
			bool bIsConfilictApp = false;
			DateTime dt = new DateTime();
			//Actually selection contains only 1 appointment
			foreach (Appointment app in schedulerControl1.SelectedAppointments)
			{
				dt = app.Start.Date;
				if ((int)(app.CustomFields["AppointmentCustomField"]) == (int)ResolveDlgAppointmentType.EditedConflictingAppointment || 
					(int)(app.CustomFields["AppointmentCustomField"]) == (int)ResolveDlgAppointmentType.TimeZoneConflictingAppointment) //delete only conflicting conf occurences
				{
					TreeListNode node = treeList1.FindNodeByFieldValue(treeListColumn2.FieldName, app.Start);
					if (node != null)
					{
						treeList1.DeleteNode(node);
						bIsConfilictApp = true;
						break;
					}
				}
			}

			if (bIsConfilictApp)
			{
				schedulerControl1.DeleteSelectedAppointments();

				for (int i = 0; i < schedulerStorage1.Appointments.Count; i++)
				{
					//Skip self and "other" appointments
					if ((int)(schedulerStorage1.Appointments[i].CustomFields["AppointmentCustomField"]) == (int)ResolveDlgAppointmentType.OtherAppointment)
						continue;
					if (dt == schedulerStorage1.Appointments[i].Start.Date
							&& schedulerStorage1.Appointments[i].LabelId != _normalLabelIndex)
					{
						CalculateConflicts(schedulerStorage1.Appointments[i]);
						//schedulerStorage1.Appointments[i].LabelId = _roomConflictLabelIndex;
						//break;
					}
				}
			}
		}

		void schedulerControl1_AllowAppointmentDelete(object sender, AppointmentOperationEventArgs e)
		{
			//Actually selection contains only 1 appointment
			foreach (Appointment app in schedulerControl1.SelectedAppointments)
			{
				if ((int)(app.CustomFields["AppointmentCustomField"]) == (int)ResolveDlgAppointmentType.EditedConflictingAppointment ||
					(int)(app.CustomFields["AppointmentCustomField"]) == (int)ResolveDlgAppointmentType.TimeZoneConflictingAppointment) //delete only conflicting conf occurences
				{
					TreeListNode node = treeList1.FindNodeByFieldValue(treeListColumn1.FieldName, app.Start);
					if (node != null)
					{
						treeList1.DeleteNode(node);
						break;
					}
				}
				else
				{
					e.Allow = false;
					break;
				}
			}
		}

		void schedulerControl1_AppointmentResized(object sender, AppointmentResizeEventArgs e)
		{
			Appointment apt = e.EditedAppointment;
			if (apt != null)
			{
				//Do not allow to change non-conflicting conference occurences
				if ((int)(apt.CustomFields["AppointmentCustomField"]) != (int)ResolveDlgAppointmentType.EditedConflictingAppointment &&
					(int)(apt.CustomFields["AppointmentCustomField"]) != (int)ResolveDlgAppointmentType.TimeZoneConflictingAppointment)
				{
					e.Allow = false;
					e.Handled = true;
				}
				else
				{
					CalculateConflicts(apt);
					e.SourceAppointment.LabelId = apt.LabelId;
				}
			}
		}

		internal static MyVrm.WebServices.Data.DaysOfWeek SystemDayOfWeek_To_MyVrmDayOfWeek(System.DayOfWeek dayOfWeek)
		{
			switch (dayOfWeek)
			{
				case System.DayOfWeek.Sunday:
					return MyVrm.WebServices.Data.DaysOfWeek.Sunday;
				case System.DayOfWeek.Monday:
					return MyVrm.WebServices.Data.DaysOfWeek.Monday;
				case System.DayOfWeek.Tuesday:
					return MyVrm.WebServices.Data.DaysOfWeek.Tuesday;
				case System.DayOfWeek.Wednesday:
					return MyVrm.WebServices.Data.DaysOfWeek.Wednesday;
				case System.DayOfWeek.Thursday:
					return MyVrm.WebServices.Data.DaysOfWeek.Thursday;
				case System.DayOfWeek.Friday:
					return MyVrm.WebServices.Data.DaysOfWeek.Friday;
				case System.DayOfWeek.Saturday:
					return MyVrm.WebServices.Data.DaysOfWeek.Saturday;
				default:
					return MyVrm.WebServices.Data.DaysOfWeek.None;
			}
		}
		//Calclate conflicts - setup an appropriate lable to the appointment
		private void CalculateConflicts(Appointment apt)
		{
			int initLableId = apt.LabelId;
			apt.LabelId = _normalLabelIndex;

			AppointmentBaseCollection conflicts = _conflictsCalculator.CalculateConflicts(apt, new TimeInterval(apt.Start, apt.End));

			int conflictsCount = conflicts.Count;
			MyVrmAddin.TraceSource.TraceInformation("conflicts.count = {0}", conflictsCount);

			if (conflictsCount > 0)
			{
				MyVrmAddin.TraceSource.TraceInformation("\n Moving: Subject = {0}, Start = {1}, Duration = {2}, ndx = {3}",
									apt.Subject, apt.Start, apt.Duration, apt.CustomFields["appID"]);
				
				foreach (Appointment anApp in conflicts)
				{
					MyVrmAddin.TraceSource.TraceInformation("\n Subject = {0}, Start = {1}, Duration = {2}, ndx = {3}",
									anApp.Subject, anApp.Start, anApp.Duration, anApp.CustomFields["appID"]);
					if (apt.CustomFields["appID"] == anApp.CustomFields["appID"]) //Skip self
						conflictsCount--;
				}
			}
			apt.LabelId = conflictsCount > 0 ? _roomConflictLabelIndex : _normalLabelIndex;

			//If no conflicts then check working hours
			if(conflictsCount == 0 && apt.LabelId == _normalLabelIndex )
				//&& (int)apt.CustomFields["AppointmentCustomField"] == (int)ResolveDlgAppointmentType.TimeZoneConflictingAppointment)
			{
				bool isInWorkingHours = _workingTime.StartTimeInLocalTimezone <= apt.Start.TimeOfDay && apt.Start.TimeOfDay < _workingTime.EndTimeInLocalTimezone &&
										_workingTime.StartTimeInLocalTimezone < apt.End.TimeOfDay && apt.End.TimeOfDay <= _workingTime.EndTimeInLocalTimezone;
				if (!isInWorkingHours || 
					( (int)(_workingTime.DaysClosed ) & (int)(SystemDayOfWeek_To_MyVrmDayOfWeek(apt.Start.DayOfWeek)) ) > 0 || 
					( (int)(_workingTime.DaysClosed ) & (int)(SystemDayOfWeek_To_MyVrmDayOfWeek(apt.End.DayOfWeek)) ) > 0)
				{
					apt.LabelId = _timeZoneConflictLabelIndex;
				}
				//for( int i = 0 ; i < treeList1.Nodes.Count ; i++ )
				//{
				//    if (treeList1.Nodes[i].Tag != apt.CustomFields["appID"])
				//    {
				//        DateTime currDateTime = new DateTime(((DateTime)treeList1.Nodes[i][treeListColumn1]).Ticks);
				//        if (apt.Start.Date.Day == currDateTime.Day && apt.Start.Date.Month == currDateTime.Month &&
				//                    apt.Start.Date.Year == currDateTime.Year )
				//            apt.LabelId = _timeZoneConflictLabelIndex;
				//    }
				//}
			}
			if (conflictsCount == 0 && apt.LabelId == _normalLabelIndex) //Check if conf has already an instance on that date
			{
				for (int i = 0; i < schedulerStorage1.Appointments.Count; i++)
				{
					//Skip self and "other" appointments
					if (apt.CustomFields["appID"] == schedulerStorage1.Appointments[i].CustomFields["appID"] ||
							(int)(schedulerStorage1.Appointments[i].CustomFields["AppointmentCustomField"]) == (int)ResolveDlgAppointmentType.OtherAppointment)
						continue;
					if (apt.Start.Date == schedulerStorage1.Appointments[i].Start.Date
							&& schedulerStorage1.Appointments[i].LabelId != _roomConflictLabelIndex)
					{
						apt.LabelId = _roomConflictLabelIndex;
						break;
					}
				}
			}
		}

		void schedulerControl1_AppointmentDrag(object sender, AppointmentDragEventArgs e)
		{
			Appointment apt = e != null ? e.EditedAppointment : null;
			DateTime dt = new DateTime();
			if (apt != null)
			{
				//Allow to drag only  conflicting conference appointments 
				if ((int)(apt.CustomFields["AppointmentCustomField"]) == (int)ResolveDlgAppointmentType.EditedConflictingAppointment ||
					(int)(apt.CustomFields["AppointmentCustomField"]) == (int)ResolveDlgAppointmentType.TimeZoneConflictingAppointment )
				{
					//Check conflicts
					CalculateConflicts(apt);

					//Correct table time
					for( int i = 0 ; i < treeList1.Nodes.Count ; i++ )
					{
						if( treeList1.Nodes[i].Tag == apt.CustomFields["appID"] )
						{
							treeList1.FocusedNode = treeList1.Nodes[i];
							dt = (DateTime)treeList1.Nodes[i][treeListColumn2];
							treeList1.Nodes[i][treeListColumn2] = apt.Start;
							e.SourceAppointment.Start = apt.Start;
							break;
						}
					}

					//If conflicting app moved to another day the rest one may become non-conflicting
					if (dt.Date != apt.Start.Date)
					{
						for (int i = 0; i < schedulerStorage1.Appointments.Count; i++)
						{
							//Skip self and "other" appointments
							if (apt.CustomFields["appID"] == schedulerStorage1.Appointments[i].CustomFields["appID"] ||
									(int) (schedulerStorage1.Appointments[i].CustomFields["AppointmentCustomField"]) ==
									(int) ResolveDlgAppointmentType.OtherAppointment)
								continue;
							if (dt.Date == schedulerStorage1.Appointments[i].Start.Date && 
								 schedulerStorage1.Appointments[i].LabelId == _roomConflictLabelIndex)
							{
								CalculateConflicts(schedulerStorage1.Appointments[i]);
							}
						}
					}
				}
				else
				{
					e.Allow = false;
					e.Handled = true;
				}
			}
		}
		
		//not used?
		void schedulerControl1_EditRecurrentAppointmentFormShowing(object sender, EditRecurrentAppointmentFormEventArgs e)
		{
			e.Handled = true;
		}

		//Do not open edit appointment dialog
		void schedulerControl1_EditAppointmentFormShowing(object sender, AppointmentFormEventArgs e)
		{
			e.Handled = true;
		}

		//Handle and arrange allowed menus' entries
		void schedulerControl1_PreparePopupMenu(object sender, DevExpress.XtraScheduler.PreparePopupMenuEventArgs e)
		{
			if (e.Menu.Id == SchedulerMenuItemId.DefaultMenu)
			{
				for (SchedulerMenuItemId item = 0; item < SchedulerMenuItemId.SplitAppointment; item++)
				{
					if (item.Equals(SchedulerMenuItemId.GotoToday) == false && item.Equals(SchedulerMenuItemId.GotoDate) == false &&
						item.Equals(SchedulerMenuItemId.SwitchTimeScale) == false &&
						item.Equals(SchedulerMenuItemId.TimeScaleEnable) == false &&
						item.Equals(SchedulerMenuItemId.SwitchToDayView) == false &&
						item.Equals(SchedulerMenuItemId.SwitchToWeekView) == false &&
						item.Equals(SchedulerMenuItemId.SwitchToWorkWeekView) == false &&
						item.Equals(SchedulerMenuItemId.SwitchToMonthView) == false &&
						item.Equals(SchedulerMenuItemId.SwitchToTimelineView) == false &&
						item.Equals(SchedulerMenuItemId.SwitchViewMenu) == false)
					{
						e.Menu.RemoveMenuItem(item);
					}
				}
			}
			else
			{
				if (e.Menu.Id == SchedulerMenuItemId.AppointmentMenu)
				{
					SchedulerMenuItem item = e.Menu.GetMenuItemById(SchedulerMenuItemId.DeleteAppointment);
					e.Menu.Items.Clear();
					item.Click += new System.EventHandler(DeleteAppointment_Click);
					e.Menu.Items.Add(item);
				}
			}
		}

		private void OnFormClosing(object sender, FormClosingEventArgs e)
		{
			if (DialogResult == DialogResult.OK)
			{
				//Check correctness - if there are some conflicts still present
				foreach( Appointment app in schedulerStorage1.Appointments.Items)
				{
					if (app.LabelId == _roomConflictLabelIndex || app.LabelId == _timeZoneConflictLabelIndex) //Has unresolved conflict
					{
						ShowError(Strings.StillHasConflictsMsg);
						//Set focus on the first problematic occurence - in treelist
						for( int i = 0; i < treeList1.Nodes.Count; i++ )
						{
							if (app.CustomFields["appID"] == treeList1.Nodes[i].Tag)
							{
								treeList1.FocusedNode = treeList1.Nodes[i];
								break;
							} 
						}
						//Set focus on the first problematic occurence - in scheduler
						Wrap_GoToDate(app.Start);
						
						//Do not close dialog
						e.Cancel = true;
						break;
					}
				}
			}
		}

		//Display current appointment in the middle of timeline 
		private void Wrap_GoToDate(DateTime currTimeDate)
		{
			schedulerControl1.GoToDate(currTimeDate);
			if (schedulerControl1.ActiveViewType == SchedulerViewType.Timeline)
			{
				TimeIntervalCollection ti = schedulerControl1.ActiveView.GetVisibleIntervals();

				//NOTE: 0-24 h cycle, 1-31/30(28/29) day cycle, 1-12 month cycle
				DateTime displayPosition = currTimeDate;
				displayPosition = displayPosition.AddHours(-ti.Duration.Hours/2);
				displayPosition = displayPosition.AddMinutes(-ti.Duration.Minutes / 2);
				displayPosition = displayPosition.AddSeconds(-ti.Duration.Seconds / 2);

				schedulerControl1.GoToDate(displayPosition);
			}
		}

		//Custom tooltips for appoinments
		void ToolTipController_GetActiveObjectInfo(object sender, ToolTipControllerGetActiveObjectInfoEventArgs e)
		{
			if (e != null && e.Info != null && e.Info.Object != null &&
				e.Info.Object.GetType() == typeof(TimeLineAppointmentViewInfo))
			{
				TimeLineAppointmentViewInfo obj = (TimeLineAppointmentViewInfo)(e.Info.Object);
				if (obj != null)
				{
					SuperToolTip sTooltip = new SuperToolTip();
					ToolTipTitleItem titleItem = new ToolTipTitleItem();

					titleItem.Text = obj.Appointment.Subject;
					ToolTipItem item = new ToolTipItem();
					sTooltip.Items.Add(titleItem);
					sTooltip.Items.Add(item);

					string tip = "<br>" + Strings.ConfName + " " + obj.Appointment.Subject + "</br>" ;

					tip += "<br>" + Strings.Start + " " + obj.Appointment.Start + "</br>";
					tip += Strings.End + " " + obj.Appointment.End;
					if( obj.Appointment.LabelId == _normalLabelIndex)
					{
						tip += "<br>" + Strings.ConflictType + " " + Strings.NoConflict + "</br>";
					}
					else
					{
						if(obj.Appointment.LabelId == _roomConflictLabelIndex)
						{
							tip += "<br>" + Strings.ConflictType + " " + Strings.RoomConflict + "</br>";
						}
						else
						{
							if (obj.Appointment.LabelId == _timeZoneConflictLabelIndex)
							{
								tip += "<br>" + Strings.ConflictType + " " + Strings.TimeZoneConflict + "</br>";
							}
							//else
							//{
							//    if (obj.Appointment.LabelId == _otherLabelIndex)
							//    {

							//    }
							//}
						}
					}
					
					ToolTipControlInfo info = new ToolTipControlInfo(obj.Appointment, tip,
												  obj.Appointment.Subject, true, ToolTipIconType.None, DefaultBoolean.True);
					//item.Text);
					e.Info = info;
				}
			}
		}
	}
}
