﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace MyVrm.WebServices.Data
{
    public sealed class FindUsersResults : IEnumerable<ManagedUser>
    {
        private readonly List<ManagedUser> _users = new List<ManagedUser>();

        internal FindUsersResults()
        {
        }

        #region Implementation of IEnumerable

        public IEnumerator<ManagedUser> GetEnumerator()
        {
            return InternalUsers.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        public ReadOnlyCollection<ManagedUser> Users
        {
            get
            {
                return new ReadOnlyCollection<ManagedUser>(InternalUsers);
            }
        }

        internal List<ManagedUser> InternalUsers
        {
            get { return _users; }
        }
    }
}
