/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Columns;
using MyVrm.Common;
using MyVrm.Common.Collections;
using MyVrm.Common.ComponentModel;
using MyVrm.Common.EventBroker;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook.WinForms.Conference
{
    public partial class RoomAudioVideoSettingsListControl : BaseControl
    {
        private readonly RepositoryItemEnumComboBox _callerRepositoryItem;
        private readonly List<RepositoryItemLookUpEdit> _endpointProfileEditors = new List<RepositoryItemLookUpEdit>();
    	private ReadOnlyCollection<BridgeName> _bridgeNames;

		public ReadOnlyCollection<BridgeName> Bridges 
		{ 
			get { return _bridgeNames; }
		}

    	private bool _isDirty;

		EventBroker _broker;
		public void SetEventBroker(EventBroker broker)
		{
			_broker = broker;
		}

    	public RoomAudioVideoSettingsListControl()
        {
            InitializeComponent();
			roomNameColumn.Caption = Strings.RoomNameLableText;
			endpointColumn.Caption = Strings.EndpointLableText;
        	endpointProfileColumn.Caption = Strings.EndpointProfileColummnText;
			callerColumn.Caption = Strings.CallerCalleeColummnText; 
        	endpointRepositoryItem.NullText = Strings.NoEndpointsForRoomText; 

            _callerRepositoryItem = new RepositoryItemEnumComboBox
                                        {
                                            TextEditStyle = TextEditStyles.DisableTextEditor
                                        };

			_callerRepositoryItem.Items.AddRange(new EnumListSource(typeof(ConferenceEndpointCallMode)).GetList());
			LocalizedEnum enumNone = new LocalizedEnum("None", ConferenceEndpointCallMode.None);
			_callerRepositoryItem.Items.Remove(enumNone);
			_callerRepositoryItem.EditValueChanged += new EventHandler(_callerRepositoryItem_EditValueChanged);
			endpointsTreeList.RepositoryItems.AddRange(new RepositoryItem[] { _callerRepositoryItem });

            callerColumn.ColumnEdit = _callerRepositoryItem;
            callerColumn.Visible = false;

			useDefaultColumnVisibility = true;
        }

		void _callerRepositoryItem_EditValueChanged(object sender, EventArgs e)
		{
			if (_broker != null)
				_broker.OnEvent("event_nonOutlookPropChanged", null);
		}

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false),
         EditorBrowsable(EditorBrowsableState.Never)]
        public object DataSource
        {
            get
            {
                return bindingSource.DataSource;
            }
            set
            {
                bindingSource.DataSource = value;
            }
        }

        private RepositoryItemLookUpEdit CreateEndpointProfileRepositoryItem()
        {
            var lookUpEdit = new RepositoryItemLookUpEdit
                                 {
                                     DisplayMember = "Name",
                                     ValueMember = "Id",
                                     ShowHeader = false,
                                     ShowLines = false,
                                     PopupSizeable = false,
                                     TextEditStyle = TextEditStyles.DisableTextEditor,
                                     NullText = ""
                                 };
            lookUpEdit.Columns.Add(new LookUpColumnInfo("Name"));
            endpointsTreeList.RepositoryItems.Add(lookUpEdit);
            return lookUpEdit;
        }

        private void ResetEndpointList()
        {
            endpointsTreeList.BeginUpdate();
            try
            {
                _endpointProfileEditors.ForEach(item => endpointsTreeList.RepositoryItems.Remove(item));
                _endpointProfileEditors.Clear();
                var roomEndpoints = ((IList<RoomEndpoint>)DataSource);
				if (roomEndpoints != null)
				{
					foreach (var roomEndpoint in roomEndpoints)
					{
						RepositoryItemLookUpEdit lookUpEdit = CreateEndpointProfileRepositoryItem();
						if (roomEndpoint.Endpoint != null)
						{
							List<EndpointProfile> profilesCommonList = roomEndpoint.Endpoint.Profiles.ToList();

							lookUpEdit.DataSource = profilesCommonList;
						}
						
						_endpointProfileEditors.Add(lookUpEdit);
					}
				}
            }
            finally
            {
                endpointsTreeList.EndUpdate();
            }
        }

        private void bindingSource_DataSourceChanged(object sender, EventArgs e)
        {
            ResetEndpointList();
        }

        private void bindingSource_ListChanged(object sender, ListChangedEventArgs e)
        {
            var roomEndpoints = ((IList<RoomEndpoint>)DataSource);
            switch (e.ListChangedType)
            {
                case ListChangedType.Reset:
                {
                    ResetEndpointList();
                    break;
                }
                case ListChangedType.ItemAdded:
                {
                    RoomEndpoint roomEndpoint = roomEndpoints[e.NewIndex];
                    RepositoryItemLookUpEdit lookUpEdit = CreateEndpointProfileRepositoryItem();
                    if (roomEndpoint.Endpoint != null)
                    {
						List<EndpointProfile> profilesCommonList = roomEndpoint.Endpoint.Profiles.ToList();
						
                    	lookUpEdit.DataSource = profilesCommonList;
                    }
                    _endpointProfileEditors.Insert(e.NewIndex, lookUpEdit);
                    break;
                }
                case ListChangedType.ItemDeleted:
                {
                    _endpointProfileEditors.RemoveAt(e.NewIndex);
                    break;
                }
                case ListChangedType.ItemMoved:
                {
                    ResetEndpointList();
                    break;
                }
                case ListChangedType.ItemChanged:
//				MyVrmAddin.TraceSource.TraceInformation("bindingSource_ListChanged() : ListChangedType.ItemChanged");
                    break;
            }
        }

        private void endpointsTreeList_CustomNodeCellEdit(object sender, GetCustomNodeCellEditEventArgs e)
        {
            var roomEndpoints = ((IList<RoomEndpoint>)DataSource);
            var roomEndpoint = roomEndpoints[e.Node.Id];
            if (e.Column == endpointProfileColumn && roomEndpoint.Endpoint != null)
            {
                e.RepositoryItem = _endpointProfileEditors[e.Node.Id];
            }
            else if(e.Column == bridgeNameColumn )
            {
				e.RepositoryItem = repositoryBridgeComboBox;

				//Select bridge name set for room 
				if (_bridgeNames != null && e.Node[bridgeNameColumn] == null)
				{
					string brigeNameToSelect = string.Empty;
					foreach (BridgeName brigeName in _bridgeNames)
					{
						if (brigeName.Id == roomEndpoint.BridgeId)
						{
							brigeNameToSelect = brigeName.Name;
							break;
						}
					}

					//If not set - get the first non-default
					if (brigeNameToSelect == string.Empty)
					{
						int i = 0;
						while (_bridgeNames[i].Name == Strings.UseDefault && i < _bridgeNames.Count)
							i++;
						if (i < _bridgeNames.Count)
							brigeNameToSelect = _bridgeNames[i].Name;
					}

					endpointsTreeList.Nodes[e.Node.Id].SetValue(bridgeNameColumn, brigeNameToSelect);
				}
            }
        }

        private void endpointsTreeList_ShowingEditor(object sender, CancelEventArgs e)
        {
            var treeList = (TreeList) sender;
            if (treeList.FocusedNode == null)
            {
                return;
            }
            var roomEndpoints = ((IList<RoomEndpoint>)DataSource);
            var roomEndpoint = roomEndpoints[treeList.FocusedNode.Id];
            if (roomEndpoint.Endpoint == null)
            {
                e.Cancel = true;
                return;
            }

            e.Cancel = false;
        }

    	public bool BridgeNameColumnVisibility
    	{
    		set { bridgeNameColumn.Visible = value; }
			get { return bridgeNameColumn.Visible; }
    	}

		public bool CallerColumnVisibility
    	{
    		set { callerColumn.Visible = value; }
			get { return callerColumn.Visible; }
    	}

		public bool useDefaultColumnVisibility
		{
			set { useDefaultColumn.Visible = value; }
			get { return useDefaultColumn.Visible; }
		}

		void ColumnEdit_EditValueChanged(object sender, System.EventArgs e)
		{
			if (_broker != null)
				_broker.OnEvent("event_nonOutlookPropChanged", null);
		}

		public TreeListColumn getEndpointProfileColumn
		{
			get { return endpointProfileColumn; }
		}

    	public RepositoryItemComboBox _repositoryBridgeComboBox
    	{
			get { return repositoryBridgeComboBox; }
    		set { repositoryBridgeComboBox = value; }
    	}

		private void RoomAudioVideoSettingsListControl_Load(object sender, EventArgs e)
		{
            if (!DesignMode)
			    GetMCUs();
		}

		//Get MCUs
		private void GetMCUs()
		{
			Cursor cursor = Cursor.Current;
			Cursor.Current = Cursors.WaitCursor;

			try
			{
				if (_bridgeNames == null)
				{
					_bridgeNames = MyVrmService.Service != null ? MyVrmService.Service.GetBridges() : null;

					if (repositoryBridgeComboBox != null)
					{
						if (repositoryBridgeComboBox.Items != null)
						{
							repositoryBridgeComboBox.Items.Clear();
							if (_bridgeNames != null)
							{
								foreach (var bridgeName in _bridgeNames)
								{
									repositoryBridgeComboBox.Items.Add(bridgeName.Name);
								}
							}
						}
					}
				}
			}
			finally
			{
				Cursor.Current = cursor;
			}
		}

    	private void endpointsTreeList_CellValueChanging(object sender, CellValueChangedEventArgs e)
    	{
    		bool doRet = false;
			EndpointProfileId defaultEndpointProfileId = EndpointProfileId.Default; //("-1");
			
			EndpointProfileId currEndpointProfile = (EndpointProfileId)((TreeList)(sender)).FocusedNode.GetValue(endpointProfileColumn);
    		
			//Synchronize comboboxes if "default" changed to non-"default" 
			if (doRet == false && e.Column == bridgeNameColumn )
			{
				doRet = true;
				var roomEndpoints = ((IList<RoomEndpoint>)DataSource);
				RoomEndpoint roomEndpoint = roomEndpoints[((TreeList)(sender)).FocusedNode.Id];
				endpointsTreeList.FocusedNode.SetValue(useDefaultColumn, false);

				/**/
				if (roomEndpoint != null)
				{
					BridgeName bridgeToSelect = _bridgeNames.FirstOrDefault(br => br.Name == (string) e.Value);
					if (bridgeToSelect != null)
					{
						roomEndpoint.BridgeName = bridgeToSelect.Name;
						if (roomEndpoint.ConferenceEndpoint != null)
							roomEndpoint.ConferenceEndpoint.BridgeId = bridgeToSelect.Id;
					}
				}
				/**/
			}
			if (doRet == false && e.Column == endpointProfileColumn && (EndpointProfileId)e.Value != defaultEndpointProfileId )
			{
				doRet = true;
				IList<RoomEndpoint> roomEndpoints = ((IList<RoomEndpoint>)DataSource);
				RoomEndpoint currEndP = roomEndpoints[((TreeList)(sender)).FocusedNode.Id];
				endpointsTreeList.FocusedNode.SetValue(useDefaultColumn, false);
				EndpointProfile currProfile = currEndP.Endpoint.Profiles.FirstOrDefault(endp => endp.Id == (EndpointProfileId)e.Value);
				if(currProfile != null)
				{
					BridgeName bridgeToSelect = _bridgeNames.FirstOrDefault(bridge => bridge.Id == currProfile.BridgeId);
					if (bridgeToSelect != null)
					{
						((TreeList)(sender)).FocusedNode.SetValue(bridgeNameColumn, bridgeToSelect.Name);
						currEndP.BridgeName = bridgeToSelect.Name;
						if (currEndP.ConferenceEndpoint != null)
							currEndP.ConferenceEndpoint.BridgeId = bridgeToSelect.Id;
					}
					currEndP.ProfileId = currProfile.Id;
				}
				/*
				RoomEndpoint roomEndpoint = roomEndpoints[((TreeList)(sender)).FocusedNode.Id];
				if (roomEndpoint != null && _bridgeNames != null)
				{
					string brigeNameToSelect = string.Empty;
					foreach( BridgeName brigeName in _bridgeNames )
					{
						if( brigeName.Id == roomEndpoint.BridgeId)
						{
							brigeNameToSelect = brigeName.Name;
							break;
						}
					}
					if (brigeNameToSelect == string.Empty)
					{
						int i = 0;
						while (_bridgeNames[i].Name == Strings.UseDefault && i < _bridgeNames.Count)
							i++;
						if( i < _bridgeNames.Count)
							brigeNameToSelect = _bridgeNames[i].Name;
					}

					((TreeList)(sender)).FocusedNode.SetValue(bridgeNameColumn, brigeNameToSelect);
					endpointsTreeList.FocusedNode.SetValue(useDefaultColumn, false);
				}
				 */
			}
			
			if (doRet == false && e.Column == callerColumn )
    		{
				doRet = true;
				ConferenceEndpointCallMode currVal = (ConferenceEndpointCallMode)endpointsTreeList.FocusedNode.GetValue(callerColumn);
				if (currVal != (ConferenceEndpointCallMode)e.Value)
				{
					int otherNdx = 1 - endpointsTreeList.FocusedNode.Id;
					endpointsTreeList.Nodes[otherNdx].SetValue(callerColumn, currVal);
					endpointsTreeList.FocusedNode.SetValue(callerColumn, e.Value);
				}
    		}

			if (doRet == false && e.Column == useDefaultColumn )
			{
				doRet = true;
				IList<RoomEndpoint> roomEndpoints = ((IList<RoomEndpoint>)DataSource);
				RoomEndpoint currRoomEndpoint = roomEndpoints[((TreeList)(sender)).FocusedNode.Id];
				bool bSet = e.Value is bool ? (bool) e.Value : false;
				currRoomEndpoint.UseDefault = bSet;
				if(bSet)
				{
					EndpointProfile currProfile = currRoomEndpoint.Endpoint.Profiles.FirstOrDefault(endp => endp.Id == currRoomEndpoint.Endpoint.Profiles.DefaultProfile.Id);
					((TreeList)(sender)).FocusedNode.SetValue(endpointProfileColumn, currRoomEndpoint.Endpoint.Profiles.DefaultProfile.Id);
					BridgeName bridgeToSelect = _bridgeNames.FirstOrDefault(bridge => bridge.Id == currProfile.BridgeId);
					if (bridgeToSelect != null)
					{
						((TreeList)(sender)).FocusedNode.SetValue(bridgeNameColumn, bridgeToSelect.Name);
						currRoomEndpoint.BridgeName = bridgeToSelect.Name;
						if (currRoomEndpoint.ConferenceEndpoint != null)
							currRoomEndpoint.ConferenceEndpoint.BridgeId = bridgeToSelect.Id;
					}
				}
			}

    		_isDirty = ((DataList<RoomEndpoint>) DataSource).RaiseListChangedEvents;
			if(_isDirty && _broker != null)
				_broker.OnEvent("event_nonOutlookPropChanged", null);
    	}

		public void CloseEditor()
		{
			endpointsTreeList.PostEditor();
		}
    }
}
