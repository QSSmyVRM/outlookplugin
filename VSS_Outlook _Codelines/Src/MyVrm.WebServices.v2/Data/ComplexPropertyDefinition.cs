﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace MyVrm.WebServices.Data
{
    internal delegate TComplexProperty CreateComplexPropertyDelegate<TComplexProperty>() where TComplexProperty : ComplexProperty;

    internal class ComplexPropertyDefinition<TComplexProperty> : ComplexPropertyDefinitionBase where TComplexProperty : ComplexProperty
    {
        private readonly CreateComplexPropertyDelegate<TComplexProperty> _propertyCreationDelegate;

        internal ComplexPropertyDefinition(string xmlElementName, CreateComplexPropertyDelegate<TComplexProperty> propertyCreationDelegate) : base(xmlElementName)
        {
            if (propertyCreationDelegate == null) 
                throw new ArgumentNullException("propertyCreationDelegate");
            _propertyCreationDelegate = propertyCreationDelegate;
            PropertyType = typeof (TComplexProperty);
        }

        internal ComplexPropertyDefinition(string xmlElementName, PropertyDefinitionFlags flags, CreateComplexPropertyDelegate<TComplexProperty> propertyCreationDelegate)
            : base(xmlElementName, flags)
        {
            if (propertyCreationDelegate == null)
                throw new ArgumentNullException("propertyCreationDelegate");
            _propertyCreationDelegate = propertyCreationDelegate;
            PropertyType = typeof(TComplexProperty);
        }

        internal ComplexPropertyDefinition(string xmlElementName, string xmlElementNameForWrite, PropertyDefinitionFlags flags, CreateComplexPropertyDelegate<TComplexProperty> propertyCreationDelegate)
            : base(xmlElementName, xmlElementNameForWrite, flags)
        {
            if (propertyCreationDelegate == null)
                throw new ArgumentNullException("propertyCreationDelegate");
            _propertyCreationDelegate = propertyCreationDelegate;
            PropertyType = typeof(TComplexProperty);
        }

        #region Overrides of ComplexPropertyDefinitionBase

        internal override ComplexProperty CreatePropertyInstance(ServiceObject owner)
        {
            var property = _propertyCreationDelegate();
            var ownedProperty = property as IOwnedProperty;
            if (ownedProperty != null)
            {
                ownedProperty.Owner = owner;
            }
            return property;
        }

        #endregion
    }
}
