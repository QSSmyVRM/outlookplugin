﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System.Collections;
using System.Collections.Generic;

namespace MyVrm.WebServices.Data
{
    public sealed class BridgeTypeCollection : BaseCollection<BridgeType>
    {
        internal override void LoadFromXml(MyVrmServiceXmlReader reader)
        {
            do
            {
                reader.Read();
                if (reader.IsStartElement(XmlNamespace.NotSpecified, "type"))
                {
                    var bridgeType = new BridgeType();
                    bridgeType.LoadFromXml(reader);
                    Items.Add(bridgeType);
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "bridgeTypes"));
        }
    }
}