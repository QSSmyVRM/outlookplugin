﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    internal delegate T InitializeLazyMember<T>();

    internal class LazyMember<T>
    {
        private readonly InitializeLazyMember<T> _initializationDelegate;
        private readonly object _lockObject;
        private bool _initialized;
        private T _lazyMember;

        public LazyMember(InitializeLazyMember<T> initializationDelegate)
        {
            _lockObject = new object();
            _initializationDelegate = initializationDelegate;
        }

        public T Member
        {
            get
            {
                if (!_initialized)
                {
                    lock (_lockObject)
                    {
                        if (!_initialized)
                        {
                            _lazyMember = _initializationDelegate();
                        }
                        _initialized = true;
                    }
                }
                return _lazyMember;
            }
        }
    }
}