﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using MyVrm.WebServices.Data;

namespace MyVrm.Outlook.WinForms.Conference
{
    public class ConferenceRoom
    {
        public ConferenceRoom()
        {
            ImageIndex = 0;
        }

        public int ImageIndex { get; set; }
        public RoomId Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Floor { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Phone { get; set; }
        public MediaType MediaType { get; set; }
        public string AssistantInCharge { get; set; }
        public string MiddleTier { get; set; }
        public int Capacity { get; set; }
		public bool Approval { get; set; }
		public RoomCategoryType RoomCategory { get; set; }
    }
}