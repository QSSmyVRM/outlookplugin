﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    public sealed class ManagedRoom
    {
        internal ManagedRoom()
        {
        }

        public RoomId Id { get; private set; }
        public string Name { get; private set; }
        public int Capacity { get; private set; }
        public bool Projector { get; private set; }
        public int MaxNumConcurrent { get; private set; }
        public bool VideoAvailable { get; private set; }
        public bool Deleted { get; private set; }
        public bool Locked { get; private set; }
        public TopTier TopTier { get; internal set; }
        public MiddleTier MiddleTier { get; internal set; }
		public bool Approval { get; private set; }

        internal void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
        {
            do
            {
                reader.Read();
                switch (reader.LocalName)
                {
                    case "level1ID":
                        Id = new RoomId();
                        Id.LoadFromXml(reader, "level1ID");
                        break;
                    case "level1Name":
                        Name = reader.ReadElementValue();
                        break;
                    case "capacity":
                        Capacity = reader.ReadElementValue<int>();
                        break;
                    case "projector":
                        Projector = Utilities.BoolStringToBool(reader.ReadElementValue());
                        break;
                    case "maxNumConcurrent":
                        MaxNumConcurrent = reader.ReadElementValue<int>();
                        break;
                    case "videoAvailable":
                        VideoAvailable = Utilities.BoolStringToBool(reader.ReadElementValue());
                        break;
                    case "deleted":
                        Deleted = Utilities.BoolStringToBool(reader.ReadElementValue());
                        break;
                    case "locked":
                        Locked = Utilities.BoolStringToBool(reader.ReadElementValue());
                        break;
					case "Approval":
						Approval = Utilities.BoolStringToBool(reader.ReadElementValue());
						break;
                    default:
                        reader.SkipCurrentElement();
                        break;
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, xmlElementName));
        }
    }
}
