﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace MyVrm.WebServices.Data
{
    internal class GetRoomCalendarResponse : ServiceResponse
    {
        internal string ResponseXmlElementName { get; set; }
        private readonly List<ConferenceOccurrence> _occurences = new List<ConferenceOccurrence>();

        internal ReadOnlyCollection<ConferenceOccurrence> Occurences
        {
            get
            {
                return new ReadOnlyCollection<ConferenceOccurrence>(_occurences);
            }
        }
        internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
        {
            reader.EnsureCurrentNodeIsStartElement(XmlNamespace.NotSpecified, ResponseXmlElementName);
            do
            {
                reader.Read();
                if (reader.LocalName == "days")
                {
                    do
                    {
                        reader.Read();
                        if (reader.LocalName == "day")
                        {
                            do
                            {
                                reader.Read();
                                if (reader.LocalName == "conferences")
                                {
                                    do
                                    {
                                        reader.Read();
                                        if (reader.LocalName == "conference")
                                        {
                                            var occurence = new ConferenceOccurrence();
                                            occurence.LoadFromXml(reader, "conference");
                                            _occurences.Add(occurence);
                                        }
                                        else
                                        {
                                            reader.SkipCurrentElement();
                                        }
                                    } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "conferences"));
                                }
                            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "day"));
                        }
                    } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "days"));
                }
                else
                {
                    reader.SkipCurrentElement();
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, ResponseXmlElementName));
        }
    }
}
