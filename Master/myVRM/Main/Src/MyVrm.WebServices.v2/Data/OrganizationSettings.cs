﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    public class OrganizationSettingsOld
    {
        public class Preferences : ComplexProperty
        {
            /// <summary>
            /// If true Audio video work order is available.
            /// </summary>
            public bool IsFacilitiesEnabled { get; set; }
            /// <summary>
            /// If true catering work order is available.
            /// </summary>
            public bool IsCateringEnabled { get; set; }
            /// <summary>
            /// if true house keeping work order is available.
            /// </summary>
            public bool IsHouseKeepingEnabled { get; set; }

			/// <summary>
			/// if true Blue Jeans conference is available.
            /// </summary>
            public bool IsBlueJeansEnabled{ get; set; }
			/// <summary>
			/// if true Jabber conference is available.
            /// </summary>
            public bool IsJabberEnabled{ get; set; }
			/// <summary>
			/// if true Lync conference is available.
            /// </summary>
            public bool IsLyncEnabled{ get; set; }
			/// <summary>
			/// if true Vidtel conference is available.
            /// </summary>
            public bool IsVidtelEnabled{ get; set; }
			/// <summary>
			/// if true Cloud conference is available.
            /// </summary>
			public bool IsCloudEnabled { get; set; }

            public bool EnablePublicConferencing { get; set; }
			
			public override object Clone()
			{
				Preferences copy = new Preferences();
				copy.IsCateringEnabled = IsCateringEnabled;
				copy.IsFacilitiesEnabled = IsFacilitiesEnabled;
				copy.IsHouseKeepingEnabled = IsHouseKeepingEnabled;
				copy.IsBlueJeansEnabled = IsBlueJeansEnabled;
				copy.IsJabberEnabled = IsJabberEnabled;
				copy.IsLyncEnabled = IsLyncEnabled;
				copy.IsVidtelEnabled = IsVidtelEnabled;
				copy.IsCloudEnabled = IsCloudEnabled;
                copy.EnablePublicConferencing = EnablePublicConferencing;

				return copy;
			}

            internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
            {
                switch(reader.LocalName)
                {
                    case "EnableFacilites":
                    {
                        IsFacilitiesEnabled = reader.ReadElementValue<bool>();
                        return true;
                    }
                    case "EnableCatering":
                    {
                        IsCateringEnabled = reader.ReadElementValue<bool>();
                        return true;
                    }
                    case "EnableHouseKeeping":
                    {
                        IsHouseKeepingEnabled = reader.ReadElementValue<bool>();
                        return true;
                    }
					case "EnableBlueJeans":
                    {
						IsBlueJeansEnabled = reader.ReadElementValue<bool>();
                        return true;
                    }
					case "EnableJabber":
					{
						IsJabberEnabled = reader.ReadElementValue<bool>();
						return true;
					}
					case "EnableLync":
					{
						IsLyncEnabled = reader.ReadElementValue<bool>();
						return true;
					}
					case "EnableVidtel":
					{
						IsVidtelEnabled = reader.ReadElementValue<bool>();
						return true;
					}
					case "EnableCloud":
					{
						IsCloudEnabled = reader.ReadElementValue<bool>();
						return true;
					}
                    case "EnablePublicConference":
                    {
                        EnablePublicConferencing = reader.ReadElementValue<bool>();
                        return true;
                    }
                }
                return false;
            }
        }

        public Preferences Preference { get; private set; }
        public OrganizationId OrganizationId { get; private set; }

        internal void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
        {
            do
            {
                reader.Read();
                switch (reader.LocalName)
                {
                    case "preference":
                        Preference = new Preferences();
                        Preference.LoadFromXml(reader, reader.LocalName);
                        break;
                    case "organizationID":
                        OrganizationId = new OrganizationId();
                        OrganizationId.LoadFromXml(reader, reader.LocalName);
                        break;
                    default:
                        reader.SkipCurrentElement();
                        break;
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, xmlElementName));
        }
    }
}
