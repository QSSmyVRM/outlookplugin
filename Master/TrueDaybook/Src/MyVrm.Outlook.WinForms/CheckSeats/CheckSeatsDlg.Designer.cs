﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.Outlook.WinForms.CheckSeats
{
	partial class CheckSeatsDlg
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			DevExpress.XtraCharts.XYDiagram xyDiagram1 = new DevExpress.XtraCharts.XYDiagram();
			DevExpress.XtraCharts.ConstantLine constantLine1 = new DevExpress.XtraCharts.ConstantLine();
			DevExpress.XtraCharts.SecondaryAxisX secondaryAxisX1 = new DevExpress.XtraCharts.SecondaryAxisX();
			DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
			DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel1 = new DevExpress.XtraCharts.PointSeriesLabel();
			DevExpress.XtraCharts.PointOptions pointOptions1 = new DevExpress.XtraCharts.PointOptions();
			DevExpress.XtraCharts.StepLineSeriesView stepLineSeriesView1 = new DevExpress.XtraCharts.StepLineSeriesView();
			DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel2 = new DevExpress.XtraCharts.PointSeriesLabel();
			DevExpress.XtraCharts.StepLineSeriesView stepLineSeriesView2 = new DevExpress.XtraCharts.StepLineSeriesView();
			this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
			this.radioGroupDisplayMode = new DevExpress.XtraEditors.RadioGroup();
			this.chartControl = new DevExpress.XtraCharts.ChartControl();
			this.bnRefresh = new DevExpress.XtraEditors.SimpleButton();
			this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
			this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.layoutControlDisplayMode = new DevExpress.XtraLayout.LayoutControlItem();
			this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.schedulerStorage1 = new DevExpress.XtraScheduler.SchedulerStorage(this.components);
			((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).BeginInit();
			this.ContentPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
			this.layoutControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.radioGroupDisplayMode.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chartControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(xyDiagram1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(secondaryAxisX1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(pointSeriesLabel1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(stepLineSeriesView1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(pointSeriesLabel2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(stepLineSeriesView2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlDisplayMode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.schedulerStorage1)).BeginInit();
			this.SuspendLayout();
			// 
			// ContentPanel
			// 
			this.ContentPanel.Controls.Add(this.layoutControl1);
			this.ContentPanel.Size = new System.Drawing.Size(1137, 414);
			// 
			// layoutControl1
			// 
			this.layoutControl1.Controls.Add(this.radioGroupDisplayMode);
			this.layoutControl1.Controls.Add(this.chartControl);
			this.layoutControl1.Controls.Add(this.bnRefresh);
			this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.layoutControl1.Location = new System.Drawing.Point(0, 0);
			this.layoutControl1.Name = "layoutControl1";
			this.layoutControl1.Root = this.layoutControlGroup1;
			this.layoutControl1.Size = new System.Drawing.Size(1137, 414);
			this.layoutControl1.TabIndex = 0;
			this.layoutControl1.Text = "layoutControl1";
			// 
			// radioGroupDisplayMode
			// 
			this.radioGroupDisplayMode.Location = new System.Drawing.Point(108, 372);
			this.radioGroupDisplayMode.Name = "radioGroupDisplayMode";
			this.radioGroupDisplayMode.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
			this.radioGroupDisplayMode.Properties.Appearance.Options.UseForeColor = true;
			this.radioGroupDisplayMode.Size = new System.Drawing.Size(621, 27);
			this.radioGroupDisplayMode.StyleController = this.layoutControl1;
			this.radioGroupDisplayMode.TabIndex = 7;
			this.radioGroupDisplayMode.SelectedIndexChanged += new System.EventHandler(this.radioGroupDisplayMode_SelectedIndexChanged);
			// 
			// chartControl
			// 
			this.chartControl.AppearanceName = "Northern Lights";
			xyDiagram1.AxisX.AutoScaleBreaks.Enabled = true;
			xyDiagram1.AxisX.AutoScaleBreaks.MaxCount = 12;
			xyDiagram1.AxisX.DateTimeGridAlignment = DevExpress.XtraCharts.DateTimeMeasurementUnit.Minute;
			xyDiagram1.AxisX.DateTimeOptions.Format = DevExpress.XtraCharts.DateTimeFormat.Custom;
			xyDiagram1.AxisX.DateTimeOptions.FormatString = "HH:mm";
			xyDiagram1.AxisX.GridLines.Visible = true;
			xyDiagram1.AxisX.MinorCount = 12;
			xyDiagram1.AxisX.Range.Auto = false;
			xyDiagram1.AxisX.Range.MaxValueSerializable = "12/29/2012 00:00:00.000";
			xyDiagram1.AxisX.Range.MinValueSerializable = "12/25/2012 00:00:00.000";
			xyDiagram1.AxisX.Range.ScrollingRange.Auto = false;
			xyDiagram1.AxisX.Range.ScrollingRange.MaxValueSerializable = "12/29/2012 00:00:00.000";
			xyDiagram1.AxisX.Range.ScrollingRange.MinValueSerializable = "12/25/2012 00:00:00.000";
			xyDiagram1.AxisX.Range.ScrollingRange.SideMarginsEnabled = true;
			xyDiagram1.AxisX.Range.SideMarginsEnabled = true;
			xyDiagram1.AxisX.VisibleInPanesSerializable = "-1";
			constantLine1.AxisValueSerializable = "7";
			constantLine1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			constantLine1.LineStyle.DashStyle = DevExpress.XtraCharts.DashStyle.Solid;
			constantLine1.LineStyle.Thickness = 4;
			constantLine1.Name = "MaxSeats";
			constantLine1.Title.Visible = false;
			xyDiagram1.AxisY.ConstantLines.AddRange(new DevExpress.XtraCharts.ConstantLine[] {
            constantLine1});
			xyDiagram1.AxisY.NumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Number;
			xyDiagram1.AxisY.NumericOptions.Precision = 0;
			xyDiagram1.AxisY.Range.ScrollingRange.SideMarginsEnabled = true;
			xyDiagram1.AxisY.Range.SideMarginsEnabled = true;
			xyDiagram1.AxisY.VisibleInPanesSerializable = "-1";
			xyDiagram1.DefaultPane.EnableAxisYScrolling = DevExpress.Utils.DefaultBoolean.True;
			xyDiagram1.DefaultPane.ScrollBarOptions.BarThickness = 15;
			xyDiagram1.EnableAxisXScrolling = true;
			xyDiagram1.EnableAxisYScrolling = true;
			secondaryAxisX1.AxisID = 0;
			secondaryAxisX1.DateTimeScaleMode = DevExpress.XtraCharts.DateTimeScaleMode.Manual;
			secondaryAxisX1.GridLines.Color = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			secondaryAxisX1.GridLines.LineStyle.Thickness = 5;
			secondaryAxisX1.GridLines.Visible = true;
			secondaryAxisX1.GridSpacingAuto = false;
			secondaryAxisX1.Name = "Secondary AxisX 1";
			secondaryAxisX1.NumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Number;
			secondaryAxisX1.NumericOptions.Precision = 0;
			secondaryAxisX1.Range.ScrollingRange.SideMarginsEnabled = true;
			secondaryAxisX1.Range.SideMarginsEnabled = false;
			secondaryAxisX1.VisibleInPanesSerializable = "-1";
			xyDiagram1.SecondaryAxesX.AddRange(new DevExpress.XtraCharts.SecondaryAxisX[] {
            secondaryAxisX1});
			this.chartControl.Diagram = xyDiagram1;
			this.chartControl.EmptyChartText.Text = "No data";
			this.chartControl.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.Left;
			this.chartControl.Legend.AlignmentVertical = DevExpress.XtraCharts.LegendAlignmentVertical.TopOutside;
			this.chartControl.Legend.EquallySpacedItems = false;
			this.chartControl.Legend.Shadow.Color = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
			this.chartControl.Location = new System.Drawing.Point(12, 12);
			this.chartControl.Name = "chartControl";
			series1.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
			pointSeriesLabel1.LineVisible = true;
			pointSeriesLabel1.Visible = false;
			series1.Label = pointSeriesLabel1;
			series1.Name = "Seats Availability";
			pointOptions1.ArgumentDateTimeOptions.Format = DevExpress.XtraCharts.DateTimeFormat.Custom;
			series1.PointOptions = pointOptions1;
			stepLineSeriesView1.LineMarkerOptions.Kind = DevExpress.XtraCharts.MarkerKind.Diamond;
			stepLineSeriesView1.LineMarkerOptions.Size = 14;
			stepLineSeriesView1.LineMarkerOptions.Visible = false;
			series1.View = stepLineSeriesView1;
			this.chartControl.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1};
			this.chartControl.SeriesTemplate.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
			pointSeriesLabel2.LineVisible = true;
			this.chartControl.SeriesTemplate.Label = pointSeriesLabel2;
			this.chartControl.SeriesTemplate.View = stepLineSeriesView2;
			this.chartControl.Size = new System.Drawing.Size(1113, 341);
			this.chartControl.TabIndex = 4;
			this.chartControl.Scroll += new DevExpress.XtraCharts.ChartScrollEventHandler(this.chartControl_Scroll);
			this.chartControl.ObjectHotTracked += new DevExpress.XtraCharts.HotTrackEventHandler(this.chartControl_ObjectHotTracked);
			// 
			// bnRefresh
			// 
			this.bnRefresh.Location = new System.Drawing.Point(934, 374);
			this.bnRefresh.Name = "bnRefresh";
			this.bnRefresh.Size = new System.Drawing.Size(191, 28);
			this.bnRefresh.StyleController = this.layoutControl1;
			this.bnRefresh.TabIndex = 5;
			this.bnRefresh.Text = "Refresh displayed data";
			this.bnRefresh.Click += new System.EventHandler(this.bnRefresh_Click);
			// 
			// layoutControlGroup1
			// 
			this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
			this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
			this.layoutControlGroup1.GroupBordersVisible = false;
			this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.emptySpaceItem2,
            this.layoutControlDisplayMode,
            this.emptySpaceItem1,
            this.emptySpaceItem3});
			this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup1.Name = "Root";
			this.layoutControlGroup1.Size = new System.Drawing.Size(1137, 414);
			this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup1.Text = "Root";
			this.layoutControlGroup1.TextVisible = false;
			// 
			// layoutControlItem1
			// 
			this.layoutControlItem1.Control = this.chartControl;
			this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
			this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItem1.Name = "layoutControlItem1";
			this.layoutControlItem1.Size = new System.Drawing.Size(1117, 345);
			this.layoutControlItem1.Text = "layoutControlItem1";
			this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem1.TextToControlDistance = 0;
			this.layoutControlItem1.TextVisible = false;
			// 
			// layoutControlItem2
			// 
			this.layoutControlItem2.Control = this.bnRefresh;
			this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
			this.layoutControlItem2.Location = new System.Drawing.Point(922, 362);
			this.layoutControlItem2.MinSize = new System.Drawing.Size(95, 27);
			this.layoutControlItem2.Name = "layoutControlItem2";
			this.layoutControlItem2.Size = new System.Drawing.Size(195, 32);
			this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem2.Text = "layoutControlItem2";
			this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem2.TextToControlDistance = 0;
			this.layoutControlItem2.TextVisible = false;
			// 
			// emptySpaceItem2
			// 
			this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
			this.emptySpaceItem2.Location = new System.Drawing.Point(0, 345);
			this.emptySpaceItem2.MinSize = new System.Drawing.Size(1, 1);
			this.emptySpaceItem2.Name = "emptySpaceItem2";
			this.emptySpaceItem2.Size = new System.Drawing.Size(1117, 12);
			this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.emptySpaceItem2.Text = "emptySpaceItem2";
			this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
			// 
			// layoutControlDisplayMode
			// 
			this.layoutControlDisplayMode.Control = this.radioGroupDisplayMode;
			this.layoutControlDisplayMode.CustomizationFormText = "Display data for";
			this.layoutControlDisplayMode.Location = new System.Drawing.Point(0, 357);
			this.layoutControlDisplayMode.MinSize = new System.Drawing.Size(147, 31);
			this.layoutControlDisplayMode.Name = "layoutControlDisplayMode";
			this.layoutControlDisplayMode.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
			this.layoutControlDisplayMode.Size = new System.Drawing.Size(724, 37);
			this.layoutControlDisplayMode.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlDisplayMode.Text = "Display data for";
			this.layoutControlDisplayMode.TextSize = new System.Drawing.Size(89, 16);
			// 
			// emptySpaceItem1
			// 
			this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
			this.emptySpaceItem1.Location = new System.Drawing.Point(724, 357);
			this.emptySpaceItem1.MinSize = new System.Drawing.Size(104, 24);
			this.emptySpaceItem1.Name = "emptySpaceItem1";
			this.emptySpaceItem1.Size = new System.Drawing.Size(198, 37);
			this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.emptySpaceItem1.Text = "emptySpaceItem1";
			this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
			// 
			// emptySpaceItem3
			// 
			this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
			this.emptySpaceItem3.Location = new System.Drawing.Point(922, 357);
			this.emptySpaceItem3.MinSize = new System.Drawing.Size(1, 1);
			this.emptySpaceItem3.Name = "emptySpaceItem3";
			this.emptySpaceItem3.Size = new System.Drawing.Size(195, 5);
			this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.emptySpaceItem3.Text = "emptySpaceItem3";
			this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
			// 
			// CheckSeatsDlg
			// 
			this.ApplyEnabled = true;
			this.ApplyVisible = true;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
			this.ClientSize = new System.Drawing.Size(1161, 470);
			this.Name = "CheckSeatsDlg";
			this.Text = "Check Available Seats";
			this.Load += new System.EventHandler(this.CheckSeatsDlg_Load);
			((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).EndInit();
			this.ContentPanel.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
			this.layoutControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.radioGroupDisplayMode.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(secondaryAxisX1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(xyDiagram1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(pointSeriesLabel1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(stepLineSeriesView1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(pointSeriesLabel2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(stepLineSeriesView2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chartControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlDisplayMode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.schedulerStorage1)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private DevExpress.XtraLayout.LayoutControl layoutControl1;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private DevExpress.XtraScheduler.SchedulerStorage schedulerStorage1;
		private DevExpress.XtraCharts.ChartControl chartControl;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
		private DevExpress.XtraEditors.SimpleButton bnRefresh;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
		private DevExpress.XtraEditors.RadioGroup radioGroupDisplayMode;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlDisplayMode;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
	}
}
