﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	internal class TDDeleteConferenceRequest : ServiceRequestBase<TDDeleteConferenceResponse>
	{
		internal string ConfId;

		internal TDDeleteConferenceRequest(MyVrmService service)
			: base(service)
		{
		}

		#region Overrides of ServiceRequestBase

		internal override string GetXmlElementName()
		{
			return "TDDeleteConference";
		}

		internal override string GetCommandName()
		{
			return Constants.TDDeleteConferenceCommandName;
		}

		internal override string GetResponseXmlElementName()
		{
			return "TDDeleteConference";
		}

		internal override TDDeleteConferenceResponse ParseResponse(MyVrmServiceXmlReader reader)
		{
			var response = new TDDeleteConferenceResponse(); 
			response.LoadFromXml(reader, GetResponseXmlElementName());
			return response;
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			UserId.WriteToXml(writer, "userID");
			writer.WriteElementValue(XmlNamespace.NotSpecified, "conferenceId", ConfId);
		}
		#endregion
	}

	public class TDDeleteConferenceResponse : ServiceResponse
	{
		public string RetMessage { set; get; }
        public int RetCode { set; get; }
		public long SettingsVersion { set; get; }
		public XMLError XMLError { set; get; }

		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{
			while (!reader.IsEndElement(XmlNamespace.NotSpecified, "retState"))
			{
				try
				{
					switch (reader.LocalName)
					{
						case "retCode":
							RetCode = reader.ReadElementValue<int>();
							break;
						case "retMessage":
							RetMessage = reader.ReadElementValue();
							break;
						case "settingsVersion":
							SettingsVersion = reader.ReadElementValue<long>();
							break;
						case "error":
							XMLError = new XMLError();
							XMLError.LoadFromXml(reader, XmlNamespace.NotSpecified, "error");
							break;
					}
					reader.Read();
				}
				catch (Exception ex)
				{
					MyVrmService.TraceSource.TraceError(reader.LocalName + ": " + ex.Message, null);
					reader.Read();
					//throw;
				}
			}
		}
	}
}