﻿using MyVrm.Outlook.WinForms.Conference;

namespace MyVrmAddin2003
{
    partial class ConferencePage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.conferenceControl = new MyVrm.Outlook.WinForms.Conference.ConferenceControl();
            this.SuspendLayout();
            // 
            // conferenceControl
            // 
            this.conferenceControl.AutoSize = true;
            this.conferenceControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.conferenceControl.Location = new System.Drawing.Point(0, 0);
            this.conferenceControl.Name = "conferenceControl";
            this.conferenceControl.Size = new System.Drawing.Size(744, 441);
            this.conferenceControl.TabIndex = 0;
            // 
            // ConferencePage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.conferenceControl);
            this.Name = "ConferencePage";
            this.Size = new System.Drawing.Size(744, 441);
            this.CustomFormShowing += new System.EventHandler(this.ConferencePage_CustomFormShowing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ConferenceControl conferenceControl;




    }
}
