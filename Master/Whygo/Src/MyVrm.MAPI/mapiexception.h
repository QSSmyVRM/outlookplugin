#pragma once

#include "Stdafx.h"

namespace MyVrm 
{
	namespace MAPI
	{
		using namespace System::Runtime::Serialization;
		using namespace System::Resources;
		using namespace System::Reflection;

		[Serializable]
		public ref class MapiException: public Exception
		{
		public:

			MapiException(SerializationInfo ^info, StreamingContext context);
			MapiException(HRESULT hr, String ^message);
			MapiException(String^ localizedString, String^ message, HRESULT hr);

			static void ThrowIfError(HRESULT hResult, String ^message);
		internal:
			template<class I>
			static void ThrowIfError(HRESULT hResult, String ^sProcName, I mapiInterface)
			{
				if (FAILED(hResult))
				{
					LPMAPIERROR pMapiError;
					String ^sMsg = String::Format("{0} (0x{1:X8})", sProcName, hResult);

					try
					{
						if (mapiInterface && SUCCEEDED(mapiInterface->GetLastError(hResult, 0, &pMapiError)))
						{
							if (pMapiError)
								sMsg += ". " + gcnew String(reinterpret_cast<LPSTR>(pMapiError->lpszError));
						}
					}
					catch(...) {};

					ThrowIfError(hResult, sMsg);
				}	
			}
		private:
			static Exception^ Create(HRESULT hResult, String^ message);
			static ResourceManager^ resourceManager = gcnew ResourceManager("MapiLib.Strings", Assembly::GetExecutingAssembly());
		};
		[Serializable]
		public ref class MapiCallFailedException : public MapiException
		{
		public:
			MapiCallFailedException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiCallFailedException(HRESULT hr, String ^message) : MapiException("ExceptionMapiCallFailed", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiNotEnoughMemoryException : public MapiException
		{
		public:
			MapiNotEnoughMemoryException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiNotEnoughMemoryException(HRESULT hr, String ^message) : MapiException("ExceptionMapiNotEnoughMemory", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiInvalidParameterException : public MapiException
		{
		public:
			MapiInvalidParameterException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiInvalidParameterException(HRESULT hr, String ^message) : MapiException("ExceptionMapiInvalidParameter", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiInterfaceNotSupportedException : public MapiException
		{
		public:
			MapiInterfaceNotSupportedException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiInterfaceNotSupportedException(HRESULT hr, String ^message) : MapiException("ExceptionMapiInterfaceNotSupported", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiNoAccessException : public MapiException
		{
		public:
			MapiNoAccessException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiNoAccessException(HRESULT hr, String ^message) : MapiException("ExceptionMapiNoAccess", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiNoSupportException : public MapiException
		{
		public:
			MapiNoSupportException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiNoSupportException(HRESULT hr, String ^message) : MapiException("ExceptionMapiNoSupport", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiBadCharWidthException : public MapiException
		{
		public:
			MapiBadCharWidthException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiBadCharWidthException(HRESULT hr, String ^message) : MapiException("ExceptionMapiBadCharWidth", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiStringTooLongException : public MapiException
		{
		public:
			MapiStringTooLongException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiStringTooLongException(HRESULT hr, String ^message) : MapiException("ExceptionMapiStringTooLong", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiUnknownFlagsException : public MapiException
		{
		public:
			MapiUnknownFlagsException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiUnknownFlagsException(HRESULT hr, String ^message) : MapiException("ExceptionMapiUnknownFlags", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiInvalidEntryIdException : public MapiException
		{
		public:
			MapiInvalidEntryIdException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiInvalidEntryIdException(HRESULT hr, String ^message) : MapiException("ExceptionMapiInvalidEntryId", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiInvalidObjectException : public MapiException
		{
		public:
			MapiInvalidObjectException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiInvalidObjectException(HRESULT hr, String ^message) : MapiException("ExceptionMapiInvalidObject", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiObjectChangedException : public MapiException
		{
		public:
			MapiObjectChangedException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiObjectChangedException(HRESULT hr, String ^message) : MapiException("ExceptionMapiObjectChanged", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiObjectDeletedException : public MapiException
		{
		public:
			MapiObjectDeletedException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiObjectDeletedException(HRESULT hr, String ^message) : MapiException("ExceptionMapiObjectDeleted", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiBusyException : public MapiException
		{
		public:
			MapiBusyException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiBusyException(HRESULT hr, String ^message) : MapiException("ExceptionMapiBusy", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiNotEnoughDiskException : public MapiException
		{
		public:
			MapiNotEnoughDiskException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiNotEnoughDiskException(HRESULT hr, String ^message) : MapiException("ExceptionMapiNotEnoughDisk", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiNotEnoughResourcesException : public MapiException
		{
		public:
			MapiNotEnoughResourcesException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiNotEnoughResourcesException(HRESULT hr, String ^message) : MapiException("ExceptionMapiNotEnoughResources", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiNotFoundException : public MapiException
		{
		public:
			MapiNotFoundException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiNotFoundException(HRESULT hr, String ^message) : MapiException("ExceptionMapiNotFound", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiVersionException : public MapiException
		{
		public:
			MapiVersionException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiVersionException(HRESULT hr, String ^message) : MapiException("ExceptionMapiVersion", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiLogonFailedException : public MapiException
		{
		public:
			MapiLogonFailedException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiLogonFailedException(HRESULT hr, String ^message) : MapiException("ExceptionMapiLogonFailed", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiSessionLimitException : public MapiException
		{
		public:
			MapiSessionLimitException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiSessionLimitException(HRESULT hr, String ^message) : MapiException("ExceptionMapiSessionLimit", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiUserCancelException : public MapiException
		{
		public:
			MapiUserCancelException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiUserCancelException(HRESULT hr, String ^message) : MapiException("ExceptionMapiUserCancel", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiUnableToAbortException : public MapiException
		{
		public:
			MapiUnableToAbortException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiUnableToAbortException(HRESULT hr, String ^message) : MapiException("ExceptionMapiUnableToAbort", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiNetworkErrorException : public MapiException
		{
		public:
			MapiNetworkErrorException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiNetworkErrorException(HRESULT hr, String ^message) : MapiException("ExceptionMapiNetworkError", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiDiskErrorException : public MapiException
		{
		public:
			MapiDiskErrorException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiDiskErrorException(HRESULT hr, String ^message) : MapiException("ExceptionMapiDiskError", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiTooComplexException : public MapiException
		{
		public:
			MapiTooComplexException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiTooComplexException(HRESULT hr, String ^message) : MapiException("ExceptionMapiTooComplex", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiBadColumnException : public MapiException
		{
		public:
			MapiBadColumnException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiBadColumnException(HRESULT hr, String ^message) : MapiException("ExceptionMapiBadColumn", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiExtendedErrorException : public MapiException
		{
		public:
			MapiExtendedErrorException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiExtendedErrorException(HRESULT hr, String ^message) : MapiException("ExceptionMapiExtendedError", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiComputedException : public MapiException
		{
		public:
			MapiComputedException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiComputedException(HRESULT hr, String ^message) : MapiException("ExceptionMapiComputed", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiCorruptDataException : public MapiException
		{
		public:
			MapiCorruptDataException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiCorruptDataException(HRESULT hr, String ^message) : MapiException("ExceptionMapiCorruptData", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiUnconfiguredException : public MapiException
		{
		public:
			MapiUnconfiguredException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiUnconfiguredException(HRESULT hr, String ^message) : MapiException("ExceptionMapiUnconfigured", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiFailOneProviderException : public MapiException
		{
		public:
			MapiFailOneProviderException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiFailOneProviderException(HRESULT hr, String ^message) : MapiException("ExceptionMapiFailOneProvider", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiUnknownCodePageException : public MapiException
		{
		public:
			MapiUnknownCodePageException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiUnknownCodePageException(HRESULT hr, String ^message) : MapiException("ExceptionMapiUnknownCodePage", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiUnknownLocaleException : public MapiException
		{
		public:
			MapiUnknownLocaleException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiUnknownLocaleException(HRESULT hr, String ^message) : MapiException("ExceptionMapiUnknownLocale", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiPasswordChangeRequiredException : public MapiException
		{
		public:
			MapiPasswordChangeRequiredException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiPasswordChangeRequiredException(HRESULT hr, String ^message) : MapiException("ExceptionMapiPasswordChangeRequired", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiPasswordExpiredException : public MapiException
		{
		public:
			MapiPasswordExpiredException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiPasswordExpiredException(HRESULT hr, String ^message) : MapiException("ExceptionMapiPasswordExpired", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiInvalidWorkstationAccountException : public MapiException
		{
		public:
			MapiInvalidWorkstationAccountException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiInvalidWorkstationAccountException(HRESULT hr, String ^message) : MapiException("ExceptionMapiInvalidWorkstationAccount", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiInvalidAccessTimeException : public MapiException
		{
		public:
			MapiInvalidAccessTimeException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiInvalidAccessTimeException(HRESULT hr, String ^message) : MapiException("ExceptionMapiInvalidAccessTime", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiAccountDisabledException : public MapiException
		{
		public:
			MapiAccountDisabledException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiAccountDisabledException(HRESULT hr, String ^message) : MapiException("ExceptionMapiAccountDisabled", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiEndOfSessionException : public MapiException
		{
		public:
			MapiEndOfSessionException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiEndOfSessionException(HRESULT hr, String ^message) : MapiException("ExceptionMapiEndOfSession", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiUnknownEntryIdException : public MapiException
		{
		public:
			MapiUnknownEntryIdException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiUnknownEntryIdException(HRESULT hr, String ^message) : MapiException("ExceptionMapiUnknownEntryId", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiMissingRequiredColumnException : public MapiException
		{
		public:
			MapiMissingRequiredColumnException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiMissingRequiredColumnException(HRESULT hr, String ^message) : MapiException("ExceptionMapiMissingRequiredColumn", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiBadValueException : public MapiException
		{
		public:
			MapiBadValueException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiBadValueException(HRESULT hr, String ^message) : MapiException("ExceptionMapiBadValue", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiInvalidTypeException : public MapiException
		{
		public:
			MapiInvalidTypeException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiInvalidTypeException(HRESULT hr, String ^message) : MapiException("ExceptionMapiInvalidType", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiTypeNoSupportException : public MapiException
		{
		public:
			MapiTypeNoSupportException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiTypeNoSupportException(HRESULT hr, String ^message) : MapiException("ExceptionMapiTypeNoSupport", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiUnexpectedTypeException : public MapiException
		{
		public:
			MapiUnexpectedTypeException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiUnexpectedTypeException(HRESULT hr, String ^message) : MapiException("ExceptionMapiUnexpectedType", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiTooBigException : public MapiException
		{
		public:
			MapiTooBigException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiTooBigException(HRESULT hr, String ^message) : MapiException("ExceptionMapiTooBig", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiDeclineCopyException : public MapiException
		{
		public:
			MapiDeclineCopyException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiDeclineCopyException(HRESULT hr, String ^message) : MapiException("ExceptionMapiDeclineCopy", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiUnexpectedIdException : public MapiException
		{
		public:
			MapiUnexpectedIdException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiUnexpectedIdException(HRESULT hr, String ^message) : MapiException("ExceptionMapiUnexpectedId", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiUnableToCompleteException : public MapiException
		{
		public:
			MapiUnableToCompleteException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiUnableToCompleteException(HRESULT hr, String ^message) : MapiException("ExceptionMapiUnableToComplete", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiTimeOutException : public MapiException
		{
		public:
			MapiTimeOutException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiTimeOutException(HRESULT hr, String ^message) : MapiException("ExceptionMapiTimeOut", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiTableEmptyException : public MapiException
		{
		public:
			MapiTableEmptyException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiTableEmptyException(HRESULT hr, String ^message) : MapiException("ExceptionMapiTableEmpty", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiTableTooBigException : public MapiException
		{
		public:
			MapiTableTooBigException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiTableTooBigException(HRESULT hr, String ^message) : MapiException("ExceptionMapiTableTooBig", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiInvalidBookmarkException : public MapiException
		{
		public:
			MapiInvalidBookmarkException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiInvalidBookmarkException(HRESULT hr, String ^message) : MapiException("ExceptionMapiInvalidBookmark", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiWaitException : public MapiException
		{
		public:
			MapiWaitException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiWaitException(HRESULT hr, String ^message) : MapiException("ExceptionMapiWait", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiCancelException : public MapiException
		{
		public:
			MapiCancelException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiCancelException(HRESULT hr, String ^message) : MapiException("ExceptionMapiCancel", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiNotMeException : public MapiException
		{
		public:
			MapiNotMeException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiNotMeException(HRESULT hr, String ^message) : MapiException("ExceptionMapiNotMe", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiCorruptStoreException : public MapiException
		{
		public:
			MapiCorruptStoreException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiCorruptStoreException(HRESULT hr, String ^message) : MapiException("ExceptionMapiCorruptStore", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiNotInQueueException : public MapiException
		{
		public:
			MapiNotInQueueException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiNotInQueueException(HRESULT hr, String ^message) : MapiException("ExceptionMapiNotInQueue", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiNoSuppressException : public MapiException
		{
		public:
			MapiNoSuppressException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiNoSuppressException(HRESULT hr, String ^message) : MapiException("ExceptionMapiNoSupress", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiCollisionException : public MapiException
		{
		public:
			MapiCollisionException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiCollisionException(HRESULT hr, String ^message) : MapiException("ExceptionMapiCollision", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiNotInitializedException : public MapiException
		{
		public:
			MapiNotInitializedException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiNotInitializedException(HRESULT hr, String ^message) : MapiException("ExceptionMapiNotInitialized", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiNonStandardException : public MapiException
		{
		public:
			MapiNonStandardException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiNonStandardException(HRESULT hr, String ^message) : MapiException("ExceptionMapiNonStandard", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiNoRecipientsException : public MapiException
		{
		public:
			MapiNoRecipientsException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiNoRecipientsException(HRESULT hr, String ^message) : MapiException("ExceptionMapiNoRecipients", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiSubmittedException : public MapiException
		{
		public:
			MapiSubmittedException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiSubmittedException(HRESULT hr, String ^message) : MapiException("ExceptionMapiSubmitted", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiHasFoldersException : public MapiException
		{
		public:
			MapiHasFoldersException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiHasFoldersException(HRESULT hr, String ^message) : MapiException("ExceptionMapiHasFolders", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiHasMessagesException : public MapiException
		{
		public:
			MapiHasMessagesException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiHasMessagesException(HRESULT hr, String ^message) : MapiException("ExceptionMapiHasMessages", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiFolderCycleException : public MapiException
		{
		public:
			MapiFolderCycleException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiFolderCycleException(HRESULT hr, String ^message) : MapiException("ExceptionMapiFolderCycle", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiAmbiguousRecipientException : public MapiException
		{
		public:
			MapiAmbiguousRecipientException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiAmbiguousRecipientException(HRESULT hr, String ^message) : MapiException("ExceptionMapiAmbiguousRecipient", message, hr)
			{
			}
		};
		[Serializable]
		public ref class MapiStoreBasedQueryNotSupportedException : public MapiException
		{
		public:
			MapiStoreBasedQueryNotSupportedException(SerializationInfo ^info, StreamingContext context) : MapiException(info, context)
			{
			}
			MapiStoreBasedQueryNotSupportedException(HRESULT hr, String ^message) : MapiException("ExceptionMapiStoreBasedQueryNotSupported", message, hr)
			{
			}
		};
	}
}
