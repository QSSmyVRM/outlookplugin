﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    public class BridgeName
    {
        internal BridgeName()
        {
        }

        public BridgeId Id { get; private set; }
        public string Name { get; private set; }

        internal void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
        {
            do
            {
                reader.Read();
                if (reader.LocalName == "BridgeID")
                {
                    Id = new BridgeId();
                    Id.LoadFromXml(reader, "BridgeID");
                }
                else if (reader.LocalName == "BridgeName")
                {
                    Name = reader.ReadElementValue(XmlNamespace.NotSpecified, "BridgeName");        
                }
                else
                {
                    reader.SkipCurrentElement();
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, xmlElementName));
        }
    }
}
