/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Nodes;
using MyVrm.Common;
using MyVrm.Common.Collections;
using MyVrm.Common.ComponentModel;
using MyVrm.Common.EventBroker;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook.WinForms.Conference
{
    public partial class RoomAudioVideoSettingsListControl : BaseControl
    {
        private readonly RepositoryItemEnumComboBox _callerRepositoryItem;
        private readonly List<RepositoryItemLookUpEdit> _endpointProfileEditors = new List<RepositoryItemLookUpEdit>();
    	private ReadOnlyCollection<BridgeName> _bridgeNames;

		private Dictionary<BridgeId, ReadOnlyCollection<MCUProfile>> _MCUWithProfiles = new Dictionary<BridgeId, ReadOnlyCollection<MCUProfile>>();
		private readonly List<RepositoryItemLookUpEdit> _bridgeProfileEditors = new List<RepositoryItemLookUpEdit>();
		
		public ReadOnlyCollection<BridgeName> Bridges 
		{ 
			get { return _bridgeNames; }
		}

		public Dictionary<BridgeId, ReadOnlyCollection<MCUProfile>> MCUWithProfiles
		{
			get { return _MCUWithProfiles; }
		}

    	private bool _isDirty;

		EventBroker _broker;
		public void SetEventBroker(EventBroker broker)
		{
			_broker = broker;
		}

    	public RoomAudioVideoSettingsListControl()
        {
            InitializeComponent();
			roomNameColumn.Caption = Strings.RoomNameLableText;
			endpointColumn.Caption = Strings.EndpointLableText;
        	endpointProfileColumn.Caption = Strings.EndpointProfileColummnText;
			callerColumn.Caption = Strings.CallerCalleeColummnText; 
        	endpointRepositoryItem.NullText = Strings.NoEndpointsForRoomText;

            bridgeNameColumn.Caption = Strings.BridgeNameLabelText;
            selectedBrigeProfileColumn.Caption = Strings.SelectProfileLabelText;
            useDefaultColumn.Caption = Strings.UseDefault;


            _callerRepositoryItem = new RepositoryItemEnumComboBox
                                        {
                                            TextEditStyle = TextEditStyles.DisableTextEditor
                                        };

			_callerRepositoryItem.Items.AddRange(new EnumListSource(typeof(ConferenceEndpointCallMode)).GetList());
			LocalizedEnum enumNone = new LocalizedEnum("None", ConferenceEndpointCallMode.None);
			_callerRepositoryItem.Items.Remove(enumNone);
			_callerRepositoryItem.EditValueChanged += new EventHandler(_callerRepositoryItem_EditValueChanged);
			endpointsTreeList.RepositoryItems.AddRange(new RepositoryItem[] { _callerRepositoryItem });

            callerColumn.ColumnEdit = _callerRepositoryItem;
            callerColumn.Visible = false;

			useDefaultColumnVisibility = true;
			_delegateExternalSetMCUProfile = new delegateExternalSetMCUProfile(ExternalSetMCUProfile);
        }

		void _callerRepositoryItem_EditValueChanged(object sender, EventArgs e)
		{
			if (_broker != null)
				_broker.OnEvent("event_nonOutlookPropChanged", null);
		}

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false),
         EditorBrowsable(EditorBrowsableState.Never)]
        public object DataSource
        {
            get
            {
                return bindingSource.DataSource;
            }
            set
            {
                bindingSource.DataSource = value;
            }
        }

        private RepositoryItemLookUpEdit CreateEndpointProfileRepositoryItem()
        {
            var lookUpEdit = new RepositoryItemLookUpEdit
                                 {
                                     DisplayMember = "Name",
                                     ValueMember = "Id",
                                     ShowHeader = false,
                                     ShowLines = false,
                                     PopupSizeable = false,
                                     TextEditStyle = TextEditStyles.DisableTextEditor,
                                     NullText = ""
                                 };
            lookUpEdit.Columns.Add(new LookUpColumnInfo("Name"));
			//lookUpEdit.NullText = Strings.NoBridgesForRoomText;
            endpointsTreeList.RepositoryItems.Add(lookUpEdit);
            return lookUpEdit;
        }

		private RepositoryItemLookUpEdit CreateBridgeProfileRepositoryItem()
		{
			var lookUpEdit = new RepositoryItemLookUpEdit
			{
				DisplayMember = "Name",//"BridgeProfileName",
				ValueMember = "Name",////"Id",//"BridgeProfileID",
				ShowHeader = false,
				ShowLines = false,
				PopupSizeable = false,
				TextEditStyle = TextEditStyles.DisableTextEditor,
				NullText = ""//WinForms.Strings.TxtNone
			};
			lookUpEdit.Columns.Add(new LookUpColumnInfo("Name"));//"BridgeProfileName"));
			endpointsTreeList.RepositoryItems.Add(lookUpEdit);
			return lookUpEdit;
		}

        private void ResetEndpointList()
        {
            endpointsTreeList.BeginUpdate();
            try
            {
                _endpointProfileEditors.ForEach(item => endpointsTreeList.RepositoryItems.Remove(item));
                _endpointProfileEditors.Clear();
                var roomEndpoints = ((IList<RoomEndpoint>)DataSource);
				if (roomEndpoints != null)
				{
					foreach (var roomEndpoint in roomEndpoints)
					{
						RepositoryItemLookUpEdit lookUpEdit = CreateEndpointProfileRepositoryItem();
						if (roomEndpoint.Endpoint != null)
						{
							List<EndpointProfile> profilesCommonList = roomEndpoint.Endpoint.Profiles.ToList();

							lookUpEdit.DataSource = profilesCommonList;
						}
						
						_endpointProfileEditors.Add(lookUpEdit);
					}
				}

            	ResetBridgeProfilesRepositoryList();
            }
            finally
            {
                endpointsTreeList.EndUpdate();
            }
        }


		private void ResetBridgeProfilesRepositoryList()
        {
            endpointsTreeList.BeginUpdate();
            try
            {
                _bridgeProfileEditors.ForEach(item => endpointsTreeList.RepositoryItems.Remove(item));
                _bridgeProfileEditors.Clear();
                var roomEndpoints = ((IList<RoomEndpoint>)DataSource);
				if (roomEndpoints != null && roomEndpoints.Count() > 0)
				{
					int i = 0;
					if( _bridgeNames == null )
						GetMCUs();

					foreach (var roomEndpoint in roomEndpoints)
					{
						var foundMCU = _bridgeNames.FirstOrDefault(a => a.Id == roomEndpoint.BridgeId);

						RepositoryItemLookUpEdit lookUpBridgeProfileEdit = CreateBridgeProfileRepositoryItem();
						if (foundMCU != null)
						{
							//var profiles = MCUWithProfiles.FirstOrDefault(a => a.Key == found.Id);
							SetBridgeProfileLookUpEdit(lookUpBridgeProfileEdit, foundMCU.Id);
							SuspendLayout();
							if (i < _bridgeProfileEditors.Count)
								_bridgeProfileEditors.Insert(i, lookUpBridgeProfileEdit);
							else
							{
								_bridgeProfileEditors.Add(lookUpBridgeProfileEdit);
							}
							ResumeLayout();
							roomEndpoint.BridgeName = foundMCU.Name;
							if (roomEndpoint.BridgeId != foundMCU.Id)
							{
								endpointsTreeList.Nodes[i].SetValue(selectedBrigeProfileColumn, ((List<MCUProfile>)lookUpBridgeProfileEdit.DataSource)[0].Name);
							}
							else
							{
								var profiles = MCUWithProfiles.FirstOrDefault(a => a.Key == foundMCU.Id);
								var profile = profiles.Value.FirstOrDefault(a => a.Id == roomEndpoint.BridgeProfileID);
								string setBridgeProfileName = string.Empty;
								if (profile != null)
									setBridgeProfileName = profile.Name;
								else
								{
									if (profiles.Value.Count > 0)
										setBridgeProfileName = MCUProfile.MCUProfileNone.Name;
									else
									{
										setBridgeProfileName = MCUProfile.MCUProfileNoItems.Name;
									}
								}
								roomEndpoint.BridgeProfileName = setBridgeProfileName;
								if (endpointsTreeList.Nodes.Count > i)
									endpointsTreeList.Nodes[i].SetValue(selectedBrigeProfileColumn, setBridgeProfileName);
							}
						}
						i++;
						/*
						RepositoryItemLookUpEdit lookUpBridgeProfileEdit = CreateBridgeProfileRepositoryItem();
						SetBridgeProfileLookUpEdit(lookUpBridgeProfileEdit, roomEndpoint.BridgeId);
						_bridgeProfileEditors.Insert(i++, lookUpBridgeProfileEdit);
						var found = roomEndpoint.Endpoint.Profiles.FirstOrDefault(a=> a.Id == roomEndpoint.ProfileId);
						if (endpointsTreeList.Nodes.Count > i - 1)
							endpointsTreeList.Nodes[i - 1].SetValue(selectedBrigeProfileColumn, found != null? found.BridgeProfileName : WinForms.Strings.TxtNoItems );

						*/
						//if (roomEndpoint.Endpoint != null)
						//{
						//    //List<EndpointProfile> profilesCommonList = roomEndpoint.Endpoint.Profiles.ToList();
						//    var found = MCUWithProfiles.FirstOrDefault(a => a.Key == roomEndpoint.BridgeId);

						//    List<MCUProfile> bridgeProfileCommonList = new List<MCUProfile>();
						//    if (found.Value.Count > 0)
						//    {
						//        bridgeProfileCommonList = found.Value.ToList();
						//        if (found.Value.FirstOrDefault(a => a.Id == MCUProfile.MCUProfileNone.Id) == null)
						//            bridgeProfileCommonList.Add(MCUProfile.MCUProfileNone);
						//    }
						//    else
						//        bridgeProfileCommonList.Add(MCUProfile.MCUProfileNoItems);
						//    lookUpEdit.DataSource = bridgeProfileCommonList;//profilesCommonList;
						//}

						//_bridgeProfileEditors.Add(lookUpBridgeProfileEdit);

						////
						//foreach (var profile in roomEndpoint.Endpoint.Profiles)
						//{
						//    if( !repositoryBridgeProfileNameComboBox.Items.Contains(profile.BridgeProfileName))
						//        repositoryBridgeProfileNameComboBox.Items.Add(profile.BridgeProfileName);
						//}
						//

					}
				}
            }
            finally
            {
                endpointsTreeList.EndUpdate();
            }
        }
		
        private void bindingSource_DataSourceChanged(object sender, EventArgs e)
        {
            ResetEndpointList();
        }

        private void bindingSource_ListChanged(object sender, ListChangedEventArgs e)
        {
            var roomEndpoints = ((IList<RoomEndpoint>)DataSource);
            switch (e.ListChangedType)
            {
                case ListChangedType.Reset:
                {
                    ResetEndpointList();
                    break;
                }
                case ListChangedType.ItemAdded:
                {
                    RoomEndpoint roomEndpoint = roomEndpoints[e.NewIndex];
                    RepositoryItemLookUpEdit lookUpEdit = CreateEndpointProfileRepositoryItem();
                    if (roomEndpoint.Endpoint != null)
                    {
						List<EndpointProfile> profilesCommonList = roomEndpoint.Endpoint.Profiles.ToList();
						
                    	lookUpEdit.DataSource = profilesCommonList;
                    }
                    _endpointProfileEditors.Insert(e.NewIndex, lookUpEdit);

					//
                	RepositoryItemLookUpEdit lookUpBridgeProfileEdit = CreateBridgeProfileRepositoryItem();
					SetBridgeProfileLookUpEdit(lookUpBridgeProfileEdit, roomEndpoint.BridgeId);
					if (e.NewIndex < _bridgeProfileEditors.Count)
						_bridgeProfileEditors.Insert(e.NewIndex, lookUpBridgeProfileEdit);
					else
					{
						_bridgeProfileEditors.Add(lookUpBridgeProfileEdit);
					}
					//endpointsTreeList.Nodes[e.NewIndex].SetValue(selectedBrigeProfileColumn, roomEndpoint.BridgeProfileName);
					//if (roomEndpoint.Endpoint != null)
					//{
					//    var found = MCUWithProfiles.FirstOrDefault(a => a.Key == roomEndpoint.BridgeId);

					//    List<MCUProfile> bridgeProfileCommonList = new List<MCUProfile>();
					//    if (found.Value.Count > 0)
					//    {
					//        bridgeProfileCommonList = found.Value.ToList();
					//        if (found.Value.FirstOrDefault(a => a.Id == MCUProfile.MCUProfileNone.Id) == null)
					//            bridgeProfileCommonList.Add(MCUProfile.MCUProfileNone);
					//    }
					//    else
					//    {
					//        bridgeProfileCommonList.Add(MCUProfile.MCUProfileNoItems);
					//    }
					//    lookUpBridgeProfileEdit.DataSource = bridgeProfileCommonList;
					//}
					
                    break;
                }
                case ListChangedType.ItemDeleted:
                {
                    _endpointProfileEditors.RemoveAt(e.NewIndex);
					if (_bridgeProfileEditors.Count > e.NewIndex)
						_bridgeProfileEditors.RemoveAt(e.NewIndex);
                    break;
                }
                case ListChangedType.ItemMoved:
                {
                    ResetEndpointList();
                    break;
                }
                case ListChangedType.ItemChanged:
//				MyVrmAddin.TraceSource.TraceInformation("bindingSource_ListChanged() : ListChangedType.ItemChanged");
                    break;
            }
        }

		internal void SetBridgeProfileLookUpEdit(RepositoryItemLookUpEdit lookUpBridgeProfileEdit, BridgeId bridgeId)
		{
			if (lookUpBridgeProfileEdit != null)
			{
				if (bridgeId != null)
				{
					var found = MCUWithProfiles.FirstOrDefault(a => a.Key == bridgeId);

					List<MCUProfile> bridgeProfileCommonList = new List<MCUProfile>();
					if (found.Value.Count > 0)
					{
						if (found.Value.FirstOrDefault(a => a.Id == MCUProfile.MCUProfileNone.Id) == null)
							bridgeProfileCommonList.Add(MCUProfile.MCUProfileNone);
						bridgeProfileCommonList.AddRange( found.Value.ToList());
					}
					else
					{
						bridgeProfileCommonList.Add(MCUProfile.MCUProfileNoItems);
					}
					lookUpBridgeProfileEdit.DataSource = bridgeProfileCommonList;
				}
			}
		}

    	public void StopEndpointCustomNodeCellEdit()
		{
			SuspendLayout();
			//StartStop_endpointsTreeList_CustomNodeCellEdit(true);
			//bindingSource.DataSourceChanged -= bindingSource_DataSourceChanged;
		}
		public void StartEndpointCustomNodeCellEdit()
		{
			//StartStop_endpointsTreeList_CustomNodeCellEdit(false);
			//bindingSource.DataSourceChanged += bindingSource_DataSourceChanged;
			ResumeLayout();
			//DataSource = DataSource;
			//ResetEndpointList();
		}
		private void StartStop_endpointsTreeList_CustomNodeCellEdit(bool _bStop)
		{
			if (_bStop)
				endpointsTreeList.CustomNodeCellEdit -= endpointsTreeList_CustomNodeCellEdit;
			else
				endpointsTreeList.CustomNodeCellEdit += endpointsTreeList_CustomNodeCellEdit;
		}

    	private void endpointsTreeList_CustomNodeCellEdit(object sender, GetCustomNodeCellEditEventArgs e)
        {
            var roomEndpoints = ((IList<RoomEndpoint>)DataSource);

        	bool bDoRet = false;
			if( roomEndpoints == null)
			{
				MyVrmAddin.TraceSource.TraceInformation("endpointsTreeList_CustomNodeCellEdit() : DataSource = null");
				bDoRet = true;
			}
			try
			{
				if (e == null)
				{
					MyVrmAddin.TraceSource.TraceInformation("endpointsTreeList_CustomNodeCellEdit() : e == null");
					bDoRet = true;
				}
				else
				{
					if (e.Node == null)
					{
						MyVrmAddin.TraceSource.TraceInformation("endpointsTreeList_CustomNodeCellEdit() : e.Node == null");
						bDoRet = true;
					}
					else
					{
						if (e.Node.Id < 0)
						{
							MyVrmAddin.TraceSource.TraceInformation("endpointsTreeList_CustomNodeCellEdit() : e.Node.Id < 0");
							bDoRet = true;
						}

						if (e.Node.Id >= roomEndpoints.Count)
						{
							MyVrmAddin.TraceSource.TraceInformation("endpointsTreeList_CustomNodeCellEdit() : e.Node.Id ({0}) >= roomEndpoints.Count ({1})", e.Node.Id, roomEndpoints.Count);
							bDoRet = true;
						}
					}
				}
			}
			catch (Exception)
			{
				MyVrmAddin.TraceSource.TraceInformation("endpointsTreeList_CustomNodeCellEdit() : catch GUI error.");
				return;
			}
			if(bDoRet)
				return;
			
        	var roomEndpoint = roomEndpoints != null ? roomEndpoints[e.Node.Id] : null;
			if (e.Column == endpointProfileColumn && roomEndpoint != null && roomEndpoint.Endpoint != null)
            {
                e.RepositoryItem = _endpointProfileEditors[e.Node.Id];
            }
            else if(e.Column == bridgeNameColumn )
            {
				e.RepositoryItem = repositoryBridgeComboBox;

				//Select bridge name set for room 
				if (_bridgeNames != null && e.Node[bridgeNameColumn] == null)
				{
					string brigeNameToSelect = string.Empty;
					foreach (BridgeName brigeName in _bridgeNames)
					{
						if (roomEndpoint != null && brigeName.Id == roomEndpoint.BridgeId)
						{
							brigeNameToSelect = brigeName.Name;
							break;
						}
					}

					//If not set - get the first non-default
					if (brigeNameToSelect == string.Empty)
					{
						int i = 0;
						while (_bridgeNames[i].Name == Strings.UseDefault && i < _bridgeNames.Count)
							i++;
						if (i < _bridgeNames.Count)
							brigeNameToSelect = _bridgeNames[i].Name;
					}

					endpointsTreeList.Nodes[e.Node.Id].SetValue(bridgeNameColumn, brigeNameToSelect);
				}
            }
			else if (e.Column == selectedBrigeProfileColumn && roomEndpoint != null && roomEndpoint.Endpoint != null)
			{
				if( _bridgeProfileEditors.Count > e.Node.Id)
					e.RepositoryItem = _bridgeProfileEditors[e.Node.Id];
			}
        }

        private void endpointsTreeList_ShowingEditor(object sender, CancelEventArgs e)
        {
            var treeList = (TreeList) sender;
            if (treeList.FocusedNode == null)
            {
                return;
            }
            var roomEndpoints = ((IList<RoomEndpoint>)DataSource);
            var roomEndpoint = roomEndpoints[treeList.FocusedNode.Id];
            if (roomEndpoint.Endpoint == null)
            {
                e.Cancel = true;
                return;
            }

            e.Cancel = false;
        }

    	public bool BridgeNameColumnVisibility
    	{
    		set { bridgeNameColumn.Visible = value; }
			get { return bridgeNameColumn.Visible; }
    	}

		public bool SelectedBrigeProfileColumnVisibility
		{
			set { selectedBrigeProfileColumn.Visible = value; }
			get { return selectedBrigeProfileColumn.Visible; }
		}

		public bool CallerColumnVisibility
    	{
    		set { callerColumn.Visible = value; }
			get { return callerColumn.Visible; }
    	}

		public bool useDefaultColumnVisibility
		{
			set { useDefaultColumn.Visible = value; }
			get { return useDefaultColumn.Visible; }
		}

		public int useDefaultColumnVisibileIndex
		{
			set { useDefaultColumn.VisibleIndex = value; }
			get { return useDefaultColumn.VisibleIndex; }
		}

		public int BridgeNameColumnVisibileIndex
		{
			set { bridgeNameColumn.VisibleIndex = value; }
			get { return bridgeNameColumn.VisibleIndex; }
		}

		public int BridgeProfileNameColumnVisibileIndex
		{
			set { selectedBrigeProfileColumn.VisibleIndex = value; }
			get { return selectedBrigeProfileColumn.VisibleIndex; }
		}

		public int selectedBrigeProfileColumnVisibileIndex
		{
			set { selectedBrigeProfileColumn.VisibleIndex = value; }
			get { return selectedBrigeProfileColumn.VisibleIndex; }
		}

		public int CallerColumnVisibileIndex
		{
			set { callerColumn.VisibleIndex = value; }
			get { return callerColumn.VisibleIndex; }
		}

		void ColumnEdit_EditValueChanged(object sender, System.EventArgs e)
		{
			if (_broker != null)
				_broker.OnEvent("event_nonOutlookPropChanged", null);
		}

		public TreeListColumn getEndpointProfileColumn
		{
			get { return endpointProfileColumn; }
		}

    	public RepositoryItemComboBox _repositoryBridgeComboBox
    	{
			get { return repositoryBridgeComboBox; }
    		set { repositoryBridgeComboBox = value; }
    	}

		private void RoomAudioVideoSettingsListControl_Load(object sender, EventArgs e)
		{
			if (!DesignMode)
			{
				SuspendLayout();
				GetMCUs();
				selectedBrigeProfileColumn.OptionsColumn.ReadOnly =
					!MyVrmService.Service.OrganizationSettings.Preference.IsProfileSelectionEnabled;
				ResumeLayout();
			}
		}

		//Get MCUs
		private void GetMCUs()
		{
			Cursor cursor = Cursor.Current;
			Cursor.Current = Cursors.WaitCursor;

			try
			{
				if (_bridgeNames == null)
				{
					_bridgeNames = MyVrmService.Service != null ? MyVrmService.Service.GetBridges() : null;

					if (repositoryBridgeComboBox != null)
					{
						if (repositoryBridgeComboBox.Items != null)
						{
							repositoryBridgeComboBox.Items.Clear();
							//repositoryBridgeProfileNameComboBox.Items.Clear();
							//repositoryBridgeProfileNameComboBox.Items.Add(BridgeProfileSelectedNone.None);//"None");
							if (_bridgeNames != null)
							{
								if(_MCUWithProfiles != null)
									_MCUWithProfiles.Clear();
								
								foreach (var bridgeName in _bridgeNames)
								{
									repositoryBridgeComboBox.Items.Add(bridgeName.Name);

									var profiles = MyVrmService.Service.GetMCUProfiles(bridgeName.Id.Id);
									_MCUWithProfiles.Add(bridgeName.Id, profiles);

									//foreach (var profile in profiles)
									//{
									//    if (!repositoryBridgeProfileNameComboBox.Items.Contains(profile.Name))
									//        repositoryBridgeProfileNameComboBox.Items.Add(profile.Name);
									//}
								}
							}
						}
					}
				}
			}
			finally
			{
				Cursor.Current = cursor;
			}
		}

		void SynchronizeMCUProfile(string mcuName)
		{
			try
			{
				endpointsTreeList.BeginUpdate();
				var roomEndpoints = ((IList<RoomEndpoint>)DataSource);
				RoomEndpoint roomEndpoint = roomEndpoints[endpointsTreeList.FocusedNode.Id];
				int iNdx = endpointsTreeList.FocusedNode.Id;
				var bridgeProfileEditor = _bridgeProfileEditors[iNdx];

				endpointsTreeList.RepositoryItems.Remove(bridgeProfileEditor);
				_bridgeProfileEditors.RemoveAt(iNdx);

				RepositoryItemLookUpEdit lookUpBridgeProfileEdit = CreateBridgeProfileRepositoryItem();
				var found = _bridgeNames.FirstOrDefault(a => a.Name == (string)mcuName);
				if (found != null)
				{
					//var profiles = MCUWithProfiles.FirstOrDefault(a => a.Key == found.Id);
					SetBridgeProfileLookUpEdit(lookUpBridgeProfileEdit, found.Id);
					SuspendLayout();
					_bridgeProfileEditors.Insert(iNdx, lookUpBridgeProfileEdit);
					ResumeLayout();
					roomEndpoint.BridgeName = found.Name;
					roomEndpoint.ConferenceEndpoint.BridgeId = found.Id; //??
					//if (roomEndpoint.BridgeId != found.Id)
					//{
					//    endpointsTreeList.FocusedNode.SetValue(selectedBrigeProfileColumn, ((List<MCUProfile>)lookUpBridgeProfileEdit.DataSource)[0].Name);
					//    roomEndpoint.BridgeProfileID = ((List<MCUProfile>)lookUpBridgeProfileEdit.DataSource)[0].Id;
					//}
					//else
					{
						var profiles = MCUWithProfiles.FirstOrDefault(a => a.Key == found.Id);
						var profile = profiles.Value.FirstOrDefault(a => a.Id == roomEndpoint.BridgeProfileID);
						string setBridgeProfileName = string.Empty;
						if (profile == null)
						{
							//Try to get the default profile
							profile = profiles.Value.FirstOrDefault(a => a.Id == found.DefaultBridgeProfileId);
							if (profile == null)
								profile = profiles.Value.Count > 0 ? MCUProfile.MCUProfileNone : MCUProfile.MCUProfileNoItems;
						}
						setBridgeProfileName = profile.Name;
						//if( profile != null)
						//    setBridgeProfileName = profile.Name;
						//else
						//{
						//    if (profiles.Value.Count > 0)
						//    {
						//        setBridgeProfileName = MCUProfile.MCUProfileNone.Name;
						//        profile = MCUProfile.MCUProfileNone;
						//    }
						//    else
						//    {
						//        setBridgeProfileName = MCUProfile.MCUProfileNoItems.Name;
						//        profile = MCUProfile.MCUProfileNoItems;
						//    }
						//}
						endpointsTreeList.FocusedNode.SetValue(selectedBrigeProfileColumn, setBridgeProfileName);
						foreach (TreeListNode node in endpointsTreeList.Nodes)
						{
							if ((string)node.GetValue(bridgeNameColumn) == mcuName)
							{
								node.SetValue(selectedBrigeProfileColumn, /*currEndP.BridgeProfileName*/setBridgeProfileName);
								roomEndpoints[node.Id].BridgeProfileID = roomEndpoint.BridgeProfileID;
							}
						}
						ExternalRoomsControl._delegateExternalSetMCUProfile(found.Id, profile.Id, profile.Name);
					}
				}
			}
			catch (Exception ex)
			{
				MyVrmAddin.TraceSource.TraceInformation("SynchronizeMCUProfile() : " + ex.Message);
			}
			finally
			{
				endpointsTreeList.EndUpdate();
			}
		}

		public delegate void delegateExternalSetMCUProfile(BridgeId mcuId, string mcuName, string newMCUProfileName);

    	public static delegateExternalSetMCUProfile _delegateExternalSetMCUProfile;

		public void ExternalSetMCUProfile(BridgeId mcuId, string mcuName, string newMCUProfileName)
		{
			IList<RoomEndpoint> roomEndpoints = ((IList<RoomEndpoint>)DataSource);
			var foundMCU = MCUWithProfiles.FirstOrDefault(a => a.Key == mcuId);
			var mcuProfile = foundMCU.Value.FirstOrDefault(b => b.Name == newMCUProfileName);
			MCUProfileId mcuProfileId = MCUProfile.MCUProfileNone.Id;
			if (mcuProfile != null)
			{
				mcuProfileId = mcuProfile.Id;
			}
			else
			{
				if (newMCUProfileName == MCUProfile.MCUProfileNone.Name)
				{
					mcuProfileId = MCUProfile.MCUProfileNone.Id;
				}
				else
				{
					if (newMCUProfileName == MCUProfile.MCUProfileNoItems.Name)
					{
						mcuProfileId = MCUProfile.MCUProfileNoItems.Id;
					}

				}
			}

			foreach (TreeListNode node in endpointsTreeList.Nodes)
			{
				if ((string)node.GetValue(bridgeNameColumn) == mcuName)
				{
					node.SetValue(selectedBrigeProfileColumn, newMCUProfileName);
					roomEndpoints[node.Id].BridgeProfileID = mcuProfileId;
				}
			}
		}

    	private void endpointsTreeList_CellValueChanging(object sender, CellValueChangedEventArgs e)
    	{
    		bool doRet = false;
			EndpointProfileId defaultEndpointProfileId = EndpointProfileId.Default; //("-1");
			
			EndpointProfileId currEndpointProfile = (EndpointProfileId)((TreeList)(sender)).FocusedNode.GetValue(endpointProfileColumn);
    		
			//Synchronize comboboxes if "default" changed to non-"default" 
			if (doRet == false && e.Column == bridgeNameColumn )
			{
				doRet = true;
				var roomEndpoints = ((IList<RoomEndpoint>)DataSource);
				RoomEndpoint roomEndpoint = roomEndpoints[((TreeList)(sender)).FocusedNode.Id];
				endpointsTreeList.FocusedNode.SetValue(useDefaultColumn, false);

				SynchronizeMCUProfile((string)e.Value);
				/**
				var newval = e.Value;
				var cur = e.Node.GetValue(bridgeNameColumn);
				if( newval != cur)
				{
					int iNdx = endpointsTreeList.FocusedNode.Id;
					var bridgeProfileEditor = _bridgeProfileEditors[iNdx];

					endpointsTreeList.RepositoryItems.Remove(bridgeProfileEditor);
					_bridgeProfileEditors.RemoveAt(iNdx);

					RepositoryItemLookUpEdit lookUpBridgeProfileEdit = CreateBridgeProfileRepositoryItem();
					var found = _bridgeNames.FirstOrDefault(a => a.Name == (string) newval);
					if(found != null)
					{
						//var profiles = MCUWithProfiles.FirstOrDefault(a => a.Key == found.Id);
						SetBridgeProfileLookUpEdit(lookUpBridgeProfileEdit, found.Id);
						SuspendLayout();
						_bridgeProfileEditors.Insert(iNdx, lookUpBridgeProfileEdit);
						ResumeLayout();
						roomEndpoint.BridgeName = found.Name;
						if(roomEndpoint.BridgeId != found.Id)
						{
							e.Node.SetValue(selectedBrigeProfileColumn, ((List<MCUProfile>) lookUpBridgeProfileEdit.DataSource)[0].Name);
						}
						else
						{
							var profiles = MCUWithProfiles.FirstOrDefault(a => a.Key == found.Id);
							string setBridgeProfileName = profiles.Value.FirstOrDefault(a => a.Id == roomEndpoint.BridgeProfileID).Name;
							e.Node.SetValue(selectedBrigeProfileColumn, setBridgeProfileName);
						}
					}
				}
				**/
				//RepositoryItemLookUpEdit lookUpBridgeProfileEdit = CreateBridgeProfileRepositoryItem();
				//SetBridgeProfileLookUpEdit(lookUpBridgeProfileEdit, roomEndpoint);

				//endpointsTreeList.RepositoryItems.Remove(_bridgeProfileEditors[endpointsTreeList.FocusedNode.Id]);
				//_bridgeProfileEditors.RemoveAt(endpointsTreeList.FocusedNode.Id);
				//_bridgeProfileEditors.Insert(endpointsTreeList.FocusedNode.Id, lookUpBridgeProfileEdit);

				//var found = roomEndpoint.Endpoint.Profiles.FirstOrDefault(a => a.Id == roomEndpoint.ProfileId);
				//endpointsTreeList.Nodes[endpointsTreeList.FocusedNode.Id].SetValue(selectedBrigeProfileColumn, found != null ? found.BridgeProfileName : WinForms.Strings.TxtNoItems);

				/**
				if (roomEndpoint != null)
				{
					BridgeName bridgeToSelect = _bridgeNames.FirstOrDefault(br => br.Name == (string) e.Value);
					if (bridgeToSelect != null)
					{
						roomEndpoint.BridgeName = bridgeToSelect.Name;
						if (roomEndpoint.ConferenceEndpoint != null)
							roomEndpoint.ConferenceEndpoint.BridgeId = bridgeToSelect.Id;

						var found = MCUWithProfiles.FirstOrDefault(a => a.Key == bridgeToSelect.Id);
						//if(found != null)
						{
							//repositoryBridgeProfileNameComboBox.Items.Clear();
							//foreach (var profile in found.Value)
							//{
							//    if (!repositoryBridgeProfileNameComboBox.Items.Contains(profile.Name))
							//        repositoryBridgeProfileNameComboBox.Items.Add(profile.Name);
							//}

							string displayBridgeProfileName = string.Empty;
							foreach( var profile in roomEndpoint.Endpoint.Profiles)
							{
								if (profile.Id == currEndpointProfile)
								{
									displayBridgeProfileName = profile.BridgeProfileName;
									break;
								}
							}
							roomEndpoint.BridgeProfileName = found.Value.Count > 0
																? displayBridgeProfileName
																: BridgeProfileSelectedNone.NoItems.ToString();//"None";
						}
					}
				}
				**/
			}
			if (doRet == false && e.Column == endpointProfileColumn && (EndpointProfileId)e.Value != defaultEndpointProfileId )
			{
				doRet = true;
				IList<RoomEndpoint> roomEndpoints = ((IList<RoomEndpoint>)DataSource);
				RoomEndpoint currEndP = roomEndpoints[((TreeList)(sender)).FocusedNode.Id];
				endpointsTreeList.FocusedNode.SetValue(useDefaultColumn, false);
				EndpointProfile currProfile = currEndP.Endpoint.Profiles.FirstOrDefault(endp => endp.Id == (EndpointProfileId)e.Value);
				if(currProfile != null)
				{
					BridgeName bridgeToSelect = _bridgeNames.FirstOrDefault(bridge => bridge.Id == currProfile.BridgeId);
					if (bridgeToSelect != null)
					{
						((TreeList)(sender)).FocusedNode.SetValue(bridgeNameColumn, bridgeToSelect.Name);
						currEndP.BridgeName = bridgeToSelect.Name;
						if (currEndP.ConferenceEndpoint != null)
							currEndP.ConferenceEndpoint.BridgeId = bridgeToSelect.Id;
					}
					currEndP.ProfileId = currProfile.Id;
					SynchronizeMCUProfile(currEndP.BridgeName);
				}
				/*
				RoomEndpoint roomEndpoint = roomEndpoints[((TreeList)(sender)).FocusedNode.Id];
				if (roomEndpoint != null && _bridgeNames != null)
				{
					string brigeNameToSelect = string.Empty;
					foreach( BridgeName brigeName in _bridgeNames )
					{
						if( brigeName.Id == roomEndpoint.BridgeId)
						{
							brigeNameToSelect = brigeName.Name;
							break;
						}
					}
					if (brigeNameToSelect == string.Empty)
					{
						int i = 0;
						while (_bridgeNames[i].Name == Strings.UseDefault && i < _bridgeNames.Count)
							i++;
						if( i < _bridgeNames.Count)
							brigeNameToSelect = _bridgeNames[i].Name;
					}

					((TreeList)(sender)).FocusedNode.SetValue(bridgeNameColumn, brigeNameToSelect);
					endpointsTreeList.FocusedNode.SetValue(useDefaultColumn, false);
				}
				 */
			}
			if (doRet == false && e.Column == selectedBrigeProfileColumn )
			{
				doRet = true;
				IList<RoomEndpoint> roomEndpoints = ((IList<RoomEndpoint>)DataSource);
				RoomEndpoint currEndP = roomEndpoints[((TreeList)(sender)).FocusedNode.Id];
				//currEndP.BridgeProfileName = (string)e.Value;
				string newBridgeProfileName = (string)e.Value;
				var mcuId = currEndP.BridgeId;
				var found = MCUWithProfiles.FirstOrDefault(a => a.Key == mcuId);
				var bridgeProfile = found.Value.FirstOrDefault(b => b.Name == newBridgeProfileName);
					//currEndP.BridgeProfileName);
				MCUProfileId bridgeProfileId = MCUProfile.MCUProfileNone.Id; 
				if(bridgeProfile != null)
				{
					bridgeProfileId = bridgeProfile.Id;
				}
				else
				{
					if (/*currEndP.BridgeProfileName*/newBridgeProfileName == MCUProfile.MCUProfileNone.Name)
					{
						bridgeProfileId = MCUProfile.MCUProfileNone.Id;
					}
					else
					{
						if (/*currEndP.BridgeProfileName*/newBridgeProfileName == MCUProfile.MCUProfileNoItems.Name)
						{
							bridgeProfileId = MCUProfile.MCUProfileNoItems.Id;
						}

					}
				}
			
				currEndP.BridgeProfileID = bridgeProfileId;
				var mcuName = currEndP.BridgeName;
				foreach (TreeListNode node in endpointsTreeList.Nodes)
				{
					if ((string)node.GetValue(bridgeNameColumn) == mcuName)
					{
						node.SetValue(selectedBrigeProfileColumn, /*currEndP.BridgeProfileName*/newBridgeProfileName);
						roomEndpoints[node.Id].BridgeProfileID = bridgeProfileId;
					}
				}
				ExternalRoomsControl._delegateExternalSetMCUProfile(mcuId, bridgeProfileId, newBridgeProfileName);
				//endpointsTreeList.FocusedNode.SetValue(useDefaultColumn, false);
				//EndpointProfile currProfile = currEndP.Endpoint.Profiles.FirstOrDefault(endp => endp.Id == (EndpointProfileId)e.Value);
			}
    	
			if (doRet == false && e.Column == callerColumn )
    		{
				doRet = true;
				ConferenceEndpointCallMode currVal = (ConferenceEndpointCallMode)endpointsTreeList.FocusedNode.GetValue(callerColumn);
				if (currVal != (ConferenceEndpointCallMode)e.Value)
				{
					int otherNdx = 1 - endpointsTreeList.FocusedNode.Id;
					if (otherNdx < endpointsTreeList.Nodes.Count)
					{
						endpointsTreeList.Nodes[otherNdx].SetValue(callerColumn, currVal);
						endpointsTreeList.FocusedNode.SetValue(callerColumn, e.Value);
					}
				}
    		}

			if (doRet == false && e.Column == useDefaultColumn )
			{
				doRet = true;
				IList<RoomEndpoint> roomEndpoints = ((IList<RoomEndpoint>)DataSource);
				RoomEndpoint currRoomEndpoint = roomEndpoints[((TreeList)(sender)).FocusedNode.Id];
				bool bSet = e.Value is bool ? (bool) e.Value : false;
				currRoomEndpoint.UseDefault = bSet;
				if(bSet)
				{
					EndpointProfile currProfile = currRoomEndpoint.Endpoint.Profiles.FirstOrDefault(endp => endp.Id == currRoomEndpoint.Endpoint.Profiles.DefaultProfile.Id);
					((TreeList)(sender)).FocusedNode.SetValue(endpointProfileColumn, currRoomEndpoint.Endpoint.Profiles.DefaultProfile.Id);
					BridgeName bridgeToSelect = _bridgeNames.FirstOrDefault(bridge => bridge.Id == currProfile.BridgeId);
					if (bridgeToSelect != null)
					{
						((TreeList)(sender)).FocusedNode.SetValue(bridgeNameColumn, bridgeToSelect.Name);
						currRoomEndpoint.BridgeName = bridgeToSelect.Name;
						if (currRoomEndpoint.ConferenceEndpoint != null)
							currRoomEndpoint.ConferenceEndpoint.BridgeId = bridgeToSelect.Id;
						SynchronizeMCUProfile(bridgeToSelect.Name);
					}
				}
			}

    		_isDirty = ((DataList<RoomEndpoint>) DataSource).RaiseListChangedEvents;
			if(_isDirty && _broker != null)
				_broker.OnEvent("event_nonOutlookPropChanged", null);
    	}

		public void CloseEditor()
		{
			endpointsTreeList.PostEditor();
		}
    }
}
