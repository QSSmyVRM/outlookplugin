﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Utils.Menu;
using DevExpress.XtraScheduler;
using DevExpress.XtraScheduler.Drawing;
using MyVrm.Common.ComponentModel;
using MyVrm.WebServices.Data;
using Exception = System.Exception;
using Image=System.Drawing.Image;

namespace MyVrm.Outlook.WinForms.Conference
{
    public partial class ResourcesScheduleControl : ConferenceAwareControl
    {
        private readonly DataList<ConferenceResource> _conferenceRooms = new DataList<ConferenceResource>();
        private readonly DataList<ConferenceResource> _conferenceUsers = new DataList<ConferenceResource>();
        private ReadOnlyCollection<OccurrenceInfo> _occurrencesForVisibleInterval;
        private readonly ConferenceFakeResource _fakeUserResource;
        private readonly ConferenceFakeResource _fakeRoomResource;
        private Resource _selectedResource;
        private Point _usersSchedulerMouseLocation;
        private Point _roomsSchedulerMouseLocation;
        TimeInterval _userLastFetchedInterval = new TimeInterval();
        TimeInterval _roomLastFetchedInterval = new TimeInterval();

        private struct RoomFreeBusyUpdaterParameters
        {
            public RoomId[] RoomIds;
            public DateTime Start;
            public DateTime End;
        }

        private struct UserFreeBusyUpdaterParameters
        {
            public OutlookRecipient[] Recipients;
            public DateTime Start;
            public DateTime End;
        }

		public delegate void UpdateSatusText(string text);
		private readonly UpdateSatusText _updateSatusText;

        public ResourcesScheduleControl()
        {
            InitializeComponent();
			resourcesBar.Text = Strings.ResourcesLableText;
        	layoutControlGroup7.Text = Strings.ConferenceParticipantsRoomsLableText;
			layoutControlGroup7.CustomizationFormText = Strings.ConferenceParticipantsRoomsLableText;
			layoutControlItem7.Text = Strings.AttendeesLableText + ":";
			layoutControlItem7.CustomizationFormText = Strings.AttendeesLableText + ":";
			selectRoomsBarButtonItem.Caption = Strings.SelectRoomsMenuItemText;
			selectRoomsBarButtonItem.Hint = Strings.SelectRoomsHintText;
			removeResourceBarButtonItem.Caption = Strings.RemoveMenuItemText;
        	removeResourceBarButtonItem.Hint = Strings.RemoveHintText;
			moveRoomUpBarButtonItem.Caption = Strings.MoveRoomUpMenuItemText;
			moveRoomUpBarButtonItem.Hint = Strings.MoveRoomUpHintText;
        	moveRoomDownBarButtonItem.Caption = Strings.MoveRoomDownMenuItemText;
			moveRoomDownBarButtonItem.Hint = Strings.MoveRoomDownHintText;

            removeResourceBarButtonItem.Enabled = false;
            //moveRoomUpBarButtonItem.Enabled = false;
            //moveRoomDownBarButtonItem.Enabled = false;

            // General settings

            usersSchedulerStorage.Resources.DataSource = _conferenceUsers;
            usersSchedulerStorage.Resources.Mappings.Id = "Id";
            usersSchedulerStorage.Resources.Mappings.Caption = "Caption";
            usersSchedulerStorage.Resources.Mappings.Image = "Image";
            // Add fake resource
            _fakeUserResource = new ConferenceFakeResource();
            _conferenceUsers.Add(_fakeUserResource);
            _conferenceUsers.ListChanged += ConferenceUsersListChanged;

            roomsSchedulerStorage.Resources.DataSource = _conferenceRooms;
            roomsSchedulerStorage.Resources.Mappings.Id = "Id";
            roomsSchedulerStorage.Resources.Mappings.Caption = "Caption";
            roomsSchedulerStorage.Resources.Mappings.Image = "Image";
            // Add fake resource
            _fakeRoomResource = new ConferenceFakeResource();
            _conferenceRooms.Add(_fakeRoomResource);
            
            _conferenceRooms.AllowSorting = false;
            _conferenceRooms.ListChanged += ConferenceRoomsListChanged;
            usersSchedulerControl.TimelineView.WorkTime = TimeOfDayInterval.Day;
			_updateSatusText = new UpdateSatusText(DlgUpdateStatusText);
        }

		private void DlgUpdateStatusText(string text)
		{
            if (!string.IsNullOrEmpty(text))
            {
                labelControl1.Text = text;
                labelControl1.BackColor = SystemColors.Info;
            }
            else if (!roomFreeBusyUpdater.IsBusy && !userFreeBusyUpdater.IsBusy)
            {
                labelControl1.Text = text;
                labelControl1.BackColor = SystemColors.Info;
            }
		}

        protected override void OnReadOnlyChanged(ReadOnlyChangedEventArgs e)
        {
            standaloneBarDockControl2.Enabled = !e.ReadOnly;
        }

        private void ConferenceRoomsListChanged(object sender, ListChangedEventArgs e)
        {
            // Update meeting location
            var sb = new StringBuilder();
            for (var i = 0; i < _conferenceRooms.Count; i++)
            {
                var conferenceResource = _conferenceRooms[i];
                // Ignore fake resource
                if (!(conferenceResource is ConferenceFakeResource))
                {
                    sb.Append(conferenceResource.Caption);
                    if (i < _conferenceRooms.Count - 1)
                        sb.Append(";");
                }
            }
            Conference.Appointment.Location = sb.ToString();
            if (_conferenceRooms.Count == 0)
            {
                // Add fake room
                _conferenceRooms.Add(_fakeRoomResource);
            }
            else if (_conferenceRooms.Count > 1 && _conferenceRooms[0] == _fakeRoomResource)
            {
                // Remove fake room
                _conferenceRooms.Remove(_fakeRoomResource);
            }
        }

        private void ConferenceUsersListChanged(object sender, ListChangedEventArgs e)
        {
            if (_conferenceUsers.Count == 0)
            {
                // Add fake user
                _conferenceUsers.Add(_fakeUserResource);
            }
            else if (_conferenceUsers.Count > 1 && _conferenceUsers[0] == _fakeUserResource)
            {
                // Remove fake user
                _conferenceUsers.Remove(_fakeUserResource);
            }
        }
        
        protected override void OnConferenceBindingSourceDataSourceChanged(EventArgs e)
        {
            if (Conference != null)
            {
                Conference.PropertyChanged += ConferencePropertyChanged;
                Conference.LocationIds.ListChanged += LocationIdsListChanged;
                Conference.Participants.ListChanged += ParticipantsListChanged;
                ResetConferenceRooms();
            }
            UpdateControls();
        }

		public bool IsConfTypeMatchRoomTypes(ConferenceType conferenceType)
		{
			bool bRet = true;

			List<MediaType> validRoomTypes = new List<MediaType>();
			validRoomTypes.Add(MediaType.NoAudioVideo);
			validRoomTypes.Add(MediaType.AudioOnly);
			validRoomTypes.Add(MediaType.AudioVideo);

			switch (conferenceType)
			{
				case ConferenceType.RoomConference:
					break;
				case ConferenceType.PointToPoint:
				case ConferenceType.AudioOnly:
					validRoomTypes.Remove(MediaType.NoAudioVideo);
					break;
				case ConferenceType.AudioVideo:
				//case ConferenceType.VMR:
				case ConferenceType.Hotdesking:
					validRoomTypes.Remove(MediaType.NoAudioVideo);
					validRoomTypes.Remove(MediaType.AudioOnly);
					break;
			}

			List<ConferenceResource> invalidRooms = new List<ConferenceResource>();
			List<ConferenceResource> noEndPointRooms = new List<ConferenceResource>();
			foreach (var room in _conferenceRooms)
			{
				if (room == _fakeRoomResource)
					continue;

				Room aRoom = MyVrmAddin.Instance.GetRoomFromCache((RoomId) room.Id);
				if (!validRoomTypes.Contains(aRoom.Media))
				{
					invalidRooms.Add(room);
				}
				if (aRoom.Media == MediaType.AudioVideo && Conference.VMRType == VMRConferenceType.None &&
					(aRoom.EndpointId == EndpointId.Default || aRoom.EndpointId == EndpointId.Empty))
				{
					noEndPointRooms.Add(room);
				}
			}
			if (invalidRooms.Count > 0)
			{
				string msg = string.Empty;
				msg = Strings.TheRoomTypesContradictConfType_MsgTxt_Part1;
				msg += "\n\n";
				foreach (var room in invalidRooms)
				{
					msg += room.Caption + "\n";
				}
				msg += "\n";
				msg += string.Format(Strings.TheRoomTypesContradictConfType_MsgTxt_Part2, conferenceType);
				UIHelper.ShowMessage(msg, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				bRet = false;
			}

			if (bRet && conferenceType != ConferenceType.RoomConference && noEndPointRooms.Count > 0)
			{
				string msg = string.Empty;
				msg = Strings.TheRoomTypesContradictConfType_MsgTxt_Part1;
				msg += "\n\n";
				foreach (var room in noEndPointRooms)
				{
					msg += room.Caption + "\n";
				}
				msg += "\n";
				msg += string.Format(Strings.TheRoomHaveNoEndpoint_MsgTxt);
				UIHelper.ShowMessage(msg, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				bRet = false;
			}

			return bRet;
		}

        private void ConferencePropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch(e.PropertyName)
            {
                case "StartDate":
                case "EndDate":
                {
                    UpdateConferenceStartDate();
                    roomsSchedulerControl.Start = Conference.StartDate;
                    break;
                }
				case "Type":
				case "VMRType":
					IsConfTypeMatchRoomTypes(Conference.Type);
            		break;
            }
        }

        private void ParticipantsListChanged(object sender, ListChangedEventArgs e)
        {
            switch(e.ListChangedType)
            {
                case ListChangedType.Reset:
                    ResetConferenceUsers();
                    break;
                case ListChangedType.ItemAdded:
                    var participant = Conference.Participants[e.NewIndex];
                    AddConferenceUser(participant);
                    var outlookRecipient =
                        Conference.Appointment.Recipients.SingleOrDefault(
                            recipient => recipient.SmtpAddress.Equals(participant.Email));
                    FetchUserAppointmentsForVisibleInterval(new[]{outlookRecipient}, true);
                    break;
                case ListChangedType.ItemDeleted:
                    _conferenceUsers.RemoveAt(e.NewIndex);
                    break;
                case ListChangedType.ItemMoved:
                    _conferenceUsers.Move(_conferenceUsers[e.OldIndex],e.NewIndex);
                    break;
                case ListChangedType.ItemChanged:
                    break;
                case ListChangedType.PropertyDescriptorAdded:
                    break;
                case ListChangedType.PropertyDescriptorDeleted:
                    break;
                case ListChangedType.PropertyDescriptorChanged:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void ResetConferenceUsers()
        {
            _conferenceUsers.Clear();
            foreach (var participant in Conference.Participants)
            {
                AddConferenceUser(participant);
            }
            FetchUserAppointmentsForVisibleInterval();
        }

        private void AddConferenceUser(Participant participant)
        {
            var conferenceUser = new ConferenceResource
                                 {
                                     Id = participant.Email,
                                     Caption = string.Format("{0} {1}", participant.FirstName, participant.LastName),
                                     Image = Images.User.ToBitmap(),
                                     Type = ConferenceResourceType.User
                                 };
            _conferenceUsers.Add(conferenceUser);
        }

        private void ResetConferenceRooms()
        {
            _conferenceRooms.Clear();
            foreach (var roomId in Conference.LocationIds)
            {
                AddConferenceRoom(roomId);
            }
            FetchRoomAppointmentsForVisibleInterval(Conference.LocationIds.ToArray(), true);
        }

        private void AddConferenceRoom(RoomId roomId)
        {
            var room = MyVrmAddin.Instance.GetRoomFromCache(roomId);
            var conferenceRoom = new ConferenceResource
            {
                Id = room.Id,
                Caption = room.Name,
                Image =
                    (room.Media == MediaType.AudioOnly ||
                     room.Media == MediaType.AudioVideo)
                        ? Images.Video.ToBitmap()
                        : Images.Users.ToBitmap(),
                Type = ConferenceResourceType.Room

            };
            _conferenceRooms.Add(conferenceRoom);
        }

        void LocationIdsListChanged(object sender, ListChangedEventArgs e)
        {
            switch(e.ListChangedType)
            {
                case ListChangedType.Reset:
                {
                    ResetConferenceRooms();
                    break;
                }
                case ListChangedType.ItemAdded:
                {
                    AddConferenceRoom(Conference.LocationIds[e.NewIndex]);
                    break;
                }
                case ListChangedType.ItemDeleted:
                {
                    _conferenceRooms.RemoveAt(e.NewIndex);
                    break;
                }
                case ListChangedType.ItemMoved:
                {
                    var conferenceResource = _conferenceRooms[e.OldIndex];
                    _conferenceRooms.Move(conferenceResource, e.NewIndex);
                    break;
                }
                case ListChangedType.ItemChanged:
                    break;
                case ListChangedType.PropertyDescriptorAdded:
                    break;
                case ListChangedType.PropertyDescriptorDeleted:
                    break;
                case ListChangedType.PropertyDescriptorChanged:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private ReadOnlyCollection<OccurrenceInfo> OccurrencesForVisibleInterval
        {
            get
            {
                if (Conference.IsRecurring)
                {
					if (_occurrencesForVisibleInterval == null && Conference.RecurrencePattern != null)
					{
						_occurrencesForVisibleInterval = Conference.RecurrencePattern.GetOccurrenceInfoList(usersSchedulerControl.Start,
                                                                               usersSchedulerControl.Start.AddDays
                                                                                   (1));
					}

                	return _occurrencesForVisibleInterval;
                	//return _occurrencesForVisibleInterval ??
                	//       (_occurrencesForVisibleInterval = 
                	//        Conference.RecurrencePattern.GetOccurrenceInfoList(usersSchedulerControl.Start,
                	//                                                           usersSchedulerControl.Start.AddDays
                	//                                                               (1)));
                }
                return null;
            }
            set
            {
                _occurrencesForVisibleInterval = value;
            }
        }

        private void UpdateConferenceStartDate()
        {
            ConferenceBindingSource.ResetCurrentItem();
            usersSchedulerControl.Refresh();
            roomsSchedulerControl.Refresh();
        }


        void BuildResources()
        {
            usersSchedulerStorage.BeginUpdate();
            try
            {
                _conferenceUsers.Clear();
                foreach (var conferenceParticipant in Conference.Participants)
                {
                    _conferenceUsers.Add(new ConferenceResource
                                             {
                                                 Id = conferenceParticipant.Email,
                                                 Caption = string.Format("{0} {1} ({2})", new object[] {conferenceParticipant.FirstName, 
                                                                                                        conferenceParticipant.LastName, conferenceParticipant.Email}),
                                                 Type = ConferenceResourceType.User,
                                                 ParticipantInvitationMode = conferenceParticipant.InvitationMode,
                                                 Image = Images.User.ToBitmap()
                                             });
                }
            }
            finally
            {
                usersSchedulerStorage.EndUpdate();
            }
            _userLastFetchedInterval = new TimeInterval();
            FetchUserAppointmentsForVisibleInterval();
        }

        private void UpdateControls()
        {
            if (Conference != null)
            {
                UpdateConferenceStartDate();

                usersSchedulerControl.Start = Conference.StartDate;
                BuildResources();
            }
        }


        private void RemoveAppointments(AppointmentStorageBase storage, object resourceId)
        {
            var toDelete = storage.Items.Where(item => item.ResourceId.Equals(resourceId)).ToList();
            foreach (var appointment in toDelete)
            {
                storage.Remove(appointment);
            }
        }

        private void schedulerControl_CustomDrawTimeCell(object sender, CustomDrawObjectEventArgs e)
        {
            if (DesignMode)
            {
                return;
            }
            var viewInfo = e.ObjectInfo as SelectableIntervalViewInfo;
            KnownColor brushColor;
            if (Conference.IsRecurring)
            {
				if (OccurrencesForVisibleInterval != null)
				{
					foreach (var conferenceTimeInterval in
						OccurrencesForVisibleInterval.Select(occurrenceInfo => new TimeInterval(occurrenceInfo.Start, occurrenceInfo.End))
							.Where(conferenceTimeInterval => viewInfo.Interval.IntersectsWithExcludingBounds(conferenceTimeInterval)))
					{
						brushColor = KnownColor.Blue;
						using (var brush = new SolidBrush(Color.FromKnownColor(brushColor)))
						{
							e.Cache.FillRectangle(brush, e.Bounds);
							e.Handled = true;
						}
					}
				}
            }
            else
            {
                var conferenceTimeInterval = new TimeInterval(Conference.StartDate, Conference.Duration);
                if (viewInfo.Interval.IntersectsWithExcludingBounds(conferenceTimeInterval))
                {
                    brushColor = KnownColor.Blue;
                    using (var brush = new SolidBrush(Color.FromKnownColor(brushColor)))
                    {
                        e.Cache.FillRectangle(brush, e.Bounds);
                    }
                    e.Handled = true;
                }
            }
        }

        private void usersSchedulerControl_SelectionChanged(object sender, EventArgs e)
        {
            removeResourceBarButtonItem.Enabled = usersSchedulerControl.SelectedResource != null 
                                                  && !(usersSchedulerControl.SelectedResource.GetRow(usersSchedulerStorage) is ConferenceFakeResource);
            _selectedResource = usersSchedulerControl.SelectedResource;
        }

        private void removeResourceBarButtonItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var resource = _selectedResource;
            RemoveResource(resource);
        }

        private void RemoveResource(Resource resource)
        {
            if (resource != null)
            {
                var i = _conferenceRooms.Find("Id", resource.Id);
                if (i != -1)
                {
                    Conference.LocationIds.RemoveAt(i);
                    RemoveAppointments(roomsSchedulerStorage.Appointments, resource.Id);
                }
                else
                {
                    i = _conferenceUsers.Find("Id", resource.Id);
                    if (i != -1)
                    {
                        Conference.Participants.RemoveAt(i);
                        RemoveAppointments(usersSchedulerStorage.Appointments, resource.Id);
                    }
                }
            }
        }

        private void selectRoomsBarButtonItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ShowSelectRoomsDialog();
        }

        protected virtual void ShowSelectRoomsDialog()
        {
            using(var dlg = new RoomSelectionDialog())
            {
                dlg.SelectedRooms = Conference.LocationIds.ToArray();
            	dlg.ConferenceType = Conference.Type;
            	dlg.Conf = Conference;
                if (dlg.ShowDialog(this) == DialogResult.OK)
                {
                    AddRoomResources(dlg.SelectedRooms);
                }
            }
        }


        protected void AddRoomResources(IEnumerable<RoomId> roomIds)
        {
            roomsSchedulerStorage.BeginUpdate();
            try
            {
                foreach (var roomId in
                    roomIds.Where(roomId => Conference.LocationIds.FirstOrDefault(id => roomId == id) == null))
                {
                    Conference.LocationIds.Add(roomId);
                }
                var ids = Conference.LocationIds.ToArray();
                foreach (var roomId in ids)
                {
                    if (!roomIds.Contains(roomId))
                    {
                        Conference.LocationIds.Remove(roomId);
                    }
                }
            }
            finally
            {
                roomsSchedulerStorage.EndUpdate();
            }
            UpdateConferenceType();

            FetchRoomAppointmentsForVisibleInterval(roomIds.ToArray(), true);
        }

        private void UpdateConferenceType()
        {
			if (!Conference.IsNew || Conference.Type == ConferenceType.Hotdesking)
                return;
            bool hasNoAudioVideo = false, hasAudioOnly = false, hasAudioVideo = false;
            foreach (var roomProfile in from conferenceResource in _conferenceRooms
                                        where conferenceResource != _fakeRoomResource
                                        select MyVrmAddin.Instance.GetRoomFromCache((RoomId) conferenceResource.Id))
            {
                switch(roomProfile.Media)
                {
                    case MediaType.NoAudioVideo:
                        hasNoAudioVideo = true;
                        break;
                    case MediaType.AudioOnly:
                        hasAudioOnly = true;
                        break;
                    case MediaType.AudioVideo:
                        hasAudioVideo = true;
                        break;
                }
            }
            if (hasNoAudioVideo)
            {
                Conference.Type = ConferenceType.RoomConference;
            }
            else if (hasAudioOnly)
            {
                Conference.Type = hasAudioVideo ? ConferenceType.AudioVideo : ConferenceType.AudioOnly;
            }
            else if (hasAudioVideo)
            {
                Conference.Type = ConferenceType.AudioVideo;
            }
        }

        private void moveRoomUpBarButtonItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (roomsSchedulerControl.SelectedResource != null)
            {
                var i = _conferenceRooms.Find("Id", roomsSchedulerControl.SelectedResource.Id);
                if (i != -1)
                {
                    if (i > 0)
                    {
                        var prevResource = _conferenceRooms[i - 1];
                        if (prevResource.Type == ConferenceResourceType.Room)
                        {
                            var selectedInterval = roomsSchedulerControl.ActiveView.SelectedInterval;
                            var selectedResource = roomsSchedulerControl.ActiveView.SelectedResource;
                            roomsSchedulerStorage.BeginUpdate();
                            var prevIndex = _conferenceRooms.IndexOf(prevResource);
                            Conference.LocationIds.Move(Conference.LocationIds[i], prevIndex);
                            roomsSchedulerStorage.EndUpdate();
                            // Restore room selection
                            roomsSchedulerControl.ActiveView.SetSelection(selectedInterval, selectedResource);

							if (Conference != null)
								Conference.Appointment.SetNonOutlookProperty(true);
                        }
                    }
                }
            }
        }

        private void moveRoomDownBarButtonItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (roomsSchedulerControl.SelectedResource != null)
            {
                var i = _conferenceRooms.Find("Id", roomsSchedulerControl.SelectedResource.Id);
                if (i != -1)
                {
                    var resource = _conferenceRooms[i];
                    if (resource.Type == ConferenceResourceType.Room)
                    {
                        if (i < _conferenceRooms.Count - 1)
                        {
                            var prevResource = _conferenceRooms[i + 1];
                            if (prevResource.Type == ConferenceResourceType.Room)
                            {
                                var selectedInterval = roomsSchedulerControl.ActiveView.SelectedInterval;
                                var selectedResource = roomsSchedulerControl.ActiveView.SelectedResource;
                                roomsSchedulerControl.BeginUpdate();
                                var prevIndex = _conferenceRooms.IndexOf(prevResource);
                                Conference.LocationIds.Move(Conference.LocationIds[i], prevIndex);
                                roomsSchedulerControl.EndUpdate();
                                // Restore room selection
                                roomsSchedulerControl.ActiveView.SetSelection(selectedInterval, selectedResource);
								if (Conference != null)
									Conference.Appointment.SetNonOutlookProperty(true);
                            }
                        }
                    }
                }
            }
        }

        private void roomsSchedulerControl_VisibleIntervalChanged(object sender, EventArgs e)
        {
            OccurrencesForVisibleInterval = null;
            usersSchedulerControl.Start = roomsSchedulerControl.Start;
            FetchRoomAppointmentsForVisibleInterval();
        }

        private void FetchRoomAppointmentsForVisibleInterval()
        {
            if (Conference != null)
            {
                FetchRoomAppointmentsForVisibleInterval(Conference.LocationIds.ToArray(), false);
            }
        }

        private void FetchRoomAppointmentsForVisibleInterval(RoomId[] roomIds, bool force)
        {
            if (force)
            {
                _roomLastFetchedInterval = new TimeInterval();
            }
            var start = roomsSchedulerControl.ActiveView.GetVisibleIntervals().Start;
            var end = roomsSchedulerControl.ActiveView.GetVisibleIntervals().End;
            if (start <= _roomLastFetchedInterval.Start || end >= _roomLastFetchedInterval.End)
            {
                var startToFetch = new DateTime(start.Year, start.Month, 1);
                var endToFetch = new DateTime(end.Year, end.Month,
                                            DateTime.DaysInMonth(end.Year, end.Month));
                FetchRoomAppointments(startToFetch, endToFetch, roomIds);
            }

        }

    	private void FetchRoomAppointments(DateTime start, DateTime end, RoomId[] roomIds)
        {
            if (roomFreeBusyUpdater.IsBusy)
            {
                return;
            }
            MyVrmAddin.TraceSource.TraceInformation("Fetching room appointments...");
            if (Conference != null)
            {
                if (Conference.LocationIds.Count > 0)
                {
                	DlgUpdateStatusText(WinForms.Strings.ProgressBarMsgTxt);
					roomFreeBusyUpdater.RunWorkerAsync(new RoomFreeBusyUpdaterParameters
					                                       {RoomIds = roomIds, Start = start, End = end});
                }
            }
        }

        private void usersSchedulerControl_VisibleIntervalChanged(object sender, EventArgs e)
        {
            roomsSchedulerControl.Start = usersSchedulerControl.Start;
            FetchUserAppointmentsForVisibleInterval();
        }

        private void FetchUserAppointmentsForVisibleInterval()
        {
            var start = usersSchedulerControl.ActiveView.GetVisibleIntervals().Start;
            var end = usersSchedulerControl.ActiveView.GetVisibleIntervals().End;
            if (start <= _userLastFetchedInterval.Start || end >= _userLastFetchedInterval.End)
            {
                var startToFetch = new DateTime(start.Year, start.Month, 1);
                var endToFetch = new DateTime(end.Year, end.Month,
                                            DateTime.DaysInMonth(end.Year, end.Month));
                FetchUserAppointments(startToFetch, endToFetch);
            }
        }

        private void FetchUserAppointmentsForVisibleInterval(OutlookRecipient[] recipients, bool force)
        {
            if (force)
            {
                _userLastFetchedInterval = new TimeInterval();
            }
            var start = usersSchedulerControl.ActiveView.GetVisibleIntervals().Start;
            var end = usersSchedulerControl.ActiveView.GetVisibleIntervals().End;
            if (start <= _userLastFetchedInterval.Start || end >= _userLastFetchedInterval.End)
            {
                var startToFetch = new DateTime(start.Year, start.Month, 1);
                var endToFetch = new DateTime(end.Year, end.Month,
                                            DateTime.DaysInMonth(end.Year, end.Month));
                FetchUserAppointments(startToFetch, endToFetch, recipients);
            }
        }


        private void FetchUserAppointments(DateTime start, DateTime end)
        {
            if (userFreeBusyUpdater.IsBusy)
            {
                return;
            }
            MyVrmAddin.TraceSource.TraceInformation("Fetching user's appointments...");
            var recipients = new List<OutlookRecipient>();
            foreach (var conferenceResource in _conferenceUsers)
            {
                if (!(conferenceResource is ConferenceFakeResource))
                {
                    var email = (string)conferenceResource.Id;
                    if (string.Compare(Conference.Appointment.CurrentUser.SmtpAddress, email) == 0)
                    {
                        if (recipients != null) //ZD 102111 start
                            {
                                if (recipients.Count == 0)
                                    recipients.Add(Conference.Appointment.CurrentUser);
                                else
                                {
                                    for (int i = 0; i < recipients.Count; i++)
                                    {
                                        if (recipients[i].SmtpAddress != Conference.Appointment.CurrentUser.SmtpAddress)
                                            recipients.Add(Conference.Appointment.CurrentUser);
                                    }
                                }//ZD 102111 End
                            }
                    }
                    foreach (var recipient in Conference.Appointment.Recipients)
                    {
                        email = (string)conferenceResource.Id;
                        if (string.Compare(recipient.SmtpAddress, email) == 0)
                        {
                            if (recipients != null) //ZD 102111 start
                            {
                                if (recipients.Count == 0)
                                    recipients.Add(recipient);
                                else
                                {
                                    for (int i = 0; i < recipients.Count; i++)
                                    {
                                        if (recipients[i].SmtpAddress != recipient.SmtpAddress)
                                            recipients.Add(recipient);
                                    }
                                }
                            }//ZD 102111 End
                        }
                    }
                }
            }
            FetchUserAppointments(start, end, recipients.ToArray());
        }

        private void FetchUserAppointments(DateTime start, DateTime end, OutlookRecipient[] recipients)
        {
            if (userFreeBusyUpdater.IsBusy)
            {
                return;
            }
            MyVrmAddin.TraceSource.TraceInformation("Fetching user's appointments...");
            if (recipients.Length > 0 && !usersSchedulerStorage.Appointments.IsUpdateLocked)
            {
				DlgUpdateStatusText(WinForms.Strings.ProgressBarMsgTxt);
                userFreeBusyUpdater.RunWorkerAsync(new UserFreeBusyUpdaterParameters { Recipients = recipients, Start = start, End = end });
            }
        }

        private void roomFreeBusyUpdater_DoWork(object sender, DoWorkEventArgs e)
        {
            var parameters = (RoomFreeBusyUpdaterParameters)e.Argument;
            var roomOccurrences = new Dictionary<RoomId, List<ConferenceOccurrence>>();
            foreach (var roomId in parameters.RoomIds)
            {
                var occurrences = new List<ConferenceOccurrence>();
                if (roomFreeBusyUpdater.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }
                try
                {
                    var firstDay = new DateTime(parameters.Start.Year, parameters.Start.Month, 1);
                    var firstDayOfEnd = new DateTime(parameters.End.Year, parameters.End.Month, 1);
                    occurrences.AddRange(MyVrmService.Service.GetRoomMonthlyCalendar(roomId, firstDay));
                    if(firstDayOfEnd != firstDay)
                    {
                        occurrences.AddRange(MyVrmService.Service.GetRoomMonthlyCalendar(roomId, firstDayOfEnd));
                    }
                    roomOccurrences.Add(roomId, occurrences);
                }
                catch (Exception exception)
                {
                    MyVrmAddin.TraceSource.TraceInformation("Failed to retrieve room calendar: {0}", exception.Message);
                }
            }
            _roomLastFetchedInterval = new TimeInterval(parameters.Start,
                                            parameters.End);
            e.Result = roomOccurrences;
        }

        private delegate void UpdateRoomUserFreeBusyDelegate(
            Dictionary<RoomId, List<ConferenceOccurrence>> occurrences);

		

        private void roomFreeBusyUpdater_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MyVrmAddin.TraceSource.TraceInformation("Failed to update room busy information : {0}", e.Error.Message);
            }
            else if (!e.Cancelled)
            {
                UpdateRoomUserFreeBusyDelegate updateFreeBusy = occurrences =>
                                                                    {
                                                                        MyVrmAddin.TraceSource.TraceInformation("Adding room appointments...");
                                                                        roomsSchedulerStorage.BeginUpdate();
                                                                        try
                                                                        {
                                                                            roomsSchedulerStorage.Appointments.Clear();
                                                                            var appointments = new List<Appointment>();
                                                                            foreach (var roomId in occurrences.Keys)
                                                                            {
                                                                                appointments.AddRange(
                                                                                    occurrences[roomId].Select(
                                                                                        occurrence =>
                                                                                        new Appointment(
                                                                                            AppointmentType.Normal,
                                                                                            occurrence.Date,
                                                                                            occurrence.Duration,
                                                                                            occurrence.ConferenceName)
                                                                                            {
                                                                                                ResourceId = roomId,
                                                                                                StatusId =
                                                                                                    roomsSchedulerStorage
                                                                                                    .Appointments.
                                                                                                    Statuses.
                                                                                                    GetStandardStatusId(
                                                                                                        AppointmentStatusType
                                                                                                            .Busy)
                                                                                            }));
                                                                                roomsSchedulerStorage.Appointments.
                                                                                    AddRange(appointments.ToArray());
                                                                            }
                                                                        }
                                                                        finally
                                                                        {
                                                                            roomsSchedulerStorage.EndUpdate();
                                                                            MyVrmAddin.TraceSource.TraceInformation("Adding room appointments compeleted");
                                                                        }
                                                                    };
                
                var roomOccurrences = (Dictionary<RoomId, List<ConferenceOccurrence>>)e.Result;
                if (IsHandleCreated)
                {
                    BeginInvoke(updateFreeBusy, new object[] { roomOccurrences });
                }
            }
			if (IsHandleCreated)
        		Invoke( _updateSatusText, "");
        }

        private void userFreeBusyUpdater_DoWork(object sender, DoWorkEventArgs e)
        {
            var parameters = (UserFreeBusyUpdaterParameters) e.Argument;
            var userAppointments = new Dictionary<OutlookRecipient, ReadOnlyCollection<FreeBusyInfo>>();
            MyVrmAddin.TraceSource.TraceInformation("Getting user's free/busy information...");
           foreach (var outlookRecipient in parameters.Recipients)
           {
                try
                {
                    var freeBusyInfos = outlookRecipient.GetFreeBusy(parameters.Start, parameters.End);
                    userAppointments.Add(outlookRecipient, freeBusyInfos);
                }
                catch (Exception exception)
                {
                    MyVrmAddin.TraceSource.TraceInformation("Failed to retrieve recipient busy information for {0}: {1}", outlookRecipient.Name, exception.Message);
                }
           }
            _userLastFetchedInterval = new TimeInterval(parameters.Start, parameters.End);

            e.Result = userAppointments;
            MyVrmAddin.TraceSource.TraceInformation("Getting user's free/busy information done.");
        }

        private delegate void UpdateUserFreeBusyDelegate(
            Dictionary<OutlookRecipient, ReadOnlyCollection<FreeBusyInfo>> appointments);

        private void userFreeBusyUpdater_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MyVrmAddin.TraceSource.TraceInformation("Failed to update user busy information : {0}", e.Error.Message);
            }
            else if (!e.Cancelled)
            {
                UpdateUserFreeBusyDelegate updateFreeBusy =
                    appointments =>
                        {
                            MyVrmAddin.TraceSource.TraceInformation("Adding user appointments...");
                            usersSchedulerStorage.BeginUpdate();
                            try
                            {
//                                usersSchedulerStorage.Appointments.Clear();
                                var appointmentList = new List<Appointment>();
                                foreach (var outlookRecipient in appointments.Keys)
                                {
                                    var appts = usersSchedulerStorage.Appointments.Items.Where(
                                        appt =>
                                        string.Equals((string)appt.ResourceId, outlookRecipient.SmtpAddress,
                                                      StringComparison.InvariantCultureIgnoreCase)).ToArray();
                                    foreach (var appt in appts)
                                    {
                                        appt.Delete();
                                    }
                                    var freeBusyInfos = appointments[outlookRecipient];
                                    foreach (var freeBusyInfo in freeBusyInfos)
                                    {
                                        var appointment = new Appointment(AppointmentType.Normal, freeBusyInfo.Start,
                                                                          freeBusyInfo.End,
                                                                          freeBusyInfo.Description) { ResourceId = outlookRecipient.SmtpAddress };
                                        switch (freeBusyInfo.Status)
                                        {
                                            case FreeBusyStatus.Busy:
                                                appointment.StatusId = usersSchedulerStorage.Appointments.Statuses.
                                                    GetStandardStatusId(
                                                    AppointmentStatusType.Busy);
                                                break;
                                            case FreeBusyStatus.Tentative:
                                                appointment.StatusId = usersSchedulerStorage.Appointments.Statuses.
                                                    GetStandardStatusId(
                                                    AppointmentStatusType.Tentative);
                                                break;
                                            case FreeBusyStatus.OutOfOffice:
                                                appointment.StatusId = usersSchedulerStorage.Appointments.Statuses.
                                                    GetStandardStatusId(
                                                    AppointmentStatusType.OutOfOffice);
                                                break;
                                        }
                                        appointmentList.Add(appointment);
                                    }
                                }
                                usersSchedulerStorage.Appointments.AddRange(appointmentList.ToArray());
                            }
                            finally
                            {
                                usersSchedulerStorage.EndUpdate();
                                MyVrmAddin.TraceSource.TraceInformation("Adding user appointments compeleted");
                            }
                        };
                
                var userAppointments =
                    (Dictionary<OutlookRecipient, ReadOnlyCollection<FreeBusyInfo>>) e.Result;
                if (IsHandleCreated)
                {
                    BeginInvoke(updateFreeBusy, new object[] { userAppointments });
                }
            }
			if (IsHandleCreated)
				Invoke(_updateSatusText, "");
        }

        private void roomsSchedulerControl_SelectionChanged(object sender, EventArgs e)
        {
            removeResourceBarButtonItem.Enabled = roomsSchedulerControl.SelectedResource != null
                                                  && !(roomsSchedulerControl.SelectedResource.GetRow(roomsSchedulerStorage) is ConferenceFakeResource);
            _selectedResource = roomsSchedulerControl.SelectedResource;
        }

        private void usersSchedulerControl_CustomDrawResourceHeader(object sender, CustomDrawObjectEventArgs e)
        {
            var header = (SchedulerHeader) e.ObjectInfo;
            if (header.Resource.Id == _fakeUserResource.Id)
            {
                return;
            }
            var appearanceObject = header.Appearance.HeaderCaption;
            var x = e.Bounds.X;
            var y = e.Bounds.Y;
            var height = e.Bounds.Height;
            const int dx = 2;
            const int imageWidth = 16;
            const int imageHeight = 16;
            var imageY = y + height / 2 - imageHeight / 2;
            var rect = new Rectangle(x + dx, imageY, imageWidth, imageHeight);
            var textX = x + 2 * dx + rect.Width;
            var rectangle = new RectangleF(textX, imageY, e.Bounds.Width - textX, e.Bounds.Height);
            var bmp = header.Resource.Image;
            e.Graphics.DrawImage(bmp, rect);
            e.Graphics.DrawString(header.Caption, appearanceObject.Font, appearanceObject.GetForeBrush(e.Cache), rectangle, new StringFormat(StringFormatFlags.NoWrap));
            e.Handled = true;
        }

        private void roomsSchedulerControl_CustomDrawResourceHeader(object sender, CustomDrawObjectEventArgs e)
        {
            var header = (SchedulerHeader)e.ObjectInfo;
            if (header.Resource.Id == _fakeRoomResource.Id)
            {
                return;
            }
            var appearanceObject = header.Appearance.HeaderCaption;
            var x = e.Bounds.X;
            var y = e.Bounds.Y;
            var height = e.Bounds.Height;
            const int dx = 2;
            const int imageWidth = 16;
            const int imageHeight = 16;
            var bmp = header.Resource.Image;
            var imageY = y + height / 2 - imageHeight / 2;
            var rect = new Rectangle(x + dx, imageY, imageWidth, imageHeight);
            var infoX = x + 2 * dx + rect.Width;
            var rect1 = new Rectangle(infoX, imageY, imageWidth, imageHeight);
            var textX = infoX + rect1.Width;
            var rectangle = new RectangleF(textX, imageY, e.Bounds.Width - textX, e.Bounds.Height);
            e.Graphics.DrawImage(bmp, rect);
            Image bmp1 = Properties.Resources.information;
            e.Graphics.DrawImage(bmp1, rect1);
            e.Graphics.DrawString(header.Caption, appearanceObject.Font, appearanceObject.GetForeBrush(e.Cache), rectangle, new StringFormat());
            e.Handled = true;
        }

        private void roomsSchedulerControl_Click(object sender, EventArgs e)
        {
            var eventArgs = (MouseEventArgs)e;
            var pos = new Point(eventArgs.X, eventArgs.Y);
            var viewInfo = roomsSchedulerControl.ActiveView.ViewInfo;
            var hitInfo = viewInfo.CalcHitInfo(pos, true);

            if (hitInfo.HitTest == SchedulerHitTest.ResourceHeader)
            {
                var res = hitInfo.ViewInfo.Resource;
                if (res.Id == _fakeRoomResource.Id)
                {
                    return;
                }
                var resourceHeader = hitInfo.ViewInfo as ResourceHeader;
                var x = resourceHeader.Bounds.X;
                var y = resourceHeader.Bounds.Y;
                var height = resourceHeader.Bounds.Height;
                const int dx = 2;
                const int imageWidth = 16;
                const int imageHeight = 16;
                var imageY = y + height / 2 - imageHeight / 2;
                var rect = new Rectangle(x + dx, imageY, imageWidth, imageHeight);
                var infoX = x + 2 * dx + rect.Width;
                var rect1 = new Rectangle(infoX, imageY, imageWidth, imageHeight);
                if (rect1.Contains(pos))
                {
                    var roomProfile = MyVrmAddin.Instance.GetRoomFromCache((RoomId)res.Id);
                    ShowRoomInformationDialog(roomProfile);
                }
            }
        }

        protected virtual void ShowRoomInformationDialog(Room room)
        {
            using(var dialog = new RoomInformationDialog())
            {
                dialog.RoomProfile = room;
                dialog.ShowDialog(this);
            }
        }

        private void roomsSchedulerControl_MouseMove(object sender, MouseEventArgs e)
        {
            var pos = new Point(e.X, e.Y);
            var viewInfo = roomsSchedulerControl.ActiveView.ViewInfo;
            var hitInfo = viewInfo.CalcHitInfo(pos, true);

            if (hitInfo.HitTest == SchedulerHitTest.ResourceHeader)
            {
                var res = hitInfo.ViewInfo.Resource;
                if (res.Id == _fakeRoomResource.Id)
                {
                    return;
                }
                var resourceHeader = hitInfo.ViewInfo as ResourceHeader;
                var x = resourceHeader.Bounds.X;
                var y = resourceHeader.Bounds.Y;
                var height = resourceHeader.Bounds.Height;
                const int dx = 2;
                const int imageWidth = 16;
                const int imageHeight = 16;
                var imageY = y + height / 2 - imageHeight / 2;
                var rect = new Rectangle(x + dx, imageY, imageWidth, imageHeight);
                var infoX = x + 2 * dx + rect.Width;
                var rect1 = new Rectangle(infoX, imageY, imageWidth, imageHeight);
                if (rect1.Contains(e.Location))
                {
                    hitInfo.ViewInfo.ShouldShowToolTip = true;
					hitInfo.ViewInfo.ToolTipText = Strings.InfoToolTipText;
                }
                else
                {
                    hitInfo.ViewInfo.ToolTipText = "";
                }
            }
        }

        private void usersSchedulerControl_Enter(object sender, EventArgs e)
        {
            _selectedResource = usersSchedulerControl.ActiveView.SelectedResource;
        }

        private void roomsSchedulerControl_Enter(object sender, EventArgs e)
        {
            _selectedResource = roomsSchedulerControl.ActiveView.SelectedResource;
        }

        private void usersSchedulerControl_PreparePopupMenu(object sender, PreparePopupMenuEventArgs e)
        {
            var sc = usersSchedulerControl.ActiveView.ViewInfo.CalcHitInfo(_usersSchedulerMouseLocation, true);
            if (sc.HitTest == SchedulerHitTest.ResourceHeader)
            {
                e.Menu.Items.Clear();
                if (sc.ViewInfo.Resource.Id != _fakeUserResource.Id)
                {
                    var participant = Conference.Participants.FirstOrDefault(match => match.Email == (string)sc.ViewInfo.Resource.Id);
                    if (participant != null)
                    {
                        if (Conference.IsAllowedToModify)
                        {
                            e.Menu.Items.Add(new DXMenuCheckItem("Room",
                                                                 participant.InvitationMode ==
                                                                 ParticipantInvitationMode.Room,
                                                                 null, (s, args) => participant.InvitationMode =
                                                                                    ParticipantInvitationMode.Room));
                            e.Menu.Items.Add(new DXMenuCheckItem("External",
                                                                 participant.InvitationMode ==
                                                                 ParticipantInvitationMode.External,
                                                                 null, (s, args) => participant.InvitationMode =
                                                                                    ParticipantInvitationMode.External));
                            e.Menu.Items.Add(new DXMenuCheckItem("CC only",
                                                                 participant.InvitationMode ==
                                                                 ParticipantInvitationMode.CcOnly,
                                                                 null, (s, args) => participant.InvitationMode =
                                                                                    ParticipantInvitationMode.CcOnly));
                            e.Menu.Items.Add(new DXMenuItem("Remove", (s, args) => RemoveResource(sc.ViewInfo.Resource),
                                                            Images.Remove)
                                                 {
                                                     BeginGroup = true
                                                 });
                        }
                    }
                }
            }
            else
            {
                if (e.Menu.Id == SchedulerMenuItemId.DefaultMenu)
                {
                    e.Menu.RemoveMenuItem(SchedulerMenuItemId.SwitchViewMenu);
                    e.Menu.Items.Insert(0, new DXMenuItem("Go to Conference Date",
                                                    (s, args) => usersSchedulerControl.Start = Conference.StartDate));
                }
            }
        }

        private void usersSchedulerControl_MouseDown(object sender, MouseEventArgs e)
        {
            _usersSchedulerMouseLocation = e.Location;
        }

        private void roomsSchedulerControl_MouseDown(object sender, MouseEventArgs e)
        {
            _roomsSchedulerMouseLocation = e.Location;
        }

        private void roomsSchedulerControl_PreparePopupMenu(object sender, PreparePopupMenuEventArgs e)
        {
            var sc = roomsSchedulerControl.ActiveView.ViewInfo.CalcHitInfo(_roomsSchedulerMouseLocation, true);
            if (sc.HitTest == SchedulerHitTest.ResourceHeader)
            {
                e.Menu.Items.Clear();
                if (sc.ViewInfo.Resource.Id != _fakeRoomResource.Id)
                {
                    var roomProfile = MyVrmAddin.Instance.GetRoomFromCache((RoomId)sc.ViewInfo.Resource.Id);
					e.Menu.Items.Add(new DXMenuItem(Strings.InfoMenuItemText, (s, args) => ShowRoomInformationDialog(roomProfile)));
                    if (Conference.IsAllowedToModify)
                    {
                        e.Menu.Items.Add(new DXMenuItem(Strings.RemoveMenuItemText, (s, args) => RemoveResource(sc.ViewInfo.Resource),
                                                        Images.Remove)
                                             {
                                                 BeginGroup = true
                                             });
                    }
                }
            }
            else
            {
                if (e.Menu.Id == SchedulerMenuItemId.DefaultMenu)
                {
                    e.Menu.RemoveMenuItem(SchedulerMenuItemId.SwitchViewMenu);
                    e.Menu.Items.Insert(0, new DXMenuItem("Go to Conference Date",
                                                    (s, args) => roomsSchedulerControl.Start = Conference.StartDate));
                }
            }
        }

        private void ResourcesScheduleControl_Load(object sender, EventArgs e)
        {
            if (!DesignMode)
            {
                var cursor = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                try
                {
                    var orgOptions = MyVrmService.Service.OrganizationOptions;
                    var workTime = orgOptions.AvailableTime.IsOpen24Hours
                                       ? TimeOfDayInterval.Day
									   : new TimeOfDayInterval(orgOptions.AvailableTime.StartTimeInLocalTimezone,
															   orgOptions.AvailableTime.EndTimeInLocalTimezone);

                    usersSchedulerControl.TimelineView.WorkTime = roomsSchedulerControl.TimelineView.WorkTime = workTime;
                }
                finally
                {
                    Cursor.Current = cursor;
                }
            }
        }
    }
}
