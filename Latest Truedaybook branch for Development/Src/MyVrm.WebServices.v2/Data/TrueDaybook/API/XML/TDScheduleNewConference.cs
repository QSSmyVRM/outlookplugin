﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using CallWebApi.Classes;
using MyVrm.Common.Diagnostics;

namespace MyVrm.WebServices.Data
{
	internal class TDScheduleNewConferenceRequest : ServiceRequestBase<TDScheduleNewConferenceResponse>
	{
		internal string Subject;
		internal string Body;
		internal DateTime StartDate;
		internal DateTime EndDate;
		internal List<ConferenceRequest.Participant> Participants;
		internal bool Over;
        internal RoomIdCollection rooms;
        internal bool IsStatic = false;
        internal string staticID = string.Empty;
        internal bool immediateconf=false;//ZD 101226

		internal TDScheduleNewConferenceRequest(MyVrmService service)
			: base(service)
		{
		}

		#region Overrides of ServiceRequestBase

		internal override string GetXmlElementName()
		{
			return "TDScheduleNewConference";
		}

		internal override string GetCommandName()
		{
			return Constants.TDScheduleNewConferenceCommandName;
		}

		internal override string GetResponseXmlElementName()
		{
			return "TDScheduleNewConference";
		}

		internal override TDScheduleNewConferenceResponse ParseResponse(MyVrmServiceXmlReader reader)
		{
			var response = new TDScheduleNewConferenceResponse(); 
			response.LoadFromXml(reader, GetResponseXmlElementName());
			return response;
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			UserId.WriteToXml(writer, "userID");
			writer.WriteElementValue(XmlNamespace.NotSpecified, "subject", Subject);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "invitationBody", Body);
            
            //ZD 101226 Starts
            
            if (immediateconf)
            {
                writer.WriteElementValue(XmlNamespace.NotSpecified, "immediate", "1");
                writer.WriteElementValue(XmlNamespace.NotSpecified, "startDate", "");
                writer.WriteElementValue(XmlNamespace.NotSpecified, "endDate", "");

            }
            else
            {
                writer.WriteElementValue(XmlNamespace.NotSpecified, "startDate", StartDate.ToString("s", DateTimeFormatInfo.InvariantInfo));
                writer.WriteElementValue(XmlNamespace.NotSpecified, "endDate", EndDate.ToString("s", DateTimeFormatInfo.InvariantInfo));
                
            }
            //ZD 101226 Ends
			writer.WriteElementValue(XmlNamespace.NotSpecified, "overBookConf", Over ? 1 : 0);


            if (IsStatic)
            {
                writer.WriteElementValue(XmlNamespace.NotSpecified, "EnableStaticID", "1");
                //Need to set static number for user
                writer.WriteElementValue(XmlNamespace.NotSpecified, "StaticID", staticID);

            }
            else
                writer.WriteElementValue(XmlNamespace.NotSpecified, "EnableStaticID", "0");
			//<recurrence>????</recurrence>
			writer.WriteStartElement(XmlNamespace.NotSpecified, "participantsList");
			foreach (var participant in Participants)
			{
				writer.WriteStartElement(XmlNamespace.NotSpecified, "participant");
				writer.WriteElementValue(XmlNamespace.NotSpecified, "name", participant.Name);
				writer.WriteElementValue(XmlNamespace.NotSpecified, "email", participant.Email);
				writer.WriteEndElement();
			}
			writer.WriteEndElement();

            writer.WriteStartElement(XmlNamespace.NotSpecified, "locationList");
            writer.WriteStartElement(XmlNamespace.NotSpecified, "selected");
            foreach (var room in rooms)
            {

                writer.WriteElementValue(XmlNamespace.NotSpecified, "level1ID", room.Id);
                
                
            
            }

           
                
            writer.WriteEndElement();
            writer.WriteEndElement();
		}
		#endregion
	}

	public class TDScheduleNewConferenceResponse : ServiceResponse
	{
		public string ConferenceId { get; internal set; }
		public long ExternalId { get; internal set; }
		public string ConferenceUrl{ get; internal set; }
		
		public string RetMessage { set; get; }
        public int RetCode { set; get; }
		public long SettingsVersion { set; get; }
		public XMLError XMLError { set; get; }

		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{

			while (!reader.IsEndElement(XmlNamespace.NotSpecified, "retState"))
			{
				try
				{
					switch (reader.LocalName)
					{
						case "conferenceId":
							ConferenceId = reader.ReadElementValue();
							break;
						case "externalId":
							ExternalId = reader.ReadElementValue<long>();
							break;
						case "conferenceUrl":
							ConferenceUrl = reader.ReadElementValue();
							break;
						case "retCode":
							RetCode = reader.ReadElementValue<int>();
							break;
						case "retMessage":
							RetMessage = reader.ReadElementValue();
							break;
						case "settingsVersion":
							SettingsVersion = reader.ReadElementValue<long>();
							break;
						case "error":
							XMLError = new XMLError();
							XMLError.LoadFromXml(reader, XmlNamespace.NotSpecified, "error");
							break;
					}
					reader.Read();
				}
				catch (Exception ex)
				{
					MyVrmService.TraceSource.TraceError(reader.LocalName + ": " + ex.Message, null);
					reader.Read();
					//throw;
				}
			}
		}
	}
}