﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using Microsoft.Deployment.WindowsInstaller;
using Microsoft.Win32;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace myVRM.Deployment.CustomAction
{
    public class CustomActions
    {
        [DllImport("kernel32.dll", SetLastError = true, CallingConvention = CallingConvention.Winapi)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool IsWow64Process([In] IntPtr hProcess, [Out] out bool lpSystemInfo);

        private static bool Is64Bit()
        {
            if (IntPtr.Size == 8 || (IntPtr.Size == 4 && Is32BitProcessOn64BitProcessor()))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private static bool Is32BitProcessOn64BitProcessor()
        {
            bool retVal;

            IsWow64Process(Process.GetCurrentProcess().Handle, out retVal);

            return retVal;
        }
        [CustomAction]
        public static ActionResult RegisterOfficeAddinCustomAction(Session session)
        {
            session.Log("Begin RegisterOfficeAddinCustomAction");
            var addinName = session.CustomActionData["AddinName"];
            var officeVersion = session.CustomActionData["OfficeVersion"];

            string CompanyName, ProductName, ProductStorageVersion;

            CompanyName = session.CustomActionData["Manufacturer"];
            ProductName = session.CustomActionData["ProductName"];
            ProductStorageVersion = session.CustomActionData["ProductVersion"];

            

            RegisterAddIn(addinName, officeVersion);

            RemoveAllXmlFiles(CompanyName,ProductName,ProductStorageVersion,session);
            System.Diagnostics.Process[] processes = System.Diagnostics.Process.GetProcessesByName("OUTLOOK");

            if (processes.Length > 0)
                System.Windows.Forms.MessageBox.Show(Strings.OutlookRestartMessage, Strings.OutlookCaption);
            return ActionResult.Success;
        }
        [CustomAction]
        public static ActionResult InstallPrerequisitesCustomAction(Session session)
        {
            ActionResult actResult = ActionResult.Success;

            string val = session["OriginalDatabase"];
            //to be commented for msi !Is2016Version() &&
            if ( !Is2016Version() && !Is2013Version() && !IS2010Version() && !Is2007Version()) //ALLDEV-737
            {
                //System.Windows.Forms.MessageBox.Show("into 1", Strings.myVRMInstalHeader);

                //101346 starts && !Is2016PIAExists(val)
                if (!Is2016PIAExists(val) && !Is2007PIAExists(val) && !Is2010PIAExists(val) && !Is2013PIAExists(val)) ///102887 start //ALLDEV-737
                {
                    System.Windows.Forms.MessageBox.Show(Strings.OutlookNotInstalled, Strings.myVRMInstalHeader);
                    actResult = ActionResult.UserExit;
                    return actResult;
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show(Strings.PlatformMismatch, Strings.myVRMInstalHeader);
                    actResult = ActionResult.UserExit;
                    return actResult;
                }
                //101346 Ends
            }
            if (!IsNet40Exists())
            {
                InstallNet40();
                
                if (!IsNet40Exists())
                {
                    actResult = ActionResult.UserExit;
                    System.Windows.Forms.MessageBox.Show(Strings.DotNetRequiredMessage, Strings.myVRMInstalHeader);
                    return actResult;
                }
                else
                    actResult = ActionResult.Success;

            }
            if (!VSTOExists())
            {
                InstallVSTO();
                if (!VSTOExists())
                {
                    actResult = ActionResult.UserExit;
                    System.Windows.Forms.MessageBox.Show(Strings.VSTORequiredMessage, Strings.myVRMInstalHeader);
                    return actResult;
                }
                else
                    actResult = ActionResult.Success;
            }

            if (Is2007Version() && !Is2007PIAExists(val))
            {
                Install2007PIA();
                if (!Is2007PIAExists(val))
                {
                    actResult = ActionResult.UserExit;
                    System.Windows.Forms.MessageBox.Show(Strings._2007PIARequired, Strings.myVRMInstalHeader);
                    return actResult;
                }
                else
                    actResult = ActionResult.Success;
            }

            if (IS2010Version() && !Is2010PIAExists(val))
            {

                Install2010PIA();
                if (!Is2010PIAExists(val))
                {
                    actResult = ActionResult.UserExit;
                    System.Windows.Forms.MessageBox.Show(Strings._2010PIARequired, Strings.myVRMInstalHeader);
                    return actResult;

                }
                else
                    actResult = ActionResult.Success;
            }
           
          return ActionResult.Success;
        }

        [CustomAction]
        public static ActionResult RegisterOffice2016AddinCustomAction(Session session)
        {  
            try
            {
                var addinName = session.CustomActionData["AddinName"];
                //addinName = addinName + "2016";
                if (Is2016Version())
                {

                    Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"Software\Microsoft\Office\Outlook\AddIns\", true);

                    
                    if (regKey != null)
                    {
                       
                        try
                        {
                            regKey.DeleteSubKeyTree(addinName);
                        }
                        catch (Exception exin)
                        { }
                        regKey.CreateSubKey(addinName);
                        
                        regKey.Flush();
                        Microsoft.Win32.RegistryKey subregKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"Software\Microsoft\Office\Outlook\Addins\" + addinName + @"\", true);
                        subregKey.SetValue("Description", addinName, Microsoft.Win32.RegistryValueKind.String);
                        subregKey.SetValue("FriendlyName", addinName, Microsoft.Win32.RegistryValueKind.String);
                        subregKey.SetValue("LoadBehavior", 0x00000003, Microsoft.Win32.RegistryValueKind.DWord);
                        subregKey.SetValue("Manifest", "file:///" + session.CustomActionData["installpath"] + "MyVrmAddin2007.vsto|vstolocal", Microsoft.Win32.RegistryValueKind.String);
                        subregKey.Flush();
                        regKey = null;
                        subregKey = null;
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);

            }

            return ActionResult.Success;
         
        }

        [CustomAction]
        public static ActionResult UnRegisterOffice2016AddinCustomAction(Session session)
        {
            try
            {

                if (Is2016Version())
                {

                    Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"Software\Microsoft\Office\Outlook\AddIns\", true);


                    if (regKey != null)
                    {

                        try
                        {
                            regKey.DeleteSubKeyTree("myVRM.OutlookAddIn");
                        }
                        catch (Exception exin)
                        { }
                        
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);

            }

            return ActionResult.Success;

        }

       
        private static int CheckComponent(string guid,string componentPath)
        {
            int iReturnCode = 1670;
            System.Diagnostics.Process pr = new System.Diagnostics.Process();
            pr.StartInfo = new System.Diagnostics.ProcessStartInfo();
            pr.StartInfo.Arguments = guid;
            pr.StartInfo.FileName = componentPath;
            bool bretunr = pr.Start();
            pr.WaitForExit();
            iReturnCode = pr.ExitCode;
            return iReturnCode;
        }
        //ALLDEV-737 start
        private static bool Is2016PIAExists(string val)
        {
            bool b2016PIAExists = false;
            string strCheckPath = System.IO.Path.GetDirectoryName(val);
            //System.Windows.Forms.MessageBox.Show(strCheckPath, Strings.OutlookCaption);
            if (!strCheckPath.EndsWith("\\"))
                strCheckPath += "\\";

            //to be commented for MSI starts
            strCheckPath += "\\MyVrmAddin\\ComponentCheck.exe";//MyVrmAddin for Myvrmgenric, RMTAddin for RMT plugin 

            //int iCode = CheckComponent("{9AC08E99-230B-47e8-9721-4577B7F124EA}", strCheckPath);
            //if (iCode == 0)
            //    iCode = CheckComponent("{9AC08E99-230B-47e8-9721-4577B7F124EA}", strCheckPath);
            //if (iCode == 0)
                //to be commented for MSI Ends
               // System.Windows.Forms.MessageBox.Show("2016 PIA versiom: ", b2016PIAExists.ToString());

                b2016PIAExists = true;
            
           
            return b2016PIAExists;
        }

        //ALLDEV-737 End

        private static bool Is2010PIAExists(string val)
        {
            bool b2010PIAExists = false;
            string strCheckPath = System.IO.Path.GetDirectoryName(val);
            if (!strCheckPath.EndsWith("\\"))
                strCheckPath += "\\";

            //to be commented for MSI starts
            strCheckPath += "\\MyVrmAddin\\ComponentCheck.exe";//MyVrmAddin for Myvrmgenric, RMTAddin for RMT plugin 

            int iCode = CheckComponent("{1D844339-3DAE-413E-BC13-62D6A52816B2}", strCheckPath);
            if (iCode == 0)
                iCode = CheckComponent("{64E2917E-AA13-4CA4-BFFE-EA6EDA3AFCB4}", strCheckPath);
            if (iCode == 0)
            //to be commented for MSI Ends
                 b2010PIAExists = true;
       //     System.Windows.Forms.MessageBox.Show("2007 PIA versiom: ", b2010PIAExists.ToString());

            return b2010PIAExists;
        }
        
        private static bool Is2007PIAExists(string val)
        {
            bool b2007PIAExists = false;
            
            string strCheckPath = System.IO.Path.GetDirectoryName(val);
            if (!strCheckPath.EndsWith("\\"))
                strCheckPath += "\\";
            //to be commented for MSI starts
            strCheckPath += "\\MyVrmAddin\\ComponentCheck.exe";//MyVrmAddin for Myvrmgenric, RMTAddin for RMT plugin 
            int iCode = CheckComponent("{ED569DB3-58C4-4463-971F-4AAABB6440BD}", strCheckPath);
            if (iCode == 0)
                iCode = CheckComponent("{FAB10E66-B22C-4274-8647-7CA1BA5EF30F}", strCheckPath);
            if (iCode == 0)
                //to be commented for MSI Ends
                b2007PIAExists = true;
       //     System.Windows.Forms.MessageBox.Show("2010 PIA versiom: ", b2007PIAExists.ToString());

            return b2007PIAExists;
        }

        // 102887 start
        private static bool Is2013PIAExists(string val)
        {
            bool b2013PIAExists = false;

            string strCheckPath = System.IO.Path.GetDirectoryName(val);
            if (!strCheckPath.EndsWith("\\"))
                strCheckPath += "\\";
            //to be commented for MSI starts
            strCheckPath += "\\MyVrmAddin\\ComponentCheck.exe";//MyVrmAddin for Myvrmgenric, RMTAddin for RMT plugin 
            int iCode = CheckComponent("{F9F828D5-9F0B-46F9-9E3E-9C59F3C5E136}", strCheckPath);
            if (iCode == 0)
                iCode = CheckComponent("{6A174BDB-0049-4D1C-86EF-3114CB0C4C4E}", strCheckPath);
            if (iCode == 0)
            //to be commented for MSI starts
                b2013PIAExists = true;
           // System.Windows.Forms.MessageBox.Show("2013 PIA versiom: ", b2013PIAExists.ToString());

            return b2013PIAExists;

        }
        // 102887 End
        //ALLDEV-737 start
        private static bool Is2016Version()
        {

            bool bis2016 = false;
            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Office\16.0\Outlook");
            regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Office\16.0\Outlook");
            //Strings.OutlookCaption

            if (regKey != null)
            {
                string strPath = regKey.GetValue("Path") as string;
                if (strPath != null && strPath.Length > 0)
                {
                    bis2016 = true;
                }
                else
                {
                    strPath ="C:/Program Files (x86)/Microsoft Office/Office16";
                    bis2016 = true;
                }
              
                regKey.Close();
            }
          
            return bis2016;

        }
        //ALLDEV-737 end
        private static bool Is2013Version()
        {
            bool bis2013 = false;
            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Office\15.0\Outlook\InstallRoot");
            regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Office\15.0\Outlook\InstallRoot");
            if (regKey != null)
            {
                string strPath = regKey.GetValue("Path") as string;

                if (strPath != null && strPath.Length > 0)
                    bis2013 = true;
                regKey.Close();
            }
         //   System.Windows.Forms.MessageBox.Show("2013 versiom: ", bis2013.ToString());

            return bis2013;

        }
       
        private static bool IS2010Version()
        {
            bool bis2010 = false;
            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Office\14.0\Outlook\InstallRoot");
            if (regKey != null)
            {
                string strPath = regKey.GetValue("Path") as string;

                if (strPath != null && strPath.Length > 0)
                    bis2010 = true;
                regKey.Close();
            }

       //     System.Windows.Forms.MessageBox.Show("2010 versiom: ", bis2010.ToString());

            

            return bis2010;
        }

        private static bool Is2007Version()
        {
            bool bis2007 = false;
            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Office\12.0\Outlook\InstallRoot");
            if (regKey != null)
            {
                string strPath = regKey.GetValue("Path") as string;

                if (strPath != null && strPath.Length > 0)
                    bis2007 = true;
            }

          //  System.Windows.Forms.MessageBox.Show("2007 versiom: ", bis2007.ToString());

            return bis2007;
        }

        private static void Install2010PIA()
        {

            //http://download.microsoft.com/download/C/1/D/C1D6DBBB-700D-4669-98CF-820AC3AE8E55/PIARedist.exe 
            try
            {
               
                    string vstoURL = "http://go.microsoft.com/fwlink/?linkid=187780";
                    string strTempPath = System.IO.Path.GetTempPath();
                    if (!strTempPath.EndsWith("\\"))
                        strTempPath += "\\";
                    strTempPath += "Office2010PIA.exe";
                    System.Net.WebClient webClient = new System.Net.WebClient();
                    webClient.DownloadFile(new Uri(vstoURL), strTempPath);
                    System.Diagnostics.Process pro = new System.Diagnostics.Process();
                    pro.StartInfo = new System.Diagnostics.ProcessStartInfo();
                    pro.StartInfo.FileName = strTempPath;
                    bool bproStarted = pro.Start();
                    pro.WaitForExit();
               
            }
            catch
            {

            }

        }

        private static void Install2007PIA()
        {


            try
            {
                string vstoURL = "http://download.microsoft.com/download/e/1/d/e1df4622-5f6c-4fb9-845b-38d009cc1188/PrimaryInteropAssembly.exe";
                string strTempPath = System.IO.Path.GetTempPath();
                if (!strTempPath.EndsWith("\\"))
                    strTempPath += "\\";
                strTempPath += "Office2007PIA";
                if (!System.IO.Directory.Exists(strTempPath))
                    System.IO.Directory.CreateDirectory(strTempPath);
                string PrimaryInteropExePath = strTempPath;
                PrimaryInteropExePath += "\\PrimaryInteropAssembly.exe";

                System.Net.WebClient webClient = new System.Net.WebClient();
                webClient.DownloadFile(new Uri(vstoURL), PrimaryInteropExePath);

                System.Diagnostics.Process pro = new System.Diagnostics.Process();
                pro.StartInfo = new System.Diagnostics.ProcessStartInfo();

                pro.StartInfo.FileName = PrimaryInteropExePath;
                pro.StartInfo.Arguments = "/q /extract:";
                pro.StartInfo.Arguments += "\"";
                pro.StartInfo.Arguments += strTempPath;
                pro.StartInfo.Arguments += "\"";


                bool bproStarted = pro.Start();
                pro.WaitForExit();


                System.Diagnostics.Process process1 = new System.Diagnostics.Process();
                process1.StartInfo = new System.Diagnostics.ProcessStartInfo();
                process1.StartInfo.FileName = strTempPath + "\\o2007pia.msi";

                process1.Start();
                process1.WaitForExit();
            }
            catch
            {   
            }



        }

        private static void InstallVSTO()
        {
            try
            {
                string vstoURL = "http://go.microsoft.com/fwlink/?LinkId=158917";
                string strTempPath = System.IO.Path.GetTempPath();
                if (!strTempPath.EndsWith("\\"))
                    strTempPath += "\\";
                strTempPath += "vstor.exe";
                System.Net.WebClient webClient = new System.Net.WebClient();
                webClient.DownloadFile(new Uri(vstoURL), strTempPath);
                System.Diagnostics.Process pro = new System.Diagnostics.Process();
                pro.StartInfo = new System.Diagnostics.ProcessStartInfo();
                pro.StartInfo.FileName = strTempPath;
                pro.StartInfo.Arguments = "/norestart";
                bool bproStarted = pro.Start();
                pro.WaitForExit();
            }
            catch
            {
            }



        }

        private static bool VSTOExists()
        {
            bool bVstoExists = false;


            Microsoft.Win32.RegistryKey regKey = null;
            if(Is64Bit())
                regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Microsoft\VSTO Runtime Setup\v4R");
            else
                regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\VSTO Runtime Setup\v4R");

            if (regKey != null)
            {
                bVstoExists = true;
                regKey.Close();
            }

            return bVstoExists;
        }

        private static void InstallNet40()
        {
            try
            {
                string netURL = "http://download.microsoft.com/download/1/B/E/1BE39E79-7E39-46A3-96FF-047F95396215/dotNetFx40_Full_setup.exe";
                string strTempPath = System.IO.Path.GetTempPath();
                if (!strTempPath.EndsWith("\\"))
                    strTempPath += "\\";
                strTempPath += "dotnet40.exe";
                System.Net.WebClient webClient = new System.Net.WebClient();
                webClient.DownloadFile(new Uri(netURL), strTempPath);
                System.Diagnostics.Process pro = new System.Diagnostics.Process();
                pro.StartInfo = new System.Diagnostics.ProcessStartInfo();
                pro.StartInfo.FileName = strTempPath;
                bool bproStarted = pro.Start();
                pro.WaitForExit();
            }
            catch
            {
            }



        }

        private static bool IsNet40Exists()
        {
            bool bNet40exists = false;

            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\NET Framework Setup\NDP\Full");

            if (regKey != null)
            {
                object objVal = regKey.GetValue("Version");
                object objRelVal = regKey.GetValue("Release");
                if (objVal != null || objRelVal != null)
                {
                    bNet40exists = true;
                    regKey.Close();
                    return bNet40exists;
                }
                else
                    regKey.Close();


            }
            regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full");
            if (regKey != null)
            {
                object objVal = regKey.GetValue("Version");
                object objRelVal = regKey.GetValue("Release");
                if (objVal != null || objRelVal != null)
                {
                    bNet40exists = true;
                    regKey.Close();
                    return bNet40exists;
                }
                else
                    regKey.Close();

            }
            return bNet40exists;
        }
        
        private static readonly object SyncObject = new object();
        
        private static string GetDataPath(string basePath,string CompanyName,string ProductName,string ProductStorageVersion)
        {
            string path = string.Format(@"{0}\{1}\{2}\{3}", new object[] { basePath, CompanyName, ProductName, ProductStorageVersion });
            lock (SyncObject)
            {
                if (!System.IO.Directory.Exists(path))
                {
                    System.IO.Directory.CreateDirectory(path);
                }
            }
            return path;
        }

        private static void RemoveAllXmlFiles(string CompanyName, string ProductName, string ProductStorageVersion, Session session)
        {

            
            string strDataPath = GetDataPath(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),CompanyName,ProductName,ProductStorageVersion);
            session.Log("DataPath is " + strDataPath);
            string[] xmlFiles = System.IO.Directory.GetFiles(strDataPath, "*.xml");
            foreach (string xmlFile in xmlFiles)
            {
                try
                {
                    session.Log("FilePath is " + xmlFile);
                    System.IO.File.Delete(xmlFile);
                }
                catch
                {
                }
            }


           
        }

        [CustomAction]
        public static ActionResult UnRegisterOfficeAddinCustomAction(Session session)
        {
            session.Log("Begin UnRegisterOfficeAddinCustomAction");
            var addinName = session.CustomActionData["AddinName"];
            var applicationName = session.CustomActionData["OfficeApplication"];
            var officeVersion = session.CustomActionData["OfficeVersion"];
            session.Log("{0}; {1}; {2}", addinName, applicationName, officeVersion);
            UnRegisterAddIn(applicationName, addinName, officeVersion);
            return ActionResult.Success;
        }

        [CustomAction]
        public static ActionResult RemoveVideoMeetingAddinCustomAction(Session session)
        {
            // since Video Meeting add-in originally installed in "per user" mode the setup cannot uninstall it if running in "per machine" mode
            // So uninstall manually here
            session.Log("Begin UninstallProductCustomAction");
            var upgradeCodes = new string[]
                               {
                                   "{DFD6E955-3A2D-406A-AC6E-E7B4F83D23FB}", // Video Meeting Addin for Outlook 2007
                                   "{515C50A3-BFA9-47EC-8680-5FB26E64DE68}", // Video Meeting Addin for Outlook 2010 32-bit
                                   "{6F459644-24E7-4A62-92AF-676D435F7C6C}"  // Video Meeting Addin for Outlook 2010 64-bit
                               };
            foreach (var upgradeCode in upgradeCodes)
            {

                session.Log("Get related products for UpgradeCode: {0}", upgradeCode);
                var products = ProductInstallation.GetRelatedProducts(upgradeCode);
                foreach (var productInstallation in products)
                {
                    session.Log("Uninstall product {0}", productInstallation.ProductName);
                    try
                    {
                        Installer.SetInternalUI(InstallUIOptions.Basic);
                        Installer.ConfigureProduct(productInstallation.ProductCode, 1, InstallState.Absent, "");
                    }
                    catch (Exception e)
                    {
                        session.Log(e.Message);
                        throw;
                    }
                }
            }
            return ActionResult.Success;
        }

        private static void RegisterAddIn(string addInName, string officeVersion)
        {
            RegistryKey userSettingsKey = null;
            RegistryKey instructionKey = null;

            try
            {
                userSettingsKey = Registry.LocalMachine.OpenSubKey(GetUserSettingsLocation(officeVersion), true);

                if (userSettingsKey == null)
                {
                    throw new Exception("Internal error: Office User Settings key does not exist", null);
                }

                instructionKey = userSettingsKey.OpenSubKey(addInName, true);

                if (instructionKey == null)
                {
                    instructionKey = userSettingsKey.CreateSubKey(addInName);
                }
                else
                {
                    // Remove the Delete instruction
                    try
                    {
                        instructionKey.DeleteSubKeyTree("DELETE");
                    }
                    catch (ArgumentException) { } // Delete instruction did not exist but that is ok.
                }

                IncrementCounter(instructionKey);
            }
            finally
            {
                if (instructionKey != null)
                    instructionKey.Close();
                if (userSettingsKey != null)
                    userSettingsKey.Close();
            }
        }

        private static void UnRegisterAddIn(string applicationName, string addInName, string officeVersion)
        {
            RegistryKey userSettingsKey = null;
            RegistryKey instructionKey = null;
            RegistryKey deleteKey = null;

            try
            {
                userSettingsKey = Registry.LocalMachine.OpenSubKey(GetUserSettingsLocation(officeVersion), true);

                if (userSettingsKey == null)
                {
                    throw new Exception("Internal error: Office User Settings key does not exist", null);
                }

                instructionKey = userSettingsKey.OpenSubKey(addInName, true);

                if (instructionKey == null)
                {
                    instructionKey = userSettingsKey.CreateSubKey(addInName);
                }
                else
                {
                    // Make sure there is no Create instruction
                    try
                    {
                        instructionKey.DeleteSubKeyTree("CREATE");
                    }
                    catch (ArgumentException) { } // Create instruction did not exist but that is ok.
                }

                string instructionString =
                                @"DELETE\" +
                                GetApplicationPath(applicationName) +
                                @"\" +
                                addInName;

                deleteKey = instructionKey.CreateSubKey(instructionString);

                IncrementCounter(instructionKey);
            }
            finally
            {
                if (deleteKey != null)
                    deleteKey.Close();
                if (instructionKey != null)
                    instructionKey.Close();
                if (userSettingsKey != null)
                    userSettingsKey.Close();
            }
        }


        #region private methods


        private static string GetUserSettingsLocation(string officeVersion)
        {
            if (string.IsNullOrEmpty(officeVersion))
                officeVersion = "2007";
            switch (officeVersion)
            {
                case "2007":
                    return @"Software\Microsoft\Office\12.0\User Settings";
                case "2010":
                    return @"Software\Microsoft\Office\14.0\User Settings";
                case "2013":
                    return @"Software\Microsoft\Office\15.0\User Settings";
                case "2016":
                    return @"Software\Microsoft\Office\16.0\User Settings";
                default:
                    throw new Exception(officeVersion + " is not a supported Office version", null);
            }
        }

        private static void IncrementCounter(RegistryKey instructionKey)
        {
            int count = 1;
            object value = instructionKey.GetValue("Count");

            if (value != null)
            {
                if ((int)value != Int32.MaxValue)
                    count = (int)value + 1;
            }
            instructionKey.SetValue("Count", count);
        }

        private static string GetApplicationPath(string applicationName)
        {

            switch (applicationName.ToLower())
            {

                case "excel":
                    return @"Software\Microsoft\Office\Excel\Addins\";
                case "infopath":
                    return @"Software\Microsoft\Office\InfoPath\Addins\";
                case "outlook":
                    return @"Software\Microsoft\Office\Outlook\Addins\";
                case "powerpoint":
                    return @"Software\Microsoft\Office\PowerPoint\Addins\";
                case "word":
                    return @"Software\Microsoft\Office\Word\Addins\";
                case "visio":
                    return @"Software\Microsoft\Visio\Addins\";
                case "project":
                    return @"Software\Microsoft\Office\MS Project\Addins\";
                default:
                    throw new Exception(applicationName + " is not a supported application", null);
            }
        }
        # endregion
    }
}
