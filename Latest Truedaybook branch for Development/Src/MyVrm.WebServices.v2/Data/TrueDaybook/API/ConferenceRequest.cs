﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;


namespace CallWebApi.Classes
{
    /// <summary>
    /// Class with conference to update/insert in WebApi actions
    /// </summary>
    public class ConferenceRequest 
    {
        /// <summary>
        /// Conference participant
        /// </summary>
        public class Participant
        {
            /// <summary>
            /// Email of participant
            /// </summary>
            public string Email { set; get; }

            /// <summary>
            /// Name of participant
            /// </summary>
            public string Name { set; get; }
        }

        /// <summary>
        /// Id of conference
        /// </summary>
        public int Id { set; get; }

        /// <summary>
        /// Conference subject
        /// </summary>
        public string Subject { set; get; }

        /// <summary>
        /// Conference invitation body
        /// </summary>
        public string InvitationBody { set; get; }

        /// <summary>
        /// Start Date of conference
        /// </summary>
        public DateTime StartDate { set; get; }

        /// <summary>
        /// End Date of conference
        /// </summary>
        public DateTime EndDate { set; get; }

        /// <summary>
        /// List of conference participants
        /// </summary>
        public List<Participant> Participants { set; get; }

        /// <summary>
        /// Not supported so far
        /// </summary>
        public string Recurrence { set; get; }
    }
}