﻿namespace MyVrm.Outlook.WinForms.Conference
{
    partial class MaxSeatExceededDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnCheckAvailableSeats = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(22, 17);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(63, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "labelControl1";
            // 
            // btnCheckAvailableSeats
            // 
            this.btnCheckAvailableSeats.Location = new System.Drawing.Point(508, 81);
            this.btnCheckAvailableSeats.Name = "btnCheckAvailableSeats";
            this.btnCheckAvailableSeats.Size = new System.Drawing.Size(119, 23);
            this.btnCheckAvailableSeats.TabIndex = 1;
            this.btnCheckAvailableSeats.Text = "Check Available Seats";
            this.btnCheckAvailableSeats.Click += new System.EventHandler(this.btnCheckAvailableSeats_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(631, 81);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // MaxSeatExceededDialog
            // 
            this.AcceptButton = this.btnCheckAvailableSeats;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(708, 116);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnCheckAvailableSeats);
            this.Controls.Add(this.labelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MaxSeatExceededDialog";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Maximum Seats Exceeded";
            this.Load += new System.EventHandler(this.MaxSeatExceededDialog_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MaxSeatExceededDialog_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btnCheckAvailableSeats;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
    }
}