﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    internal class SaveCateringWorkOrdersRequest : ServiceRequestBase<SaveCateringWorkOrdersResponse>
    {
        public SaveCateringWorkOrdersRequest(MyVrmService service) : base(service)
        {
        }

        internal ConferenceId ConferenceId { get; set; }
        internal CateringWorkOrderCollection WorkOrders { get; set; }

        #region Overrides of ServiceRequestBase<SaveCateringWorkOrdersResponse>

        internal override string GetXmlElementName()
        {
            return "SetProviderWorkorderDetails";
        }

        internal override string GetCommandName()
        {
            return "SetProviderWorkorderDetails";
        }

        internal override string GetResponseXmlElementName()
        {
            return "success";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            writer.WriteElementValue(XmlNamespace.NotSpecified, "Type", 2);
            ConferenceId.WriteToXml(writer, "ConfID");
            WorkOrders.WriteToXml(writer, "Workorders");
        }

        #endregion
    }
}
