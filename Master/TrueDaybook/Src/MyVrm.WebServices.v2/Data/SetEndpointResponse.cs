﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.WebServices.Data
{
    internal class SetEndpointResponse : ServiceResponse
    {
        internal EndpointId Id { get; private set; }

        internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
        {
            reader.Read();
            var id = new EndpointId();
            id.LoadFromXml(reader, "EndpointID");
            Id = id;
        }
    }
}
