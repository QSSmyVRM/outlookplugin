﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using DevExpress.LookAndFeel;
using DevExpress.UserSkins;
using Microsoft.Office.Interop.Outlook;
using MyVrm.Outlook;
using MyVrm.Outlook.WinForms;
using MyVrm.Outlook.WinForms.Conference;
using MyVrm.Outlook.WinForms.Options;
using MyVrm.WebServices.Data;
using MyVrmAddin2007.Properties;
using Exception = System.Exception;
using RecurrencePattern = Microsoft.Office.Interop.Outlook.RecurrencePattern;

namespace MyVrmAddin2007
{
    public partial class ThisAddIn
    {
        private Explorer _currentExplorer;

        private Inspectors _inspectors;
        private Explorers _openExplorers;
        private readonly Dictionary<Guid, InspectorWrapper> _wrappedInspectors = new Dictionary<Guid, InspectorWrapper>();

        private readonly OutlookAppointmentImplCollection _appointments = new OutlookAppointmentImplCollection();

        private readonly List<AppointmentCoreTriats> _shadowAppointments = new List<AppointmentCoreTriats>();
        private readonly List<string> _deletedConferences = new List<string>();

        private readonly Dictionary<OutlookAppointmentImpl, InspectorWrapper> _appointmentToWrappers = new Dictionary<OutlookAppointmentImpl, InspectorWrapper>();
        private readonly Dictionary<OutlookAppointmentImpl, ConferenceWrapper> _appointmentToConferenceWrappers = new Dictionary<OutlookAppointmentImpl, ConferenceWrapper>();

        private readonly ExplorerWrapperCollection _wrappedExplorers = new ExplorerWrapperCollection();

        private DefaultLookAndFeel _defaultLookAndFeel;

        private MAPIFolder _deletedItemsFolder;


        [EditorBrowsable(EditorBrowsableState.Never)]
        public OutlookAppointmentImplCollection Appointments
        {
            get { return _appointments; }
        }

        //Already closed appointments - just for Live Meeting co-existance
        [EditorBrowsable(EditorBrowsableState.Never)]
        public List<AppointmentCoreTriats> ShadowAppointments
        {
            get { return _shadowAppointments; }
        }

        //Already deleted conferences
        [EditorBrowsable(EditorBrowsableState.Never)]
        public List<string> DeletedConferences
        {
            get { return _deletedConferences; }
        }

        //List of converted from myVRM to Live Meeting conferences:
        // Key      - LM GlobalAppointmentId, which accept ConferenceId
        // Value    - ConferenceId of the converted myVRM conference
        public Dictionary<string, ConferenceId> ConvertedConfs = new Dictionary<string, ConferenceId>();

        public Dictionary<OutlookAppointmentImpl, ConferenceWrapper> AppointmentToConferenceWrappers
        {
            get { return _appointmentToConferenceWrappers; }
        }

        internal Dictionary<OutlookAppointmentImpl, InspectorWrapper> AppointmentToWrappers
        {
            get { return _appointmentToWrappers; }
        }

        public bool HasConferenceWrapper(AppointmentItem appointmentItem)
        {
            if (appointmentItem == null)
            {
                MyVrmAddin.TraceSource.TraceWarning("HasConferenceWrapper() : appointmentItem = null");
                return false;
            }

            var outlookAppointmentImpl = Appointments[appointmentItem];
            if (outlookAppointmentImpl != null)
            {
                ConferenceWrapper conferenceWrapper;
                return AppointmentToConferenceWrappers.TryGetValue(outlookAppointmentImpl, out conferenceWrapper);
            }
            return false;
        }

        public ConferenceWrapper GetConferenceWrapperForAppointment(AppointmentItem appointmentItem)
        {
            if (appointmentItem == null)
            {
                MyVrmAddin.TraceSource.TraceWarning("GetConferenceWrapperForAppointment() : appointmentItem = null");
                return null;
            }

            ConferenceWrapper conferenceWrapper;
            var outlookAppointmentImpl = Appointments[appointmentItem];
            if (outlookAppointmentImpl == null)
            {
                outlookAppointmentImpl = new OutlookAppointmentImpl(appointmentItem);
                Appointments.Add(outlookAppointmentImpl);
            }
            if (!AppointmentToConferenceWrappers.TryGetValue(outlookAppointmentImpl, out conferenceWrapper))
            {
                conferenceWrapper = new ConferenceWrapper(outlookAppointmentImpl, MyVrmService.Service);
                AppointmentToConferenceWrappers.Add(outlookAppointmentImpl, conferenceWrapper);
            }
            return conferenceWrapper;
        }

        private void ThisAddIn_Startup(object sender, EventArgs e)
        {
            try
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = System.Threading.Thread.CurrentThread.CurrentCulture;

                MyVrmAddin.TraceSource.TraceInformation("Add-in is starting up...");
                var thisAssembly = Assembly.GetExecutingAssembly();
                foreach (Attribute attribute in thisAssembly.GetCustomAttributes(true))
                {
                    if (attribute is AssemblyFileVersionAttribute)
                    {
                        MyVrmAddin.ProductVersion = ((AssemblyFileVersionAttribute)attribute).Version;
                    }
                    else if (attribute is AssemblyProductAttribute)
                    {
                        MyVrmAddin.ProductName = ((AssemblyProductAttribute)attribute).Product;
                    }
                    else if (attribute is AssemblyCompanyAttribute)
                    {
                        MyVrmAddin.CompanyName = ((AssemblyCompanyAttribute)attribute).Company;
                    }
                    else if (attribute is AssemblyCopyrightAttribute)
                    {
                        MyVrmAddin.Copyright = ((AssemblyCopyrightAttribute)attribute).Copyright;
                    }
                }

                //Properties.Settings settings = new Properties.Settings();
                string settingValue = GetSettingValue("ProductDisplayName") as string;//settings["ProductDisplayName"] as string;
                MyVrmAddin.ProductDisplayName = "myVRM"; //default
                if (!string.IsNullOrEmpty(settingValue))
                    MyVrmAddin.ProductDisplayName = settingValue;

                settingValue = GetSettingValue("Logo16x16") as string;//settings["Logo16x16"] as string;
                MyVrmAddin.Logo16 = Resources.Logo; //default
                if (!string.IsNullOrEmpty(settingValue))
                    MyVrmAddin.Logo16 = Resources.ResourceManager.GetObject(settingValue) as Bitmap;

                settingValue = GetSettingValue("Logo65x65") as string;//settings["Logo65x65"] as string;
                MyVrmAddin.Logo65 = Resources.Logo65x65; //default
                if (!string.IsNullOrEmpty(settingValue))
                    MyVrmAddin.Logo65 = Resources.ResourceManager.GetObject(settingValue) as Bitmap;

                settingValue = GetSettingValue("Copyright") as string;//settings["Copyright"] as string;
                if (!string.IsNullOrEmpty(settingValue))
                    MyVrmAddin.Copyright = settingValue;

                settingValue = GetSettingValue("BuildType") as string;//settings["Copyright"] as string;
                if (!string.IsNullOrEmpty(settingValue))
                    MyVrmAddin.BuildType = settingValue;

                MyVrmAddin.TraceSource.TraceInformation("OS version: {0}", Environment.OSVersion.ToString());
                MyVrmAddin.TraceSource.TraceInformation("Local timezone: {0}", TimeZoneInfo.Local.StandardName);
                MyVrmAddin.TraceSource.TraceInformation("OS language: {0}", CultureInfo.InstalledUICulture.LCID);
                MyVrmAddin.TraceSource.TraceInformation(".NET Framework version: {0}", Environment.Version.ToString());
                MyVrmAddin.TraceSource.TraceInformation("Outlook version: {0}", Application.Version);
                MyVrmAddin.TraceSource.TraceInformation("Add-in: {0}", MyVrmAddin.ProductName);
                MyVrmAddin.TraceSource.TraceInformation("Add-in version: {0}", MyVrmAddin.ProductVersion);

                MyVrmAddin.ProductStorageVersion = "1.0";
                MyVrmService.Service.UtcEnabled = 1; //??
                MyVrmService.TraceSource = MyVrmAddin.TraceSource;
                // Outlook Generic client
                MyVrmService.Service.ClientType = "02";
                // Client version
                MyVrmService.Service.ClientVersion = ClientVersion.Parse(MyVrmAddin.ProductVersion);
                UIService.DefaultCaption = MyVrmAddin.ProductDisplayName;

                OfficeSkins.Register();
                _defaultLookAndFeel = new DefaultLookAndFeel();
                _defaultLookAndFeel.LookAndFeel.SetSkinStyle(UIHelper.GetCurrentSkinName(Application.Version));
                // Display EULA dialog if neccessary
                if (!MyVrmAddin.Instance.IsLicenseAgreementAccepted)
                {
                    string licenseAgreementFile;
                    using (var eulaDlg = new LicenseAgreementDialog())
                    {
                        eulaDlg.Text = MyVrmAddin.ProductDisplayName;
                        licenseAgreementFile = MyVrmAddin.DefaultLicenseAgreementFile;
                        eulaDlg.LicenseAgreementFile = licenseAgreementFile;
                        if (eulaDlg.ShowDialog() == DialogResult.Cancel)
                        {
                            return;
                        }
                    }
                    MyVrmAddin.Instance.AcceptLicenseAgreement(licenseAgreementFile);
                }
                Application.OptionsPagesAdd += ApplicationOptionsPagesAdd;
                Application.ItemSend += ApplicationItemSend;
                _inspectors = Application.Inspectors;
                _inspectors.NewInspector += OnNewInspector;
                _currentExplorer = Application.ActiveExplorer();

                _openExplorers = Application.Explorers;
                _openExplorers.NewExplorer += openExplorers_NewExplorer;

                foreach (Explorer explorer in _openExplorers)
                {
                    _wrappedExplorers.Add(new MyVrmExplorer(explorer));
                }

                _currentExplorer.BeforeFolderSwitch += ExplorerBeforeFolderSwitch;
                _deletedItemsFolder = Application.Session.GetDefaultFolder(OlDefaultFolders.olFolderDeletedItems);
                MyVrmAddin.Instance.Settings.Reload();
                ShadowAppointments.Clear();
                ConvertedConfs.Clear();
            }
            catch (Exception exception)
            {
                MyVrmAddin.TraceSource.TraceException(exception);
                UIHelper.ShowError(exception.Message);
            }
        }

        private MAPIFolder _currentFolder;
        private Items _currentFolderItems;

        void ExplorerBeforeFolderSwitch(object newFolder, ref bool cancel)
        {
            var folder = (MAPIFolder)newFolder;
            if (folder == null)
                return;
            //if (_currentFolderItems != null)
            //{
            //    try
            //    {
            //        _currentFolderItems.ItemChange -= ItemsItemChange;
            //    }
            //    catch (Exception exception)
            //    {
            //        MyVrmAddin.TraceSource.TraceInformation("Eat this exception: ", exception);
            //    }

            //    _currentFolderItems = null;
            //}
            if (_currentFolder != null)
            {
                ((MAPIFolderEvents_12_Event)_currentFolder).BeforeItemMove -= FolderBeforeItemMove;
                _currentFolder = null;
            }
            // Ignore non-calendar folders
            if (folder.DefaultItemType != OlItemType.olAppointmentItem)
                return;
            _currentFolder = folder;
            ((MAPIFolderEvents_12_Event)_currentFolder).BeforeItemMove += FolderBeforeItemMove;

            _currentFolderItems = _currentFolder.Items;
            //_currentFolderItems.ItemChange += ItemsItemChange;
        }

        private bool IsDeletedItemsFolder(MAPIFolder aFolder) { return (aFolder.EntryID == _deletedItemsFolder.EntryID); }

        public bool IsItLiveMeetingAppointment(AppointmentItem item)
        {
            try
            {
                if (item != null && item.MessageClass.Contains("IPM.Appointment.Live Meeting Request"))
                    return true;
            }
            catch
            {
            }
            return false;
        }

        private void ItemsItemChange(object item)
        {
            try
            {
                var appointmentItem = item as AppointmentItem;
                if (appointmentItem == null)
                    return;

                MyVrmAddin.TraceSource.TraceInformation("ItemsItemChange() : appointmentItem = " + appointmentItem.GlobalAppointmentID);

                var appointmentImpl = Globals.ThisAddIn.Appointments[appointmentItem] ?? new OutlookAppointmentImpl(appointmentItem);
                {
                    if (appointmentImpl.ConferenceId == null && IsItLiveMeetingAppointment((AppointmentItem)item))
                    {
                        SetOverriddenLiveMeetingConfId(appointmentImpl);
                        //ConferenceId closedConfId = GetOverriddenLiveMeetingConfId(appointmentImpl);
                        //if(closedConfId != null)
                        //    appointmentImpl.m_ConvertedConfs.Add(appointmentImpl.GlobalAppointmentId, closedConfId);                    
                    }

                    if (!appointmentImpl.LinkedWithConference)
                    {
                        return;
                    }
                    var conferenceId = appointmentImpl.ConferenceId;
                    if (conferenceId != null)
                    {
                        Conference conference = null;
                        conference = Conference.Bind(MyVrmService.Service, conferenceId);
                        if (conference != null)
                        {
                            DateTime dt = conference.StartDate.Date + appointmentImpl.StartInStartTimeZone.TimeOfDay;
                            conference.StartDate = dt;
                            conference.EndDate = dt + appointmentImpl.Duration;

                            if (conference.RecurrencePattern != null)
                            {
                                conference.RecurrencePattern.StartTime = appointmentImpl.StartInStartTimeZone.TimeOfDay;
                                conference.RecurrencePattern.EndTime = appointmentImpl.EndInEndTimeZone.TimeOfDay;
                            }
                            if (conference.AppointmentTime != null)
                            {
                                conference.AppointmentTime.StartTime = appointmentImpl.StartInStartTimeZone.TimeOfDay;
                                conference.AppointmentTime.EndTime = appointmentImpl.EndInEndTimeZone.TimeOfDay;
                            }
                        }

                        switch (appointmentItem.RecurrenceState)
                        {
                            case OlRecurrenceState.olApptMaster:
                                if (conference != null)
                                {
                                    var recurrencePattern = appointmentItem.GetRecurrencePattern();
                                    //bool bSkipDeleteFromServer = DoSkipDeleteFromServer(false);

                                    //if (bSkipDeleteFromServer == false)
                                    {
                                        foreach (var exception in recurrencePattern.Exceptions.Cast<Microsoft.Office.Interop.Outlook.
                                            Exception>().Where(except => except.Deleted))
                                        {
                                            if (conference.RecurrencePattern != null)
                                            {
                                                if (conference.RecurrencePattern.RecurrenceType == RecurrenceType.Custom)
                                                {
                                                    //conference.RecurrencePattern.InitialOriginalDates ??
                                                    int ndx = conference.RecurrencePattern.CustomDates.FindIndex(
                                                        customDate => customDate == exception.OriginalDate.Date);
                                                    if (ndx != -1)
                                                    {
                                                        conference.DeleteOccurrance(exception.OriginalDate);
                                                    }
                                                }
                                                else
                                                {
                                                    conference.DeleteOccurrance(exception.OriginalDate);
                                                }
                                            }
                                        }
                                    }

                                    var changed = recurrencePattern.Exceptions.Cast<Microsoft.Office.Interop.Outlook.
                                        Exception>().Where(except => except.Deleted == false);
                                    bool bIsChanged = false;

                                    //If it was regular recurrent conf and we moved one occurence - transform to CustomDates
                                    if (changed.Count() > 0)
                                    {
                                        conference.ModifiedOccurrences.Clear();
                                        if (conference.RecurrencePattern != null)
                                        {
                                            conference.RecurrencePattern.CustomDates.Clear();
                                            conference.RecurrencePattern.RecurrenceType = RecurrenceType.Custom;
                                            conference.RecurrencePattern.StartDate = recurrencePattern.PatternStartDate;
                                            conference.RecurrencePattern.EndDate = recurrencePattern.PatternEndDate;
                                            RecurrencePattern rr = recurrencePattern;
                                            for (DateTime dt = rr.PatternStartDate; dt <= rr.PatternEndDate; dt = dt.AddDays(1))
                                            {
                                                try
                                                {
                                                    //Get regular instances and transform them into CustomDates
                                                    DateTime curt = dt;
                                                    curt = curt.AddHours(rr.StartTime.Hour);
                                                    curt = curt.AddMinutes(rr.StartTime.Minute);
                                                    curt = curt.AddSeconds(rr.StartTime.Second);

                                                    AppointmentItem ouItem = rr.GetOccurrence(curt);
                                                    ////if not null and not already in
                                                    if (ouItem != null)//????? && conference.RecurrencePattern.CustomDates.FirstOrDefault(a => a == ouItem.Start) == DateTime.MinValue)
                                                    {
                                                        DateTime found = conference.RecurrencePattern.CustomDates.FirstOrDefault(a => a.CompareTo(ouItem.Start) == 0);
                                                        if (found == DateTime.MinValue)
                                                            conference.RecurrencePattern.CustomDates.Add(ouItem.Start);
                                                        OccurrenceInfo anOccurence = new OccurrenceInfo(ouItem.Start, ouItem.End);
                                                        conference.ModifiedOccurrences.Add(anOccurence);
                                                    }
                                                }
                                                catch (Exception) //get an exception in case of date has already been changed
                                                {
                                                }
                                            }
                                            //bIsChanged = true;
                                        }

                                        foreach (var change in changed)
                                        {
                                            if (conference.RecurrencePattern != null)
                                            {
                                                //if not null and not already in
                                                DateTime found = conference.RecurrencePattern.CustomDates.FirstOrDefault(a => a.CompareTo(change.AppointmentItem.Start) == 0);
                                                if (found == DateTime.MinValue)
                                                    //if (conference.RecurrencePattern.CustomDates.FirstOrDefault(a => a == change.AppointmentItem.Start) == DateTime.MinValue)
                                                    conference.RecurrencePattern.CustomDates.Add(change.AppointmentItem.Start);
                                            }
                                            OccurrenceInfo anOccurence = new OccurrenceInfo(change.AppointmentItem.Start, change.AppointmentItem.End);
                                            conference.ModifiedOccurrences.Add(anOccurence);
                                        }
                                        if (conference.RecurrencePattern != null)
                                            conference.RecurrencePattern.CustomDates.Sort();

                                        //re-oreder ModifiedOccurrences
                                        OccurrenceInfoCollection orderedModifiedOccurrences = new OccurrenceInfoCollection();
                                        if (conference.RecurrencePattern != null)
                                        {
                                            foreach (DateTime dt in conference.RecurrencePattern.CustomDates)
                                            {
                                                OccurrenceInfo anOccurence = conference.ModifiedOccurrences.FirstOrDefault(occInfo => occInfo.Start == dt);
                                                orderedModifiedOccurrences.Add(anOccurence);
                                            }
                                        }
                                        conference.ModifiedOccurrences.Clear();
                                        foreach (var orderedModifiedOccurrence in orderedModifiedOccurrences)
                                        {
                                            conference.ModifiedOccurrences.Add(orderedModifiedOccurrence);
                                        }

                                        if (conference.RecurrencePattern != null)
                                            bIsChanged = conference.RecurrencePattern.CustomDates.Count > 0;
                                    }
                                    /* 
                                     * Commented only for check in - to build 2.0.58 - MUST BE RE-WORKED!! 
                                     * 
                                    var changed = recurrencePattern.Exceptions.Cast<Microsoft.Office.Interop.Outlook.
                                        Exception>().Where(except => except.Deleted == false);
                                    bool bIsChanged = false;

                                    //If it was regular recurrent conf and we moved one occurence - transform to CustomDates
                                    if (changed.Count() > 0 && conference.RecurrencePattern.CustomDates.Count == 0)
                                    {
                                        var occurrences = conference.RecurrencePattern.GetOccurrenceInfoList(conference.StartDate, //WRONG!!
                                                                                                             conference.EndDate); //WRONG!!
                                        foreach (var occurrenceInfo in occurrences)
                                        {
                                            conference.RecurrencePattern.CustomDates.Add(occurrenceInfo.Start); //+??
                                            conference.RecurrencePattern.RecurrenceType = RecurrenceType.Custom; //???
                                        }
                                        conference.RecurrencePattern.InitialOriginalDates.Clear();
                                        conference.RecurrencePattern.InitialOriginalDates.AddRange(conference.RecurrencePattern.CustomDates);
                                    }
                                    //Find changed occurence and change its' date
                                    foreach (var change in changed)
                                    {
                                        var ndx = conference.RecurrencePattern.InitialOriginalDates.FindIndex(a => a.Date == change.OriginalDate.Date);
                                        if (ndx != -1)
                                        {
                                            if (conference.RecurrencePattern.CustomDates[ndx].Date != change.AppointmentItem.StartInStartTimeZone.Date)
                                            {
                                                conference.RecurrencePattern.CustomDates[ndx] = change.AppointmentItem.Start;
                                                bIsChanged = true;
                                            }
                                        }
                                    }*/
                                    if (bIsChanged)
                                        conference.Save();

                                }
                                break;
                            case OlRecurrenceState.olApptNotRecurring:
                                if (conference != null)
                                {
                                    if (conference.StartDate.Date != appointmentImpl.StartInStartTimeZone.Date)
                                    {
                                        conference.StartDate = appointmentImpl.StartInStartTimeZone;
                                        conference.Save();
                                    }
                                }
                                break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MyVrmAddin.TraceSource.TraceException(e);
                UIHelper.ShowError(e.Message);
            }
        }

        bool DoSkipDeleteFromServer()//bool forConference)
        {
            bool bSkipDeleteFromServer = false;
            MyVrmAddin.TraceSource.TraceInformation("DoSkipDeleteFromServer() In.");
            switch (UIHelper.DeletionOptionValue)
            {
                case ConferenceOptionsDialog.DeletionOption.NoConfirmNoDelete:
                    bSkipDeleteFromServer = true;
                    break;
                case ConferenceOptionsDialog.DeletionOption.NoConfirmDelete:
                    bSkipDeleteFromServer = false;
                    break;
                case ConferenceOptionsDialog.DeletionOption.NeedConfirm:
                    if (UIHelper.ShowMessage(Strings.DeleteConferenceFromServerQuestion,//forConference ? Strings.DeleteConferenceFromServerQuestion : Strings.DeleteConferenceOccurenceFromServerQuestion, 
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    {
                        bSkipDeleteFromServer = true;
                    }
                    break;
            }
            MyVrmAddin.TraceSource.TraceInformation("DoSkipDeleteFromServer() Out, bSkipDeleteFromServer = " + bSkipDeleteFromServer);
            return bSkipDeleteFromServer;
        }

        void DeleteAppointment(AppointmentItem appointmentItem, ref bool cancel)
        {
            bool bDoForOutlook = true;
            bool doDeleteOnServer = false;
            bool br = IsItLiveMeetingAppointment(appointmentItem);
            MyVrmAddin.TraceSource.TraceInformation("DeleteAppointment() In.");

            if (appointmentItem == null)
            {
                MyVrmAddin.TraceSource.TraceInformation("DeleteAppointment() Out - null appointment.");
                return;
            }

            using (var appointmentImpl = Globals.ThisAddIn.Appointments[appointmentItem] ??
                                  new OutlookAppointmentImpl(appointmentItem))
            {
                try
                {
                    if (!appointmentImpl.LinkedWithConference)
                    {
                        bDoForOutlook = false;
                        MyVrmAddin.TraceSource.TraceInformation("DeleteAppointment() Out - not a conference.");
                        return;
                    }

                    var conferenceId = appointmentImpl.ConferenceId;
                    //If conference was not deleted yet - to avoid conference double deletion
                    doDeleteOnServer = conferenceId != null && DeletedConferences.FirstOrDefault(a => a.CompareTo(conferenceId.Id) == 0) == null;

                    MyVrmAddin.TraceSource.TraceInformation("DeleteAppointment() : conferenceId = " + (conferenceId != null ? conferenceId.Id : "null"));
                    if (conferenceId != null && doDeleteOnServer)
                    {
                        Conference conference = null;
                        MyVrmAddin.TraceSource.TraceInformation("DeleteAppointment() : delete conf state = " + appointmentItem.RecurrenceState);
                        switch (appointmentItem.RecurrenceState)
                        {
                            // Delete the occurrence if the occurrence or exception of recurring appointment is being deleted
                            case OlRecurrenceState.olApptOccurrence:
                            case OlRecurrenceState.olApptException:
                                {
                                    conference = Conference.Bind(MyVrmService.Service, conferenceId, appointmentImpl.Start,
                                                                 appointmentImpl.StartInStartTimeZone, appointmentImpl.EndInEndTimeZone);
                                    conference.DeleteOccurrance(appointmentImpl.Start);
                                    break;
                                }
                            // Delete the conference if master or not recurring appointment is being deleted
                            case OlRecurrenceState.olApptNotRecurring:
                            case OlRecurrenceState.olApptMaster:
                                {
                                    if (!DoSkipDeleteFromServer())
                                    {
                                        conference = Conference.Bind(MyVrmService.Service, conferenceId);
                                        conference.Delete();
                                    }
                                    DeletedConferences.Add(conferenceId.Id);
                                    break;
                                }
                        }
                        //appointmentImpl.UnlinkConference();
                        //Appointments.Remove(appointmentImpl);
                    }
                }
                catch (Exception e)
                {
                    MyVrmAddin.TraceSource.TraceException(e);
                    UIHelper.ShowError(e.Message);

                    if (UIHelper.ShowMessage(Strings.DeleteOUConferenceQuestion, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    {
                        bDoForOutlook = false;
                        cancel = true;
                    }
                    else
                    {
                        cancel = false;
                    }
                }

                if (bDoForOutlook)
                {
                    MyVrmAddin.TraceSource.TraceInformation("DeleteAppointment() : delete in Outlook");
                    appointmentImpl.UnlinkConference();
                    Appointments.Remove(appointmentImpl);
                }
            }

            MyVrmAddin.TraceSource.TraceInformation("DeleteAppointment() Out.");
        }

        void FolderBeforeItemMove(object item, MAPIFolder moveTo, ref bool cancel)
        {
            MyVrmAddin.TraceSource.TraceInformation("FolderBeforeItemMove() In.");
            //bool bSkipDeleteFromServer = false;
            //bool bDoForOutlook = true;
            if (!IsDeletedItemsFolder(moveTo))
            {
                MyVrmAddin.TraceSource.TraceInformation("FolderBeforeItemMove() Out - move to non-deleted folder.");
                return;
            }

            var appointmentItem = item as AppointmentItem;

            //Skip deletion for a converted to LM conference
            if (appointmentItem != null)
            {
                using (var appointmentImpl = Globals.ThisAddIn.Appointments[appointmentItem] ??
                                             new OutlookAppointmentImpl(appointmentItem))
                {
                    try
                    {
                        var conferenceId = appointmentImpl.ConferenceId;
                        if (conferenceId != null)
                        {
                            var found = Globals.ThisAddIn.ConvertedConfs.FirstOrDefault(a => a.Value == conferenceId);
                            if (found.Key != null && found.Key != appointmentItem.GlobalAppointmentID)
                            {
                                MyVrmAddin.TraceSource.TraceInformation("This conference (" + conferenceId.Id +
                                                                        ") is converted to Live Meeting - skip deletion on server.");
                                return;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        MyVrmAddin.TraceSource.TraceException(e);
                    }
                }
            }
            DeleteAppointment(appointmentItem, ref cancel);
            /*
            if (appointmentItem == null)
            {
                MyVrmAddin.TraceSource.TraceInformation("FolderBeforeItemMove() Out - null appointment.");
                return;
            }

            using (var appointmentImpl = Globals.ThisAddIn.Appointments[appointmentItem] ??
                                  new OutlookAppointmentImpl(appointmentItem))
            {
                try
                {
                    if (!appointmentImpl.LinkedWithConference)
                    {
                        bDoForOutlook = false;
                        MyVrmAddin.TraceSource.TraceInformation("FolderBeforeItemMove() Out - not a conference.");
                        return;
                    }

                    var conferenceId = appointmentImpl.ConferenceId;
                    MyVrmAddin.TraceSource.TraceInformation("FolderBeforeItemMove() : conferenceId = " + (conferenceId != null ? conferenceId.Id : "null"));
                    if (conferenceId != null)
                    {
                        Conference conference = null;
                        MyVrmAddin.TraceSource.TraceInformation("FolderBeforeItemMove() : delete conf state = " + appointmentItem.RecurrenceState);
                        switch (appointmentItem.RecurrenceState)
                        {
                                // Delete the occurrence if the occurrence or exception of recurring appointment is being deleted
                            case OlRecurrenceState.olApptOccurrence:
                            case OlRecurrenceState.olApptException:
                                {
                                    conference = Conference.Bind(MyVrmService.Service, conferenceId, appointmentImpl.Start,
                                                                 appointmentImpl.StartInStartTimeZone, appointmentImpl.EndInEndTimeZone);
                                    conference.DeleteOccurrance(appointmentImpl.Start);
                                    break;
                                }
                                // Delete the conference if master or not recurring appointment is being deleted
                            case OlRecurrenceState.olApptNotRecurring:
                            case OlRecurrenceState.olApptMaster:
                                {
                                    if (!DoSkipDeleteFromServer())
                                    {
                                        conference = Conference.Bind(MyVrmService.Service, conferenceId);
                                        conference.Delete();
                                        DeletedConferences.Add(conferenceId.Id);
                                    }
                                    else
                                    {
                                        DeletedConferences.Add(conferenceId.Id);
                                    }
                                    break;
                                }
                        }
                        //appointmentImpl.UnlinkConference();
                        //Appointments.Remove(appointmentImpl);
                    }
                }
                catch (Exception e)
                {
                    MyVrmAddin.TraceSource.TraceException(e);
                    UIHelper.ShowError(e.Message);
					
                    if (UIHelper.ShowMessage(Strings.DeleteOUConferenceQuestion, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    {
                        bDoForOutlook = false;
                        cancel = true;
                    }
                    else
                    {
                        cancel = false;
                    }
                }

                if (bDoForOutlook)
                {
                    MyVrmAddin.TraceSource.TraceInformation("FolderBeforeItemMove() : delete in Outlook");
                    appointmentImpl.UnlinkConference();
                    Appointments.Remove(appointmentImpl);
                }
            }
            */

            if (appointmentItem != null)
                Marshal.ReleaseComObject(appointmentItem);

            MyVrmAddin.TraceSource.TraceInformation("FolderBeforeItemMove() Out.");
        }


        void openExplorers_NewExplorer(Explorer explorer)
        {
            _wrappedExplorers.Add(new MyVrmExplorer(explorer));
        }

        //Tool bar items' tags:

        public static void Sub_SetAppointmentParamsFromTemplate(AppointmentItem appItem, Conference template, object asMissing)
        {
            //AppointmentItem appItem = (AppointmentItem)Application.CreateItem(OlItemType.olAppointmentItem);

            var conferenceWrapper = Globals.ThisAddIn.GetConferenceWrapperForAppointment(appItem);
            conferenceWrapper.Appointment.RaiseSavingEvent = false;
            conferenceWrapper.Host = MyVrmService.Service.UserId;
            conferenceWrapper.Conference.Name = template.Name;
            //+ Outlook assign 
            conferenceWrapper.Appointment.Subject = template.Name;

            conferenceWrapper.Conference.Password = template.Password;

            //Fake assign indeed
            conferenceWrapper.Conference.Description = template.Description;
            //Real assign
            conferenceWrapper.Appointment.Body = template.Description;

            conferenceWrapper.Conference.Duration = template.Duration;
            //+ Outlook assign 
            conferenceWrapper.Appointment.Duration = template.Duration;

            //conferenceWrapper.template.Type = template.Type;

            //Fake assign indeed
            conferenceWrapper.Conference.Rooms.Clear();
            conferenceWrapper.Conference.Rooms.AddRange(template.Rooms);
            //Real assign
            conferenceWrapper.LocationIds.Clear();
            conferenceWrapper.LocationIds.AddRange(template.Rooms);

            //Assign participants
            conferenceWrapper.Participants.Clear();
            //Outlook assign - for mail notification sending
            var participantsEmialList = template.Participants.Select(participant => participant.Email).ToList();
            conferenceWrapper.Appointment.SetRecipients(participantsEmialList);

            conferenceWrapper.Conference.IsPublic = template.IsPublic;

            appItem.Display(asMissing);
            conferenceWrapper.Appointment.RaiseSavingEvent = true;
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
            try
            {
                MyVrmAddin.TraceSource.TraceInformation("Add-in is shutting down...");
                Application.OptionsPagesAdd -= ApplicationOptionsPagesAdd;
                Application.ItemSend -= ApplicationItemSend;
                _inspectors.NewInspector -= OnNewInspector;
                _openExplorers.NewExplorer -= openExplorers_NewExplorer;
                /*???*/
                if (_currentExplorer != null)
                    _currentExplorer.BeforeFolderSwitch -= ExplorerBeforeFolderSwitch;
                if (((MAPIFolderEvents_12_Event)_currentFolder) != null)
                    ((MAPIFolderEvents_12_Event)_currentFolder).BeforeItemMove -= FolderBeforeItemMove;
                if (_currentFolderItems != null)
                {
                    try
                    {
                        _currentFolderItems.ItemChange -= ItemsItemChange;
                    }
                    catch (Exception exception)
                    {
                        MyVrmAddin.TraceSource.TraceInformation("Eat this exception: ", exception);
                    }
                }
                /**/
                //if (Application.Version.StartsWith("12."))
                //    ((ExplorerEvents_Event)_currentExplorer).Deactivate/*Close*/ += OnClose;
                foreach (var appointmentImpl in Appointments.Where(appointmentImpl => appointmentImpl != null))
                {
                    appointmentImpl.Dispose();
                }
                Appointments.Clear();
                ShadowAppointments.Clear();
                ConvertedConfs.Clear();
            }
            catch (Exception exception)
            {
                MyVrmAddin.TraceSource.TraceException(exception);
            }
        }

        private void ApplicationItemSend(object item, ref bool cancel)
        {
            AppointmentItem apptItem = null;
            try
            {
                MyVrmAddin.TraceSource.TraceInformation("ApplicationItemSend() In.");
                if (item is MeetingItem)
                {
                    var meetingItem = (MeetingItem)item;
                    apptItem = meetingItem.GetAssociatedAppointment(false);
                    if (apptItem == null)
                    {
                        MyVrmAddin.TraceSource.TraceInformation("ApplicationItemSend() : apptItem = null.");
                    }
                    //Ignore cancelled and already deleted meetings - delete only if needed
                    if (apptItem != null && apptItem.MeetingStatus == OlMeetingStatus.olMeetingCanceled)
                    {
                        DeleteAppointment(apptItem, ref cancel);
                        /*
                        var confId = OutlookAppointmentImpl.GetConferenceId(apptItem);
                        MyVrmAddin.TraceSource.TraceInformation("ApplicationItemSend() : a cancelled meeting, confId = " + (confId != null ? confId.Id : "null"));
                        bool doDelete = confId != null && DeletedConferences.FirstOrDefault(a => a.CompareTo(confId.Id) == 0) == null;
                        if (confId != null && doDelete) // && Appointments[confId] == null
                        {
                            MyVrmAddin.TraceSource.TraceInformation("ApplicationItemSend() : delete.");
                            if (apptItem.RecurrenceState == OlRecurrenceState.olApptMaster || apptItem.RecurrenceState == OlRecurrenceState.olApptNotRecurring)
                            {
                                if (!DoSkipDeleteFromServer())
                                {
                                    Conference conference = Conference.Bind(MyVrmService.Service, confId);
                                    conference.Delete();
                                    DeletedConferences.Add(confId.Id);
                                }
                            }
                            else
                            {
                               // if (apptItem.IsRecurring && confId.IsRecurringId)
                                {
                                    Conference conference = Conference.Bind(MyVrmService.Service, confId, apptItem.Start,
                                                                         apptItem.StartInStartTimeZone, apptItem.EndInEndTimeZone);
                                    conference.DeleteOccurrance(apptItem.Start);
                                }
                            }
						 
                        }
                        */

                        MyVrmAddin.TraceSource.TraceInformation("ApplicationItemSend() deleted.");
                        //return;
                    }
                    else //Send a letter 
                    {
                        MyVrmAddin.TraceSource.TraceInformation("ApplicationItemSend() : send a msg.");
                        var conferenceId = apptItem != null ? OutlookAppointmentImpl.GetConferenceId(apptItem) : null;
                        if (conferenceId != null)
                        {
                            var appointmentItem = Appointments[conferenceId];
                            if (appointmentItem != null)
                            {
                                if (appointmentItem.Item.MeetingStatus == OlMeetingStatus.olMeetingCanceled)
                                    return;

                                var conferenceWrapper = AppointmentToConferenceWrappers[appointmentItem];
                                conferenceWrapper.Conference.UpdateCalendarUniqueId(appointmentItem.GlobalAppointmentId);

                                if (UIHelper.AppendConferenceDetailsToMeeting)
                                {
                                    var conference = conferenceWrapper.Conference;

                                    Conference oldConf = MyVrmService.Service.GetConference(conference.Id);
                                    ConferenceId uniqueId = oldConf.UniqueId;
                                    if (conference != null)
                                    {
                                        //  var cat = meetingItem.Attachments;
                                        //  cat.Add("C:\\Users\\Administrator\\Desktop\\AdminSettings.png");
                                        var body = meetingItem.Body;
                                        //  meetingItem.RTFBody =@"{\rtf1\ansi\deff0{\fonttbl{\f0 Arial;}}{\colortbl ;\red0\green0\blue255;}\pard\cf1\f0\fs24 Test}";
                                        var sb = new StringBuilder(body);
                                        sb.AppendLine("");
                                        sb.AppendLine("");
                                        sb.AppendLine(Strings.InitationTextStart);
                                        sb.AppendLine();
                                        string temp = Strings.ConferenceNameText;
                                        temp += String.Format(": {0}", conference.Name);
                                        sb.AppendLine(temp);
                                        //	sb.AppendLine();
                                        temp = Strings.txtUniqueID;
                                        temp += String.Format(": {0}", oldConf.UniqueId);
                                        sb.AppendLine(temp);
                                        //	sb.AppendLine();
                                        temp = Strings.txtDateAndTime;
                                        temp += String.Format(" {0} {1}", conference.StartDate.ToString("g"),
                                                        conference.TimeZone.DisplayName);
                                        sb.AppendLine(temp);

                                        //	sb.AppendLine();
                                        temp = "Conference Setup Time: ";
                                        temp += String.Format(" {0} {1}", conference.StartDate.ToString("g"),
                                                         conference.TimeZone.DisplayName);
                                        sb.AppendLine(temp);
                                        //   sb.AppendLine();
                                        temp = "Conference Teardown Time: ";
                                        temp += String.Format(" {0} {1}", conference.EndDate.ToString("g"),
                                                         conference.TimeZone.DisplayName);
                                        sb.AppendLine(temp);
                                        //sb.AppendLine();
                                        temp = Strings.txtDuration;
                                        temp += String.Format(": {0}", conference.Duration);
                                        sb.AppendLine(temp);
                                        //  sb.AppendLine();
                                        temp = "Requestor Name: ";
                                        temp += oldConf.HostName;
                                        sb.AppendLine(temp);
                                        temp = "Host Name:";
                                        temp += oldConf.HostName;
                                        sb.AppendLine(temp);
                                        if (MyVrmService.Service.OrganizationOptions.EnableConferencePassword)
                                        {
                                            temp = "Password: ";
                                            if (!String.IsNullOrEmpty(oldConf.Password))
                                                temp += oldConf.Password;
                                            else
                                                temp += "N/A";
                                            sb.AppendLine(temp);
                                        }
                                        // sb.AppendLine();
                                        temp = "Conference Recurring: ";
                                        string t1 = "", t2 = "", t3 = "";
                                        if (oldConf.IsRecurring)
                                        {
                                            if (oldConf.RecurrencePattern.RecurrenceType == RecurrenceType.Daily)
                                            {
                                                if (oldConf.RecurrencePattern.StartDate != null && oldConf.RecurrencePattern.StartTime != null && String.Format(": {0}", conference.Duration) != null)
                                                {
                                                    t1 = "Occurs every " + oldConf.RecurrencePattern.Interval + " day(s) effective " + oldConf.RecurrencePattern.StartDate.ToString("MM/dd/yy") + " from  " + oldConf.StartDate.ToString("HH:mm") +" for " + String.Format(": {0}", conference.Duration) + " mins";
                                                    t2 = "Occurs every " + oldConf.RecurrencePattern.Interval + " day(s) effective " + oldConf.RecurrencePattern.StartDate.ToString("MM/dd/yy") + " occurs " + oldConf.RecurrencePattern.Occurrences + " time(s) from " + oldConf.RecurrencePattern.StartTime.ToString("hh:mm") + " for " + String.Format(": {0}", conference.Duration) + " mins";
                                                    t3 = "Occurs every " + oldConf.RecurrencePattern.Interval + " day(s) effective " + oldConf.RecurrencePattern.StartDate.ToString("MM/dd/yy") + " until " + oldConf.RecurrencePattern.EndDate + " from {4} for " + String.Format(": {0}", conference.Duration) + " mins";

                                                }
                                            }

                                            else if (oldConf.RecurrencePattern.RecurrenceType == RecurrenceType.Weekly)
                                            {
                                                if (oldConf.RecurrencePattern.StartDate != null && oldConf.RecurrencePattern.StartTime != null && String.Format(": {0}", conference.Duration) != null)
                                                {
                                                    t1 = "Occurs every weekday effective " + oldConf.RecurrencePattern.StartDate.ToString("MM/dd/yy") + " from " + oldConf.RecurrencePattern.StartTime.ToString("hh:mm") + " for " + String.Format(": {0}", conference.Duration) + " mins"; //ZD 100528
                                                    t2 = "Occurs every weekday effective " + oldConf.RecurrencePattern.StartDate.ToString("MM/dd/yy") + " occurs  " + oldConf.RecurrencePattern.Occurrences + " time(s) from " + oldConf.RecurrencePattern.StartTime.ToString("HH:mm") + " for " + String.Format(": {0}", conference.Duration) + " mins"; //ZD 100528
                                                    t3 = "Occurs every weekday effective  " + oldConf.RecurrencePattern.StartDate.ToString("MM/dd/yy") + " until " + oldConf.RecurrencePattern.Occurrences + " from " + oldConf.RecurrencePattern.StartTime.ToString("HH:MM") + " for " + String.Format(": {0}", conference.Duration) + " mins"; //ZD 100528
                                                }
                                            }
                                            // if(!oldConf.RecurrencePattern.NeverEnds())
                                        }
                                        else
                                        {
                                            temp += "N/A";
                                        }
                                        sb.AppendLine(temp);
                                        sb.AppendLine();
                                        var roomNames = new string[conference.Rooms.Count];
                                        string vmrDetails = string.Empty;

                                        for (var i = 0; i < conference.Rooms.Count; i++)
                                        {

                                            var roomId = conference.Rooms[i];
                                            var roomProfile = MyVrmAddin.Instance.GetRoomFromCache(roomId);

                                            if (roomProfile.IsVMR)
                                            {
                                                vmrDetails += Strings.IntrenalBridge;
                                                vmrDetails += " ";
                                                vmrDetails += roomProfile.Name;
                                                vmrDetails += " > ";
                                                vmrDetails += roomProfile.InternalNumber;
                                                vmrDetails += "\n";
                                                vmrDetails += Strings.ExternalBridge;
                                                vmrDetails += " ";
                                                vmrDetails += roomProfile.Name;
                                                vmrDetails += " > ";
                                                vmrDetails += roomProfile.ExternalNumber;

                                            }

                                            if (roomProfile != null)
                                            {
                                                roomNames[i] = roomProfile.Name;

                                            }
                                        }


                                        sb.AppendLine();
                                        temp = Strings.txtLocations + " :";
                                        sb.AppendLine(temp);
                                        temp = "";
                                        foreach (RoomId room in conference.Rooms)
                                        {
                                            //temp += ": " + room + " (";
                                            temp += MyVrmAddin.Instance.GetRoomFromCache(room).TopTierName;
                                            temp += ">";
                                            temp += MyVrmAddin.Instance.GetRoomFromCache(room).MiddleTierName;
                                            temp += ">";
                                            temp += MyVrmAddin.Instance.GetRoomFromCache(room).Name;
                                            temp += "( ";
                                            temp += MyVrmAddin.Instance.GetRoomFromCache(room).TimeZone;
                                            temp += " )";
                                            EndpointId endptid = MyVrmAddin.Instance.GetRoomFromCache(room).EndpointId;
                                            sb.AppendLine(temp);
                                            sb.AppendLine();
                                            temp = "Endpoint: ";
                                            sb.AppendLine(temp);
                                            //temp += oldConf.AdvancedAudioVideoSettings.Endpoints.ToString();
                                            if (oldConf.AdvancedAudioVideoSettings.Endpoints != null)
                                            {
                                                if (oldConf.AdvancedAudioVideoSettings.Endpoints.Count > 0)
                                                {
                                                    foreach (ConferenceEndpoint endpt in oldConf.AdvancedAudioVideoSettings.Endpoints)
                                                    {
                                                        if (endpt.EndpointId == endptid)
                                                        {
                                                            if (endpt.Connection == MediaType.AudioVideo)
                                                            {
                                                                string n = "";
                                                                if (endpt.ConnectionType > 0)
                                                                {
                                                                    switch (endpt.ConnectionType)
                                                                    {
                                                                        case 1: n = "Dial in";
                                                                            break;
                                                                        case 2: n = "Dial out";
                                                                            break;
                                                                        case 3: n = "Direct";
                                                                            break;
                                                                        default: n = "N/A";
                                                                            break;
                                                                    }
                                                                }
                                                                sb.AppendLine("(Endpoint Address: " + endpt.Address + "; Connect Type - " + n + ")");
                                                            }
                                                            else if (endpt.Connection == MediaType.AudioOnly)
                                                            {
                                                                temp = "Audio Information: ";
                                                                string n = "";
                                                                if (endpt.ConnectionType > 0)
                                                                {
                                                                    switch (endpt.ConnectionType)
                                                                    {
                                                                        case 1: n = "Dial in";
                                                                            break;
                                                                        case 2: n = "Dial out";
                                                                            break;
                                                                        case 3: n = "Direct";
                                                                            break;
                                                                        default: n = "N/A";
                                                                            break;
                                                                    }
                                                                }
                                                                sb.AppendLine(temp);
                                                                sb.AppendLine("(Endpoint Address: " + endpt.Address + "; Connection Type - " + n + ")");
                                                            }
                                                            break;
                                                        }
                                                    }
                                                }
                                                else
                                                    sb.AppendLine("N/A");
                                            }
                                            else
                                            {
                                                sb.AppendLine("N/A");
                                            }


                                            sb.AppendLine("");
                                            temp = "";
                                        }
                                        //if (roomNames != null && roomNames.Length > 0)
                                        //{
                                        //    temp += String.Format(": {0}", string.Join("; ", roomNames));
                                        //}
                                        //else
                                        //    temp += ": N/A";
                                        sb.AppendLine(temp);
                                        string temp1 = "";
                                        temp1 = "External Participant(s) : ";
                                        int n1 = 0;
                                        string connectype = "";
                                        temp = "";
                                        foreach (ConferenceEndpoint cfdt in conference.AdvancedAudioVideoSettings.Endpoints)
                                        {

                                            if (cfdt.Type == ConferenceEndpointType.User)
                                            {
                                                n1 = 1;
                                                foreach (Participant party in conference.ExternalParticipants)
                                                {
                                                    if (cfdt.ExternalUserEmail == party.Email)
                                                    {
                                                        temp += party.FirstName + " " + party.LastName;
                                                        break;
                                                    }
                                                }
                                                temp += " ( Email - ";
                                                temp += cfdt.ExternalUserEmail;
                                                temp += ") ";
                                                temp += "\n";
                                                temp += "Connection : ";
                                                switch (cfdt.Connection)
                                                {
                                                    case MediaType.AudioOnly:
                                                        connectype = "Audio Only";
                                                        break;
                                                    case MediaType.AudioVideo:
                                                        connectype = "Audio/Video";
                                                        break;
                                                    case MediaType.NoAudioVideo:
                                                        connectype = "No Audio Video";
                                                        break;
                                                    default: connectype = "N/A";
                                                        break;
                                                }
                                                temp += connectype + " ";
                                                temp += "\n";
                                                temp += "IP/ISDN address: ";
                                                temp += cfdt.Address;
                                                temp += "\n";
                                                temp += "Connection type : ";
                                                switch (cfdt.ConnectionType)
                                                {
                                                    case 1: connectype = "Dial in";
                                                        break;
                                                    case 2: connectype = "Dial out";
                                                        break;
                                                    case 3: connectype = "Direct";
                                                        break;
                                                    default: connectype = "N/A";
                                                        break;
                                                }
                                                temp += connectype;
                                                temp += "\n";

                                                //sb.AppendLine("");
                                            }
                                        }
                                        if (n1 == 1)
                                            temp = temp1 + " " + temp;
                                        else
                                            temp = temp1 + " N/A";
                                        sb.AppendLine(temp);
                                        sb.AppendLine();
                                        n1 = 0;
                                        if (conference.Rooms.Count > 0)
                                        {
                                            temp1 = "Video Guest Location(s): ";
                                            // sb.AppendLine(temp1);
                                            temp = "";
                                            foreach (ConferenceEndpoint cfdt in conference.AdvancedAudioVideoSettings.Endpoints)
                                            {

                                                if (cfdt.Type == ConferenceEndpointType.Room)
                                                {

                                                    if (!String.IsNullOrEmpty(cfdt.EndPointName))
                                                    {
                                                        n1 = 1;
                                                        temp += MyVrmService.Service.OrganizationOptions.OnFlyTier.DefTopTier;
                                                        temp += " > ";
                                                        temp += MyVrmService.Service.OrganizationOptions.OnFlyTier.DefMiddleTier;
                                                        temp += " > ";
                                                        temp += cfdt.EndPointName;
                                                        temp += " \n ";
                                                        temp += "Endpoint ";
                                                        //sb.AppendLine(temp);
                                                        string n = "";
                                                        temp += " \n ";
                                                        if (cfdt.ConnectionType > 0)
                                                        {

                                                            switch (cfdt.ConnectionType)
                                                            {
                                                                case 1: n = "Dial in";
                                                                    break;
                                                                case 2: n = "Dial out";
                                                                    break;
                                                                case 3: n = "Direct";
                                                                    break;
                                                                default: n = "N/A";
                                                                    break;
                                                            }
                                                        }
                                                        temp += "(Endpoint Address: " + cfdt.Address + "; Connect Type - " + n + ")";

                                                    }
                                                }
                                            }
                                            if (n1 == 1)
                                                sb.AppendLine(temp1 + temp);
                                            else
                                                sb.AppendLine(temp1 + "N/A");
                                            sb.AppendLine();
                                        }
                                        string dialIn = oldConf.ConfDialIn;
                                        if (dialIn != null && dialIn.Length > 0)
                                        {
                                            sb.AppendLine();
                                            string dialinInformation = "Dial-in Information: ";
                                            dialinInformation += dialIn;
                                            sb.AppendLine(dialinInformation);
                                        }

                                        //  sb.AppendLine();

                                        sb.AppendLine();
                                        //temp = "Audio Information: ";
                                        //if (String.IsNullOrEmpty(oldConf.AudioVideoParameters.AudioCodec.ToString()))
                                        //    temp += oldConf.AudioVideoParameters.AudioCodec.ToString();
                                        //else
                                        //    temp += "N/A";
                                        // sb.AppendLine(temp);
                                        //   sb.AppendLine();
                                        if (!string.IsNullOrEmpty(vmrDetails))
                                        {
                                            sb.AppendLine("VMR :");
                                            sb.AppendLine(vmrDetails);
                                        }
                                        else
                                            sb.AppendLine("VMR : N/A");
                                        sb.AppendLine();

                                        if (MyVrmService.Service.OrganizationOptions.EnableCongSupport)
                                        {
                                            sb.AppendLine("Conference Support:");
                                            sb.AppendLine();
                                            if (MyVrmService.Service.OrganizationOptions.OnSiteAVSupportinEmail)
                                            {
                                                temp = "On-Site A/V Support: ";
                                                if (oldConf.ConciergeSupportParams.OnSiteAVSupport)
                                                    temp += "Yes";
                                                else
                                                    temp += "No";
                                                sb.AppendLine(temp);
                                            }
                                            if (MyVrmService.Service.OrganizationOptions.MeetandGreetinEmail)
                                            {
                                                temp = "Meet and Greet: ";
                                                if (oldConf.ConciergeSupportParams.MeetandGreet)
                                                    temp += "Yes";
                                                else
                                                    temp += "No";
                                                sb.AppendLine(temp);
                                            }
                                            if (MyVrmService.Service.OrganizationOptions.ConciergeMonitoringinEmail)
                                            {
                                                temp = "Call Monitoring: ";
                                                if (oldConf.ConciergeSupportParams.ConciergeMonitoring)
                                                    temp += "Yes";
                                                else
                                                    temp += "No";
                                                sb.AppendLine(temp);
                                            }
                                            if (MyVrmService.Service.OrganizationOptions.DedicatedVNOCOperatorinEmail)
                                            {
                                                temp = "Dedicated VNOC Operator: ";
                                                if (oldConf.ConciergeSupportParams.DedicatedVNOCOperator)
                                                    temp += "Yes";
                                                else
                                                    temp += "No";
                                                sb.AppendLine(temp);
                                            }
                                            sb.AppendLine();
                                        }
                                        sb.AppendLine("Custom Options: ");

                                        if (oldConf.CustomAttributes != null)
                                        {
                                            foreach (CustomAttribute CustAttr in oldConf.CustomAttributes)
                                            {
                                                if (!string.IsNullOrEmpty(CustAttr.Value))
                                                    sb.AppendLine(CustAttr.Title + ": " + CustAttr.Value);
                                            }
                                        }
                                        else
                                        {
                                            sb.AppendLine("N/A");
                                        }

                                        sb.AppendLine();
                                        var hostProfile = MyVrmService.Service.GetUser(conference.HostId);

                                        // sb.AppendLine();
                                        //sb.AppendLine();
                                        if (conference.VMRType == VMRConferenceType.Room && vmrDetails.Length > 0)
                                        {
                                            //  sb.AppendLine();
                                            sb.AppendLine();
                                            sb.AppendLine(Strings.VmrDetails);
                                            sb.AppendLine(vmrDetails);
                                        }
                                        else if (conference.VMRType == VMRConferenceType.Personal)
                                        {
                                            GetNewConferenceResponse response = MyVrmService.Service.GetNewConference();
                                            if (response != null)
                                            {
                                                // sb.AppendLine();
                                                sb.AppendLine();
                                                sb.AppendLine(Strings.VmrDetails);
                                                string details = Strings.IntrenalBridge;
                                                details += response.internalBridge;
                                                details += "\n";
                                                details += Strings.ExternalBridge;

                                                details += response.externalBridge;
                                                sb.AppendLine(details);
                                            }
                                        }
                                        else if (conference.VMRType == VMRConferenceType.External)
                                        {
                                            GetNewConferenceResponse response = MyVrmService.Service.GetNewConference();
                                            if (response != null)
                                            {
                                                //  sb.AppendLine();
                                                sb.AppendLine();
                                                sb.AppendLine(Strings.VmrDetails);
                                                string details = response.PrivateVMR;
                                                sb.AppendLine(details);
                                            }

                                        }
                                        var contactInfo = MyVrmService.Service.OrganizationOptions.Contact;
                                        sb.AppendLine("Tech Support: ");
                                        sb.AppendLine("Contact: " + contactInfo.Name);
                                        sb.AppendLine("eMail: " + contactInfo.Email);
                                        sb.AppendLine("Phone: " + contactInfo.Phone);
                                        sb.AppendLine();
                                        sb.AppendLine();

                                        // MeetingItem e = meetingItem.AddEvent();

                                        //e.XAltDescription = @"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 3.2//EN""><HTML><BODY>html goes here</BODY></HTML>";

                                        meetingItem.Body = sb.ToString();
                                        meetingItem.Save();
                                        //   meetingItem.Send();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MyVrmAddin.TraceSource.TraceException(e);
                UIHelper.ShowError(e.Message);
                cancel = true;
            }
            finally
            {
                if (apptItem != null)
                {
                    Marshal.ReleaseComObject(apptItem);
                }
                MyVrmAddin.TraceSource.TraceInformation("ApplicationItemSend() Out");
            }
        }

        private void OnNewInspector(Inspector inspector)
        {
            bool bExisting = false;
            try
            {


                var item = inspector.CurrentItem as AppointmentItem;
                if (item != null)
                    MyVrmAddin.TraceSource.TraceInformation("OnNewInspector() : appointmentItem = " + item.GlobalAppointmentID);

                if (item != null && (item.MeetingStatus != OlMeetingStatus.olMeetingReceived && item.MeetingStatus != OlMeetingStatus.olMeetingCanceled))
                {
                    bool br = IsItLiveMeetingAppointment(item);
                    var appointmentImpl = Globals.ThisAddIn.Appointments[item] ?? new OutlookAppointmentImpl(item);
                    if (appointmentImpl.ConferenceId == null && appointmentImpl.GlobalAppointmentId != null &&
                        (br || Globals.ThisAddIn.ConvertedConfs.ContainsKey(appointmentImpl.GlobalAppointmentId)))
                    //IsItLiveMeetingAppointment(item))
                    {
                        SetOverriddenLiveMeetingConfId(appointmentImpl);
                        if (Globals.ThisAddIn.ConvertedConfs.ContainsKey(appointmentImpl.GlobalAppointmentId))
                        {
                            appointmentImpl.ConferenceId = Globals.ThisAddIn.ConvertedConfs[appointmentImpl.GlobalAppointmentId];
                            appointmentImpl.SetNonOutlookProperty(true);
                        }
                        //ConferenceId closedConfId = GetOverriddenLiveMeetingConfId(appointmentImpl);
                        //if(closedConfId != null)
                        //    appointmentImpl.m_ConvertedConfs.Add(appointmentImpl.GlobalAppointmentId, closedConfId);                    
                    }
                    if (OutlookAppointmentImpl.GetConferenceId(item) != null)
                    {
                        bExisting = true;
                        InitConferenceWrapper(inspector, item);
                    }
                }
            }
            catch (Exception e)
            {
                MyVrmAddin.TraceSource.TraceException(e);
                //Commented when all 3 plugins are active this throws unnecessary error
                if (!bExisting)
                    UIHelper.ShowError(e.Message);
            }
        }

        internal ConferenceWrapper InitConferenceWrapper(Inspector inspector, AppointmentItem item)
        {
            if (item == null)
                MyVrmAddin.TraceSource.TraceWarning("InitConferenceWrapper() : item = null");

            if (_currentFolderItems != null)
            {
                try
                {
                    //_currentFolderItems.ItemChange -= ItemsItemChange;
                    _currentFolderItems.ItemChange += ItemsItemChange;
                }
                catch (Exception exception)
                {
                    MyVrmAddin.TraceSource.TraceInformation("Eat this exception: ", exception);
                }

                //_currentFolderItems.ItemChange += ItemsItemChange;
            }
            var conferenceWrapper = GetConferenceWrapperForAppointment(item);
            var apptImpl = item != null ? Appointments[item] : null;
            if (apptImpl != null && !AppointmentToWrappers.ContainsKey(apptImpl))
            {
                var inspectorWrapper = InspectorWrapper.GetWrapperFor(inspector);
                if (inspectorWrapper != null)
                {
                    //((InspectorEvents_10_Event)inspectorWrapper.Inspector).Close += new InspectorEvents_10_CloseEventHandler(myClose);
                    inspectorWrapper.Closed += InspectorWrapperClose;
                    _wrappedInspectors.Add(inspectorWrapper.Id, inspectorWrapper);
                    AppointmentToWrappers.Add(Appointments[item], inspectorWrapper);
                    //?????????
                    //Appointments[item].Body = conferenceWrapper.Description;
                }
            }
            if (conferenceWrapper != null)
                conferenceWrapper.ClearConfCopy();
            return conferenceWrapper;
        }

        private void InspectorWrapperClose(object sender, EventArgs e)
        {
            try
            {
                AppointmentItem Item2Save = null;
                var wrapper = (InspectorWrapper)sender;
                //Unsubscribe from appointment Saving event
                OutlookAppointmentImpl appointmentImpl = (from appointmentToWrapper in AppointmentToWrappers
                                                          where appointmentToWrapper.Value == wrapper
                                                          select appointmentToWrapper.Key).FirstOrDefault();

                if (appointmentImpl != null)
                {
                    if (appointmentImpl.ConferenceId != null)
                    {
                        if (ShadowAppointments.FirstOrDefault(item => item.GlobalAppointmentId == appointmentImpl.GlobalAppointmentId) == null)
                        {
                            ShadowAppointments.Add(new AppointmentCoreTriats()
                            {
                                ConfId = appointmentImpl.ConferenceId,
                                Subject = appointmentImpl.Subject,
                                Start = appointmentImpl.Start,
                                End = appointmentImpl.End,
                                Location = appointmentImpl.Location,
                                IsRecurring = appointmentImpl.IsRecurring,
                                GlobalAppointmentId = appointmentImpl.GlobalAppointmentId,
                            });

                        }
                        //If LM conference is already saved - remove converting record from list
                        KeyValuePair<string, ConferenceId> entry = new KeyValuePair<string, ConferenceId>(appointmentImpl.GlobalAppointmentId, appointmentImpl.ConferenceId);
                        if (ConvertedConfs.Contains(entry))
                        {
                            ConvertedConfs.Remove(appointmentImpl.GlobalAppointmentId);
                            Item2Save = appointmentImpl.Item;
                        }
                    }

                    AppointmentToWrappers.Remove(appointmentImpl);
                    ConferenceWrapper conference;
                    if (appointmentImpl.bSave && AppointmentToConferenceWrappers.TryGetValue(appointmentImpl, out conference))
                    {
                        bool nonOutlookPropsChanged = conference.Appointment.GetNonOutlookProperty();
                        if ((conference.Conference.IsDirty || nonOutlookPropsChanged) && conference.IsConfChanged())
                        {
                            conference.Save();
                        }
                    }
                    AppointmentToConferenceWrappers.Remove(appointmentImpl);
                    appointmentImpl.Dispose();
                }
                //unsubscribe from inspector Close event
                wrapper.Closed -= InspectorWrapperClose;
                // remove from opened inspector collection
                _wrappedInspectors.Remove(wrapper.Id);
                // remove from appointments collection
                Appointments.Remove(appointmentImpl);
                if (Item2Save != null)
                    Item2Save.Save();
            }
            catch (Exception exception)
            {
                MyVrmAddin.TraceSource.TraceException(exception);
                UIHelper.ShowError(exception.Message);
            }
        }

        private void ApplicationOptionsPagesAdd(PropertyPages pages)
        {
            try
            {
                var ctrl2 = new OptionsPage();
                pages.Add(ctrl2, Strings.SchedulerOptionsPageTitle);
            }
            catch (Exception e)
            {
                MyVrmAddin.TraceSource.TraceException(e);
                UIHelper.ShowError(e.Message);
            }
        }

        private object GetSettingValue(string settingName)
        {
            Properties.Settings settings = new Properties.Settings();
            try
            {
                return settings[settingName];
            }
            catch (Exception)
            {
                return null;
            }
        }

        // ConferenceId GetOverriddenLiveMeetingConfId(OutlookAppointmentImpl appointmentItem)
        void SetOverriddenLiveMeetingConfId(OutlookAppointmentImpl appointmentItem)
        {
            //ConferenceId confId = null;

            try
            {
                if (appointmentItem.ConferenceId == null && ShadowAppointments.Count > 0)
                {
                    AppointmentCoreTriats ret = ShadowAppointments.FirstOrDefault(
                        item => item.Subject == appointmentItem.Subject &&
                        item.Start == appointmentItem.Start &&
                        item.End == appointmentItem.End &&
                        item.Location == appointmentItem.Location &&
                        item.IsRecurring == appointmentItem.IsRecurring
                         );
                    if (ret != null)
                    {
                        //confId = ret.ConfId;

                        //Add converted to LM conference and according ConfId to list
                        if (string.IsNullOrEmpty(ConvertedConfs.FirstOrDefault(it => it.Key == appointmentItem.GlobalAppointmentId).Key))
                        {
                            ConvertedConfs.Add(appointmentItem.GlobalAppointmentId, ret.ConfId);
                            appointmentItem.m_ConvertedConfs.Add(appointmentItem.GlobalAppointmentId, ret.ConfId);
                        }

                        ShadowAppointments.Remove(ret);
                    }
                }
            }
            catch (Exception ex)
            {
                MyVrmAddin.TraceSource.TraceException(ex);
            }

            //return confId;
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            Startup += ThisAddIn_Startup;
            Shutdown += ThisAddIn_Shutdown;
        }

        #endregion
    }

    public class AppointmentCoreTriats
    {
        public string Subject { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string Location { get; set; }
        public bool IsRecurring { get; set; }
        public ConferenceId ConfId { get; set; }
        public string GlobalAppointmentId { get; set; }
    }
}
