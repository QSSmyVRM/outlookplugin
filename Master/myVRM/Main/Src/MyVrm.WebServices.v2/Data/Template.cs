﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
	/// <summary>
	/// Represents a conference in myVRM Web Service.
	/// </summary>
	[ServiceObjectDefinition("confInfo")]
	public class Template : ConferenceBase
	{
		/// <summary>
		/// Creates a new instance of the <see cref="Conference"/> class.
		/// </summary>
		public Template(MyVrmService service) : base(service)
		{
			Type = Service.OrganizationOptions.DefaultConferenceType;
		}
		/// <summary>
		/// Gets or sets conference type.
		/// </summary>
		public override ConferenceType Type
		{
			get
			{
				return (ConferenceType)PropertyBag[TemplateSchema.Type];
			}
			set
			{
				PropertyBag[TemplateSchema.Type] = value;
			}
		}
		
		internal override ServiceObjectSchema GetSchema()
		{
			return TemplateSchema.Instance;
		}
	}
}