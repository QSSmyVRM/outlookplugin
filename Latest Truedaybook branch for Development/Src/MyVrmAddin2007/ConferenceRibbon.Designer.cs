﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using Microsoft.Office.Tools.Ribbon;

namespace MyVrmAddin2007
{
	partial class ConferenceRibbon
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.tabMyVRM = this.Factory.CreateRibbonTab();
            this.groupShow = this.Factory.CreateRibbonGroup();
            this.appointmentButton = this.Factory.CreateRibbonButton();
            this.templatesGroup = this.Factory.CreateRibbonGroup();
            this.templateMenu = this.Factory.CreateRibbonMenu();
            this.separator1 = this.Factory.CreateRibbonSeparator();
            this.saveAsTemplateBn = this.Factory.CreateRibbonButton();
            this.generalGroup = this.Factory.CreateRibbonGroup();
            this.roomsPrivateCalendarButton = this.Factory.CreateRibbonButton();
            this.roomsPublicCalendarButton = this.Factory.CreateRibbonButton();
            this.manageRoomButton = this.Factory.CreateRibbonButton();
            this.preferencesGroup = this.Factory.CreateRibbonGroup();
            this.favoriteRoomsButton = this.Factory.CreateRibbonButton();
            this.optionsGroup = this.Factory.CreateRibbonGroup();
            this.optionsButton = this.Factory.CreateRibbonButton();
            this.tabVC = this.Factory.CreateRibbonTab();
            this.groupTrueDaybook = this.Factory.CreateRibbonGroup();
            this.bnVirtualConference = this.Factory.CreateRibbonButton();
            this.separator2 = this.Factory.CreateRibbonSeparator();
            this.tdTemplateMenu = this.Factory.CreateRibbonMenu();
            this.tdSaveAsTemplateBn = this.Factory.CreateRibbonButton();
            this.separator3 = this.Factory.CreateRibbonSeparator();
            this.bnCheckSeats = this.Factory.CreateRibbonButton();
            this.bnOptions = this.Factory.CreateRibbonButton();
            this.conferenceButton = this.Factory.CreateRibbonButton();
            this.tabMyVRM.SuspendLayout();
            this.groupShow.SuspendLayout();
            this.templatesGroup.SuspendLayout();
            this.generalGroup.SuspendLayout();
            this.preferencesGroup.SuspendLayout();
            this.optionsGroup.SuspendLayout();
            this.tabVC.SuspendLayout();
            this.groupTrueDaybook.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabMyVRM
            // 
            this.tabMyVRM.Groups.Add(this.groupShow);
            this.tabMyVRM.Groups.Add(this.templatesGroup);
            this.tabMyVRM.Groups.Add(this.generalGroup);
            this.tabMyVRM.Groups.Add(this.preferencesGroup);
            this.tabMyVRM.Groups.Add(this.optionsGroup);
            this.tabMyVRM.Label = "myVRM";
            this.tabMyVRM.Name = "tabMyVRM";
            this.tabMyVRM.Position = this.Factory.RibbonPosition.AfterOfficeId("TabAppointment");
            // 
            // groupShow
            // 
            this.groupShow.Items.Add(this.appointmentButton);
            this.groupShow.Items.Add(this.conferenceButton);
            this.groupShow.Label = "Show";
            this.groupShow.Name = "groupShow";
            // 
            // appointmentButton
            // 
            this.appointmentButton.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.appointmentButton.Label = "Appointment";
            this.appointmentButton.Name = "appointmentButton";
            this.appointmentButton.OfficeImageId = "ShowAppointmentPage";
            this.appointmentButton.ShowImage = true;
            this.appointmentButton.Click += appointmentButton_Click;
            // 
            // templatesGroup
            // 
            this.templatesGroup.Items.Add(this.templateMenu);
            this.templatesGroup.Items.Add(this.separator1);
            this.templatesGroup.Items.Add(this.saveAsTemplateBn);
            this.templatesGroup.Label = "Templates";
            this.templatesGroup.Name = "templatesGroup";
            // 
            // templateMenu
            // 
            this.templateMenu.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.templateMenu.Dynamic = true;
            this.templateMenu.Label = "Create From Template";
            this.templateMenu.Name = "templateMenu";
            this.templateMenu.OfficeImageId = "CreateForm";
            this.templateMenu.ShowImage = true;
            this.templateMenu.ItemsLoading +=templateMenu_ItemsLoading;
            // 
            // separator1
            // 
            this.separator1.Name = "separator1";
            // 
            // saveAsTemplateBn
            // 
            this.saveAsTemplateBn.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.saveAsTemplateBn.Label = "Save As Template";
            this.saveAsTemplateBn.Name = "saveAsTemplateBn";
            this.saveAsTemplateBn.OfficeImageId = "FileSaveAs";
            this.saveAsTemplateBn.ShowImage = true;
            this.saveAsTemplateBn.Click +=saveAsTemplateBn_Click;
            // 
            // generalGroup
            // 
            this.generalGroup.Items.Add(this.roomsPrivateCalendarButton);
            this.generalGroup.Items.Add(this.roomsPublicCalendarButton);
            this.generalGroup.Items.Add(this.manageRoomButton);
            this.generalGroup.Label = "General";
            this.generalGroup.Name = "generalGroup";
            // 
            // roomsPrivateCalendarButton
            // 
            this.roomsPrivateCalendarButton.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.roomsPrivateCalendarButton.Label = "Private Rooms Calendar";
            this.roomsPrivateCalendarButton.Name = "roomsPrivateCalendarButton";
            this.roomsPrivateCalendarButton.OfficeImageId = "OpenAttachedCalendar";
            this.roomsPrivateCalendarButton.ShowImage = true;
            this.roomsPrivateCalendarButton.Click += roomsCalendarButton_Click;
            // 
            // roomsPublicCalendarButton
            // 
            this.roomsPublicCalendarButton.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.roomsPublicCalendarButton.Label = "Public Rooms Calendar";
            this.roomsPublicCalendarButton.Name = "roomsPublicCalendarButton";
            this.roomsPublicCalendarButton.OfficeImageId = "SharingOpenWssCalendar";
            this.roomsPublicCalendarButton.ShowImage = true;
            this.roomsPublicCalendarButton.Click += roomsCalendarButton_Click;
            // 
            // manageRoomButton
            // 
            this.manageRoomButton.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.manageRoomButton.Label = "Manage Rooms";
            this.manageRoomButton.Name = "manageRoomButton";
            this.manageRoomButton.OfficeImageId = "GroupAdpDiagramLayout";
            this.manageRoomButton.ShowImage = true;
            this.manageRoomButton.Click +=manageRoomButton_Click;
            // 
            // preferencesGroup
            // 
            this.preferencesGroup.Items.Add(this.favoriteRoomsButton);
            this.preferencesGroup.Label = "Preferences";
            this.preferencesGroup.Name = "preferencesGroup";
            // 
            // favoriteRoomsButton
            // 
            this.favoriteRoomsButton.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.favoriteRoomsButton.Image = global::MyVrmAddin2007.Properties.Resources.Favorites;
            this.favoriteRoomsButton.Label = "Favorite Rooms";
            this.favoriteRoomsButton.Name = "favoriteRoomsButton";
            this.favoriteRoomsButton.ShowImage = true;
            this.favoriteRoomsButton.Click +=favoriteRoomsButton_Click;
            // 
            // optionsGroup
            // 
            this.optionsGroup.Items.Add(this.optionsButton);
            this.optionsGroup.Label = "Options";
            this.optionsGroup.Name = "optionsGroup";
            // 
            // optionsButton
            // 
            this.optionsButton.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.optionsButton.Label = "Options";
            this.optionsButton.Name = "optionsButton";
            this.optionsButton.OfficeImageId = "ControlsGallery";
            this.optionsButton.ShowImage = true;
            this.optionsButton.Click +=optionsButton_Click;
            // 
            // tabVC
            // 
            this.tabVC.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.tabVC.ControlId.OfficeId = "TabAppointment";
            this.tabVC.Groups.Add(this.groupTrueDaybook);
            this.tabVC.Label = "TabAppointment";
            this.tabVC.Name = "tabVC";
            this.tabVC.Position = this.Factory.RibbonPosition.AfterOfficeId("TabAppointment");
            // 
            // groupTrueDaybook
            // 
            this.groupTrueDaybook.Items.Add(this.bnVirtualConference);
            this.groupTrueDaybook.Items.Add(this.separator2);
            this.groupTrueDaybook.Items.Add(this.tdTemplateMenu);
            this.groupTrueDaybook.Items.Add(this.tdSaveAsTemplateBn);
            this.groupTrueDaybook.Items.Add(this.separator3);
            this.groupTrueDaybook.Items.Add(this.bnCheckSeats);
            this.groupTrueDaybook.Items.Add(this.bnOptions);
            this.groupTrueDaybook.Label = "TrueDaybook";
            this.groupTrueDaybook.Name = "groupTrueDaybook";
            this.groupTrueDaybook.Position = this.Factory.RibbonPosition.AfterOfficeId("GroupShow");
            this.groupTrueDaybook.Tag = "";
            // 
            // bnVirtualConference
            // 
            this.bnVirtualConference.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.bnVirtualConference.Image = global::MyVrmAddin2007.Properties.Resources.virtualConf32x32;
            this.bnVirtualConference.Label = "Virtual Conference";
            this.bnVirtualConference.Name = "bnVirtualConference";
            this.bnVirtualConference.Position = this.Factory.RibbonPosition.AfterOfficeId("ShowSchedulingPage");
            this.bnVirtualConference.ShowImage = true;
            this.bnVirtualConference.Click += bnVirtualConference_Click;
            // 
            // separator2
            // 
            this.separator2.Name = "separator2";
            // 
            // tdTemplateMenu
            // 
            this.tdTemplateMenu.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.tdTemplateMenu.Dynamic = true;
            this.tdTemplateMenu.Label = "Create From Template";
            this.tdTemplateMenu.Name = "tdTemplateMenu";
            this.tdTemplateMenu.OfficeImageId = "CreateForm";
            this.tdTemplateMenu.ShowImage = true;
            this.tdTemplateMenu.ItemsLoading +=tdTemplateMenu_ItemsLoading;
            // 
            // tdSaveAsTemplateBn
            // 
            this.tdSaveAsTemplateBn.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.tdSaveAsTemplateBn.Label = "Save As Template";
            this.tdSaveAsTemplateBn.Name = "tdSaveAsTemplateBn";
            this.tdSaveAsTemplateBn.OfficeImageId = "FileSaveAs";
            this.tdSaveAsTemplateBn.ShowImage = true;
            this.tdSaveAsTemplateBn.Click += saveAsTemplateBn_Click;
            // 
            // separator3
            // 
            this.separator3.Name = "separator3";
            // 
            // bnCheckSeats
            // 
            this.bnCheckSeats.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.bnCheckSeats.Image = global::MyVrmAddin2007.Properties.Resources.checkSeats32x32;
            this.bnCheckSeats.Label = "Check Available Seats";
            this.bnCheckSeats.Name = "bnCheckSeats";
            this.bnCheckSeats.ShowImage = true;
            this.bnCheckSeats.Click +=bnCheckSeats_Click;
            // 
            // bnOptions
            // 
            this.bnOptions.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.bnOptions.Image = global::MyVrmAddin2007.Properties.Resources.Options32x32;
            this.bnOptions.Label = "Options";
            this.bnOptions.Name = "bnOptions";
            this.bnOptions.ShowImage = true;
            this.bnOptions.Click += optionsButton_Click;
            // 
            // conferenceButton
            // 
            this.conferenceButton.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.conferenceButton.Image = global::MyVrmAddin2007.Properties.Resources.Logo65x65;
            this.conferenceButton.Label = "Conference";
            this.conferenceButton.Name = "conferenceButton";
            this.conferenceButton.ShowImage = true;
            this.conferenceButton.Click +=conferenceButton_Click;
            // 
            // ConferenceRibbon
            // 
            this.Name = "ConferenceRibbon";
            this.RibbonType = "Microsoft.Outlook.Appointment";
            this.Tabs.Add(this.tabMyVRM);
            this.Tabs.Add(this.tabVC);
            this.Load +=ConferenceRibbon_Load;
            this.tabMyVRM.ResumeLayout(false);
            this.tabMyVRM.PerformLayout();
            this.groupShow.ResumeLayout(false);
            this.groupShow.PerformLayout();
            this.templatesGroup.ResumeLayout(false);
            this.templatesGroup.PerformLayout();
            this.generalGroup.ResumeLayout(false);
            this.generalGroup.PerformLayout();
            this.preferencesGroup.ResumeLayout(false);
            this.preferencesGroup.PerformLayout();
            this.optionsGroup.ResumeLayout(false);
            this.optionsGroup.PerformLayout();
            this.tabVC.ResumeLayout(false);
            this.tabVC.PerformLayout();
            this.groupTrueDaybook.ResumeLayout(false);
            this.groupTrueDaybook.PerformLayout();
            this.ResumeLayout(false);

		}

		#endregion

		internal Microsoft.Office.Tools.Ribbon.RibbonTab tabMyVRM;
		internal Microsoft.Office.Tools.Ribbon.RibbonGroup generalGroup;
		internal Microsoft.Office.Tools.Ribbon.RibbonButton roomsPrivateCalendarButton;
		internal Microsoft.Office.Tools.Ribbon.RibbonGroup templatesGroup;
		internal Microsoft.Office.Tools.Ribbon.RibbonButton saveAsTemplateBn;
		internal Microsoft.Office.Tools.Ribbon.RibbonSeparator separator1;
		internal Microsoft.Office.Tools.Ribbon.RibbonGroup preferencesGroup;
		internal Microsoft.Office.Tools.Ribbon.RibbonButton favoriteRoomsButton;
		internal RibbonMenu templateMenu;
        internal RibbonGroup groupShow;
		internal RibbonButton appointmentButton;
		internal RibbonGroup optionsGroup;
		internal RibbonButton optionsButton;
		internal RibbonButton manageRoomButton;
		internal RibbonButton roomsPublicCalendarButton;
		private RibbonTab tabVC;
		internal RibbonGroup groupTrueDaybook;
		internal RibbonButton bnVirtualConference;
		internal RibbonButton bnCheckSeats;
		internal RibbonButton bnOptions;
		internal RibbonMenu tdTemplateMenu;
		internal RibbonSeparator separator2;
		internal RibbonButton tdSaveAsTemplateBn;
        internal RibbonSeparator separator3;
        internal RibbonButton conferenceButton;
	}

	partial class ThisRibbonCollection : Microsoft.Office.Tools.Ribbon.RibbonReadOnlyCollection
	{
		internal ConferenceRibbon ConferenceRibbon
		{
			get { return this.GetRibbon<ConferenceRibbon>(); }
		}
	}
}
