﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.Outlook.WinForms.Conference
{
	partial class ExternalRoomDlg
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.txtEdnpt = new DevExpress.XtraEditors.TextEdit();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.chkBoxExternalRoom = new System.Windows.Forms.CheckBox();
            this.callerCalleeCombo = new DevExpress.XtraEditors.ComboBoxEdit();
            this.equipmentCombo = new DevExpress.XtraEditors.ComboBoxEdit();
            this.MediaTypeCombo = new DevExpress.XtraEditors.ComboBoxEdit();
            this.protocolCombo = new DevExpress.XtraEditors.ComboBoxEdit();
            this.connectionCombo = new DevExpress.XtraEditors.ComboBoxEdit();
            this.addressTypesCombo = new DevExpress.XtraEditors.ComboBoxEdit();
            this.profileCombo = new DevExpress.XtraEditors.ComboBoxEdit();
            this.mcuCombo = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lineRateCombo = new DevExpress.XtraEditors.ComboBoxEdit();
            this.url = new DevExpress.XtraEditors.TextEdit();
            this.apiPort = new DevExpress.XtraEditors.TextEdit();
            this.addressPhone = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.MCUControl = new DevExpress.XtraLayout.LayoutControlItem();
            this.addressPhoneItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.ProfileControl = new DevExpress.XtraLayout.LayoutControlItem();
            this.addressTypeItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.connectionItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.CallerCalleeControl = new DevExpress.XtraLayout.LayoutControlItem();
            this.EquipmentControl = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.urlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.protocolItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.MediaTypeControl = new DevExpress.XtraLayout.LayoutControlItem();
            this.EndPtName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lineRateItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.apiPortItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.roomInfoGroupBox = new System.Windows.Forms.GroupBox();
            this.userInfoGroupBox = new System.Windows.Forms.GroupBox();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.userFirstName = new DevExpress.XtraEditors.TextEdit();
            this.userEmail = new DevExpress.XtraEditors.TextEdit();
            this.userLastName = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.userLastNameItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.userEmailItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.userFirstNameItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).BeginInit();
            this.ContentPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEdnpt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.callerCalleeCombo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentCombo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MediaTypeCombo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.protocolCombo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.connectionCombo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressTypesCombo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.profileCombo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mcuCombo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lineRateCombo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.url.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.apiPort.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressPhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MCUControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressPhoneItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProfileControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressTypeItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.connectionItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CallerCalleeControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EquipmentControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.urlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.protocolItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MediaTypeControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndPtName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lineRateItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.apiPortItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            this.roomInfoGroupBox.SuspendLayout();
            this.userInfoGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userFirstName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userLastName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userLastNameItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userEmailItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userFirstNameItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            this.SuspendLayout();
            // 
            // ContentPanel
            // 
            this.ContentPanel.Controls.Add(this.layoutControl1);
            this.ContentPanel.Size = new System.Drawing.Size(837, 290);
            // 
            // txtEdnpt
            // 
            this.txtEdnpt.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.txtEdnpt.Location = new System.Drawing.Point(532, 98);
            this.txtEdnpt.Name = "txtEdnpt";
            this.txtEdnpt.Properties.NullValuePromptShowForEmptyValue = true;
            this.txtEdnpt.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtEdnpt.Size = new System.Drawing.Size(293, 20);
            this.txtEdnpt.StyleController = this.layoutControl2;
            this.txtEdnpt.TabIndex = 18;
            this.txtEdnpt.EditValueChanged += new System.EventHandler(this.txtEdnpt_EditValueChanged);
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.chkBoxExternalRoom);
            this.layoutControl2.Controls.Add(this.txtEdnpt);
            this.layoutControl2.Controls.Add(this.callerCalleeCombo);
            this.layoutControl2.Controls.Add(this.equipmentCombo);
            this.layoutControl2.Controls.Add(this.MediaTypeCombo);
            this.layoutControl2.Controls.Add(this.protocolCombo);
            this.layoutControl2.Controls.Add(this.connectionCombo);
            this.layoutControl2.Controls.Add(this.addressTypesCombo);
            this.layoutControl2.Controls.Add(this.profileCombo);
            this.layoutControl2.Controls.Add(this.mcuCombo);
            this.layoutControl2.Controls.Add(this.lineRateCombo);
            this.layoutControl2.Controls.Add(this.url);
            this.layoutControl2.Controls.Add(this.apiPort);
            this.layoutControl2.Controls.Add(this.addressPhone);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(3, 17);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(539, 282, 250, 350);
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(827, 214);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // chkBoxExternalRoom
            // 
            this.chkBoxExternalRoom.Location = new System.Drawing.Point(2, 192);
            this.chkBoxExternalRoom.Name = "chkBoxExternalRoom";
            this.chkBoxExternalRoom.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.chkBoxExternalRoom.Size = new System.Drawing.Size(823, 20);
            this.chkBoxExternalRoom.TabIndex = 1;
            this.chkBoxExternalRoom.Text = "Save external room as guest location";
            this.chkBoxExternalRoom.UseVisualStyleBackColor = true;
            this.chkBoxExternalRoom.CheckedChanged += new System.EventHandler(this.chkBoxExternalRoom_CheckedChanged);
            // 
            // callerCalleeCombo
            // 
            this.callerCalleeCombo.Location = new System.Drawing.Point(713, 2);
            this.callerCalleeCombo.Name = "callerCalleeCombo";
            this.callerCalleeCombo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.callerCalleeCombo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.callerCalleeCombo.Size = new System.Drawing.Size(112, 20);
            this.callerCalleeCombo.StyleController = this.layoutControl2;
            this.callerCalleeCombo.TabIndex = 17;
            // 
            // equipmentCombo
            // 
            this.equipmentCombo.Location = new System.Drawing.Point(532, 50);
            this.equipmentCombo.Name = "equipmentCombo";
            this.equipmentCombo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.equipmentCombo.Properties.NullText = "Please select...";
            this.equipmentCombo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.equipmentCombo.Size = new System.Drawing.Size(293, 20);
            this.equipmentCombo.StyleController = this.layoutControl2;
            this.equipmentCombo.TabIndex = 16;
            // 
            // MediaTypeCombo
            // 
            this.MediaTypeCombo.Location = new System.Drawing.Point(532, 74);
            this.MediaTypeCombo.Name = "MediaTypeCombo";
            this.MediaTypeCombo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MediaTypeCombo.Properties.NullText = "Please select...";
            this.MediaTypeCombo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.MediaTypeCombo.Size = new System.Drawing.Size(293, 20);
            this.MediaTypeCombo.StyleController = this.layoutControl2;
            this.MediaTypeCombo.TabIndex = 17;
            //this.MediaTypeCombo.SelectedIndexChanged += new System.EventHandler(this.Connection_EditValueChanged);
            // 
            // protocolCombo
            // 
            this.protocolCombo.Location = new System.Drawing.Point(98, 74);
            this.protocolCombo.Name = "protocolCombo";
            this.protocolCombo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.protocolCombo.Properties.NullText = "Please select...";
            this.protocolCombo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.protocolCombo.Size = new System.Drawing.Size(336, 20);
            this.protocolCombo.StyleController = this.layoutControl2;
            this.protocolCombo.TabIndex = 14;
            // 
            // connectionCombo
            // 
            this.connectionCombo.Location = new System.Drawing.Point(98, 50);
            this.connectionCombo.Name = "connectionCombo";
            this.connectionCombo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.connectionCombo.Properties.NullText = "Please select...";
            this.connectionCombo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.connectionCombo.Size = new System.Drawing.Size(336, 20);
            this.connectionCombo.StyleController = this.layoutControl2;
            this.connectionCombo.TabIndex = 13;
            // 
            // addressTypesCombo
            // 
            this.addressTypesCombo.Location = new System.Drawing.Point(98, 26);
            this.addressTypesCombo.Name = "addressTypesCombo";
            this.addressTypesCombo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.addressTypesCombo.Properties.NullText = "Please select...";
            this.addressTypesCombo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.addressTypesCombo.Size = new System.Drawing.Size(336, 20);
            this.addressTypesCombo.StyleController = this.layoutControl2;
            this.addressTypesCombo.TabIndex = 12;
            // 
            // profileCombo
            // 
            this.profileCombo.Location = new System.Drawing.Point(534, 26);
            this.profileCombo.Name = "profileCombo";
            this.profileCombo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.profileCombo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.profileCombo.Size = new System.Drawing.Size(291, 20);
            this.profileCombo.StyleController = this.layoutControl2;
            this.profileCombo.TabIndex = 11;
            // 
            // mcuCombo
            // 
            this.mcuCombo.Location = new System.Drawing.Point(534, 2);
            this.mcuCombo.Name = "mcuCombo";
            this.mcuCombo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.mcuCombo.Properties.NullText = "Please select...";
            this.mcuCombo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.mcuCombo.Size = new System.Drawing.Size(79, 20);
            this.mcuCombo.StyleController = this.layoutControl2;
            this.mcuCombo.TabIndex = 9;
            this.mcuCombo.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.mcuCombo_EditValueChanging);
            // 
            // lineRateCombo
            // 
            this.lineRateCombo.Location = new System.Drawing.Point(98, 98);
            this.lineRateCombo.Name = "lineRateCombo";
            this.lineRateCombo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lineRateCombo.Properties.NullText = "Please select...";
            this.lineRateCombo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.lineRateCombo.Size = new System.Drawing.Size(336, 20);
            this.lineRateCombo.StyleController = this.layoutControl2;
            this.lineRateCombo.TabIndex = 8;
            // 
            // url
            // 
            this.url.Location = new System.Drawing.Point(98, 148);
            this.url.Name = "url";
            this.url.Size = new System.Drawing.Size(336, 20);
            this.url.StyleController = this.layoutControl2;
            this.url.TabIndex = 6;
            // 
            // apiPort
            // 
            this.apiPort.Location = new System.Drawing.Point(98, 122);
            this.apiPort.Name = "apiPort";
            this.apiPort.Size = new System.Drawing.Size(336, 20);
            this.apiPort.StyleController = this.layoutControl2;
            this.apiPort.TabIndex = 5;
            // 
            // addressPhone
            // 
            this.addressPhone.Location = new System.Drawing.Point(98, 2);
            this.addressPhone.Name = "addressPhone";
            this.addressPhone.Properties.NullValuePromptShowForEmptyValue = true;
            this.addressPhone.Size = new System.Drawing.Size(336, 20);
            this.addressPhone.StyleController = this.layoutControl2;
            this.addressPhone.TabIndex = 4;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.MCUControl,
            this.addressPhoneItem,
            this.ProfileControl,
            this.addressTypeItem,
            this.connectionItem,
            this.CallerCalleeControl,
            this.EquipmentControl,
            this.layoutControlItem4,
            this.urlItem,
            this.protocolItem,
            this.MediaTypeControl,
            this.EndPtName,
            this.emptySpaceItem2,
            this.lineRateItem,
            this.apiPortItem,
            this.emptySpaceItem1,
            this.emptySpaceItem3});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(827, 214);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // MCUControl
            // 
            this.MCUControl.Control = this.mcuCombo;
            this.MCUControl.CustomizationFormText = "MCU:";
            this.MCUControl.Location = new System.Drawing.Point(436, 0);
            this.MCUControl.MinSize = new System.Drawing.Size(143, 24);
            this.MCUControl.Name = "MCUControl";
            this.MCUControl.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 2, 2, 2);
            this.MCUControl.Size = new System.Drawing.Size(179, 24);
            this.MCUControl.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.MCUControl.Text = "MCU:";
            this.MCUControl.TextSize = new System.Drawing.Size(85, 13);
            // 
            // addressPhoneItem
            // 
            this.addressPhoneItem.Control = this.addressPhone;
            this.addressPhoneItem.CustomizationFormText = "Address/Phone:";
            this.addressPhoneItem.Location = new System.Drawing.Point(0, 0);
            this.addressPhoneItem.MinSize = new System.Drawing.Size(143, 24);
            this.addressPhoneItem.Name = "addressPhoneItem";
            this.addressPhoneItem.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 2, 2, 2);
            this.addressPhoneItem.Size = new System.Drawing.Size(436, 24);
            this.addressPhoneItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.addressPhoneItem.Text = "Address/Phone:";
            this.addressPhoneItem.TextSize = new System.Drawing.Size(85, 13);
            // 
            // ProfileControl
            // 
            this.ProfileControl.Control = this.profileCombo;
            this.ProfileControl.CustomizationFormText = "Equipment:";
            this.ProfileControl.Location = new System.Drawing.Point(436, 24);
            this.ProfileControl.MinSize = new System.Drawing.Size(10, 10);
            this.ProfileControl.Name = "ProfileControl";
            this.ProfileControl.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 2, 2, 2);
            this.ProfileControl.Size = new System.Drawing.Size(391, 24);
            this.ProfileControl.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ProfileControl.Text = "Profile:";
            this.ProfileControl.TextSize = new System.Drawing.Size(85, 13);
            // 
            // addressTypeItem
            // 
            this.addressTypeItem.Control = this.addressTypesCombo;
            this.addressTypeItem.CustomizationFormText = "Address Type:";
            this.addressTypeItem.Location = new System.Drawing.Point(0, 24);
            this.addressTypeItem.MinSize = new System.Drawing.Size(143, 24);
            this.addressTypeItem.Name = "addressTypeItem";
            this.addressTypeItem.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 2, 2, 2);
            this.addressTypeItem.Size = new System.Drawing.Size(436, 24);
            this.addressTypeItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.addressTypeItem.Text = "Address Type:";
            this.addressTypeItem.TextSize = new System.Drawing.Size(85, 13);
            // 
            // connectionItem
            // 
            this.connectionItem.Control = this.connectionCombo;
            this.connectionItem.CustomizationFormText = "Connection Type:";
            this.connectionItem.Location = new System.Drawing.Point(0, 48);
            this.connectionItem.MinSize = new System.Drawing.Size(143, 24);
            this.connectionItem.Name = "connectionItem";
            this.connectionItem.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 2, 2, 2);
            this.connectionItem.Size = new System.Drawing.Size(436, 24);
            this.connectionItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.connectionItem.Text = "Connection Type:";
            this.connectionItem.TextSize = new System.Drawing.Size(85, 13);
            // 
            // CallerCalleeControl
            // 
            this.CallerCalleeControl.Control = this.callerCalleeCombo;
            this.CallerCalleeControl.CustomizationFormText = "Caller/Callee:";
            this.CallerCalleeControl.Location = new System.Drawing.Point(615, 0);
            this.CallerCalleeControl.MaxSize = new System.Drawing.Size(0, 24);
            this.CallerCalleeControl.MinSize = new System.Drawing.Size(151, 24);
            this.CallerCalleeControl.Name = "CallerCalleeControl";
            this.CallerCalleeControl.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 2, 2, 2);
            this.CallerCalleeControl.Size = new System.Drawing.Size(212, 24);
            this.CallerCalleeControl.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.CallerCalleeControl.Text = "Caller/Callee:";
            this.CallerCalleeControl.TextSize = new System.Drawing.Size(85, 13);
            // 
            // EquipmentControl
            // 
            this.EquipmentControl.Control = this.equipmentCombo;
            this.EquipmentControl.CustomizationFormText = "Equipment:";
            this.EquipmentControl.Location = new System.Drawing.Point(436, 48);
            this.EquipmentControl.MaxSize = new System.Drawing.Size(0, 24);
            this.EquipmentControl.MinSize = new System.Drawing.Size(157, 24);
            this.EquipmentControl.Name = "EquipmentControl";
            this.EquipmentControl.Padding = new DevExpress.XtraLayout.Utils.Padding(8, 2, 2, 2);
            this.EquipmentControl.Size = new System.Drawing.Size(391, 24);
            this.EquipmentControl.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.EquipmentControl.Text = "Equipment:";
            this.EquipmentControl.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.chkBoxExternalRoom;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 190);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(827, 24);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // urlItem
            // 
            this.urlItem.Control = this.url;
            this.urlItem.CustomizationFormText = "URL:";
            this.urlItem.Location = new System.Drawing.Point(0, 144);
            this.urlItem.MinSize = new System.Drawing.Size(14, 24);
            this.urlItem.Name = "urlItem";
            this.urlItem.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 2, 4, 0);
            this.urlItem.Size = new System.Drawing.Size(436, 25);
            this.urlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.urlItem.Text = "URL:";
            this.urlItem.TextSize = new System.Drawing.Size(85, 13);
            // 
            // protocolItem
            // 
            this.protocolItem.Control = this.protocolCombo;
            this.protocolItem.CustomizationFormText = "Protocol:";
            this.protocolItem.Location = new System.Drawing.Point(0, 72);
            this.protocolItem.MinSize = new System.Drawing.Size(143, 24);
            this.protocolItem.Name = "protocolItem";
            this.protocolItem.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 2, 2, 2);
            this.protocolItem.Size = new System.Drawing.Size(436, 24);
            this.protocolItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.protocolItem.Text = "Protocol:";
            this.protocolItem.TextSize = new System.Drawing.Size(85, 13);
            // 
            // MediaTypeControl
            // 
            this.MediaTypeControl.Control = this.MediaTypeCombo;
            this.MediaTypeControl.CustomizationFormText = "Media Type:";
            this.MediaTypeControl.Location = new System.Drawing.Point(436, 72);
            this.MediaTypeControl.MaxSize = new System.Drawing.Size(0, 24);
            this.MediaTypeControl.MinSize = new System.Drawing.Size(157, 24);
            this.MediaTypeControl.Name = "MediaTypeControl";
            this.MediaTypeControl.Padding = new DevExpress.XtraLayout.Utils.Padding(8, 2, 2, 2);
            this.MediaTypeControl.Size = new System.Drawing.Size(391, 24);
            this.MediaTypeControl.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.MediaTypeControl.Text = "Media Type:";
            this.MediaTypeControl.TextSize = new System.Drawing.Size(85, 13);
            // 
            // EndPtName
            // 
            this.EndPtName.Control = this.txtEdnpt;
            this.EndPtName.CustomizationFormText = "Media Type:";
            this.EndPtName.Location = new System.Drawing.Point(436, 96);
            this.EndPtName.MaxSize = new System.Drawing.Size(0, 24);
            this.EndPtName.MinSize = new System.Drawing.Size(157, 24);
            this.EndPtName.Name = "EndPtName";
            this.EndPtName.Padding = new DevExpress.XtraLayout.Utils.Padding(8, 2, 2, 2);
            this.EndPtName.Size = new System.Drawing.Size(391, 24);
            this.EndPtName.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.EndPtName.Text = "End Point Name:";
            this.EndPtName.TextSize = new System.Drawing.Size(85, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(436, 120);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(104, 24);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(391, 24);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lineRateItem
            // 
            this.lineRateItem.Control = this.lineRateCombo;
            this.lineRateItem.CustomizationFormText = "Line Rate:";
            this.lineRateItem.Location = new System.Drawing.Point(0, 96);
            this.lineRateItem.MinSize = new System.Drawing.Size(143, 24);
            this.lineRateItem.Name = "lineRateItem";
            this.lineRateItem.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 2, 2, 2);
            this.lineRateItem.Size = new System.Drawing.Size(436, 24);
            this.lineRateItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lineRateItem.Text = "Line Rate:";
            this.lineRateItem.TextSize = new System.Drawing.Size(85, 13);
            // 
            // apiPortItem
            // 
            this.apiPortItem.Control = this.apiPort;
            this.apiPortItem.CustomizationFormText = "API Port:";
            this.apiPortItem.Location = new System.Drawing.Point(0, 120);
            this.apiPortItem.MinSize = new System.Drawing.Size(143, 24);
            this.apiPortItem.Name = "apiPortItem";
            this.apiPortItem.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 2, 2, 2);
            this.apiPortItem.Size = new System.Drawing.Size(436, 24);
            this.apiPortItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.apiPortItem.Text = "API Port:";
            this.apiPortItem.TextSize = new System.Drawing.Size(85, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(436, 144);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(391, 46);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 169);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(436, 21);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.roomInfoGroupBox);
            this.layoutControl1.Controls.Add(this.userInfoGroupBox);
            this.layoutControl1.Controls.Add(this.groupBox1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(837, 290);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "External User Info";
            // 
            // roomInfoGroupBox
            // 
            this.roomInfoGroupBox.Controls.Add(this.layoutControl2);
            this.roomInfoGroupBox.Location = new System.Drawing.Point(2, 54);
            this.roomInfoGroupBox.Name = "roomInfoGroupBox";
            this.roomInfoGroupBox.Size = new System.Drawing.Size(833, 234);
            this.roomInfoGroupBox.TabIndex = 6;
            this.roomInfoGroupBox.TabStop = false;
            this.roomInfoGroupBox.Text = "Room Info";
            // 
            // userInfoGroupBox
            // 
            this.userInfoGroupBox.Controls.Add(this.layoutControl3);
            this.userInfoGroupBox.Location = new System.Drawing.Point(2, 2);
            this.userInfoGroupBox.Name = "userInfoGroupBox";
            this.userInfoGroupBox.Size = new System.Drawing.Size(833, 48);
            this.userInfoGroupBox.TabIndex = 5;
            this.userInfoGroupBox.TabStop = false;
            this.userInfoGroupBox.Text = "User Info";
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.userFirstName);
            this.layoutControl3.Controls.Add(this.userEmail);
            this.layoutControl3.Controls.Add(this.userLastName);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(3, 17);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.layoutControl3.Root = this.layoutControlGroup3;
            this.layoutControl3.Size = new System.Drawing.Size(827, 28);
            this.layoutControl3.TabIndex = 0;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // userFirstName
            // 
            this.userFirstName.Location = new System.Drawing.Point(68, 2);
            this.userFirstName.Name = "userFirstName";
            this.userFirstName.Size = new System.Drawing.Size(139, 20);
            this.userFirstName.StyleController = this.layoutControl3;
            this.userFirstName.TabIndex = 8;
            // 
            // userEmail
            // 
            this.userEmail.Location = new System.Drawing.Point(541, 2);
            this.userEmail.Name = "userEmail";
            this.userEmail.Size = new System.Drawing.Size(276, 20);
            this.userEmail.StyleController = this.layoutControl3;
            this.userEmail.TabIndex = 7;
            // 
            // userLastName
            // 
            this.userLastName.Location = new System.Drawing.Point(279, 2);
            this.userLastName.Name = "userLastName";
            this.userLastName.Size = new System.Drawing.Size(190, 20);
            this.userLastName.StyleController = this.layoutControl3;
            this.userLastName.TabIndex = 6;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.userLastNameItem,
            this.userEmailItem,
            this.userFirstNameItem});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(827, 28);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // userLastNameItem
            // 
            this.userLastNameItem.AppearanceItemCaption.Options.UseForeColor = true;
            this.userLastNameItem.AppearanceItemCaption.Options.UseTextOptions = true;
            this.userLastNameItem.Control = this.userLastName;
            this.userLastNameItem.CustomizationFormText = "Last Name:";
            this.userLastNameItem.Location = new System.Drawing.Point(209, 0);
            this.userLastNameItem.MinSize = new System.Drawing.Size(151, 24);
            this.userLastNameItem.Name = "userLastNameItem";
            this.userLastNameItem.Size = new System.Drawing.Size(262, 28);
            this.userLastNameItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.userLastNameItem.Spacing = new DevExpress.XtraLayout.Utils.Padding(10, 0, 0, 0);
            this.userLastNameItem.Text = "Last Name:";
            this.userLastNameItem.TextSize = new System.Drawing.Size(55, 13);
            // 
            // userEmailItem
            // 
            this.userEmailItem.Control = this.userEmail;
            this.userEmailItem.CustomizationFormText = "E-mail:";
            this.userEmailItem.Location = new System.Drawing.Point(471, 0);
            this.userEmailItem.MinSize = new System.Drawing.Size(1, 1);
            this.userEmailItem.Name = "userEmailItem";
            this.userEmailItem.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 10, 2, 2);
            this.userEmailItem.Size = new System.Drawing.Size(356, 28);
            this.userEmailItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.userEmailItem.Spacing = new DevExpress.XtraLayout.Utils.Padding(10, 0, 0, 0);
            this.userEmailItem.Text = "E-mail:";
            this.userEmailItem.TextSize = new System.Drawing.Size(55, 13);
            // 
            // userFirstNameItem
            // 
            this.userFirstNameItem.Control = this.userFirstName;
            this.userFirstNameItem.CustomizationFormText = "First Name:";
            this.userFirstNameItem.Location = new System.Drawing.Point(0, 0);
            this.userFirstNameItem.MinSize = new System.Drawing.Size(157, 24);
            this.userFirstNameItem.Name = "userFirstNameItem";
            this.userFirstNameItem.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 2, 2, 2);
            this.userFirstNameItem.Size = new System.Drawing.Size(209, 28);
            this.userFirstNameItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.userFirstNameItem.Text = "First Name:";
            this.userFirstNameItem.TextSize = new System.Drawing.Size(55, 13);
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(109, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(627, 314);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.groupBox1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(728, 318);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "External User Info";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(837, 290);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.userInfoGroupBox;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(104, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(837, 52);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.roomInfoGroupBox;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 52);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(104, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(837, 238);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.addressPhone;
            this.layoutControlItem5.CustomizationFormText = "Address/Phone:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(143, 24);
            this.layoutControlItem5.Name = "addressPhoneItem";
            this.layoutControlItem5.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 2, 2, 2);
            this.layoutControlItem5.Size = new System.Drawing.Size(406, 24);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "Address/Phone:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.addressPhone;
            this.layoutControlItem7.CustomizationFormText = "Address/Phone:";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(143, 24);
            this.layoutControlItem7.Name = "addressPhoneItem";
            this.layoutControlItem7.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 2, 2, 2);
            this.layoutControlItem7.Size = new System.Drawing.Size(406, 24);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "Address/Phone:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.txtEdnpt;
            this.layoutControlItem8.CustomizationFormText = "Address/Phone:";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(1, 1);
            this.layoutControlItem8.Name = "addressPhoneItem";
            this.layoutControlItem8.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 2, 2, 2);
            this.layoutControlItem8.Size = new System.Drawing.Size(406, 24);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "Address/Phone:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(85, 13);
            // 
            // ExternalRoomDlg
            // 
            this.ApplyEnabled = true;
            this.ApplyVisible = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(861, 346);
            this.Name = "ExternalRoomDlg";
            this.Text = "External Room";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ExternalRoomDlg_FormClosing);
            this.Load += new System.EventHandler(this.ExternalRoomDlg_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).EndInit();
            this.ContentPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtEdnpt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.callerCalleeCombo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentCombo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MediaTypeCombo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.protocolCombo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.connectionCombo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressTypesCombo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.profileCombo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mcuCombo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lineRateCombo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.url.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.apiPort.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressPhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MCUControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressPhoneItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProfileControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressTypeItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.connectionItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CallerCalleeControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EquipmentControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.urlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.protocolItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MediaTypeControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndPtName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lineRateItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.apiPortItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            this.roomInfoGroupBox.ResumeLayout(false);
            this.userInfoGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.userFirstName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userLastName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userLastNameItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userEmailItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userFirstNameItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private DevExpress.XtraLayout.LayoutControl layoutControl1;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private System.Windows.Forms.GroupBox groupBox1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
		private System.Windows.Forms.GroupBox roomInfoGroupBox;
		private System.Windows.Forms.GroupBox userInfoGroupBox;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
		private DevExpress.XtraLayout.LayoutControl layoutControl2;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
		private DevExpress.XtraLayout.LayoutControl layoutControl3;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
		private DevExpress.XtraEditors.TextEdit userEmail;
		private DevExpress.XtraEditors.TextEdit userLastName;
		private DevExpress.XtraLayout.LayoutControlItem userLastNameItem;
		private DevExpress.XtraLayout.LayoutControlItem userEmailItem;
		private DevExpress.XtraEditors.TextEdit url;
		private DevExpress.XtraEditors.TextEdit apiPort;
        private DevExpress.XtraEditors.TextEdit addressPhone;
		private DevExpress.XtraLayout.LayoutControlItem apiPortItem;
		private DevExpress.XtraLayout.LayoutControlItem urlItem;
		private DevExpress.XtraEditors.ComboBoxEdit lineRateCombo;
		private DevExpress.XtraLayout.LayoutControlItem lineRateItem;
		//private MyVrm.Outlook.WinForms.EnumComboBoxEdit protocolCombo;
		private DevExpress.XtraEditors.ComboBoxEdit protocolCombo;
		private DevExpress.XtraEditors.ComboBoxEdit connectionCombo;
		private DevExpress.XtraEditors.ComboBoxEdit addressTypesCombo;
		private DevExpress.XtraEditors.ComboBoxEdit profileCombo;
		private DevExpress.XtraEditors.ComboBoxEdit mcuCombo;
		private DevExpress.XtraLayout.LayoutControlItem MCUControl;
		private DevExpress.XtraLayout.LayoutControlItem ProfileControl;
		private DevExpress.XtraLayout.LayoutControlItem connectionItem;
		private DevExpress.XtraLayout.LayoutControlItem protocolItem;
		private DevExpress.XtraLayout.LayoutControlItem addressTypeItem;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
		private DevExpress.XtraEditors.ComboBoxEdit equipmentCombo;
        private DevExpress.XtraLayout.LayoutControlItem EquipmentControl;
        private DevExpress.XtraEditors.ComboBoxEdit MediaTypeCombo;
        private DevExpress.XtraLayout.LayoutControlItem MediaTypeControl;
		private DevExpress.XtraEditors.TextEdit userFirstName;
		private DevExpress.XtraLayout.LayoutControlItem userFirstNameItem;
		private DevExpress.XtraEditors.ComboBoxEdit callerCalleeCombo;
		private DevExpress.XtraLayout.LayoutControlItem CallerCalleeControl;
        private System.Windows.Forms.CheckBox chkBoxExternalRoom;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.TextEdit txtEdnpt;
        private DevExpress.XtraLayout.LayoutControlItem addressPhoneItem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem EndPtName;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
	}
}
