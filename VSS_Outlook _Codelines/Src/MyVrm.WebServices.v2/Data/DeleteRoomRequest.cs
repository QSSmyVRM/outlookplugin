﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace MyVrm.WebServices.Data
{
    internal sealed class DeleteRoomRequest : ServiceRequestBase<DeleteRoomResponse>
    {
        internal DeleteRoomRequest(MyVrmService service)
            : base(service)
        {
        }

        internal RoomId RoomId { get; set; }

        #region Overrides of ServiceRequestBase

        internal override string GetXmlElementName()
        {
            return "login";
        }

        internal override string GetCommandName()
        {
            return "DeleteRoom";
        }

        internal override string GetResponseXmlElementName()
        {
            return "success";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            writer.WriteElementValue(XmlNamespace.NotSpecified, "userID", Service.UserId);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "roomID", RoomId.Id);
        }

        #endregion

    }
}
