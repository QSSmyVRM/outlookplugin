﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.IO;
using System.Net;
using MyVrm.WebServices.Vrmws;

namespace MyVrm.WebServices.Data
{
    internal abstract class ServiceRequestBase<TServiceResponse> where TServiceResponse : ServiceResponse, new()
    {
        private const string ErrorElementName = "error";
        private const string ErrorCodeElementName = "errorCode";
        private const string ErrorMessageElementName = "message";
        private const string ResponseRootElementName = "myVRM";
        private const string RequestRootElementName = "myVRM";
        private const string LoginElementName = "login";
        private const string PasswordElementName = "password";
        private const string ClientVersionElementName = "version";
        private const string ClientTypeElementName = "client";
        private const string CommandNameElementName = "commandname";
        private const string CommandElementName = "command";
        private const string ErrorLevelElementName = "level";

        private readonly MyVrmService _service;

        internal ServiceRequestBase(MyVrmService service)
        {
            _service = service;
        }


        internal MyVrmService Service
        {
            get { return _service; }
        }

        internal UserId UserId { get; set; }
        internal OrganizationId OrganizationId { get; set; }

        internal abstract string GetXmlElementName();
        
        internal abstract string GetCommandName();

        internal abstract string GetResponseXmlElementName();

        internal TServiceResponse Execute()
        {
            return InternalExecute();
        }

        internal TServiceResponse InternalExecute()
        {
            Validate();
            using (var webService = new VRMNewWebService())
            {
                webService.Url = Service.FullUrl;
                // Pass windows credentials
                if (Service.AuthenticationMode == AuthenticationMode.Windows)
                {
                    webService.UseDefaultCredentials = Service.UseDefaultCredential;
                    webService.Credentials = Service.UseDefaultCredential ? CredentialCache.DefaultNetworkCredentials : Service.Credential;
                }
                using (var stream = new MemoryStream())
                {
                    using (var writer = new MyVrmXmlWriter(Service, stream))
                    {
                        WriteToXml(writer);
                        writer.Flush();
                    }
                    string request;
                    using(var streamReader = new StreamReader(stream))
                    {
                        stream.Position = 0;
                        request = streamReader.ReadToEnd();
                    }
                    string response;
                    try
                    {
                        //MyVrmService.TraceSource.TraceInformation("Invoke web service command: {0}", GetCommandName());
						MyVrmService.TraceSource.TraceInfoEx("Invoke web service command: {0}", GetCommandName());
                        response = webService.InvokeWebservice(request);
                        //MyVrmService.TraceSource.TraceInformation("Invoke web service command {0} succeeded", GetCommandName());
						MyVrmService.TraceSource.TraceInfoEx("Invoke web service command {0} succeeded", GetCommandName());
                    }
                    catch (WebException exception)
                    {
                        //MyVrmService.TraceSource.TraceError("Invoke web service command {0} failed. {1}", GetCommandName(), exception.Message);
						MyVrmService.TraceSource.TraceError("{2} Invoke web service command {0} failed. {1}", GetCommandName(), exception.Message, DateTime.Now.ToString("dd.mm.yyyy HH:mm:ss:ffff"));
                        throw new ServiceRequestException(string.Format(Strings.ServiceRequestFailed, exception.Message), exception);
                    }
                    try
                    {
                        return ReadResponse(response);
                    }
                    catch(MyVrmServiceException exception)
                    {
                        //MyVrmService.TraceSource.TraceError("Invoke web service command {0} failed. {1}", GetCommandName(), exception);
						MyVrmService.TraceSource.TraceError("{2} Invoke web service command {0} failed. {1}", GetCommandName(), exception, DateTime.Now.ToString("dd.mm.yyyy HH:mm:ss:ffff"));
                        throw;
                    }
                }
            }
        }

        internal virtual TServiceResponse ParseResponse(MyVrmServiceXmlReader reader)
        {
            var response = new TServiceResponse();
            response.LoadFromXml(reader, GetResponseXmlElementName());
            return response;
        }

        internal virtual void Validate()
        {
            Service.Validate();
        }

        internal void WriteBodyToXml(MyVrmXmlWriter writer)
        {
            writer.WriteStartElement(XmlNamespace.NotSpecified, GetXmlElementName());
            WriteElementsToXml(writer);
            writer.WriteEndElement();
        }

        internal abstract void WriteElementsToXml(MyVrmXmlWriter writer);

        internal void WriteToXml(MyVrmXmlWriter writer)
        {
            writer.WriteStartElement(XmlNamespace.NotSpecified, RequestRootElementName);
            {
                // Add login and password for myVRM authentication mode only
                if (Service.AuthenticationMode == AuthenticationMode.Custom)
                {
                    writer.WriteElementValue(XmlNamespace.NotSpecified, LoginElementName, Service.Credential.UserName);
                    writer.WriteElementValue(XmlNamespace.NotSpecified, PasswordElementName, Service.Credential.Password);
                }
                if (Service.ClientVersion == null)
                {
                    throw new ServiceRequestException(Strings.ClientVersionNotSpecifiedInRequest);
                }
                writer.WriteElementValue(XmlNamespace.NotSpecified, ClientVersionElementName, Service.ClientVersion);
                if (Service.ClientType == null)
                {
                    throw new ServiceRequestException(Strings.ClientTypeNotSpecifiedInRequest);
                }
                writer.WriteElementValue(XmlNamespace.NotSpecified, ClientTypeElementName, Service.ClientType);
                writer.WriteElementValue(XmlNamespace.NotSpecified, CommandNameElementName, GetCommandName());
                writer.WriteStartElement(XmlNamespace.NotSpecified, CommandElementName);
                WriteBodyToXml(writer);
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
        }

        /*private*/internal TServiceResponse ReadResponse(string response)
        {
            using(var stream = new MemoryStream())
            {
                using(var streamWriter = new StreamWriter(stream))
                {
                    streamWriter.Write(response);
                    streamWriter.Flush();
                    stream.Position = 0;
                    return ReadResponse(new MyVrmServiceXmlReader(stream, Service));
                }
            }
        }

        private TServiceResponse ReadResponse(MyVrmServiceXmlReader reader)
        {
            reader.Read();
            if (reader.IsStartElement(XmlNamespace.NotSpecified, ErrorElementName))
            {
                throw ParseError(reader, ErrorElementName);
            }
            if (!reader.IsStartElement(XmlNamespace.NotSpecified, ResponseRootElementName))
            {
                throw new ServiceRequestException(string.Format(Strings.UnexpectedResponseElement,
                                                                new object[] { ResponseRootElementName, reader.LocalName }));
            }
            reader.ReadElementValue(XmlNamespace.NotSpecified, ClientVersionElementName);
            reader.ReadElementValue(XmlNamespace.NotSpecified, ClientTypeElementName);
            var commandName = reader.ReadElementValue(XmlNamespace.NotSpecified, CommandNameElementName);
            if (commandName != GetCommandName())
            {
                throw new ServiceRequestException(string.Format(Strings.UnexpectedMyVrmCommand,
                                                                new object[] {GetCommandName(), commandName}));
            }
            reader.ReadStartElement(XmlNamespace.NotSpecified, CommandElementName);
            reader.Read();
            if (reader.IsStartElement(XmlNamespace.NotSpecified, ErrorElementName))
            {
                throw ParseError(reader, ErrorElementName);
            }
            if (!reader.IsStartElement(XmlNamespace.NotSpecified, GetResponseXmlElementName()))
            {
                throw new ServiceRequestException(string.Format(Strings.UnexpectedResponseElement,
                                                                new object[]
                                                                    {GetResponseXmlElementName(), reader.LocalName}));
            }
            var obj = ParseResponse(reader);
            reader.ReadEndElementIfNecessary(XmlNamespace.NotSpecified, GetResponseXmlElementName());
            reader.ReadEndElement(XmlNamespace.NotSpecified, CommandElementName);
            reader.ReadEndElement(XmlNamespace.NotSpecified, ResponseRootElementName);
            return obj;
        }

        private static MyVrmServiceException ParseError(MyVrmXmlReader reader, string errorElementName)
        {
            var errorCode = 0;
            var errorMessage = String.Empty;
            var errorLevel = MyVrmServiceErrorLevel.Unknown;

            do
            {
                reader.Read();
                switch (reader.LocalName)
                {
                    case ErrorCodeElementName:
                        errorCode = reader.ReadElementValue<int>(XmlNamespace.NotSpecified, ErrorCodeElementName);
                        break;
                    case ErrorMessageElementName:
                        errorMessage = reader.ReadElementValue(XmlNamespace.NotSpecified, ErrorMessageElementName);
                        break;
                    case ErrorLevelElementName:
                        errorLevel = Utilities.ErrorLevelFromString(reader.ReadElementValue(XmlNamespace.NotSpecified,
                                                                                            ErrorLevelElementName));
                        break;
                    default:
                        reader.SkipCurrentElement();
                        break;
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, errorElementName));
            return MyVrmServiceException.CreateException(errorCode, errorLevel, errorMessage);
        }
    }
}
