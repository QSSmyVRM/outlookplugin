using System.Runtime.InteropServices;

namespace MyVrm.Outlook
{
    [ComVisible(true)]
    [Guid("6BC7303A-ED39-4798-BCF9-0A4B83E9ED5B")]
    public interface ICustomFormControlHost
    {
        object CustomFormControl { get; set; }
        void OnStartup();
        void OnShutdown();
    }
}