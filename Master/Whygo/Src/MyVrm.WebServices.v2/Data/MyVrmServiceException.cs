﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Runtime.Serialization;
using System.Text;
using MyVrm.Common;

namespace MyVrm.WebServices.Data
{
    public enum MyVrmServiceErrorLevel
    {
        [LocalizedDescription("ErrorLevelUnknown", typeof(Strings))]
        Unknown,
        [LocalizedDescription("ErrorLevelUser", typeof(Strings))]
        User,
        [LocalizedDescription("ErrorLevelSystem", typeof(Strings))]
        System,
        [LocalizedDescription("ErrorLevelCustom", typeof(Strings))]
        Custom
    }
    /// <summary>
    /// Represents an error that occurs when a service fails remotely.
    /// </summary>
    [Serializable]
    public class MyVrmServiceException : Exception
    {
        private readonly int _errorCode;
        private readonly MyVrmServiceErrorLevel _errorLevel;

        public MyVrmServiceException(int errorCode, MyVrmServiceErrorLevel errorLevel, string message) :
            base(FormatMessage(message, errorCode, errorLevel))
        {
            _errorCode = errorCode;
            _errorLevel = errorLevel;
        }

        public MyVrmServiceException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public MyVrmServiceErrorLevel ErrorLevel
        {
            get { return _errorLevel; }
        }

        public int ErrorCode
        {
            get { return _errorCode; }
        }

        internal static MyVrmServiceException CreateException(int errorCode, MyVrmServiceErrorLevel errorLevel, string errorMessage)
        {
            switch (errorCode)
            {
                case 205:
                {
                    return new UserIsNotRegisteredException(errorCode, errorLevel, errorMessage);
                }
                case 241:
                {
                    return new RoomConflictException(errorCode, errorLevel, errorMessage);
                }
                case 253:
                {
                    return new OutOfSystemAvailabilityException(errorCode, errorLevel, errorMessage);
                }
                case 254:
                {
                    return new AccountIsLockedException(errorCode, errorLevel, errorMessage);
                }
                default:
                    return new MyVrmServiceException(errorCode, errorLevel, errorMessage);
            }
        }

        private static string FormatMessage(string message, int errorCode, MyVrmServiceErrorLevel errorLevel)
        {
            var sb = new StringBuilder(message);
            sb.AppendLine();
            sb.AppendFormat(Strings.MyVrmServiceExceptionErrorCodeFormat, errorCode);
            sb.AppendLine();
            sb.AppendFormat(Strings.MyVrmServiceExceptionErrorLevelFormat, errorLevel);
            return sb.ToString();
        }
    }
    /// <summary>
    /// The exception that is thrown when a user is not registered in VRM.
    /// </summary>
    [Serializable]
    public class UserIsNotRegisteredException : MyVrmServiceException
    {
        public UserIsNotRegisteredException(int errorCode, MyVrmServiceErrorLevel errorLevel, string message) : base(errorCode, errorLevel, message)
        {
        }

        public UserIsNotRegisteredException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
    /// <summary>
    /// The exception that is thrown when an VRM account is locked.
    /// </summary>
    [Serializable]
    public class AccountIsLockedException : MyVrmServiceException
    {
        public AccountIsLockedException(int errorCode, MyVrmServiceErrorLevel errorLevel, string message) : base(errorCode, errorLevel, message)
        {
        }

        public AccountIsLockedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
    /// <summary>
    /// The exception that is thrown when one or more selected rooms have alread been booked.
    /// </summary>
    [Serializable]
    public class RoomConflictException : MyVrmServiceException
    {
        public RoomConflictException(int errorCode, MyVrmServiceErrorLevel errorLevel, string message) : base(errorCode, errorLevel, message)
        {
        }

        public RoomConflictException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
    /// <summary>
    /// The exception that is thrown when VRM system is unavailable during the chosen date and time.
    /// </summary>
    public class OutOfSystemAvailabilityException : MyVrmServiceException
    {
        public OutOfSystemAvailabilityException(int errorCode, MyVrmServiceErrorLevel errorLevel, string message) : base(errorCode, errorLevel, message)
        {
        }

        public OutOfSystemAvailabilityException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
