﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using Microsoft.VisualStudio.Tools.Applications.Runtime;
using MyVrm.DataImport.Excel;
using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;

namespace MyVrmExcelImportTemplate
{
    public partial class EndpointSheet
    {
        private EndpointTable _endpointTable;

        private void EndpointSheet_Startup(object sender, System.EventArgs e)
        {
            _endpointTable = new EndpointTable(endpointTable, Globals.ThisWorkbook.Service, Globals.ThisWorkbook.ValidationListManager);
            Globals.ThisWorkbook.RegisterTable(endpointTable, _endpointTable);
        }

        private void EndpointSheet_Shutdown(object sender, System.EventArgs e)
        {
        }

        #region VSTO Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
	    private void InternalStartup()
        {
            this.Shutdown += new System.EventHandler(this.EndpointSheet_Shutdown);
            this.Startup += new System.EventHandler(this.EndpointSheet_Startup);

        }
        
        #endregion
    }
}
