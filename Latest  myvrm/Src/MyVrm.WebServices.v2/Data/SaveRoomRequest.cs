﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace MyVrm.WebServices.Data
{
    internal class SaveRoomRequest : ServiceRequestBase<SaveRoomResponse>
    {
        internal SaveRoomRequest(MyVrmService service) : base(service)
        {
        }

        internal Room Room { get; set; }

        #region Overrides of ServiceRequestBase

        internal override string GetXmlElementName()
        {
            return "SetRoomProfile";
        }

        internal override string GetCommandName()
        {
            return "SetRoomProfile";
        }

        internal override string GetResponseXmlElementName()
        {
            return "success";
        }


        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            OrganizationId.WriteToXml(writer);
            UserId.WriteToXml(writer, "UserID");
            Room.WriteToXmlPlain(writer);
        }

        #endregion
    }
}
