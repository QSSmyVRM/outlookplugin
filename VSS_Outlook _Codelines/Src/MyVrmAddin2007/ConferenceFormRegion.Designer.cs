﻿using System.Drawing;
using MyVrm.Outlook;
using MyVrm.Outlook.WinForms.Conference;

namespace MyVrmAddin2007
{
    [System.ComponentModel.ToolboxItemAttribute(false)]
    partial class ConferenceFormRegion : Microsoft.Office.Tools.Outlook.FormRegionBase
    {
        public ConferenceFormRegion(Microsoft.Office.Interop.Outlook.FormRegion formRegion)
            : base(Globals.Factory,formRegion)
        {
            this.InitializeComponent();
        }

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
			//if (!conferenceControl.IsDisposed)
			//{
			//    conferenceControl.Dispose();
			//}
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.conferenceControl = new MyVrm.Outlook.WinForms.Conference.ConferenceControl();
            this.SuspendLayout();
            // 
            // conferenceControl
            // 
            this.conferenceControl.AutoSize = true;
            this.conferenceControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.conferenceControl.Location = new System.Drawing.Point(0, 0);
            this.conferenceControl.Name = "conferenceControl";
            this.conferenceControl.Size = new System.Drawing.Size(751, 476);
            this.conferenceControl.TabIndex = 0;
            // 
            // ConferenceFormRegion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.conferenceControl);
            this.Name = "ConferenceFormRegion";
            this.Size = new System.Drawing.Size(751, 476);
            this.FormRegionClosed += new System.EventHandler(this.ConferenceFormRegion_FormRegionClosed);
            this.FormRegionShowing += new System.EventHandler(this.ConferenceFormRegion_FormRegionShowing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        #region Form Region Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private static void InitializeManifest(Microsoft.Office.Tools.Outlook.FormRegionManifest manifest,
    Microsoft.Office.Tools.Outlook.Factory factory)
        {
			manifest.FormRegionName = Strings.Conference;
        	manifest.Icons.Page = (Image) MyVrmAddin.Logo65;//global::MyVrmAddin2007.Properties.Resources.Logo65x65;
            manifest.ShowReadingPane = false;

        }

        #endregion

        private ConferenceControl conferenceControl;


        public partial class ConferenceFormRegionFactory : Microsoft.Office.Tools.Outlook.IFormRegionFactory
        {
            public event Microsoft.Office.Tools.Outlook.FormRegionInitializingEventHandler FormRegionInitializing;

            private Microsoft.Office.Tools.Outlook.FormRegionManifest _Manifest;

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public ConferenceFormRegionFactory()
            {
                this._Manifest = Globals.Factory.CreateFormRegionManifest();
                ConferenceFormRegion.InitializeManifest(this._Manifest, Globals.Factory);
                this.FormRegionInitializing += new Microsoft.Office.Tools.Outlook.FormRegionInitializingEventHandler(this.ConferenceFormRegionFactory_FormRegionInitializing);
            }

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public Microsoft.Office.Tools.Outlook.FormRegionManifest Manifest
            {
                get
                {
                    return this._Manifest;
                }
            }

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            Microsoft.Office.Tools.Outlook.IFormRegion Microsoft.Office.Tools.Outlook.IFormRegionFactory.CreateFormRegion(Microsoft.Office.Interop.Outlook.FormRegion formRegion)
            {
                ConferenceFormRegion form = new ConferenceFormRegion(formRegion);
                form.Factory = this;
                return form;
            }

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            byte[] Microsoft.Office.Tools.Outlook.IFormRegionFactory.GetFormRegionStorage(object outlookItem, Microsoft.Office.Interop.Outlook.OlFormRegionMode formRegionMode, Microsoft.Office.Interop.Outlook.OlFormRegionSize formRegionSize)
            {
                throw new System.NotSupportedException();
            }

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            bool Microsoft.Office.Tools.Outlook.IFormRegionFactory.IsDisplayedForItem(object outlookItem, Microsoft.Office.Interop.Outlook.OlFormRegionMode formRegionMode, Microsoft.Office.Interop.Outlook.OlFormRegionSize formRegionSize)
            {
                if (this.FormRegionInitializing != null)
                {
                    Microsoft.Office.Tools.Outlook.FormRegionInitializingEventArgs cancelArgs = Globals.Factory.CreateFormRegionInitializingEventArgs(outlookItem, formRegionMode, formRegionSize, false);
                    this.FormRegionInitializing(this, cancelArgs);
                    return !cancelArgs.Cancel;
                }
                else
                {
                    return true;
                }
            }

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            Microsoft.Office.Tools.Outlook.FormRegionKindConstants Microsoft.Office.Tools.Outlook.IFormRegionFactory.Kind
            {
                get
                {
                    return Microsoft.Office.Tools.Outlook.FormRegionKindConstants.WindowsForms;
                }
            }
        }
    }

    partial class WindowFormRegionCollection
    {
        internal ConferenceFormRegion ConferenceFormRegion
        {
            get
            {
                foreach (var item in this)
                {
                    if (item.GetType() == typeof(ConferenceFormRegion))
                        return (ConferenceFormRegion)item;
                }
                return null;
            }
        }
    }
}
