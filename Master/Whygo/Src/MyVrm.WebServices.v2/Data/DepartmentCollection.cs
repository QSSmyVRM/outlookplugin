﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections;
using System.Collections.Generic;

namespace MyVrm.WebServices.Data
{
    [Serializable]
    public class DepartmentCollection : ComplexProperty, IEnumerable<Department>
    {
        private readonly List<Department> _departments = new List<Department>();

		public override object Clone()
		{
			DepartmentCollection copy = new DepartmentCollection();
			foreach (var item in _departments)
			{
				copy._departments.Add((Department)item.Clone()); 
			}

			return copy;
		}

        internal DepartmentCollection()
        {
        }

        public int Count
        {
            get
            {
                return _departments.Count;
            }
        }

        public Department this[int index]
        {
            get
            {
                return _departments[index];
            }
        }

        #region Implementation of IEnumerable

        public IEnumerator<Department> GetEnumerator()
        {
            return _departments.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        internal override void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
        {
            do
            {
                reader.Read();
                if (reader.IsStartElement(XmlNamespace.NotSpecified, "Department"))
                {
                    var department = new Department();
                    do
                    {
                        reader.Read();
                        if (reader.IsStartElement(XmlNamespace.NotSpecified, "ID"))
                        {
                            department.Id = reader.ReadValue<int>();
                        }
                        else if(reader.IsStartElement(XmlNamespace.NotSpecified, "Name"))
                        {
                            department.Name = reader.ReadValue();
                        }
                        else
                        {
                            reader.SkipCurrentElement();
                        }
                    } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "Department"));
                    _departments.Add(department);
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, xmlElementName));
        }
    }
}
