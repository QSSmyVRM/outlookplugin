﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MyVrm.WebServices.Data.TrueDaybook
{
	public class TrueDaybookResponse
	{
		private string _response;
		private JObject _json;

		public TrueDaybookErrCode Code
		{
			get { return new TrueDaybookErrCode(_json); }
		}

		public TrueDaybookResponse(string response)
		{
			_response = response;
			_json = (JObject)JsonConvert.DeserializeObject(_response);
		}


		public JArray GetJArray(string tagName)
		{
			return _json[tagName] as JArray;
		}

		public object GetValue(string tagName)
		{
			return _json.GetValue(tagName);
		}
		public T GetValue<T>(string tagName)
		{
			return _json[tagName].Value<T>();
		}
	}
}
