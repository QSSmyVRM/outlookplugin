/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using Microsoft.Office.Interop.Outlook;

namespace MyVrm.Outlook
{
    public class AppointmentInspectorWrapper : InspectorWrapper
    {
        public AppointmentInspectorWrapper(Inspector inspector) : base(inspector)
        {
            Item = (AppointmentItem) inspector.CurrentItem;
        }

        public AppointmentItem Item { get; private set; }

        protected override void Initialize()
        {
            Item = (AppointmentItem)Inspector.CurrentItem;
            // Outlook 2003 specifics. Outlook does not fire Inspector_Close event if user click Save&Close or Send buttons
            // Handle Item_Close event to close inspector
            if (Item.Application.Version != null && Item.Application.Version.StartsWith("11."))
            {
                ((ItemEvents_10_Event)Item).Close += ItemClose;
            }
        }

        protected override void Close()
        {
            if (Item.Application.Version != null && Item.Application.Version.StartsWith("11."))
            {
                ((ItemEvents_10_Event)Item).Close -= ItemClose;
            }
            Item = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        private void ItemClose(ref bool cancel)
        {
            OnInspectorClose();
        }
    }
}