﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	public class ConciergeSupportParams : ComplexProperty
    {
		public override bool Equals(object obj)
		{
			if (ReferenceEquals(this, obj))
				return true;

			ConciergeSupportParams parameters = obj as ConciergeSupportParams;
			if (parameters == null)
				return false;
			if (parameters.OnSiteAVSupport != OnSiteAVSupport)
				return false;
			if (parameters.MeetandGreet != MeetandGreet)
				return false;
			if (parameters.ConciergeMonitoring != ConciergeMonitoring)
				return false;
			if (parameters.DedicatedVNOCOperator != DedicatedVNOCOperator)
				return false;
			List<int> compare = new List<int>(parameters.ConfVNOCOperators.Keys);
			List<int> src = new List<int>(ConfVNOCOperators.Keys);
			compare.Sort();
			src.Sort();
			if (compare != src)
				return false;
			
			return true;
		}

		public override object Clone()//CopyTo(AdvancedAudioVideoParameters dest)
		{
			ConciergeSupportParams dest = new ConciergeSupportParams();
			dest.OnSiteAVSupport = OnSiteAVSupport;
			dest.MeetandGreet = MeetandGreet;
			dest.ConciergeMonitoring = ConciergeMonitoring;
			dest.DedicatedVNOCOperator = DedicatedVNOCOperator;
			dest.ConfVNOCOperators = new Dictionary<int, string>(ConfVNOCOperators);
			
			return dest;
		}

		public ConciergeSupportParams()
		{
			OnSiteAVSupport = false;
			MeetandGreet = false;
			ConciergeMonitoring = false;
			DedicatedVNOCOperator  = false;
			ConfVNOCOperators = new Dictionary<int, string>();
		}

		public bool OnSiteAVSupport { get; set; }
		public bool MeetandGreet { get; set; }
		public bool ConciergeMonitoring { get; set; }
		public bool DedicatedVNOCOperator { get; set; }
		public Dictionary<int, string> ConfVNOCOperators { get; set; }
		
        internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
        {
            switch(reader.LocalName)
            {
                case "OnSiteAVSupport":
                {
					OnSiteAVSupport = reader.ReadValue<bool>();
                    return true;
                }
                case "MeetandGreet":
                {
					MeetandGreet = reader.ReadValue<bool>();
                    return true;
                }
				case "ConciergeMonitoring":
                {
					ConciergeMonitoring = reader.ReadValue<bool>();
                    return true;
                }
				case "DedicatedVNOCOperator":
                {
					DedicatedVNOCOperator = reader.ReadValue<bool>();
                    return true;
                }
				case "ConfVNOCOperators":
                {
					ConfVNOCOperators.Clear();
					do
					{
						reader.Read();
						if (reader.IsStartElement(XmlNamespace.NotSpecified, "VNOCOperatorID"))
						{
							int id = reader.ReadValue<int>();
							ConfVNOCOperators.Add(id, string.Empty);
						}
						else
						{
							reader.SkipCurrentElement();
						}
					} while (!reader.IsEndElement(XmlNamespace.NotSpecified, "ConfVNOCOperators"));
                    return true;
                }
            }
            return false;
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            base.WriteElementsToXml(writer);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "OnSiteAVSupport", OnSiteAVSupport);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "MeetandGreet", MeetandGreet);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "ConciergeMonitoring", ConciergeMonitoring);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "DedicatedVNOCOperator", DedicatedVNOCOperator);
			writer.WriteStartElement(XmlNamespace.NotSpecified, "ConfVNOCOperators");
        	foreach (var id in ConfVNOCOperators)
        	{
				writer.WriteElementValue(XmlNamespace.NotSpecified, "VNOCOperatorID", id.Key);	
        	}
			writer.WriteEndElement();
        }

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
    }
}

