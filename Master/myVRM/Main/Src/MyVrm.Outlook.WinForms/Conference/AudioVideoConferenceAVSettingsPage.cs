﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using MyVrm.Common.ComponentModel;
using MyVrm.Common.EventBroker;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook.WinForms.Conference
{
    public partial class AudioVideoConferenceAVSettingsPage : ConferencePage
    {
		public enum AVPageType
    	{
    		AudioVideo,
    		PointToPoint,
			VMR
    	} ;

    	public AVPageType PageType { get; set; }

    	//private ConferenceAdvancedAudioVideoSettings _conferenceAdvancedAudioVideoSettings;
        private readonly DataList<RoomEndpoint> _roomEndpoints = new DataList<RoomEndpoint>();

        public AudioVideoConferenceAVSettingsPage()
        {
            InitializeComponent();
			roomAudioVideoSettingsListControl.BridgeNameColumnVisibility = true;
			roomAudioVideoSettingsListControl.useDefaultColumnVisibility = true;//false;
            Text = Strings.AudioVideoConferenceAVSettingsPageText;

        	commonAVSettingsGroup.CustomizationFormText = Strings.CommonSettingsLableText;
			commonAVSettingsGroup.Text = Strings.CommonSettingsLableText;
			layoutControlItem22.CustomizationFormText = Strings.MaximumLineRateLableText;
			layoutControlItem22.Text = Strings.MaximumLineRateLableText;
			layoutControlItem19.CustomizationFormText = Strings.VideoCodecsLableText;
			layoutControlItem19.Text = Strings.VideoCodecsLableText;
			layoutControlItem17.CustomizationFormText = Strings.RestrictNetworkAccessToLableText;
			layoutControlItem17.Text = Strings.RestrictNetworkAccessToLableText;
			layoutControlItem23.Text = Strings.RestrictUsageToLableText;
			layoutControlItem23.CustomizationFormText = Strings.RestrictUsageToLableText;
        	layoutControlItem25.Text = Strings.AudioCodecsLableText;
        	layoutControlItem27.Text = Strings.VideoDisplayLableText;
			layoutControlItem27.CustomizationFormText = Strings.VideoDisplayLableText;
        	layoutControlItem11.Text = Strings.MaximumVideoPortsLableText;
			layoutControlItem11.CustomizationFormText = Strings.MaximumVideoPortsLableText;
        	layoutControlItem12.Text = Strings.MaximumAudioPortsLableText;
			layoutControlItem12.CustomizationFormText = Strings.MaximumAudioPortsLableText;
			polycomSettingsGroup.Text = Strings.PolycomSpecificSettingsLableText;
			polycomSettingsGroup.CustomizationFormText = Strings.PolycomSpecificSettingsLableText;
			layoutControlItem28.Text = Strings.VideoModeLableText;
			layoutControlGroup4.Text = Strings.RoomsLableText;
			layoutControlGroup4.CustomizationFormText = Strings.RoomsLableText;
			layoutControlGroup5.Text = Strings.UsersLableText;
			layoutControlGroup5.CustomizationFormText = Strings.UsersLableText;
			conferenceOnPortEdit.Properties.Caption = Strings.ConferenceOnPortCaptionText;
			lectureModeEdit.Properties.Caption = Strings.LectureModeCaptionText;
			singleDialinEdit.Properties.Caption = Strings.SingleDialInNumberCaptionText;
			encryptedEdit.Properties.Caption = Strings.EncryptionCaptionText;
			dualStreamModeEdit.Properties.Caption = Strings.DualStreamModeCaptionText;

            roomAudioVideoSettingsListControl.DataSource = _roomEndpoints;

        	roomAudioVideoSettingsListControl.useDefaultColumnVisibility = true; // false;
        	SetPageType(AVPageType.AudioVideo);
			layoutControlGroup2.Text = Strings.TxtExternalRooms;
			externalRoomsControl.ConferenceBindingSource = ConferenceBindingSource;
        }

		public void SetPageType( AVPageType pageType)
		{
			PageType = pageType;
			externalRoomsControl.PageType = PageType;
			if (PageType == AVPageType.PointToPoint)
			{
				roomAudioVideoSettingsListControl.CallerColumnVisibility = true;
				roomAudioVideoSettingsListControl.BridgeNameColumnVisibility = false;
				roomAudioVideoSettingsListControl.SelectedBrigeProfileColumnVisibility = false;
				roomAudioVideoSettingsListControl.useDefaultColumnVisibileIndex = roomAudioVideoSettingsListControl.CallerColumnVisibileIndex + 1;
			}
			else
			{
				roomAudioVideoSettingsListControl.CallerColumnVisibility = false;
				roomAudioVideoSettingsListControl.BridgeNameColumnVisibility = true;
				roomAudioVideoSettingsListControl.BridgeNameColumnVisibileIndex =
					roomAudioVideoSettingsListControl.getEndpointProfileColumn.VisibleIndex + 1;
				
				roomAudioVideoSettingsListControl.SelectedBrigeProfileColumnVisibility = true;
				roomAudioVideoSettingsListControl.BridgeProfileNameColumnVisibileIndex =
					roomAudioVideoSettingsListControl.BridgeNameColumnVisibileIndex + 1;
				roomAudioVideoSettingsListControl.useDefaultColumnVisibileIndex =
					roomAudioVideoSettingsListControl.selectedBrigeProfileColumnVisibileIndex + 1;
				//roomAudioVideoSettingsListControl.BridgeNameColumnVisibileIndex + 1;
			}
			externalRoomsControl.SetColumnVisibility();
		}

		EventBroker _broker ;
		public void SetEventBroker(EventBroker broker)
		{
			_broker = broker;
			roomAudioVideoSettingsListControl.SetEventBroker(_broker);
		}

    	internal ReadOnlyCollection<RoomEndpoint> Endpoints
        {
            get
            {
                return new ReadOnlyCollection<RoomEndpoint>(_roomEndpoints);
            }
        }

        protected override void OnReadOnlyChanged(ReadOnlyChangedEventArgs e)
        {
            roomAudioVideoSettingsListControl.Enabled = !e.ReadOnly;
        }

        private void ConferenceBindingSource_DataSourceChanged(object sender, EventArgs e)
        {
            ResetData();
        }

        private void ResetData()
        {
            Conference.LocationIds.ListChanged += LocationIds_ListChanged;
            ResetRoomEndpoints();
        }

		public void CorrectPTPCaller()
		{
			//if (PageType == AVPageType.PointToPoint)
			{
				if (_roomEndpoints.Count == 0) //if not received yet - get it
					ResetRoomEndpoints();
				if (_roomEndpoints.Count >= 2 && _roomEndpoints[0].Caller != ConferenceEndpointCallMode.Caller &&
						_roomEndpoints[1].Caller != ConferenceEndpointCallMode.Caller ||
						_roomEndpoints.Count == 1 && Conference.ExternalRoomsAVSettings.Count == 0)
					_roomEndpoints[0].Caller = ConferenceEndpointCallMode.Caller;

				for (int i = 1; i < _roomEndpoints.Count && _roomEndpoints.Count > 1; i++)
				{
					if (_roomEndpoints[i].Caller == ConferenceEndpointCallMode.None)
						_roomEndpoints[i].Caller = ConferenceEndpointCallMode.Callee;
				}
			}
		}
		private bool IsLoaded{ set; get; }
        private void AudioVideoConferenceAVSettingsPage_Load(object sender, EventArgs e)
        {
        	Visible = false;
        	SuspendLayout();
            ConferenceBindingSource.DataSourceChanged += ConferenceBindingSource_DataSourceChanged;
            ResetData();
        	CorrectPTPCaller();
			IsLoaded = true;
			ResumeLayout();
			Visible = true;
        }

		public void Ext_ResetRoomEndpoints()
		{
			if (_roomEndpoints.Count == 0) //if not received yet - get it
				ResetRoomEndpoints();
		}
        private void ResetRoomEndpoints()
        {
			if (Conference != null)
			{
				var query = from roomId in Conference.LocationIds
				            join endpoint in Conference.RoomEndpoints on roomId equals endpoint.Id
				            	into conferenceEndpoints
				            from conferenceEndpoint in conferenceEndpoints.DefaultIfEmpty()
							select NewRoomEndpoint(roomId);
				_roomEndpoints.CopyFrom(query);
			}

			//if (Conference != null)
			//{
			//    //Check integrity: in case of page was not loaded and a room was removed or replaced with another room
			//    if (Conference.RoomEndpoints.Count != _roomEndpoints.Count)
			//    {
			//        List<ConferenceEndpoint> RoomEndpointsCopy =
			//            _roomEndpoints.Where(
			//                roomEndpoint => Conference.RoomEndpoints.FirstOrDefault(roomEndP => roomEndP.Id.Id == roomEndpoint.Id.Id) != null)
			//                .Select(roomEndpoint => roomEndpoint.ConferenceEndpoint).ToList();
			//        Conference.RoomEndpoints.Clear();
			//        Conference.RoomEndpoints.AddRange(RoomEndpointsCopy);
			//    }
			//}
        }

        void LocationIds_ListChanged(object sender, ListChangedEventArgs e)
        {
            switch (e.ListChangedType)
            {
                case ListChangedType.Reset:
                    {
                        ResetRoomEndpoints();
                        break;
                    }
                case ListChangedType.ItemAdded:
                    {
                        var roomId = Conference.LocationIds[e.NewIndex];
                        AddRoomEndpoint(e.NewIndex, roomId);
                        break;
                    }
                case ListChangedType.ItemDeleted:
                    {
						if (e.NewIndex >= 0 && _roomEndpoints.Count > 0 && e.NewIndex < _roomEndpoints.Count)
                        {
                            var roomEndpoint = _roomEndpoints[e.NewIndex];
                            Conference.RoomEndpoints.Remove(roomEndpoint.ConferenceEndpoint);
                            _roomEndpoints.RemoveAt(e.NewIndex);
                        }
                        break;
                    }
                case ListChangedType.ItemMoved:
                {
                    var roomEndpoint = _roomEndpoints[e.OldIndex];
                    _roomEndpoints.Move(roomEndpoint, e.NewIndex);
                    break;
                }
                case ListChangedType.ItemChanged:
                    break;
            }

        	CorrectPTPCaller();
        }

        private void AddRoomEndpoint(int index, RoomId roomId)
        {
            if (roomId == null) return;

			RoomEndpoint roomEndpoint = NewRoomEndpoint(roomId);
			RoomEndpoint foundEndpoint =  _roomEndpoints.FirstOrDefault( a => a.BridgeId == roomEndpoint.BridgeId);
			if (foundEndpoint != null)
			{
				roomEndpoint.BridgeProfileID = foundEndpoint.BridgeProfileID;
				roomEndpoint.BridgeProfileName = foundEndpoint.BridgeProfileName;
			}
			else
			{
				ExternalRoom foundRoom = externalRoomsControl.Rooms.FirstOrDefault(a => a.MCU != null && a.MCU.FirstObj == roomEndpoint.BridgeId);
				if(foundRoom != null)
				{
					roomEndpoint.BridgeProfileID = foundRoom.MCUProfile != null ? foundRoom.MCUProfile.FirstObj : null;
					roomEndpoint.BridgeProfileName = foundRoom.MCUProfile != null ? foundRoom.MCUProfile.SecondObj : null;
				}
			}
			//???? if (roomEndpoint.Endpoint != null) //Commented to process AV-rooms without endpoints (e.g. VRM rooms(?))
			{
				if(index < _roomEndpoints.Count )
					_roomEndpoints.Insert(index, roomEndpoint);
				else
					_roomEndpoints.Add(roomEndpoint);
			}
        }

		private RoomEndpoint NewRoomEndpoint(RoomId roomId)
		{
			Room roomDetails = MyVrmAddin.Instance.GetRoomFromCache(roomId);
			Endpoint endpoint = null;
			var conferenceEndpoint = Conference.RoomEndpoints.FirstOrDefault(endp => endp.Id == roomDetails.Id);
			if (conferenceEndpoint == null)
			{
				conferenceEndpoint = new ConferenceEndpoint
				{
					Id = roomId,
					UseDefault = false,
					EndpointId = roomDetails.EndpointId,
					Connection = MediaType.AudioVideo,
				};
				if ((roomDetails.Media == MediaType.AudioOnly || roomDetails.Media == MediaType.AudioVideo)
					&& roomDetails.EndpointId != null)
				{
					Conference.RoomEndpoints.Add(conferenceEndpoint);
				}
			}
			if (roomDetails.EndpointId != null)
			{
				endpoint = MyVrmService.Service.GetEndpoint(roomDetails.EndpointId);
			}
			RoomEndpoint ret = new RoomEndpoint(roomDetails, conferenceEndpoint, endpoint);
			return ret;// new RoomEndpoint(roomDetails, conferenceEndpoint, endpoint);
		}
		bool RoomsAndEndpointsAreDiffer(RoomIdCollection rooms, DataList<RoomEndpoint> roomEndpoints)
		{
			if (rooms.Count != roomEndpoints.Count)
				return true;

			foreach (RoomId room in rooms)
			{
				if(roomEndpoints.FirstOrDefault( roomEndpoint => roomEndpoint.Id.Id == room.Id) == null)
				{
					return true;
				}
			}
			return false;
		}

    	protected override void OnApplying(CancelEventArgs e)
		{
			base.OnApplying(e);
			if (e.Cancel || Conference == null)
				return;

			roomAudioVideoSettingsListControl.CloseEditor();

    		roomAudioVideoSettingsListControl.StopEndpointCustomNodeCellEdit();
			//If room endpoints where not read (i.e. page was not loaded) - get them
			//if (Conference.Conference.Rooms.Count > 0 && Conference.Conference.Rooms.Count >_roomEndpoints.Count )

			//1. room endpoints where not read (i.e. page was not loaded) - get them
			//2. location list was changed w/o av page load - need to synchronise endpoints with location list
			if ( RoomsAndEndpointsAreDiffer( Conference.Conference.Rooms, _roomEndpoints) ||
				Conference.RoomEndpoints.Count != Conference.Conference.Rooms.Count) //In case of Room type conf is converting to PTP
			{
				//DataList<RoomEndpoint> roomEndpointsCopy = new DataList<RoomEndpoint>();
				//roomEndpointsCopy.CopyFrom(_roomEndpoints);

				//ResetRoomEndpoints();

				//if (roomEndpointsCopy.Count > 0)
				//{
				//    foreach (RoomEndpoint rep in roomEndpointsCopy)
				//    {
				//        RoomEndpoint rr = _roomEndpoints.FirstOrDefault<RoomEndpoint>(r => r.Id == rep.Id);
				//        rr.Caller = rep.Caller;
				//    }
				//}

				CorrectPTPCaller();
			}

			//Check integrity: in case of page was not loaded and a room was removed or replaced with another room
			//if (Conference.RoomEndpoints.Count != _roomEndpoints.Count)
			{
				if (!IsLoaded) //page was not loaded -> store to page
				{
					ResetRoomEndpoints();
					List<ConferenceEndpoint> RoomEndpointsCopy =
						_roomEndpoints.Where(
							roomEndpoint => Conference.RoomEndpoints.FirstOrDefault(roomEndP => roomEndP.Id.Id == roomEndpoint.Id.Id) != null)
							.Select(roomEndpoint => roomEndpoint.ConferenceEndpoint).ToList();
					Conference.RoomEndpoints.Clear();
					Conference.RoomEndpoints.AddRange(RoomEndpointsCopy);
				}
				else //page was loaded <- store from page
				{
					Conference.RoomEndpoints.Clear();
					foreach (RoomEndpoint conferenceEndpoint in _roomEndpoints)
					{
						ConferenceEndpoint confEndp = new ConferenceEndpoint();
						confEndp = (ConferenceEndpoint) conferenceEndpoint.ConferenceEndpoint.Clone();
						Conference.RoomEndpoints.Add(confEndp);
					}
					externalRoomsControl.MakeEndpoints();
				}
			}
			roomAudioVideoSettingsListControl.StartEndpointCustomNodeCellEdit();
			roomAudioVideoSettingsListControl.Refresh();
			roomAudioVideoSettingsListControl.Invalidate();

			if (Conference != null)
			{
				if (Conference.Type == ConferenceType.PointToPoint)
				{
					//Prohibite to save more than 2 or less than 2 rooms for a PTP conference
					if (Conference.LocationIds.Count + Conference.ExternalRoomsAVSettings.Count != 2)
					{
						throw new Exception(Strings.Point2PointConferenceMaxRoomCountReached);
					}

					//Prohibite to save both Caller or both Callee endpoints
					if (_roomEndpoints.Count == 1 && Conference.ExternalRoomsAVSettings.Count == 1 &&
						_roomEndpoints[0].Caller == Conference.ExternalRoomsAVSettings[0].Endpoint.Caller ||
						_roomEndpoints.Count == 0 && Conference.ExternalRoomsAVSettings.Count == 2 && 
						Conference.ExternalRoomsAVSettings[0].Endpoint.Caller == Conference.ExternalRoomsAVSettings[1].Endpoint.Caller)
					{
						throw new Exception(Strings.Point2PointConferenceMustHaveCallerCallee);
					}
				}
			}
		}
    }
}
