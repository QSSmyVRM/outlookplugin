﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace MyVrm.WebServices.Data
{
    internal abstract class GetTierListResponse<TTier> : ServiceResponse where TTier : Tier
    {
        private readonly List<TTier> _tiers = new List<TTier>();

        internal ReadOnlyCollection<TTier> Tiers
        {
            get
            {
                return new ReadOnlyCollection<TTier>(_tiers);
            }
        }


        internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
        {
            base.ReadElementsFromXml(reader);
            do
            {
                reader.Read();
                if (reader.IsStartElement(XmlNamespace.NotSpecified, "Location"))
                {
                    var tier = CreateTierInstance(reader.Service);
                    tier.LoadFromXml(reader, false);
                    _tiers.Add(tier);
                }
                else
                {
                    reader.SkipCurrentElement();
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, GetResponseElementName()));
        }

        protected abstract string GetResponseElementName();

        protected abstract TTier CreateTierInstance(MyVrmService service);
    }

    internal class GetTopTierListResponse : GetTierListResponse<TopTier>
    {
        #region Overrides of GetTierListResponse<TopTier>

        protected override string GetResponseElementName()
        {
            return "GetLocations";
        }

        protected override TopTier CreateTierInstance(MyVrmService service)
        {
            return new TopTier(service);
        }

        #endregion
    }

    internal class GetMiddleTierListResponse : GetTierListResponse<MiddleTier>
    {
        #region Overrides of GetTierListResponse<MiddleTier>

        protected override string GetResponseElementName()
        {
            return "GetLocations2";
        }

        protected override MiddleTier CreateTierInstance(MyVrmService service)
        {
            return new MiddleTier(service);
        }

        #endregion
    }
}
