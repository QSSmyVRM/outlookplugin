/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.ComponentModel;
using System.Net;
using System.Security.Principal;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook.WinForms.Options
{
    public partial class ConnectionOptionsDialog : Dialog
    {
        public delegate void OptionsChangedHandler(object sender, EventArgs args);

        public ConnectionOptionsDialog()
        {
            InitializeComponent();
			ApplyEnabled = false;
			ApplyVisible = false;
            differentWindowsUserCredentialControlGroup.Enabled = false;
            customUserCredentialControlGroup.Enabled = false;

            labelControl1.Text = WinForms.Strings.AuthenticateAsLableText;
            testConnectionButton.Text = WinForms.Strings.TestConnectionLableText;
            layoutControlItem1.Text = WinForms.Strings.WebServiceURLLableText;
            layoutControlItem1.CustomizationFormText = WinForms.Strings.WebServiceURLLableText;
            layoutControlItem10.Text = WinForms.Strings.DomainLableText;
            layoutControlItem11.Text = WinForms.Strings.UserLableText;
            layoutControlItem12.Text = WinForms.Strings.PasswordLableText;
            layoutControlItem2.Text = WinForms.Strings.LoginLableText;
            layoutControlItem3.Text = WinForms.Strings.PasswordLableText;

            layoutControlItem10.CustomizationFormText = WinForms.Strings.DomainLableText;
            layoutControlItem11.CustomizationFormText = WinForms.Strings.UserLableText;
            layoutControlItem12.CustomizationFormText = WinForms.Strings.PasswordLableText;
            layoutControlItem2.CustomizationFormText = WinForms.Strings.LoginLableText;
            layoutControlItem3.CustomizationFormText = WinForms.Strings.PasswordLableText;

            Text = WinForms.Strings.ConnectionOptionsLableText;
            customUserCheckEdit.Properties.Caption = WinForms.Strings.CustomUserCaptionText;
            differentWindowsUserCheckEdit.Properties.Caption = WinForms.Strings.DifferentWindowsUserCaptionText;
            defaultWindowsUserCheckEdit.Properties.Caption = WinForms.Strings.CurrentWindowsUserCaptionText;
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false),
         EditorBrowsable(EditorBrowsableState.Never)]
        public string ServiceUrl
        {
            get
            {
                return (string)webServiceUrlEdit.EditValue;
            }
            set
            {
                webServiceUrlEdit.EditValue = value;
            }
        }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false),
         EditorBrowsable(EditorBrowsableState.Never)]
        public string UserName
        {
            get
            {
                if (AuthenticationMode == AuthenticationMode.Windows && !UseDefaultCredential)
                {
                    return (string)windowsUserAccountNameTextEdit.EditValue;
                }
                if (AuthenticationMode == AuthenticationMode.Custom)
                {
                    return (string)userNameEdit.EditValue;
                }
                return null;
            }
            set
            {
                if (AuthenticationMode == AuthenticationMode.Windows && !UseDefaultCredential)
                {
                    windowsUserAccountNameTextEdit.EditValue = value;
                }
                if (AuthenticationMode == AuthenticationMode.Custom)
                {
                    userNameEdit.EditValue = value;
                }
            }
        }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false),
         EditorBrowsable(EditorBrowsableState.Never)]
        public string UserDomain
        {
            get
            {
                if (AuthenticationMode == AuthenticationMode.Windows && !UseDefaultCredential)
                {
                    return (string)windowsUserDomainNameTextEdit.EditValue;
                }
                return null;
            }
            set
            {
                windowsUserDomainNameTextEdit.EditValue = value;
            }
        }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false),
         EditorBrowsable(EditorBrowsableState.Never)]
        public string UserPassword
        {
            get
            {
                if (AuthenticationMode == AuthenticationMode.Windows && !UseDefaultCredential)
                {
                    return (string)windowsUserPasswordTextEdit.EditValue;
                }
                if (AuthenticationMode == AuthenticationMode.Custom)
                {
                    return (string)userPasswordEdit.EditValue;
                }
                return null;
            }
            set
            {
                if (AuthenticationMode == AuthenticationMode.Windows && !UseDefaultCredential)
                {
                    windowsUserPasswordTextEdit.EditValue = value;
                }
                if (AuthenticationMode == AuthenticationMode.Custom)
                {
                    userPasswordEdit.EditValue = value;
                }
            }
        }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false),
         EditorBrowsable(EditorBrowsableState.Never)]
        public bool UseDefaultCredential
        {
            get
            {
                return defaultWindowsUserCheckEdit.Checked;
            }
            set
            {
                if (AuthenticationMode == AuthenticationMode.Windows && value)
                {
                    defaultWindowsUserCheckEdit.Checked = true;
                }
                else if(AuthenticationMode == AuthenticationMode.Windows && !value)
                {
                    differentWindowsUserCheckEdit.Checked = true;
                }
                else
                {
                    customUserCheckEdit.Checked = true;
                }
            }
        }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false),
         EditorBrowsable(EditorBrowsableState.Never)]
        public AuthenticationMode AuthenticationMode
        {
            get
            {
                //if (defaultWindowsUserCheckEdit.Checked || differentWindowsUserCheckEdit.Checked)
                //    return AuthenticationMode.Windows;
                //else if (customUserCheckEdit.Checked)
                //    return AuthenticationMode.Custom;
                //else
                //    return AuthenticationMode.Windows;
                return (defaultWindowsUserCheckEdit.Checked || differentWindowsUserCheckEdit.Checked) ? AuthenticationMode.Windows : AuthenticationMode.Custom;
            }
            set
            {
                switch(value)
                {
                    case AuthenticationMode.Windows:
                    {
                        if (UseDefaultCredential)
                        {
                            defaultWindowsUserCheckEdit.Checked = true;
                        }
                        else
                        {
                            differentWindowsUserCheckEdit.Checked = true;
                        }
                        break;
                    }
                    case AuthenticationMode.Custom:
                    {
                        customUserCheckEdit.Checked = true;
                        break;
                    }
                    default:
                        throw new ArgumentOutOfRangeException("value");
                }
            }
        }

        private void testConnectionButton_Click(object sender, EventArgs e)
        {
            Cursor cursor = Cursor;
            try
            {
                Cursor = Cursors.WaitCursor;
                MyVrmService.TestConnection(webServiceUrlEdit.Text, AuthenticationMode,
                                            UseDefaultCredential ? null : 
                                            AuthenticationMode == AuthenticationMode.Windows ? 
											new NetworkCredential(windowsUserAccountNameTextEdit.Text, windowsUserPasswordTextEdit.Text, windowsUserDomainNameTextEdit.Text) :
											new NetworkCredential(userNameEdit.Text, userPasswordEdit.Text));
                ShowMessage(WinForms.Strings.TestConnectionSucceeded);
            }
            catch (Exception exception)
            {
                ShowError(string.Format(WinForms.Strings.TestConnectionFailed, exception.Message));
            }
            finally
            {
                Cursor = cursor;
            }
        }

        private void differentWindowsUserCheckEdit_CheckedChanged(object sender, EventArgs e)
        {
            differentWindowsUserCredentialControlGroup.Enabled = differentWindowsUserCheckEdit.Checked;
        }

        private void customUserCheckEdit_CheckedChanged(object sender, EventArgs e)
        {
            customUserCredentialControlGroup.Enabled = customUserCheckEdit.Checked;
        }

        private void OptionsControl_Load(object sender, EventArgs e)
        {
            WindowsIdentity currentIdentity = WindowsIdentity.GetCurrent();
            defaultWindowsUserCheckEdit.Text = String.Format(defaultWindowsUserCheckEdit.Text, currentIdentity.Name);
        }

        private void ConnectionOptionsDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult == DialogResult.OK)
            {
                errorProvider.ClearErrors();
                ValidateChildren(ValidationConstraints.Enabled);
                if (errorProvider.HasErrors)
                {
                    var controlsWithError = errorProvider.GetControlsWithError();
                    var sb = new StringBuilder();
                    foreach (var control in controlsWithError)
                    {
                        sb.AppendLine(string.Format("{0}", errorProvider.GetError(control)));
                    }
                    ShowError(sb.ToString());
                    e.Cancel = true;
                }
            }
        }

        private void ValidateTextEdit(TextEdit textEdit, string errorText, CancelEventArgs e)
        {
            if (string.IsNullOrEmpty(textEdit.Text) || string.IsNullOrEmpty(textEdit.Text.Trim()))
            {
                errorProvider.SetError(textEdit, errorText);
            }
            else
            {
                errorProvider.SetError(textEdit, "");
            }
        }

        private void webServiceUrlEdit_Validating(object sender, CancelEventArgs e)
        {
            var textEdit = (TextEdit) sender;
            ValidateTextEdit(textEdit, Strings.WebServiceUrlCannotBeEmpty, e);
        }

        private void windowsUserDomainNameTextEdit_Validating(object sender, CancelEventArgs e)
        {
            var textEdit = (TextEdit)sender;
            ValidateTextEdit(textEdit, Strings.DomainNameCannotBeEmpty, e);
        }

        private void windowsUserAccountNameTextEdit_Validating(object sender, CancelEventArgs e)
        {
            var textEdit = (TextEdit)sender;
            ValidateTextEdit(textEdit, Strings.UserNameCannotBeEmpty, e);
        }

        private void windowsUserPasswordTextEdit_Validating(object sender, CancelEventArgs e)
        {
            var textEdit = (TextEdit)sender;
            ValidateTextEdit(textEdit, Strings.PasswordCannotBeEmpty, e);
        }

        private void userNameEdit_Validating(object sender, CancelEventArgs e)
        {
            var textEdit = (TextEdit)sender;
            ValidateTextEdit(textEdit, Strings.LoginCannotBeEmpty, e);
        }

        private void userPasswordEdit_Validating(object sender, CancelEventArgs e)
        {
            var textEdit = (TextEdit)sender;
            ValidateTextEdit(textEdit, Strings.PasswordCannotBeEmpty, e);
        }
    }
}