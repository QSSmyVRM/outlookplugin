﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	public enum LocationPofileType //?????????
	{
	}

	public class LocationPofile
	{
		public string Address { get; set; }
		public /*LocationPofileType*/string Type { get; set; }
		public string Speed { get; set; } //????????
	}

	public class LocationDetails
	{
		public int LocationID { get; set; }
		public string Description { get; set; } //-
		public string LocationName { get; set; }
		public string CitySuburb { get; set; }
		public int ZipCode { get; set; }
		public string StateName { get; set; }
		public double GenericSellPrice { get; set; }
		public double LocationCurrencyPrice { get; set; } //- //local type of currency?
		public int Capacity { get; set; }
		public List<LocationPofile> LocationPofiles = new List<LocationPofile>();
		//---------------------------------------------
		public string generalPhone{ get; set; }
		public string fakeFax { get; set; }
		public bool isVCCapable{ get; set; }
		public TimeZoneInfo locationTimeZone { get; set; } //format????
		public bool isAutomatic{ get; set; }
		public string imageLocation{ get; set; }
		public string geoCodeAddress{ get; set; }
		public string isHDCapable { get; set; } //??? type??
		public string isIPCapable { get; set; }
		public bool isIPCConnectionCapable { get; set; } //??? type??
		public string isInternetCapable { get; set; } //??? type??
		public bool isInternetFree{ get; set; }
		public double internetPrice{ get; set; }
		public string /*???*/ internetPriceCurrency{ get; set; }
		public bool isTP{ get; set; }
		public string extraNotes{ get; set; }
		public string equipmentDescription{ get; set; }
		public string openHours { get; set; } //format???? type?? (Business Hours/Available 24 hours)
		public int roomType{ get; set; } // == Room.MediaType ?????????
		public bool isAutomated24x7{ get; set; }
		public bool isIPDedicated{ get; set; }
		public string internetBiller{ get; set; }
		public bool isEarlyHoursSupported{ get; set; }
		public DateTime earlyHoursStart{ get; set; }
		public DateTime earlyHoursEnd{ get; set; }
		public double earlyHoursPrice{ get; set; }
		public DateTime openHoursStart{ get; set; }
		public DateTime openHoursEnd{ get; set; }
		public double openHoursPrice{ get; set; }
		public bool isAfterHoursSupported{ get; set; }
		public DateTime afterHoursStart{ get; set; }
		public DateTime afterHoursEnd{ get; set; }
		public double afterHoursPrice{ get; set; }
		public bool isCrazyHoursSupported{ get; set; }
		public DateTime crazyHoursStart{ get; set; }
		public DateTime crazyHoursEnd{ get; set; }
		public double crazyHoursPrice{ get; set; }
	}

	//Request
	class GetLocationDetailsRequest : ServiceRequestBase<GetLocationDetailsResponse>
	{
		internal RoomId LocationID { get; set; }

		internal GetLocationDetailsRequest(MyVrmService service)
			: base(service)
		{
		}

		#region Overrides of ServiceRequestBase<GetTemplateResponse>

		internal override string GetXmlElementName()
		{
			return "login";
		}

		internal override string GetCommandName()
		{
			return Constants.GetLocationDetailsCommandName;
		}

		internal override string GetResponseXmlElementName()
		{
			return "GetLocationDetails";
		}

		internal override GetLocationDetailsResponse ParseResponse(MyVrmServiceXmlReader reader)
		{
			var response = new GetLocationDetailsResponse();
			response.ReadElementsFromXml(reader);
			return response;
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			if (UserId != null)
				UserId.WriteToXml(writer, "userID");
		}

		#endregion
	}

	//Response
	public class GetLocationDetailsResponse : ServiceResponse
	{
		//Result
		public LocationDetails LocationDetails = new LocationDetails();

		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{

		}
	}
}
