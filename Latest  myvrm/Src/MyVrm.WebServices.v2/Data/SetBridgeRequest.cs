﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace MyVrm.WebServices.Data
{
    internal class SetBridgeRequest : ServiceRequestBase<SetBridgeResponse>
    {
        public SetBridgeRequest(MyVrmService service) : base(service)
        {
        }

        #region Overrides of ServiceRequestBase

        internal override string GetXmlElementName()
        {
            return "setBridge";
        }

        internal override string GetCommandName()
        {
            return "SetBridge";
        }

        internal override string GetResponseXmlElementName()
        {
            return "success";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            UserId.WriteToXml(writer);
            OrganizationId.WriteToXml(writer);
            Bridge.WriteToXml(writer);
        }

        #endregion

        internal Bridge Bridge { get; set; }
        
    }
}
