﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    internal class InventorySetSchema : ServiceObjectSchema
    {
        public static readonly PropertyDefinition Id;
        public static readonly PropertyDefinition Type;
        public static readonly PropertyDefinition Name;
        public static readonly PropertyDefinition Comments;
        //public static readonly PropertyDefinition Administrator;
        public static readonly PropertyDefinition Notify;
        //public static readonly PropertyDefinition AssignedCostCtr;
        //public static readonly PropertyDefinition AllowOverBooking;
        public static readonly PropertyDefinition Items;
        public static readonly PropertyDefinition Rooms;

        internal static readonly InventorySetSchema Instance;

        static InventorySetSchema()
        {
            Id = new IntPropertyDefinition("ID", PropertyDefinitionFlags.CanSet);
            Type = new IntPropertyDefinition("Type", PropertyDefinitionFlags.CanSet);
            Name = new StringPropertyDefinition("Name", PropertyDefinitionFlags.CanSet);
            Comments = new StringPropertyDefinition("Comments", PropertyDefinitionFlags.CanSet);
            Notify = new BoolPropertyDefinition("Notify", PropertyDefinitionFlags.CanSet);
            Items = new ComplexPropertyDefinition<InventorySetItemCollection>("ItemList",
                                                                              () => new InventorySetItemCollection());
            Rooms = new ComplexPropertyDefinition<RoomIdCollection>("locationList", () => new RoomIdCollection());

            Instance = new InventorySetSchema();
        }

        internal override void RegisterProperties()
        {
            base.RegisterProperties();
            RegisterProperty(Id);
            RegisterProperty(Type);
            RegisterProperty(Name);
            RegisterProperty(Comments);
            //RegisterProperty(Administrator);
            RegisterProperty(Notify);
            //RegisterProperty(AssignedCostCtr);
            //RegisterProperty(AllowOverBooking);
            RegisterProperty(Items);
            RegisterProperty(Rooms);
        }
    }
}
