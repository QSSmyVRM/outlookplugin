﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Linq;
using System.ComponentModel;
using System.Windows.Forms;
using DevExpress.XtraTreeList.Nodes;
using MyVrm.Common.ComponentModel;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook.WinForms.Conference
{
    public partial class InventoryWorkOrdersPage : ConferencePage
    {
        private class WorkOrderRecord
        {
            private Room _room;
            private WorkOrder _workOrder;

            internal WorkOrderRecord(Room room, WorkOrder workOrder)
            {
                _room = room;
                _workOrder = workOrder;
            }
            public RoomId RoomId
            {
                get
                {
                    return _room.Id;
                }
            }
            public string RoomName 
            { 
                get
                {
                    return _room.Name;        
                }
            }
            public WorkOrderId Id
            {
                get
                {
                    return _workOrder != null ? _workOrder.Id : null;
                }
            }
            public string Name
            {
                get 
                {
                    return _workOrder != null ? _workOrder.Name : null;
                }
            }
            public WorkOrderStatus? Status
            {
                get
                {
                    return _workOrder != null ? new WorkOrderStatus?(_workOrder.Status) : null;    
                }
            }

            internal WorkOrder WorkOrder
            {
                get
                {
                    return _workOrder;
                }
                set
                {
                    _workOrder = value;
                }
            }

            internal void DetachWorkOrder()
            {
                _workOrder = null;
            }
        }

        private readonly DataList<WorkOrderRecord> _workOrderRecords = new DataList<WorkOrderRecord>();

        public InventoryWorkOrdersPage()
        {
            InitializeComponent();
            Text = Strings.AudioVisualWorkOrdersPageText;
        	roomNameColumn.Caption = Strings.RoomLableText;
        	workOrderNameColumn.Caption = Strings.WorkOrderLableText;
			workOrderStatusColumn.Caption = Strings.WorkOrderStatusColumnText;
			workOrderRepositoryItemTextEdit.NullText = Strings.NoAssignedWorkOrderText;
        	workOrderStatusRepositoryItemTextEdit.NullText = Strings.NotAvailableText;

            workOrderList.DataSource = _workOrderRecords;
        	bar1.Text = Strings.ToolsBarText;
			assignWorkOrderBarButtonItem.Caption = Strings.AssignMenuItemText;
			editWorkOrderBarButtonItem.Caption = Strings.EditMenuItemText;
			removeWorkOrderBarButtonItem.Caption = Strings.RemoveMenuItemText;
        }

        protected override void OnReadOnlyChanged(ReadOnlyChangedEventArgs e)
        {
            base.OnReadOnlyChanged(e);
            bar1.Visible = !e.ReadOnly;
            workOrderList.Enabled = !e.ReadOnly;
        }

        void WorkOrderRecordsListChanged(object sender, ListChangedEventArgs e)
        {
            switch (e.ListChangedType)
            {
                case ListChangedType.Reset:
                    break;
                case ListChangedType.ItemAdded:
                    break;
                case ListChangedType.ItemDeleted:
                    break;
                case ListChangedType.ItemMoved:
                    break;
                case ListChangedType.ItemChanged:
                    {
                        EnableBarButtons(_workOrderRecords[e.NewIndex].WorkOrder);
                        break;
                    }
                case ListChangedType.PropertyDescriptorAdded:
                    break;
                case ListChangedType.PropertyDescriptorDeleted:
                    break;
                case ListChangedType.PropertyDescriptorChanged:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void EnableBarButtons(WorkOrder workOrder)
        {
            var isWorkOrderAssigned = workOrder != null;
            assignWorkOrderBarButtonItem.Enabled = !isWorkOrderAssigned;
            editWorkOrderBarButtonItem.Enabled = isWorkOrderAssigned;
            removeWorkOrderBarButtonItem.Enabled = isWorkOrderAssigned;
        }

        private void EnableBarButtons(TreeListNode node)
        {
            if (node != null)
            {
                EnableBarButtons(_workOrderRecords[node.Id].WorkOrder);
            }
            else
            {
                assignWorkOrderBarButtonItem.Enabled = false;
                editWorkOrderBarButtonItem.Enabled = false;
                removeWorkOrderBarButtonItem.Enabled = false;
            }
        }

        void ConferenceBindingSource_DataSourceChanged(object sender, EventArgs e)
        {
            ResetData();
        }

        private void ResetData()
        {
            if (Conference != null)
            {
                Conference.LocationIds.ListChanged += LocationIds_ListChanged;
                ResetWorkOrderRecords();
            }
        }

        private void InventoryWorkOrdersPage_Load(object sender, EventArgs e)
        {
            ConferenceBindingSource.DataSourceChanged += ConferenceBindingSource_DataSourceChanged;
            _workOrderRecords.ListChanged += WorkOrderRecordsListChanged;
            ResetData();
        }

        void LocationIds_ListChanged(object sender, ListChangedEventArgs e)
        {
            switch(e.ListChangedType)
            {
                case ListChangedType.Reset:
                {
                    ResetWorkOrderRecords();
                    break;
                }
                case ListChangedType.ItemAdded:
                {
                    var room = MyVrmAddin.Instance.GetRoomFromCache(Conference.LocationIds[e.NewIndex]);
                    WorkOrder workorder = Conference.Conference.InventoryWorkOrders.FirstOrDefault(order => order.RoomId.Equals(room.Id));
                    _workOrderRecords.Add(new WorkOrderRecord(room, workorder));
                    break;
                }
                case ListChangedType.ItemDeleted:
                {
                    _workOrderRecords.RemoveAt(e.NewIndex);
                    break;
                }
                case ListChangedType.ItemMoved:
                {
                    var workOrderRecord = _workOrderRecords[e.OldIndex];
                    _workOrderRecords.Move(workOrderRecord, e.NewIndex);
                    break;
                }
                case ListChangedType.ItemChanged:
                    break;
                case ListChangedType.PropertyDescriptorAdded:
                    break;
                case ListChangedType.PropertyDescriptorDeleted:
                    break;
                case ListChangedType.PropertyDescriptorChanged:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            workOrderList.RefreshDataSource();
        }

        private void ResetWorkOrderRecords()
        {
            _workOrderRecords.Clear();
            var query = from roomId in Conference.LocationIds
                        join workOrder in Conference.Conference.InventoryWorkOrders on roomId equals
                            workOrder.RoomId into workOrders
                        from wo in workOrders.DefaultIfEmpty()
                        select new WorkOrderRecord (MyVrmAddin.Instance.GetRoomFromCache(roomId), wo);

            _workOrderRecords.AddRange(query);
        }

        private void editWorkOrderBarButtonItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EditWorkOrder();
        }

        private void EditWorkOrder()
        {
            if (workOrderList.FocusedNode == null)
            {
                return;
            }
            using (var dialog = new InventoryWorkOrderDialog())
            {
                var workOrderRecord = _workOrderRecords[workOrderList.FocusedNode.Id];
                dialog.WorkOrder = workOrderRecord.WorkOrder;
                if( dialog.ShowDialog(this) == DialogResult.OK)
                {
					if (Conference != null)
						Conference.Appointment.SetNonOutlookProperty(true);
                }
            }
        }

        private void assignWorkOrderBarButtonItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            AssignWorkOrder();
        }

        private void AssignWorkOrder()
        {
            if (workOrderList.FocusedNode == null)
            {
                return;
            }
            using (var dialog = new InventoryWorkOrderDialog())
            {
                var workOrderRecord = _workOrderRecords[workOrderList.FocusedNode.Id];
                dialog.WorkOrder = new WorkOrder(MyVrmService.Service, workOrderRecord.RoomId)
                                       {
                                           Name = string.Format("{0}_AV", Conference.Conference.Name),
                                           SetId = 0,
                                           Status = WorkOrderStatus.Pending,
                                           Type = WorkOrderType.Inventory,
                                           AdministratorId = MyVrmService.Service.UserId,
                                           Start = Conference.StartDate,
                                           Complete = Conference.EndDate,
                                           TimeZone = Conference.TimeZone,
                                           DeliveryType = WorkOrderDeliveryType.DeliveryAndPickup,
                                           DeliveryCost = 0,
                                           ServiceCharge = 0,
                                           TotalCost = 0,
                                           Notify = false,
                                           Reminder = false
                                       };
                if (dialog.ShowDialog(this) == DialogResult.OK)
                {
                    Conference.Conference.InventoryWorkOrders.Add(dialog.WorkOrder);
                    var record = _workOrderRecords.FirstOrDefault(rec => rec.RoomId == dialog.WorkOrder.RoomId);
                    record.WorkOrder = dialog.WorkOrder;
                    _workOrderRecords.ResetItem(workOrderList.FocusedNode.Id);

					if (Conference != null)
						Conference.Appointment.SetNonOutlookProperty(true);
                }
            }
        }

        private void removeWorkOrderBarButtonItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (workOrderList.FocusedNode != null)
            {
                var workOrderRecord = _workOrderRecords[workOrderList.FocusedNode.Id];
                Conference.Conference.InventoryWorkOrders.Remove(workOrderRecord.WorkOrder);
                workOrderRecord.DetachWorkOrder();
                _workOrderRecords.ResetItem(workOrderList.FocusedNode.Id);
				if (Conference != null)
					Conference.Appointment.SetNonOutlookProperty(true);
            }
        }

        private void workOrderList_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            EnableBarButtons(e.Node);
        }

        private void workOrderList_DoubleClick(object sender, EventArgs e)
        {
            if (workOrderList.FocusedNode != null)
            {
                if (_workOrderRecords[workOrderList.FocusedNode.Id].WorkOrder == null)
                {
                    AssignWorkOrder();
                }
                else
                {
                    EditWorkOrder();
                }
            }
        }
    }
}
