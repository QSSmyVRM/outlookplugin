﻿using System;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
using MyVrm.Outlook;
using MyVrm.WebServices.Data;

namespace WindowsFormsApplication1
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += currentDomain_UnhandledException;
            //Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");
            //Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
            Application.EnableVisualStyles();
            DevExpress.UserSkins.OfficeSkins.Register();
            Application.SetCompatibleTextRenderingDefault(false);
            MyVrmService.TraceSource = MyVrmAddin.TraceSource;
            MyVrmService.Service.ClientType = "02";
            MyVrmService.Service.ClientVersion = ClientVersion.Parse("2.0.38");

            MyVrmAddin.Instance.Settings.UseDefaultCredentials = false;
            MyVrmAddin.Instance.Settings.AuthenticationMode = AuthenticationMode.Custom;
            MyVrmAddin.Instance.Settings.UserName = "achochia@logic-lab.com";
            MyVrmAddin.Instance.Settings.UserPassword = "logiclab";
            MyVrmAddin.Instance.Settings.UserDomain = "";
            MyVrmAddin.Instance.Settings.MyVrmServiceUrl = "http://myvrm01/en/myvrmws.asmx?WSDL";

            //MyVrmAddin.Instance.Settings.UseDefaultCredentials = false;
            //MyVrmAddin.Instance.Settings.AuthenticationMode = AuthenticationMode.Custom;
            //MyVrmAddin.Instance.Settings.UserName = "admin@myvrm.com";
            //MyVrmAddin.Instance.Settings.UserPassword = "myvrm";
            //MyVrmAddin.Instance.Settings.UserDomain = "";
            //MyVrmAddin.Instance.Settings.MyVrmServiceUrl = "http://cqa1.myvrm.local/en/myvrmws.asmx";

            //MyVrmAddin.Instance.Settings.UseDefaultCredentials = false;
            //MyVrmAddin.Instance.Settings.AuthenticationMode = AuthenticationMode.Windows;
            //MyVrmAddin.Instance.Settings.UserName = "achochia";
            //MyVrmAddin.Instance.Settings.UserPassword = "Logiclab2010";
            //MyVrmAddin.Instance.Settings.UserDomain = "myvrm";
            //MyVrmAddin.Instance.Settings.MyVrmServiceUrl = "http://cqad.myvrm.local/en/myvrmws.asmx";
            //MyVrmAddin.Instance.Settings.Save();
            Application.Run(new XtraForm1());
        }

        static void currentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            MessageBox.Show(((Exception) e.ExceptionObject).Message);
        }
    }
}
