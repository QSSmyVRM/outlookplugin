﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace MyVrm.WebServices.Data
{
    internal class GetConferenceRequest : ServiceRequestBase<GetConferenceResponse>
    {
        public GetConferenceRequest(MyVrmService service) : base(service)
        {
        }

        internal ConferenceId ConferenceId { get; set; }

        #region Overrides of ServiceRequestBase

        internal override string GetXmlElementName()
        {
            return "login";
        }

        internal override string GetCommandName()
        {
            return "GetOldConference";
        }

        internal override string GetResponseXmlElementName()
        {
            return "conference";
        }

        internal override GetConferenceResponse ParseResponse(MyVrmServiceXmlReader reader)
        {
            var response = new GetConferenceResponse
                           {
                               Conference = new Conference(Service)
                           };
            response.LoadFromXml(reader, GetResponseXmlElementName());
            return response;
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            UserId.WriteToXml(writer, "userID");
            ConferenceId.WriteToXml(writer, "selectID");
			writer.WriteElementValue(XmlNamespace.NotSpecified, "utcEnabled", Service.IsUtcEnabled);
        }

        #endregion
    }
}
