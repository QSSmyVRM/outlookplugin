﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections;
using System.Collections.Generic;

namespace MyVrm.WebServices.Data
{
   
    /// <summary>
    /// Represents organization options.
    /// </summary>
    /// 
    public class OrganizationOptions
    {
        /// <summary>
        /// Represents contact person details.
        /// </summary>
        public class ContactDetails : ComplexProperty
        {
            /// <summary>
            /// Name of contact person appearing on the bottom of each window.
            /// </summary>
            public string Name { get; set; }
            /// <summary>
            /// Email for tech support contact appearing on the bottom of each window.
            /// </summary>
            public string Email { get; set; }
            /// <summary>
            /// Phone number for tech support contact appearing on the bottom of each window.
            /// </summary>
            public string Phone { get; set; }
            /// <summary>
            /// Any additional information.
            /// </summary>
            public string AdditionInfo { get; set; }

            public override object Clone()
            {
                ContactDetails copy = new ContactDetails();
                copy.AdditionInfo = string.Copy(AdditionInfo);
                copy.Email = string.Copy(Email);
                copy.Name = string.Copy(Name);
                copy.Phone = string.Copy(Phone);

                return copy;
            }

            internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
            {
                switch (reader.LocalName)
                {
                    case "Name":
                        {
                            Name = reader.ReadElementValue();
                            Name = Utilities.ReplaceOutXMLSpecialCharacters(Name); //104771
                            return true;
                        }
                    case "Email":
                        {
                            Email = reader.ReadElementValue();
                            Email = Utilities.ReplaceOutXMLSpecialCharacters(Email); //104771
                            return true;
                        }
                    case "Phone":
                        {
                            Phone = reader.ReadElementValue();
                            Phone = Utilities.ReplaceOutXMLSpecialCharacters(Phone); //104771
                            return true;
                        }
                    case "AdditionInfo":
                        {
                            AdditionInfo = reader.ReadElementValue();
                            AdditionInfo = Utilities.ReplaceOutXMLSpecialCharacters(AdditionInfo); //104771
                            return true;
                        }
                }
                return false;
            }
        }

        /// <summary>
        /// Represents system availablity options.
        /// </summary>
        public class SystemAvailableTime : ComplexProperty
        {
            private int _systemTimeZoneID;

            public override object Clone()
            {
                SystemAvailableTime copy = new SystemAvailableTime();
                copy._systemTimeZoneID = _systemTimeZoneID;
                copy.DaysClosed = DaysClosed;
                copy.EndTimeInLocalTimezone = new TimeSpan(EndTimeInLocalTimezone.Ticks);
                copy.EndTimeInServerTimezone = new TimeSpan(EndTimeInServerTimezone.Ticks);
                copy.StartTimeInLocalTimezone = new TimeSpan(StartTimeInLocalTimezone.Ticks);
                copy.StartTimeInServerTimezone = new TimeSpan(StartTimeInServerTimezone.Ticks);
                copy.IsOpen24Hours = IsOpen24Hours;

                return copy;
            }

            public void SetupSystemTimeZoneID(int systemTimeZoneID)
            {
                _systemTimeZoneID = systemTimeZoneID;
                TimeZoneInfo ti = TimeZoneConvertion.ConvertToTimeZoneInfo(_systemTimeZoneID);
                StartTimeInLocalTimezone = new TimeSpan(TimeZoneInfo.ConvertTimeBySystemTimeZoneId(new DateTime(StartTimeInServerTimezone.Ticks), ti.Id, TimeZoneInfo.Local.Id).Ticks);
                EndTimeInLocalTimezone = new TimeSpan(TimeZoneInfo.ConvertTimeBySystemTimeZoneId(new DateTime(EndTimeInServerTimezone.Ticks), ti.Id, TimeZoneInfo.Local.Id).Ticks);
            }
            /// <summary>
            /// If true, rooms can be booked any day and any hour of the day.
            /// </summary>
            public bool IsOpen24Hours { get; set; }
            /// <summary>
            /// The start time and during which conferences can run - server time zone.
            /// </summary>
            public TimeSpan StartTimeInServerTimezone { get; set; }
            /// <summary>
            /// The end time and during which conferences can run - server time zone.
            /// </summary>
            public TimeSpan EndTimeInServerTimezone { get; set; }
            /// <summary>
            /// The start time and during which conferences can run - local time zone.
            /// </summary>
            public TimeSpan StartTimeInLocalTimezone { get; set; }
            /// <summary>
            /// The end time and during which conferences can run - local time zone.
            /// </summary>
            public TimeSpan EndTimeInLocalTimezone { get; set; }
            /// <summary>
            /// Any days of the week for which conferences cannot be booked.
            /// </summary>
            public DaysOfWeek DaysClosed { get; set; }

            internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
            {
                switch (reader.LocalName)
                {
                    case "IsOpen24Hours":
                        {
                            IsOpen24Hours = reader.ReadElementValue<bool>();
                            return true;
                        }
                    case "StartTime":
                        {
                            StartTimeInServerTimezone = Utilities.TimeOfDayStringToTimeSpan(reader.ReadElementValue());
                            return true;
                        }
                    case "EndTime":
                        {
                            EndTimeInServerTimezone = Utilities.TimeOfDayStringToTimeSpan(reader.ReadElementValue());
                            return true;
                        }
                    case "DaysClosed":
                        {
                            DaysClosed = Utilities.StringToDaysOfWeek(reader.ReadElementValue());
                            return true;
                        }
                }
                return false;
            }
        }
        /// <summary>
        /// Represents on fly room locations.
        /// </summary>
        public class OnflyTier : ComplexProperty
        {
            /// <summary>
            /// Default top tier
            /// </summary>
            public string DefTopTier { get; set; }
            /// <summary>
            /// Email for tech support contact appearing on the bottom of each window.
            /// </summary>
            public string DefMiddleTier { get; set; }

            public override object Clone()
            {
                OnflyTier copy = new OnflyTier();
                copy.DefTopTier = string.Copy(DefTopTier);
                copy.DefMiddleTier = string.Copy(DefMiddleTier);
                return copy;
            }

            internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
            {
                switch (reader.LocalName)
                {
                    case "DefTopTier":
                        {
                            DefTopTier = reader.ReadElementValue();
                            return true;
                        }
                    case "DefMiddleTier":
                        {
                            DefMiddleTier = reader.ReadElementValue();
                            return true;
                        }
                }
                return false;
            }
        }
        public OrganizationOptions()
        {
            Contact = new ContactDetails();
            AvailableTime = new SystemAvailableTime();
            OnFlyTier = new OnflyTier();
        }

        public ContactDetails Contact { get; private set; }
        /// <summary>
        /// If true, any approved conference modified in any way does not require reapproval.
        /// </summary>
        public bool AutoAcceptModifiedConference { get; set; }
        /// <summary>
        /// Allows users to schedule recurring conferences
        /// </summary>
        public bool IsRecurringConferenceEnabled { get; set; }
        /// <summary>
        /// If true, and if conference is labeled as public, then the Enable Open for Registration field is available. 
        /// This field enables anyone with access to the website to register for the conference.
        /// </summary>
        public bool IsDynamicInviteEnabled { get; set; }
        /// <summary>
        /// Enables point-to-point conferences.
        /// </summary>
        public bool IsPointToPointConferenceEnabled { get; set; }
        /// <summary>
        /// Legacy. Do not use.
        /// </summary>
        public bool IsRealtimeDisplayEnabled { get; set; }
        /// <summary>
        /// Enables user to set dial-out for a conference. If dial-out not enabled, users cannot set dial-out for any conferences.
        /// </summary>
        public bool IsDialoutEnabled { get; set; }
        /// <summary>
        /// Defaults all conferences to public. 
        /// </summary>
        public bool IsDefaultConferenceAsPublic { get; set; }
        /// <summary>
        /// Defaults conference type.
        /// </summary>
        public ConferenceType DefaultConferenceType { get; set; }
      

        public NetworkSwitchingOptions[] NetworkingOptions
        {
            get
            {
                var networkSwitchingOptions = new List<NetworkSwitchingOptions>();
                networkSwitchingOptions.Add(NetworkSwitchingOptions.NATOSecret);
                networkSwitchingOptions.Add(NetworkSwitchingOptions.NATOUnClassified);
                return networkSwitchingOptions.ToArray();
            }
        }

        public ConferenceType[] EnabledConferenceTypes
        {
            get
            {
                var enabledConferenceTypes = new List<ConferenceType>();
                if (IsRoomConferenceEnabled && EnableRoomSelection) //ALLDEV-834
                {
                    enabledConferenceTypes.Add(ConferenceType.RoomConference);
                }
                if (IsPointToPointConferenceEnabled)
                {
                    enabledConferenceTypes.Add(ConferenceType.PointToPoint);
                }
                if (IsAudioOnlyConferenceEnabled)
                {
                    enabledConferenceTypes.Add(ConferenceType.AudioOnly);
                }
                if (IsAudioVideoConferenceEnabled)
                {
                    enabledConferenceTypes.Add(ConferenceType.AudioVideo);
                    //enabledConferenceTypes.Add(ConferenceType.VMR);
                }
                if (IsHotDesking)
                {
                    enabledConferenceTypes.Add(ConferenceType.Hotdesking);
                }
                //ZD 103432 start
                if (IsOBTPConferenceEnabled)
                {
                    enabledConferenceTypes.Add(ConferenceType.OBTP);
                }
                //ZD 103432
                return enabledConferenceTypes.ToArray();
            }
        }


        public PCConferenceVendor[] EnabledPCConferenceTypes
        {
            get
            {
                var enabledPCConferenceTypes = new List<PCConferenceVendor>();
                enabledPCConferenceTypes.Add(PCConferenceVendor.None);
                var preference = MyVrmService.Service;//101456 
                if (preference.enableBlueJeans)
                    enabledPCConferenceTypes.Add(PCConferenceVendor.BlueJeans);
                if (preference.enableJabber)
                    enabledPCConferenceTypes.Add(PCConferenceVendor.Jabber);
                if (preference.enableLync)
                    enabledPCConferenceTypes.Add(PCConferenceVendor.Linc);
                if (preference.enableVidtel)
                    enabledPCConferenceTypes.Add(PCConferenceVendor.Vidtel);

                return enabledPCConferenceTypes.ToArray();
            }
        }

        public VMRConferenceType[] EnabledVMRConferenceTypes
        {
            get
            {
                var enabledVMRConferenceTypes = new List<VMRConferenceType>();
                enabledVMRConferenceTypes.Add(VMRConferenceType.None);
                //var preference = MyVrmService.Service.OrganizationSettings.Preference;
                if (this.EnablePersonaVMR)
                    enabledVMRConferenceTypes.Add(VMRConferenceType.Personal);
                if (this.EnableRoomVMR)
                    enabledVMRConferenceTypes.Add(VMRConferenceType.Room);
                if (this.EnableExternalVMR)
                    enabledVMRConferenceTypes.Add(VMRConferenceType.External);

                return enabledVMRConferenceTypes.ToArray();
            }
        }
        public eBJNMeetingType[] EnabledBJNTypes
        {
            get
            {
                var enabledbjntypes=new List<eBJNMeetingType>();
                enabledbjntypes.Add(eBJNMeetingType.OneTimeMeeting);
                enabledbjntypes.Add(eBJNMeetingType.PersonalMeeting);
               // enabledbjntypes.Add(eBJNMeetingType.None);
                return enabledbjntypes.ToArray();
            }
        }

        /// <summary>
        /// Enables room conferences.
        /// </summary>
        public bool IsRoomConferenceEnabled { get; set; }
        /// <summary>
        /// Enables Audio-Video conferences.
        /// </summary>
        public bool IsAudioVideoConferenceEnabled { get; set; }
        /// <summary>
        /// Enables Audio-only conferences.
        /// </summary>
        public bool IsAudioOnlyConferenceEnabled { get; set; }
        public bool IsOBTPConferenceEnabled { get; set; } //ZD 103432 
        public int DefaultCalendarToOfficeHours { get; set; }
        public string RoomTreeExpandLevel { get; set; }
        public bool IsCustomOptionEnabled { get; set; }
        public bool IsBufferZoneEnabled { get; set; }
        public bool IsConferenceCodeEnabled { get; set; }
        public bool IsLeaderPinEnabled { get; set; }
        public bool IsAdvancedAudioVideoParamsEnabled { get; set; }
        public bool IsAudioParamsEnabled { get; set; }
        public SystemAvailableTime AvailableTime { get; private set; }
        public byte[] MailLogoImage { get; set; }
        public int GuestLocApprovalTime { get; set; } //ZD 100818
        public int EnableGuestLocWarningMsg { get; set; }//ZD 100818
        //101456 starts       
        public bool ViewVMR { get; set; }
        public bool EnablePersonaVMR { get; set; }
        public bool EnableRoomVMR { get; set; }
        public bool EnableExternalVMR { get; set; }
        public bool IsHotDesking { get; set; }
        //101456 ends
        public bool EnableConferencePassword { get; set; }
        public int PasswordCharLength { get; set; }
        public bool EnableImmConf { get; set; }
        // 101864 starts
        public bool EnableCongSupport { get; set; }
        public bool MeetandGreetinEmail { get; set; }
        public bool OnSiteAVSupportinEmail { get; set; }
        public bool ConciergeMonitoringinEmail { get; set; }
        public bool DedicatedVNOCOperatorinEmail { get; set; }
        public bool EnableActMsgDelivery { get; set; }
        public OnflyTier OnFlyTier { get;private set; }
        public bool EnableAudioBridges { get; set; } //ZD 101838
        public int DefaultConfDuration { get; set; } //ZD 102200
        public string WebExURL{get;set;} //ZD 102193
        public int EnableWebExIntg {get;set;}
        public bool EnableRoomAdminDetails { get; set; }
        public bool EnableEPDetails { get; set; }
        public bool EnableStartMode { get; set; }
        public bool EnableConfTZinLoc { get; set; }
        public bool showBridgeExt { get; set; }
        public bool EnablePartyCode { get; set; }
        public int SetupTime { get; set; }
        public int TearDownTime { get; set; }
        public int MeetandGreetBuffer { get; set; } //102651
      //  public enum eBJNMeetingType { PersonalMeeting = 1, OneTimeMeeting = 2 }; //ZD 104021 //103947
        public bool EnableBJNIntegration { get; set; }//103947
        public bool BJNSelectOption { get; set; }//103947
        public eBJNMeetingType BJNMeetingType { get; set; }//103947
        public bool BJNDisplay { get; set; }
        public bool EnableRoomSelection { get; set; }//ALLDEV-834
        public bool EnableAudbridgefreebusy { get; set; }//ALLDEV-814
       
     
        internal void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElement)
        {
            do
            {
                reader.Read();
                switch (reader.LocalName)
                {
                    case "ContactDetails":
                        {
                            Contact.LoadFromXml(reader, reader.LocalName);
                            break;
                        }
                    case "AutoAcceptModifiedConference":
                        {
                            AutoAcceptModifiedConference = reader.ReadElementValue<bool>();
                            break;
                        }
                    case "EnableRecurringConference":
                        {
                            IsRecurringConferenceEnabled = reader.ReadElementValue<bool>();
                            break;
                        }
                    case "EnableDynamicInvite":
                        {
                            IsDynamicInviteEnabled = reader.ReadElementValue<bool>();
                            break;
                        }
                    case "EnableP2PConference":
                        {
                            IsPointToPointConferenceEnabled = reader.ReadElementValue<bool>();
                            break;
                        }
                    case "EnableRealtimeDisplay":
                        {
                            IsRealtimeDisplayEnabled = reader.ReadElementValue<bool>();
                            break;
                        }
                    case "EnableDialout":
                        {
                            IsDialoutEnabled = reader.ReadElementValue<bool>();
                            break;
                        }
                    case "DefaultConferencesAsPublic":
                        {
                            IsDefaultConferenceAsPublic = reader.ReadElementValue<bool>();
                            break;
                        }
                    case "DefaultConferenceType":
                        {
                            DefaultConferenceType = reader.ReadElementValue<ConferenceType>();
                            break;
                        }
                    case "EnableRoomConference":
                        {
                            IsRoomConferenceEnabled = reader.ReadElementValue<bool>();
                            break;
                        }
                    case "EnableHotdeskingConference":
                        {
                            IsHotDesking = reader.ReadElementValue<bool>();
                            break;
                        }
                    case "EnableAudioVideoConference":
                        {
                            IsAudioVideoConferenceEnabled = reader.ReadElementValue<bool>();
                            break;
                        }
                    case "EnableAudioOnlyConference":
                        {
                            IsAudioOnlyConferenceEnabled = reader.ReadElementValue<bool>();
                            break;
                        }
                    //    //ZD 103432 start
                    case "EnableWETConference":
                        {
                            IsOBTPConferenceEnabled = reader.ReadElementValue<bool>();
                            break;
                        }
                    //    //ZD 103432 End
                    case "DefaultCalendarToOfficeHours":
                        {
                            DefaultCalendarToOfficeHours = reader.ReadElementValue<int>();
                            break;
                        }
                    case "RoomTreeExpandLevel":
                        {
                            RoomTreeExpandLevel = reader.ReadElementValue();
                            break;
                        }
                    case "EnableCustomOption":
                        {
                            IsCustomOptionEnabled = reader.ReadElementValue<bool>();
                            break;
                        }
                    case "EnableBufferZone":
                        {
                            IsBufferZoneEnabled = reader.ReadElementValue<bool>();
                            break;
                        }
                    case "ConferenceCode":
                        {
                            IsConferenceCodeEnabled = reader.ReadElementValue<bool>();
                            break;
                        }
                    case "LeaderPin":
                        {
                            IsLeaderPinEnabled = reader.ReadElementValue<bool>();
                            break;
                        }
                    case "AdvAvParams":
                        {
                            IsAdvancedAudioVideoParamsEnabled = reader.ReadElementValue<bool>();
                            break;
                        }
                    case "AudioParams":
                        {
                            IsAudioParamsEnabled = reader.ReadElementValue<bool>();
                            break;
                        }
                    case "SystemAvailableTime":
                        {
                            AvailableTime.LoadFromXml(reader, reader.LocalName);
                            break;
                        }
                    case "MailLogoImage":
                        {
                            MailLogoImage = reader.ReadBase64ElementValue();
                            break;
                        }
                    //ZD 100818 Starts
                    case "GuestLocApprovalTime":
                        {
                            GuestLocApprovalTime = reader.ReadElementValue<int>();
                            break;
                        }
                    case "EnableGuestLocWarningMsg":
                        {
                            EnableGuestLocWarningMsg = reader.ReadElementValue<int>();
                            break;
                        }
                    //ZD 100818 Ends
                    //101456 starts
                    case "ShowHideVMR":
                        {
                            ViewVMR = reader.ReadElementValue<bool>();
                            break;
                        }
                    case "EnablePersonaVMR":
                        {
                            EnablePersonaVMR = reader.ReadElementValue<bool>();
                            break;
                        }
                    case "EnableRoomVMR":
                        {
                            EnableRoomVMR = reader.ReadElementValue<bool>();
                            break;
                        }
                    case "EnableExternalVMR":
                        {
                            EnableExternalVMR = reader.ReadElementValue<bool>();
                            break;
                        }
                    //101456 ends
                    case "EnableConferencePassword":
                        {
                            EnableConferencePassword = reader.ReadElementValue<bool>();
                            break;
                        }
                    case "EnableImmConf":
                        {
                            EnableImmConf = reader.ReadElementValue<bool>();
                            break;
                        }
                    case "PasswordCharLength":
                        {
                            PasswordCharLength = reader.ReadElementValue<int>();
                            break;
                        }
                    case "EnableCongSupport":
                        {
                            EnableCongSupport = reader.ReadElementValue<bool>();
                            break;
                        }
                    case "MeetandGreetinEmail":
                        {
                            MeetandGreetinEmail = reader.ReadElementValue<bool>();
                            break;
                        }
                    case "OnSiteAVSupportinEmail":
                        {
                            OnSiteAVSupportinEmail = reader.ReadElementValue<bool>();
                            break;
                        }
                    case "ConciergeMonitoringinEmail":
                        {
                            ConciergeMonitoringinEmail = reader.ReadElementValue<bool>();
                            break;
                        }
                    case "DedicatedVNOCOperatorinEmail":
                        {
                            DedicatedVNOCOperatorinEmail = reader.ReadElementValue<bool>();
                            break;
                        }
                    case "EnableActMsgDelivery":
                        {
                            EnableActMsgDelivery = reader.ReadElementValue<bool>();
                            break;
                        }
                    case "OnflyTier":
                        {
                            OnFlyTier.LoadFromXml(reader, reader.LocalName);
                            break;
                        }
                    case "EnableAudioBridges":
                        {
                            EnableAudioBridges = reader.ReadElementValue<bool>();
                            break;
                        }
                    case "DefaultConfDuration": //ZD 102200
                        {
                            DefaultConfDuration = reader.ReadElementValue<int>();
                            break;
                        }
                    case "EnableWebExIntg": 
                        {
                            EnableWebExIntg = reader.ReadElementValue<int>();
                            break;
                        }
                    case "WebExURL":
                        {
                            WebExURL = reader.ReadElementValue();
                            break;
                        }
                    case "EnableRoomAdminDetails":
                        {
                            EnableRoomAdminDetails = reader.ReadElementValue<bool>();
                            break;
                        }
                    case "EnableEPDetails":
                    {
                        EnableEPDetails = reader.ReadElementValue<bool>();
                        break;
                    }
                    case "EnableStartMode":
                    {
                        EnableStartMode = reader.ReadElementValue<bool>();
                        break;
                    }
                    case "EnableConfTZinLoc":
                    {
                        EnableConfTZinLoc = reader.ReadElementValue<bool>();
                        break;
                    }
                    case "showBridgeExt":
                    {
                        showBridgeExt = reader.ReadElementValue<bool>();
                        break;
                    }
                    case "EnablePartyCode":
                    {
                        EnablePartyCode = reader.ReadElementValue<bool>();
                        break;
                    }
                    // 102651
                    case "MeetandGreetBuffer":
                    {
                        MeetandGreetBuffer = reader.ReadElementValue<int>();
                        break;
                    }
                    case "SetupTime":
                    {
                        SetupTime = reader.ReadElementValue<int>();
                        break;
                    }

                    case "TearDownTime":
                    {
                        TearDownTime = reader.ReadElementValue<int>();
                        break;
                    }
                    //103947 starts
                    case "EnableBJNIntegration":
                    {
                        EnableBJNIntegration = reader.ReadElementValue<bool>();
                        break;
                    }
                    case "BJNSelectOption":
                    {
                        BJNSelectOption = reader.ReadElementValue<bool>();
                        break;
                    }
                    case "BJNMeetingType":
                    {
                        BJNMeetingType = reader.ReadElementValue<eBJNMeetingType>();
                        break;
                    }
                    case "BJNDisplay":
                    {
                        BJNDisplay = reader.ReadElementValue<bool>();
                         break;
                    }
                    //103947 Ends
                    //ALLDEV-834 Start
                    case "EnableRoomSelection":
                    {
                        EnableRoomSelection = reader.ReadElementValue<bool>();
                        break;
                    }
                    //ALLDEV-834 End
                    case "EnableAudbridgefreebusy": //ALLDEV-814 start
                    {
                        EnableAudbridgefreebusy = reader.ReadElementValue<bool>();
                        break;
                    }//ALLDEV-814 End

                    default:
                        {
                            reader.SkipCurrentElement();
                            break;
                        }
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, xmlElement));
        }
    }
}
