﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace MyVrm.WebServices.Data
{
    /// <summary>
    /// Represents a room in myVRM Web Services.
    /// </summary>
    [ServiceObjectDefinition("room")]
    [Serializable]
    public class Room : ServiceObject, ISerializable
    {
        /// <summary>
        /// Creates a new instance of the <see cref="Room"/> class.
        /// </summary>
        /// <param name="service"></param>
        public Room(MyVrmService service) : base(service)
        {
        }

        public Room(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
        /// <summary>
        /// Gets room ID.
        /// </summary>
        public RoomId Id
        {
            get
            {
                return (RoomId) PropertyBag[RoomSchema.Id];
            }
        }
        /// <summary>
        /// Gets or sets room name.
        /// </summary>
        public string Name
        {
            get
            {
                return (string) PropertyBag[RoomSchema.Name];
            }
            set
            {
                PropertyBag[RoomSchema.Name] = value;
            }
        }
        /// <summary>
        /// Gets or sets room phone number.
        /// </summary>
        /// <remarks>Include prefixes such as country code and access codes.</remarks>
        public string Phone
        {
            get
            {
                return (string)PropertyBag[RoomSchema.Phone];
            }
            set
            {
                PropertyBag[RoomSchema.Phone] = value;
            }
        }
        /// <summary>
        /// Gets or sets maximum seating capacity of room. 
        /// </summary>
        /// <remarks>For informational use only; does not restrict the number of participants to be invited.</remarks>
        public int Capacity
        {
            get
            {
                int iRet = 0;
				if (PropertyBag[RoomSchema.Capacity] != null)
            		int.TryParse(PropertyBag[RoomSchema.Capacity].ToString(), out iRet);
            	return iRet; //(int)PropertyBag[RoomSchema.Capacity];
            }
            set
            {
                PropertyBag[RoomSchema.Capacity] = value;
            }
        }
        /// <summary>
        /// Gets or sets number of phone ports in the room that can be used at the same time.
        /// </summary>
        public int PhoneLinesCount
        {
            get
            {
                int iRet = 0;
				if (PropertyBag[RoomSchema.PhoneLinesCount] != null)
					int.TryParse(PropertyBag[RoomSchema.PhoneLinesCount].ToString(), out iRet);
				return iRet; //(int)PropertyBag[RoomSchema.PhoneLinesCount];
            }
            set
            {
                PropertyBag[RoomSchema.PhoneLinesCount] = value;
            }
        }
        /// <summary>
        /// Gets or sets amount of time needed to physically set up room furniture. For information only.
        /// </summary>
        public int SetupTime
        {
            get
            {
                int iRet = 0;
				if (PropertyBag[RoomSchema.SetupTime] != null)
					int.TryParse(PropertyBag[RoomSchema.SetupTime].ToString(), out iRet);
				return iRet; //(int)PropertyBag[RoomSchema.SetupTime];
            }
            set
            {
                PropertyBag[RoomSchema.SetupTime] = value;
            }
        }
        /// <summary>
        /// Gets or sets amount of time needed to teardown room furniture. For information only.
        /// </summary>
        public int TeardownTime
        {
            get
            {
                int iRet = 0;
				if (PropertyBag[RoomSchema.TeardownTime] != null)
					int.TryParse(PropertyBag[RoomSchema.TeardownTime].ToString(), out iRet);
				return iRet; //(int)PropertyBag[RoomSchema.TeardownTime];
            }
            set
            {
                PropertyBag[RoomSchema.TeardownTime] = value;
            }
        }
        /// <summary>
        /// Gets or sets the person receives all email notifications concerning the room.
        /// </summary>
        /// <remarks>This individual is not necessarily a room approver. Nor is this person necessarily the individual who performs any setup/teardown actions on the room.</remarks>
        public UserId AssistantId
        {
            get
            {
                return (UserId)PropertyBag[RoomSchema.AssistantId];
            }
            set
            {
                PropertyBag[RoomSchema.AssistantId] = value;
            }
        }
        public string AssistantName
        {
            get
            {
                return (string)PropertyBag[RoomSchema.AssistantName];
            }
        }

        public string AssistantInchargeEmail
        {
            get
            {
                return (string)PropertyBag[RoomSchema.AssistantInchargeEmail];
            }
        }

        /// <summary>
        /// Gets or sets Tier 1 (top tier).
        /// </summary>
        public TierId TopTierId
        {
            get
            {
                return (TierId)PropertyBag[RoomSchema.TopTierId];
            }
            set
            {
                PropertyBag[RoomSchema.TopTierId] = value;
            }
        }

        public string TopTierName
        {
            get
            {
                return (string)PropertyBag[RoomSchema.TopTierName];
            }
        }
        /// <summary>
        /// Gets or sets Tier 2 (middle tier).
        /// </summary>
        public TierId MiddleTierId
        {
            get
            {
                return (TierId)PropertyBag[RoomSchema.MiddleTierId];
            }
            set
            {
                PropertyBag[RoomSchema.MiddleTierId] = value;
            }
        }

        public string MiddleTierName
        {
            get
            {
                return (string)PropertyBag[RoomSchema.MiddleTierName];
            }
        }
        /// <summary>
        /// Gets or sets building floor. For informational use only.
        /// </summary>
        public string Floor
        {
            get
            {
                return (string)PropertyBag[RoomSchema.Floor];
            }
            set
            {
                PropertyBag[RoomSchema.Floor] = value;
            }
        }
        /// <summary>
        /// Gets or sets room number or name. For informational use only.
        /// </summary>
        public string Number
        {
            get
            {
                return (string)PropertyBag[RoomSchema.Number];
            }
            set
            {
                PropertyBag[RoomSchema.Number] = value;
            }
        }
        /// <summary>
        /// Gets or sets the street address for the room.
        /// </summary>
        public string StreetAddress1
        {
            get
            {
                return (string)PropertyBag[RoomSchema.StreetAddress1];
            }
            set
            {
                PropertyBag[RoomSchema.StreetAddress1] = value;
            }
        }
        /// <summary>
        /// Gets or sets the street address for the room.
        /// </summary>
        public string StreetAddress2
        {
            get
            {
                return (string)PropertyBag[RoomSchema.StreetAddress2];
            }
            set
            {
                PropertyBag[RoomSchema.StreetAddress2] = value;
            }
        }
        /// <summary>
        /// Gets or sets the city for the room.
        /// </summary>
        public string City
        {
            get
            {
                return (string)PropertyBag[RoomSchema.City];
            }
            set
            {
                PropertyBag[RoomSchema.City] = value;
            }
        }
        /// <summary>
        /// Gets or sets state or province for the room.
        /// </summary>
        public string StateId
        {
            get
            {
                return (string)PropertyBag[RoomSchema.StateId];
            }
            set
            {
                PropertyBag[RoomSchema.StateId] = value;
            }
        }
        public string State
        {
            get
            {
                return (string)PropertyBag[RoomSchema.State];
            }
        }
        /// <summary>
        /// Gets or sets the postal code for the room.
        /// </summary>
        public string ZipCode
        {
            get
            {
                return (string)PropertyBag[RoomSchema.ZipCode];
            }
            set
            {
                PropertyBag[RoomSchema.ZipCode] = value;
            }
        }
        /// <summary>
        /// Gets or sets the country id for the room.
        /// </summary>
        public string CountryId
        {
            get
            {
                return (string)PropertyBag[RoomSchema.CountryId];
            }
            set
            {
                PropertyBag[RoomSchema.CountryId] = value;
            }
        }
        /// <summary>
        /// Gets the country for the room.
        /// </summary>
        public string Country
        {
            get
            {
                return (string)PropertyBag[RoomSchema.Country];
            }
            set
            {
                PropertyBag[RoomSchema.Country] = value;
            }
        }

        /// <summary>
        /// Gets the country for the room.
        /// </summary>
        public string InternalNumber
        {
            get
            {
                return (string)PropertyBag[RoomSchema.InternalNumber];
            }
            set
            {
                PropertyBag[RoomSchema.InternalNumber] = value;
            }
        }

        /// <summary>
        /// Gets the country for the room.
        /// </summary>
        public string ExternalNumber
        {
            get
            {
                return (string)PropertyBag[RoomSchema.ExternalNumber];
            }
            set
            {
                PropertyBag[RoomSchema.Country] = ExternalNumber;
            }
        }



        /// <summary>
        /// 
        /// </summary>
        public bool IsVMR
        {
            get
            {
                bool bRet = false;
                if (PropertyBag[RoomSchema.isVMR] != null)
                    bool.TryParse(PropertyBag[RoomSchema.isVMR].ToString(), out bRet);
                return bRet;//(bool)PropertyBag[RoomSchema.HandicappedAccess];
            }
            set
            {
                PropertyBag[RoomSchema.isVMR] = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool HandicappedAccess
        {
            get
            {
				bool bRet = false;
				if (PropertyBag[RoomSchema.HandicappedAccess] != null)
					bool.TryParse(PropertyBag[RoomSchema.HandicappedAccess].ToString(), out bRet);
				return bRet;//(bool)PropertyBag[RoomSchema.HandicappedAccess];
            }
            set
            {
                PropertyBag[RoomSchema.HandicappedAccess] = value;
            }
        }

        public string MapLink
        {
            get
            {
                return (string)PropertyBag[RoomSchema.MapLink];
            }
            set
            {
                PropertyBag[RoomSchema.MapLink] = value;
            }
        }

        public string ParkingDirections
        {
            get
            {
                return (string)PropertyBag[RoomSchema.ParkingDirections];
            }
            set
            {
                PropertyBag[RoomSchema.ParkingDirections] = value;
            }
        }
        /// <summary>
        /// Gets or sets the additional comments.
        /// </summary>
        public string AdditionalComments
        {
            get
            {
                return (string)PropertyBag[RoomSchema.AdditionalComments];
            }
            set
            {
                PropertyBag[RoomSchema.AdditionalComments] = value;
            }
        }
        /// <summary>
        /// Gets or sets the timezone.
        /// </summary>
        public TimeZoneInfo TimeZone
        {
            get
            {
                return (TimeZoneInfo)PropertyBag[RoomSchema.TimeZone];
            }
            set
            {
                PropertyBag[RoomSchema.TimeZone] = value;
            }
        }
        /// <summary>
        /// Gets or sets the longitude.
        /// </summary>
        public string Longitude
        {
            get
            {
                return (string)PropertyBag[RoomSchema.Longitude];
            }
            set
            {
                PropertyBag[RoomSchema.Longitude] = value;
            }
        }
        /// <summary>
        /// Gets or sets the latitude.
        /// </summary>
        public string Latitude
        {
            get
            {
                return (string)PropertyBag[RoomSchema.Latitude];
            }
            set
            {
                PropertyBag[RoomSchema.Latitude] = value;
            }
        }
        /// <summary>
        /// Gets or sets room approvers. See <seealso cref="RoomApprovers"/>.
        /// </summary>
        public RoomApprovers Approvers
        {
            get
            {
                return (RoomApprovers)PropertyBag[RoomSchema.Approvers];
            }
            set
            {
                PropertyBag[RoomSchema.Approvers] = value;
            }
        }
        /// <summary>
        /// Gets or sets endpoint ID for the room.
        /// </summary>
        /// <remarks>Endpoint must be set if <see cref="Media"/> is <see cref="MediaType.AudioVideo"/> or <see cref="MediaType.AudioOnly"/>. 
        /// If an endpoint is not set (that is, not associated with a room), the room is not available for an audio or audio/video conference.</remarks>
        public EndpointId EndpointId
        {
            get
            {
                return (EndpointId)PropertyBag[RoomSchema.EndpointId];
            }
            set
            {
                PropertyBag[RoomSchema.EndpointId] = value;
            }
        }
        /// <summary>
        /// Gets or sets if projector is available for the room.
        /// </summary>
        public bool Projector
        {
            get
            {
				bool bRet = false;
				if (PropertyBag[RoomSchema.Projector] != null)
					bool.TryParse(PropertyBag[RoomSchema.Projector].ToString(), out bRet);
				return bRet;//(bool)PropertyBag[RoomSchema.Projector];
            }
            set
            {
                PropertyBag[RoomSchema.Projector] = value;
            }
        }
        /// <summary>
        /// Gets or sets if the room is available for a specific conference type.
        /// </summary>
        public MediaType Media
        {
            get
            {
                return (MediaType)PropertyBag[RoomSchema.Media];
            }
            set
            {
                PropertyBag[RoomSchema.Media] = value;
            }
        }
		//
    	public RoomCategoryType RoomCategory
    	{
			get
			{
				return (RoomCategoryType)PropertyBag[RoomSchema.RoomCategory];
			}
			set
			{
				PropertyBag[RoomSchema.RoomCategory] = value;
			}
    	}
        /// <summary>
        /// Gets or sets if the room furniture can be moved, as necessary.
        /// </summary>
        /// <remarks>This is for information purposes only.</remarks>
        public bool DynamicLayout
        {
            get
            {
				bool bRet = false;
				if (PropertyBag[RoomSchema.DynamicLayout] != null)
					bool.TryParse(PropertyBag[RoomSchema.DynamicLayout].ToString(), out bRet);
				return bRet;//(bool)PropertyBag[RoomSchema.DynamicLayout];
            }
            set
            {
                PropertyBag[RoomSchema.DynamicLayout] = value;
            }
        }
        /// <summary>
        /// Gets or sets if if the room has a caterer facility.
        /// </summary>
        /// <remarks>This is for information purposes only.</remarks>
        public bool CateringFacility
        {
            get
            {
				bool bRet = false;
				if (PropertyBag[RoomSchema.CateringFacility] != null)
					bool.TryParse(PropertyBag[RoomSchema.CateringFacility].ToString(), out bRet);
				return bRet;//(bool)PropertyBag[RoomSchema.CateringFacility];
            }
            set
            {
                PropertyBag[RoomSchema.CateringFacility] = value;
            }
        }
        /// <summary>
        /// Room departments.
        /// </summary>
        public DepartmentCollection Departments
        {
            get
            {
                return (DepartmentCollection)PropertyBag[RoomSchema.Departments];
            }
            set
            {
                PropertyBag[RoomSchema.Departments] = value;
            }
        }

        public RoomImageCollection RoomImages
        {
            get
            {
                return (RoomImageCollection)PropertyBag[RoomSchema.RoomImages];
            }
            set
            {
                PropertyBag[RoomSchema.RoomImages] = value;
            }
        }

        public string RoomImageName
        {
            get
            {
                return (string)PropertyBag[RoomSchema.RoomImageName];
            }
            set
            {
                PropertyBag[RoomSchema.RoomImageName] = value;
            }
        }

        public RoomImages Images
        {
            get
            {
                return (RoomImages)PropertyBag[RoomSchema.Images];
            }
            set
            {
                PropertyBag[RoomSchema.Images] = value;
            }
        }
        /// <summary>
        /// Saves the room or if this is a new room then creates this.
        /// </summary>
        public void Save()
        {
            Service.SaveRoom(this);
        }
        /// <summary>
        /// Deletes the room.
        /// </summary>
        public void Delete()
        {
            Service.DeleteRoom(Id);
        }

        #region Overrides of ServiceObject

        internal override ServiceObjectSchema GetSchema()
        {
            return RoomSchema.Instance;
        }

        #endregion

        internal override PropertyDefinition GetIdPropertyDefinition()
        {
            return RoomSchema.Id;
        }

        #region ISerializable Members
        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }

        #endregion
    }
}
