﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;

namespace MyVrm.WebServices.Data
{
    [Serializable]
    public class RoomId : ServiceId
    {
		public static readonly RoomId NewRoomId = new RoomId(Constants.NewRoomId);
        public RoomId()
        {
        }

        public RoomId(string id) : base(id)
        {
        }

        private string _roomName;

        public string RoomName
        {
            get { return _roomName; }
            set { _roomName = value; }
        }

        #region Overrides of ServiceId

        internal override string GetXmlElementName()
        {
            return "roomID";
        }

        #endregion
    }
}
