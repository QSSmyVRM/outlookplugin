﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	//Request
	class GetPublicRoomsAvailabilityRequest : ServiceRequestBase<GetPublicRoomsAvailabilityResponse>
	{
		private RoomIdCollection _roomList;
		private int _isRecurring;
		private ConferenceId _confID;
		private bool _immediate;
		private DateTime _startDate;
		private int _startHour;
		private int _startMin;
		private string _startSet;
		private int _timeZone;
		private int _durationMin;

		public RoomIdCollection RoomList
		{
			set { _roomList = value; }
			get { return _roomList; }
		}
		public int IsRecurring
		{
			set { _isRecurring = value; }
			get { return _isRecurring; }
		}

		public ConferenceId ConfID 
		{
			set { _confID = value; }
			get { return _confID; }
		}

		public bool IsImmediate
		{
			set { _immediate = value; }
			get { return _immediate; }
		}

		public DateTime StartDate
		{
			set { _startDate = value; }
			get { return _startDate; }
		}
        public int StartHour
		{
			set { _startHour = value; }
			get { return _startHour; }
		}
		public int StartMin
		{
			set { _startMin = value; }
			get { return _startMin; }
		}
		public string StartSet
		{
			set { _startSet = value; }
			get { return _startSet; }
		}
		public int TimeZone
		{
			set { _timeZone = value; }
			get { return _timeZone; }
		}
		public int DurationMin
		{
			set { _durationMin = value; }
			get { return _durationMin; }
		}

		internal GetPublicRoomsAvailabilityRequest(MyVrmService service)
			: base(service)
		{
		}

		#region Overrides of ServiceRequestBase<GetPublicRoomsAvailabilityResponse>

		internal override string GetXmlElementName()
		{
			return Constants.GetPublicRoomsAvailabilityCommandName; 
		}

		internal override string GetCommandName()
		{
			return Constants.GetPublicRoomsAvailabilityCommandName;
		}

		internal override string GetResponseXmlElementName()
		{
			return "GetPublicRoomsAvailability";
		}

		internal override GetPublicRoomsAvailabilityResponse ParseResponse(MyVrmServiceXmlReader reader)
		{
			var response = new GetPublicRoomsAvailabilityResponse();
			response.ReadElementsFromXml(reader);
			return response;
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			if (UserId != null)
				UserId.WriteToXml(writer, "userID");
			ConfID.WriteToXml(writer, "confID");
			writer.WriteElementValue(XmlNamespace.NotSpecified, "immediate", IsImmediate ? 1 : 0);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "recurring", IsRecurring);
      
			string rooms = string.Empty;
			foreach (RoomId room in RoomList)
			{
				if( rooms.Length > 0 )
					rooms += ",";
				rooms += room.Id;
			}
			writer.WriteElementValue(XmlNamespace.NotSpecified, "selectedLocationID", rooms);
			writer.WriteStartElement(XmlNamespace.NotSpecified, "conferenceTime");
			writer.WriteElementValue(XmlNamespace.NotSpecified, "startDate", Utilities.DateToString(StartDate.Date));
			writer.WriteElementValue(XmlNamespace.NotSpecified, "startHour", StartHour);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "startMin", StartMin);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "startSet", StartSet);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "timeZone", TimeZone);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "durationMin", DurationMin);
			writer.WriteEndElement();
		}

		#endregion
	}

	//Response
	public class GetPublicRoomsAvailabilityResponse : ServiceResponse
	{
		//Result 
		private readonly List<RoomId> _availRooms = new List<RoomId>();
		public List<RoomId> AvailRooms
		{
			get { return _availRooms; }
		}

		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{
			do
			{
				//Forward till "Location" element 
				while (!reader.IsEndElement(XmlNamespace.NotSpecified, "Location"))
				{
					while (!reader.IsStartElement(XmlNamespace.NotSpecified, "Location") //new entry or
					       && !reader.IsEndElement(XmlNamespace.NotSpecified, "Locationlist")) //empty list
						reader.Read();
					if (reader.IsEndElement(XmlNamespace.NotSpecified, "Locationlist"))
						break;
					if (reader.LocalName == "Location")
					{
						string room = string.Empty;
						int avail = 0;
						while (!reader.IsEndElement(XmlNamespace.NotSpecified, "Location"))
						{
							if (reader.IsStartElement(XmlNamespace.NotSpecified, "LocationID"))
							{
								room = reader.ReadElementValue();
							}
							else
							{
								if (reader.IsStartElement(XmlNamespace.NotSpecified, "Available"))
								{
									avail = reader.ReadElementValue<int>();
								}
								else
								{
									reader.Read();
								}
							}
						}
						if (avail > 0)
							_availRooms.Add(new RoomId(room));
					}
				}
				if (!reader.IsEndElement(XmlNamespace.NotSpecified, "Locationlist"))
					reader.Read();

			} while (!reader.IsEndElement(XmlNamespace.NotSpecified, "Locationlist")); //read till the "Locationlist" node end 
		}
	}
}
