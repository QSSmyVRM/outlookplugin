﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Reflection;

namespace MyVrm.WebServices.Data
{
    [Serializable]
    public class UserId : ServiceId
    {
        public static readonly UserId Default = new UserId("");
        public static readonly UserId DefaultUserId = new UserId("11");
        public static readonly UserId New = new UserId(Constants.NewServiceId);

        internal UserId()
        {
        }

		internal UserId(string userId): base(userId)
        {
        }

        #region Overrides of ServiceId

        internal override string GetXmlElementName()
        {
            return "userID";
        }

        #endregion
    }
}
