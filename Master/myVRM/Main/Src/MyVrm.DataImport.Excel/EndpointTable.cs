﻿using System;
using System.Linq;
using Microsoft.Office.Interop.Excel.Extensions;
using MyVrm.DataImport.Excel.Extensions;
using MyVrm.Common;
using MyVrm.Common.Collections;
using MyVrm.WebServices.Data;
using ListObject=Microsoft.Office.Tools.Excel.ListObject;

namespace MyVrm.DataImport.Excel
{
    public class EndpointTable : ExcelTable
    {
        private readonly ExcelTableColumn _idColumn;
        private readonly ExcelTableColumn _nameColumn;
        private readonly ExcelTableColumn _videoEquipmentColumn;
        private readonly ExcelTableColumn _addressColumn;
        private readonly ExcelTableColumn _addressTypeColumn;
        private readonly ExcelTableColumn _passwordColumn;
        private readonly ExcelTableColumn _lineRateColumn;
        private readonly ExcelTableColumn _connectionTypeColumn;
        private readonly ExcelTableColumn _bridgeNameColumn;
        private readonly ExcelTableColumn _isOutsideColumn;

        public EndpointTable(ListObject listObject, MyVrmService service, ValidationListManager validationListManager) : base(listObject, service, validationListManager)
        {
            var listColumns = ListObject.ListColumns;
            _idColumn = new ExcelTableColumn(listColumns[1]) {IsReadOnly = true};
            _nameColumn = new ExcelTableColumn(listColumns[2]) {IsRequired = true};
            _videoEquipmentColumn = new ExcelTableColumn(listColumns[3])
                                        {
                                            IsRequired = true,
                                            HasFixedSetOfValues = true,
                                            ValuesRangeName = ValidationListManager.GetVideoEquipmentRange()
                                        };
            _addressColumn = new ExcelTableColumn(listColumns[4]){IsRequired = true};
            _addressTypeColumn = new ExcelTableColumn(listColumns[5])
                                     {
                                         IsRequired = true,
                                         HasFixedSetOfValues = true,
                                         ValuesRangeName = ValidationListManager.GetAddressTypeRange()
                                     };
            _passwordColumn = new ExcelTableColumn(listColumns[6]) {IsRequired = true};
            _lineRateColumn = new ExcelTableColumn(listColumns[7])
                                  {
                                      IsRequired = true,
                                      HasFixedSetOfValues = true,
                                      ValuesRangeName = ValidationListManager.GetLineRateRange()
                                  };
            _connectionTypeColumn = new ExcelTableColumn(listColumns[8])
                                        {
                                            IsRequired = true,
                                            HasFixedSetOfValues = true,
                                            ValuesRangeName = ValidationListManager.GetConnectionTypeRange()
                                        };
            _bridgeNameColumn = new ExcelTableColumn(listColumns[9])
                                    {
                                        IsRequired = true,
                                        HasFixedSetOfValues = true,
                                        ValuesRangeName = ValidationListManager.GetBridgeNamesRange()
                                    };
            _isOutsideColumn = new ExcelTableColumn(listColumns[10])
                                   {
                                       HasFixedSetOfValues = true, 
                                       ValuesRangeName = ValidationListManager.GetYesNoRange()
                                   };

            TableColumns.AddRange(new[]
                                      {
                                          _idColumn, _nameColumn, _videoEquipmentColumn, _addressColumn,
                                          _addressTypeColumn, _passwordColumn, _lineRateColumn, _connectionTypeColumn,
                                          _bridgeNameColumn, _isOutsideColumn
                                      });
            Initialize();
        }

        #region Overrides of ExcelTable

        protected override void Save(SaveErrorCollection errors)
        {
            var videoEquipments = Service.GetVideoEquipment();
            var bridges = Service.GetBridges();
            var lineRates = Service.GetLineRate();
            var connectTypeEnumList = new EnumListSource(typeof (ConnectionType));
            foreach (var listRow in ListObject.ListRows.Items())
            {
                var endpoint = new Endpoint(Service);
                var profile = new EndpointProfile
                                          {
                                              Name = "Default",
                                              IsDefault = true,
                                              Deleted = false,
                                              EncryptionPreferred = false,
                                              Url = "",
                                              ConnectionType = ConnectionType.DialInToMCU,
                                              DefaultProtocol = 1, //IP
                                              BridgeId = BridgeId.Default,
                                              McuAddress = "",
                                              McuAddressType = -1, //default
                                              ExchangeId = "",
                                              IsOutside = false
                                          };

                try
                {
                    var range = listRow.Range;
                    endpoint.Name = range.Item(1, _nameColumn.Index).ValueAsString();
                    var equipment = range.Item(1, _videoEquipmentColumn.Index).ValueAsString();
                    profile.VideoEquipment = videoEquipments.First(match => match.Name == equipment).Id;
                
                    profile.Address = range.Item(1, _addressColumn.Index).ValueAsString();
                    profile.AddressType = AddressType.IpAddress;
                    profile.Password = range.Item(1, _passwordColumn.Index).ValueAsString();
                
                    var lineRate = range.Item(1, _lineRateColumn.Index).ValueAsString();
                    profile.LineRate = lineRates.First(match => match.Name == lineRate).Id;
                
                    var connectionType = range.Item(1, _connectionTypeColumn.Index).ValueAsString();
                    var list = connectTypeEnumList.GetList();
                    foreach (LocalizedEnum locEnum in list)
                    {
                        if (locEnum.Text.Equals(connectionType, StringComparison.OrdinalIgnoreCase))
                        {
                            profile.ConnectionType = (ConnectionType)locEnum.Value;
                            break;
                        }
                    }
                
                    var bridgeName = range.Item(1, _bridgeNameColumn.Index).ValueAsString();
                    if(!string.IsNullOrEmpty(bridgeName))
                    {
                        profile.BridgeId = bridges.First(match => match.Name == bridgeName).Id;
                    }
                
                    var isOutside = range.Item(1, _isOutsideColumn.Index).ValueAsString();
                    profile.IsOutside = "Yes".Equals(isOutside, StringComparison.OrdinalIgnoreCase);
                    endpoint.Profiles.Add(profile);
                    endpoint.Save();
                }
                catch (Exception e)
                {
                    errors.Add(new SaveError(endpoint.Name, e));
                }
            }
        }

        #endregion
    }
}
