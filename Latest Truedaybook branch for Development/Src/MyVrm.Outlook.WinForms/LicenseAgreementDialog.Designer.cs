﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.Outlook.WinForms
{
    partial class LicenseAgreementDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.eulaEditControl = new DevExpress.XtraRichEdit.RichEditControl();
            ((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).BeginInit();
            this.ContentPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ContentPanel
            // 
            this.ContentPanel.Controls.Add(this.eulaEditControl);
            this.ContentPanel.Size = new System.Drawing.Size(610, 393);
            // 
            // eulaEditControl
            // 
            this.eulaEditControl.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple;
            this.eulaEditControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.eulaEditControl.Location = new System.Drawing.Point(0, 0);
            this.eulaEditControl.Name = "eulaEditControl";
            this.eulaEditControl.ReadOnly = true;
            this.eulaEditControl.Size = new System.Drawing.Size(610, 393);
            this.eulaEditControl.TabIndex = 1;
            // 
            // LicenseAgreementDialog
            // 
            this.ApplyEnabled = true;
            this.ApplyVisible = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.ClientSize = new System.Drawing.Size(634, 449);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MinimumSize = new System.Drawing.Size(640, 480);
            this.Name = "LicenseAgreementDialog";
            this.Load += new System.EventHandler(this.LicenseAgreementDialog_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).EndInit();
            this.ContentPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

		private DevExpress.XtraRichEdit.RichEditControl eulaEditControl;
    }
}
