/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MyVrm.Common.Collections.Generic;

namespace MyVrm.Common.ComponentModel
{
    /// <summary>
    /// Provides a generic sortable collection 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class DataList<T> : BindingList<T>, ITypedList
    {
        private bool _allowSorting = true;
        private PropertyDescriptor _sortProperty;
        private ListSortDirection _sortDirection;

        #region ITypedList Members

        ///<summary>
        ///Returns the name of the list.
        ///</summary>
        ///
        ///<returns>
        ///The name of the list.
        ///</returns>
        ///
        ///<param name="listAccessors">An array of <see cref="T:System.ComponentModel.PropertyDescriptor"></see> objects, for which the list name is returned. This can be null. </param>
        public string GetListName(PropertyDescriptor[] listAccessors)
        {
            return typeof (T).Name;
        }

        ///<summary>
        ///Returns the <see cref="T:System.ComponentModel.PropertyDescriptorCollection"></see> that represents the properties on each item used to bind data.
        ///</summary>
        ///
        ///<returns>
        ///The <see cref="T:System.ComponentModel.PropertyDescriptorCollection"></see> that represents the properties on each item used to bind data.
        ///</returns>
        ///
        ///<param name="listAccessors">An array of <see cref="T:System.ComponentModel.PropertyDescriptor"></see> objects to find in the collection as bindable. This can be null. </param>
        public PropertyDescriptorCollection GetItemProperties(PropertyDescriptor[] listAccessors)
        {
            PropertyDescriptorCollection pdc = listAccessors == null ? GetProperties() : GetListItemProperties(listAccessors[0].PropertyType);
            return pdc;
        }

        #endregion

        /// <summary>
        /// Adds the elements of specified collection to the end of the <see cref="DataList{T}"/>.
        /// </summary>
        /// <param name="collection">The collection whose elements should be added to the end of <see cref="DataList{T}"/>. The collection itself cannot be a null reference, but it can contain elements that are a null reference, if type {T} is a reference type.</param>
        public void AddRange(IEnumerable<T> collection)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }
            foreach (T listItem in collection)
            {
                Add(listItem);
            }
        }

        /// <summary>
        /// Sorts the list based on a name of property and a ListSortDirection.
        /// </summary>
        /// <param name="propertyName">The propety to sort by</param>
        /// <param name="direction">One of the <see cref="System.ComponentModel.ListSortDirection"/> values.</param>
        /// <exception cref="System.ArgumentOutOfRangeException"><see cref="T:T"/> does not contain property with propertyName name.</exception>
        public void ApplySort(string propertyName, ListSortDirection direction)
        {
            PropertyDescriptor prop = GetProperties()[propertyName];
            if (prop == null)
            {
                throw new ArgumentOutOfRangeException("propertyName");
            }
            ApplySortCore(prop, direction);
        }

        /// <summary>
        /// Copies list items from a <see cref="IEnumerable"/> object.
        /// </summary>
        /// <param name="collection">The <see cref="IEnumerable"/> object to base the new list on.</param>
        public void CopyFrom(IEnumerable collection)
        {
            bool raisedListChangedEvents = RaiseListChangedEvents;
            RaiseListChangedEvents = false;
            PropertyDescriptor prop = _sortProperty;
            _sortProperty = null;
            try
            {
                Clear();
                if (collection != null)
                {
                    foreach (T listItem in collection)
                    {
                        Add(listItem);
                    }
                }
            }
            finally
            {
                RaiseListChangedEvents = raisedListChangedEvents;
                if (prop != null && SupportsSortingCore)
                {
                    ApplySortCore(prop, SortDirectionCore);
                }
                else
                {
                    ResetBindings();
                }
            }
        }

        public void CopyFrom(IEnumerable<T> collection)
        {
            CopyFrom((IEnumerable) collection);
        }

        /// <summary>
        /// Returns index of the row that has given property name.
        /// </summary>
        /// <param name="propertyName">The property name to search on.</param>
        /// <param name="key">The value of the <paramref name="propertyName"/> parameter to search for.</param>
        /// <returns>The index of the row that has given property name.</returns>
        public int Find(string propertyName, object key)
        {
            PropertyDescriptor prop = TypeDescriptor.GetProperties(typeof (T))[propertyName];
            return FindCore(prop, key);
        }
        /// <summary>
        /// Moves the item at specified position.
        /// </summary>
        /// <param name="item">The item to be moved.</param>
        /// <param name="index">The new position.</param>
        public void Move(T item, int index)
        {
            bool raiseListChangedEvents = RaiseListChangedEvents;
            try
            {
                RaiseListChangedEvents = false;
                int oldIndex = IndexOf(item);
                RemoveAt(oldIndex);
                InsertItem(index, item);

                OnListChanged(new ListChangedEventArgs(ListChangedType.ItemMoved, index, oldIndex));
            }
            finally
            {
                RaiseListChangedEvents = raiseListChangedEvents;
            }
        }
        /// <summary>
        /// Copies list items to an array.
        /// </summary>
        /// <returns>Array of copied list items.</returns>
        public T[] ToArray()
        {
            var array = new T[Count];
            CopyTo(array, 0);
            return array;
        }

        /// <summary>
        /// Gets or sets whether you can sort items in the list using ApplySort.
        /// </summary>
        public bool AllowSorting
        {
            get { return _allowSorting; }
            set
            {
                if (AllowSorting != value)
                {
                    _allowSorting = value;
                    ResetBindings();
                }
            }
        }

        protected override bool IsSortedCore
        {
            get { return (null != _sortProperty); }
        }

        protected override ListSortDirection SortDirectionCore
        {
            get { return _sortDirection; }
        }

        protected override PropertyDescriptor SortPropertyCore
        {
            get { return _sortProperty; }
        }

        protected override bool SupportsSearchingCore
        {
            get { return true; }
        }

        protected override bool SupportsSortingCore
        {
            get { return AllowSorting; }
        }

        protected override void ApplySortCore(PropertyDescriptor prop, ListSortDirection direction)
        {
            if (!SupportsSortingCore)
            {
                throw new NotSupportedException();
            }
            ((List<T>) Items).Sort(new PropertyComparer<T>(prop, direction));
            _sortProperty = prop;
            _sortDirection = direction;
            OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
        }

        protected override int FindCore(PropertyDescriptor property, object key)
        {
            if (property != null)
            {
                for (int i = 0; i < Items.Count; i++)
                {
                    object y = property.GetValue(Items[i]);
                    var keyStr = key as string;
                    if (keyStr != null)
                    {
                        var yStr = y as string;
                        if (yStr != null)
                        {
                            if (StringComparer.OrdinalIgnoreCase.Compare(key, y) == 0)
                            {
                                return i;
                            }
                        }
                    }
                    if (key != null)
                    {
                        if (key.Equals(y))
                        {
                            return i;
                        }
                    }
                }
            }
            return -1;
        }

        protected override void InsertItem(int index, T item)
        {
            if (IsSortedCore)
            {
                index =
                    ((List<T>) Items).BinarySearch(item,
                                                   new PropertyComparer<T>(SortPropertyCore, SortDirectionCore));
                if (index < 0)
                {
                    index = ~index;
                }
            }
            base.InsertItem(index, item);
        }

        protected override void RemoveSortCore()
        {
            _sortProperty = null;
            _sortDirection = ListSortDirection.Ascending;
            OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
        }

        protected override void SetItem(int index, T item)
        {
            base.SetItem(index, item);
            if (IsSortedCore)
            {
                ApplySortCore(_sortProperty, _sortDirection);
            }
        }

        private static PropertyDescriptorCollection GetProperties()
        {
            Type type = typeof (T);
            if (type.IsInterface)
            {
                var propertyDescriptors =
                    new PropertyDescriptorCollection(new PropertyDescriptor[] {}, false);
                PropertyDescriptorCollection thisInterfaceProperties = TypeDescriptor.GetProperties(type);
                foreach (PropertyDescriptor propertyDescriptor in thisInterfaceProperties)
                {
                    propertyDescriptors.Add(propertyDescriptor);
                }
                Type[] interfaceTypes = type.GetInterfaces();
                foreach (Type interfaceType in interfaceTypes)
                {
                    PropertyDescriptorCollection interfaceProperties = TypeDescriptor.GetProperties(interfaceType);
                    foreach (PropertyDescriptor propertyDescriptor in interfaceProperties)
                    {
                        propertyDescriptors.Add(propertyDescriptor);
                    }
                }
                return propertyDescriptors;
            }
            return TypeDescriptor.GetProperties(type);
        }

        private static PropertyDescriptorCollection GetListItemProperties(object list)
        {
            if (list == null)
            {
                return new PropertyDescriptorCollection(null);
            }
            if (list is Type)
            {
                return TypeDescriptor.GetProperties(list as Type, new Attribute[] {new BrowsableAttribute(true)});
            }
            return TypeDescriptor.GetProperties(list);
        }
    }
}