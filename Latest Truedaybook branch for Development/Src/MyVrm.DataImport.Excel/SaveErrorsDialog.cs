﻿using System.Windows.Forms;
using MyVrm.Common.ComponentModel;

namespace MyVrm.DataImport.Excel
{
    public partial class SaveErrorsDialog : Form
    {
        private readonly DataList<SaveError> _errors = new DataList<SaveError>();

        public SaveErrorsDialog()
        {
            InitializeComponent();
            errorsListView.DataSource = _errors;
        }

        public SaveErrorCollection Errors
        {
            set
            {
                _errors.AddRange(value);
            }
        }

        private void errorsListView_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (errorsListView.SelectedIndices.Count > 0)
            {
                errorTextLabel.Text = _errors[errorsListView.SelectedIndices[0]].ErrorText;
            }
        }

        private void SaveErrorsDialog_Load(object sender, System.EventArgs e)
        {
            if (errorsListView.Items.Count > 0)
            {
                errorsListView.Items[0].Selected = true;
            }
        }
    }
}
