﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;


namespace MyVrm.WebServices.Data
{
    internal class GetOldBridgeRequest : ServiceRequestBase<GetOldBridgeResponse>
    {
        public GetOldBridgeRequest(MyVrmService service)
            : base(service)
        {
        }

        public ConferenceId contentid { get; set; }
        // public string ICalId { get; set; }


        public int bridgeID { get; set; }

        #region Overrides of ServiceRequestBase<GetEmailContentResponse>

        internal override string GetXmlElementName()
        {
            return "GetOldBridge";
        }

        internal override string GetCommandName()
        {
            return Constants.GetOldBridge;
        }

        internal override string GetResponseXmlElementName()
        {
            return "bridge";
        }

        internal override GetOldBridgeResponse ParseResponse(MyVrmServiceXmlReader reader)
        {
            var response = new GetOldBridgeResponse();
            response.LoadFromXml(reader, GetResponseXmlElementName());
            return response;
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            writer.WriteStartElement(XmlNamespace.NotSpecified, "login");
            UserId.WriteToXml(writer, "userID");
            MyVrmService.Service.OrganizationId.WriteToXml(writer, "organizationID");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "bridgeID", bridgeID);
        }

        #endregion
    }

    public class GetOldBridgeResponse : ServiceResponse
    {
        public string BridgeExtNo { get; internal set; }
        public string E164Dialnumber { get; internal set; }
        //Read template info and template conference instance 
        internal void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
        {
           try
            {
                do
                {
                    reader.Read();
                    if (reader.LocalName != "" && reader.LocalName != null)
                    {
                        switch (reader.LocalName)
                        {
                            case "bridgeDetails":
                               LoadFromXml(reader, "bridgeDetails");
                               //reader.Read();
                               break;
                            case "BridgeExtNo":
                                BridgeExtNo = reader.ReadElementValue();
                                break;
                            case "portA":
                                E164Dialnumber = reader.ReadElementValue();
                                break;
                            default:
                               reader.SkipCurrentElement();
                               break;
                              
                        }
                    }
                } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "bridge"));
            }
            catch (Exception ex)
            {
                MyVrmService.TraceSource.TraceError(reader.LocalName + ": " + ex.Message, null);
                //reader.Read();
                //throw;
            }
        }

    }




}