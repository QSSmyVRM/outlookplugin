﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections;
using System.Collections.Generic;

namespace MyVrm.WebServices.Data
{
    [Serializable]
    public class RoomImageCollection : ComplexProperty, IEnumerable<Image>
    {
        private readonly List<Image> _images = new List<Image>();

		public override object Clone()
		{
			RoomImageCollection copy = new RoomImageCollection();
			foreach (var item in _images)
			{
				copy._images.Add(item); //???????
			}

			return copy;
		}

        internal RoomImageCollection()
        {
        }

		public void SetImages(List<Image> images)
		{
			_images.Clear();
			_images.AddRange(images);
		}
        public int Count
        {
            get
            {
                return _images.Count;
            }
        }

        public Image this[int index]
        {
            get
            {
                return _images[index];
            }
        }

        internal override void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
        {
            do
            {
                reader.Read();
                if (reader.IsStartElement(XmlNamespace.NotSpecified, "ImageDetails"))
                {
                    string name = null;
                    byte[] data = null;
                    do
                    {
                        reader.Read();
                        if (reader.IsStartElement(XmlNamespace.NotSpecified, "ImageName"))
                        {
                            name = reader.ReadValue();
                        }
                        else if (reader.IsStartElement(XmlNamespace.NotSpecified, "Image"))
                        {
                            data = reader.ReadBase64ElementValue();
                        }
                        else
                        {
                            reader.SkipCurrentElement();
                        }
                    } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "ImageDetails"));
                    var image = new Image(name, data);
                    _images.Add(image);
                }
                else
                {
                    reader.SkipCurrentElement();
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, xmlElementName));
        }

        #region Implementation of IEnumerable

        public IEnumerator<Image> GetEnumerator()
        {
            return _images.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}
