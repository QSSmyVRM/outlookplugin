﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;

namespace MyVrm.WebServices.Data
{
    /// <summary>
    /// Represents a property definition for properties of type TimeZoneInfo.
    /// </summary>
    internal class TimeZonePropertyDefinition : PropertyDefinition
    {
        internal TimeZonePropertyDefinition(string xmlElementName) : base(xmlElementName)
        {
            PropertyType = typeof(TimeZoneInfo);
        }

        internal TimeZonePropertyDefinition(string xmlElementName, PropertyDefinitionFlags flags)
            : base(xmlElementName, flags)
        {
            PropertyType = typeof(TimeZoneInfo);
        }

        #region Overrides of PropertyDefinition

        internal override void LoadPropertyValueFromXml(MyVrmServiceXmlReader reader, PropertyBag propertyBag)
        {
            propertyBag[this] = TimeZoneConvertion.ConvertToTimeZoneInfo(reader.ReadElementValue<int>());
        }

        internal override void WritePropertyValueToXml(MyVrmXmlWriter writer, PropertyBag propertyBag)
        {
            var value = (TimeZoneInfo)propertyBag[this];
            writer.WriteElementValue(XmlNamespace.NotSpecified, XmlElementName, TimeZoneConvertion.ConvertToTimeZoneId(value));
        }

        #endregion
    }
}
