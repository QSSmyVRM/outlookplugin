using System;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Outlook;
using Microsoft.Office.Tools;
using MyVrm.Common;

namespace MyVrm.Outlook
{
    public class CommandBarManager : DisposableObject
    {
        private readonly AddIn _addIn;
        private readonly CommandBarKeyedByWindowCollection _commandBars = new CommandBarKeyedByWindowCollection();

        public CommandBarManager(AddIn addIn)
        {
            if (addIn == null) 
                throw new ArgumentNullException("addIn");
            _addIn = addIn;
        }

        public void RegisterCommandBar(OfficeCommandBar commandBar, Inspector inspector)
        {
            commandBar.Init(inspector);
            CommandBars.Add(commandBar, inspector);
            commandBar.Close += CommandBarClose;
        }

        public CommandBarKeyedByWindowCollection CommandBars
        {
            get { return _commandBars; }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            GC.SuppressFinalize(this);
        }

        void CommandBarClose(object sender, EventArgs e)
        {
            CommandBars.Remove((OfficeCommandBar) sender);
        }
    }
}