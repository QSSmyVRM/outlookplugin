﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
    class LanguagePropertyDefinition :PropertyDefinition
    {
        internal LanguagePropertyDefinition(string xmlElementName)
            : base(xmlElementName)
        {
            PropertyType = typeof(LanguageInfo);
        }
        internal LanguagePropertyDefinition(string xmlElementName, PropertyDefinitionFlags flags)
            : base(xmlElementName, flags)
        {
            PropertyType = typeof(LanguageInfo);
        }
        internal override void LoadPropertyValueFromXml(MyVrmServiceXmlReader reader, PropertyBag propertyBag)
        {
            propertyBag[this] = LanguageInfo.ConvertToLanguageName(reader.ReadElementValue<int>());
        }

        internal override void WritePropertyValueToXml(MyVrmXmlWriter writer, PropertyBag propertyBag)
        {
            var value = (LanguageInfo)propertyBag[this];
            writer.WriteElementValue(XmlNamespace.NotSpecified, XmlElementName, LanguageInfo.ConvertToLanguageId(value));
        }
    }
}
