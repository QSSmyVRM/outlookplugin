﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace MyVrm.WebServices.Data
{
	internal class TemplateSchema : ConferenceBaseSchema
	{
		internal static readonly PropertyDefinition Type;

		public new static readonly TemplateSchema Instance;

		static TemplateSchema()
		{
			Type = new GenericPropertyDefinition<ConferenceType>("confType", PropertyDefinitionFlags.CanSet | PropertyDefinitionFlags.CanUpdate);
			Instance = new TemplateSchema();
		}

		internal override void RegisterProperties()
		{
			base.RegisterProperties();
			RegisterProperty(Type);
		}
	}
}
