﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;

namespace MyVrm.WebServices.Data
{
    internal class TimeSpanPropertyDefinition : TypedPropertyDefinition
    {
        internal TimeSpanPropertyDefinition(string xmlElementName) : base(xmlElementName)
        {
            PropertyType = typeof(TimeSpan);
        }

        internal TimeSpanPropertyDefinition(string xmlElementName, PropertyDefinitionFlags flags)
            : base(xmlElementName, flags)
        {
            PropertyType = typeof(TimeSpan);
        }

        #region Overrides of TypedPropertyDefinition

        internal override object Parse(string value)
        {
            double min;
            if (Double.TryParse(value, out min))
            {
                return TimeSpan.FromMinutes(min);
            }
            return null;
        }

        internal override string ToString(object value)
        {
            return ((TimeSpan) value).TotalMinutes.ToString();
        }

        #endregion
    }
}
