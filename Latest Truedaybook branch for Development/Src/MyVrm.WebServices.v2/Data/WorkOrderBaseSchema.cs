﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.WebServices.Data
{
    internal abstract class WorkOrderBaseSchema : ServiceObjectSchema
    {
        public static readonly PropertyDefinition Id;
        public static readonly PropertyDefinition Name;
        public static readonly PropertyDefinition RoomId;
        public static readonly PropertyDefinition RoomName;

        static WorkOrderBaseSchema()
        {
            Id = new ComplexPropertyDefinition<WorkOrderId>("ID", () => new WorkOrderId());
            Name = new StringPropertyDefinition("Name", PropertyDefinitionFlags.CanSet | PropertyDefinitionFlags.CanUpdate);
            RoomId = new ComplexPropertyDefinition<RoomId>("RoomID", PropertyDefinitionFlags.CanSet | PropertyDefinitionFlags.CanUpdate, () => new RoomId());
            RoomName = new StringPropertyDefinition("RoomName");
        }

        internal override void RegisterProperties()
        {
            base.RegisterProperties();
            RegisterProperty(Id);
            RegisterProperty(Name);
            RegisterProperty(RoomId);
            RegisterProperty(RoomName);
        }
    }
}
