﻿using System;

namespace MyVrm.DataImport.Excel
{
    public class SaveError
    {
        internal SaveError()
        {
        }

        internal SaveError(string itemName, Exception error)
        {
            ItemName = itemName;
            Error = error;
        }
        public string ItemName { get; internal set; }
        public Exception Error { get; internal set; }
        public string ErrorText
        {
            get
            {
                return Error.Message;
            }
        }
    }
}
