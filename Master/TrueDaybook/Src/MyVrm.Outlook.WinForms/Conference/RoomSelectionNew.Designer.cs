﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook.WinForms.Conference
{
	partial class RoomSelectionNew
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RoomSelectionNew));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.vScrollBar1 = new DevExpress.XtraEditors.VScrollBar();
            this.bnRemove = new DevExpress.XtraEditors.SimpleButton();
            this.bnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.dropBnChangeView = new DevExpress.XtraEditors.DropDownButton();
            this.searchBn = new DevExpress.XtraEditors.SimpleButton();
            this.textEditSearchFor = new DevExpress.XtraEditors.TextEdit();
            this.roomListBoxControl = new DevExpress.XtraEditors.ListBoxControl();
            this.comboBoxSearchColumn = new DevExpress.XtraEditors.ComboBoxEdit();
            this.checkEditShowFavorites = new DevExpress.XtraEditors.CheckEdit();
            this.roomsCalendarButton = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl_RoomsInfoNew = new DevExpress.XtraGrid.GridControl();
            this.layoutPublicBasic = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPublicPicture = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemPictureEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.colPublicShowDetails = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colPublicRoomType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPublicSite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPublicName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPublicCountry = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPublicState = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPublicZipCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPublicCity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPublicPhone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPublicCurrency = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPublicBusinesPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPublicISDNIP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPublicSDHD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPublicAutomated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPublicCapacity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryAddRoom = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.layoutPrivateBasicView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.layoutViewColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutViewColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutViewColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutViewColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutViewColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutViewColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutViewColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutViewColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutViewColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutPublicGraphic = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this.graphicColPublicPicture = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_graphicColPublicPicture = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.graphicColPublicName = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_graphicColPublicName = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.graphicColPublicCity = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_graphicColPublicCity = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.graphicColPublicziipCode = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_graphicColPublicziipCode = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.graphicColPublicCapacity = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_graphicColPublicCapacity = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.graphicColPublicISDNIP = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_graphicColPublicISDNIP = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.graphicColPublicSDHD = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_graphicColPublicSDHD = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.graphicColPublicBusinessHrs = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_graphicColPublicBusinessHrs = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.graphicColPublicShowDetails = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_graphicColPublicShowDetails = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.graphicColPublicAdd = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_graphicColPublicAdd = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewCard1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewCard();
            this.item7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.item8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.item10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutPrivateAdvancedView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.checkEditPublicOnly = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.roomListLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.calendarLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutViewColumn1 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn1_1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn2 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn2_1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn3 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn3_1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn4 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn4_1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn5 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn5_1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn6 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn6_1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn7 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn7_1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn8 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn8_1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colIsFavorite = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colIsFavorite_1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colRoomType = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colRoomType_1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.item1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.item3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.item4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.item6 = new DevExpress.XtraLayout.SimpleSeparator();
            this.item2 = new DevExpress.XtraLayout.SimpleSeparator();
            this.item5 = new DevExpress.XtraLayout.SimpleSeparator();
            this.item9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.backgroundWorkerFirstPage = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).BeginInit();
            this.ContentPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditSearchFor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.roomListBoxControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxSearchColumn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditShowFavorites.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_RoomsInfoNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPublicBasic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryAddRoom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPrivateBasicView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPublicGraphic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_graphicColPublicPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_graphicColPublicName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_graphicColPublicCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_graphicColPublicziipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_graphicColPublicCapacity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_graphicColPublicISDNIP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_graphicColPublicSDHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_graphicColPublicBusinessHrs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_graphicColPublicShowDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_graphicColPublicAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPrivateAdvancedView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditPublicOnly.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.roomListLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calendarLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn2_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn3_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn4_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn5_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn6_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn7_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn8_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colIsFavorite_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colRoomType_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            this.SuspendLayout();
            // 
            // ContentPanel
            // 
            this.ContentPanel.Controls.Add(this.layoutControl1);
            this.ContentPanel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ContentPanel.Size = new System.Drawing.Size(917, 451);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.FieldName = "pictureCol";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "gridColumn2";
            this.gridColumn2.FieldName = "propCol";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.vScrollBar1);
            this.layoutControl1.Controls.Add(this.bnRemove);
            this.layoutControl1.Controls.Add(this.bnAdd);
            this.layoutControl1.Controls.Add(this.dropBnChangeView);
            this.layoutControl1.Controls.Add(this.searchBn);
            this.layoutControl1.Controls.Add(this.textEditSearchFor);
            this.layoutControl1.Controls.Add(this.roomListBoxControl);
            this.layoutControl1.Controls.Add(this.comboBoxSearchColumn);
            this.layoutControl1.Controls.Add(this.checkEditShowFavorites);
            this.layoutControl1.Controls.Add(this.roomsCalendarButton);
            this.layoutControl1.Controls.Add(this.gridControl_RoomsInfoNew);
            this.layoutControl1.Controls.Add(this.checkEditPublicOnly);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(917, 451);
            this.layoutControl1.TabIndex = 1;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // vScrollBar1
            // 
            this.vScrollBar1.Location = new System.Drawing.Point(901, 89);
            this.vScrollBar1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.vScrollBar1.Name = "vScrollBar1";
            this.vScrollBar1.Size = new System.Drawing.Size(14, 215);
            this.vScrollBar1.TabIndex = 19;
            this.vScrollBar1.Visible = false;
            // 
            // bnRemove
            // 
            this.bnRemove.Location = new System.Drawing.Point(810, 308);
            this.bnRemove.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bnRemove.Name = "bnRemove";
            this.bnRemove.Size = new System.Drawing.Size(105, 23);
            this.bnRemove.StyleController = this.layoutControl1;
            this.bnRemove.TabIndex = 18;
            this.bnRemove.Text = "Remove";
            this.bnRemove.Click += new System.EventHandler(this.bnRemove_Click);
            // 
            // bnAdd
            // 
            this.bnAdd.Location = new System.Drawing.Point(2, 308);
            this.bnAdd.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bnAdd.Name = "bnAdd";
            this.bnAdd.Size = new System.Drawing.Size(89, 19);
            this.bnAdd.StyleController = this.layoutControl1;
            this.bnAdd.TabIndex = 17;
            this.bnAdd.Text = "Rooms->";
            this.bnAdd.Click += new System.EventHandler(this.bnAdd_Click);
            // 
            // dropBnChangeView
            // 
            this.dropBnChangeView.Location = new System.Drawing.Point(808, 62);
            this.dropBnChangeView.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dropBnChangeView.Name = "dropBnChangeView";
            this.dropBnChangeView.Size = new System.Drawing.Size(107, 23);
            this.dropBnChangeView.StyleController = this.layoutControl1;
            this.dropBnChangeView.TabIndex = 16;
            this.dropBnChangeView.Text = "Change View";
            this.dropBnChangeView.ArrowButtonClick += new System.EventHandler(this.dropBnChangeView_ArrowButtonClick);
            this.dropBnChangeView.Click += new System.EventHandler(this.dropBnChangeView_ArrowButtonClick);
            this.dropBnChangeView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dropBnChangeView_MouseClick);
            // 
            // searchBn
            // 
            this.searchBn.Location = new System.Drawing.Point(808, 19);
            this.searchBn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.searchBn.Name = "searchBn";
            this.searchBn.Size = new System.Drawing.Size(107, 23);
            this.searchBn.StyleController = this.layoutControl1;
            this.searchBn.TabIndex = 20;
            this.searchBn.Text = "Search";
            this.searchBn.Click += new System.EventHandler(this.searchBn_Click);
            // 
            // textEditSearchFor
            // 
            this.textEditSearchFor.Location = new System.Drawing.Point(64, 34);
            this.textEditSearchFor.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditSearchFor.Name = "textEditSearchFor";
            this.textEditSearchFor.Size = new System.Drawing.Size(525, 20);
            this.textEditSearchFor.StyleController = this.layoutControl1;
            this.textEditSearchFor.TabIndex = 9;
            this.textEditSearchFor.EditValueChanged += new System.EventHandler(this.textEditSearchFor_EditValueChanged);
            // 
            // roomListBoxControl
            // 
            this.roomListBoxControl.Location = new System.Drawing.Point(95, 308);
            this.roomListBoxControl.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.roomListBoxControl.Name = "roomListBoxControl";
            this.roomListBoxControl.Padding = new System.Windows.Forms.Padding(2);
            this.roomListBoxControl.Size = new System.Drawing.Size(711, 139);
            this.roomListBoxControl.StyleController = this.layoutControl1;
            this.roomListBoxControl.TabIndex = 10;
            this.roomListBoxControl.ToolTip = "Double click on a room to remove it from the list of selected rooms";
            this.roomListBoxControl.DoubleClick += new System.EventHandler(this.roomListBoxControl_DoubleClick);
            // 
            // comboBoxSearchColumn
            // 
            this.comboBoxSearchColumn.Location = new System.Drawing.Point(64, 4);
            this.comboBoxSearchColumn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxSearchColumn.Name = "comboBoxSearchColumn";
            this.comboBoxSearchColumn.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxSearchColumn.Properties.Sorted = true;
            this.comboBoxSearchColumn.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxSearchColumn.Size = new System.Drawing.Size(525, 20);
            this.comboBoxSearchColumn.StyleController = this.layoutControl1;
            this.comboBoxSearchColumn.TabIndex = 8;
            this.comboBoxSearchColumn.SelectedIndexChanged += new System.EventHandler(this.comboBoxSearchColumn_SelectedIndexChanged);
            // 
            // checkEditShowFavorites
            // 
            this.checkEditShowFavorites.Location = new System.Drawing.Point(597, 6);
            this.checkEditShowFavorites.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkEditShowFavorites.Name = "checkEditShowFavorites";
            this.checkEditShowFavorites.Properties.Caption = "Favorites Only";
            this.checkEditShowFavorites.Size = new System.Drawing.Size(205, 19);
            this.checkEditShowFavorites.StyleController = this.layoutControl1;
            this.checkEditShowFavorites.TabIndex = 11;
            this.checkEditShowFavorites.CheckedChanged += new System.EventHandler(this.checkEditXX_CheckedChanged);
            // 
            // roomsCalendarButton
            // 
            this.roomsCalendarButton.Appearance.Image = global::MyVrm.Outlook.WinForms.Properties.Resources.Calendar_scheduleHS;
            this.roomsCalendarButton.Appearance.Options.UseImage = true;
            this.roomsCalendarButton.Appearance.Options.UseTextOptions = true;
            this.roomsCalendarButton.Image = global::MyVrm.Outlook.WinForms.Properties.Resources.Calendar_scheduleHS;
            this.roomsCalendarButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.roomsCalendarButton.Location = new System.Drawing.Point(810, 408);
            this.roomsCalendarButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.roomsCalendarButton.Name = "roomsCalendarButton";
            this.roomsCalendarButton.Size = new System.Drawing.Size(105, 39);
            this.roomsCalendarButton.StyleController = this.layoutControl1;
            this.roomsCalendarButton.TabIndex = 15;
            this.roomsCalendarButton.Text = "Rooms Calendar";
            this.roomsCalendarButton.ToolTip = "Show a calendar for non-public rooms";
            this.roomsCalendarButton.Click += new System.EventHandler(this.roomsCalendarButton_Click);
            // 
            // gridControl_RoomsInfoNew
            // 
            this.gridControl_RoomsInfoNew.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl_RoomsInfoNew.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl_RoomsInfoNew.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl_RoomsInfoNew.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl_RoomsInfoNew.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl_RoomsInfoNew.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl_RoomsInfoNew.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl_RoomsInfoNew.EmbeddedNavigator.Enabled = false;
            this.gridControl_RoomsInfoNew.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControl_RoomsInfoNew.Location = new System.Drawing.Point(2, 89);
            this.gridControl_RoomsInfoNew.MainView = this.layoutPublicBasic;
            this.gridControl_RoomsInfoNew.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControl_RoomsInfoNew.Name = "gridControl_RoomsInfoNew";
            this.gridControl_RoomsInfoNew.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPictureEdit2,
            this.repositoryItemButtonEdit2,
            this.repositoryAddRoom,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEdit1,
            this.repositoryItemButtonEdit1});
            this.gridControl_RoomsInfoNew.Size = new System.Drawing.Size(895, 215);
            this.gridControl_RoomsInfoNew.TabIndex = 7;
            this.gridControl_RoomsInfoNew.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.layoutPublicBasic,
            this.layoutPrivateBasicView,
            this.layoutPublicGraphic,
            this.layoutPrivateAdvancedView});
            this.gridControl_RoomsInfoNew.DoubleClick += new System.EventHandler(this.gridControl_RoomsInfo_DoubleClick);
            this.gridControl_RoomsInfoNew.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.gridControl_RoomsInfoNew_PreviewKeyDown);
            // 
            // layoutPublicBasic
            // 
            this.layoutPublicBasic.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPublicPicture,
            this.colPublicShowDetails,
            this.colPublicRoomType,
            this.colPublicSite,
            this.colPublicName,
            this.colPublicCountry,
            this.colPublicState,
            this.colPublicZipCode,
            this.colPublicCity,
            this.colPublicPhone,
            this.colPublicCurrency,
            this.colPublicBusinesPrice,
            this.colPublicISDNIP,
            this.colPublicSDHD,
            this.colPublicAutomated,
            this.colPublicCapacity});
            this.layoutPublicBasic.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.layoutPublicBasic.GridControl = this.gridControl_RoomsInfoNew;
            this.layoutPublicBasic.Name = "layoutPublicBasic";
            this.layoutPublicBasic.OptionsCustomization.AllowFilter = false;
            this.layoutPublicBasic.OptionsCustomization.AllowGroup = false;
            this.layoutPublicBasic.OptionsFilter.AllowColumnMRUFilterList = false;
            this.layoutPublicBasic.OptionsFilter.AllowFilterEditor = false;
            this.layoutPublicBasic.OptionsFilter.AllowMRUFilterList = false;
            this.layoutPublicBasic.OptionsMenu.EnableFooterMenu = false;
            this.layoutPublicBasic.OptionsMenu.EnableGroupPanelMenu = false;
            this.layoutPublicBasic.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.layoutPublicBasic.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.Hidden;
            this.layoutPublicBasic.OptionsView.RowAutoHeight = true;
            this.layoutPublicBasic.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.layoutPublicBasic.OptionsView.ShowGroupPanel = false;
            this.layoutPublicBasic.OptionsView.ShowIndicator = false;
            this.layoutPublicBasic.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPublicName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.layoutPublicBasic.TopRowChanged += new System.EventHandler(this.layoutPublicBasic_TopRowChanged);
            // 
            // colPublicPicture
            // 
            this.colPublicPicture.ColumnEdit = this.repositoryItemPictureEdit2;
            this.colPublicPicture.FieldName = "Picture";
            this.colPublicPicture.Name = "colPublicPicture";
            this.colPublicPicture.OptionsColumn.AllowEdit = false;
            this.colPublicPicture.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colPublicPicture.OptionsFilter.AllowAutoFilter = false;
            // 
            // repositoryItemPictureEdit2
            // 
            this.repositoryItemPictureEdit2.Name = "repositoryItemPictureEdit2";
            this.repositoryItemPictureEdit2.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            // 
            // colPublicShowDetails
            // 
            this.colPublicShowDetails.Caption = "Show Details";
            this.colPublicShowDetails.ColumnEdit = this.repositoryItemButtonEdit1;
            this.colPublicShowDetails.MinWidth = 10;
            this.colPublicShowDetails.Name = "colPublicShowDetails";
            this.colPublicShowDetails.Visible = true;
            this.colPublicShowDetails.VisibleIndex = 0;
            this.colPublicShowDetails.Width = 88;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            serializableAppearanceObject1.Image = global::MyVrm.Outlook.WinForms.Properties.Resources.information;
            serializableAppearanceObject1.Options.UseImage = true;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonEdit1.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Show room information", null, null, true)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit1.Click += new System.EventHandler(this.ShowDetails);
            // 
            // colPublicRoomType
            // 
            this.colPublicRoomType.AppearanceCell.Options.UseTextOptions = true;
            this.colPublicRoomType.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colPublicRoomType.Caption = "Room Type";
            this.colPublicRoomType.FieldName = "Type";
            this.colPublicRoomType.Name = "colPublicRoomType";
            this.colPublicRoomType.OptionsColumn.AllowEdit = false;
            this.colPublicRoomType.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colPublicRoomType.OptionsColumn.ReadOnly = true;
            this.colPublicRoomType.OptionsFilter.AllowAutoFilter = false;
            this.colPublicRoomType.OptionsFilter.AllowFilter = false;
            this.colPublicRoomType.Width = 105;
            // 
            // colPublicSite
            // 
            this.colPublicSite.Caption = "Site";
            this.colPublicSite.FieldName = "Site";
            this.colPublicSite.Name = "colPublicSite";
            this.colPublicSite.Width = 86;
            // 
            // colPublicName
            // 
            this.colPublicName.Caption = "Name";
            this.colPublicName.FieldName = "Name";
            this.colPublicName.Name = "colPublicName";
            this.colPublicName.OptionsColumn.AllowEdit = false;
            this.colPublicName.OptionsFilter.AllowAutoFilter = false;
            this.colPublicName.Visible = true;
            this.colPublicName.VisibleIndex = 1;
            this.colPublicName.Width = 103;
            // 
            // colPublicCountry
            // 
            this.colPublicCountry.Caption = "Country";
            this.colPublicCountry.FieldName = "Country";
            this.colPublicCountry.Name = "colPublicCountry";
            this.colPublicCountry.OptionsColumn.AllowEdit = false;
            this.colPublicCountry.Visible = true;
            this.colPublicCountry.VisibleIndex = 2;
            // 
            // colPublicState
            // 
            this.colPublicState.Caption = "State";
            this.colPublicState.FieldName = "State";
            this.colPublicState.Name = "colPublicState";
            this.colPublicState.OptionsColumn.AllowEdit = false;
            this.colPublicState.OptionsFilter.AllowAutoFilter = false;
            this.colPublicState.Visible = true;
            this.colPublicState.VisibleIndex = 3;
            this.colPublicState.Width = 66;
            // 
            // colPublicZipCode
            // 
            this.colPublicZipCode.Caption = "Zip Code";
            this.colPublicZipCode.FieldName = "ZipCode";
            this.colPublicZipCode.Name = "colPublicZipCode";
            this.colPublicZipCode.OptionsColumn.AllowEdit = false;
            this.colPublicZipCode.Visible = true;
            this.colPublicZipCode.VisibleIndex = 4;
            this.colPublicZipCode.Width = 92;
            // 
            // colPublicCity
            // 
            this.colPublicCity.FieldName = "City";
            this.colPublicCity.Name = "colPublicCity";
            this.colPublicCity.OptionsColumn.AllowEdit = false;
            this.colPublicCity.Visible = true;
            this.colPublicCity.VisibleIndex = 5;
            this.colPublicCity.Width = 115;
            // 
            // colPublicPhone
            // 
            this.colPublicPhone.FieldName = "Phone";
            this.colPublicPhone.Name = "colPublicPhone";
            this.colPublicPhone.OptionsColumn.AllowEdit = false;
            this.colPublicPhone.Visible = true;
            this.colPublicPhone.VisibleIndex = 6;
            this.colPublicPhone.Width = 129;
            // 
            // colPublicCurrency
            // 
            this.colPublicCurrency.Caption = "Currency";
            this.colPublicCurrency.FieldName = "pubCurrency";
            this.colPublicCurrency.Name = "colPublicCurrency";
            this.colPublicCurrency.OptionsColumn.AllowEdit = false;
            this.colPublicCurrency.Visible = true;
            this.colPublicCurrency.VisibleIndex = 7;
            this.colPublicCurrency.Width = 69;
            // 
            // colPublicBusinesPrice
            // 
            this.colPublicBusinesPrice.Caption = "Business Hrs Price";
            this.colPublicBusinesPrice.FieldName = "pubBusinessHrsPrice";
            this.colPublicBusinesPrice.Name = "colPublicBusinesPrice";
            this.colPublicBusinesPrice.OptionsColumn.AllowEdit = false;
            this.colPublicBusinesPrice.Visible = true;
            this.colPublicBusinesPrice.VisibleIndex = 8;
            this.colPublicBusinesPrice.Width = 134;
            // 
            // colPublicISDNIP
            // 
            this.colPublicISDNIP.Caption = "ISDN/IP";
            this.colPublicISDNIP.FieldName = "pubISDNIP";
            this.colPublicISDNIP.Name = "colPublicISDNIP";
            this.colPublicISDNIP.Visible = true;
            this.colPublicISDNIP.VisibleIndex = 9;
            this.colPublicISDNIP.Width = 67;
            // 
            // colPublicSDHD
            // 
            this.colPublicSDHD.Caption = "SD/HD";
            this.colPublicSDHD.FieldName = "pubSDHD";
            this.colPublicSDHD.Name = "colPublicSDHD";
            this.colPublicSDHD.OptionsColumn.AllowEdit = false;
            this.colPublicSDHD.Visible = true;
            this.colPublicSDHD.VisibleIndex = 10;
            this.colPublicSDHD.Width = 69;
            // 
            // colPublicAutomated
            // 
            this.colPublicAutomated.Caption = "Automated";
            this.colPublicAutomated.FieldName = "pubAutomated";
            this.colPublicAutomated.Name = "colPublicAutomated";
            this.colPublicAutomated.OptionsColumn.AllowEdit = false;
            this.colPublicAutomated.Visible = true;
            this.colPublicAutomated.VisibleIndex = 11;
            this.colPublicAutomated.Width = 151;
            // 
            // colPublicCapacity
            // 
            this.colPublicCapacity.Caption = "Capacity";
            this.colPublicCapacity.FieldName = "Capacity";
            this.colPublicCapacity.Name = "colPublicCapacity";
            this.colPublicCapacity.OptionsColumn.AllowEdit = false;
            this.colPublicCapacity.OptionsColumn.AllowFocus = false;
            this.colPublicCapacity.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colPublicCapacity.OptionsColumn.AllowMove = false;
            this.colPublicCapacity.OptionsColumn.AllowShowHide = false;
            this.colPublicCapacity.OptionsColumn.AllowSize = false;
            this.colPublicCapacity.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemButtonEdit2
            // 
            this.repositoryItemButtonEdit2.AutoHeight = false;
            serializableAppearanceObject2.BorderColor = System.Drawing.Color.Black;
            serializableAppearanceObject2.Options.UseBorderColor = true;
            this.repositoryItemButtonEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Show Room Info", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.repositoryItemButtonEdit2.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.repositoryItemButtonEdit2.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.repositoryItemButtonEdit2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.repositoryItemButtonEdit2.Name = "repositoryItemButtonEdit2";
            this.repositoryItemButtonEdit2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit2.Click += new System.EventHandler(this.ShowDetails);
            // 
            // repositoryAddRoom
            // 
            this.repositoryAddRoom.AutoHeight = false;
            serializableAppearanceObject3.BorderColor = System.Drawing.Color.Black;
            serializableAppearanceObject3.Options.UseBorderColor = true;
            this.repositoryAddRoom.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Add to conference", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "", null, null, true)});
            this.repositoryAddRoom.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.repositoryAddRoom.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.repositoryAddRoom.LookAndFeel.UseDefaultLookAndFeel = false;
            this.repositoryAddRoom.Name = "repositoryAddRoom";
            this.repositoryAddRoom.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryAddRoom.Click += new System.EventHandler(this.repositoryAddRoom_Click);
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // layoutPrivateBasicView
            // 
            this.layoutPrivateBasicView.ActiveFilterEnabled = false;
            this.layoutPrivateBasicView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.layoutViewColumn14,
            this.gridColumn15,
            this.layoutViewColumn17,
            this.layoutViewColumn19,
            this.layoutViewColumn16,
            this.layoutViewColumn25,
            this.layoutViewColumn21,
            this.layoutViewColumn22,
            this.layoutViewColumn24,
            this.layoutViewColumn29});
            this.layoutPrivateBasicView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.layoutPrivateBasicView.GridControl = this.gridControl_RoomsInfoNew;
            this.layoutPrivateBasicView.Name = "layoutPrivateBasicView";
            this.layoutPrivateBasicView.OptionsCustomization.AllowFilter = false;
            this.layoutPrivateBasicView.OptionsCustomization.AllowGroup = false;
            this.layoutPrivateBasicView.OptionsFilter.AllowColumnMRUFilterList = false;
            this.layoutPrivateBasicView.OptionsFilter.AllowFilterEditor = false;
            this.layoutPrivateBasicView.OptionsFilter.AllowMRUFilterList = false;
            this.layoutPrivateBasicView.OptionsMenu.EnableFooterMenu = false;
            this.layoutPrivateBasicView.OptionsMenu.EnableGroupPanelMenu = false;
            this.layoutPrivateBasicView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.layoutPrivateBasicView.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.layoutPrivateBasicView.OptionsView.ShowGroupPanel = false;
            this.layoutPrivateBasicView.OptionsView.ShowIndicator = false;
            this.layoutPrivateBasicView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.layoutViewColumn19, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.layoutPrivateBasicView.TopRowChanged += new System.EventHandler(this.layoutPrivateBasicView_TopRowChanged);
            // 
            // layoutViewColumn14
            // 
            this.layoutViewColumn14.Caption = "Show Details";
            this.layoutViewColumn14.ColumnEdit = this.repositoryItemButtonEdit1;
            this.layoutViewColumn14.MinWidth = 10;
            this.layoutViewColumn14.Name = "layoutViewColumn14";
            this.layoutViewColumn14.OptionsFilter.AllowAutoFilter = false;
            this.layoutViewColumn14.OptionsFilter.AllowFilter = false;
            this.layoutViewColumn14.Visible = true;
            this.layoutViewColumn14.VisibleIndex = 0;
            this.layoutViewColumn14.Width = 90;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Approval";
            this.gridColumn15.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn15.FieldName = "Approval";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 1;
            // 
            // layoutViewColumn17
            // 
            this.layoutViewColumn17.Caption = "Site";
            this.layoutViewColumn17.FieldName = "Site";
            this.layoutViewColumn17.Name = "layoutViewColumn17";
            this.layoutViewColumn17.OptionsColumn.AllowEdit = false;
            this.layoutViewColumn17.Visible = true;
            this.layoutViewColumn17.VisibleIndex = 2;
            this.layoutViewColumn17.Width = 106;
            // 
            // layoutViewColumn19
            // 
            this.layoutViewColumn19.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.layoutViewColumn19.AppearanceCell.Options.UseBackColor = true;
            this.layoutViewColumn19.AppearanceCell.Options.UseTextOptions = true;
            this.layoutViewColumn19.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutViewColumn19.Caption = "Name";
            this.layoutViewColumn19.FieldName = "Name";
            this.layoutViewColumn19.Name = "layoutViewColumn19";
            this.layoutViewColumn19.OptionsColumn.AllowEdit = false;
            this.layoutViewColumn19.OptionsFilter.AllowAutoFilter = false;
            this.layoutViewColumn19.Visible = true;
            this.layoutViewColumn19.VisibleIndex = 3;
            this.layoutViewColumn19.Width = 126;
            // 
            // layoutViewColumn16
            // 
            this.layoutViewColumn16.AppearanceCell.Options.UseTextOptions = true;
            this.layoutViewColumn16.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutViewColumn16.Caption = "Room Type";
            this.layoutViewColumn16.FieldName = "Type";
            this.layoutViewColumn16.Name = "layoutViewColumn16";
            this.layoutViewColumn16.OptionsColumn.AllowEdit = false;
            this.layoutViewColumn16.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.layoutViewColumn16.OptionsColumn.ReadOnly = true;
            this.layoutViewColumn16.OptionsFilter.AllowAutoFilter = false;
            this.layoutViewColumn16.OptionsFilter.AllowFilter = false;
            this.layoutViewColumn16.Visible = true;
            this.layoutViewColumn16.VisibleIndex = 4;
            this.layoutViewColumn16.Width = 130;
            // 
            // layoutViewColumn25
            // 
            this.layoutViewColumn25.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.layoutViewColumn25.AppearanceCell.Options.UseBackColor = true;
            this.layoutViewColumn25.AppearanceCell.Options.UseTextOptions = true;
            this.layoutViewColumn25.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutViewColumn25.FieldName = "Capacity";
            this.layoutViewColumn25.Name = "layoutViewColumn25";
            this.layoutViewColumn25.OptionsColumn.AllowEdit = false;
            this.layoutViewColumn25.Visible = true;
            this.layoutViewColumn25.VisibleIndex = 5;
            this.layoutViewColumn25.Width = 92;
            // 
            // layoutViewColumn21
            // 
            this.layoutViewColumn21.Caption = "State";
            this.layoutViewColumn21.FieldName = "State";
            this.layoutViewColumn21.Name = "layoutViewColumn21";
            this.layoutViewColumn21.OptionsColumn.AllowEdit = false;
            this.layoutViewColumn21.OptionsFilter.AllowAutoFilter = false;
            this.layoutViewColumn21.Width = 80;
            // 
            // layoutViewColumn22
            // 
            this.layoutViewColumn22.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.layoutViewColumn22.AppearanceCell.Options.UseBackColor = true;
            this.layoutViewColumn22.AppearanceCell.Options.UseTextOptions = true;
            this.layoutViewColumn22.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutViewColumn22.FieldName = "City";
            this.layoutViewColumn22.Name = "layoutViewColumn22";
            this.layoutViewColumn22.Width = 142;
            // 
            // layoutViewColumn24
            // 
            this.layoutViewColumn24.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.layoutViewColumn24.AppearanceCell.Options.UseBackColor = true;
            this.layoutViewColumn24.AppearanceCell.Options.UseTextOptions = true;
            this.layoutViewColumn24.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutViewColumn24.Caption = "Zip Code";
            this.layoutViewColumn24.FieldName = "ZipCode";
            this.layoutViewColumn24.Name = "layoutViewColumn24";
            this.layoutViewColumn24.Width = 115;
            // 
            // layoutViewColumn29
            // 
            this.layoutViewColumn29.FieldName = "Phone";
            this.layoutViewColumn29.Name = "layoutViewColumn29";
            this.layoutViewColumn29.Width = 168;
            // 
            // layoutPublicGraphic
            // 
            this.layoutPublicGraphic.ActiveFilterEnabled = false;
            this.layoutPublicGraphic.CardMinSize = new System.Drawing.Size(714, 185);
            this.layoutPublicGraphic.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.graphicColPublicPicture,
            this.graphicColPublicName,
            this.graphicColPublicCity,
            this.graphicColPublicziipCode,
            this.graphicColPublicCapacity,
            this.graphicColPublicISDNIP,
            this.graphicColPublicSDHD,
            this.graphicColPublicBusinessHrs,
            this.graphicColPublicShowDetails,
            this.graphicColPublicAdd});
            this.layoutPublicGraphic.GridControl = this.gridControl_RoomsInfoNew;
            this.layoutPublicGraphic.Name = "layoutPublicGraphic";
            this.layoutPublicGraphic.OptionsBehavior.AllowExpandCollapse = false;
            this.layoutPublicGraphic.OptionsBehavior.ScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Auto;
            this.layoutPublicGraphic.OptionsCustomization.ShowGroupView = false;
            this.layoutPublicGraphic.OptionsFilter.AllowColumnMRUFilterList = false;
            this.layoutPublicGraphic.OptionsFilter.AllowFilterEditor = false;
            this.layoutPublicGraphic.OptionsFilter.AllowMRUFilterList = false;
            this.layoutPublicGraphic.OptionsItemText.TextToControlDistance = 3;
            this.layoutPublicGraphic.OptionsView.CardArrangeRule = DevExpress.XtraGrid.Views.Layout.LayoutCardArrangeRule.AllowPartialCards;
            this.layoutPublicGraphic.OptionsView.ShowCardCaption = false;
            this.layoutPublicGraphic.OptionsView.ShowCardExpandButton = false;
            this.layoutPublicGraphic.OptionsView.ShowCardLines = false;
            this.layoutPublicGraphic.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.layoutPublicGraphic.OptionsView.ShowHeaderPanel = false;
            this.layoutPublicGraphic.OptionsView.ViewMode = DevExpress.XtraGrid.Views.Layout.LayoutViewMode.Column;
            this.layoutPublicGraphic.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.graphicColPublicName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.layoutPublicGraphic.TemplateCard = this.layoutViewCard1;
            this.layoutPublicGraphic.VisibleRecordIndexChanged += new DevExpress.XtraGrid.Views.Layout.Events.LayoutViewVisibleRecordIndexChangedEventHandler(this.layoutPublicGraphic_VisibleRecordIndexChanged);
            // 
            // graphicColPublicPicture
            // 
            this.graphicColPublicPicture.AppearanceCell.Options.UseTextOptions = true;
            this.graphicColPublicPicture.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.graphicColPublicPicture.ColumnEdit = this.repositoryItemPictureEdit2;
            this.graphicColPublicPicture.FieldName = "Picture";
            this.graphicColPublicPicture.LayoutViewField = this.layoutViewField_graphicColPublicPicture;
            this.graphicColPublicPicture.Name = "graphicColPublicPicture";
            this.graphicColPublicPicture.OptionsColumn.AllowEdit = false;
            this.graphicColPublicPicture.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.graphicColPublicPicture.OptionsColumn.AllowIncrementalSearch = false;
            this.graphicColPublicPicture.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.graphicColPublicPicture.OptionsFilter.AllowAutoFilter = false;
            this.graphicColPublicPicture.OptionsFilter.AllowFilter = false;
            // 
            // layoutViewField_graphicColPublicPicture
            // 
            this.layoutViewField_graphicColPublicPicture.EditorPreferredWidth = 199;
            this.layoutViewField_graphicColPublicPicture.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_graphicColPublicPicture.Name = "layoutViewField_graphicColPublicPicture";
            this.layoutViewField_graphicColPublicPicture.Size = new System.Drawing.Size(203, 140);
            this.layoutViewField_graphicColPublicPicture.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField_graphicColPublicPicture.TextToControlDistance = 0;
            this.layoutViewField_graphicColPublicPicture.TextVisible = false;
            // 
            // graphicColPublicName
            // 
            this.graphicColPublicName.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.graphicColPublicName.AppearanceCell.Options.UseBackColor = true;
            this.graphicColPublicName.AppearanceCell.Options.UseTextOptions = true;
            this.graphicColPublicName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.graphicColPublicName.Caption = "Name";
            this.graphicColPublicName.FieldName = "Name";
            this.graphicColPublicName.LayoutViewField = this.layoutViewField_graphicColPublicName;
            this.graphicColPublicName.Name = "graphicColPublicName";
            this.graphicColPublicName.OptionsColumn.AllowEdit = false;
            this.graphicColPublicName.OptionsFilter.AllowAutoFilter = false;
            this.graphicColPublicName.Width = 103;
            // 
            // layoutViewField_graphicColPublicName
            // 
            this.layoutViewField_graphicColPublicName.EditorPreferredWidth = 395;
            this.layoutViewField_graphicColPublicName.Location = new System.Drawing.Point(203, 0);
            this.layoutViewField_graphicColPublicName.Name = "layoutViewField_graphicColPublicName";
            this.layoutViewField_graphicColPublicName.Size = new System.Drawing.Size(512, 20);
            this.layoutViewField_graphicColPublicName.TextSize = new System.Drawing.Size(90, 13);
            // 
            // graphicColPublicCity
            // 
            this.graphicColPublicCity.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.graphicColPublicCity.AppearanceCell.Options.UseBackColor = true;
            this.graphicColPublicCity.AppearanceCell.Options.UseTextOptions = true;
            this.graphicColPublicCity.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.graphicColPublicCity.FieldName = "City";
            this.graphicColPublicCity.LayoutViewField = this.layoutViewField_graphicColPublicCity;
            this.graphicColPublicCity.Name = "graphicColPublicCity";
            this.graphicColPublicCity.OptionsColumn.AllowEdit = false;
            this.graphicColPublicCity.Width = 115;
            // 
            // layoutViewField_graphicColPublicCity
            // 
            this.layoutViewField_graphicColPublicCity.EditorPreferredWidth = 395;
            this.layoutViewField_graphicColPublicCity.Location = new System.Drawing.Point(203, 20);
            this.layoutViewField_graphicColPublicCity.Name = "layoutViewField_graphicColPublicCity";
            this.layoutViewField_graphicColPublicCity.Size = new System.Drawing.Size(512, 20);
            this.layoutViewField_graphicColPublicCity.TextSize = new System.Drawing.Size(90, 13);
            // 
            // graphicColPublicziipCode
            // 
            this.graphicColPublicziipCode.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.graphicColPublicziipCode.AppearanceCell.Options.UseBackColor = true;
            this.graphicColPublicziipCode.AppearanceCell.Options.UseTextOptions = true;
            this.graphicColPublicziipCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.graphicColPublicziipCode.Caption = "Zip Code";
            this.graphicColPublicziipCode.FieldName = "ZipCode";
            this.graphicColPublicziipCode.LayoutViewField = this.layoutViewField_graphicColPublicziipCode;
            this.graphicColPublicziipCode.Name = "graphicColPublicziipCode";
            this.graphicColPublicziipCode.OptionsColumn.AllowEdit = false;
            this.graphicColPublicziipCode.Width = 92;
            // 
            // layoutViewField_graphicColPublicziipCode
            // 
            this.layoutViewField_graphicColPublicziipCode.EditorPreferredWidth = 395;
            this.layoutViewField_graphicColPublicziipCode.Location = new System.Drawing.Point(203, 40);
            this.layoutViewField_graphicColPublicziipCode.Name = "layoutViewField_graphicColPublicziipCode";
            this.layoutViewField_graphicColPublicziipCode.Size = new System.Drawing.Size(512, 20);
            this.layoutViewField_graphicColPublicziipCode.TextSize = new System.Drawing.Size(90, 13);
            // 
            // graphicColPublicCapacity
            // 
            this.graphicColPublicCapacity.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.graphicColPublicCapacity.AppearanceCell.Options.UseBackColor = true;
            this.graphicColPublicCapacity.AppearanceCell.Options.UseTextOptions = true;
            this.graphicColPublicCapacity.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.graphicColPublicCapacity.FieldName = "Capacity";
            this.graphicColPublicCapacity.LayoutViewField = this.layoutViewField_graphicColPublicCapacity;
            this.graphicColPublicCapacity.Name = "graphicColPublicCapacity";
            this.graphicColPublicCapacity.OptionsColumn.AllowEdit = false;
            // 
            // layoutViewField_graphicColPublicCapacity
            // 
            this.layoutViewField_graphicColPublicCapacity.EditorPreferredWidth = 395;
            this.layoutViewField_graphicColPublicCapacity.Location = new System.Drawing.Point(203, 60);
            this.layoutViewField_graphicColPublicCapacity.Name = "layoutViewField_graphicColPublicCapacity";
            this.layoutViewField_graphicColPublicCapacity.Size = new System.Drawing.Size(512, 20);
            this.layoutViewField_graphicColPublicCapacity.TextSize = new System.Drawing.Size(90, 13);
            // 
            // graphicColPublicISDNIP
            // 
            this.graphicColPublicISDNIP.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.graphicColPublicISDNIP.AppearanceCell.Options.UseBackColor = true;
            this.graphicColPublicISDNIP.AppearanceCell.Options.UseTextOptions = true;
            this.graphicColPublicISDNIP.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.graphicColPublicISDNIP.Caption = "ISDN/IP";
            this.graphicColPublicISDNIP.FieldName = "pubISDNIP";
            this.graphicColPublicISDNIP.LayoutViewField = this.layoutViewField_graphicColPublicISDNIP;
            this.graphicColPublicISDNIP.Name = "graphicColPublicISDNIP";
            this.graphicColPublicISDNIP.OptionsColumn.AllowEdit = false;
            this.graphicColPublicISDNIP.Width = 67;
            // 
            // layoutViewField_graphicColPublicISDNIP
            // 
            this.layoutViewField_graphicColPublicISDNIP.EditorPreferredWidth = 395;
            this.layoutViewField_graphicColPublicISDNIP.Location = new System.Drawing.Point(203, 80);
            this.layoutViewField_graphicColPublicISDNIP.Name = "layoutViewField_graphicColPublicISDNIP";
            this.layoutViewField_graphicColPublicISDNIP.Size = new System.Drawing.Size(512, 20);
            this.layoutViewField_graphicColPublicISDNIP.TextSize = new System.Drawing.Size(90, 13);
            // 
            // graphicColPublicSDHD
            // 
            this.graphicColPublicSDHD.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.graphicColPublicSDHD.AppearanceCell.Options.UseBackColor = true;
            this.graphicColPublicSDHD.AppearanceCell.Options.UseTextOptions = true;
            this.graphicColPublicSDHD.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.graphicColPublicSDHD.Caption = "SD/HD";
            this.graphicColPublicSDHD.FieldName = "pubSDHD";
            this.graphicColPublicSDHD.LayoutViewField = this.layoutViewField_graphicColPublicSDHD;
            this.graphicColPublicSDHD.Name = "graphicColPublicSDHD";
            this.graphicColPublicSDHD.OptionsColumn.AllowEdit = false;
            this.graphicColPublicSDHD.Width = 69;
            // 
            // layoutViewField_graphicColPublicSDHD
            // 
            this.layoutViewField_graphicColPublicSDHD.EditorPreferredWidth = 395;
            this.layoutViewField_graphicColPublicSDHD.Location = new System.Drawing.Point(203, 100);
            this.layoutViewField_graphicColPublicSDHD.Name = "layoutViewField_graphicColPublicSDHD";
            this.layoutViewField_graphicColPublicSDHD.Size = new System.Drawing.Size(512, 20);
            this.layoutViewField_graphicColPublicSDHD.TextSize = new System.Drawing.Size(90, 13);
            // 
            // graphicColPublicBusinessHrs
            // 
            this.graphicColPublicBusinessHrs.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.graphicColPublicBusinessHrs.AppearanceCell.Options.UseBackColor = true;
            this.graphicColPublicBusinessHrs.AppearanceCell.Options.UseTextOptions = true;
            this.graphicColPublicBusinessHrs.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.graphicColPublicBusinessHrs.Caption = "Business Hrs Price";
            this.graphicColPublicBusinessHrs.FieldName = "pubBusinessHrsPrice";
            this.graphicColPublicBusinessHrs.LayoutViewField = this.layoutViewField_graphicColPublicBusinessHrs;
            this.graphicColPublicBusinessHrs.Name = "graphicColPublicBusinessHrs";
            this.graphicColPublicBusinessHrs.OptionsColumn.AllowEdit = false;
            this.graphicColPublicBusinessHrs.Width = 134;
            // 
            // layoutViewField_graphicColPublicBusinessHrs
            // 
            this.layoutViewField_graphicColPublicBusinessHrs.EditorPreferredWidth = 395;
            this.layoutViewField_graphicColPublicBusinessHrs.Location = new System.Drawing.Point(203, 120);
            this.layoutViewField_graphicColPublicBusinessHrs.Name = "layoutViewField_graphicColPublicBusinessHrs";
            this.layoutViewField_graphicColPublicBusinessHrs.Size = new System.Drawing.Size(512, 20);
            this.layoutViewField_graphicColPublicBusinessHrs.TextSize = new System.Drawing.Size(90, 13);
            // 
            // graphicColPublicShowDetails
            // 
            this.graphicColPublicShowDetails.Caption = "Show Details";
            this.graphicColPublicShowDetails.ColumnEdit = this.repositoryItemButtonEdit2;
            this.graphicColPublicShowDetails.LayoutViewField = this.layoutViewField_graphicColPublicShowDetails;
            this.graphicColPublicShowDetails.MinWidth = 10;
            this.graphicColPublicShowDetails.Name = "graphicColPublicShowDetails";
            this.graphicColPublicShowDetails.OptionsField.SortFilterButtonShowMode = DevExpress.XtraGrid.Views.Layout.SortFilterButtonShowMode.Nowhere;
            this.graphicColPublicShowDetails.OptionsFilter.AllowAutoFilter = false;
            this.graphicColPublicShowDetails.OptionsFilter.AllowFilter = false;
            this.graphicColPublicShowDetails.Width = 88;
            // 
            // layoutViewField_graphicColPublicShowDetails
            // 
            this.layoutViewField_graphicColPublicShowDetails.EditorPreferredWidth = 169;
            this.layoutViewField_graphicColPublicShowDetails.Location = new System.Drawing.Point(201, 141);
            this.layoutViewField_graphicColPublicShowDetails.Name = "layoutViewField_graphicColPublicShowDetails";
            this.layoutViewField_graphicColPublicShowDetails.Size = new System.Drawing.Size(173, 24);
            this.layoutViewField_graphicColPublicShowDetails.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField_graphicColPublicShowDetails.TextToControlDistance = 0;
            this.layoutViewField_graphicColPublicShowDetails.TextVisible = false;
            // 
            // graphicColPublicAdd
            // 
            this.graphicColPublicAdd.Caption = "layoutViewColumn10";
            this.graphicColPublicAdd.ColumnEdit = this.repositoryAddRoom;
            this.graphicColPublicAdd.LayoutViewField = this.layoutViewField_graphicColPublicAdd;
            this.graphicColPublicAdd.Name = "graphicColPublicAdd";
            this.graphicColPublicAdd.OptionsField.SortFilterButtonShowMode = DevExpress.XtraGrid.Views.Layout.SortFilterButtonShowMode.Nowhere;
            // 
            // layoutViewField_graphicColPublicAdd
            // 
            this.layoutViewField_graphicColPublicAdd.EditorPreferredWidth = 165;
            this.layoutViewField_graphicColPublicAdd.Location = new System.Drawing.Point(546, 141);
            this.layoutViewField_graphicColPublicAdd.Name = "layoutViewField_graphicColPublicAdd";
            this.layoutViewField_graphicColPublicAdd.Size = new System.Drawing.Size(169, 24);
            this.layoutViewField_graphicColPublicAdd.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField_graphicColPublicAdd.TextToControlDistance = 0;
            this.layoutViewField_graphicColPublicAdd.TextVisible = false;
            // 
            // layoutViewCard1
            // 
            this.layoutViewCard1.CustomizationFormText = "TemplateCard";
            this.layoutViewCard1.ExpandButtonLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutViewCard1.GroupBordersVisible = false;
            this.layoutViewCard1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutViewField_graphicColPublicName,
            this.layoutViewField_graphicColPublicCity,
            this.layoutViewField_graphicColPublicziipCode,
            this.layoutViewField_graphicColPublicCapacity,
            this.layoutViewField_graphicColPublicISDNIP,
            this.layoutViewField_graphicColPublicSDHD,
            this.layoutViewField_graphicColPublicBusinessHrs,
            this.layoutViewField_graphicColPublicPicture,
            this.layoutViewField_graphicColPublicAdd,
            this.layoutViewField_graphicColPublicShowDetails,
            this.item7,
            this.item8,
            this.item10});
            this.layoutViewCard1.Name = "layoutViewTemplateCard";
            this.layoutViewCard1.Text = "TemplateCard";
            // 
            // item7
            // 
            this.item7.AllowHotTrack = false;
            this.item7.CustomizationFormText = "item7";
            this.item7.Location = new System.Drawing.Point(0, 140);
            this.item7.MinSize = new System.Drawing.Size(1, 1);
            this.item7.Name = "item7";
            this.item7.Size = new System.Drawing.Size(715, 1);
            this.item7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.item7.Text = "item7";
            this.item7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // item8
            // 
            this.item8.AllowHotTrack = false;
            this.item8.CustomizationFormText = "item8";
            this.item8.Location = new System.Drawing.Point(374, 141);
            this.item8.MinSize = new System.Drawing.Size(104, 24);
            this.item8.Name = "item8";
            this.item8.Size = new System.Drawing.Size(172, 24);
            this.item8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.item8.Text = "item8";
            this.item8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // item10
            // 
            this.item10.AllowHotTrack = false;
            this.item10.CustomizationFormText = "item10";
            this.item10.Location = new System.Drawing.Point(0, 141);
            this.item10.MinSize = new System.Drawing.Size(104, 24);
            this.item10.Name = "item10";
            this.item10.Size = new System.Drawing.Size(201, 24);
            this.item10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.item10.Text = "item10";
            this.item10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutPrivateAdvancedView
            // 
            this.layoutPrivateAdvancedView.ActiveFilterEnabled = false;
            this.layoutPrivateAdvancedView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5,
            this.gridColumn3,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn6,
            this.gridColumn12,
            this.gridColumn4,
            this.gridColumn9,
            this.gridColumn14,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn16,
            this.gridColumn20});
            this.layoutPrivateAdvancedView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.layoutPrivateAdvancedView.GridControl = this.gridControl_RoomsInfoNew;
            this.layoutPrivateAdvancedView.Name = "layoutPrivateAdvancedView";
            this.layoutPrivateAdvancedView.OptionsCustomization.AllowFilter = false;
            this.layoutPrivateAdvancedView.OptionsCustomization.AllowGroup = false;
            this.layoutPrivateAdvancedView.OptionsDetail.ShowDetailTabs = false;
            this.layoutPrivateAdvancedView.OptionsFilter.AllowColumnMRUFilterList = false;
            this.layoutPrivateAdvancedView.OptionsFilter.AllowFilterEditor = false;
            this.layoutPrivateAdvancedView.OptionsFilter.AllowMRUFilterList = false;
            this.layoutPrivateAdvancedView.OptionsMenu.EnableFooterMenu = false;
            this.layoutPrivateAdvancedView.OptionsMenu.EnableGroupPanelMenu = false;
            this.layoutPrivateAdvancedView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.layoutPrivateAdvancedView.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.layoutPrivateAdvancedView.OptionsView.ShowGroupPanel = false;
            this.layoutPrivateAdvancedView.OptionsView.ShowIndicator = false;
            this.layoutPrivateAdvancedView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn8, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.layoutPrivateAdvancedView.TopRowChanged += new System.EventHandler(this.layoutPrivateAdvancedView_TopRowChanged);
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Show Details";
            this.gridColumn5.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn5.MinWidth = 10;
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn5.OptionsFilter.AllowFilter = false;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 90;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Approval";
            this.gridColumn3.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn3.FieldName = "Approval";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Site";
            this.gridColumn7.FieldName = "Site";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 2;
            this.gridColumn7.Width = 106;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn8.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn8.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn8.Caption = "Name";
            this.gridColumn8.FieldName = "Name";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 3;
            this.gridColumn8.Width = 126;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn6.Caption = "Room Type";
            this.gridColumn6.FieldName = "Type";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn6.OptionsFilter.AllowFilter = false;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 4;
            this.gridColumn6.Width = 130;
            // 
            // gridColumn12
            // 
            this.gridColumn12.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn12.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn12.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn12.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn12.FieldName = "Capacity";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 5;
            this.gridColumn12.Width = 92;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Address";
            this.gridColumn4.FieldName = "Address";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 6;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "State";
            this.gridColumn9.FieldName = "State";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 9;
            this.gridColumn9.Width = 80;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Floor";
            this.gridColumn14.FieldName = "Floor";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 7;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn10.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn10.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn10.FieldName = "City";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 8;
            this.gridColumn10.Width = 142;
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn11.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn11.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn11.Caption = "Zip Code";
            this.gridColumn11.FieldName = "ZipCode";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 10;
            this.gridColumn11.Width = 115;
            // 
            // gridColumn16
            // 
            this.gridColumn16.FieldName = "Phone";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 11;
            this.gridColumn16.Width = 168;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Assistant";
            this.gridColumn20.FieldName = "Assistant";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 12;
            // 
            // checkEditPublicOnly
            // 
            this.checkEditPublicOnly.Location = new System.Drawing.Point(597, 35);
            this.checkEditPublicOnly.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkEditPublicOnly.Name = "checkEditPublicOnly";
            this.checkEditPublicOnly.Properties.Caption = "Public Only";
            this.checkEditPublicOnly.Size = new System.Drawing.Size(205, 19);
            this.checkEditPublicOnly.StyleController = this.layoutControl1;
            this.checkEditPublicOnly.TabIndex = 13;
            this.checkEditPublicOnly.CheckedChanged += new System.EventHandler(this.checkEditXX_CheckedChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem3,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem1,
            this.emptySpaceItem1,
            this.roomListLayoutControlItem,
            this.emptySpaceItem2,
            this.layoutControlItem2,
            this.calendarLayoutControlItem,
            this.emptySpaceItem3,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(917, 451);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.gridControl_RoomsInfoNew;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 87);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(104, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(899, 219);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.textEditSearchFor;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(123, 26);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 2, 2, 2);
            this.layoutControlItem5.Size = new System.Drawing.Size(593, 30);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem5.Text = "Search for:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(54, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.comboBoxSearchColumn;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(152, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 2, 2, 2);
            this.layoutControlItem3.Size = new System.Drawing.Size(593, 30);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem3.Text = "Search by:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(54, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.checkEditShowFavorites;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(593, 0);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(112, 31);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(213, 31);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 4, 2);
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.checkEditPublicOnly;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(593, 31);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(93, 29);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(213, 29);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dropBnChangeView;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(806, 60);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(104, 27);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(111, 27);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 60);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(1, 1);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(806, 27);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = " ";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(54, 0);
            this.emptySpaceItem1.TextVisible = true;
            // 
            // roomListLayoutControlItem
            // 
            this.roomListLayoutControlItem.AppearanceItemCaption.Options.UseTextOptions = true;
            this.roomListLayoutControlItem.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.roomListLayoutControlItem.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.roomListLayoutControlItem.Control = this.roomListBoxControl;
            this.roomListLayoutControlItem.CustomizationFormText = "roomListLayoutControlItem";
            this.roomListLayoutControlItem.Location = new System.Drawing.Point(93, 306);
            this.roomListLayoutControlItem.MinSize = new System.Drawing.Size(54, 20);
            this.roomListLayoutControlItem.Name = "roomListLayoutControlItem";
            this.roomListLayoutControlItem.Size = new System.Drawing.Size(715, 145);
            this.roomListLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.roomListLayoutControlItem.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 2);
            this.roomListLayoutControlItem.Text = "roomListLayoutControlItem";
            this.roomListLayoutControlItem.TextLocation = DevExpress.Utils.Locations.Top;
            this.roomListLayoutControlItem.TextSize = new System.Drawing.Size(0, 0);
            this.roomListLayoutControlItem.TextToControlDistance = 0;
            this.roomListLayoutControlItem.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 329);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(1, 1);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(93, 122);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.bnAdd;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 306);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(1, 1);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(93, 23);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // calendarLayoutControlItem
            // 
            this.calendarLayoutControlItem.Control = this.roomsCalendarButton;
            this.calendarLayoutControlItem.CustomizationFormText = "calendarLayoutControlItem";
            this.calendarLayoutControlItem.Location = new System.Drawing.Point(808, 406);
            this.calendarLayoutControlItem.MinSize = new System.Drawing.Size(109, 27);
            this.calendarLayoutControlItem.Name = "calendarLayoutControlItem";
            this.calendarLayoutControlItem.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 4);
            this.calendarLayoutControlItem.Size = new System.Drawing.Size(109, 45);
            this.calendarLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.calendarLayoutControlItem.Text = "Rooms Calendar";
            this.calendarLayoutControlItem.TextSize = new System.Drawing.Size(0, 0);
            this.calendarLayoutControlItem.TextToControlDistance = 0;
            this.calendarLayoutControlItem.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(808, 333);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(1, 1);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(109, 73);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.bnRemove;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(808, 306);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(95, 27);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(109, 27);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.vScrollBar1;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(899, 87);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(1, 1);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(18, 219);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            this.layoutControlItem9.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.OnlyInCustomization;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem7,
            this.layoutControlItem10,
            this.emptySpaceItem8});
            this.layoutControlGroup2.Location = new System.Drawing.Point(806, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(111, 60);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem7.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(111, 17);
            this.emptySpaceItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem7.Text = "emptySpaceItem7";
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.searchBn;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 17);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(10, 10);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(111, 27);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "layoutControlItem10";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(0, 44);
            this.emptySpaceItem8.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(111, 16);
            this.emptySpaceItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem8.Text = "emptySpaceItem8";
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutViewColumn1
            // 
            this.layoutViewColumn1.AppearanceCell.Options.UseTextOptions = true;
            this.layoutViewColumn1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutViewColumn1.FieldName = "Id";
            this.layoutViewColumn1.LayoutViewField = this.layoutViewField_layoutViewColumn1_1;
            this.layoutViewColumn1.Name = "layoutViewColumn1";
            this.layoutViewColumn1.OptionsColumn.AllowEdit = false;
            this.layoutViewColumn1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.layoutViewColumn1.OptionsColumn.ReadOnly = true;
            this.layoutViewColumn1.OptionsFilter.AllowAutoFilter = false;
            this.layoutViewColumn1.OptionsFilter.AllowFilter = false;
            // 
            // layoutViewField_layoutViewColumn1_1
            // 
            this.layoutViewField_layoutViewColumn1_1.EditorPreferredWidth = 481;
            this.layoutViewField_layoutViewColumn1_1.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_layoutViewColumn1_1.Name = "layoutViewField_layoutViewColumn1_1";
            this.layoutViewField_layoutViewColumn1_1.Size = new System.Drawing.Size(639, 20);
            this.layoutViewField_layoutViewColumn1_1.TextSize = new System.Drawing.Size(150, 16);
            this.layoutViewField_layoutViewColumn1_1.TextToControlDistance = 5;
            // 
            // layoutViewColumn2
            // 
            this.layoutViewColumn2.AppearanceCell.Options.UseTextOptions = true;
            this.layoutViewColumn2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutViewColumn2.Caption = "Room Name";
            this.layoutViewColumn2.FieldName = "Name";
            this.layoutViewColumn2.LayoutViewField = this.layoutViewField_layoutViewColumn2_1;
            this.layoutViewColumn2.Name = "layoutViewColumn2";
            this.layoutViewColumn2.OptionsColumn.AllowEdit = false;
            this.layoutViewColumn2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.layoutViewColumn2.OptionsColumn.ReadOnly = true;
            this.layoutViewColumn2.OptionsFilter.AllowAutoFilter = false;
            this.layoutViewColumn2.OptionsFilter.AllowFilter = false;
            // 
            // layoutViewField_layoutViewColumn2_1
            // 
            this.layoutViewField_layoutViewColumn2_1.EditorPreferredWidth = 481;
            this.layoutViewField_layoutViewColumn2_1.Location = new System.Drawing.Point(0, 20);
            this.layoutViewField_layoutViewColumn2_1.Name = "layoutViewField_layoutViewColumn2_1";
            this.layoutViewField_layoutViewColumn2_1.Size = new System.Drawing.Size(639, 20);
            this.layoutViewField_layoutViewColumn2_1.TextSize = new System.Drawing.Size(150, 16);
            this.layoutViewField_layoutViewColumn2_1.TextToControlDistance = 5;
            // 
            // layoutViewColumn3
            // 
            this.layoutViewColumn3.AppearanceCell.Options.UseTextOptions = true;
            this.layoutViewColumn3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutViewColumn3.ColumnEdit = this.repositoryItemPictureEdit2;
            this.layoutViewColumn3.FieldName = "Picture";
            this.layoutViewColumn3.LayoutViewField = this.layoutViewField_layoutViewColumn3_1;
            this.layoutViewColumn3.Name = "layoutViewColumn3";
            this.layoutViewColumn3.OptionsColumn.AllowEdit = false;
            this.layoutViewColumn3.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.layoutViewColumn3.OptionsColumn.ReadOnly = true;
            this.layoutViewColumn3.OptionsFilter.AllowAutoFilter = false;
            this.layoutViewColumn3.OptionsFilter.AllowFilter = false;
            this.layoutViewColumn3.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            // 
            // layoutViewField_layoutViewColumn3_1
            // 
            this.layoutViewField_layoutViewColumn3_1.EditorPreferredWidth = 481;
            this.layoutViewField_layoutViewColumn3_1.Location = new System.Drawing.Point(0, 40);
            this.layoutViewField_layoutViewColumn3_1.Name = "layoutViewField_layoutViewColumn3_1";
            this.layoutViewField_layoutViewColumn3_1.Size = new System.Drawing.Size(639, 26);
            this.layoutViewField_layoutViewColumn3_1.TextSize = new System.Drawing.Size(150, 16);
            this.layoutViewField_layoutViewColumn3_1.TextToControlDistance = 5;
            // 
            // layoutViewColumn4
            // 
            this.layoutViewColumn4.Caption = "Show Details";
            this.layoutViewColumn4.ColumnEdit = this.repositoryItemButtonEdit2;
            this.layoutViewColumn4.LayoutViewField = this.layoutViewField_layoutViewColumn4_1;
            this.layoutViewColumn4.Name = "layoutViewColumn4";
            this.layoutViewColumn4.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.layoutViewColumn4.OptionsFilter.AllowAutoFilter = false;
            this.layoutViewColumn4.OptionsFilter.AllowFilter = false;
            this.layoutViewColumn4.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            // 
            // layoutViewField_layoutViewColumn4_1
            // 
            this.layoutViewField_layoutViewColumn4_1.EditorPreferredWidth = 481;
            this.layoutViewField_layoutViewColumn4_1.Location = new System.Drawing.Point(0, 66);
            this.layoutViewField_layoutViewColumn4_1.Name = "layoutViewField_layoutViewColumn4_1";
            this.layoutViewField_layoutViewColumn4_1.Size = new System.Drawing.Size(639, 20);
            this.layoutViewField_layoutViewColumn4_1.TextSize = new System.Drawing.Size(150, 16);
            this.layoutViewField_layoutViewColumn4_1.TextToControlDistance = 5;
            // 
            // layoutViewColumn5
            // 
            this.layoutViewColumn5.FieldName = "Address";
            this.layoutViewColumn5.LayoutViewField = this.layoutViewField_layoutViewColumn5_1;
            this.layoutViewColumn5.Name = "layoutViewColumn5";
            this.layoutViewColumn5.OptionsColumn.AllowEdit = false;
            this.layoutViewColumn5.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.layoutViewColumn5.OptionsColumn.ReadOnly = true;
            this.layoutViewColumn5.OptionsFilter.AllowAutoFilter = false;
            this.layoutViewColumn5.OptionsFilter.AllowFilter = false;
            // 
            // layoutViewField_layoutViewColumn5_1
            // 
            this.layoutViewField_layoutViewColumn5_1.EditorPreferredWidth = 481;
            this.layoutViewField_layoutViewColumn5_1.Location = new System.Drawing.Point(0, 86);
            this.layoutViewField_layoutViewColumn5_1.Name = "layoutViewField_layoutViewColumn5_1";
            this.layoutViewField_layoutViewColumn5_1.Size = new System.Drawing.Size(639, 20);
            this.layoutViewField_layoutViewColumn5_1.TextSize = new System.Drawing.Size(150, 16);
            this.layoutViewField_layoutViewColumn5_1.TextToControlDistance = 5;
            // 
            // layoutViewColumn6
            // 
            this.layoutViewColumn6.AppearanceCell.Options.UseTextOptions = true;
            this.layoutViewColumn6.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutViewColumn6.FieldName = "Capacity";
            this.layoutViewColumn6.LayoutViewField = this.layoutViewField_layoutViewColumn6_1;
            this.layoutViewColumn6.Name = "layoutViewColumn6";
            this.layoutViewColumn6.OptionsColumn.AllowEdit = false;
            this.layoutViewColumn6.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.layoutViewColumn6.OptionsColumn.ReadOnly = true;
            this.layoutViewColumn6.OptionsFilter.AllowAutoFilter = false;
            this.layoutViewColumn6.OptionsFilter.AllowFilter = false;
            // 
            // layoutViewField_layoutViewColumn6_1
            // 
            this.layoutViewField_layoutViewColumn6_1.EditorPreferredWidth = 481;
            this.layoutViewField_layoutViewColumn6_1.Location = new System.Drawing.Point(0, 106);
            this.layoutViewField_layoutViewColumn6_1.Name = "layoutViewField_layoutViewColumn6_1";
            this.layoutViewField_layoutViewColumn6_1.Size = new System.Drawing.Size(639, 20);
            this.layoutViewField_layoutViewColumn6_1.TextSize = new System.Drawing.Size(150, 16);
            this.layoutViewField_layoutViewColumn6_1.TextToControlDistance = 5;
            // 
            // layoutViewColumn7
            // 
            this.layoutViewColumn7.FieldName = "State";
            this.layoutViewColumn7.LayoutViewField = this.layoutViewField_layoutViewColumn7_1;
            this.layoutViewColumn7.Name = "layoutViewColumn7";
            this.layoutViewColumn7.OptionsColumn.AllowEdit = false;
            this.layoutViewColumn7.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.layoutViewColumn7.OptionsColumn.ReadOnly = true;
            this.layoutViewColumn7.OptionsFilter.AllowAutoFilter = false;
            this.layoutViewColumn7.OptionsFilter.AllowFilter = false;
            // 
            // layoutViewField_layoutViewColumn7_1
            // 
            this.layoutViewField_layoutViewColumn7_1.EditorPreferredWidth = 481;
            this.layoutViewField_layoutViewColumn7_1.Location = new System.Drawing.Point(0, 126);
            this.layoutViewField_layoutViewColumn7_1.Name = "layoutViewField_layoutViewColumn7_1";
            this.layoutViewField_layoutViewColumn7_1.Size = new System.Drawing.Size(639, 20);
            this.layoutViewField_layoutViewColumn7_1.TextSize = new System.Drawing.Size(150, 16);
            this.layoutViewField_layoutViewColumn7_1.TextToControlDistance = 5;
            // 
            // layoutViewColumn8
            // 
            this.layoutViewColumn8.Caption = "layoutViewColumnAddBtn";
            this.layoutViewColumn8.ColumnEdit = this.repositoryAddRoom;
            this.layoutViewColumn8.LayoutViewField = this.layoutViewField_layoutViewColumn8_1;
            this.layoutViewColumn8.Name = "layoutViewColumn8";
            this.layoutViewColumn8.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.layoutViewColumn8.OptionsColumn.ShowCaption = false;
            this.layoutViewColumn8.OptionsFilter.AllowAutoFilter = false;
            this.layoutViewColumn8.OptionsFilter.AllowFilter = false;
            // 
            // layoutViewField_layoutViewColumn8_1
            // 
            this.layoutViewField_layoutViewColumn8_1.EditorPreferredWidth = 481;
            this.layoutViewField_layoutViewColumn8_1.Location = new System.Drawing.Point(0, 146);
            this.layoutViewField_layoutViewColumn8_1.Name = "layoutViewField_layoutViewColumn8_1";
            this.layoutViewField_layoutViewColumn8_1.Size = new System.Drawing.Size(639, 20);
            this.layoutViewField_layoutViewColumn8_1.TextSize = new System.Drawing.Size(150, 16);
            this.layoutViewField_layoutViewColumn8_1.TextToControlDistance = 5;
            // 
            // colIsFavorite
            // 
            this.colIsFavorite.AppearanceCell.Options.UseTextOptions = true;
            this.colIsFavorite.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colIsFavorite.AppearanceHeader.Options.UseTextOptions = true;
            this.colIsFavorite.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colIsFavorite.ColumnEdit = this.repositoryItemTextEdit1;
            this.colIsFavorite.FieldName = "IsFavorite";
            this.colIsFavorite.LayoutViewField = this.layoutViewField_colIsFavorite_1;
            this.colIsFavorite.Name = "colIsFavorite";
            this.colIsFavorite.OptionsColumn.AllowEdit = false;
            this.colIsFavorite.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colIsFavorite.OptionsColumn.ReadOnly = true;
            this.colIsFavorite.OptionsFilter.AllowAutoFilter = false;
            this.colIsFavorite.OptionsFilter.AllowFilter = false;
            // 
            // layoutViewField_colIsFavorite_1
            // 
            this.layoutViewField_colIsFavorite_1.EditorPreferredWidth = 481;
            this.layoutViewField_colIsFavorite_1.Location = new System.Drawing.Point(0, 166);
            this.layoutViewField_colIsFavorite_1.Name = "layoutViewField_colIsFavorite_1";
            this.layoutViewField_colIsFavorite_1.Size = new System.Drawing.Size(639, 20);
            this.layoutViewField_colIsFavorite_1.TextSize = new System.Drawing.Size(150, 16);
            this.layoutViewField_colIsFavorite_1.TextToControlDistance = 5;
            // 
            // colRoomType
            // 
            this.colRoomType.AppearanceCell.Options.UseTextOptions = true;
            this.colRoomType.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colRoomType.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colRoomType.Caption = "Type";
            this.colRoomType.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colRoomType.FieldName = "Type";
            this.colRoomType.LayoutViewField = this.layoutViewField_colRoomType_1;
            this.colRoomType.Name = "colRoomType";
            this.colRoomType.OptionsColumn.AllowEdit = false;
            this.colRoomType.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colRoomType.OptionsColumn.ReadOnly = true;
            this.colRoomType.OptionsFilter.AllowAutoFilter = false;
            this.colRoomType.OptionsFilter.AllowFilter = false;
            // 
            // layoutViewField_colRoomType_1
            // 
            this.layoutViewField_colRoomType_1.EditorPreferredWidth = 481;
            this.layoutViewField_colRoomType_1.Location = new System.Drawing.Point(0, 186);
            this.layoutViewField_colRoomType_1.Name = "layoutViewField_colRoomType_1";
            this.layoutViewField_colRoomType_1.Size = new System.Drawing.Size(639, 20);
            this.layoutViewField_colRoomType_1.TextSize = new System.Drawing.Size(150, 16);
            this.layoutViewField_colRoomType_1.TextToControlDistance = 5;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "Item1";
            this.emptySpaceItem4.Location = new System.Drawing.Point(390, 141);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(104, 24);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(164, 24);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "Empty Space Item";
            this.emptySpaceItem5.Location = new System.Drawing.Point(211, 140);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(1, 1);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(523, 1);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "Item2";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 140);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(104, 24);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(211, 25);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.Text = "emptySpaceItem6";
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // item1
            // 
            this.item1.AllowHotTrack = false;
            this.item1.CustomizationFormText = "item1";
            this.item1.Location = new System.Drawing.Point(192, 144);
            this.item1.Name = "item1";
            this.item1.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.item1.Size = new System.Drawing.Size(514, 10);
            this.item1.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.item1.Text = "item1";
            this.item1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // item3
            // 
            this.item3.AllowHotTrack = false;
            this.item3.CustomizationFormText = "item3";
            this.item3.Location = new System.Drawing.Point(590, 156);
            this.item3.MinSize = new System.Drawing.Size(106, 26);
            this.item3.Name = "item3";
            this.item3.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.item3.Size = new System.Drawing.Size(116, 26);
            this.item3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.item3.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.item3.Text = "item3";
            this.item3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // item4
            // 
            this.item4.AllowHotTrack = false;
            this.item4.CustomizationFormText = "item4";
            this.item4.Location = new System.Drawing.Point(192, 156);
            this.item4.MinSize = new System.Drawing.Size(106, 26);
            this.item4.Name = "item4";
            this.item4.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.item4.Size = new System.Drawing.Size(119, 26);
            this.item4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.item4.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.item4.Text = "item4";
            this.item4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // item6
            // 
            this.item6.AllowHotTrack = false;
            this.item6.CustomizationFormText = "item6";
            this.item6.Location = new System.Drawing.Point(192, 40);
            this.item6.Name = "item6";
            this.item6.Size = new System.Drawing.Size(514, 2);
            this.item6.Text = "item6";
            // 
            // item2
            // 
            this.item2.AllowHotTrack = false;
            this.item2.CustomizationFormText = "item2";
            this.item2.Location = new System.Drawing.Point(192, 82);
            this.item2.Name = "item2";
            this.item2.Size = new System.Drawing.Size(514, 2);
            this.item2.Text = "item2";
            // 
            // item5
            // 
            this.item5.AllowHotTrack = false;
            this.item5.CustomizationFormText = "item5";
            this.item5.Location = new System.Drawing.Point(192, 154);
            this.item5.Name = "item5";
            this.item5.Size = new System.Drawing.Size(514, 2);
            this.item5.Text = "item5";
            // 
            // item9
            // 
            this.item9.AllowHotTrack = false;
            this.item9.CustomizationFormText = "item9";
            this.item9.Location = new System.Drawing.Point(437, 156);
            this.item9.Name = "item9";
            this.item9.Size = new System.Drawing.Size(19, 26);
            this.item9.Text = "item9";
            this.item9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem9.Location = new System.Drawing.Point(10, 0);
            this.emptySpaceItem9.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(81, 57);
            this.emptySpaceItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem9.Text = "emptySpaceItem9";
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // backgroundWorkerFirstPage
            // 
            this.backgroundWorkerFirstPage.WorkerSupportsCancellation = true;
            this.backgroundWorkerFirstPage.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerFirstPage_DoWork);
            this.backgroundWorkerFirstPage.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerFirstPage_RunWorkerCompleted);
            // 
            // RoomSelectionNew
            // 
            this.AcceptButton = this.searchBn;
            this.ApplyEnabled = true;
            this.ApplyVisible = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(941, 507);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "RoomSelectionNew";
            this.Text = "Room Selection";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.RoomSelectionNew_Closing);
            this.Closed += new System.EventHandler(this.RoomSelectionNew_Closed);
            this.Load += new System.EventHandler(this.RoomSelectionNew_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).EndInit();
            this.ContentPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditSearchFor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.roomListBoxControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxSearchColumn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditShowFavorites.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_RoomsInfoNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPublicBasic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryAddRoom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPrivateBasicView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPublicGraphic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_graphicColPublicPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_graphicColPublicName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_graphicColPublicCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_graphicColPublicziipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_graphicColPublicCapacity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_graphicColPublicISDNIP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_graphicColPublicSDHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_graphicColPublicBusinessHrs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_graphicColPublicShowDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_graphicColPublicAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPrivateAdvancedView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditPublicOnly.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.roomListLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calendarLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn2_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn3_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn4_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn5_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn6_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn7_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn8_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colIsFavorite_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colRoomType_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            this.ResumeLayout(false);

		}

		
		#endregion

		private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
		private DevExpress.XtraLayout.LayoutControl layoutControl1;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private DevExpress.XtraGrid.GridControl gridControl_RoomsInfoNew;
		private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn1;
		private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn2;
		private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn3;
		private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit2;
		private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn4;
		private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit2;
		private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn5;
		private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn6;
		private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn7;
		private DevExpress.XtraGrid.Views.Grid.GridView layoutPublicBasic;
		private DevExpress.XtraGrid.Columns.GridColumn colPublicPicture;
		private DevExpress.XtraGrid.Columns.GridColumn colPublicName;
		private DevExpress.XtraGrid.Columns.GridColumn colPublicState;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
		private DevExpress.XtraGrid.Columns.GridColumn colPublicShowDetails;
		private DevExpress.XtraEditors.ComboBoxEdit comboBoxSearchColumn;
		private DevExpress.XtraEditors.TextEdit textEditSearchFor;
		private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn8;
		private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryAddRoom;
		private DevExpress.XtraEditors.ListBoxControl roomListBoxControl;
		private DevExpress.XtraEditors.CheckEdit checkEditShowFavorites;
		private DevExpress.XtraEditors.CheckEdit checkEditPublicOnly;
		private DevExpress.XtraEditors.SimpleButton roomsCalendarButton;
		private DevExpress.XtraGrid.Columns.LayoutViewColumn colIsFavorite;
		private DevExpress.XtraGrid.Columns.LayoutViewColumn colRoomType;
		private DevExpress.XtraLayout.EmptySpaceItem item1;
		private DevExpress.XtraLayout.EmptySpaceItem item3;
		private DevExpress.XtraLayout.EmptySpaceItem item4;
		private DevExpress.XtraLayout.SimpleSeparator item6;
		private DevExpress.XtraLayout.SimpleSeparator item2;
		private DevExpress.XtraLayout.SimpleSeparator item5;
		private DevExpress.XtraLayout.EmptySpaceItem item9;
		private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
		private DevExpress.XtraGrid.Columns.GridColumn colPublicRoomType;
		private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
		private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
		private DevExpress.XtraGrid.Columns.GridColumn colPublicSite;
		private DevExpress.XtraGrid.Columns.GridColumn colPublicZipCode;
		private DevExpress.XtraGrid.Columns.GridColumn colPublicCity;
		private DevExpress.XtraGrid.Columns.GridColumn colPublicPhone;
		private DevExpress.XtraGrid.Columns.GridColumn colPublicBusinesPrice;
		private DevExpress.XtraGrid.Columns.GridColumn colPublicCurrency;
		private DevExpress.XtraGrid.Columns.GridColumn colPublicISDNIP;
		private DevExpress.XtraGrid.Columns.GridColumn colPublicSDHD;
		private DevExpress.XtraGrid.Columns.GridColumn colPublicAutomated;
		private DevExpress.XtraEditors.DropDownButton dropBnChangeView;
		private DevExpress.XtraEditors.SimpleButton bnRemove;
		private DevExpress.XtraEditors.SimpleButton bnAdd;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
		private DevExpress.XtraLayout.LayoutControlItem roomListLayoutControlItem;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
		private DevExpress.XtraLayout.LayoutControlItem calendarLayoutControlItem;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
		private DevExpress.XtraGrid.Views.Layout.LayoutView layoutPublicGraphic;
		private DevExpress.XtraGrid.Columns.LayoutViewColumn graphicColPublicPicture;
		private DevExpress.XtraGrid.Columns.LayoutViewColumn graphicColPublicShowDetails;
		private DevExpress.XtraGrid.Columns.LayoutViewColumn graphicColPublicName;
		private DevExpress.XtraGrid.Columns.LayoutViewColumn graphicColPublicziipCode;
		private DevExpress.XtraGrid.Columns.LayoutViewColumn graphicColPublicCity;
		private DevExpress.XtraGrid.Columns.LayoutViewColumn graphicColPublicBusinessHrs;
		private DevExpress.XtraGrid.Columns.LayoutViewColumn graphicColPublicISDNIP;
		private DevExpress.XtraGrid.Columns.LayoutViewColumn graphicColPublicSDHD;
		private DevExpress.XtraGrid.Columns.LayoutViewColumn graphicColPublicCapacity;
		private DevExpress.XtraGrid.Columns.LayoutViewColumn graphicColPublicAdd;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn10_1;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn11;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn14;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn16;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn17;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn19;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn21;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn22;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn24;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn25;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn26;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn27;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn28;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn29;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn30;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn31;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn32;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn10_2;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn11_1;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn14_1;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn16_1;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn17_1;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn19_1;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn21_1;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn22_1;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn24_1;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn25_1;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn26_1;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn27_1;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn28_1;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn29_1;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn30_1;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn31_1;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn32_1;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField8;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField9;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField10;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField11;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField12;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField13;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField14;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField15;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField16;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField17;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField18;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField19;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField20;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField21;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField22;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField23;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField24;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField1;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField2;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField3;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField4;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField5;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField6;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField7;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField25;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField26;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField27;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField28;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField29;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField30;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField31;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField32;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField33;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField34;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField35;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField36;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField37;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField38;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField39;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField40;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField41;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField42;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField43;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField44;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField45;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField46;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField47;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField48;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField49;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField50;
		//private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField51;
		private DevExpress.XtraGrid.Views.Grid.GridView layoutPrivateBasicView;
		private DevExpress.XtraGrid.Columns.GridColumn layoutViewColumn14;
		private DevExpress.XtraGrid.Columns.GridColumn layoutViewColumn16;
		private DevExpress.XtraGrid.Columns.GridColumn layoutViewColumn17;
		private DevExpress.XtraGrid.Columns.GridColumn layoutViewColumn19;
		private DevExpress.XtraGrid.Columns.GridColumn layoutViewColumn21;
		private DevExpress.XtraGrid.Columns.GridColumn layoutViewColumn22;
		private DevExpress.XtraGrid.Columns.GridColumn layoutViewColumn24;
		private DevExpress.XtraGrid.Columns.GridColumn layoutViewColumn25;
		private DevExpress.XtraGrid.Columns.GridColumn layoutViewColumn29;
		private DevExpress.XtraGrid.Views.Grid.GridView layoutPrivateAdvancedView;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
		private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn1_1;
		private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn2_1;
		private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn3_1;
		private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn4_1;
		private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn5_1;
		private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn6_1;
		private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn7_1;
		private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn8_1;
		private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colIsFavorite_1;
		private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colRoomType_1;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
		private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_graphicColPublicPicture;
		private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_graphicColPublicName;
		private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_graphicColPublicCity;
		private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_graphicColPublicziipCode;
		private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_graphicColPublicCapacity;
		private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_graphicColPublicISDNIP;
		private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_graphicColPublicSDHD;
		private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_graphicColPublicBusinessHrs;
		private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_graphicColPublicShowDetails;
		private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_graphicColPublicAdd;
		private DevExpress.XtraGrid.Views.Layout.LayoutViewCard layoutViewCard1;
		private DevExpress.XtraLayout.EmptySpaceItem item7;
		private DevExpress.XtraLayout.EmptySpaceItem item8;
		private DevExpress.XtraLayout.EmptySpaceItem item10;
		private DevExpress.XtraEditors.VScrollBar vScrollBar1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
		private DevExpress.XtraGrid.Columns.GridColumn colPublicCountry;
		private DevExpress.XtraEditors.SimpleButton searchBn;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
		private System.ComponentModel.BackgroundWorker backgroundWorkerFirstPage;
		private DevExpress.XtraGrid.Columns.GridColumn colPublicCapacity;
	}
}