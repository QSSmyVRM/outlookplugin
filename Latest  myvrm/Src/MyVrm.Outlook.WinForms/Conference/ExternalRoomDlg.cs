﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Layout;
using DevExpress.XtraLayout.Utils;
using MyVrm.Common;
using MyVrm.WebServices.Data;
using System.Text.RegularExpressions;
using MyVrm.Common.ComponentModel;
using DevExpress.XtraEditors.ViewInfo;

namespace MyVrm.Outlook.WinForms.Conference
{
    public partial class ExternalRoomDlg : MyVrm.Outlook.WinForms.Dialog
	{
		public enum Mode
		{
			Add,
			Edit,
            Show //ALLDEV-763
		}

		internal Mode DlgMode;
		internal AudioVideoConferenceAVSettingsPage.AVPageType PageType;
// ZD 101343 starts
        public int ConfGuestLisc;
        private EndpointId EndPtID = null;
        private string ConfGuestRID = null;
        private string RoomID = null;
// ZD 101343 Ends
		private Dictionary<int, string> _connectionTypes = null;
		private Dictionary<int, string> _lineRates = null;
		private Dictionary<int, string> _videoProtocols = null;
		private Dictionary<int, string> _addressTypes = null;
		private VideoEquipmentCollection _videoEquipment = null;
		private ReadOnlyCollection<BridgeName> _bridges = null;
		private ReadOnlyCollection<MCUProfile> _profiles = null;

        //ALLDEV-763
        public ConferenceWrapper ConfRef;
        private RoomId rmID;
        private DevExpress.Utils.ImageCollection imgCollection;

		public ExternalRoom Room { set; get; }
		public enum ConnectionTypes
		{
			[LocalizedDescription("ConnectionTypeDialinToMCU", typeof(Strings))]
			DialIn = 1,
			[LocalizedDescription("ConnectionTypeDialoutFromMCU", typeof(Strings))]
			DialOut = 2,
			[LocalizedDescription("ConnectionTypeDirectToMCU", typeof(Strings))]
			Direct = 3
		}

        public ExternalRoomDlg(int CheckGust)// ZD 101343
		{
			InitializeComponent();
			ApplyEnabled = false;
			ApplyVisible = false;
            ConfGuestLisc = CheckGust;// ZD 101343
			Text = Strings.TxtExternalRoomDlgText;
			userInfoGroupBox.Text = Strings.TxtExternalRoomDlg_UserInfo;
			userFirstNameItem.Text = Strings.TxtExternalRoomDlg_FirstName;
			userLastNameItem.Text = Strings.TxtExternalRoomDlg_LastName;
			userEmailItem.Text = Strings.TxtExternalRoomDlg_Email;
			roomInfoGroupBox.Text = Strings.TxtExternalRoomDlg_RoomInfo;
			addressPhoneItem.Text = Strings.TxtExternalRoomDlg_AddressPhone;
			addressTypeItem.Text = Strings.TxtExternalRoomDlg_AddressType;
			connectionItem.Text = Strings.TxtExternalRoomDlg_ConnectionType;
			protocolItem.Text = Strings.TxtExternalRoomDlg_Protocol;
			apiPortItem.Text = Strings.TxtExternalRoomDlg_APIPort;
			lineRateItem.Text = Strings.TxtExternalRoomDlg_LineRate;
			urlItem.Text = Strings.TxtExternalRoomDlg_URL;
			MCUControl.Text = Strings.TxtExternalRoomDlg_MCU;
			ProfileControl.Text = Strings.TxtExternalRoomDlg_Profile;
			EquipmentControl.Text = Strings.TxtExternalRoomDlg_Equipment;
			CallerCalleeControl.Text = Strings.TxtExternalRoomDlg_CallerCallee;
            chkBoxExternalRoom.Text = Strings.EndpointstringText;
            EndPtName.Text = Strings.EndpointName;
            MediaTypeControl.Text = Strings.ExConnection;//ZD 104278
			
			equipmentCombo.Properties.NullText = Strings.TxtExternalRoomDlg_PleaseSelect;
			protocolCombo.Properties.NullText =Strings.TxtExternalRoomDlg_PleaseSelect;
			connectionCombo.Properties.NullText = Strings.TxtExternalRoomDlg_PleaseSelect;
			addressTypesCombo.Properties.NullText = Strings.TxtExternalRoomDlg_PleaseSelect;
			mcuCombo.Properties.NullText = Strings.TxtExternalRoomDlg_PleaseSelect;
			lineRateCombo.Properties.NullText = Strings.TxtExternalRoomDlg_PleaseSelect;
            MediaTypeCombo.Properties.NullText = Strings.TxtExternalRoomDlg_PleaseSelect;
            //MediaTypeCombo.EditValueChanging += Connection_EditValueChanged;

            addressPhone.Properties.ValidateOnEnterKey = true;
		}
        //ALLDEV-763
        public ExternalRoomDlg(DataList<Participant> partys, ref ConferenceWrapper Conf, RoomId id, ref DevExpress.Utils.ImageCollection imageCollection)
        {
            ConfRef = Conf;
            rmID = id;
            imgCollection = imageCollection;
            InitializeComponent_Party();

        }

        private void lstChkParty_DrawItem(object sender, DevExpress.XtraEditors.ListBoxDrawItemEventArgs e)
        {
            //if (lstChkParty.GetItemChecked(e.Index))
            //{
                CheckedListBoxViewInfo cInfo = (CheckedListBoxViewInfo)lstChkParty.GetViewInfo();
                Size checkBoxSize = cInfo.CheckMarkSize;
                e.Graphics.DrawImage(imgCollection.Images[0], new Rectangle(e.Bounds.X, e.Bounds.Y, checkBoxSize.Width, checkBoxSize.Height));
                e.Graphics.DrawString(e.Item.ToString(), e.Appearance.Font, e.Cache.GetSolidBrush(Color.Black), new Point(checkBoxSize.Width + e.Bounds.X, e.Bounds.Y));
                e.Handled = true;
            //}
        }

        void lstChkParty_ItemCheck(object sender, DevExpress.XtraEditors.Controls.ItemCheckEventArgs e)
        {
            CheckedListBoxControl clb = (CheckedListBoxControl)sender;

            foreach (var participant in ConfRef.Participants)
            {
                if (clb.Items[e.Index].Value.ToString() == participant.FirstName)
                {
                    if (clb.Items[e.Index].CheckState == CheckState.Checked)
                    {
                        participant.partyRoomID = rmID.Id;
                        ConfRef.partyAssignedRoom += participant.Email + ',' + rmID.Id + "|";
                    }
                    else
                    {
                        participant.partyRoomID = ""; // participant.partyRoomID.Replace(participant.Email + ',' + rmID.Id + "|", "");
                        ConfRef.partyAssignedRoom = ConfRef.partyAssignedRoom.Replace(participant.Email + ',' + rmID.Id + "|", "");
                    }
                }
            }
        }

        private void ExternalRoomDlg_Party_Load(object sender, EventArgs e)
        {
            Text = Strings.AllAttendees;
            
            Boolean isPartyExists = false;
            
            foreach (var participant in ConfRef.Participants)
            {
                if (participant.partyRoomID == "" || participant.partyRoomID == null || participant.partyRoomID == "0" || participant.partyRoomID == rmID.Id)
                {
                    isPartyExists = true;

                    if (participant.partyRoomID != null && participant.partyRoomID == rmID.Id)
                    {
                        this.lstChkParty.Items.Add(participant.FirstName, true);

                        if (ConfRef.partyAssignedRoom == null || (ConfRef.partyAssignedRoom != null && ConfRef.partyAssignedRoom.IndexOf(participant.Email + ',' + rmID.Id + "|") < 0))
                            ConfRef.partyAssignedRoom += participant.Email + ',' + rmID.Id + "|";
                    }
                    else
                    {
                        this.lstChkParty.Items.Add(participant.FirstName);
                        //this.lstChkParty.HtmlImages
                    }
                }
            }

            if (!isPartyExists)
                this.lstChkParty.Items.Add("No Data to Display", CheckState.Indeterminate,false);
            
            if (MyVrmService.Service.OrganizationOptions.EnableAssignPartytoRoom)
            {
                CancelVisible = false;
            }
        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (Form.ModifierKeys == Keys.None && keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            return base.ProcessDialogKey(keyData);
        }

        //ALLDEV-763 - End

		public override sealed string Text
		{
			get { return base.Text; }
			set { base.Text = value; }
		}

        private void ExternalRoomDlg_Load(object sender, EventArgs e)
        {

            if (DesignMode)
                return;

            switch (DlgMode)
            {
                case Mode.Add:
                    Text = Strings.TxtAddExternalRoom;
                    break;
                case Mode.Edit:
                    Text = Strings.TxtEditSelectedExternalRoom;
                    break;
            }

            int iH = 0;
            if (PageType == AudioVideoConferenceAVSettingsPage.AVPageType.PointToPoint)
            {
                CallerCalleeControl.Visibility = LayoutVisibility.Always;
                //Point first = MCUControl.Location;
                //Point second = ProfileControl.Location;
                //iH = MCUControl.Height + ProfileControl.Height;
                emptySpaceItem2.Height += iH;
                MCUControl.Visibility = LayoutVisibility.Never;
                ProfileControl.Visibility = LayoutVisibility.Never;
                MCUControl.Dispose();
                ProfileControl.Dispose();
                //CallerCalleeControl.Location = first;
                //EquipmentControl.Location = second; 
                callerCalleeCombo.Properties.Items.Add(
                    LocalizedDescriptionAttribute.FromEnum(ConferenceEndpointCallMode.Callee.GetType(), ConferenceEndpointCallMode.Callee));
                //ConferenceEndpointCallMode.Callee.ToString());// "Callee");
                callerCalleeCombo.Properties.Items.Add(
                    LocalizedDescriptionAttribute.FromEnum(ConferenceEndpointCallMode.Caller.GetType(), ConferenceEndpointCallMode.Caller));
                //ConferenceEndpointCallMode.Caller.ToString());//"Caller");
                callerCalleeCombo.SelectedIndex = 0;
            }
            else
            {
                //iH = CallerCalleeControl.Height;
                CallerCalleeControl.Visibility = LayoutVisibility.Never;
                //CallerCalleeControl.Dispose();
                MCUControl.Visibility = LayoutVisibility.Always;
                ProfileControl.Visibility = LayoutVisibility.Always;
            }

            //
            //_mediaTypes = MyVrmService.Service.MediaTypes;
            SuspendLayout();
            _connectionTypes = new Dictionary<int, string>();
            string cc = WinForms.Strings.ConnectionTypeDialoutFromMCU;
            _connectionTypes.Add((int)ConnectionTypes.DialIn,
                Common.LocalizedDescriptionAttribute.FromEnum(ConnectionTypes.DialIn.GetType(), ConnectionTypes.DialIn));
            //ConnectionTypes.DialIn.ToString());//"Dial-in to MCU");
            _connectionTypes.Add((int)ConnectionTypes.DialOut,
                Common.LocalizedDescriptionAttribute.FromEnum(ConnectionTypes.DialOut.GetType(), ConnectionTypes.DialOut));
            //ConnectionTypes.DialOut.ToString());//"Dial-out from MCU");
            _connectionTypes.Add((int)ConnectionTypes.Direct,
                Common.LocalizedDescriptionAttribute.FromEnum(ConnectionTypes.Direct.GetType(), ConnectionTypes.Direct));
            //ConnectionTypes.Direct.ToString());//"Direct to MCU");
            FillCombo(_connectionTypes, connectionCombo);

            _lineRates = MyVrmService.Service.LineRates;
            FillCombo(_lineRates, lineRateCombo);


            //ZD 104278 start
            //MediaTypeCombo.Properties.Items.Add(
            //      LocalizedDescriptionAttribute.FromEnum(MediaType.AudioOnly.GetType(), MediaType.AudioOnly));
            //MediaTypeCombo.Properties.Items.Add(
            //    LocalizedDescriptionAttribute.FromEnum(MediaType.AudioOnly.GetType(), MediaType.AudioVideo));
            EndPtName.Visibility = LayoutVisibility.Never;
            MediaTypeCombo.Properties.Items.Clear();
            if (PageType == AudioVideoConferenceAVSettingsPage.AVPageType.Audioonly)
            {
                MediaTypeCombo.Properties.Items.Add(
                  LocalizedDescriptionAttribute.FromEnum(MediaType.AudioOnly.GetType(), MediaType.AudioOnly));
                MediaTypeCombo.SelectedIndex = 0;

            }
            else
            {
                MediaTypeCombo.Properties.Items.Add(
                 LocalizedDescriptionAttribute.FromEnum(MediaType.AudioOnly.GetType(), MediaType.AudioOnly));
                MediaTypeCombo.Properties.Items.Add(
                    LocalizedDescriptionAttribute.FromEnum(MediaType.AudioOnly.GetType(), MediaType.AudioVideo));
                MediaTypeCombo.Enabled = true;
                MediaTypeCombo.SelectedIndex = 1;
            }
            //ZD 104278 End

            _videoProtocols = MyVrmService.Service.VideoProtocols;
            FillCombo(_videoProtocols, protocolCombo);
            _addressTypes = MyVrmService.Service.AddressTypes;
            FillCombo(_addressTypes, addressTypesCombo);
            _videoEquipment = MyVrmService.Service.VideoEquipment;

            if (_videoEquipment.Count > 0)
            {
                foreach (var equip in _videoEquipment)
                {
                    equipmentCombo.Properties.Items.Add(equip.Name);
                }
                equipmentCombo.SelectedItem = 0; //??
            }

            _bridges = MyVrmService.Service.Bridges;
            if (_bridges.Count > 0)
            {
                foreach (var bridge in _bridges)
                {
                    mcuCombo.Properties.Items.Add(bridge.Name);
                }
                mcuCombo.SelectedItem = 0; //??
            }
            ResumeLayout();

            if (Room != null)
            {
                FillRoomData();
            }
        }
        

		void FillRoomData()
		{
            chkBoxExternalRoom.Text = Strings.EndpointstringText;
			addressPhone.Text = Room.AddressPhone;
			userEmail.Text = Room.UserEmail;
			userFirstName.Text = Room.UserFirstName;
			userLastName.Text = Room.UserLastName;
			apiPort.Text = Room.APIPort;
			url.Text = Room.URL;
            EndPtID = Room.EndPointID ;//ZD 101343
            ConfGuestRID = Room.ID;//ZD 101343
			if(Room.AddressType != null)
				addressTypesCombo.EditValue = Room.AddressType.SecondObj;
			
			if (Room.ConnectionType != null)
				connectionCombo.EditValue = Room.ConnectionType.SecondObj;
			
			if(Room.ProtocolType != null)
				protocolCombo.EditValue = Room.ProtocolType.SecondObj;
			
			if(Room.LineRate != null )
				lineRateCombo.EditValue = Room.LineRate.SecondObj;
			
			if (PageType != AudioVideoConferenceAVSettingsPage.AVPageType.PointToPoint)
			{
				if (Room.MCU != null)
					mcuCombo.EditValue = Room.MCU.SecondObj;
				
				if(Room.MCUProfile != null)
					profileCombo.EditValue = Room.MCUProfile.SecondObj;
			}
            if(!string.IsNullOrEmpty(Room.RoomID))//ZD 101343
            RoomID = Room.RoomID;
			if (Room.Equipment != null)
				equipmentCombo.EditValue = Room.Equipment.SecondObj;

			if (PageType == AudioVideoConferenceAVSettingsPage.AVPageType.PointToPoint)
			{
				if (Room.CallerCallee != null)
					callerCalleeCombo.EditValue = Room.CallerCallee.SecondObj;
			}
//ZD 101343 starts
            if (Room.IsGuestLoc)
            {
                chkBoxExternalRoom.Checked = true;
                txtEdnpt.Text = Room.EndPtName;
               
               EndPtName.Visibility = LayoutVisibility.Always;
                
            }
//ZD 101343 Ends

            //ZD 104278
            if (PageType == AudioVideoConferenceAVSettingsPage.AVPageType.Audioonly)
            {
                MediaTypeCombo.SelectedIndex = 0;
            }
            else
            {
                if(Room.Connection == 1)
                    MediaTypeCombo.SelectedIndex = 0;
                else
                    MediaTypeCombo.SelectedIndex = 1;
            }
           //ZZD 104278
		}
		void FillCombo(Dictionary<int, string > src, EnumComboBoxEdit combo)
		{
			if (src.Count > 0)
			{
				combo.Properties.Items.AddRange(src.Values);
				combo.SelectedItem = 0; //??
			}
		}
		void FillCombo(Dictionary<int, string> src, ComboBoxEdit combo)
		{
			if (src.Count > 0)
			{
				combo.Properties.Items.AddRange(src.Values);
				combo.SelectedItem = 0; //??
			}
		}

		private void mcuCombo_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
		{
			if(!e.Cancel)
			{
				profileCombo.Properties.Items.Clear();
				foreach (var bridge in _bridges)
				{
					if (bridge.Name == (string) e.NewValue)
					{
						profileCombo.EditValue = string.Empty;
						_profiles = MyVrmService.Service.GetMCUProfiles(bridge.Id.Id);
						if (_profiles.Count > 0) //RMX
						{
							profileCombo.Properties.Items.Add(MCUProfile.MCUProfileNone.Name);
							if (bridge.DefaultBridgeProfileId == MCUProfile.MCUProfileNone.Id)
								profileCombo.EditValue = MCUProfile.MCUProfileNone.Name;
						}
						else //codian
						{
							profileCombo.Properties.Items.Add(MCUProfile.MCUProfileNoItems.Name);
							profileCombo.EditValue = MCUProfile.MCUProfileNoItems.Name;
						}
						foreach (var profile in _profiles)
						{
							profileCombo.Properties.Items.Add(profile.Name);
							if (bridge.DefaultBridgeProfileId == profile.Id)
								profileCombo.EditValue = profile.Name;
						}
						break;
					}
				}
			}
		}

        private void ExternalRoomDlg_FormClosing(object sender, FormClosingEventArgs e)
        {
//ZD 101343 starts
            try
            {
              

                if (((ExternalRoomDlg)sender).DialogResult == DialogResult.OK)
                {
                   
                  
                    if (chkBoxExternalRoom.Checked)
                    {
                        if (string.IsNullOrEmpty(txtEdnpt.Text))
                        {
                            //UIHelper.ShowError("EndPointName Required For External Guest Location");
                            txtEdnpt.ErrorText = "EndPointName Required For External Guest Location";
                            e.Cancel = true;
                        }
                    }

                    if (chkBoxExternalRoom.Checked && ConfGuestLisc<1)
                    {
                        MessageBox.Show(Strings.guestlicenses, Strings.Alert);
                        // UIHelper.ShowError("ChkBOx");
                        e.Cancel = true;
                    }
//ZD 101343 ends

                    
                    if (string.IsNullOrEmpty(userFirstName.Text) || string.IsNullOrEmpty(userLastName.Text) ||
                        string.IsNullOrEmpty(userEmail.Text))
                    {
                        UIHelper.ShowError(Strings.TxtFillUserData);
                        e.Cancel = true;
                    }
                    string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                                            @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                                                    @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"; //ZD 103889 start
                    Regex regex = new Regex(strRegex);
                    if(!string.IsNullOrEmpty(userEmail.Text))
                    {
                        if (!regex.IsMatch(userEmail.Text))
                        {
                            e.Cancel = true;
                            userEmail.ErrorText = Strings.Invalidemailaddress;
                        }
                    }
                   
                    //ZD 103889 End
                    if (string.IsNullOrEmpty(addressPhone.Text.Trim()))
                    {
                        e.Cancel = true;
                        addressPhone.ErrorText = Strings.TxtFillAddressPhone;
                    }
                    if (addressTypesCombo.SelectedIndex == -1 || addressTypesCombo.SelectedItem == null)
                    {

                        e.Cancel = true;
                        addressTypesCombo.ErrorText = Strings.TxtSelectAddressTypes;
                    }

                    if ((mcuCombo.SelectedIndex == -1 || mcuCombo.SelectedItem == null) && PageType != AudioVideoConferenceAVSettingsPage.AVPageType.PointToPoint)//101309 zd
                    {
                        e.Cancel = true;
                        mcuCombo.ErrorText = Strings.TxtSelectMCU;
                    }

                    

                    if (protocolCombo.SelectedIndex == -1 || protocolCombo.SelectedItem == null)
                    {
                        e.Cancel = true;
                        protocolCombo.ErrorText = Strings.TxtSelectProtocol;
                    }

                    if (connectionCombo.SelectedIndex == -1 || connectionCombo.SelectedItem == null)
                    {
                        e.Cancel = true;
                        connectionCombo.ErrorText = Strings.TxtSelectConnectionType;
                    }

                    if (lineRateCombo.SelectedIndex == -1 || lineRateCombo.SelectedItem == null)
                    {
                        e.Cancel = true;
                        lineRateCombo.ErrorText = Strings.TxtSelectLineRate;
                    }


                    if (!e.Cancel)
                    {

                        Room = new ExternalRoom() { UserId = Room != null ? Room.UserId : UserId.New };
                      
                        Room.AddressPhone = addressPhone.Text;
                        Room.UserEmail = userEmail.Text;
                        Room.UserFirstName = userFirstName.Text;
                        Room.UserLastName = userLastName.Text;
                        Room.APIPort = apiPort.Text;
                        Room.URL = url.Text;
                        Room.EndPointID = EndPtID;
                        Room.ID = ConfGuestRID;
                        Room.RoomID = RoomID;
                        ////ZD 104278 start
                        if (PageType == AudioVideoConferenceAVSettingsPage.AVPageType.Audioonly)
                            Room.Connection = 1;
                        else
                        {
                            if (MediaTypeCombo.SelectedIndex == 0)
                                Room.Connection = 1;
                            else
                                Room.Connection = 2;
                        }
                        ////ZD 104278 End
                        if (chkBoxExternalRoom.Checked)
                        {
                            Room.IsGuestLoc = true;
                            Room.EndPtName = txtEdnpt.Text;
                            ConfGuestLisc--;
                        }
                        else
                            Room.IsGuestLoc = false;// 101343
                        foreach (var val in _addressTypes)
                        {
                            if (val.Value == (string)addressTypesCombo.EditValue)
                            {
                                Room.AddressType = new WrapPair<int, string>(val.Key, val.Value);
                                break;
                            }
                        }
                        foreach (var val in _connectionTypes)
                        {
                            if (val.Value == (string)connectionCombo.EditValue)
                            {
                                Room.ConnectionType = new WrapPair<int, string>(val.Key, val.Value);
                                break;
                            }
                        }
                        foreach (var val in _videoProtocols)
                        {
                            if (val.Value == (string)protocolCombo.EditValue)
                            {
                                Room.ProtocolType = new WrapPair<int, string>(val.Key, val.Value);
                                break;
                            }
                        }
                        foreach (var val in _lineRates)
                        {
                            if (val.Value == (string)lineRateCombo.EditValue)
                            {
                                Room.LineRate = new WrapPair<int, string>(val.Key, val.Value);
                                break;
                            }
                        }
                        if (PageType != AudioVideoConferenceAVSettingsPage.AVPageType.PointToPoint)
                        {
                            foreach (var val in _bridges)
                            {
                                if (val.Name == (string)mcuCombo.EditValue)
                                {
                                    Room.MCU = new WrapPair<BridgeId, string>(val.Id, val.Name);
                                    break;
                                }
                            }
                            if (_profiles != null)
                            {
                                if ((string)profileCombo.EditValue == MCUProfile.MCUProfileNone.Name)
                                {
                                    Room.MCUProfile = new WrapPair<MCUProfileId, string>(MCUProfile.MCUProfileNone.Id,
                                                                                         MCUProfile.MCUProfileNone.Name);
                                }
                                else
                                {
                                    if ((string)profileCombo.EditValue == MCUProfile.MCUProfileNoItems.Name)
                                    {
                                        Room.MCUProfile = new WrapPair<MCUProfileId, string>(MCUProfile.MCUProfileNoItems.Id,
                                                                                             MCUProfile.MCUProfileNoItems.Name);
                                    }
                                    else
                                    {
                                        foreach (var val in _profiles)
                                        {
                                            if (val.Name == (string)profileCombo.EditValue)
                                            {
                                                Room.MCUProfile = new WrapPair<MCUProfileId, string>(val.Id, val.Name);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        foreach (var val in _videoEquipment)
                        {
                            if (val.Name == (string)equipmentCombo.EditValue)
                            {
                                Room.Equipment = new WrapPair<int, string>(val.Id, val.Name);
                                break;
                            }
                        }

                        if (PageType == AudioVideoConferenceAVSettingsPage.AVPageType.PointToPoint)
                        {
                            Room.CallerCallee = new WrapPair<int, string>(
                                (string)callerCalleeCombo.EditValue == LocalizedDescriptionAttribute.FromEnum(ConferenceEndpointCallMode.Callee.GetType(), ConferenceEndpointCallMode.Callee)
                                    ? 0 : 1, (string)callerCalleeCombo.EditValue);
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
//ZD 101343 starts
        private void chkBoxExternalRoom_CheckedChanged(object sender, EventArgs e)
        {
            //Room = new ExternalRoom();
            

            if (chkBoxExternalRoom.Checked)
            {
                EndPtName.Visibility = LayoutVisibility.Always;
                
                //if (confLisNum < 0)
                //{
                //    UIHelper.ShowError("There are no available guest licenses to complete your request. Please contact your administrator or submit the external room as a participant");
                
                //}
            }
            else
                EndPtName.Visibility = LayoutVisibility.Never;

        }
//ZD 101343 Ends
        private void txtEdnpt_EditValueChanged(object sender, EventArgs e)
        {

        }
        

        #region IDXDataErrorInfo Members

        

        #endregion
    }
}
