﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    internal class FindUsersResponse : ServiceResponse
    {
        readonly FindUsersResults _results = new FindUsersResults();

        internal FindUsersResults Results
        {
            get
            {
                return _results;    
            }
        }

        internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
        {
            do
            {
                reader.Read();
                if (reader.IsStartElement(XmlNamespace.NotSpecified, "user"))
                {
                    var user = new ManagedUser();
                    user.LoadFromXml(reader, "user");
                    _results.InternalUsers.Add(user);
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "users"));
        }
    }
}
