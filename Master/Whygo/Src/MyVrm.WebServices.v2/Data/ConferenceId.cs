﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.WebServices.Data
{
    public class ConferenceId : ServiceId
    {
        public static readonly ConferenceId NewConferenceId = new ConferenceId(Constants.NewConferenceId);

        internal ConferenceId()
        {
        }
        public ConferenceId(string id) : base(id)
        {
        }

        public bool IsRecurringId
        {
            get { return IsValid && !Id.Contains(","); }
        }

        #region Overrides of ServiceId

        internal override string GetXmlElementName()
        {
            return "confID";
        }

        #endregion
    }
}
