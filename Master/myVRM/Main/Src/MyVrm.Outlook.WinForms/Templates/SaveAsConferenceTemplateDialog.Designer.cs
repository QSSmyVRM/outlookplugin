﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.Outlook.WinForms.Templates
{
	partial class SaveAsConferenceTemplateDialog
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
			this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
			this.editTemplateName = new DevExpress.XtraEditors.TextEdit();
			this.memoTemplateDescr = new DevExpress.XtraEditors.MemoEdit();
			this.checkIsPublic = new DevExpress.XtraEditors.CheckEdit();
			this.checkIsDefault = new DevExpress.XtraEditors.CheckEdit();
			this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
			((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).BeginInit();
			this.ContentPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.editTemplateName.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.memoTemplateDescr.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.checkIsPublic.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.checkIsDefault.Properties)).BeginInit();
			this.SuspendLayout();
			// 
			// ContentPanel
			// 
			this.ContentPanel.Controls.Add(this.labelControl3);
			this.ContentPanel.Controls.Add(this.checkIsDefault);
			this.ContentPanel.Controls.Add(this.checkIsPublic);
			this.ContentPanel.Controls.Add(this.memoTemplateDescr);
			this.ContentPanel.Controls.Add(this.editTemplateName);
			this.ContentPanel.Controls.Add(this.labelControl2);
			this.ContentPanel.Controls.Add(this.labelControl1);
			this.ContentPanel.Size = new System.Drawing.Size(665, 393);
			this.ContentPanel.TabIndex = 0;
			// 
			// labelControl1
			// 
			this.labelControl1.Location = new System.Drawing.Point(19, 19);
			this.labelControl1.Name = "labelControl1";
			this.labelControl1.Size = new System.Drawing.Size(42, 16);
			this.labelControl1.TabIndex = 5;
			this.labelControl1.Text = "Name: ";
			// 
			// labelControl2
			// 
			this.labelControl2.Location = new System.Drawing.Point(19, 165);
			this.labelControl2.Name = "labelControl2";
			this.labelControl2.Size = new System.Drawing.Size(68, 16);
			this.labelControl2.TabIndex = 6;
			this.labelControl2.Text = "Description:";
			// 
			// editTemplateName
			// 
			this.editTemplateName.Location = new System.Drawing.Point(93, 16);
			this.editTemplateName.Name = "editTemplateName";
			this.editTemplateName.Size = new System.Drawing.Size(555, 22);
			this.editTemplateName.TabIndex = 0;
			// 
			// memoTemplateDescr
			// 
			this.memoTemplateDescr.Location = new System.Drawing.Point(93, 44);
			this.memoTemplateDescr.Name = "memoTemplateDescr";
			this.memoTemplateDescr.Size = new System.Drawing.Size(555, 280);
			this.memoTemplateDescr.TabIndex = 1;
			// 
			// checkIsPublic
			// 
			this.checkIsPublic.Location = new System.Drawing.Point(17, 330);
			this.checkIsPublic.Name = "checkIsPublic";
			this.checkIsPublic.Properties.Caption = "Public";
			this.checkIsPublic.Size = new System.Drawing.Size(75, 21);
			this.checkIsPublic.TabIndex = 2;
			// 
			// checkIsDefault
			// 
			this.checkIsDefault.Location = new System.Drawing.Point(17, 357);
			this.checkIsDefault.Name = "checkIsDefault";
			this.checkIsDefault.Properties.Caption = "Set As Default";
			this.checkIsDefault.Size = new System.Drawing.Size(135, 21);
			this.checkIsDefault.TabIndex = 3;
			// 
			// labelControl3
			// 
			this.labelControl3.Location = new System.Drawing.Point(251, 336);
			this.labelControl3.Name = "labelControl3";
			this.labelControl3.Size = new System.Drawing.Size(364, 16);
			this.labelControl3.TabIndex = 4;
			this.labelControl3.Text = "Makes template available for use by anyone in the organization.";
			// 
			// SaveAsConferenceTemplateDialog
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
			this.ClientSize = new System.Drawing.Size(689, 449);
			this.Name = "SaveAsConferenceTemplateDialog";
			this.Text = "Edit Template";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SaveAsConferenceTemplateDialog_FormClosing);
			((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).EndInit();
			this.ContentPanel.ResumeLayout(false);
			this.ContentPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.editTemplateName.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.memoTemplateDescr.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.checkIsPublic.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.checkIsDefault.Properties)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private DevExpress.XtraEditors.TextEdit editTemplateName;
		private DevExpress.XtraEditors.LabelControl labelControl2;
		private DevExpress.XtraEditors.LabelControl labelControl1;
		private DevExpress.XtraEditors.MemoEdit memoTemplateDescr;
		private DevExpress.XtraEditors.CheckEdit checkIsPublic;
		private DevExpress.XtraEditors.CheckEdit checkIsDefault;
		private DevExpress.XtraEditors.LabelControl labelControl3;
	}
}
