﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Outlook;

namespace MyVrm.Outlook
{
    internal partial class OutlookRecipientImpl : OutlookRecipient
    {
        private readonly Recipient _recipient;
        private MAPIFolder _calendarFolder;
        private string _smtpAddress;

        internal OutlookRecipientImpl(Recipient recipient)
        {
            _recipient = recipient;
        }

        public override string Name
        {
            get { return _recipient.Name; }
        }




        protected override string GetFreeBusyInternal(DateTime start, int minutesPerChar)
        {
            return _recipient.Resolved ? _recipient.FreeBusy(start, minutesPerChar, true) : "";
        }

        public override void Dispose()
        {
            if (_calendarFolder != null)
            {
                Marshal.ReleaseComObject(_calendarFolder);
                _calendarFolder = null;
            }
            if (_recipient != null)
            {
                Marshal.ReleaseComObject(_recipient);
            }
        }
    }
}