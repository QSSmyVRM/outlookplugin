﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    public abstract class Tier : ServiceObject
    {
        protected Tier(MyVrmService service) : base(service)
        {
        }

        public TierId Id
        {
            get
            {
                return (TierId) PropertyBag[TierSchema.Id];
            }
            internal set
            {
                PropertyBag[TierSchema.Id] = value;
            }
        }

        public string Name
        {
            get
            {
                return (string) PropertyBag[TierSchema.Name];
            }
            set
            {
                PropertyBag[TierSchema.Name] = value;
            }
        }

        public void Save()
        {
            Service.SaveTier(this);
        }

        #region Overrides of ServiceObject

        internal override PropertyDefinition GetIdPropertyDefinition()
        {
            return TierSchema.Id;
        }
        #endregion
    }

    [ServiceObjectDefinition("Location")]
    public class TopTier : Tier
    {
        public TopTier(MyVrmService service) : base(service)
        {
        }

        #region Overrides of ServiceObject

        internal override ServiceObjectSchema GetSchema()
        {
            return TopTierSchema.Instance;
        }

        #endregion
    }
    
    [ServiceObjectDefinition("Location")]
    public class MiddleTier : Tier
    {
        public MiddleTier(MyVrmService service) : base(service)
        {
        }

        public TierId ParentId
        {
            get
            {
                return (TierId) PropertyBag[MiddleTierSchema.ParentId];
            }
            set
            {
                PropertyBag[MiddleTierSchema.ParentId] = value;   
            }
        }

        internal override ServiceObjectSchema GetSchema()
        {
            return MiddleTierSchema.Instance;
        }
    }
}
