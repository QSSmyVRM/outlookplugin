﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    internal class GetProviderWorkOrderDetailsRequest : ServiceRequestBase<GetProviderWorkOrderDetailsResponse>
    {
        internal GetProviderWorkOrderDetailsRequest(MyVrmService service) : base(service)
        {
        }

        internal ConferenceId ConferenceId { get; set; }
        internal WorkOrderId WorkOrderId { get; set; }

        #region Overrides of ServiceRequestBase<GetProviderWorkOrderDetailsResponse>

        internal override string GetXmlElementName()
        {
            return "GetProviderWorkorderDetails";
        }

        internal override string GetCommandName()
        {
            return "GetProviderWorkorderDetails";
        }

        internal override string GetResponseXmlElementName()
        {
            return "GetProviderWorkorderDetails";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            ConferenceId.WriteToXml(writer, "ConfID");
            WorkOrderId.WriteToXml(writer, "WorkorderID");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "Type", 2);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "utcEnabled", Service.IsUtcEnabled);
        }

        #endregion
    }
}
