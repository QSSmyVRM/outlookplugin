﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.Outlook.WinForms.Conference
{
    partial class HousekeepingWorkOrderDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.roomLayoutContainerControl = new DevExpress.XtraEditors.PopupContainerControl();
            this.roomLayoutList = new DevExpress.XtraTreeList.TreeList();
            this.roomLayoutImageColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.roomLayoutItemPictureEdit = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.roomLayoutNameColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.roomLayoutEdit = new DevExpress.XtraEditors.PopupContainerEdit();
            this.workOrderItemsList = new DevExpress.XtraTreeList.TreeList();
            this.itemNameColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.serialNumberColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.itemImageColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.itemImageRepositoryItemPictureEdit = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.commentsColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemMemoEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.requestQuantityColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.quantityRepositoryItemSpinEdit = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.roomSetComboBox = new DevExpress.XtraEditors.ComboBoxEdit();
            this.roomNameLabel = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).BeginInit();
            this.ContentPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.roomLayoutContainerControl)).BeginInit();
            this.roomLayoutContainerControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.roomLayoutList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.roomLayoutItemPictureEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.roomLayoutEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.workOrderItemsList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemImageRepositoryItemPictureEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.quantityRepositoryItemSpinEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.roomSetComboBox.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // ContentPanel
            // 
            this.ContentPanel.Controls.Add(this.layoutControl1);
            this.ContentPanel.Size = new System.Drawing.Size(600, 388);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.roomLayoutContainerControl);
            this.layoutControl1.Controls.Add(this.roomLayoutEdit);
            this.layoutControl1.Controls.Add(this.workOrderItemsList);
            this.layoutControl1.Controls.Add(this.roomSetComboBox);
            this.layoutControl1.Controls.Add(this.roomNameLabel);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(600, 388);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // roomLayoutContainerControl
            // 
            this.roomLayoutContainerControl.Controls.Add(this.roomLayoutList);
            this.roomLayoutContainerControl.Location = new System.Drawing.Point(323, 26);
            this.roomLayoutContainerControl.Name = "roomLayoutContainerControl";
            this.roomLayoutContainerControl.Size = new System.Drawing.Size(200, 100);
            this.roomLayoutContainerControl.TabIndex = 9;
            // 
            // roomLayoutList
            // 
            this.roomLayoutList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.roomLayoutImageColumn,
            this.roomLayoutNameColumn});
            this.roomLayoutList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.roomLayoutList.Location = new System.Drawing.Point(0, 0);
            this.roomLayoutList.Name = "roomLayoutList";
            this.roomLayoutList.OptionsView.ShowButtons = false;
            this.roomLayoutList.OptionsView.ShowColumns = false;
            this.roomLayoutList.OptionsView.ShowFocusedFrame = false;
            this.roomLayoutList.OptionsView.ShowHorzLines = false;
            this.roomLayoutList.OptionsView.ShowIndicator = false;
            this.roomLayoutList.OptionsView.ShowRoot = false;
            this.roomLayoutList.OptionsView.ShowVertLines = false;
            this.roomLayoutList.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.roomLayoutItemPictureEdit});
            this.roomLayoutList.Size = new System.Drawing.Size(200, 100);
            this.roomLayoutList.TabIndex = 0;
            this.roomLayoutList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.roomLayoutList_KeyDown);
            this.roomLayoutList.MouseClick += new System.Windows.Forms.MouseEventHandler(this.roomLayoutList_MouseClick);
            this.roomLayoutList.MouseMove += new System.Windows.Forms.MouseEventHandler(this.roomLayoutList_MouseMove);
            this.roomLayoutList.Click += new System.EventHandler(this.roomLayoutList_Click);
            // 
            // roomLayoutImageColumn
            // 
            this.roomLayoutImageColumn.Caption = "Image";
            this.roomLayoutImageColumn.ColumnEdit = this.roomLayoutItemPictureEdit;
            this.roomLayoutImageColumn.FieldName = "Image";
            this.roomLayoutImageColumn.Name = "roomLayoutImageColumn";
            this.roomLayoutImageColumn.OptionsColumn.AllowEdit = false;
            this.roomLayoutImageColumn.OptionsColumn.AllowFocus = false;
            this.roomLayoutImageColumn.OptionsColumn.AllowMove = false;
            this.roomLayoutImageColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.roomLayoutImageColumn.OptionsColumn.ReadOnly = true;
            this.roomLayoutImageColumn.OptionsColumn.ShowInCustomizationForm = false;
            this.roomLayoutImageColumn.Visible = true;
            this.roomLayoutImageColumn.VisibleIndex = 0;
            // 
            // roomLayoutItemPictureEdit
            // 
            this.roomLayoutItemPictureEdit.Name = "roomLayoutItemPictureEdit";
            this.roomLayoutItemPictureEdit.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            // 
            // roomLayoutNameColumn
            // 
            this.roomLayoutNameColumn.Caption = "Name";
            this.roomLayoutNameColumn.FieldName = "Name";
            this.roomLayoutNameColumn.Name = "roomLayoutNameColumn";
            this.roomLayoutNameColumn.OptionsColumn.AllowEdit = false;
            this.roomLayoutNameColumn.OptionsColumn.AllowFocus = false;
            this.roomLayoutNameColumn.OptionsColumn.AllowMove = false;
            this.roomLayoutNameColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.roomLayoutNameColumn.OptionsColumn.ReadOnly = true;
            this.roomLayoutNameColumn.OptionsColumn.ShowInCustomizationForm = false;
            this.roomLayoutNameColumn.Visible = true;
            this.roomLayoutNameColumn.VisibleIndex = 1;
            // 
            // roomLayoutEdit
            // 
            this.roomLayoutEdit.Location = new System.Drawing.Point(66, 26);
            this.roomLayoutEdit.Name = "roomLayoutEdit";
            this.roomLayoutEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.roomLayoutEdit.Properties.NullText = "Select layout...";
            this.roomLayoutEdit.Properties.PopupControl = this.roomLayoutContainerControl;
            this.roomLayoutEdit.Size = new System.Drawing.Size(211, 20);
            this.roomLayoutEdit.StyleController = this.layoutControl1;
            this.roomLayoutEdit.TabIndex = 8;
            this.roomLayoutEdit.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.roomLayoutEdit_QueryResultValue);
            this.roomLayoutEdit.QueryPopUp += new System.ComponentModel.CancelEventHandler(this.roomLayoutEdit_QueryPopUp);
            // 
            // workOrderItemsList
            // 
            this.workOrderItemsList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.itemNameColumn,
            this.serialNumberColumn,
            this.itemImageColumn,
            this.commentsColumn,
            this.requestQuantityColumn});
            this.workOrderItemsList.Location = new System.Drawing.Point(2, 50);
            this.workOrderItemsList.Name = "workOrderItemsList";
            this.workOrderItemsList.OptionsView.ShowButtons = false;
            this.workOrderItemsList.OptionsView.ShowIndicator = false;
            this.workOrderItemsList.OptionsView.ShowRoot = false;
            this.workOrderItemsList.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.quantityRepositoryItemSpinEdit,
            this.repositoryItemMemoEdit,
            this.itemImageRepositoryItemPictureEdit});
            this.workOrderItemsList.Size = new System.Drawing.Size(596, 336);
            this.workOrderItemsList.TabIndex = 7;
            // 
            // itemNameColumn
            // 
            this.itemNameColumn.Caption = "Item";
            this.itemNameColumn.FieldName = "Name";
            this.itemNameColumn.Name = "itemNameColumn";
            this.itemNameColumn.OptionsColumn.AllowEdit = false;
            this.itemNameColumn.OptionsColumn.AllowFocus = false;
            this.itemNameColumn.OptionsColumn.AllowMove = false;
            this.itemNameColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.itemNameColumn.OptionsColumn.ReadOnly = true;
            this.itemNameColumn.OptionsColumn.ShowInCustomizationForm = false;
            this.itemNameColumn.Visible = true;
            this.itemNameColumn.VisibleIndex = 0;
            // 
            // serialNumberColumn
            // 
            this.serialNumberColumn.Caption = "Serial #";
            this.serialNumberColumn.FieldName = "SerialNumber";
            this.serialNumberColumn.Name = "serialNumberColumn";
            this.serialNumberColumn.OptionsColumn.AllowEdit = false;
            this.serialNumberColumn.OptionsColumn.AllowFocus = false;
            this.serialNumberColumn.OptionsColumn.AllowMove = false;
            this.serialNumberColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.serialNumberColumn.OptionsColumn.ReadOnly = true;
            this.serialNumberColumn.OptionsColumn.ShowInCustomizationForm = false;
            this.serialNumberColumn.Visible = true;
            this.serialNumberColumn.VisibleIndex = 1;
            // 
            // itemImageColumn
            // 
            this.itemImageColumn.Caption = "Image";
            this.itemImageColumn.ColumnEdit = this.itemImageRepositoryItemPictureEdit;
            this.itemImageColumn.FieldName = "Image";
            this.itemImageColumn.Name = "itemImageColumn";
            this.itemImageColumn.OptionsColumn.AllowEdit = false;
            this.itemImageColumn.OptionsColumn.AllowFocus = false;
            this.itemImageColumn.OptionsColumn.AllowMove = false;
            this.itemImageColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.itemImageColumn.OptionsColumn.ReadOnly = true;
            this.itemImageColumn.OptionsColumn.ShowInCustomizationForm = false;
            this.itemImageColumn.Visible = true;
            this.itemImageColumn.VisibleIndex = 2;
            // 
            // itemImageRepositoryItemPictureEdit
            // 
            this.itemImageRepositoryItemPictureEdit.Name = "itemImageRepositoryItemPictureEdit";
            this.itemImageRepositoryItemPictureEdit.ReadOnly = true;
            this.itemImageRepositoryItemPictureEdit.ShowMenu = false;
            this.itemImageRepositoryItemPictureEdit.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            // 
            // commentsColumn
            // 
            this.commentsColumn.Caption = "Comments";
            this.commentsColumn.ColumnEdit = this.repositoryItemMemoEdit;
            this.commentsColumn.FieldName = "Comments";
            this.commentsColumn.Name = "commentsColumn";
            this.commentsColumn.OptionsColumn.AllowEdit = false;
            this.commentsColumn.OptionsColumn.AllowFocus = false;
            this.commentsColumn.OptionsColumn.AllowMove = false;
            this.commentsColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.commentsColumn.OptionsColumn.ReadOnly = true;
            this.commentsColumn.OptionsColumn.ShowInCustomizationForm = false;
            this.commentsColumn.Visible = true;
            this.commentsColumn.VisibleIndex = 3;
            // 
            // repositoryItemMemoEdit
            // 
            this.repositoryItemMemoEdit.LinesCount = 3;
            this.repositoryItemMemoEdit.Name = "repositoryItemMemoEdit";
            this.repositoryItemMemoEdit.ReadOnly = true;
            // 
            // requestQuantityColumn
            // 
            this.requestQuantityColumn.Caption = "Requested Quantity";
            this.requestQuantityColumn.ColumnEdit = this.quantityRepositoryItemSpinEdit;
            this.requestQuantityColumn.FieldName = "RequestedQuantity";
            this.requestQuantityColumn.Name = "requestQuantityColumn";
            this.requestQuantityColumn.OptionsColumn.AllowMove = false;
            this.requestQuantityColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.requestQuantityColumn.OptionsColumn.ShowInCustomizationForm = false;
            this.requestQuantityColumn.Visible = true;
            this.requestQuantityColumn.VisibleIndex = 4;
            // 
            // quantityRepositoryItemSpinEdit
            // 
            this.quantityRepositoryItemSpinEdit.AutoHeight = false;
            this.quantityRepositoryItemSpinEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.quantityRepositoryItemSpinEdit.IsFloatValue = false;
            this.quantityRepositoryItemSpinEdit.Mask.EditMask = "N00";
            this.quantityRepositoryItemSpinEdit.Name = "quantityRepositoryItemSpinEdit";
            this.quantityRepositoryItemSpinEdit.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.quantityRepositoryItemSpinEdit_EditValueChanging);
            // 
            // roomSetComboBox
            // 
            this.roomSetComboBox.Location = new System.Drawing.Point(345, 2);
            this.roomSetComboBox.Name = "roomSetComboBox";
            this.roomSetComboBox.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.roomSetComboBox.Properties.NullText = "Select one...";
            this.roomSetComboBox.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.roomSetComboBox.Size = new System.Drawing.Size(185, 20);
            this.roomSetComboBox.StyleController = this.layoutControl1;
            this.roomSetComboBox.TabIndex = 6;
            this.roomSetComboBox.SelectedIndexChanged += new System.EventHandler(this.roomSetComboBox_SelectedIndexChanged);
            // 
            // roomNameLabel
            // 
            this.roomNameLabel.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.roomNameLabel.Appearance.Options.UseFont = true;
            this.roomNameLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.roomNameLabel.Location = new System.Drawing.Point(66, 2);
            this.roomNameLabel.Name = "roomNameLabel";
            this.roomNameLabel.Size = new System.Drawing.Size(211, 20);
            this.roomNameLabel.StyleController = this.layoutControl1;
            this.roomNameLabel.TabIndex = 4;
            this.roomNameLabel.Text = "[Room Name]";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.emptySpaceItem1,
            this.layoutControlItem2,
            this.emptySpaceItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(600, 388);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.Control = this.roomNameLabel;
            this.layoutControlItem1.CustomizationFormText = "Room";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(279, 24);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(279, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(279, 24);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "Room";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(60, 16);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.roomSetComboBox;
            this.layoutControlItem3.CustomizationFormText = "Set Name";
            this.layoutControlItem3.Location = new System.Drawing.Point(279, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(253, 24);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(253, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(253, 24);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "Set Name";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(60, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.workOrderItemsList;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(600, 340);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(532, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(68, 24);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.roomLayoutEdit;
            this.layoutControlItem2.CustomizationFormText = "Room layout";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(279, 24);
            this.layoutControlItem2.Text = "Room layout";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(60, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(279, 24);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(321, 24);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // HousekeepingWorkOrderDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(624, 444);
            this.MinimumSize = new System.Drawing.Size(640, 480);
            this.Name = "HousekeepingWorkOrderDialog";
            this.Text = "Work Order";
            this.Load += new System.EventHandler(this.HousekeepingWorkOrderDialog_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.HousekeepingWorkOrderDialog_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).EndInit();
            this.ContentPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.roomLayoutContainerControl)).EndInit();
            this.roomLayoutContainerControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.roomLayoutList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.roomLayoutItemPictureEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.roomLayoutEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.workOrderItemsList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemImageRepositoryItemPictureEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.quantityRepositoryItemSpinEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.roomSetComboBox.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraTreeList.TreeList workOrderItemsList;
        private DevExpress.XtraEditors.ComboBoxEdit roomSetComboBox;
        private DevExpress.XtraEditors.LabelControl roomNameLabel;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraTreeList.Columns.TreeListColumn itemNameColumn;
        private DevExpress.XtraTreeList.Columns.TreeListColumn serialNumberColumn;
        private DevExpress.XtraTreeList.Columns.TreeListColumn commentsColumn;
        private DevExpress.XtraTreeList.Columns.TreeListColumn itemImageColumn;
        private DevExpress.XtraTreeList.Columns.TreeListColumn requestQuantityColumn;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit quantityRepositoryItemSpinEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit itemImageRepositoryItemPictureEdit;
        private DevExpress.XtraEditors.PopupContainerEdit roomLayoutEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.PopupContainerControl roomLayoutContainerControl;
        private DevExpress.XtraTreeList.TreeList roomLayoutList;
        private DevExpress.XtraTreeList.Columns.TreeListColumn roomLayoutImageColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit roomLayoutItemPictureEdit;
        private DevExpress.XtraTreeList.Columns.TreeListColumn roomLayoutNameColumn;
    }
}
