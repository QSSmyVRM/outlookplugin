﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace MyVrm.WebServices.Data
{
    class FetchUpdatedRoom : ServiceRequestBase<GetRecentlyUpdatedRoomResponse>
    {
        internal DateTime cacheLastmodifiedDatetime { get; set; }
        internal TimeZoneInfo TimeZone { get; set; }

        internal FetchUpdatedRoom(MyVrmService service): base(service)
        {
        }

        internal RoomId RoomId { get; set; }

        #region Overrides of ServiceRequestBase

        internal override string GetXmlElementName()
        {
            return "GetRecentlyUpdatedRooms";
        }

        internal override string GetCommandName()
        {
            return "GetRecentlyUpdatedRoom";
        }

        internal override string GetResponseXmlElementName()
        {
            return "GetRecentlyUpdatedRooms";
        }

        internal override GetRecentlyUpdatedRoomResponse ParseResponse(MyVrmServiceXmlReader reader)
        {
            var response = new GetRecentlyUpdatedRoomResponse { Room = new Room(Service) };
            response.LoadFromXml(reader, GetResponseXmlElementName());
            return response;
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            UserId.WriteToXml(writer, "UserID");
            OrganizationId.WriteToXml(writer);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "cacheLastmodifiedDate", Utilities.DateToString(cacheLastmodifiedDatetime.Date));
            writer.WriteElementValue(XmlNamespace.NotSpecified, "cacheLastmodifiedHour", Utilities.HourToString(cacheLastmodifiedDatetime.Hour));
            writer.WriteElementValue(XmlNamespace.NotSpecified, "cacheLastmodifiedMin", cacheLastmodifiedDatetime.Minute);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "cacheLastmodifiedSet", Utilities.GetNoonAbbr(cacheLastmodifiedDatetime.TimeOfDay));
            writer.WriteElementValue(XmlNamespace.NotSpecified, "timeZone", TimeZoneConvertion.ConvertToTimeZoneId(TimeZoneInfo.Local));
        }

        #endregion
    }


    //public class GetRecentlyUpdatedRoomResponse : ServiceResponse
    //{
    //    public string body { get; internal set; }
    //    public string placeholders { get; internal set; }
    //    public string ConferenceUrl { get; internal set; }
    //    public int contentid { get; internal set; }
    //    public string subject { get; internal set; }
    //    public string emailcontent { get; internal set; }
    //    public int emailtype { get; internal set; }

    //}


    public class GetRecentlyUpdatedRoomResponse : ServiceResponse
    {

        internal Room Room { get; set; }
        private readonly MyVrmService _services;
        private MyVrmService Services
        {
            get { return _services; }
        }

        readonly List<Room> _rooms = new List<Room>();
        internal ReadOnlyCollection<Room> Rooms
        {
            get
            {
                return new ReadOnlyCollection<Room>(_rooms);
            }
        }

        internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
        {
            do
            {
                reader.Read();
                if (reader.LocalName == "GetRecentlyUpdatedRoom")
                {
                    Room = new Room(Services);
                    Room.LoadFromXml(reader, true, "GetRecentlyUpdatedRoom");
                   _rooms.Add(Room);
                   
                }
                else
                {
                    reader.SkipCurrentElement();
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "GetRecentlyUpdatedRooms"));
            
        }

    }

}
