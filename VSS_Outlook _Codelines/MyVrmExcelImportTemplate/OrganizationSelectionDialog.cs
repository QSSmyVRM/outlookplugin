﻿using System;
using System.Windows.Forms;
using MyVrm.WebServices.Data;

namespace MyVrmExcelImportTemplate
{
    public partial class OrganizationSelectionDialog : Form
    {
        public OrganizationSelectionDialog()
        {
            InitializeComponent();
        }

        private void OrganizationSelectionDialog_Load(object sender, EventArgs e)
        {
            organizationsComboBox.DataSource = Globals.ThisWorkbook.Organizations;
            organizationsComboBox.ValueMember = "Id";
            organizationsComboBox.DisplayMember = "Name";
        }

        public OrganizationId SelectedOrganization
        {
            get
            {
                return ((Organization)organizationsComboBox.SelectedItem).Id;
            }
        }
    }
}
