﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace MyVrm.WebServices.Data
{
    internal class GetConferenceAdvancedAudioVideoSettingsRequest : ServiceRequestBase<GetConferenceAdvancedAudioVideoSettingsResponse>
    {
        public GetConferenceAdvancedAudioVideoSettingsRequest(MyVrmService service) : base(service)
        {
        }

        internal ConferenceId ConferenceId { get; set; }

        #region Overrides of ServiceRequestBase<GetConferenceAdvancedAudioVideoSettingsResponse>

        internal override string GetXmlElementName()
        {
            return "GetAdvancedAVSettings";
        }

        internal override string GetCommandName()
        {
            return "GetAdvancedAVSettings";
        }

        internal override string GetResponseXmlElementName()
        {
            return "GetAdvancedAVSettings";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            UserId.WriteToXml(writer, "UserID");
            ConferenceId.WriteToXml(writer, "ConfID");
        }

        #endregion
    }
}
