﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace MyVrm.Outlook
{
    public enum FreeBusyStatus
    {
        Free,
        Busy,
        Tentative,
        OutOfOffice
    }

    public class FreeBusyInfo
    {
        private readonly FreeBusyStatus _status;
        private readonly DateTime _start;
        private readonly DateTime _end;
        private readonly string _description;
        
        public FreeBusyInfo(FreeBusyStatus status, DateTime start, DateTime end)
            : this(status, start, end, "")
        {
        }

        public FreeBusyInfo(FreeBusyStatus status, DateTime start, DateTime end, string description)
        {
            _status = status;
            _start = start;
            _end = end;
            _description = description;
        }

        public FreeBusyStatus Status
        {
            get { return _status; }
        }

        public DateTime Start
        {
            get { return _start; }
        }

        public DateTime End
        {
            get { return _end; }
        }

        public string Description
        {
            get { return _description; }
        }
    }

    public abstract class OutlookRecipient : IDisposable
    {
        private bool _isDetailedFreeBusyAvailable = true;
        private bool _isGenericFreeBusyAvailable = true;
        private readonly FreeBusyInfoList _freeBusyInfos = new FreeBusyInfoList();

        public abstract string Name { get;}
        public abstract string SmtpAddress { get;}
        public ReadOnlyCollection<FreeBusyInfo> GetFreeBusy(DateTime start, DateTime end)
        {
            if (start > end)
            {
                throw new ArgumentOutOfRangeException("end");
            }
            var startDate = new DateTime(start.Year, start.Month, 1); // begining of month
            var endDate = new DateTime(end.Year, end.Month, DateTime.DaysInMonth(end.Year, end.Month)); // end of month
            if (_freeBusyInfos.Count > 0)
            {
                var cachedStartDate = _freeBusyInfos.GetFirstDate();
                cachedStartDate = new DateTime(cachedStartDate.Year, cachedStartDate.Month, 1);
                var cachedEndDate = _freeBusyInfos.GetLastDate();
                cachedEndDate = new DateTime(cachedEndDate.Year, cachedEndDate.Month, DateTime.DaysInMonth(cachedEndDate.Year, cachedEndDate.Month)); // end of month
                if (startDate >= cachedStartDate && end <= cachedEndDate)
                {
                    return new ReadOnlyCollection<FreeBusyInfo>(_freeBusyInfos.GetFreeBusyInfos(start, end));
                }
                if (startDate > cachedStartDate)
                {
                    startDate = cachedStartDate;
                }
                else if (endDate < cachedEndDate)
                {
                    endDate = cachedEndDate;
                }
            }
            var infos2 = GetFreeBusyInfos(startDate, endDate);
            _freeBusyInfos.AddRange(infos2);
            return new ReadOnlyCollection<FreeBusyInfo>(infos2);
        }

        private IList<FreeBusyInfo> GetFreeBusyInfos(DateTime start, DateTime end)
        {
            List<FreeBusyInfo> freeBusyInfos;
            if (_isDetailedFreeBusyAvailable)
            {
                // Read recipient's default calendar folder 
                try
                {
                    MyVrmAddin.TraceSource.TraceWarning("Retrieving appointments from recipient's default calendar for {0}", Name);
                    freeBusyInfos = GetFreeBusyFromDefaultCalendarFolder(start, end);
                    return freeBusyInfos;
                }
                catch (COMException exception)
                {
                    // COMException might occurs if an user doesn't have enought permissions to recipient's default calendar folder or
                    // recipient's calendar folder is not shared.
                    // Ignore COMException and go on.
                    _isDetailedFreeBusyAvailable = false;
                    MyVrmAddin.TraceSource.TraceWarning("Failed to retrieve appointments from recipient's default calendar: {0}", exception.Message);
                }
            }            // Read generic free/busy information
            if (_isGenericFreeBusyAvailable)
            {
                string freeBusyAsString;
                freeBusyInfos = new List<FreeBusyInfo>();
                const int minutesPerChar = 15;
                try
                {
                    MyVrmAddin.TraceSource.TraceWarning("Retrieving recipient's general free/busy information for {0}", Name);
                    freeBusyAsString = GetFreeBusyInternal(start, minutesPerChar);
                }
                catch (COMException exception)
                {
                    // COMException might occurs if there is no free/busy information available for this recipient.
                    // Simply return an empty collection.
                    _isGenericFreeBusyAvailable = false;
                    MyVrmAddin.TraceSource.TraceWarning("Failed to retrieve recipient's general free/busy information: {0}", exception.Message);
                    return freeBusyInfos;
                }

                var startDate = start.Date;
                DateTime blockStart = startDate;
                FreeBusyStatus blockStatus = FreeBusyStatus.Free;
                int blockLength = 0;
                foreach (var ch in freeBusyAsString)
                {
                    FreeBusyStatus nextBlockStatus;
                    switch (ch)
                    {
                        case '0': //free
                        {
                            nextBlockStatus = FreeBusyStatus.Free;
                            break;
                        }
                        case '1': //tentative
                        {
                            nextBlockStatus = FreeBusyStatus.Tentative;
                            break;
                        }
                        case '2': //busy
                        {
                            nextBlockStatus = FreeBusyStatus.Busy;
                            break;
                        }
                        case '3': // out of office
                        {
                            nextBlockStatus = FreeBusyStatus.OutOfOffice;
                            break;
                        }
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                    if (nextBlockStatus != blockStatus)
                    {
                        if (blockStatus != FreeBusyStatus.Free)
                        {
                            var endTime = blockStart + TimeSpan.FromMinutes(minutesPerChar * blockLength);
                            freeBusyInfos.Add(new FreeBusyInfo(blockStatus, blockStart, endTime));
                        }
                        blockStart += TimeSpan.FromMinutes(minutesPerChar * blockLength);
                        blockLength = 0;
                    }
                    blockLength++;
                    blockStatus = nextBlockStatus;
                }
                return freeBusyInfos;
            }
            return new List<FreeBusyInfo>();
        }

        protected abstract List<FreeBusyInfo> GetFreeBusyFromDefaultCalendarFolder(DateTime start, DateTime end);
        protected abstract string GetFreeBusyInternal(DateTime start, int minutesPerChar);

        #region Implementation of IDisposable

        public abstract void Dispose();

        #endregion
    }
}
