﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Xml;

namespace MyVrm.WebServices.Data
{
    /// <summary>
    /// Represents recurrence range with start and end dates.
    /// </summary>
    public class RecurrenceRange : ComplexProperty //102720
    {
        private DateTime _startDate;
        public  string endType;

        public override object Clone()
        {
            RecurrenceRange Range = new RecurrenceRange();

            Range.endType = endType;

            return Range;
        }

        internal RecurrenceRange()
        {
            StartDate = DateTime.MinValue.Date;
        }

        internal RecurrenceRange(DateTime startDate)
        {
            StartDate = startDate;
        }
        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        internal DateTime StartDate
        {
            get { return _startDate; }
            set
            {
                SetFieldValue(ref _startDate, value);
            }
        }
      

        internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
        {
            if (reader.LocalName == "startDate")
            {
                _startDate = Utilities.StringToDate(reader.ReadElementValue());
                return true;
            }

             if (reader.LocalName == "endType")
            {
                endType = reader.ReadElementValue();
                return true;
            }
            return false;
        }
       

        internal static RecurrenceRange FromXml(MyVrmServiceXmlReader reader, XmlNamespace xmlNamespace, string xmlElementName)
        {
            reader.EnsureCurrentNodeIsStartElement(xmlNamespace, xmlElementName);
            var doc = new XmlDocument();
            using (var subReader = reader.ReadSubtree())
            {
                doc.Load(subReader);
                var rangeNode = doc.DocumentElement;
                RecurrenceRange recurrenceRange;
                var startDateNode = rangeNode.SelectSingleNode("child::startDate");
                var startDate = Utilities.StringToDate(startDateNode.InnerText);
                var endType = rangeNode.SelectSingleNode("child::endType").InnerText;
                switch (endType)
                {
                    case "1":
                    {
                        recurrenceRange = new NoEndDateRecurrenceRange(startDate);
                        break;
                    }
                    case "2":
                    {
                        var occurrenceNode = rangeNode.SelectSingleNode("child::occurrence");
                        var occurrences = Utilities.Parse<int>(occurrenceNode.InnerText);
                        recurrenceRange = new NumberedRecurrenceRange(startDate, occurrences);
                        break;
                    }
                    case "3":
                    {
                        var endDate = Utilities.StringToDate(rangeNode.SelectSingleNode("child::endDate").InnerText);
                        recurrenceRange = new EndDateRecurrenceRange(startDate, endDate);
                        break;
                    }
                    default:
                    {
                        throw new NotSupportedException(string.Format(Strings.InvalidRecurrenceRangeType, endType));
                    }
                }
                return recurrenceRange;
            }
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            writer.WriteElementValue(Namespace, "startDate", Utilities.DateToString(StartDate));
        }
        /// <summary>
        /// Setup the recurrence.
        /// </summary>
        /// <param name="recurrencePattern">The recurrence pattern.</param>
        internal virtual void SetupRecurrence(RecurrencePattern recurrencePattern)
        {
            recurrencePattern.StartDate = StartDate;
        }
    }
    /// <summary>
    /// Represents recurrent range with an end date.
    /// </summary>
    internal sealed class EndDateRecurrenceRange : RecurrenceRange
    {
        private DateTime _endDate;

		public override object Clone()
		{
			EndDateRecurrenceRange copy = new EndDateRecurrenceRange();
			copy._endDate = new DateTime(_endDate.Ticks);
			
			return copy;
		}

        public EndDateRecurrenceRange()
        {
        }

        public EndDateRecurrenceRange(DateTime startDate, DateTime endDate)
            : base(startDate)
        {
            _endDate = endDate;
        }
        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        public DateTime EndDate
        {
            get { return _endDate; }
            set
            {
                SetFieldValue(ref _endDate, value);
            }

        }

        internal override void SetupRecurrence(RecurrencePattern recurrencePattern)
        {
            base.SetupRecurrence(recurrencePattern);
            recurrencePattern.EndDate = EndDate;
        }

        internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
        {
            if (base.TryReadElementFromXml(reader))
            {
                return true;
            }
            if (reader.LocalName == "endDate")
            {
                _endDate = Utilities.StringToDate(reader.ReadElementValue());
                return true;
            }
            return false;
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            base.WriteElementsToXml(writer);
            writer.WriteElementValue(Namespace, "endType", "3");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "endDate", Utilities.DateToString(EndDate));
        }
    }
    /// <summary>
    /// Represents recurrence range with no end date.
    /// </summary>
    internal sealed class NoEndDateRecurrenceRange : RecurrenceRange
    {
		public override object Clone()
		{
			NoEndDateRecurrenceRange copy = new NoEndDateRecurrenceRange();
			copy.StartDate = new DateTime(StartDate.Ticks);

			return copy;
		}

        public NoEndDateRecurrenceRange()
        {
        }

        public NoEndDateRecurrenceRange(DateTime startDate)
            : base(startDate)
        {
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            base.WriteElementsToXml(writer);
            writer.WriteElementValue(Namespace, "endType", "1");
			writer.WriteElementValue(Namespace, "occurrence", "-1");
        }
    }
    /// <summary>
    /// Represents recurrence range with number of occurrences.
    /// </summary>
    internal sealed class NumberedRecurrenceRange : RecurrenceRange
    {
        private int? _numberOfOccurrences;

		public override object Clone()
		{
			NumberedRecurrenceRange copy = new NumberedRecurrenceRange();
			copy.StartDate = new DateTime(StartDate.Ticks);
			copy._numberOfOccurrences = _numberOfOccurrences;

			return copy;
		}

        public NumberedRecurrenceRange()
        {
        }

        public NumberedRecurrenceRange(DateTime startDate, int? numberOfOccurrences) : base(startDate)
        {
            _numberOfOccurrences = numberOfOccurrences;
        }
        /// <summary>
        /// Gets or sets the number of occurrences.
        /// </summary>
        public int? NumberOfOccurrences
        {
            get { return _numberOfOccurrences; }
            set
            {
                SetFieldValue(ref _numberOfOccurrences, value);
            }
        }

        internal override void SetupRecurrence(RecurrencePattern recurrencePattern)
        {
            base.SetupRecurrence(recurrencePattern);
            recurrencePattern.Occurrences = NumberOfOccurrences;
        }

        internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
        {
            if (base.TryReadElementFromXml(reader))
            {
                return true;
            }
            if (reader.LocalName == "occurrence")
            {
                _numberOfOccurrences = new int?(reader.ReadElementValue<int>());
                return true;
            }
            return false;
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            base.WriteElementsToXml(writer);
            writer.WriteElementValue(Namespace, "endType", "2");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "occurrence", NumberOfOccurrences);
        }
    }
}
