﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System.Collections.Generic;
using System.Linq;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook.WinForms.Conference
{
    internal class RoomEndpoint
    {
        private readonly Room _room;
        private readonly ConferenceEndpoint _conferenceEndpoint;
        private readonly Endpoint _endpoint;
    	private string _bridgeName;
    	private string _bridgeProfileName;
		private MCUProfileId _bridgeProfileId;

        public RoomEndpoint(Room room, ConferenceEndpoint conferenceEndpoint, Endpoint endpoint)
        {
            _room = room;
            _conferenceEndpoint = conferenceEndpoint;
            _endpoint = endpoint;
            if (_conferenceEndpoint != null && Endpoint != null)
            {
                // Set initial values
                var defaultProfile = Endpoint.Profiles.DefaultProfile;
				if (_conferenceEndpoint.ProfileId == null )
					_conferenceEndpoint.ProfileId = defaultProfile.Id;
				if( _conferenceEndpoint.BridgeId == null )
					_conferenceEndpoint.BridgeId = defaultProfile.BridgeId;
            }
			if (_conferenceEndpoint != null)
			{
				BridgeProfileID = conferenceEndpoint.BridgeProfileID ;
				if(BridgeProfileID == null) //i.e. for a new conference
				{
					BridgeProfileID = Endpoint != null ? Endpoint.Profiles.DefaultProfile.BridgeProfileID : null;
				}
				var profiles = _conferenceEndpoint.BridgeId != null ? MyVrmService.Service.GetMCUProfiles(_conferenceEndpoint.BridgeId.Id) : null;

				if (profiles != null)
				{
					var profile = profiles.FirstOrDefault(a => a.Id == BridgeProfileID);
					if (profile != null)
					{
						BridgeProfileName = profile.Name;
					}
					else
					{
						if (BridgeProfileID == MCUProfile.MCUProfileNoItems.Id)
							BridgeProfileName = MCUProfile.MCUProfileNoItems.Name;
						else
						{
							if (BridgeProfileID == MCUProfile.MCUProfileNone.Id)
								BridgeProfileName = MCUProfile.MCUProfileNone.Name;
						}
					}
				}
			}
        }

        public string RoomName
        {
            get { return _room.Name; }
        }

        public string Name
        {
            get
            {
                return Endpoint != null ? Endpoint.Name : null;
            }
        }

        public RoomId Id
        {
            get { return _room.Id; }
        }

        public EndpointId EndpointId
        {
            get
            {
                return _conferenceEndpoint != null ? _conferenceEndpoint.EndpointId : null;
            }
        }

        public EndpointProfileId ProfileId
        {
            get
            {
                return _conferenceEndpoint != null ? _conferenceEndpoint.ProfileId : null;
            }
            set
            {
                if (_conferenceEndpoint != null)
                {
                    _conferenceEndpoint.ProfileId = value;
                }
            }
        }

        public BridgeId BridgeId
        {
            get
            {
                return _conferenceEndpoint != null ? _conferenceEndpoint.BridgeId : null;
            }
        }

        public bool UseDefault
        {
            get
            {
                return _conferenceEndpoint != null ? _conferenceEndpoint.UseDefault : false;
            }
            set
            {
                if (_conferenceEndpoint != null)
                {
                    _conferenceEndpoint.UseDefault = value;
                }
            }
        }

		public string BridgeName
    	{
    		set
    		{
				_bridgeName = value;
    		} 
			get
			{
				return _bridgeName;
			}
		}

		public string BridgeProfileName
    	{
    		set
    		{
				_bridgeProfileName = value;
    		} 
			get
			{
				if (string.IsNullOrEmpty(_bridgeProfileName))
				{
					var found = _endpoint != null ? _endpoint.Profiles.FirstOrDefault(a => a.Id == _conferenceEndpoint.ProfileId) : null;
					_bridgeProfileName = found != null ? found.BridgeProfileName : MCUProfile.MCUProfileNoItems.Name;
					_bridgeProfileId = found != null ? found.BridgeProfileID : MCUProfile.MCUProfileNoItems.Id;
					//_bridgeProfileName = _endpoint.Profiles.Count > 0 ? _endpoint.Profiles[0].BridgeProfileName : string.Empty;
				}
				return _bridgeProfileName;
			}
		}

    	public MCUProfileId BridgeProfileID
    	{
    		set
    		{
    			_bridgeProfileId = value;
				if (_conferenceEndpoint != null)
					_conferenceEndpoint.BridgeProfileID = _bridgeProfileId;
    		}
			get { return _bridgeProfileId; }
    	}
		

        public ConferenceEndpointCallMode Caller
        {
            get
            {
                return _conferenceEndpoint != null ? _conferenceEndpoint.Caller : ConferenceEndpointCallMode.Callee;
            }
            set
            {
                if (_conferenceEndpoint != null) _conferenceEndpoint.Caller = value;
            }
        }

        internal ConferenceEndpoint ConferenceEndpoint
        {
            get
            {
                return _conferenceEndpoint;
            }
        }

        internal Endpoint Endpoint
        {
            get { return _endpoint; }
        }
    }
}