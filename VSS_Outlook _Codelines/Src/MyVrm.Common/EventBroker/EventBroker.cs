﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.Common.EventBroker
{
	public class EventBroker
	{
		Dictionary<string, List<Delegate>> _subscriptions = new Dictionary<string, List<Delegate>>();

		public void Register(string eventId, Delegate method)
		{
			//associate an event handler for an eventId
			List<Delegate> delegates = null;
			if (!_subscriptions.TryGetValue(eventId, out delegates))
			{
				delegates = new List<Delegate>();
				_subscriptions[eventId] = delegates;
			}
			delegates.Add(method);
		}

		public void Unregister(string eventId, Delegate method)
		{
			//unassociate a specific event handler method for the eventId
			List<Delegate> delegates = null;
			if (_subscriptions.TryGetValue(eventId, out delegates))
			{
				delegates.Remove(method);
				if (delegates.Count == 0)
				{
					_subscriptions.Remove(eventId);
				}
			}
		}

		public void OnEvent(string eventId, params object[] args)
		{
			//call all event handlers for the given eventId
			List<Delegate> delegates = null;
			if (_subscriptions.TryGetValue(eventId, out delegates))
			{
				foreach (Delegate del in delegates)
				{
					if (del.Method != null)
					{
						if (del.Target != null)
						{
							del.DynamicInvoke(args);
						}
					}
				}
			}
		}
	}
}
