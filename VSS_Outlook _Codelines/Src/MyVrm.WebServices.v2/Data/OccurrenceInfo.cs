﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace MyVrm.WebServices.Data
{
    /// <summary>
    /// Encapsulates information on the occurrence of a recurring conference.
    /// </summary>
    public class OccurrenceInfo : ComplexProperty
    {
        private const string StartDateElementName = "startDate";
        private const string StartHourElementName = "startHour";
        private const string StartMinuteElementName = "startMin";
        private const string StartSetElementName = "startSet";
        private const string DurationElementName = "durationMin";
        private const string SetupDurationElementName = "setupDuration";
        private const string TeardownDurationElementName = "teardownDuration";
        private int _startHour;
        private int _startMinute;
        private string _startSet = Constants.BeforeNoonAbbr;

        internal OccurrenceInfo() : this(DateTime.MinValue, DateTime.MinValue)
        {
        }

        /*internal*/public OccurrenceInfo(DateTime startTime, DateTime endTime)
        {
            Start = startTime;
            End = endTime;
            SetupDuration = TimeSpan.Zero;
            TeardownDuration = TimeSpan.Zero;
        }

        private DateTime? _start;
        /// <summary>
        /// Gets or sets the start date and time of the occurrence.
        /// </summary>
        public DateTime Start
        {
            get
            {
                if (_start == null)
                {
                    _start = Utilities.BuildDateTime(OccurrenceDate,_startHour, _startMinute, _startSet);
                }
                return _start.Value;
            }
            set { _start = value; }
        }

        private DateTime? _end;
        /// <summary>
        /// Gets or sets the end date and time of the occurrence.
        /// </summary>
        public DateTime End
        {
            get
            {
                if (_end == null)
                {
                    _end = Start + Duration;
                }
                return _end.Value;
            }
            set
            {
                if (Start > value)
                {
                    throw new Exception(Strings.ConferenceEndDateOccursBeforeStartDate);
                }
                _end = value;
            }
        }
        /// <summary>
        /// Gets or sets setup duration of the occurrence.
        /// </summary>
        public TimeSpan SetupDuration { get; set; }
        /// <summary>
        /// Gets or sets teardown duration of the occurrence.
        /// </summary>
        public TimeSpan TeardownDuration { get; set; }
        private TimeSpan Duration { get; set; }

        private DateTime OccurrenceDate { get; set; }

        internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
        {
            switch (reader.LocalName)
            {
                case StartDateElementName:
                {
                    OccurrenceDate = Utilities.StringToDate(reader.ReadElementValue());
                    break;
                }
                case StartHourElementName:
                {
                    _startHour = reader.ReadElementValue<int>();
                    return true;
                }
                case StartMinuteElementName:
                {
                    _startMinute = reader.ReadElementValue<int>();
                    return true;
                }
                case StartSetElementName:
                {
                    _startSet = reader.ReadElementValue();
                    return true;
                }
                case DurationElementName:
                {
                    Duration = TimeSpan.FromMinutes(reader.ReadElementValue<int>());
                    return true;
                }
                case SetupDurationElementName:
                {
                    SetupDuration = TimeSpan.FromMinutes(reader.ReadElementValue<int>());
                    return true;
                }
                case TeardownDurationElementName:
                {
                    TeardownDuration = TimeSpan.FromMinutes(reader.ReadElementValue<int>());
                    return true;
                }
            }
            return false;
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            writer.WriteElementValue(Namespace, StartDateElementName, Utilities.DateToString(Start));
            writer.WriteElementValue(Namespace, StartHourElementName, (Start.Hour > 12 ? Start.Hour - 12 : Start.Hour).ToString("D2"));
            writer.WriteElementValue(Namespace, StartMinuteElementName, Start.Minute.ToString("D2"));
            writer.WriteElementValue(Namespace, StartSetElementName, Start.ToString("tt", Utilities.CultureProvider));
            writer.WriteElementValue(Namespace, DurationElementName, (End - Start).TotalMinutes);
            writer.WriteElementValue(Namespace, SetupDurationElementName, SetupDuration.TotalMinutes);
            writer.WriteElementValue(Namespace, TeardownDurationElementName, TeardownDuration.TotalMinutes);
        }

		public override object Clone()//CopyTo(OccurrenceInfo info)
		{
			//if (info == null)
			OccurrenceInfo info = new OccurrenceInfo();

			if (_end != null)
				info._end = new DateTime(_end.Value.Ticks);
			if (_start != null)
				info._start = new DateTime(_start.Value.Ticks);
			info._startHour = _startHour;
			info.Duration = new TimeSpan(Duration.Ticks);
			info.OccurrenceDate = new DateTime( OccurrenceDate.Ticks);
			info.SetupDuration = new TimeSpan(SetupDuration.Ticks);
			info.TeardownDuration = new TimeSpan(TeardownDuration.Ticks);
			return info;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(this, obj))
				return true;

			OccurrenceInfo info = obj as OccurrenceInfo;
			if (info == null)
				return false;
			if(info._end != _end)
				return false;
			if(info._start != _start)
				return false;
			if(info._startHour != _startHour)
				return false;
			if(info.Duration != Duration)
				return false;
			//if(info.End != End) //??
			//    return false;
			if(info.OccurrenceDate != OccurrenceDate)
				return false;
			if(info.SetupDuration != SetupDuration)
				return false;
			//if (info.Start != Start) //??
			//    return false;
			if(info.TeardownDuration != TeardownDuration)
				return false;
			
			return true;
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
    }
}
