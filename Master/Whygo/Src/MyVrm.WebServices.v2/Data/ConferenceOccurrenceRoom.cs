﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.WebServices.Data
{
    public class ConferenceOccurrenceRoom : ComplexProperty
    {
        public RoomId Id { get; private set; }
        public string Name { get; private set; }

		public override object Clone()
		{
			ConferenceOccurrenceRoom copy = new ConferenceOccurrenceRoom();
			copy.Id = new RoomId(Id.Id);
			copy.Name = string.Copy(Name);

			return copy;
		}

        public override string ToString()
        {
            if (!string.IsNullOrEmpty(Name))
                return Name;
            if (Id != null && Id.IsValid)
                return Id.ToString();
            return base.ToString();
        }

        internal override void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
        {
            do
            {
                reader.Read();
                if (reader.LocalName == "locationID")
                {
                    Id = new RoomId();
                    Id.LoadFromXml(reader, "locationID");
                }
                else if (reader.LocalName == "locationName")
                {
                    Name = reader.ReadValue();
                }
                else
                {
                    reader.SkipCurrentElement();
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, xmlElementName));
        }
    }
}
