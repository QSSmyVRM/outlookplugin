﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	internal class GetTemplateRequest : ServiceRequestBase<GetTemplateResponse>
	{
		public int templateId;

		internal GetTemplateRequest(MyVrmService service)
			: base(service)
		{
		}

		#region Overrides of ServiceRequestBase<GetTemplateResponse>

		internal override string GetXmlElementName()
		{
			return "login";
		}

		internal override string GetCommandName()
		{
			return Constants.GetOldTemplateCommandName; 
		}

		internal override string GetResponseXmlElementName()
		{
			return "template"; 
		}

		internal override GetTemplateResponse ParseResponse(MyVrmServiceXmlReader reader)
		{
			var response = new GetTemplateResponse();
			response.ReadElementsFromXml(reader);
			return response;
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			if (UserId != null)
				UserId.WriteToXml(writer, "userID");
			writer.WriteElementValue(XmlNamespace.NotSpecified, "templateID", templateId);
		}

		#endregion
	}

	//Response
	public class GetTemplateResponse : ServiceResponse
	{
	    public Conference Conference { get; private set; }
		//Read template info and template conference instance 
		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{
			Conference = new Conference(MyVrmService.Service);
			do
			{
				reader.Read();
				if (reader.IsStartElement(XmlNamespace.NotSpecified, "confInfo"))
				{
					Conference.LoadFromXml(reader, true);
				}
				else
				{
					reader.SkipCurrentElement();
				}
			} while (!reader.IsEndElement(XmlNamespace.NotSpecified, "confInfo"));
		}
	}
}
