﻿PRAGMA page_size = 4096;

CREATE TABLE [CacheData]
(
	[Key] varchar (128),
	[Value] BLOB,
	CONSTRAINT PK_CacheData PRIMARY KEY ([Key])
);


PRAGMA user_version = 2;