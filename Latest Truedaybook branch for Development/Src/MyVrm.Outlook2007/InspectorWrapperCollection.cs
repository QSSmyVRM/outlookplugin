﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace MyVrm.Outlook
{
    public class InspectorWrapperCollection : ICollection<InspectorWrapper>
    {
        private readonly List<InspectorWrapper> _inspectors = new List<InspectorWrapper>();

        #region Implementation of IEnumerable

        public IEnumerator<InspectorWrapper> GetEnumerator()
        {
            return _inspectors.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region Implementation of ICollection<InspectorWrapper>

        public void Add(InspectorWrapper item)
        {
            _inspectors.Add(item);
        }

        public void Clear()
        {
            _inspectors.Clear();
        }

        public bool Contains(InspectorWrapper item)
        {
            return _inspectors.Contains(item);
        }

        public void CopyTo(InspectorWrapper[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public bool Remove(InspectorWrapper item)
        {
            return _inspectors.Remove(item);
        }

        public int Count
        {
            get
            {
                return _inspectors.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        #endregion

        public InspectorWrapper this[object outlookItem]
        {
            get
            {
                return this.FirstOrDefault(inspectorWrapper => inspectorWrapper.Inspector.CurrentItem == outlookItem);
            }
        }
    }
}