﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.Outlook.WinForms.Conference
{
    partial class ConferenceGeneralControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            this.layoutControl2 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.editFormUserControl1 = new DevExpress.XtraGrid.Views.Grid.EditFormUserControl();
            this.checkEditSecure = new DevExpress.XtraEditors.CheckEdit();
            this.pCConferencingComboBoxEdit = new MyVrm.Outlook.WinForms.EnumComboBoxEdit();
            this.networkingSwitchingComboEdit = new MyVrm.Outlook.WinForms.EnumComboBoxEdit();
            this.vidyoCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.conferencePasswordEdit = new DevExpress.XtraEditors.TextEdit();
            this.conferenceTypeEdit = new MyVrm.Outlook.WinForms.EnumComboBoxEdit();
            this.vMRComboBoxEdit = new MyVrm.Outlook.WinForms.EnumComboBoxEdit();
            this.checkBJN = new DevExpress.XtraEditors.CheckEdit();
            this.BlueJeansMeetingID = new MyVrm.Outlook.WinForms.EnumComboBoxEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSecure.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pCConferencingComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.networkingSwitchingComboEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vidyoCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.conferencePasswordEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.conferenceTypeEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vMRComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBJN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BlueJeansMeetingID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl2
            // 
            this.layoutControl2.AllowCustomization = false;
            this.editFormUserControl1.SetBoundPropertyName(this.layoutControl2, "");
            this.layoutControl2.Controls.Add(this.editFormUserControl1);
            this.layoutControl2.Controls.Add(this.checkEditSecure);
            this.layoutControl2.Controls.Add(this.pCConferencingComboBoxEdit);
            this.layoutControl2.Controls.Add(this.networkingSwitchingComboEdit);
            this.layoutControl2.Controls.Add(this.vidyoCheckEdit);
            this.layoutControl2.Controls.Add(this.conferencePasswordEdit);
            this.layoutControl2.Controls.Add(this.conferenceTypeEdit);
            this.layoutControl2.Controls.Add(this.vMRComboBoxEdit);
            this.layoutControl2.Controls.Add(this.checkBJN);
            this.layoutControl2.Controls.Add(this.BlueJeansMeetingID);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Margin = new System.Windows.Forms.Padding(3, 44, 3, 44);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(388, 232, 250, 350);
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(949, 83);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // editFormUserControl1
            // 
            this.editFormUserControl1.Location = new System.Drawing.Point(2, 80);
            this.editFormUserControl1.Name = "editFormUserControl1";
            this.editFormUserControl1.Size = new System.Drawing.Size(929, 1);
            this.editFormUserControl1.TabIndex = 26;
            // 
            // checkEditSecure
            // 
            this.editFormUserControl1.SetBoundPropertyName(this.checkEditSecure, "");
            this.checkEditSecure.Location = new System.Drawing.Point(584, -15);
            this.checkEditSecure.Name = "checkEditSecure";
            this.checkEditSecure.Properties.Caption = "Secure";
            this.checkEditSecure.Size = new System.Drawing.Size(171, 19);
            this.checkEditSecure.StyleController = this.layoutControl2;
            this.checkEditSecure.TabIndex = 18;
            // 
            // pCConferencingComboBoxEdit
            // 
            this.editFormUserControl1.SetBoundPropertyName(this.pCConferencingComboBoxEdit, "");
            this.pCConferencingComboBoxEdit.Location = new System.Drawing.Point(434, -17);
            this.pCConferencingComboBoxEdit.Margin = new System.Windows.Forms.Padding(0, 2, 3, 2);
            this.pCConferencingComboBoxEdit.Name = "pCConferencingComboBoxEdit";
            this.pCConferencingComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.pCConferencingComboBoxEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.pCConferencingComboBoxEdit.Size = new System.Drawing.Size(146, 20);
            this.pCConferencingComboBoxEdit.StyleController = this.layoutControl2;
            this.pCConferencingComboBoxEdit.TabIndex = 16;
            // 
            // networkingSwitchingComboEdit
            // 
            this.editFormUserControl1.SetBoundPropertyName(this.networkingSwitchingComboEdit, "");
            this.networkingSwitchingComboEdit.Location = new System.Drawing.Point(711, 9);
            this.networkingSwitchingComboEdit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.networkingSwitchingComboEdit.Name = "networkingSwitchingComboEdit";
            this.networkingSwitchingComboEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.networkingSwitchingComboEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.networkingSwitchingComboEdit.Size = new System.Drawing.Size(133, 20);
            this.networkingSwitchingComboEdit.StyleController = this.layoutControl2;
            this.networkingSwitchingComboEdit.TabIndex = 17;
            // 
            // vidyoCheckEdit
            // 
            this.editFormUserControl1.SetBoundPropertyName(this.vidyoCheckEdit, "");
            this.vidyoCheckEdit.Location = new System.Drawing.Point(759, -17);
            this.vidyoCheckEdit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.vidyoCheckEdit.Name = "vidyoCheckEdit";
            this.vidyoCheckEdit.Properties.Caption = "Vidyo";
            this.vidyoCheckEdit.Size = new System.Drawing.Size(172, 19);
            this.vidyoCheckEdit.StyleController = this.layoutControl2;
            this.vidyoCheckEdit.TabIndex = 15;
            // 
            // conferencePasswordEdit
            // 
            this.editFormUserControl1.SetBoundPropertyName(this.conferencePasswordEdit, "");
            this.conferencePasswordEdit.Location = new System.Drawing.Point(123, 7);
            this.conferencePasswordEdit.Name = "conferencePasswordEdit";
            this.conferencePasswordEdit.Properties.PasswordChar = '*';
            this.conferencePasswordEdit.Properties.ValidateOnEnterKey = true;
            this.conferencePasswordEdit.Size = new System.Drawing.Size(189, 20);
            this.conferencePasswordEdit.StyleController = this.layoutControl2;
            toolTipTitleItem1.Text = "Conference Password";
            toolTipItem1.LeftIndent = 6;
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.conferencePasswordEdit.SuperTip = superToolTip1;
            this.conferencePasswordEdit.TabIndex = 12;
            // 
            // conferenceTypeEdit
            // 
            this.editFormUserControl1.SetBoundPropertyName(this.conferenceTypeEdit, "");
            this.conferenceTypeEdit.Location = new System.Drawing.Point(123, -17);
            this.conferenceTypeEdit.Name = "conferenceTypeEdit";
            this.conferenceTypeEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.conferenceTypeEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.conferenceTypeEdit.Size = new System.Drawing.Size(189, 20);
            this.conferenceTypeEdit.StyleController = this.layoutControl2;
            toolTipTitleItem2.Text = "Conference Type";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Select Audio/Video (default), Virtual Meeting Room, Audio Only, Point-to-Point, o" +
    "r Room Conference.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.conferenceTypeEdit.SuperTip = superToolTip2;
            this.conferenceTypeEdit.TabIndex = 10;
            // 
            // vMRComboBoxEdit
            // 
            this.editFormUserControl1.SetBoundPropertyName(this.vMRComboBoxEdit, "");
            this.vMRComboBoxEdit.Location = new System.Drawing.Point(434, 7);
            this.vMRComboBoxEdit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.vMRComboBoxEdit.Name = "vMRComboBoxEdit";
            this.vMRComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.vMRComboBoxEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.vMRComboBoxEdit.Size = new System.Drawing.Size(146, 20);
            this.vMRComboBoxEdit.StyleController = this.layoutControl2;
            this.vMRComboBoxEdit.TabIndex = 13;
            // 
            // checkBJN
            // 
            this.editFormUserControl1.SetBoundPropertyName(this.checkBJN, "");
            this.checkBJN.Location = new System.Drawing.Point(2, 33);
            this.checkBJN.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBJN.Name = "checkBJN";
            this.checkBJN.Properties.Caption = "BlueJeans Meeting ID";
            this.checkBJN.Size = new System.Drawing.Size(136, 19);
            this.checkBJN.StyleController = this.layoutControl2;
            this.checkBJN.TabIndex = 24;
            this.checkBJN.CheckedChanged += new System.EventHandler(this.checkBJN_CheckedChanged);
            // 
            // BlueJeansMeetingID
            // 
            this.editFormUserControl1.SetBoundPropertyName(this.BlueJeansMeetingID, "");
            this.BlueJeansMeetingID.Location = new System.Drawing.Point(121, 56);
            this.BlueJeansMeetingID.Margin = new System.Windows.Forms.Padding(0);
            this.BlueJeansMeetingID.Name = "BlueJeansMeetingID";
            this.BlueJeansMeetingID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.BlueJeansMeetingID.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.BlueJeansMeetingID.Size = new System.Drawing.Size(167, 20);
            this.BlueJeansMeetingID.StyleController = this.layoutControl2;
            this.BlueJeansMeetingID.TabIndex = 25;
            //this.BlueJeansMeetingID.SelectedIndexChanged += new System.EventHandler(this.BlueJeansMeetingID_SelectedIndexChanged);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem16,
            this.layoutControlItem4,
            this.layoutControlItem6,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem5,
            this.layoutControlItem10,
            this.layoutControlItem12,
            this.layoutControlItem7,
            this.emptySpaceItem2,
            this.emptySpaceItem1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, -17);
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Size = new System.Drawing.Size(933, 100);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.conferenceTypeEdit;
            this.layoutControlItem1.CustomizationFormText = "Conference Type:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(312, 24);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(312, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 0, 0, 0);
            this.layoutControlItem1.Size = new System.Drawing.Size(312, 24);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "Conference Type:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.conferencePasswordEdit;
            this.layoutControlItem16.CustomizationFormText = "Conference Password:";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem16.MaxSize = new System.Drawing.Size(312, 24);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(312, 24);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 0, 0, 2);
            this.layoutControlItem16.Size = new System.Drawing.Size(312, 24);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.Text = "Conference Password:";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlItem4.Control = this.vMRComboBoxEdit;
            this.layoutControlItem4.ControlAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.layoutControlItem4.ImageToTextDistance = 0;
            this.layoutControlItem4.Location = new System.Drawing.Point(312, 24);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(270, 24);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(239, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 2, 0, 2);
            this.layoutControlItem4.Size = new System.Drawing.Size(270, 24);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "Virtual Meeting Room:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlItem6.Control = this.networkingSwitchingComboEdit;
            this.layoutControlItem6.CustomizationFormText = "Network Classification:";
            this.layoutControlItem6.ImageToTextDistance = 0;
            this.layoutControlItem6.Location = new System.Drawing.Point(582, 24);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(264, 26);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(141, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 2, 2, 2);
            this.layoutControlItem6.Size = new System.Drawing.Size(351, 24);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "Network Classification:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlItem8.Control = this.pCConferencingComboBoxEdit;
            this.layoutControlItem8.CustomizationFormText = "Desktop Video:";
            this.layoutControlItem8.ImageToTextDistance = 0;
            this.layoutControlItem8.Location = new System.Drawing.Point(312, 0);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(270, 24);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(270, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 2, 0, 2);
            this.layoutControlItem8.Size = new System.Drawing.Size(270, 24);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "Desktop Video:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.checkEditSecure;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(582, 0);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(175, 24);
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.vidyoCheckEdit;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(757, 0);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(67, 23);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 0, 2);
            this.layoutControlItem5.Size = new System.Drawing.Size(176, 24);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.checkBJN;
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(140, 23);
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.BlueJeansMeetingID;
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 71);
            this.layoutControlItem12.Name = "BlueJeans Meeting Type";
            this.layoutControlItem12.Size = new System.Drawing.Size(290, 24);
            this.layoutControlItem12.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.editFormUserControl1;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 95);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(933, 5);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(290, 71);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(643, 24);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.BlueJeansMeetingID;
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(925, 705);
            this.layoutControlItem11.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(551, 24);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(433, 10);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.checkEditSecure;
            this.layoutControlItem17.CustomizationFormText = "layoutControlItem17";
            this.layoutControlItem17.Location = new System.Drawing.Point(692, 0);
            this.layoutControlItem17.MinSize = new System.Drawing.Size(67, 23);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 0, 2);
            this.layoutControlItem17.Size = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem17.Text = "layoutControlItem5";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.conferenceTypeEdit;
            this.layoutControlItem2.CustomizationFormText = "Conference Type:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(312, 24);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(312, 24);
            this.layoutControlItem2.Name = "layoutControlItem1";
            this.layoutControlItem2.Size = new System.Drawing.Size(312, 25);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "Conference Type:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(129, 16);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.conferenceTypeEdit;
            this.layoutControlItem3.CustomizationFormText = "Conference Type:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(312, 24);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(312, 24);
            this.layoutControlItem3.Name = "layoutControlItem1";
            this.layoutControlItem3.Size = new System.Drawing.Size(312, 25);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "Conference Type:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(129, 16);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(140, 48);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(793, 23);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ConferenceGeneralControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.editFormUserControl1.SetBoundPropertyName(this, "");
            this.Controls.Add(this.layoutControl2);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.Name = "ConferenceGeneralControl";
            this.Size = new System.Drawing.Size(949, 83);
            this.Load += new System.EventHandler(this.ConferenceGeneralControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSecure.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pCConferencingComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.networkingSwitchingComboEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vidyoCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.conferencePasswordEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.conferenceTypeEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vMRComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBJN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BlueJeansMeetingID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.TextEdit conferencePasswordEdit;
        private EnumComboBoxEdit conferenceTypeEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
		private /*DevExpress.XtraEditors.ComboBoxEdit*/EnumComboBoxEdit vMRComboBoxEdit;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.CheckEdit vidyoCheckEdit;
		private /*DevExpress.XtraEditors.ComboBoxEdit*/EnumComboBoxEdit pCConferencingComboBoxEdit;
        private /*DevExpress.XtraEditors.ComboBoxEdit*/EnumComboBoxEdit networkingSwitchingComboEdit;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
		
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraEditors.CheckEdit checkEditSecure;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
       //private DevExpress.XtraLayout.LayoutControlItem layoutControlItemNew;//103947
        private DevExpress.XtraEditors.CheckEdit checkBJN;//103947
        private /*DevExpress.XtraEditors.ComboBoxEdit*/EnumComboBoxEdit BlueJeansMeetingID;
       // private DevExpress.XtraLayout.LayoutControlGroup Root;
      //  private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraGrid.Views.Grid.EditFormUserControl editFormUserControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
     //   private DevExpress.XtraLayout.SplitterItem splitterItem1;//103947

    }
}