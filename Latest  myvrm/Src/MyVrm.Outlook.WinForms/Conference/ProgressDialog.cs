﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MyVrm.Outlook.WinForms.Conference
{
	public partial class ProgressDialog : MyVrm.Outlook.WinForms.Dialog
	{
		private Cursor _currCursor;
		public ProgressDialog()
		{
			InitializeComponent();
			OkEnabled = false;
			OkVisible = false;
			CancelEnabled = false;
			CancelVisible = false;
			ApplyVisible = false;
			ApplyEnabled = false;
			progressBarControl1.Position = 0;
			progressBarControl1.Properties.Minimum = 0;
			progressBarControl1.Properties.Maximum = 100;
			progressBarControl1.Properties.PercentView = true;
			progressBarControl1.Properties.Step = 10;
			_currCursor = Cursor.Current;
			Cursor.Current = Cursors.WaitCursor;
		}

		public void StepProrgessValue()
		{
			progressBarControl1.PerformStep();
			progressBarControl1.Update();
		}

		public void StepProrgessValue(int iPercent)
		{
			progressBarControl1.EditValue = iPercent;
			progressBarControl1.Update();
		}

		void ProgressDialog_Closed(object sender, System.EventArgs e)
		{
			Cursor.Current = _currCursor;
		}
	}
}
