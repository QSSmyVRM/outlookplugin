﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System.Collections;
using System.Collections.Generic;

namespace MyVrm.WebServices.Data
{
    public class RoomCollection : ICollection<Room>
    {
        private readonly List<Room> _rooms = new List<Room>();

        #region Implementation of IEnumerable

        public IEnumerator<Room> GetEnumerator()
        {
            return _rooms.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region Implementation of ICollection<Room>

        public void Add(Room item)
        {
            _rooms.Add(item);
        }

        public void Clear()
        {
            _rooms.Clear();
        }

        public bool Contains(Room item)
        {
            return _rooms.Contains(item);
        }

        public void CopyTo(Room[] array, int arrayIndex)
        {
            _rooms.CopyTo(array, arrayIndex);
        }

        public bool Remove(Room item)
        {
            return _rooms.Remove(item);
        }

        public int Count
        {
            get { return _rooms.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        #endregion
    }
}
