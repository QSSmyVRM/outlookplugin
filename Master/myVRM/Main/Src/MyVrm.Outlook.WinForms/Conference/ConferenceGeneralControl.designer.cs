﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.Outlook.WinForms.Conference
{
    partial class ConferenceGeneralControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            this.layoutControl2 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.checkEditSecure = new DevExpress.XtraEditors.CheckEdit();
            this.pCConferencingComboBoxEdit = new MyVrm.Outlook.WinForms.EnumComboBoxEdit();
            this.networkingSwitchingComboEdit = new MyVrm.Outlook.WinForms.EnumComboBoxEdit();
            this.vidyoCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.conferencePasswordEdit = new DevExpress.XtraEditors.TextEdit();
            this.conferenceTypeEdit = new MyVrm.Outlook.WinForms.EnumComboBoxEdit();
            this.vMRComboBoxEdit = new MyVrm.Outlook.WinForms.EnumComboBoxEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSecure.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pCConferencingComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.networkingSwitchingComboEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vidyoCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.conferencePasswordEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.conferenceTypeEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vMRComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl2
            // 
            this.layoutControl2.AllowCustomizationMenu = false;
            this.layoutControl2.Controls.Add(this.checkEditSecure);
            this.layoutControl2.Controls.Add(this.pCConferencingComboBoxEdit);
            this.layoutControl2.Controls.Add(this.networkingSwitchingComboEdit);
            this.layoutControl2.Controls.Add(this.vidyoCheckEdit);
            this.layoutControl2.Controls.Add(this.conferencePasswordEdit);
            this.layoutControl2.Controls.Add(this.conferenceTypeEdit);
            this.layoutControl2.Controls.Add(this.vMRComboBoxEdit);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(949, 48);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // checkEditSecure
            // 
            this.checkEditSecure.Location = new System.Drawing.Point(651, 2);
            this.checkEditSecure.Name = "checkEditSecure";
            this.checkEditSecure.Properties.Caption = "Secure";
            this.checkEditSecure.Size = new System.Drawing.Size(296, 19);
            this.checkEditSecure.StyleController = this.layoutControl2;
            this.checkEditSecure.TabIndex = 18;
            // 
            // pCConferencingComboBoxEdit
            // 
            this.pCConferencingComboBoxEdit.Location = new System.Drawing.Point(427, 0);
            this.pCConferencingComboBoxEdit.Margin = new System.Windows.Forms.Padding(0, 2, 3, 2);
            this.pCConferencingComboBoxEdit.Name = "pCConferencingComboBoxEdit";
            this.pCConferencingComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.pCConferencingComboBoxEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.pCConferencingComboBoxEdit.Size = new System.Drawing.Size(153, 20);
            this.pCConferencingComboBoxEdit.StyleController = this.layoutControl2;
            this.pCConferencingComboBoxEdit.TabIndex = 16;
            // 
            // networkingSwitchingComboEdit
            // 
            this.networkingSwitchingComboEdit.Location = new System.Drawing.Point(704, 26);
            this.networkingSwitchingComboEdit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.networkingSwitchingComboEdit.Name = "networkingSwitchingComboEdit";
            this.networkingSwitchingComboEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.networkingSwitchingComboEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.networkingSwitchingComboEdit.Size = new System.Drawing.Size(140, 20);
            this.networkingSwitchingComboEdit.StyleController = this.layoutControl2;
            this.networkingSwitchingComboEdit.TabIndex = 17;
            // 
            // vidyoCheckEdit
            // 
            this.vidyoCheckEdit.Location = new System.Drawing.Point(584, 0);
            this.vidyoCheckEdit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.vidyoCheckEdit.Name = "vidyoCheckEdit";
            this.vidyoCheckEdit.Properties.Caption = "Vidyo";
            this.vidyoCheckEdit.Size = new System.Drawing.Size(63, 19);
            this.vidyoCheckEdit.StyleController = this.layoutControl2;
            this.vidyoCheckEdit.TabIndex = 15;
            // 
            // conferencePasswordEdit
            // 
            this.conferencePasswordEdit.Location = new System.Drawing.Point(116, 24);
            this.conferencePasswordEdit.Name = "conferencePasswordEdit";
            this.conferencePasswordEdit.Properties.PasswordChar = '*';
            this.conferencePasswordEdit.Properties.ValidateOnEnterKey = true;
            this.conferencePasswordEdit.Size = new System.Drawing.Size(196, 20);
            this.conferencePasswordEdit.StyleController = this.layoutControl2;
            toolTipTitleItem1.Text = "Conference Password";
            toolTipItem1.LeftIndent = 6;
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.conferencePasswordEdit.SuperTip = superToolTip1;
            this.conferencePasswordEdit.TabIndex = 12;
            // 
            // conferenceTypeEdit
            // 
            this.conferenceTypeEdit.Location = new System.Drawing.Point(116, 0);
            this.conferenceTypeEdit.Name = "conferenceTypeEdit";
            this.conferenceTypeEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.conferenceTypeEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.conferenceTypeEdit.Size = new System.Drawing.Size(196, 20);
            this.conferenceTypeEdit.StyleController = this.layoutControl2;
            toolTipTitleItem2.Text = "Conference Type";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Select Audio/Video (default), Virtual Meeting Room, Audio Only, Point-to-Point, o" +
    "r Room Conference.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.conferenceTypeEdit.SuperTip = superToolTip2;
            this.conferenceTypeEdit.TabIndex = 10;
         //   this.conferenceTypeEdit.SelectedIndexChanged += new System.EventHandler(this.conferenceTypeEdit_SelectedIndexChanged);
            // 
            // vMRComboBoxEdit
            // 
            this.vMRComboBoxEdit.Location = new System.Drawing.Point(427, 24);
            this.vMRComboBoxEdit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.vMRComboBoxEdit.Name = "vMRComboBoxEdit";
            this.vMRComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.vMRComboBoxEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.vMRComboBoxEdit.Size = new System.Drawing.Size(153, 20);
            this.vMRComboBoxEdit.StyleController = this.layoutControl2;
            this.vMRComboBoxEdit.TabIndex = 13;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem16,
            this.layoutControlItem4,
            this.layoutControlItem6,
            this.layoutControlItem8,
            this.layoutControlItem5,
            this.layoutControlItem9});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Size = new System.Drawing.Size(949, 48);
            this.layoutControlGroup2.Text = "Root";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.conferenceTypeEdit;
            this.layoutControlItem1.CustomizationFormText = "Conference Type:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(312, 24);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(312, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 0, 0, 0);
            this.layoutControlItem1.Size = new System.Drawing.Size(312, 24);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "Conference Type:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(109, 13);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.conferencePasswordEdit;
            this.layoutControlItem16.CustomizationFormText = "Conference Password:";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem16.MaxSize = new System.Drawing.Size(312, 24);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(312, 24);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 0, 0, 2);
            this.layoutControlItem16.Size = new System.Drawing.Size(312, 24);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.Text = "Conference Password:";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(109, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlItem4.Control = this.vMRComboBoxEdit;
            this.layoutControlItem4.ControlAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.layoutControlItem4.ImageToTextDistance = 0;
            this.layoutControlItem4.Location = new System.Drawing.Point(312, 24);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(270, 24);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(239, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 2, 0, 2);
            this.layoutControlItem4.Size = new System.Drawing.Size(270, 24);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "Virtual Meeting Room:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(109, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlItem6.Control = this.networkingSwitchingComboEdit;
            this.layoutControlItem6.CustomizationFormText = "Network Classification:";
            this.layoutControlItem6.ImageToTextDistance = 0;
            this.layoutControlItem6.Location = new System.Drawing.Point(582, 24);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(264, 26);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(141, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 2, 2, 2);
            this.layoutControlItem6.Size = new System.Drawing.Size(367, 24);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "Network Classification:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(109, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlItem8.Control = this.pCConferencingComboBoxEdit;
            this.layoutControlItem8.CustomizationFormText = "Desktop Video:";
            this.layoutControlItem8.ImageToTextDistance = 0;
            this.layoutControlItem8.Location = new System.Drawing.Point(312, 0);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(270, 24);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(270, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 2, 0, 2);
            this.layoutControlItem8.Size = new System.Drawing.Size(270, 24);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "Desktop Video:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(109, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.vidyoCheckEdit;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(582, 0);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(67, 23);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 0, 2);
            this.layoutControlItem5.Size = new System.Drawing.Size(67, 24);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.checkEditSecure;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(649, 0);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(300, 24);
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(551, 24);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(433, 10);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.checkEditSecure;
            this.layoutControlItem17.CustomizationFormText = "layoutControlItem17";
            this.layoutControlItem17.Location = new System.Drawing.Point(692, 0);
            this.layoutControlItem17.MinSize = new System.Drawing.Size(67, 23);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 0, 2);
            this.layoutControlItem17.Size = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem17.Text = "layoutControlItem5";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextToControlDistance = 0;
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.conferenceTypeEdit;
            this.layoutControlItem2.CustomizationFormText = "Conference Type:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(312, 24);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(312, 24);
            this.layoutControlItem2.Name = "layoutControlItem1";
            this.layoutControlItem2.Size = new System.Drawing.Size(312, 25);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "Conference Type:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(129, 16);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.conferenceTypeEdit;
            this.layoutControlItem3.CustomizationFormText = "Conference Type:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(312, 24);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(312, 24);
            this.layoutControlItem3.Name = "layoutControlItem1";
            this.layoutControlItem3.Size = new System.Drawing.Size(312, 25);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "Conference Type:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(129, 16);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // ConferenceGeneralControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.layoutControl2);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.Name = "ConferenceGeneralControl";
            this.Size = new System.Drawing.Size(949, 48);
            this.Load += new System.EventHandler(this.ConferenceGeneralControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSecure.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pCConferencingComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.networkingSwitchingComboEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vidyoCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.conferencePasswordEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.conferenceTypeEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vMRComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.TextEdit conferencePasswordEdit;
        private EnumComboBoxEdit conferenceTypeEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
		private /*DevExpress.XtraEditors.ComboBoxEdit*/EnumComboBoxEdit vMRComboBoxEdit;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.CheckEdit vidyoCheckEdit;
		private /*DevExpress.XtraEditors.ComboBoxEdit*/EnumComboBoxEdit pCConferencingComboBoxEdit;
        private /*DevExpress.XtraEditors.ComboBoxEdit*/EnumComboBoxEdit networkingSwitchingComboEdit;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
		
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraEditors.CheckEdit checkEditSecure;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;


    }
}