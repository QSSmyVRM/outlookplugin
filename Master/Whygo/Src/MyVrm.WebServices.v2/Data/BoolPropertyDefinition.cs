﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;

namespace MyVrm.WebServices.Data
{
    internal class BoolPropertyDefinition : TypedPropertyDefinition
    {
        internal BoolPropertyDefinition(string xmlElementName) : base(xmlElementName)
        {
            PropertyType = typeof (bool);
        }

        internal BoolPropertyDefinition(string xmlElementName, PropertyDefinitionFlags flags)
            : base(xmlElementName, flags)
        {
            PropertyType = typeof(bool);
        }

        #region Overrides of TypedPropertyDefinition

        internal override object Parse(string value)
        {
            return Utilities.BoolStringToBool(value);
        }

        internal override string ToString(object value)
        {
            return Utilities.BoolToBoolString((bool)value);
        }

        #endregion
    }
}
