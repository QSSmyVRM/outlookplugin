﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook
{
    public enum RecurrenceState
    {
        NotRecurring,
        Master,
        Occurrence,
        Exception,
    }
    /// <summary>
    /// Represents Outlook version-independent appointment.
    /// </summary>
    public abstract class OutlookAppointment : IDisposable, INotifyPropertyChanged
    {
        protected OutlookAppointment()
        {
            RaiseSavingEvent = true; 
        }
        public abstract string Subject { get; set; }
        public abstract DateTime Start { get; set; }
        public abstract DateTime End { get; set; }
        public abstract TimeZoneInfo StartTimeZone { get; set; }
        public abstract TimeZoneInfo EndTimeZone { get; set; }
        public abstract TimeSpan Duration { get; set; }
        public abstract DateTime StartInStartTimeZone { get; set; }
        public abstract DateTime EndInEndTimeZone { get; set; }
        public abstract string Location { get; set; }
        public abstract ConferenceId ConferenceId { get; set; }
        public abstract string GlobalAppointmentId { get;}
        public abstract bool AllDayEvent { get; set; }//ZD 102172
        public bool LinkedWithConference
        {
            get { return ConferenceId != null; }
        }
        public abstract event PropertyChangedEventHandler PropertyChanged;
        public abstract event CancelEventHandler Saving;
        public abstract event CancelEventHandler Sending;
        public abstract event CancelEventHandler BeforeDelete;
        public abstract ReadOnlyCollection<OutlookRecipient> Recipients { get; }
    	public abstract void SetRecipients(IEnumerable<string> arg);
		public abstract void ClearRecipients();
        public abstract OutlookRecipient CurrentUser { get; }
        public abstract string Body { get; set; }
        public abstract string HtmlBody { get; set; }
        public abstract string RtfBody { get; set; }
        public abstract bool IsRecurring { get; set; }
        public abstract RecurrenceState RecurrenceState { get; set; }
        public abstract void RemoveRecipient(string email);
        public abstract FreeBusyStatus BusyStatus { get; set; }
        public abstract RecurrencePattern GetRecurrencePattern(TimeZoneInfo timeZone);
        public bool RaiseSavingEvent { get; set; }
        public abstract OutlookAppointment GetOccurrence(DateTime occurrenceTime);
        public abstract void Save();
        public abstract void Delete();
    	public abstract void SetNonOutlookProperty(bool value);
		public abstract bool GetNonOutlookProperty();
        #region Implementation of IDisposable

        public abstract void Dispose();

        #endregion
    }
}
