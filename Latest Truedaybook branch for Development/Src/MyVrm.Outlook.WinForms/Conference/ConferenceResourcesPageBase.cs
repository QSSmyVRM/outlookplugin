﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.ComponentModel;
using System.Text;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook.WinForms.Conference
{
    public partial class ConferenceResourcesPageBase : ConferencePage
    {

        public ConferenceResourcesPageBase()
        {
            InitializeComponent();

			Text = Strings.ResourcesLableText;

            ConferenceBindingSource.DataSource = typeof (ConferenceWrapper);
            dxErrorProvider.DataSource = typeof(ConferenceWrapper);
            ConferenceBindingSource.DataSourceChanged += ConferenceBindingSource_DataSourceChanged;
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
         Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        public bool HostPresent
        {
            get
            {
                return Conference.HostPresent;
            }
            set
            {
                Conference.HostPresent = value;
            }
        }

        protected override void OnApplying(CancelEventArgs e)
        {
            Validate();
            if (dxErrorProvider.HasErrors)
            {
                var controlsWithError = dxErrorProvider.GetControlsWithError();
                var sb = new StringBuilder();
                foreach (var control in controlsWithError)
                {
                    sb.AppendLine(string.Format("{0}", dxErrorProvider.GetError(control)));
                }
                ShowError(sb.ToString());
                e.Cancel = true;
            }
            if (Conference != null)
            {
                Conference.Host = MyVrmService.Service.UserId;
            }
        }

        private void ConferenceBindingSource_DataSourceChanged(object sender, EventArgs e)
        {
            if (Conference != null)
            {
                dxErrorProvider.DataSource = ConferenceBindingSource;
            }
        }
    }
}