﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System.Collections.Generic;

namespace MyVrm.WebServices.Data
{
    public sealed class ConferenceAdvancedAudioVideoSettings : ComplexProperty, IOwnedProperty
    {
        private ConferenceEndpointCollection _endpoints;
        private Conference _owner;

		public override object Clone()
		{
			ConferenceAdvancedAudioVideoSettings copy = new ConferenceAdvancedAudioVideoSettings();
			copy._endpoints = (ConferenceEndpointCollection)_endpoints.Clone();
			copy._owner = _owner;
			copy.SingleDialin = SingleDialin;

			return copy;
		}

        internal ConferenceAdvancedAudioVideoSettings()
        {
        }

        public bool SingleDialin { get; set; }

        public ConferenceEndpointCollection Endpoints
        {
            get
            {
                if (_endpoints == null)
                {
                    _endpoints = new ConferenceEndpointCollection();
                }
                return _endpoints;
            }
        }

        internal void Save()
        {
            _owner.Service.SaveConferenceAdvancedAudioVideoSettings(_owner.Id, this);
        }

        #region Implementation of IOwnedProperty

        ServiceObject IOwnedProperty.Owner
        {
            get
            {
                return _owner;
            }
            set
            {
                _owner = value as Conference;
            }
        }

        #endregion

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            writer.WriteStartElement(XmlNamespace.NotSpecified, "AVParams");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "SingleDialin", SingleDialin);
            writer.WriteEndElement();
            Endpoints.WriteToXml(writer, "Endpoints");
        }

        internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
        {
            switch(reader.LocalName)
            {
                case "SingleDialin":
                {
                    SingleDialin = Utilities.BoolStringToBool(reader.ReadElementValue());
                    return true;
                }
                case "Endpoints":
                {
                    Endpoints.LoadFromXml(reader, "Endpoints");
                    return true;
                }
            }
            return false;
        }
    }
}
