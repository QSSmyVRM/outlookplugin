﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
    public class LanguageInfo
    {

        private string _displayName;

        public string DisplayName
        {
            get { return _displayName; }
            set { _displayName = value; }
        }

        private int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public static LanguageInfo ConvertToLanguageName(int id)
        {
            LanguageInfo info = new LanguageInfo();
            info.DisplayName = "English";
            switch (id)
            {
                case 1:
                case 2:
                    info.DisplayName = "English";
                    break;
                case 3:
                    info.DisplayName = "Spanish";
                    break;
                case 5:
                    info.DisplayName = "French";
                    break;
                case 6:
                    info.DisplayName = "Japanese";
                    break;
                case 7:
                    info.DisplayName = "Italian";
                    break;
                default:
                    info.DisplayName = "English";
                    break;
            }
            return info;
        }

        public static int ConvertToLanguageId(LanguageInfo info)
        {
            int iReturn = -1;
            switch (info.DisplayName.ToLower())
            {
                case "english":
                    iReturn = 1;
                    break;
                case "spanish":
                    iReturn = 3;
                    break;
                default:
                    iReturn = -1;
                    break;
            }

            return iReturn;
        }
    }
}
