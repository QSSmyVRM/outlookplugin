﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.Outlook.WinForms.Options
{
    partial class UsersDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.lblTrueDayBookContacts = new DevExpress.XtraEditors.LabelControl();
            this.lblOutlookContacts = new DevExpress.XtraEditors.LabelControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gvOutlookContacts = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.txtSearchTDBContacts = new DevExpress.XtraEditors.TextEdit();
            this.txtOCSearch = new DevExpress.XtraEditors.TextEdit();
            this.ShowMoreButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).BeginInit();
            this.ContentPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvOutlookContacts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSearchTDBContacts.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOCSearch.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ContentPanel
            // 
            this.ContentPanel.Controls.Add(this.ShowMoreButton);
            this.ContentPanel.Controls.Add(this.txtOCSearch);
            this.ContentPanel.Controls.Add(this.txtSearchTDBContacts);
            this.ContentPanel.Controls.Add(this.gridControl2);
            this.ContentPanel.Controls.Add(this.lblOutlookContacts);
            this.ContentPanel.Controls.Add(this.lblTrueDayBookContacts);
            this.ContentPanel.Controls.Add(this.gridControl1);
            this.ContentPanel.Size = new System.Drawing.Size(657, 578);
            // 
            // gridControl1
            // 
            this.gridControl1.Location = new System.Drawing.Point(0, 31);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(654, 256);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.gridControl1.Click += new System.EventHandler(this.gridControl1_Click);
            this.gridControl1.DoubleClick += new System.EventHandler(this.gridControl1_DoubleClick);
            this.gridControl1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.gridControl1_MouseDoubleClick);
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsMenu.EnableColumnMenu = false;
            this.gridView1.OptionsMenu.EnableFooterMenu = false;
            this.gridView1.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridView1.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.Hidden;
            this.gridView1.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.Click += new System.EventHandler(this.gridView1_DoubleClick);// 102581
            // 
            // lblTrueDayBookContacts
            // 
            this.lblTrueDayBookContacts.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblTrueDayBookContacts.Location = new System.Drawing.Point(0, 11);
            this.lblTrueDayBookContacts.Name = "lblTrueDayBookContacts";
            this.lblTrueDayBookContacts.Size = new System.Drawing.Size(129, 13);
            this.lblTrueDayBookContacts.TabIndex = 1;
            this.lblTrueDayBookContacts.Text = "TrueDaybook Contacts";
            // 
            // lblOutlookContacts
            // 
            this.lblOutlookContacts.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblOutlookContacts.Location = new System.Drawing.Point(4, 294);
            this.lblOutlookContacts.Name = "lblOutlookContacts";
            this.lblOutlookContacts.Size = new System.Drawing.Size(97, 13);
            this.lblOutlookContacts.TabIndex = 2;
            this.lblOutlookContacts.Text = "Outlook Contacts";
            // 
            // gridControl2
            // 
            this.gridControl2.Location = new System.Drawing.Point(0, 317);
            this.gridControl2.MainView = this.gvOutlookContacts;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(654, 256);
            this.gridControl2.TabIndex = 3;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvOutlookContacts});
            // 
            // gvOutlookContacts
            // 
            this.gvOutlookContacts.GridControl = this.gridControl2;
            this.gvOutlookContacts.Name = "gvOutlookContacts";
            this.gvOutlookContacts.OptionsMenu.EnableColumnMenu = false;
            this.gvOutlookContacts.OptionsMenu.EnableFooterMenu = false;
            this.gvOutlookContacts.OptionsMenu.EnableGroupPanelMenu = false;
            this.gvOutlookContacts.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.Hidden;
            this.gvOutlookContacts.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.gvOutlookContacts.OptionsView.ShowGroupPanel = false;
            this.gvOutlookContacts.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gvOutlookContacts.OptionsView.ShowIndicator = false;
            this.gvOutlookContacts.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gvOutlookContacts.Click += new System.EventHandler(this.gvOutlookContacts_DoubleClick);//102581
            // 
            // txtSearchTDBContacts
            // 
            this.txtSearchTDBContacts.Location = new System.Drawing.Point(156, 8);
            this.txtSearchTDBContacts.Name = "txtSearchTDBContacts";
            this.txtSearchTDBContacts.Size = new System.Drawing.Size(223, 20);
            this.txtSearchTDBContacts.TabIndex = 4;
            this.txtSearchTDBContacts.EditValueChanged += new System.EventHandler(this.txtSearchTDBContacts_EditValueChanged);
            this.txtSearchTDBContacts.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.txtSearchTDBContacts_EditValueChanging);
            // 
            // txtOCSearch
            // 
            this.txtOCSearch.Location = new System.Drawing.Point(156, 294);
            this.txtOCSearch.Name = "txtOCSearch";
            this.txtOCSearch.Size = new System.Drawing.Size(223, 20);
            this.txtOCSearch.TabIndex = 5;
            this.txtOCSearch.EditValueChanged += new System.EventHandler(this.txtOCSearch_EditValueChanged);
            // 
            // ShowMoreButton
            // 
            this.ShowMoreButton.Location = new System.Drawing.Point(400, 294);
            this.ShowMoreButton.Name = "ShowMoreButton";
            this.ShowMoreButton.Size = new System.Drawing.Size(87, 21);
            this.ShowMoreButton.TabIndex = 6;
            this.ShowMoreButton.Text = "Show More";
            this.ShowMoreButton.UseVisualStyleBackColor = true;
            this.ShowMoreButton.Visible = false;
            this.ShowMoreButton.Click += new System.EventHandler(this.ShowMoreButton_Click);
            // 
            // UsersDialog
            // 
            this.ApplyEnabled = true;
            this.ApplyVisible = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(681, 634);
            this.Name = "UsersDialog";
            this.Text = "Users";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UsersDialog_FormClosing);
            this.Load += new System.EventHandler(this.UsersDialog_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).EndInit();
            this.ContentPanel.ResumeLayout(false);
            this.ContentPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvOutlookContacts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSearchTDBContacts.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOCSearch.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gvOutlookContacts;
        private DevExpress.XtraEditors.LabelControl lblOutlookContacts;
        private DevExpress.XtraEditors.LabelControl lblTrueDayBookContacts;
        private DevExpress.XtraEditors.TextEdit txtSearchTDBContacts;
        private DevExpress.XtraEditors.TextEdit txtOCSearch;
        private System.Windows.Forms.Button ShowMoreButton;
    }
}