﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Windows.Forms;
using Microsoft.Office.Interop.Outlook;
using Microsoft.Office.Tools.Ribbon;
using MyVrm.Outlook;
using MyVrm.Outlook.WinForms;
using MyVrm.Outlook.WinForms.Conference;
using MyVrm.Outlook.WinForms.ManageRooms;
using MyVrm.Outlook.WinForms.Options;
using MyVrm.WebServices.Data;
using Application = Microsoft.Office.Interop.Outlook.Application;
using Exception = System.Exception;
using Image = System.Drawing.Image;

namespace MyVrmAddin2007
{
    public partial class MainRibbon : RibbonBase
    {
        private string selectedTemplateID;
        public DialogResult dlgResult;
      

        public MainRibbon()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
            selectedTemplateID = string.Empty;
            //Texts localization
            //generalGroup.Label = Strings.General;
            //roomsPrivateCalendarButton.Label = Strings.RoomsPrivateCalendar;
            //roomsPrivateCalendarButton.SuperTip = Strings.RoomsPrivateCalendarButtonTip;
            //roomsPublicCalendarButton.Label = Strings.RoomsPublicCalendar;
            //roomsPublicCalendarButton.SuperTip = Strings.RoomsPublicCalendarButtonTip;
            //templatesGroup.Label = Strings.Templates;
            templateMenu.Label = Strings.CreateFromTemplate;
            templateMenu.SuperTip = Strings.CreateFromTemplateButtonTip;
            //preferencesGroup.Label = Strings.Preferences;
            //favoriteRoomsButton.Label = Strings.FavoriteRooms;
            //favoriteRoomsButton.SuperTip = Strings.FavoritRoomsButtonTip;
            groupTrueDaybook.Label = MyVrmAddin.ProductDisplayName;//Strings.Show;
            bnVirtualConference.Label = Strings.NewConference;
            btnInstanceConference.Label = Strings.InstantConference;
            //optionsGroup.Label = Strings.OptionsMenuPoint;
            optionsButton.Label = Strings.OptionsMenuPoint;
            optionsButton.SuperTip = Strings.OptionsButtonTip;
            bnCheckSeats.Label = Strings.CheckAvailableSeatsMenuItemTxt;
            bnCheckSeats.SuperTip = Strings.ConferenceApprovalButtonTip;
        }

        private void MainRibbon_Load(object sender, RibbonUIEventArgs e)
        {
            MyVrmAddin.ProductDisplayName = Properties.Settings.Default["ProductDisplayName"] as string;
            tabMyVrm.Label = MyVrmAddin.ProductDisplayName;
           
            //if (MyVrmAddin.Logo65 == null)
            //{
            //    string settingValue = Properties.Settings.Default["Logo65x65"] as string;
            //    MyVrmAddin.Logo65 = Properties.Resources.Logo65x65; //default
            //    if (!string.IsNullOrEmpty(settingValue))
            //        MyVrmAddin.Logo65 = Properties.Resources.ResourceManager.GetObject(settingValue) as Bitmap;
            //}

            //conferenceButton.Image = (Image)MyVrmAddin.Logo65;

            //if (String.IsNullOrEmpty(MyVrmAddin.BuildType))
            //{
            //    MyVrmAddin.BuildType = Properties.Settings.Default["BuildType"] as string;
            //}
            ////For York version - hide all points except "New Conference" and "Options")
            //if (MyVrmAddin.BuildType == "03")
            //{
            //    generalGroup.Visible = false;
            //    roomsPrivateCalendarButton.Visible = false;
            //    roomsPublicCalendarButton.Visible = false;
            //    bnCheckSeats.Visible = false;
            //    templatesGroup.Visible = false;
            //    templateMenu.Visible = false;
            //    preferencesGroup.Visible = false;
            //    favoriteRoomsButton.Visible = false;
            //}
            //**********
            //tabMyVRM.Label = "myVRM Virtual Conference";
            templateMenu.Enabled = true;
        }

        //private void favoriteRoomsButton_Click(object sender, RibbonControlEventArgs e)
        //{
        //    ConferenceRibbon.favoriteRoomsButton_OnClick();
        //}

        //private void roomsCalendarButton_Click(object sender, RibbonControlEventArgs e)
        //{
        //    ConferenceRibbon.roomsCalendarButton_OnClick(sender == roomsPrivateCalendarButton);
        //}

        //private void approveConferenceButton_Click(object sender, RibbonControlEventArgs e)
        //{
        //    ConferenceRibbon.approvalConferencesButton_OnClick();
        //}

        private void tdTemplateMenu_ItemsLoading(object sender, RibbonControlEventArgs e)
        {
            templateMenu_OnItemsLoading(templateMenu, tdCheckBox_Click, selectedTemplateID);
        }
        public void templateMenu_OnItemsLoading(RibbonMenu menu, RibbonControlEventHandler handler, string selected)
        {
            GetTemplateListResponse ret = null;

            var cursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                ret = MyVrmService.Service.GetTemplateList(GetTemplateListResponse.SortByType.ByName);
            }
            catch (Exception exception)
            {
                MyVrmAddin.TraceSource.TraceException(exception, true);
                UIHelper.ShowMessage(exception.Message, MessageBoxButtons.OK,
                                         MessageBoxIcon.Exclamation);
            }
            finally
            {
                Cursor.Current = cursor;
            }

            if (ret != null && ret.templateList != null)
            {
                menu.Items.Clear();

                foreach (TemplateInfo templateInfo in ret.templateList)
                {
                    RibbonToggleButton rt = this.Factory.CreateRibbonToggleButton();
                    rt.Label = templateInfo.Name;
                    rt.Visible = true;
                    rt.Tag = templateInfo.ID;
                    rt.Enabled = true;
                    rt.Checked = selected == templateInfo.ID;
                    rt.Click += handler;

                    menu.Items.Add(rt);
                }
            }
        }

        public void DisEnable_AfterSave(object sender, EventArgs e)
        {
            var inspector = (Inspector)Context;
            var conferenceWrapper = Globals.ThisAddIn.GetConferenceWrapperForAppointment(
                    (AppointmentItem)inspector.CurrentItem, false, false);//ZD 101226
            if (conferenceWrapper != null)
                conferenceWrapper.AfterSave -= DisEnable_AfterSave;
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }
        //private void checkBox_Click(object sender, RibbonControlEventArgs e)
        //{
        //    GetTemplateResponse response = ConferenceRibbon.checkBox_OnClick(sender, selectedTemplateID);
        //    if (response != null)
        //    {
        //        //OutlookAppointmentImpl apptImpl;

        //        AppointmentItem appItem = (AppointmentItem)Globals.ThisAddIn.Application.CreateItem(OlItemType.olAppointmentItem);

        //        ThisAddIn.Sub_SetAppointmentParamsFromTemplate(appItem, response.Conference, Type.Missing);
        //    }
        //}

        private void optionsButton_Click(object sender, RibbonControlEventArgs ev)
        {
            try
            {
                BindingList<UsersDatasource> lstOutlookContacts = new BindingList<UsersDatasource>(); //Utils.GetOutlookContacts(Globals.ThisAddIn.Application);
                using (var optDlg = new NewOptionsDlg(lstOutlookContacts))
                {
                    optDlg.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                UIHelper.ShowError(ex.Message);
                MyVrmAddin.TraceSource.TraceException(ex);
            }
        }

        //private void conferenceButton_Click(object sender, RibbonControlEventArgs e)
        //{
        //    try
        //    {
        //        AppointmentItem appItem = (AppointmentItem) Globals.ThisAddIn.Application.CreateItem(OlItemType.olAppointmentItem);
        //        appItem.Display(Type.Missing);
        //        appItem.GetInspector.SetCurrentFormPage("TrueDayBookAddin2007.ConferenceFormRegion");
        //    }
        //    catch (Exception exception)
        //    {
        //        MyVrmAddin.TraceSource.TraceException(exception);
        //        UIHelper.ShowError(exception.Message);
        //    }
        //}

        //private void manageRoomButton_Click(object sender, RibbonControlEventArgs e)
        //{
        //    ConferenceRibbon.manageRoomButton_OnClick();
        //}

        private static bool UserHasStaticID()
        {
            User user = MyVrmService.Service.GetUser();
            return user.IsStaticIDEnabled;
        }

        //ZD 101226 Starts
        private void bnVirtualConference_Click(object sender, RibbonControlEventArgs e)
        {
           
            try
            {
                bool IsStaticId = false;
                AppointmentItem appItem = (AppointmentItem)Globals.ThisAddIn.Application.CreateItem(OlItemType.olAppointmentItem);
                if (UserHasStaticID())
                {
                    BindingList<UsersDatasource> lstOutlookContacts = new BindingList<UsersDatasource>(); //Utils.GetOutlookContacts(Globals.ThisAddIn.Application);//new BindingList<UsersDatasource>();
                    
                    MyVrm.Outlook.WinForms.Conference.InstantConference conferenceDialog = new InstantConference(lstOutlookContacts);
                    conferenceDialog.HideRecipients = true;
                    conferenceDialog.ShowDialog();
                    if (conferenceDialog.DialogResult == DialogResult.OK)
                    {
                        IsStaticId = conferenceDialog.IsStatic;
                    }
                    else
                        return;
                }
                MyVrmExplorer.Convert2TDBWrap(appItem, IsStaticId);
            }
            catch (Exception ex)
            {
                UIHelper.ShowError(ex.Message);
                MyVrmAddin.TraceSource.TraceException(ex);
            }
            //ZD 101226 Ends
        }

        private void bnCheckSeats_Click(object sender, RibbonControlEventArgs e)
        {
            bool cancel = false;
            MyVrmExplorer.CheckSeatsButtonClick(null, ref cancel);
        }


        private void tdCheckBox_Click(object sender, RibbonControlEventArgs e)
        {
            TDGetTemplateResponse response = ConferenceRibbon.tdCheckBox_OnClick(sender, selectedTemplateID);
            if (response != null)
            {
                AppointmentItem appItem = (AppointmentItem)Globals.ThisAddIn.Application.CreateItem(OlItemType.olAppointmentItem);
                MyVrmExplorer.Convert2TDBWrap(appItem, false);

                appItem.Body = string.IsNullOrEmpty(response.tdbTemplateInfo.Body) ? appItem.Body : response.tdbTemplateInfo.Body;
                appItem.Subject = string.IsNullOrEmpty(response.tdbTemplateInfo.Subj) ? appItem.Subject : response.tdbTemplateInfo.Subj;
                appItem.Duration = response.tdbTemplateInfo.Duration > 0 ? response.tdbTemplateInfo.Duration : appItem.Duration;

                //100337 - No need to reset start time to server time at all.
                //appItem.Start = response.tdbTemplateInfo.StartTime.TimeOfDay.Ticks > 0 ?
                //	appItem.Start.Date.AddTicks(response.tdbTemplateInfo.StartTime.TimeOfDay.Ticks) : appItem.Start;

                while (appItem.Recipients.Count > 0)
                    appItem.Recipients.Remove(appItem.Recipients[appItem.Recipients.Count].Index);

                if (response.tdbTemplateInfo.Participans != null)
                {

                    var participantsEmialList = response.tdbTemplateInfo.Participans.Select(participant => participant.Email).ToList();
                    foreach (string outlookRecipientMail in participantsEmialList)
                    {
                        Recipient recipient = appItem.Recipients.Add(outlookRecipientMail);
                        bool bret = recipient.Resolve();
                        if (!recipient.Resolved)
                        {
                            appItem.Recipients.Remove(recipient.Index);
                        }
                    }
                }
                //appItem.Display(Type.Missing);
                //ThisAddIn.Sub_SetAppointmentParamsFromTemplate(appItem, response.Conference, Type.Missing);
            }
        }

        private void btnInstanceConference_Click(object sender, RibbonControlEventArgs e)
        {
            try
            {
                BindingList<UsersDatasource> lstOutlookContacts = new BindingList<UsersDatasource>();//new BindingList<UsersDatasource>();
                MyVrm.Outlook.WinForms.Conference.InstantConference iConference = new InstantConference(lstOutlookContacts);
                iConference.EnableStatic = UserHasStaticID();
                iConference.Accepted += iConference_Accepted;
                DialogResult dlgResult = iConference.ShowDialog();
            }
            catch (Exception ex)
            {
                UIHelper.ShowError(ex.Message);
                MyVrmAddin.TraceSource.TraceException(ex);
            }
        }
        void iConference_Accepted(object sender, EventArgs e)
        {
            InstantConference iConference = sender as InstantConference;
            if (iConference == null)
                return;
            try
            {
                bool isStatic = iConference.IsStatic;
                bool immediateconf = iConference.immediateconf; //ZD 101226
                string recipients = iConference.Recipients;

                AppointmentItem appItem = (AppointmentItem)Globals.ThisAddIn.Application.CreateItem(OlItemType.olAppointmentItem);
                appItem.Start = DateTime.Now.AddMinutes(1.0);
                appItem.End = appItem.Start.AddMinutes(MyVrmService.Service.OrganizationOptions.DefaultConfDuration);//ZD 101749

                ConferenceRibbon.ConvertAppointment2Virtual(appItem, isStatic, immediateconf);//ZD 101226

                OutlookAppointment outlookAppt = Globals.ThisAddIn.Appointments[appItem] as OutlookAppointment;
                if (outlookAppt != null)
                    outlookAppt.RaiseSavingEvent = true;

                char[] splitChar = { ';' };
                string[] splitParticipants = recipients.Split(splitChar, StringSplitOptions.RemoveEmptyEntries);
                if (splitParticipants.Length > 0)
                {
                    appItem.MeetingStatus = OlMeetingStatus.olMeeting;
                    foreach (string strParticipant in splitParticipants)
                        appItem.Recipients.Add(strParticipant);
                    appItem.Send();
                }
                else
                {
                    appItem.MeetingStatus = OlMeetingStatus.olNonMeeting;
                    appItem.Save();
                    appItem.Close(OlInspectorClose.olSave);
                }
            }
            catch
            {
            }
            iConference.Close();
        }
      
    }
}
