﻿namespace MyVrmExcelImportTemplate
{
    partial class MyVrmRibbon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabMyVrmImport = new Microsoft.Office.Tools.Ribbon.RibbonTab();
            this.connectionGroup = new Microsoft.Office.Tools.Ribbon.RibbonGroup();
            this.webServiceUrlLabel = new Microsoft.Office.Tools.Ribbon.RibbonLabel();
            this.changeConnectionSettingsButton = new Microsoft.Office.Tools.Ribbon.RibbonButton();
            this.organizationGroup = new Microsoft.Office.Tools.Ribbon.RibbonGroup();
            this.organizationNameLabel = new Microsoft.Office.Tools.Ribbon.RibbonLabel();
            this.changeOrganizationButton = new Microsoft.Office.Tools.Ribbon.RibbonButton();
            this.dataGroup = new Microsoft.Office.Tools.Ribbon.RibbonGroup();
            this.publishButton = new Microsoft.Office.Tools.Ribbon.RibbonButton();
            this.helpGroup = new Microsoft.Office.Tools.Ribbon.RibbonGroup();
            this.aboutButton = new Microsoft.Office.Tools.Ribbon.RibbonButton();
            this.helpButton = new Microsoft.Office.Tools.Ribbon.RibbonButton();
            this.tabMyVrmImport.SuspendLayout();
            this.connectionGroup.SuspendLayout();
            this.organizationGroup.SuspendLayout();
            this.dataGroup.SuspendLayout();
            this.helpGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabMyVrmImport
            // 
            this.tabMyVrmImport.Groups.Add(this.connectionGroup);
            this.tabMyVrmImport.Groups.Add(this.organizationGroup);
            this.tabMyVrmImport.Groups.Add(this.dataGroup);
            this.tabMyVrmImport.Groups.Add(this.helpGroup);
            this.tabMyVrmImport.Label = "myVRM";
            this.tabMyVrmImport.Name = "tabMyVrmImport";
            // 
            // connectionGroup
            // 
            this.connectionGroup.Items.Add(this.webServiceUrlLabel);
            this.connectionGroup.Items.Add(this.changeConnectionSettingsButton);
            this.connectionGroup.Label = "Connection";
            this.connectionGroup.Name = "connectionGroup";
            // 
            // webServiceUrlLabel
            // 
            this.webServiceUrlLabel.Label = "[Web Service Url]";
            this.webServiceUrlLabel.Name = "webServiceUrlLabel";
            // 
            // changeConnectionSettingsButton
            // 
            this.changeConnectionSettingsButton.Label = "Change";
            this.changeConnectionSettingsButton.Name = "changeConnectionSettingsButton";
            this.changeConnectionSettingsButton.OfficeImageId = "ServerConnection";
            this.changeConnectionSettingsButton.ShowImage = true;
            this.changeConnectionSettingsButton.Click += new System.EventHandler<Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs>(this.changeConnectionSettingsButton_Click);
            // 
            // organizationGroup
            // 
            this.organizationGroup.Items.Add(this.organizationNameLabel);
            this.organizationGroup.Items.Add(this.changeOrganizationButton);
            this.organizationGroup.Label = "Organization";
            this.organizationGroup.Name = "organizationGroup";
            // 
            // organizationNameLabel
            // 
            this.organizationNameLabel.Label = "[Organization Name]";
            this.organizationNameLabel.Name = "organizationNameLabel";
            // 
            // changeOrganizationButton
            // 
            this.changeOrganizationButton.Label = "Change";
            this.changeOrganizationButton.Name = "changeOrganizationButton";
            this.changeOrganizationButton.OfficeImageId = "SmartArtOrganizationChartStandard";
            this.changeOrganizationButton.ShowImage = true;
            this.changeOrganizationButton.Click += new System.EventHandler<Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs>(this.changeOrganizationButton_Click);
            // 
            // dataGroup
            // 
            this.dataGroup.Items.Add(this.publishButton);
            this.dataGroup.Label = "Data";
            this.dataGroup.Name = "dataGroup";
            // 
            // publishButton
            // 
            this.publishButton.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.publishButton.Enabled = false;
            this.publishButton.Label = "Publish";
            this.publishButton.Name = "publishButton";
            this.publishButton.OfficeImageId = "ExportMoreMenu";
            this.publishButton.ShowImage = true;
            this.publishButton.Click += new System.EventHandler<Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs>(this.publishButton_Click);
            // 
            // helpGroup
            // 
            this.helpGroup.Items.Add(this.aboutButton);
            this.helpGroup.Items.Add(this.helpButton);
            this.helpGroup.Label = "Help";
            this.helpGroup.Name = "helpGroup";
            // 
            // aboutButton
            // 
            this.aboutButton.Label = "About";
            this.aboutButton.Name = "aboutButton";
            this.aboutButton.Click += new System.EventHandler<Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs>(this.aboutButton_Click);
            // 
            // helpButton
            // 
            this.helpButton.Label = "Help";
            this.helpButton.Name = "helpButton";
            this.helpButton.OfficeImageId = "Help";
            this.helpButton.ShowImage = true;
            // 
            // MyVrmRibbon
            // 
            this.Name = "MyVrmRibbon";
            this.RibbonType = "Microsoft.Excel.Workbook";
            this.Tabs.Add(this.tabMyVrmImport);
            this.Load += new System.EventHandler<Microsoft.Office.Tools.Ribbon.RibbonUIEventArgs>(this.MyVrmRibbon_Load);
            this.tabMyVrmImport.ResumeLayout(false);
            this.tabMyVrmImport.PerformLayout();
            this.connectionGroup.ResumeLayout(false);
            this.connectionGroup.PerformLayout();
            this.organizationGroup.ResumeLayout(false);
            this.organizationGroup.PerformLayout();
            this.dataGroup.ResumeLayout(false);
            this.dataGroup.PerformLayout();
            this.helpGroup.ResumeLayout(false);
            this.helpGroup.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tabMyVrmImport;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup dataGroup;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton publishButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup organizationGroup;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel organizationNameLabel;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton changeOrganizationButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup connectionGroup;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton changeConnectionSettingsButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel webServiceUrlLabel;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup helpGroup;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton aboutButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton helpButton;
    }

    partial class ThisRibbonCollection : Microsoft.Office.Tools.Ribbon.RibbonReadOnlyCollection
    {
        internal MyVrmRibbon MyVrmRibbon
        {
            get { return this.GetRibbon<MyVrmRibbon>(); }
        }
    }
}
