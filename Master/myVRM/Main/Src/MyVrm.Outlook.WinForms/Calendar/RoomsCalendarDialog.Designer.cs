﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.Outlook.WinForms.Calendar
{
    partial class RoomsCalendarDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
			if (disposing)
			{
				if (disposing && _updatingRooms != null)
					_updatingRooms.ListChanged -= UpdatingRoomsListChanged;
				if (disposing && _backgroundWorker != null)
				{
					_backgroundWorker.DoWork -= BgrWorker_DoWork;
					_backgroundWorker.ProgressChanged -= BgrWorker_ProgressChanged;
					_backgroundWorker.RunWorkerCompleted -= BgrWorker_RunWorkerCompleted;
				}
				//if (_progressDlg != null)
				//    _progressDlg.Closing -= ProgressDlg_Closing;
			}

        	if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraScheduler.TimeRuler timeRuler1 = new DevExpress.XtraScheduler.TimeRuler();
            DevExpress.XtraScheduler.TimeRuler timeRuler2 = new DevExpress.XtraScheduler.TimeRuler();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RoomsCalendarDialog));
            this.dataLayoutControl = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.updatingLabelControl = new DevExpress.XtraEditors.LabelControl();
            this.refreshCalendarButton = new DevExpress.XtraEditors.SimpleButton();
            this.roomsList = new DevExpress.XtraTreeList.TreeList();
            this.columnRoomName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.schedulerControl = new DevExpress.XtraScheduler.SchedulerControl();
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.viewSelectorBar1 = new DevExpress.XtraScheduler.UI.ViewSelectorBar();
            this.viewSelectorItem1 = new DevExpress.XtraScheduler.UI.ViewSelectorItem();
            this.viewSelectorItem2 = new DevExpress.XtraScheduler.UI.ViewSelectorItem();
            this.viewSelectorItem3 = new DevExpress.XtraScheduler.UI.ViewSelectorItem();
            this.viewSelectorItem4 = new DevExpress.XtraScheduler.UI.ViewSelectorItem();
            this.viewSelectorItem5 = new DevExpress.XtraScheduler.UI.ViewSelectorItem();
            this.viewNavigatorBar1 = new DevExpress.XtraScheduler.UI.ViewNavigatorBar();
            this.viewNavigatorBackwardItem1 = new DevExpress.XtraScheduler.UI.ViewNavigatorBackwardItem();
            this.viewNavigatorForwardItem1 = new DevExpress.XtraScheduler.UI.ViewNavigatorForwardItem();
            this.viewNavigatorTodayItem1 = new DevExpress.XtraScheduler.UI.ViewNavigatorTodayItem();
            //this.viewNavigatorZoomInItem1 = new DevExpress.XtraScheduler.UI.ViewNavigatorZoomInItem();
            //this.viewNavigatorZoomOutItem1 = new DevExpress.XtraScheduler.UI.ViewNavigatorZoomOutItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.schedulerStorage = new DevExpress.XtraScheduler.SchedulerStorage(this.components);
            this.toolTipController = new DevExpress.Utils.ToolTipController(this.components);
            this.dateNavigator = new DevExpress.XtraScheduler.DateNavigator();
            this.LegendAVConference = new DevExpress.XtraEditors.LabelControl();
            this.LabelAVConference = new DevExpress.XtraEditors.LabelControl();
            this.LegendRoomConference = new DevExpress.XtraEditors.LabelControl();
            this.LabelRoomConference = new DevExpress.XtraEditors.LabelControl();
            this.LegendAVOnlyConference = new DevExpress.XtraEditors.LabelControl();
            this.LabelAVOnlyConference = new DevExpress.XtraEditors.LabelControl();
            this.LegendP2PConference = new DevExpress.XtraEditors.LabelControl();
            this.LabelP2PConference = new DevExpress.XtraEditors.LabelControl();
            this.LegendVMRConference = new DevExpress.XtraEditors.LabelControl();
            this.LabelVMRConference = new DevExpress.XtraEditors.LabelControl();
            this.LegendHotdeskingConference = new DevExpress.XtraEditors.LabelControl();
            this.LabelHotdeskingConference = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.roomListLayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.updatingLabelControlLayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).BeginInit();
            this.ContentPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl)).BeginInit();
            this.dataLayoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.roomsList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateNavigator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.roomListLayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.updatingLabelControlLayoutItem)).BeginInit();
            this.SuspendLayout();
            // 
            // ContentPanel
            // 
            this.ContentPanel.Controls.Add(this.dataLayoutControl);
            this.ContentPanel.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.ContentPanel.Size = new System.Drawing.Size(760, 477);
            // 
            // dataLayoutControl
            // 
            this.dataLayoutControl.AllowCustomizationMenu = false;
            this.dataLayoutControl.Controls.Add(this.updatingLabelControl);
            this.dataLayoutControl.Controls.Add(this.refreshCalendarButton);
            this.dataLayoutControl.Controls.Add(this.roomsList);
            this.dataLayoutControl.Controls.Add(this.schedulerControl);
            this.dataLayoutControl.Controls.Add(this.dateNavigator);
            this.dataLayoutControl.Controls.Add(this.LegendAVConference);
            this.dataLayoutControl.Controls.Add(this.LabelAVConference);
            this.dataLayoutControl.Controls.Add(this.LegendRoomConference);
            this.dataLayoutControl.Controls.Add(this.LabelRoomConference);
            this.dataLayoutControl.Controls.Add(this.LegendAVOnlyConference);
            this.dataLayoutControl.Controls.Add(this.LabelAVOnlyConference);
            this.dataLayoutControl.Controls.Add(this.LegendP2PConference);
            this.dataLayoutControl.Controls.Add(this.LabelP2PConference);
            this.dataLayoutControl.Controls.Add(this.LegendVMRConference);
            this.dataLayoutControl.Controls.Add(this.LabelVMRConference);
            this.dataLayoutControl.Controls.Add(this.LegendHotdeskingConference);
            this.dataLayoutControl.Controls.Add(this.LabelHotdeskingConference);
            this.dataLayoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutControl.Name = "dataLayoutControl";
            this.dataLayoutControl.Root = this.layoutControlGroup;
            this.dataLayoutControl.Size = new System.Drawing.Size(860, 608);
            this.dataLayoutControl.TabIndex = 0;
            this.dataLayoutControl.Text = "dataLayoutControl1";
            // 
            // updatingLabelControl
            // 
            this.updatingLabelControl.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.updatingLabelControl.Appearance.BorderColor = System.Drawing.SystemColors.ActiveBorder;
            this.updatingLabelControl.Appearance.Image = global::MyVrm.Outlook.WinForms.Properties.Resources.information;
            this.updatingLabelControl.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.updatingLabelControl.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.updatingLabelControl.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.updatingLabelControl.Location = new System.Drawing.Point(2, 2);
            this.updatingLabelControl.Name = "updatingLabelControl";
            this.updatingLabelControl.Size = new System.Drawing.Size(856, 18);
            this.updatingLabelControl.StyleController = this.dataLayoutControl;
            this.updatingLabelControl.TabIndex = 8;
            this.updatingLabelControl.Text = "[Updating...]";
            // 
            // refreshCalendarButton
            // 
            this.refreshCalendarButton.Location = new System.Drawing.Point(69, 584);
            this.refreshCalendarButton.Name = "refreshCalendarButton";
            this.refreshCalendarButton.Size = new System.Drawing.Size(103, 22);
            this.refreshCalendarButton.StyleController = this.dataLayoutControl;
            this.refreshCalendarButton.TabIndex = 7;
            this.refreshCalendarButton.Text = "[Refresh Calendar]";
            this.refreshCalendarButton.Click += new System.EventHandler(this.refreshCalendarButton_Click);
            // 
            // roomsList
            // 
            this.roomsList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.columnRoomName});
            this.roomsList.Location = new System.Drawing.Point(2, 310);
            this.roomsList.Name = "roomsList";
            this.roomsList.OptionsSelection.MultiSelect = true;
            this.roomsList.OptionsView.ShowCheckBoxes = true;
            this.roomsList.OptionsView.ShowHorzLines = false;
            this.roomsList.OptionsView.ShowIndicator = false;
            this.roomsList.OptionsView.ShowRoot = false;
            this.roomsList.OptionsView.ShowVertLines = false;
            this.roomsList.Size = new System.Drawing.Size(170, 270);
            this.roomsList.TabIndex = 6;
            // 
            // columnRoomName
            // 
            this.columnRoomName.Caption = "[Name]";
            this.columnRoomName.FieldName = "Name";
            this.columnRoomName.MinWidth = 32;
            this.columnRoomName.Name = "columnRoomName";
            this.columnRoomName.OptionsColumn.AllowEdit = false;
            this.columnRoomName.OptionsColumn.AllowMove = false;
            this.columnRoomName.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.columnRoomName.OptionsColumn.ReadOnly = true;
            this.columnRoomName.OptionsColumn.ShowInCustomizationForm = false;
            this.columnRoomName.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.columnRoomName.Visible = true;
            this.columnRoomName.VisibleIndex = 0;
            // 
            // schedulerControl
            // 
            this.schedulerControl.ActiveViewType = DevExpress.XtraScheduler.SchedulerViewType.WorkWeek;
            this.schedulerControl.GroupType = DevExpress.XtraScheduler.SchedulerGroupType.Resource;
            this.schedulerControl.Location = new System.Drawing.Point(176, 86);
            this.schedulerControl.MaximumSize = new System.Drawing.Size(682, 480);
            this.schedulerControl.MenuManager = this.barManager;
            this.schedulerControl.MinimumSize = new System.Drawing.Size(682, 480);
            this.schedulerControl.Name = "schedulerControl";
            this.schedulerControl.OptionsCustomization.AllowAppointmentCopy = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControl.OptionsCustomization.AllowAppointmentCreate = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControl.OptionsCustomization.AllowAppointmentDelete = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControl.OptionsCustomization.AllowAppointmentDrag = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControl.OptionsCustomization.AllowAppointmentDragBetweenResources = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControl.OptionsCustomization.AllowAppointmentEdit = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControl.OptionsCustomization.AllowAppointmentMultiSelect = false;
            this.schedulerControl.OptionsCustomization.AllowAppointmentResize = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControl.OptionsCustomization.AllowDisplayAppointmentForm = DevExpress.XtraScheduler.AllowDisplayAppointmentForm.Never;
            this.schedulerControl.OptionsCustomization.AllowInplaceEditor = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControl.OptionsView.ShowOnlyResourceAppointments = true;
            this.schedulerControl.OptionsView.ToolTipVisibility = DevExpress.XtraScheduler.ToolTipVisibility.Always;
            this.schedulerControl.ResourceNavigator.Buttons.NextPage.Visible = false;
            this.schedulerControl.ResourceNavigator.Buttons.PrevPage.Visible = false;
            this.schedulerControl.Size = new System.Drawing.Size(682, 480);
            this.schedulerControl.Start = new System.DateTime(2015, 2, 23, 0, 0, 0, 0);
            this.schedulerControl.Storage = this.schedulerStorage;
            this.schedulerControl.TabIndex = 4;
            this.schedulerControl.Text = "schedulerControl1";
            this.schedulerControl.ToolTipController = this.toolTipController;
            this.schedulerControl.Views.DayView.ResourcesPerPage = 3;
            this.schedulerControl.Views.DayView.ShowMoreButtons = false;
            this.schedulerControl.Views.DayView.TimeRulers.Add(timeRuler1);
            this.schedulerControl.Views.MonthView.ResourcesPerPage = 3;
            this.schedulerControl.Views.MonthView.ShowMoreButtons = false;
            this.schedulerControl.Views.TimelineView.Enabled = false;
            this.schedulerControl.Views.TimelineView.ResourcesPerPage = 3;
            this.schedulerControl.Views.TimelineView.ShowMoreButtons = false;
            this.schedulerControl.Views.WeekView.ResourcesPerPage = 3;
            this.schedulerControl.Views.WeekView.ShowMoreButtons = false;
            this.schedulerControl.Views.WorkWeekView.ResourcesPerPage = 3;
            this.schedulerControl.Views.WorkWeekView.ShowMoreButtons = false;
            this.schedulerControl.Views.WorkWeekView.TimeRulers.Add(timeRuler2);
            this.schedulerControl.VisibleIntervalChanged += new System.EventHandler(this.schedulerControl_VisibleIntervalChanged);
            // 
            // barManager
            // 
            this.barManager.AllowCustomization = false;
            this.barManager.AllowMoveBarOnToolbar = false;
            this.barManager.AllowQuickCustomization = false;
            this.barManager.AllowShowToolbarsPopup = false;
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.viewSelectorBar1,
            this.viewNavigatorBar1});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.viewSelectorItem1,
            this.viewSelectorItem2,
            this.viewSelectorItem3,
            this.viewSelectorItem4,
            this.viewSelectorItem5,
            this.viewNavigatorBackwardItem1,
            this.viewNavigatorForwardItem1,
            this.viewNavigatorTodayItem1});
            //this.viewNavigatorZoomInItem1,
            //this.viewNavigatorZoomOutItem1
            this.barManager.MaxItemId = 49;
            // 
            // viewSelectorBar1
            // 
            this.viewSelectorBar1.BarName = "";
            this.viewSelectorBar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this.viewSelectorBar1.DockCol = 7;
            this.viewSelectorBar1.DockRow = 0;
            this.viewSelectorBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.viewSelectorBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.viewSelectorItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewSelectorItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewSelectorItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewSelectorItem4),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewSelectorItem5)});
            this.viewSelectorBar1.OptionsBar.DrawDragBorder = false;
            this.viewSelectorBar1.Text = "";
            // 
            // viewSelectorItem1
            // 
            this.viewSelectorItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem1.Glyph")));
            this.viewSelectorItem1.GroupIndex = 1;
            this.viewSelectorItem1.Id = 0;
            this.viewSelectorItem1.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
                | System.Windows.Forms.Keys.D1));
            this.viewSelectorItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem1.LargeGlyph")));
            this.viewSelectorItem1.Name = "viewSelectorItem1";
            this.viewSelectorItem1.SchedulerViewType = DevExpress.XtraScheduler.SchedulerViewType.Day;
            schedulerControl.ActiveViewType = DevExpress.XtraScheduler.SchedulerViewType.Day;
            this.viewSelectorItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.schedulerControl1_ViewtypeChanged);
            // 
            // viewSelectorItem2
            // 
            this.viewSelectorItem2.Checked = true;
            this.viewSelectorItem2.Glyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem2.Glyph")));
            this.viewSelectorItem2.GroupIndex = 1;
            this.viewSelectorItem2.Id = 1;
            this.viewSelectorItem2.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
                | System.Windows.Forms.Keys.D2));
            this.viewSelectorItem2.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem2.LargeGlyph")));
            this.viewSelectorItem2.Name = "viewSelectorItem2";
            this.viewSelectorItem2.SchedulerViewType = DevExpress.XtraScheduler.SchedulerViewType.WorkWeek;
            schedulerControl.ActiveViewType = DevExpress.XtraScheduler.SchedulerViewType.WorkWeek;
            this.viewSelectorItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.schedulerControl2_ViewtypeChanged);
            // 
            // viewSelectorItem3
            // 
            this.viewSelectorItem3.Glyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem3.Glyph")));
            this.viewSelectorItem3.GroupIndex = 1;
            this.viewSelectorItem3.Id = 2;
            this.viewSelectorItem3.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
                | System.Windows.Forms.Keys.D3));
            this.viewSelectorItem3.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem3.LargeGlyph")));
            this.viewSelectorItem3.Name = "viewSelectorItem3";
            this.viewSelectorItem3.SchedulerViewType = DevExpress.XtraScheduler.SchedulerViewType.Week;
            schedulerControl.ActiveViewType = DevExpress.XtraScheduler.SchedulerViewType.Week;
            //this.schedulerControl.VisibleIntervalChanged += new System.EventHandler(this.schedulerControl_VisibleIntervalChanged);
            this.viewSelectorItem3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.schedulerControl3_ViewtypeChanged);
            
            // 
            // viewSelectorItem4
            // 
            this.viewSelectorItem4.Glyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem4.Glyph")));
            this.viewSelectorItem4.GroupIndex = 1;
            this.viewSelectorItem4.Id = 3;
            this.viewSelectorItem4.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
                | System.Windows.Forms.Keys.D4));
            this.viewSelectorItem4.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem4.LargeGlyph")));
            this.viewSelectorItem4.Name = "viewSelectorItem4";
            this.viewSelectorItem4.SchedulerViewType = DevExpress.XtraScheduler.SchedulerViewType.Month;
            schedulerControl.ActiveViewType = DevExpress.XtraScheduler.SchedulerViewType.Month;
            this.viewSelectorItem4.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.schedulerControl4_ViewtypeChanged);
          
            // 
            // viewSelectorItem5
            // 
            this.viewSelectorItem5.Glyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem5.Glyph")));
            this.viewSelectorItem5.GroupIndex = 1;
            this.viewSelectorItem5.Id = 4;
            this.viewSelectorItem5.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
                | System.Windows.Forms.Keys.D5));
            this.viewSelectorItem5.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem5.LargeGlyph")));
            this.viewSelectorItem5.Name = "viewSelectorItem5";
            this.viewSelectorItem5.SchedulerViewType = DevExpress.XtraScheduler.SchedulerViewType.Timeline;
            schedulerControl.ActiveViewType = DevExpress.XtraScheduler.SchedulerViewType.Timeline;
            this.viewSelectorItem5.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.schedulerControl5_ViewtypeChanged);
        
            // 
            // viewNavigatorBar1
            // 
            this.viewNavigatorBar1.BarName = "";
            this.viewNavigatorBar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this.viewNavigatorBar1.DockCol = 6;
            this.viewNavigatorBar1.DockRow = 0;
            this.viewNavigatorBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.viewNavigatorBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.viewNavigatorBackwardItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewNavigatorForwardItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewNavigatorTodayItem1)});
            //new DevExpress.XtraBars.LinkPersistInfo(this.viewNavigatorZoomInItem1),
            //new DevExpress.XtraBars.LinkPersistInfo(this.viewNavigatorZoomOutItem1)});
            this.viewNavigatorBar1.OptionsBar.DrawDragBorder = false;
            this.viewNavigatorBar1.Text = "";
            // 
            // viewNavigatorBackwardItem1
            // 
            this.viewNavigatorBackwardItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorBackwardItem1.Glyph")));
            this.viewNavigatorBackwardItem1.GroupIndex = 1;
            this.viewNavigatorBackwardItem1.Id = 5;
            this.viewNavigatorBackwardItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorBackwardItem1.LargeGlyph")));
            this.viewNavigatorBackwardItem1.Name = "viewNavigatorBackwardItem1";
            //this.viewNavigatorBackwardItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(viewNavigatorBackwardItem1_ItemClick);
            this.viewNavigatorBackwardItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.viewNavigatorBackwardItem1_ItemClick);//102803
            //this.viewNavigatorBackwardItem1.VisibleIntervalChanged += new System.EventHandler(this.schedulerControl_VisibleIntervalChanged);
            //this.viewNavigatorBackwardItem1. += new System.EventHandler(this.schedulerControl_VisibleIntervalChanged);

           // this.viewNavigatorBackwardItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.schedulerControl7_ViewtypeChanged);

            // 
            // viewNavigatorForwardItem1
            // 
            this.viewNavigatorForwardItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorForwardItem1.Glyph")));
            this.viewNavigatorForwardItem1.GroupIndex = 1;
            this.viewNavigatorForwardItem1.Id = 6;
            this.viewNavigatorForwardItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorForwardItem1.LargeGlyph")));
            this.viewNavigatorForwardItem1.Name = "viewNavigatorForwardItem1";
            //this.viewNavigatorForwardItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(viewNavigatorForwardItem1_ItemClick);
            this.viewNavigatorForwardItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.viewNavigatorForwardItem1_ItemClick);//102803
            //this.viewNavigatorForwardItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.schedulerControl8_ViewtypeChanged);
            //this.viewNavigatorForwardItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.schedulerControl_VisibleIntervalChanged);
            //viewNavigatorBackwardItem1
            // 
            // viewNavigatorTodayItem1
            // 
            this.viewNavigatorTodayItem1.Caption = "";
            this.viewNavigatorTodayItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorTodayItem1.Glyph")));
            this.viewNavigatorTodayItem1.GroupIndex = 1;
            this.viewNavigatorTodayItem1.Id = 7;
            this.viewSelectorItem5.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
               | System.Windows.Forms.Keys.D5));
            this.viewNavigatorTodayItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorTodayItem1.LargeGlyph")));
            this.viewNavigatorTodayItem1.Name = "viewNavigatorTodayItem1";
            schedulerControl.ActiveViewType = DevExpress.XtraScheduler.SchedulerViewType.Day;
            this.viewNavigatorTodayItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.schedulerControl6_ViewtypeChanged);
            schedulerControl.ActiveViewType = DevExpress.XtraScheduler.SchedulerViewType.WorkWeek;


            //this.viewSelectorItem5.Glyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem5.Glyph")));
            //this.viewSelectorItem5.GroupIndex = 1;
            //this.viewSelectorItem5.Id = 4;
            //this.viewSelectorItem5.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            //    | System.Windows.Forms.Keys.D5));
            //this.viewSelectorItem5.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem5.LargeGlyph")));
            //this.viewSelectorItem5.Name = "viewSelectorItem5";
            //this.viewSelectorItem5.SchedulerViewType = DevExpress.XtraScheduler.SchedulerViewType.Timeline;
            //schedulerControl.ActiveViewType = DevExpress.XtraScheduler.SchedulerViewType.Timeline;
            //this.viewSelectorItem5.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.schedulerControl5_ViewtypeChanged);
            
            // 
            // viewNavigatorZoomInItem1
            // 
            //this.viewNavigatorZoomInItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorZoomInItem1.Glyph")));
            //this.viewNavigatorZoomInItem1.GroupIndex = 1;
            //this.viewNavigatorZoomInItem1.Id = 8;
            //this.viewNavigatorZoomInItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorZoomInItem1.LargeGlyph")));
            //this.viewNavigatorZoomInItem1.Name = "viewNavigatorZoomInItem1";
            //// 
            //// viewNavigatorZoomOutItem1
            //// 
            //this.viewNavigatorZoomOutItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorZoomOutItem1.Glyph")));
            //this.viewNavigatorZoomOutItem1.GroupIndex = 1;
            //this.viewNavigatorZoomOutItem1.Id = 9;
            //this.viewNavigatorZoomOutItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorZoomOutItem1.LargeGlyph")));
            //this.viewNavigatorZoomOutItem1.Name = "viewNavigatorZoomOutItem1";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(884, 31);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 664);
            this.barDockControlBottom.Size = new System.Drawing.Size(884, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 31);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 633);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(884, 31);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 633);
            // 
            // toolTipController
            // 
            this.toolTipController.AllowHtmlText = true;
            this.toolTipController.ToolTipType = DevExpress.Utils.ToolTipType.SuperTip;
            this.toolTipController.BeforeShow += new DevExpress.Utils.ToolTipControllerBeforeShowEventHandler(this.toolTipController_BeforeShow);
            // 
            // dateNavigator
            // 
            this.dateNavigator.HotDate = null;
            this.dateNavigator.Location = new System.Drawing.Point(2, 26);
            this.dateNavigator.Name = "dateNavigator";
            this.dateNavigator.SchedulerControl = this.schedulerControl;
            this.dateNavigator.Size = new System.Drawing.Size(170, 250);
            this.dateNavigator.TabIndex = 5;
            // 
            // LegendAVConference
            // 
            this.LegendAVConference.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(182)))), ((int)(((byte)(255)))));
            this.LegendAVConference.Location = new System.Drawing.Point(179, 25);
            this.LegendAVConference.Name = "Legendlabel";
            this.LegendAVConference.Size = new System.Drawing.Size(25, 18);
            this.LegendAVConference.StyleController = this.dataLayoutControl;
            this.LegendAVConference.TabIndex = 9;
            // 
            // LabelAVConference
            // 
            this.LabelAVConference.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.LabelAVConference.Location = new System.Drawing.Point(210, 25);
            this.LabelAVConference.MinimumSize = new System.Drawing.Size(140, 18);
            this.LabelAVConference.Name = "label1";
            this.LabelAVConference.Size = new System.Drawing.Size(140, 18);
            this.LabelAVConference.StyleController = this.dataLayoutControl;
            this.LabelAVConference.TabIndex = 10;
            this.LabelAVConference.Text = "Audio/Video Conference";
            // 
            // LegendRoomConference
            // 
            this.LegendRoomConference.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(105)))), ((int)(((byte)(82)))));
            this.LegendRoomConference.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.LegendRoomConference.Location = new System.Drawing.Point(350, 25);
            this.LegendRoomConference.Name = "Legendlabel2";
            this.LegendRoomConference.Size = new System.Drawing.Size(25, 18);
            this.LegendRoomConference.StyleController = this.dataLayoutControl;
            this.LegendRoomConference.TabIndex = 11;
            // 
            // LabelRoomConference
            // 
            this.LabelRoomConference.Location = new System.Drawing.Point(380, 25);
            this.LabelRoomConference.MinimumSize = new System.Drawing.Size(140, 18);
            this.LabelRoomConference.Name = "label2";
            this.LabelRoomConference.Size = new System.Drawing.Size(140, 18);
            this.LabelRoomConference.StyleController = this.dataLayoutControl;
            this.LabelRoomConference.TabIndex = 12;
            this.LabelRoomConference.Text = "Room Conference";
            // 
            // LegendAVOnlyConference
            // 
            this.LegendAVOnlyConference.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(162)))), ((int)(((byte)(214)))));
            this.LegendAVOnlyConference.Location = new System.Drawing.Point(520, 25);
            this.LegendAVOnlyConference.Name = "LegendAVOnlyConference";
            this.LegendAVOnlyConference.Size = new System.Drawing.Size(25, 18);
            this.LegendAVOnlyConference.StyleController = this.dataLayoutControl;
            this.LegendAVOnlyConference.TabIndex = 13;
            // 
            // LabelAVOnlyConference
            // 
            this.LabelAVOnlyConference.Location = new System.Drawing.Point(550, 25);
            this.LabelAVOnlyConference.MinimumSize = new System.Drawing.Size(130, 18);
            this.LabelAVOnlyConference.Name = "LabelAVOnlyConference";
            this.LabelAVOnlyConference.Size = new System.Drawing.Size(130, 18);
            this.LabelAVOnlyConference.StyleController = this.dataLayoutControl;
            this.LabelAVOnlyConference.TabIndex = 14;
            this.LabelAVOnlyConference.Text = "Audio-Only Conference";
            // 
            // LegendP2PConference
            // 
            this.LegendP2PConference.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(239)))), ((int)(((byte)(156)))));
            this.LegendP2PConference.Location = new System.Drawing.Point(680, 25);
            this.LegendP2PConference.Name = "LegendP2PConference";
            this.LegendP2PConference.Size = new System.Drawing.Size(25, 18);
            this.LegendP2PConference.StyleController = this.dataLayoutControl;
            this.LegendP2PConference.TabIndex = 15;
            // 
            // LabelP2PConference
            // 
            this.LabelP2PConference.Location = new System.Drawing.Point(710, 25);
            this.LabelP2PConference.MinimumSize = new System.Drawing.Size(170, 18);
            this.LabelP2PConference.Name = "LabelP2PConference";
            this.LabelP2PConference.Size = new System.Drawing.Size(170, 18);
            this.LabelP2PConference.StyleController = this.dataLayoutControl;
            this.LabelP2PConference.TabIndex = 16;
            this.LabelP2PConference.Text = "Point-to-Point Conference";
            // 
            // LegendVMRConference
            // 
            this.LegendVMRConference.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(202)))), ((int)(((byte)(255)))));
            this.LegendVMRConference.Location = new System.Drawing.Point(179, 50);
            this.LegendVMRConference.Name = "LegendVMRConference";
            this.LegendVMRConference.Size = new System.Drawing.Size(25, 18);
            this.LegendVMRConference.StyleController = this.dataLayoutControl;
            this.LegendVMRConference.TabIndex = 17;
            // 
            // LabelVMRConference
            // 
            this.LabelVMRConference.Location = new System.Drawing.Point(210, 50);
            this.LabelVMRConference.Name = "LabelVMRConference";
            this.LabelVMRConference.Size = new System.Drawing.Size(140, 18);
            this.LabelVMRConference.StyleController = this.dataLayoutControl;
            this.LabelVMRConference.TabIndex = 18;
            this.LabelVMRConference.Text = "Virtual Meeting Conference";
            // 
            // LegendHotdeskingConference
            // 
            this.LegendHotdeskingConference.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(1)))), ((int)(((byte)(51)))));
            this.LegendHotdeskingConference.Location = new System.Drawing.Point(350, 50);
            this.LegendHotdeskingConference.Name = "LegendHotdeskingConference";
            this.LegendHotdeskingConference.Size = new System.Drawing.Size(25, 18);
            this.LegendHotdeskingConference.StyleController = this.dataLayoutControl;
            this.LegendHotdeskingConference.TabIndex = 19;
            // 
            // LabelHotdeskingConference
            // 
            this.LabelHotdeskingConference.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftTop;
            this.LabelHotdeskingConference.Location = new System.Drawing.Point(380, 50);
            this.LabelHotdeskingConference.Name = "LabelHotdeskingConference";
            this.LabelHotdeskingConference.Size = new System.Drawing.Size(160, 18);
            this.LabelHotdeskingConference.StyleController = this.dataLayoutControl;
            this.LabelHotdeskingConference.TabIndex = 20;
            this.LabelHotdeskingConference.Text = "Hotdesking Conference";
            // 
            // layoutControlGroup
            // 
            this.layoutControlGroup.CustomizationFormText = "layoutControlGroup";
            this.layoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup.GroupBordersVisible = false;
            this.layoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.roomListLayoutItem,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.emptySpaceItem1,
            this.updatingLabelControlLayoutItem});
            this.layoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup.Name = "layoutControlGroup";
            this.layoutControlGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup.Size = new System.Drawing.Size(860, 608);
            this.layoutControlGroup.Text = "layoutControlGroup";
            this.layoutControlGroup.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.schedulerControl;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(174, 92);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(686, 584);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // roomListLayoutItem
            // 
            this.roomListLayoutItem.Control = this.roomsList;
            this.roomListLayoutItem.CustomizationFormText = "[Rooms]";
            this.roomListLayoutItem.Location = new System.Drawing.Point(0, 292);
            this.roomListLayoutItem.Name = "roomListLayoutItem";
            this.roomListLayoutItem.Size = new System.Drawing.Size(174, 290);
            this.roomListLayoutItem.Text = "[Rooms]";
            this.roomListLayoutItem.TextLocation = DevExpress.Utils.Locations.Top;
            this.roomListLayoutItem.TextSize = new System.Drawing.Size(40, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.dateNavigator;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(174, 268);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.refreshCalendarButton;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(67, 582);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(107, 26);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 582);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(67, 26);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // updatingLabelControlLayoutItem
            // 
            this.updatingLabelControlLayoutItem.Control = this.updatingLabelControl;
            this.updatingLabelControlLayoutItem.CustomizationFormText = "layoutControlItem3";
            this.updatingLabelControlLayoutItem.Location = new System.Drawing.Point(0, 0);
            this.updatingLabelControlLayoutItem.Name = "updatingLabelControlLayoutItem";
            this.updatingLabelControlLayoutItem.Size = new System.Drawing.Size(860, 24);
            this.updatingLabelControlLayoutItem.Text = "updatingLabelControlLayoutItem";
            this.updatingLabelControlLayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.updatingLabelControlLayoutItem.TextToControlDistance = 0;
            this.updatingLabelControlLayoutItem.TextVisible = false;
            // 
            // RoomsCalendarDialog
            // 
            this.ApplyEnabled = true;
            this.ApplyVisible = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(884, 664);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = true;
            this.MinimumSize = new System.Drawing.Size(792, 592);
            this.Name = "RoomsCalendarDialog";
            this.Load += new System.EventHandler(this.RoomsCalendarDialog_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            ((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).EndInit();
            this.ContentPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl)).EndInit();
            this.dataLayoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.roomsList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateNavigator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.roomListLayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.updatingLabelControlLayoutItem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup;
        private DevExpress.XtraScheduler.SchedulerControl schedulerControl;
        private DevExpress.XtraScheduler.SchedulerStorage schedulerStorage;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraScheduler.DateNavigator dateNavigator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraScheduler.UI.ViewSelectorBar viewSelectorBar1;
        private DevExpress.XtraScheduler.UI.ViewSelectorItem viewSelectorItem1;
        private DevExpress.XtraScheduler.UI.ViewSelectorItem viewSelectorItem2;
        private DevExpress.XtraScheduler.UI.ViewSelectorItem viewSelectorItem3;
        private DevExpress.XtraScheduler.UI.ViewSelectorItem viewSelectorItem4;
        private DevExpress.XtraScheduler.UI.ViewSelectorItem viewSelectorItem5;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
       // private DevExpress.XtraScheduler.UI.ViewSelector viewSelector;
        private DevExpress.XtraScheduler.UI.ViewNavigatorBar viewNavigatorBar1;
        private DevExpress.XtraScheduler.UI.ViewNavigatorBackwardItem viewNavigatorBackwardItem1;
        private DevExpress.XtraScheduler.UI.ViewNavigatorForwardItem viewNavigatorForwardItem1;
        private DevExpress.XtraScheduler.UI.ViewNavigatorTodayItem viewNavigatorTodayItem1;
        //private DevExpress.XtraScheduler.UI.ViewNavigatorZoomInItem viewNavigatorZoomInItem1;
        //private DevExpress.XtraScheduler.UI.ViewNavigatorZoomOutItem viewNavigatorZoomOutItem1;
        //private DevExpress.XtraScheduler.UI.ViewNavigator viewNavigator;
        private DevExpress.XtraTreeList.TreeList roomsList;
        private DevExpress.XtraLayout.LayoutControlItem roomListLayoutItem;
        private DevExpress.XtraTreeList.Columns.TreeListColumn columnRoomName;
        private DevExpress.Utils.ToolTipController toolTipController;
        private DevExpress.XtraEditors.SimpleButton refreshCalendarButton;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.LabelControl updatingLabelControl;
        private DevExpress.XtraLayout.LayoutControlItem updatingLabelControlLayoutItem;
        private DevExpress.XtraEditors.LabelControl LegendAVConference;
        private DevExpress.XtraEditors.LabelControl LabelAVConference;
        private DevExpress.XtraEditors.LabelControl LegendRoomConference;
        private DevExpress.XtraEditors.LabelControl LabelRoomConference;
        private DevExpress.XtraEditors.LabelControl LegendAVOnlyConference;
        private DevExpress.XtraEditors.LabelControl LabelAVOnlyConference;
        private DevExpress.XtraEditors.LabelControl LegendP2PConference;
        private DevExpress.XtraEditors.LabelControl LabelP2PConference;
        private DevExpress.XtraEditors.LabelControl LegendVMRConference;
        private DevExpress.XtraEditors.LabelControl LabelVMRConference;
        private DevExpress.XtraEditors.LabelControl LegendHotdeskingConference;
        private DevExpress.XtraEditors.LabelControl LabelHotdeskingConference;
        
    }
}
