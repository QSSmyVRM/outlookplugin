﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Microsoft.Office.Interop.Outlook;

namespace MyVrm.Outlook
{
    public class CustomFormKeyedByWindowCollection : ReadOnlyCollection<CustomFormControl>
    {
        private readonly Dictionary<object, IList<CustomFormControl>> _formsByWindow = new Dictionary<object, IList<CustomFormControl>>();
        private readonly Dictionary<CustomFormControl, object> _formToWindow = new Dictionary<CustomFormControl, object>();


        public CustomFormKeyedByWindowCollection() : base(new List<CustomFormControl>())
        {
        }

        public void Add(CustomFormControl form, object key)
        {
            if (form == null) 
                throw new ArgumentNullException("form");
            if (key == null) 
                throw new ArgumentNullException("key");
            if (_formToWindow.ContainsKey(form))
            {
                throw new ArgumentException(string.Empty, "form");
            }
            Items.Add(form);
            AddToMultiFormDictionary(form, key, _formsByWindow);
            _formToWindow[form] = key;
        }

        public bool Remove(CustomFormControl item)
        {
            var removed = Items.Remove(item);
            if (removed)
            {
                var keyPairForItem = GetKeyPairForItem(item);
                _formToWindow.Remove(item);
                _formsByWindow.Remove(keyPairForItem.Key);
            }
            return removed;
        }

        public bool TryGetValue(Inspector inspector, out IList<CustomFormControl> value)
        {
            return _formsByWindow.TryGetValue(inspector, out value);
        }

        private void AddToMultiFormDictionary(CustomFormControl form, object key, IDictionary<object, IList<CustomFormControl>> dictionary)
        {
            IList<CustomFormControl> list = null;
            if (!dictionary.TryGetValue(key, out list))
            {
                list = new List<CustomFormControl>();
                dictionary.Add(key, list);
            }
            list.Add(form);
        }

        private KeyValuePair<object, CustomFormControl> GetKeyPairForItem(CustomFormControl item)
        {
            return new KeyValuePair<object, CustomFormControl>(_formToWindow[item], item);
        }
    }
}
