﻿using Microsoft.Office.Interop.Excel.Extensions;
using Microsoft.Office.Tools.Excel;
using MyVrm.DataImport.Excel.Extensions;
using MyVrm.WebServices.Data;

namespace MyVrm.DataImport.Excel
{
    public class SmtpTable : ExcelTable
    {
        private readonly ExcelTableColumn _serverIpAddressColumn;
        private readonly ExcelTableColumn _loginColumn;
        private readonly ExcelTableColumn _passwordColumn;
        private readonly ExcelTableColumn _portColumn;
        private readonly ExcelTableColumn _timeoutColumn;

        public SmtpTable(ListObject listObject, MyVrmService service, ValidationListManager validationListManager) : 
            base(listObject, service, validationListManager)
        {
            var listColumns = ListObject.ListColumns;
            _serverIpAddressColumn = new ExcelTableColumn(listColumns[1]){IsRequired = true};
            _loginColumn = new ExcelTableColumn(listColumns[2]){IsRequired = true};
            _passwordColumn = new ExcelTableColumn(listColumns[3]){IsRequired = true};
            _portColumn = new ExcelTableColumn(listColumns[4], typeof(int)){IsRequired = false};
            _timeoutColumn = new ExcelTableColumn(listColumns[5], typeof(int)){IsRequired = true};

            TableColumns.AddRange(new[]
                                      {
                                          _serverIpAddressColumn, _loginColumn, _passwordColumn, _portColumn,
                                          _timeoutColumn
                                      });
            Initialize();
        }

        #region Overrides of ExcelTable

        protected override void Save(SaveErrorCollection errors)
        {
            foreach (var listRow in ListObject.ListRows.Items())
            {
                var settings = Service.GetConfigurationSettings();
                var emailSystemSettings = settings.EmailSystem;
                var range = listRow.Range;
                emailSystemSettings.ServerAddress = range.Item(1, _serverIpAddressColumn.Index).ValueAsString();
                emailSystemSettings.Login = range.Item(1, _loginColumn.Index).ValueAsString();
                emailSystemSettings.Password = range.Item(1, _passwordColumn.Index).ValueAsString();
                emailSystemSettings.Port = range.Item(1, _portColumn.Index).ValueAsInt();
                emailSystemSettings.Timeout = range.Item(1, _timeoutColumn.Index).ValueAsInt();
                Service.SaveConfigurationSettings(settings);
                break;
            }
        }

        #endregion
    }
}
