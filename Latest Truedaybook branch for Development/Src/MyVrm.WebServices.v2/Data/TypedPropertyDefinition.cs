﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.WebServices.Data
{
    internal abstract class TypedPropertyDefinition : PropertyDefinition
    {
        private readonly bool _isNullable;

        internal TypedPropertyDefinition(string xmlElementName) : base(xmlElementName)
        {
            _isNullable = false;
        }

        internal TypedPropertyDefinition(string xmlElementName, PropertyDefinitionFlags flags) : base(xmlElementName, flags)
        {
            _isNullable = false;
        }

        internal TypedPropertyDefinition(string xmlElementName, PropertyDefinitionFlags flags, bool isNullable)
            : this(xmlElementName, flags)
        {
            _isNullable = isNullable;
        }

        internal TypedPropertyDefinition(string xmlElementName, string xmlElementForWrite, PropertyDefinitionFlags flags)
            : base(xmlElementName, xmlElementForWrite, flags)
        {
            _isNullable = false;
        }

        internal abstract object Parse(string value);
        
        internal virtual string ToString(object value)
        {
            return value.ToString();
        }

        internal override void LoadPropertyValueFromXml(MyVrmServiceXmlReader reader, PropertyBag propertyBag)
        {
            var value = reader.ReadElementValue(XmlNamespace.NotSpecified, XmlElementName);
            if (!string.IsNullOrEmpty(value))
            {
                propertyBag[this] = Parse(value);
            }
        }

        internal override void WritePropertyValueToXml(MyVrmXmlWriter writer, PropertyBag propertyBag)
        {
            var value = propertyBag[this];
            writer.WriteElementValue(XmlNamespace.NotSpecified, XmlElementNameForWrite,
                                     value != null ? ToString(value) : string.Empty);
        }

        internal override bool IsNullable
        {
            get
            {
                return _isNullable;
            }
        }
    }
}
