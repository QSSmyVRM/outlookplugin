﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System.Net;
using MyVrm.Common.Security;

namespace MyVrm.WebServices.Data
{
    internal class LogonRequest : ServiceRequestBase<LogonResponse>
    {
        private readonly NetworkCredential _credential;
        private readonly string _userName;
        private readonly string _domainName;

        internal LogonRequest(MyVrmService service, NetworkCredential credential) : base(service)
        {
            _credential = credential;
        }

        internal LogonRequest(MyVrmService service, string domaiName, string userName)
            : base(service)
        {
            _domainName = domaiName;
            _userName = userName;
        }


        #region Overrides of ServiceRequestBase

        internal override string GetXmlElementName()
        {
            return "GetHome";
        }

        internal override string GetCommandName()
        {
            return "GetHome";
        }

        internal override string GetResponseXmlElementName()
        {
            return "user";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            writer.WriteStartElement(XmlNamespace.NotSpecified, "login");
            if (_credential != null)
            {
                writer.WriteElementValue(XmlNamespace.NotSpecified, "emailID", _credential.UserName);
                writer.WriteElementValue(XmlNamespace.NotSpecified, "userPassword", _credential.Password);
            }
            else
            {
                writer.WriteElementValue(XmlNamespace.NotSpecified, "userName", SecurityUtils.JoinNTUserName(_domainName, _userName));
                writer.WriteElementValue(XmlNamespace.NotSpecified, "userAuthenticated", "Yes");
            }
            writer.WriteEndElement();
        }

        #endregion
    }
}
