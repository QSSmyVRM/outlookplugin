﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	internal class GetVideoProtocolsRequest : ServiceRequestBase<GetVideoProtocolsResponse>
	{
		public GetVideoProtocolsRequest(MyVrmService service)
			: base(service)
		{
		}

		#region Overrides of ServiceRequestBase

		internal override string GetXmlElementName()
		{
			return "GetVideoProtocols";
		}

		internal override string GetCommandName()
		{
			return "GetVideoProtocols";
		}

		internal override string GetResponseXmlElementName()
		{
			return "GetVideoProtocols";
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			UserId.WriteToXml(writer, "UserID");
		}

		#endregion
	}
	//
	public class GetVideoProtocolsResponse : ServiceResponse
	{
		private readonly Dictionary<int, string> _videoProtocols = new Dictionary<int, string>();

		public Dictionary<int, string> VideoProtocols
		{
			get
			{
				return new Dictionary<int, string>(_videoProtocols);
			}
		}

		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{
			base.ReadElementsFromXml(reader);
			_videoProtocols.Clear();
			do
			{
				reader.Read();
				if (reader.IsStartElement(XmlNamespace.NotSpecified, "Protocol"))
				{
					int i = reader.ReadElementValue<int>(XmlNamespace.NotSpecified, "ID");
					string name = reader.ReadElementValue(XmlNamespace.NotSpecified, "Name");
					_videoProtocols.Add(i, name);
				}
				else
				{
					reader.SkipCurrentElement();
				}
			} while (!reader.IsEndElement(XmlNamespace.NotSpecified, "GetVideoProtocols"));
		}
	}
}