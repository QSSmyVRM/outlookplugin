﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System.Collections;
using System.Collections.Generic;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook
{
    public abstract class OutlookAppointmentBaseCollection<T> : ICollection<T> where T : OutlookAppointment
    {
        private readonly List<T> _appontments = new List<T>();

        #region Implementation of IEnumerable

        public IEnumerator<T> GetEnumerator()
        {
            return _appontments.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region Implementation of ICollection<T>

        public void Add(T item)
        {
            _appontments.Add(item);
        }

        public void Clear()
        {
            _appontments.Clear();
        }

        public bool Contains(T item)
        {
            return _appontments.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            _appontments.CopyTo(array, arrayIndex);
        }

        public bool Remove(T item)
        {
            return _appontments.Remove(item);
        }

        public int Count
        {
            get
            {
                return _appontments.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        #endregion

        public T this[int index]
        {
            get
            {
                return _appontments[index];
            }
        }

        public abstract T this[object item] { get; }
        public abstract T this[ConferenceId conferenceId] { get; }
    }
}
