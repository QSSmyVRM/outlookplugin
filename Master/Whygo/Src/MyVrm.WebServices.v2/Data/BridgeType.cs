﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.WebServices.Data
{
    public class BridgeType
    {
        internal BridgeType()
        {
        }

        public int Id { get; private set; }
        public string Name { get; private set; }
        public int InterfaceType { get; private set; }

        internal void LoadFromXml(MyVrmServiceXmlReader reader)
        {
            Id = reader.ReadElementValue<int>(XmlNamespace.NotSpecified, "ID");
            Name = reader.ReadElementValue(XmlNamespace.NotSpecified, "name");
            InterfaceType = reader.ReadElementValue<int>(XmlNamespace.NotSpecified, "interfaceType");
        }
    }
}
