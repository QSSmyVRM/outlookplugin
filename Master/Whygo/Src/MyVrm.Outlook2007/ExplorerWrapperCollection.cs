﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System.Collections;
using System.Collections.Generic;
using Microsoft.Office.Interop.Outlook;

namespace MyVrm.Outlook
{
    public class ExplorerWrapperCollection : ICollection<ExplorerWrapper>
    {
        private readonly List<ExplorerWrapper> _explorers = new List<ExplorerWrapper>();

        public IEnumerator<ExplorerWrapper> GetEnumerator()
        {
            return _explorers.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(ExplorerWrapper item)
        {
            _explorers.Add(item);
            item.Close += OnItemClose;
        }

        void OnItemClose(object sender, System.EventArgs e)
        {
            var explorerWrapper = (ExplorerWrapper)sender;
            explorerWrapper.Close -= OnItemClose;
            Remove(explorerWrapper);
        }

        public void Clear()
        {
            _explorers.Clear();
        }

        public bool Contains(ExplorerWrapper item)
        {
            return _explorers.Contains(item);
        }

        public void CopyTo(ExplorerWrapper[] array, int arrayIndex)
        {
            _explorers.CopyTo(array, arrayIndex);
        }

        public bool Remove(ExplorerWrapper item)
        {
            return _explorers.Remove(item);
        }

        public int Count
        {
            get { return _explorers.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }
    }
}
