﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Outlook;
using MyVrm.WebServices.Data;
using Exception = System.Exception;
using RecurrencePattern=MyVrm.WebServices.Data.RecurrencePattern;

namespace MyVrm.Outlook
{
    public partial class OutlookAppointmentImpl : OutlookAppointment
    {
    	public bool bSave;
        private const string ConferenceIdPropertyName = "myVRMConfID";
        public static string GetConferenceIdPropertyName
        {
            get{ return ConferenceIdPropertyName; }
        }

        private AppointmentItem _item;
        private OutlookRecipientImpl _currentUserRecipient;

        public OutlookAppointmentImpl(AppointmentItem  item)
        {
			MyVrmAddin.TraceSource.TraceInformation("OutlookAppointmentImpl(): item.GlobalAppointmentID = " + item.GlobalAppointmentID);
            Id = Guid.NewGuid();
            _item = item;
            Item.PropertyChange += ItemPropertyChanged;
            Item.Write += ItemWrite;
            ((ItemEvents_10_Event)Item).Send += ItemSend;
        }

        //List of converted from myVRM to Live Meeting conferences:
        // Key      - LM GlobalAppointmentId, which accept ConferenceId
        // Value    - ConferenceId of the converted myVRM conference
        public Dictionary<string, ConferenceId> m_ConvertedConfs = new Dictionary<string, ConferenceId>();

        public Guid Id { get; private set; }

        public override string Subject
        {
            get { return Item.Subject; }
            set 
            { 
                if (Item.Subject != value)
                {
                    Item.Subject = value;
                    NotifyPropertyChanged("Subject");
                }
            }
        }

        public override DateTime Start
        {
            get { return Item.Start; }
            set
            {
                if (Item.Start != value)
                {
                    Item.Start = value;
                    NotifyPropertyChanged("Start");
                }
            }
        }

        public override DateTime End
        {
            get { return Item.End; }
            set
            {
                if (Item.End != value)
                {
                    Item.End = value;
                    NotifyPropertyChanged("End");
                }
            }
        }

        public override TimeSpan Duration
        {
            get { return TimeSpan.FromMinutes(Item.Duration); }
            set
            {
                if (Duration != value)
                {
                    Item.Duration = value.Minutes;
                    NotifyPropertyChanged("Duration");
                }
            }
        }


        public override string Location
        {
            get { return Item.Location; }
            set
            {
                if (Item.Location != value)
                {
                    Item.Location = value;
                    NotifyPropertyChanged("Location");
                }
            }
        }
        //ZD 102172 start
        public override bool AllDayEvent
        {
            get { return  Item.AllDayEvent; }
            set
            {
                if (Item.AllDayEvent != value)
                {
                    Item.AllDayEvent = value;
                    NotifyPropertyChanged("AllDayEvent");
                }
            }
        }
        //ZD 102172 End
        public override ConferenceId ConferenceId
        {
            get
            {
                try
                {
                    UserProperty confIdProp = Item.UserProperties.Find(ConferenceIdPropertyName, true);
                    if (confIdProp != null && confIdProp.Type == OlUserPropertyType.olText)
                    {
                        return new ConferenceId((string)confIdProp.Value);
                    }
                }
                catch
                {
                }
                return null;
            }
            set
            {
                var confId = ConferenceId;
                if (ConferenceId != value)
                {
                    UserProperty property = confId == null
                                                ? Item.UserProperties.Add(ConferenceIdPropertyName, OlUserPropertyType.olText,
                                                                          false, null)
                                                : Item.UserProperties[ConferenceIdPropertyName];
                    property.Value = value.ToString();
                }
            }
        }

        public override event PropertyChangedEventHandler PropertyChanged;
        
        public override event CancelEventHandler Saving;

        public override event CancelEventHandler Sending;
        
        public override event CancelEventHandler BeforeDelete;

        public override ReadOnlyCollection<OutlookRecipient> Recipients
        {
            get
            {
                var recipients = new List<OutlookRecipient>();
                foreach (Recipient recipient in Item.Recipients)
                {
                    if (!recipient.Resolved)
                    {
                        recipient.Resolve();
                    }
                    if (recipient.Resolved)
                    {
                        recipients.Add(new OutlookRecipientImpl(recipient));
                    }
                }
                return new ReadOnlyCollection<OutlookRecipient>(recipients);
            }
        }

		public override void SetRecipients(IEnumerable<string> arg)
		{
			ClearRecipients();

			foreach (string outlookRecipientMail in arg)
			{
				Recipient recipient = _item.Recipients.Add(outlookRecipientMail);
				bool bret = recipient.Resolve();
				if( !recipient.Resolved )
				{
					_item.Recipients.Remove(recipient.Index);
				}
			}
		}

		public override void ClearRecipients()
		{
			try
			{
				while (_item.Recipients.Count > 0)
					_item.Recipients.Remove(_item.Recipients[_item.Recipients.Count].Index);
			}
			catch (Exception e)
			{
				MyVrmService.TraceSource.TraceInformation("Recipients.Remove Exception {0} ", e.ToString());
				throw;
			}
		}

    	public override OutlookRecipient CurrentUser
        {
            get {
                return _currentUserRecipient ??
                       (_currentUserRecipient = new OutlookRecipientImpl(Item.Session.CurrentUser));
            }
        }

        public override string Body
        {
            get { return Item.Body; }
            set
            {
                if (Item.Body != value)
                {
                    Item.Body = value;
                    NotifyPropertyChanged("Body");
                }
            }
        }

        public override string HtmlBody
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public override string RtfBody
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public override bool IsRecurring
        {
            get { return Item.IsRecurring; }
            set {}
        }

        public override RecurrenceState RecurrenceState
        {
            get
            {
                switch(Item.RecurrenceState)
                {
                    case OlRecurrenceState.olApptNotRecurring:
                        return RecurrenceState.NotRecurring;
                    case OlRecurrenceState.olApptMaster:
                        return RecurrenceState.Master;
                    case OlRecurrenceState.olApptOccurrence:
                        return RecurrenceState.Occurrence;
                    case OlRecurrenceState.olApptException:
                        return RecurrenceState.Exception;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            set {  }
        }

        public AppointmentItem Item
        {
            get { return _item; }
        }


        public override FreeBusyStatus BusyStatus
        {
            get
            {
                switch(_item.BusyStatus)
                {
                    case OlBusyStatus.olFree:
                        return FreeBusyStatus.Free;
                    case OlBusyStatus.olTentative:
                        return FreeBusyStatus.Tentative;
                    case OlBusyStatus.olBusy:
                        return FreeBusyStatus.Busy;
                    case OlBusyStatus.olOutOfOffice:
                        return FreeBusyStatus.OutOfOffice;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            set
            {
            }
        }

        public override RecurrencePattern GetRecurrencePattern(TimeZoneInfo timeZone)
        {
            var recurrencePattern = new RecurrencePattern(timeZone);
            var outlookPattern = Item.GetRecurrencePattern();
            try
            {
                recurrencePattern.RecurrenceType = (RecurrenceType)outlookPattern.RecurrenceType;
                switch (outlookPattern.RecurrenceType)
                {
                    case OlRecurrenceType.olRecursDaily:
                    {
                        recurrencePattern.DaysOfWeek = (DaysOfWeek)outlookPattern.DayOfWeekMask;
                        break;
                    }
                    case OlRecurrenceType.olRecursWeekly:
                    {
                        recurrencePattern.DaysOfWeek = (DaysOfWeek)outlookPattern.DayOfWeekMask;
                        break;
                    }
                    case OlRecurrenceType.olRecursMonthly:
                    {
                        recurrencePattern.DayOfMonth = outlookPattern.DayOfMonth;
                        break;
                    }
                    case OlRecurrenceType.olRecursMonthNth:
                    {
                        recurrencePattern.DaysOfWeek = (DaysOfWeek)outlookPattern.DayOfWeekMask;
                        break;
                    }
                    case OlRecurrenceType.olRecursYearly:
                    {
                        recurrencePattern.MonthOfYear = outlookPattern.MonthOfYear;
                        recurrencePattern.DayOfMonth = outlookPattern.DayOfMonth;
                        break;
                    }
                    case OlRecurrenceType.olRecursYearNth:
                    {
                        recurrencePattern.MonthOfYear = outlookPattern.MonthOfYear;
                        recurrencePattern.DaysOfWeek = (DaysOfWeek)outlookPattern.DayOfWeekMask;
                        break;
                    }
                }
                recurrencePattern.StartTime = outlookPattern.StartTime.TimeOfDay;
                recurrencePattern.EndTime = outlookPattern.EndTime.TimeOfDay;
                if (outlookPattern.Interval > 0)
                {
                    recurrencePattern.Interval = outlookPattern.RecurrenceType == OlRecurrenceType.olRecursYearly ||
                                                 outlookPattern.RecurrenceType == OlRecurrenceType.olRecursYearNth
                                                     ? outlookPattern.Interval/12
                                                     : outlookPattern.Interval;
                }
                recurrencePattern.Instance = outlookPattern.Instance;
                recurrencePattern.StartDate = outlookPattern.PatternStartDate;
                if (outlookPattern.NoEndDate)
                {
                    recurrencePattern.NeverEnds();
                }
                else
                {
                    recurrencePattern.EndDate = outlookPattern.PatternEndDate;
                    recurrencePattern.Occurrences = outlookPattern.Occurrences;
                }
                return recurrencePattern;
            }
            finally
            {
                if (outlookPattern != null)
                {
                    Marshal.ReleaseComObject(outlookPattern);
                }
            }
        }


        /// <summary>
        /// Removes link (user's property) from the appointment.
        /// </summary>
        /// <param name="item">Appointment item.</param>
        public static void UnlinkConference(AppointmentItem item)
        {
            if (item == null) 
                throw new ArgumentNullException("item");
            var confIdProp = item.UserProperties.Find(ConferenceIdPropertyName, true);
            if (confIdProp != null)
            {
                confIdProp.Delete();
            }
        }

        /// <summary>
        /// Removes link (user's property) from the appointment.
        /// </summary>
        public void UnlinkConference()
        {
            UnlinkConference(Item);
        }

        public static ConferenceId GetConferenceId(AppointmentItem item)
        {
            if (item == null) 
                throw new ArgumentNullException("item");
            try
            {
                UserProperty confIdProp = item.UserProperties.Find(ConferenceIdPropertyName, true);
                if (confIdProp != null && confIdProp.Type == OlUserPropertyType.olText)
                {
                    return new ConferenceId((string)confIdProp.Value);
                }
            }
            catch
            {
            }
            return null;
        }

        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        private void NotifySaving(ref bool cancel)
        {
            // Remove one-offed form properties to avoid displaying custom forms on higher versions of Outlook 2007,2010.
            // For security and storage reasons, Microsoft Office Outlook 2007 ignores form definitions saved with any item.
            RemoveOneOff(false);

            /**/
            if (ConferenceId == null && Item.MessageClass.Contains("IPM.Appointment.Live Meeting Request"))
            {
                if (m_ConvertedConfs.ContainsKey(Item.GlobalAppointmentID))
                {
                    ConferenceId = m_ConvertedConfs[Item.GlobalAppointmentID];
                    //???? 
                    ////We have assigned the LM conference with myVRM ConfId - remove converting record from list
                    //m_ConvertedConfs.Remove(Item.GlobalAppointmentID);
                }
            }
            /**/
            // Ignore cancelled meetings
            if (Item.MeetingStatus == OlMeetingStatus.olMeetingCanceled)
            {
                return;
            }
            if (Saving != null && RaiseSavingEvent)
            {
                var args = new CancelEventArgs();
                Saving(this, args);
                cancel = args.Cancel;
            }
        }

        /// <summary>
        /// Removes custom form definition (one-off)
        /// </summary>
        /// <param name="removePropDef"></param>
        partial void RemoveOneOff(bool removePropDef);

        partial void ItemPropertyChanged(string name);

        private void ItemWrite(ref bool cancel)
        {
			bSave = true;
            NotifySaving(ref cancel);
			bSave = false;
        }

        private void ItemSend(ref bool cancel)
        {
        	bSave = true;
            NotifySending(ref cancel);
			bSave = false;
        }

        private void NotifySending(ref bool cancel)
        {
            if (Sending != null)
            {
                var args = new CancelEventArgs();
                Sending(this, args);
                cancel = args.Cancel;
            }
        }

        private void OnBeforeDelete(object item, ref bool cancel)
        {
            NotifyBeforeDelete(ref cancel);
        }

        private void NotifyBeforeDelete(ref bool cancel)
        {
            if (BeforeDelete != null)
            {
                var args = new CancelEventArgs();
                BeforeDelete(this, args);
                cancel = args.Cancel;
            }
        }

        #region Implementation of IDisposable

        public override OutlookAppointment GetOccurrence(DateTime occurrenceTime)
        {
            var outlookPattern = Item.GetRecurrencePattern();
            return new OutlookAppointmentImpl(outlookPattern.GetOccurrence(occurrenceTime));
        }

        public override void Save()
        {
            Item.Save();
        }

        public override void Delete()
        {
            Item.Delete();
        }

        public override void Dispose()
        {
            if (_item != null)
            {
				MyVrmAddin.TraceSource.TraceInformation("Dispose(): item.GlobalAppointmentID = " + _item.GlobalAppointmentID);
                ((ItemEvents_10_Event)Item).Send -= ItemSend;
                Item.PropertyChange -= ItemPropertyChanged;
                Item.Write -= ItemWrite;
                Marshal.ReleaseComObject(_item);
                _item = null;
            }
        }

        #endregion

		public override void SetNonOutlookProperty(bool value)
		{
			if (Item != null)
			{
				if (Item.UserProperties.Find("nonOutlookPropChanged", true) == null)
					Item.UserProperties.Add("nonOutlookPropChanged", OlUserPropertyType.olYesNo, false,
																	  OlFormatYesNo.olFormatYesNoTrueFalse);
				Item.UserProperties["nonOutlookPropChanged"].Value = value;
			}
		}

		public override bool GetNonOutlookProperty()
		{
			bool flag = false;

			if (Item != null)
			{
				if (Item.UserProperties.Find("nonOutlookPropChanged", true) != null)
				{
					bool.TryParse(Item.UserProperties["nonOutlookPropChanged"].Value.ToString(), out flag);
				}
			}
			return flag;
		}
    }
}