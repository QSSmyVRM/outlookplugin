﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Outlook;
using Microsoft.Win32;
using MyVrm.Outlook;
using MyVrm.Outlook.WinForms;
using MyVrm.Outlook.WinForms.Options;
using MyVrm.WebServices.Data;
using MyVrmAddin2007.Properties;
using Exception = System.Exception;
using Image = System.Drawing.Image;

namespace MyVrmAddin2007
{
    public class MyVrmExplorer : ExplorerWrapper
    {
        private CommandBar _myVrmCommandBar;

        private CommandBarComboBox _templateCombo;
        private CommandBarButton _fillTemplateCombo;
        private List<TemplateInfo> _templateList = new List<TemplateInfo>();

        private CommandBarButton _favoritesButton;
        private CommandBarButton _roomCalendarButton;
        private CommandBarButton _conferenceButton;
        private CommandBarButton _approvalPendingButton;
        private CommandBarButton _optionsButton;
        
        private const string myVRMToolbarVal = "myVRMToolbarEnabled";

        public MyVrmExplorer(Explorer explorer)
            : base(explorer)
        {
        }

        protected override void OnClose()
        {
            base.OnClose();
            try
            {
                //Do nothing for Outlook 2010
                if (Explorer.Application.Version.StartsWith("14.") || Explorer.Application.Version.StartsWith("15."))
                    return;
                _conferenceButton.Click -= _conferenceButton_Click;
                _conferenceButton.Delete(true);
                _conferenceButton = null;

                _fillTemplateCombo.Click -= _fillTemplateCombo_Click;
                _fillTemplateCombo.Delete(true);
                _fillTemplateCombo = null;

				_templateCombo.Change -= _templateCombo_Change;
				_templateCombo.Delete(true);
            	_templateCombo = null;

                _approvalPendingButton.Click -= _approvalPendingButton_Click;
                _approvalPendingButton.Delete(true);
                _approvalPendingButton = null;

                _roomCalendarButton.Click -= _roomCalendarButton_Click;
                _roomCalendarButton.Delete(true);
                _roomCalendarButton = null;

                _favoritesButton.Click -= _favoriteRoomsButton_Click;
                _favoritesButton.Delete(true);
                _favoritesButton = null;

                _optionsButton.Click -= OptionsButtonClick;
                _optionsButton.Delete(true);
                _optionsButton = null;


                bool bVisible = _myVrmCommandBar.Visible;
                using(var key = Registry.CurrentUser.OpenSubKey(MyVrmAddin.LocalUserAppRegPath, true))
                {
                    key.SetValue(myVRMToolbarVal, bVisible ? 1 : 0, RegistryValueKind.DWord);    
                }
                _myVrmCommandBar.Delete();
                Marshal.ReleaseComObject(_myVrmCommandBar);
                _myVrmCommandBar = null;
            }
            catch (Exception e)
            {
                MyVrmAddin.TraceSource.TraceException(e);
                UIHelper.ShowError(e.Message);
            }
        }

        protected override void Initialize()
        {
            base.Initialize();
            try
            {
                int _isVisible = 0;
                bool bValueIsExist = false;
                MyVrmAddin.TraceSource.TraceInformation("Intializing myvrm Explorer...");
                //Do nothing for Outlook 2010
                if (Explorer.Application.Version.StartsWith("14.") || Explorer.Application.Version.StartsWith("15."))
                    return;

                RegistryKey myVRMKey = Registry.CurrentUser.OpenSubKey(MyVrmAddin.LocalUserAppRegPath);
                if (myVRMKey != null)
                {
                    string[] values = myVRMKey.GetValueNames();
                    bValueIsExist = values.Contains(myVRMToolbarVal);
                }
                if (bValueIsExist) //if key and value exist use them
                {
                    object val = myVRMKey.GetValue(myVRMToolbarVal, 0);
                    int.TryParse(val.ToString(), out _isVisible);
                }
                else //otherwice there is the first run - show toolbar
                {
                    _isVisible = 1;
                }

                if (myVRMKey != null)
                    myVRMKey.Close();


                //Create toolbar
                CommandBars cmdBars = Explorer.CommandBars;
                _myVrmCommandBar = cmdBars.Add(/*"myVRM"*/MyVrmAddin.ProductDisplayName, MsoBarPosition.msoBarTop, false, true);
                _myVrmCommandBar.Enabled = true;
                //Create Conference button
                _conferenceButton = (CommandBarButton)_myVrmCommandBar.Controls.Add(
                                                            MsoControlType.msoControlButton, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                _conferenceButton.Style = MsoButtonStyle.msoButtonIconAndCaption;
                _conferenceButton.Caption = Strings.NewConference;
				_conferenceButton.Picture = MyVrm.Outlook.WinForms.AxHost.GetIPictureDispFromPicture((Image) MyVrmAddin.Logo16);//Resources.Logo);
				_conferenceButton.Mask = MyVrm.Outlook.WinForms.AxHost.GetMaskImage((Image)MyVrmAddin.Logo16);//Resources.Logo);
                _conferenceButton.Click += _conferenceButton_Click;
                /*exp templates ->*/
                _fillTemplateCombo = (CommandBarButton)_myVrmCommandBar.Controls.Add(
                                                            MsoControlType.msoControlButton, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                _fillTemplateCombo.Style = MsoButtonStyle.msoButtonCaption;
                _fillTemplateCombo.Caption = Strings.UpdateTemplateListMenuPoint;
                _fillTemplateCombo.Click += _fillTemplateCombo_Click;
                _fillTemplateCombo.BeginGroup = true;
                _fillTemplateCombo.TooltipText = Strings.UpdateTemplateListMenuPointToolTip;

                _templateCombo = (CommandBarComboBox)_myVrmCommandBar.Controls.Add(
                    MsoControlType.msoControlComboBox, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                _templateCombo.Visible = true;
                _templateCombo.Change += _templateCombo_Change;
                _templateCombo.Width = 200;
                _templateCombo.Enabled = false;
                _templateCombo.Caption = Strings.TemplateListMenuPointToolTip;

                //Create Approve Conference button
                _approvalPendingButton = (CommandBarButton)_myVrmCommandBar.Controls.Add(
                                                        MsoControlType.msoControlButton, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                _approvalPendingButton.Style = MsoButtonStyle.msoButtonIconAndCaption;
                _approvalPendingButton.Caption = Strings.ConferenceApproval;
                _approvalPendingButton.Picture = MyVrm.Outlook.WinForms.AxHost.GetIPictureDispFromPicture(Resources.approveConf);//Logo);
                _approvalPendingButton.Mask = MyVrm.Outlook.WinForms.AxHost.GetMaskImage(Resources.approveConf);//Logo);
                _approvalPendingButton.Click += _approvalPendingButton_Click;
                _approvalPendingButton.BeginGroup = true;
                //Create Room Calendar button
                _roomCalendarButton = (CommandBarButton)_myVrmCommandBar.Controls.Add(
                                                        MsoControlType.msoControlButton, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                _roomCalendarButton.Style = MsoButtonStyle.msoButtonIconAndCaption;
                _roomCalendarButton.Caption = Strings.RoomsCalendar;
                _roomCalendarButton.Picture = MyVrm.Outlook.WinForms.AxHost.GetIPictureDispFromPicture(Resources.RoomsCalendar);
                _roomCalendarButton.Mask = MyVrm.Outlook.WinForms.AxHost.GetMaskImage(Resources.RoomsCalendar);
                _roomCalendarButton.Click += _roomCalendarButton_Click;

                //Create Room Favorites button
                _favoritesButton = (CommandBarButton)_myVrmCommandBar.Controls.Add(
                                                        MsoControlType.msoControlButton, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                _favoritesButton.Style = MsoButtonStyle.msoButtonIconAndCaption;
                _favoritesButton.Caption = Strings.FavoriteRooms;
                _favoritesButton.Picture = MyVrm.Outlook.WinForms.AxHost.GetIPictureDispFromPicture(Resources.FavoriteRooms);
                _favoritesButton.Mask = MyVrm.Outlook.WinForms.AxHost.GetMaskImage(Resources.FavoriteRooms);
                _favoritesButton.Click += _favoriteRoomsButton_Click;
                //Create Options button
                _optionsButton = (CommandBarButton)_myVrmCommandBar.Controls.Add(
                                                        MsoControlType.msoControlButton, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                _optionsButton.Style = MsoButtonStyle.msoButtonIconAndCaption;
                _optionsButton.Caption = Strings.OptionsMenuPoint;
                _optionsButton.Picture = MyVrm.Outlook.WinForms.AxHost.GetIPictureDispFromPicture(Resources.Options);
                _optionsButton.Mask = MyVrm.Outlook.WinForms.AxHost.GetMaskImage(Resources.Options);
                _optionsButton.Click += OptionsButtonClick;

				//For York version - hide all points except "New Conference" and "Options"
				if (MyVrmAddin.BuildType == "03")
				{
					_fillTemplateCombo.Visible = false;
					_templateCombo.Visible = false;
					_approvalPendingButton.Visible = false;
					_roomCalendarButton.Visible = false;
					_favoritesButton.Visible = false;
				}
            	_myVrmCommandBar.Visible = _isVisible > 0;
            }//ZD 102062
            catch (Exception exception)
            {
                MyVrmAddin.TraceSource.TraceException(exception);
            }
        }

        private void _favoriteRoomsButton_Click(CommandBarButton ctrl, ref bool cancel)
        {
            ConferenceRibbon.favoriteRoomsButton_OnClick();
        }

        private void OptionsButtonClick(CommandBarButton ctrl, ref bool cancel)
        {
            using (var optDlg = new OptionsAsDlg())
            {
                CancelEventArgs e = new CancelEventArgs();
                if (optDlg.ShowDialog() == DialogResult.OK)
                    optDlg.optionsControl.External_OnApplying(e);
            }
        }

        private void _fillTemplateCombo_Click(CommandBarButton ctrl, ref bool cancel)
        {
            _templateCombo.Clear();
            _templateCombo.Enabled = false;
            GetTemplateListResponse ret = null;
            _templateList.Clear();

            var cursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            //Get data
            try
            {
                ret = MyVrmService.Service.GetTemplateList(GetTemplateListResponse.SortByType.ByName);
            }
            catch (Exception exception)
            {
                MyVrmAddin.TraceSource.TraceException(exception, true);
                UIHelper.ShowMessage(exception.Message, MessageBoxButtons.OK,
                                     MessageBoxIcon.Exclamation);
            }
            finally
            {
                Cursor.Current = cursor;
            }
            if (ret != null && ret.templateList != null)
            {
                _templateList = ret.templateList;
                foreach (TemplateInfo templateInfo in ret.templateList)
                {
                    _templateCombo.AddItem(templateInfo.Name, Type.Missing);
                }
                _templateCombo.Enabled = true;
                //_templateCombo.Execute();

                _templateCombo.SetFocus();
                SendKeys.Send("+({DOWN})");
            }
        }

        void SetAppointmentParamsFromTemplate(int templateID)
        {
            GetTemplateResponse response = null;
            var cursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                response = MyVrmService.Service.GetTemplate(templateID);
            }
            catch (Exception exception)
            {
                MyVrmAddin.TraceSource.TraceException(exception, true);
                UIHelper.ShowMessage(exception.Message, MessageBoxButtons.OK,
                                         MessageBoxIcon.Exclamation);
            }
            finally
            {
                Cursor.Current = cursor;
            }

            if (response != null)
            {
                //OutlookAppointmentImpl apptImpl;

                AppointmentItem appItem = (AppointmentItem)Explorer.Application.CreateItem(OlItemType.olAppointmentItem);
                ThisAddIn.Sub_SetAppointmentParamsFromTemplate(appItem, response.Conference, Type.Missing);
              //  AppointmentItem appItem = (AppointmentItem)Globals.ThisAddIn.Application.CreateItem(OlItemType.olAppointmentItem);
                appItem.Display(Type.Missing);
                appItem.MeetingStatus = OlMeetingStatus.olMeeting;//102148
                appItem.GetInspector.SetCurrentFormPage("MyVrmAddin2007.ConferenceFormRegion");
            }
        }

        void _templateCombo_Change(CommandBarComboBox Ctrl)
        {
            int selected = _templateList.FindIndex(0, tempalte => tempalte.Name == Ctrl.Text);
            if (selected >= 0)
            {
                SetAppointmentParamsFromTemplate(int.Parse(_templateList[selected].ID));
            }
        }

        private void _roomCalendarButton_Click(CommandBarButton ctrl, ref bool cancel)
        {
            ConferenceRibbon.roomsCalendarButton_OnClick();
        }
        
        private void _approvalPendingButton_Click(CommandBarButton ctrl, ref bool cancel)
        {
            ConferenceRibbon.approvalConferencesButton_OnClick();
        }

        private void _conferenceButton_Click(CommandBarButton ctrl, ref bool cancel)
        {
            var cursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                

               
                AppointmentItem appItem = (AppointmentItem)Explorer.Application.CreateItem(OlItemType.olAppointmentItem);
                appItem.Display(Type.Missing);
                appItem.MeetingStatus = OlMeetingStatus.olMeeting;//102148
                appItem.GetInspector.SetCurrentFormPage("MyVrmAddin2007.ConferenceFormRegion");
            }
            catch (Exception exception)
            {
                MyVrmAddin.TraceSource.TraceException(exception);
                UIHelper.ShowError(exception.Message);
            }
            finally
            {
                Cursor.Current = cursor;
            }
        }
    }
}
