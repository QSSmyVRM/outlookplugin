﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    internal class GetNewBridgeResponse : ServiceResponse
    {
        private readonly BridgeTypeCollection _bridgeTypes;
        
        public GetNewBridgeResponse()
        {
            _bridgeTypes = new BridgeTypeCollection();
        }

        public BridgeTypeCollection BridgeTypes
        {
            get { return _bridgeTypes; }
        }

        internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
        {
            do
            {
                reader.Read();
                if (reader.IsStartElement(XmlNamespace.NotSpecified, "bridgeTypes"))
                {
                    BridgeTypes.LoadFromXml(reader);
                }
                else
                {
                    reader.SkipCurrentElement();
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "bridge"));            
        }
    }
}
