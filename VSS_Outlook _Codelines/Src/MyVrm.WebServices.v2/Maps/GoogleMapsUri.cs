﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Text;

namespace MyVrm.WebServices.Maps
{
    public class GoogleMapsUri : MapsUri
    {
        private const string BaseAddress = " http://maps.google.com";

        public override string ToString()
        {
            // Build search url to Google Maps. 
            // See http://mapki.com/wiki/Google_Map_Parameters
            var uriBuilder = new UriBuilder(BaseAddress);
            var queryBuilder = new StringBuilder();
            // Search mode
            queryBuilder.Append("&mrt=loc"); // Locations search
            // Location
            // Latitude, longitude
            if (!string.IsNullOrEmpty(Longitude) && !string.IsNullOrEmpty(Latitude))
            {
                queryBuilder.AppendFormat("&ll={0},{1}", Latitude, Longitude);
            }
            //Query
            if (!string.IsNullOrEmpty(Location))
            {
                queryBuilder.AppendFormat("&q={0}", Location);
            }
            // Map type 
            queryBuilder.Append("&t=m"); // Display a map.
            // Zoom level
            queryBuilder.Append("&z=16");
            uriBuilder.Query = queryBuilder.ToString();
            return uriBuilder.ToString();
        }
    }
}
