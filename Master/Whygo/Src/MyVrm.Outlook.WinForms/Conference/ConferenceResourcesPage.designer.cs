﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.Outlook.WinForms.Conference
{
    partial class ConferenceResourcesPage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.layoutControl2 = new DevExpress.XtraDataLayout.DataLayoutControl();
			this.conferenceGeneralControl = new MyVrm.Outlook.WinForms.Conference.ConferenceGeneralControl();
			this.conferenceDateTimeControl = new MyVrm.Outlook.WinForms.Conference.ConferenceDateTimeControl();
			this.resourcesScheduleControl = new MyVrm.Outlook.WinForms.Conference.ResourcesScheduleControl();
			this.conferenceNameEdit = new DevExpress.XtraEditors.TextEdit();
			this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.nameLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
			this.scheduleLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
			this.dateTimeLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
			this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.generalLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
			this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
			((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
			this.layoutControl2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.conferenceNameEdit.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nameLayoutControlItem)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.scheduleLayoutControlItem)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dateTimeLayoutControlItem)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.generalLayoutControlItem)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
			this.SuspendLayout();
			// 
			// layoutControl2
			// 
			this.layoutControl2.AllowCustomizationMenu = false;
			this.layoutControl2.Controls.Add(this.conferenceGeneralControl);
			this.layoutControl2.Controls.Add(this.conferenceDateTimeControl);
			this.layoutControl2.Controls.Add(this.resourcesScheduleControl);
			this.layoutControl2.Controls.Add(this.conferenceNameEdit);
			this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.layoutControl2.Location = new System.Drawing.Point(0, 0);
			this.layoutControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.layoutControl2.Name = "layoutControl2";
			this.layoutControl2.Root = this.layoutControlGroup2;
			this.layoutControl2.Size = new System.Drawing.Size(944, 619);
			this.layoutControl2.TabIndex = 0;
			this.layoutControl2.Text = "layoutControl2";
			// 
			// conferenceGeneralControl
			// 
			this.conferenceGeneralControl.Location = new System.Drawing.Point(349, 555);
			this.conferenceGeneralControl.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
			this.conferenceGeneralControl.Name = "conferenceGeneralControl";
			this.conferenceGeneralControl.ReadOnly = false;
			this.conferenceGeneralControl.Size = new System.Drawing.Size(509, 62);
			this.conferenceGeneralControl.TabIndex = 18;
			// 
			// conferenceDateTimeControl
			// 
			this.conferenceDateTimeControl.Location = new System.Drawing.Point(2, 555);
			this.conferenceDateTimeControl.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
			this.conferenceDateTimeControl.Name = "conferenceDateTimeControl";
			this.conferenceDateTimeControl.ReadOnly = false;
			this.conferenceDateTimeControl.Size = new System.Drawing.Size(343, 62);
			this.conferenceDateTimeControl.TabIndex = 17;
			// 
			// resourcesScheduleControl
			// 
			this.resourcesScheduleControl.Location = new System.Drawing.Point(2, 28);
			this.resourcesScheduleControl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.resourcesScheduleControl.Name = "resourcesScheduleControl";
			this.resourcesScheduleControl.ReadOnly = false;
			this.resourcesScheduleControl.Size = new System.Drawing.Size(940, 523);
			this.resourcesScheduleControl.TabIndex = 16;
			// 
			// conferenceNameEdit
			// 
			this.conferenceNameEdit.Location = new System.Drawing.Point(113, 2);
			this.conferenceNameEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.conferenceNameEdit.Name = "conferenceNameEdit";
			this.conferenceNameEdit.Properties.NullText = "<Type conference name here>";
			this.conferenceNameEdit.Size = new System.Drawing.Size(829, 22);
			this.conferenceNameEdit.StyleController = this.layoutControl2;
			this.conferenceNameEdit.TabIndex = 15;
			// 
			// layoutControlGroup2
			// 
			this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
			this.layoutControlGroup2.GroupBordersVisible = false;
			this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.nameLayoutControlItem,
            this.scheduleLayoutControlItem,
            this.dateTimeLayoutControlItem,
            this.emptySpaceItem1,
            this.generalLayoutControlItem});
			this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup2.Name = "Root";
			this.layoutControlGroup2.Size = new System.Drawing.Size(944, 619);
			this.layoutControlGroup2.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup2.Text = "Root";
			this.layoutControlGroup2.TextVisible = false;
			// 
			// nameLayoutControlItem
			// 
			this.nameLayoutControlItem.Control = this.conferenceNameEdit;
			this.nameLayoutControlItem.CustomizationFormText = "Conference Name:";
			this.nameLayoutControlItem.Location = new System.Drawing.Point(0, 0);
			this.nameLayoutControlItem.Name = "nameLayoutControlItem";
			this.nameLayoutControlItem.Size = new System.Drawing.Size(944, 26);
			this.nameLayoutControlItem.Text = "Conference Name:";
			this.nameLayoutControlItem.TextSize = new System.Drawing.Size(107, 16);
			// 
			// scheduleLayoutControlItem
			// 
			this.scheduleLayoutControlItem.Control = this.resourcesScheduleControl;
			this.scheduleLayoutControlItem.CustomizationFormText = "scheduleLayoutControlItem";
			this.scheduleLayoutControlItem.Location = new System.Drawing.Point(0, 26);
			this.scheduleLayoutControlItem.Name = "scheduleLayoutControlItem";
			this.scheduleLayoutControlItem.Size = new System.Drawing.Size(944, 527);
			this.scheduleLayoutControlItem.Text = "scheduleLayoutControlItem";
			this.scheduleLayoutControlItem.TextSize = new System.Drawing.Size(0, 0);
			this.scheduleLayoutControlItem.TextToControlDistance = 0;
			this.scheduleLayoutControlItem.TextVisible = false;
			// 
			// dateTimeLayoutControlItem
			// 
			this.dateTimeLayoutControlItem.Control = this.conferenceDateTimeControl;
			this.dateTimeLayoutControlItem.CustomizationFormText = "dateTimeLayoutControlItem";
			this.dateTimeLayoutControlItem.Location = new System.Drawing.Point(0, 553);
			this.dateTimeLayoutControlItem.MaxSize = new System.Drawing.Size(0, 66);
			this.dateTimeLayoutControlItem.MinSize = new System.Drawing.Size(104, 66);
			this.dateTimeLayoutControlItem.Name = "dateTimeLayoutControlItem";
			this.dateTimeLayoutControlItem.Size = new System.Drawing.Size(347, 66);
			this.dateTimeLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.dateTimeLayoutControlItem.Text = "dateTimeLayoutControlItem";
			this.dateTimeLayoutControlItem.TextSize = new System.Drawing.Size(0, 0);
			this.dateTimeLayoutControlItem.TextToControlDistance = 0;
			this.dateTimeLayoutControlItem.TextVisible = false;
			// 
			// emptySpaceItem1
			// 
			this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
			this.emptySpaceItem1.Location = new System.Drawing.Point(860, 553);
			this.emptySpaceItem1.Name = "emptySpaceItem1";
			this.emptySpaceItem1.Size = new System.Drawing.Size(84, 66);
			this.emptySpaceItem1.Text = "emptySpaceItem1";
			this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
			// 
			// generalLayoutControlItem
			// 
			this.generalLayoutControlItem.Control = this.conferenceGeneralControl;
			this.generalLayoutControlItem.CustomizationFormText = "generalLayoutControlItem";
			this.generalLayoutControlItem.Location = new System.Drawing.Point(347, 553);
			this.generalLayoutControlItem.MaxSize = new System.Drawing.Size(0, 66);
			this.generalLayoutControlItem.MinSize = new System.Drawing.Size(104, 66);
			this.generalLayoutControlItem.Name = "generalLayoutControlItem";
			this.generalLayoutControlItem.Size = new System.Drawing.Size(513, 66);
			this.generalLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.generalLayoutControlItem.Text = "generalLayoutControlItem";
			this.generalLayoutControlItem.TextSize = new System.Drawing.Size(0, 0);
			this.generalLayoutControlItem.TextToControlDistance = 0;
			this.generalLayoutControlItem.TextVisible = false;
			// 
			// emptySpaceItem4
			// 
			this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
			this.emptySpaceItem4.Location = new System.Drawing.Point(0, 182);
			this.emptySpaceItem4.Name = "emptySpaceItem4";
			this.emptySpaceItem4.Size = new System.Drawing.Size(701, 258);
			this.emptySpaceItem4.Text = "emptySpaceItem4";
			this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
			// 
			// ConferenceResourcesPage
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.layoutControl2);
			this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
			this.Name = "ConferenceResourcesPage";
			this.Size = new System.Drawing.Size(944, 619);
			((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
			this.layoutControl2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.conferenceNameEdit.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nameLayoutControlItem)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.scheduleLayoutControlItem)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dateTimeLayoutControlItem)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.generalLayoutControlItem)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.TextEdit conferenceNameEdit;
        private DevExpress.XtraLayout.LayoutControlItem nameLayoutControlItem;
        private ResourcesScheduleControl resourcesScheduleControl;
        private DevExpress.XtraLayout.LayoutControlItem scheduleLayoutControlItem;
        private ConferenceDateTimeControl conferenceDateTimeControl;
        private DevExpress.XtraLayout.LayoutControlItem dateTimeLayoutControlItem;
        private ConferenceGeneralControl conferenceGeneralControl;
        private DevExpress.XtraLayout.LayoutControlItem generalLayoutControlItem;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;

    }
}