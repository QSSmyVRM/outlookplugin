﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace MyVrm.Common.Security
{
    public static class SecurityUtils
    {
        public static void SplitNTUserName(string userName, out string domainName, out string accountName)
        {
            if (userName == null) 
                throw new ArgumentNullException("userName");
            string[] parts = userName.Split(new[]{'\\'}, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length != 2)
            {
                throw new ArgumentException("", "userName");
            }
            domainName = parts[0];
            accountName = parts[1];
        }

        public static string JoinNTUserName(string domainName, string userName)
        {
            if (userName == null) 
                throw new ArgumentNullException("userName");
            return string.IsNullOrEmpty(domainName) ? userName : domainName + @"\" + userName;
        }
    }
}
