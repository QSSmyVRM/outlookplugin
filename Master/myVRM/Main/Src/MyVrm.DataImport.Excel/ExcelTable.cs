﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop.Excel.Extensions;
using MyVrm.WebServices.Data;
using ListObject=Microsoft.Office.Tools.Excel.ListObject;

namespace MyVrm.DataImport.Excel
{
    public abstract class ExcelTable
    {
        private readonly ExcelTableColumnCollection _tableColumns = new ExcelTableColumnCollection();

        protected ExcelTable(ListObject listObject, MyVrmService service, ValidationListManager validationListManager)
        {
            if (listObject == null) 
                throw new ArgumentNullException("listObject");
            if (service == null) 
                throw new ArgumentNullException("service");
            if (validationListManager == null) 
                throw new ArgumentNullException("validationListManager");
            ListObject = listObject;
            Service = service;
            ValidationListManager = validationListManager;
            ApplyDefaultListValidation();
        }

        public void Publish()
        {
            if (!Validate())
            {
                MessageBox.Show(Strings.ExcelTableHasInvalidData,
                                Strings.ValidationErrorsCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            var errors = new SaveErrorCollection();
            try
            {
                Save(errors);
                if (errors.Count > 0)
                {
                    using(var dialog = new SaveErrorsDialog())
                    {
                        dialog.Errors = errors;
                        dialog.ShowDialog();
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message,
                                Strings.ValidationErrorsCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        protected abstract void Save(SaveErrorCollection errors);

        protected ListObject ListObject { get; private set; }
        
        protected MyVrmService Service { get; private set; }

        protected ValidationListManager ValidationListManager { get; private set; }

        protected ExcelTableColumnCollection TableColumns
        {
            get
            {
                return _tableColumns;
            }
        }

        protected void Initialize()
        {
            var insertRowRange = ListObject.InsertRowRange;
            if (insertRowRange != null)
            {
                foreach (var tableColumn in TableColumns)
                {
                    var cell = (Range) insertRowRange[1, tableColumn.Index];
                    ApplyValidation(cell, tableColumn.ValuesRangeName, tableColumn.Type, tableColumn.IsRequired,
                                    tableColumn.HasFixedSetOfValues);
                    if (tableColumn.IsReadOnly)
                    {
                        SetCellReadOnly(cell, null);
                    }
                }
            }
        }

        protected bool Validate()
        {
            foreach(ListRow listRow in ListObject.ListRows)
            {
                var range = listRow.Range;
                if (TableColumns.Any(tableColumn => !range.Item(1, tableColumn.Index).Validation.Value))
                {
                    return false;
                }
            }
            return true;
        }

        protected void ApplyValidation(Range cell, string rangeName, Type type, bool isRequired, bool hasFixedSetOfValues)
        {
            ApplyValidationInternal(cell, rangeName, type, isRequired, hasFixedSetOfValues);
        }

        protected void ApplyDefaultValidation(Range cell)
        {
            //ApplyValidationInternal(cell, );
        }

        protected void ApplyNumberFormat(Range cell, Type type)
        {
            cell.NumberFormat = GetNumberFormat(type);
        }

        protected void SetCellReadOnly(Range cell, string value)
        {
            SetCellReadOnly(cell, value, true, false);
        }

        protected void SetCellReadOnly(Range cell, string value, bool ignoreBlank, bool showDropdown)
        {
            SetCellReadOnly(cell, value, ignoreBlank, showDropdown, null);
        }

        protected void SetCellReadOnly(Range cell, string value, bool ignoreBlank, bool showDropdown, string errorMessage)
        {
            if (cell == null)
                throw new ArgumentNullException("cell");
            Validation validation = cell.Validation;
            if (string.IsNullOrEmpty(value))
            {
                SetCellEmptyReadOnly(cell, ignoreBlank, errorMessage);
            }
            else
            {
                errorMessage = string.IsNullOrEmpty(errorMessage) ? Strings.ValidationReadOnlyError : errorMessage;
                ApplyListValidation(validation, value, ignoreBlank, showDropdown, Strings.ValidationReadOnlyTitle,
                                    errorMessage, Strings.ValidationReadOnly);
            }
        }

        private void ApplyDefaultListValidation()
        {
            Range insertRowRange = ListObject.InsertRowRange;
            if (insertRowRange != null)
            {
                insertRowRange.Validation.Delete();
            }
            ApplyHeaderValidation();
        }

        private void ApplyValidation(Range cell, IList allowedValues)
        {
            if (cell == null)
                throw new ArgumentNullException("cell");
            if (allowedValues == null)
                throw new ArgumentNullException("allowedValues");
        }

        private void ApplyListValidation(Validation validation, string rangeName, bool ignoreBlank, bool showDropdown, string errorTitle, string errorMessage, string inputMessage)
        {
            validation.Delete();
            validation.Add(XlDVType.xlValidateList, XlDVAlertStyle.xlValidAlertStop, XlFormatConditionOperator.xlEqual,
                           rangeName, Type.Missing);
            validation.IgnoreBlank = ignoreBlank;
            validation.InCellDropdown = showDropdown;
            if (!string.IsNullOrEmpty(errorTitle) && !string.IsNullOrEmpty(errorMessage))
            {
                validation.ErrorTitle = errorTitle;
                validation.ErrorMessage = errorMessage;
            }
            else
            {
                validation.ShowError = false;
            }
            if (!string.IsNullOrEmpty(inputMessage))
            {
                validation.InputMessage = inputMessage;
            }
        }

        private void ApplyValidationInternal(Range cell, string rangeName, Type type, bool isRequired, bool hasFixedSetOfValues)
        {
            Validation validation = cell.Validation;
            if (!string.IsNullOrEmpty(rangeName))
            {
                ApplyNumberFormat(cell, type);
                rangeName = "=" + rangeName;
                if (hasFixedSetOfValues)
                {
                    string errorTitle = Strings.ValidationUnsupportedValueTitle;
                    string errorMessage = Strings.ValidationUnsupportedValueError;
                    ApplyListValidation(validation, rangeName, true, true, errorTitle, errorMessage, null);
                }
                else
                {
                    ApplyListValidation(validation, rangeName, !isRequired, true, null, null, null);
                }
            }
            else
            {
                ApplyNumberFormat(cell, type);
                ApplyTypeValidation(cell, validation, type, isRequired);
            }
        }

        private void ApplyTypeValidation(Range cell, Validation validation, Type type, bool isRequired)
        {
            if (type == typeof(string))
            {
                ApplyStringTypeValidation(validation, isRequired);
            }
            else if((type == typeof(short) || type == typeof(int) || type == typeof(long) || type == typeof(ushort) || type == typeof(byte) 
                || type == typeof(sbyte) || type == typeof(uint) || type == typeof(ulong)))
            {
                ApplyIntegerTypeValidation(validation, isRequired);
            }
            else if(isRequired)
            {
                ApplyRequiredTypeValidation(cell, validation);
            }
        }

        private void ApplyRequiredTypeValidation(Range cell, Validation validation)
        {
            object cellText = cell.Text;
            if (cellText == null || cellText.ToString().Trim().Length == 0)
            {
                ApplyListValidation(cell.Validation, " ", false, false, null, null, null);
            }
            else
            {
                validation.Delete();
            }
        }

        private void ApplyIntegerTypeValidation(Validation validation, bool isRequired)
        {
            validation.Delete();
            validation.Add(XlDVType.xlValidateWholeNumber, XlDVAlertStyle.xlValidAlertStop,
                           XlFormatConditionOperator.xlBetween, int.MinValue, int.MaxValue);
            validation.ShowError = false;
            validation.IgnoreBlank = !isRequired;
        }

        private void ApplyStringTypeValidation(Validation validation, bool isRequired)
        {
            validation.Delete();
            validation.Add(XlDVType.xlValidateTextLength, XlDVAlertStyle.xlValidAlertStop,
                           XlFormatConditionOperator.xlGreaterEqual, isRequired ? 1 : 0, Type.Missing);
            validation.ShowError = false;
            validation.IgnoreBlank = !isRequired;
        }

        private void ApplyHeaderValidation()
        {
            int columnsCount = ListObject.ListColumns.Count;
            var headerRowRange = ListObject.HeaderRowRange;
            for (int i = 1; i <= columnsCount; i++)
            {
                Range cell = (Range)headerRowRange[1, i];
                SetCellReadOnly(cell, cell.Value2.ToString());
            }
        }

        private string GetNumberFormat(Type type)
        {
            if (type == typeof(string))
            {
                return "@";
            }
            if((type == typeof(short) || type == typeof(int) || type == typeof(long) || type == typeof(ushort) || type == typeof(byte) 
                || type == typeof(sbyte) || type == typeof(uint) || type == typeof(ulong)))
            {
                return "0";
            }
            if (type == typeof(DateTime))
            {
                return "m/d/yyyy h:mm";
            }
            return "General";
        }

        private void SetCellEmptyReadOnly(Range cell, bool ignoreBlank, string errorMessage)
        {
            errorMessage = string.IsNullOrEmpty(errorMessage) ? Strings.ValidationReadOnlyError : errorMessage;
            ApplyListValidation(cell.Validation, " ", ignoreBlank, false, Strings.ValidationReadOnlyTitle, errorMessage,
                                Strings.ValidationReadOnly);
        }
    }
}
