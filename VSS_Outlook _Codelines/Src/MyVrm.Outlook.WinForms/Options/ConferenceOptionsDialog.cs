﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System.ComponentModel;

namespace MyVrm.Outlook.WinForms.Options
{
    public partial class ConferenceOptionsDialog : Dialog
    {
        public ConferenceOptionsDialog()
        {
            InitializeComponent();
			Text = WinForms.Strings.ConferenceOptionsLableText;
			ApplyEnabled = false;
			ApplyVisible = false;
            appendConferenceDetailsToMeetingCheckEdit.Properties.Caption = WinForms.Strings.AppendConferenceDetailsToAMeetingRequestCaptionText;
            showPopupConferenceIsScheduledCheckEdit.Properties.Caption = WinForms.Strings.ShowPopupWhenAConferenceIsSuccessfullyScheduledCaptionText;

			checkConfirmDeletion.Properties.Caption = WinForms.Strings.ConfirmDeletionCaptionText;
			checkNotDeleteNoConfirm.Properties.Caption = WinForms.Strings.NotDeleteNoConfirmCaptionText;
			checkDeleteNoConfirm.Properties.Caption = WinForms.Strings.DeleteNoConfirmCaptionText;
			layoutControlDelitionOptions.Text = WinForms.Strings.DelitionOptionsCaptionText;
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false),
         EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShowPopupConferenceIsScheduled
        {
            get
            {
                return showPopupConferenceIsScheduledCheckEdit.Checked;
            }
            set
            {
                showPopupConferenceIsScheduledCheckEdit.Checked = value;
            }
        }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false),
         EditorBrowsable(EditorBrowsableState.Never)]
        public bool AppendConferenceDetailsToMeeting
        {
            get
            {
                return appendConferenceDetailsToMeetingCheckEdit.Checked;
            }
            set
            {
                appendConferenceDetailsToMeetingCheckEdit.Checked = value;
            }
        }

		public enum DeletionOption
		{
			NoConfirmNoDelete = 0,
			NeedConfirm = 1,
            NoConfirmDelete = 2 //102166
		}
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false),
		 EditorBrowsable(EditorBrowsableState.Never)]
		public DeletionOption DeletionOptionValue
		{
			get
			{
                DeletionOption val = DeletionOption.NoConfirmDelete; //102166
				if (checkNotDeleteNoConfirm.Checked)
					val = DeletionOption.NoConfirmNoDelete;
				if (checkDeleteNoConfirm.Checked)
					val = DeletionOption.NoConfirmDelete;
				if (checkConfirmDeletion.Checked)
					val = DeletionOption.NeedConfirm;
				return val;
			}
			set
			{
				switch(value)
				{
					case DeletionOption.NoConfirmNoDelete:
						checkNotDeleteNoConfirm.Checked = true;
						break;
					case DeletionOption.NoConfirmDelete:
						checkDeleteNoConfirm.Checked = true;
						break;
					case DeletionOption.NeedConfirm:
						checkConfirmDeletion.Checked = true;
						break;
				}
			}
		}

		static bool _blockCheck = false;
		private void CheckDeletionOption(object sender, System.EventArgs e)
		{
			if (_blockCheck)
				return;
			if (sender.Equals(checkConfirmDeletion))
			{
				_blockCheck = true;
				if (checkNotDeleteNoConfirm.Checked)
					checkNotDeleteNoConfirm.Checked = false;
				if (checkDeleteNoConfirm.Checked)
					checkDeleteNoConfirm.Checked = false;
				_blockCheck = false;
			}
			else
			{
				if (sender.Equals(checkNotDeleteNoConfirm))
				{
					_blockCheck = true;
					if (checkConfirmDeletion.Checked)
						checkConfirmDeletion.Checked = false;
					if (checkDeleteNoConfirm.Checked)
						checkDeleteNoConfirm.Checked = false;
					_blockCheck = false;
				}
				else
				{
					if (sender.Equals(checkDeleteNoConfirm))
					{
						_blockCheck = true;
						if (checkConfirmDeletion.Checked)
							checkConfirmDeletion.Checked = false;
						if (checkNotDeleteNoConfirm.Checked)
							checkNotDeleteNoConfirm.Checked = false;
						_blockCheck = false;
					}
				}
			}
		}
    }
}
