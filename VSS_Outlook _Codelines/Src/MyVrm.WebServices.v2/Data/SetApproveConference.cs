﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace MyVrm.WebServices.Data
{
	internal class SetApproveConferenceRequest : ServiceRequestBase<SetApproveConferenceResponse>
	{
		private List<ConferenceToApprove> _confList;
		public List<ConferenceToApprove> ApprovedConfList
		{
			set { _confList = value; }
			get { return _confList; }
		}

		static string GetUILangAbbr()
		{
			switch (Thread.CurrentThread.CurrentUICulture.LCID)
			{
				default:
				case 1033:	//English (US)
				case 2057:	//English (UK)
					return "en";
				case 1036:	//French (France)
					return "fr";
				case 4:		//Chinese (Simplified)
				case 1028:	//Chinese (Taiwan)  
				case 2052:	//Chinese (People's Republic of China)
				case 3076:	//Chinese (Hong Kong S.A.R.)
				case 4100:	//Chinese (Singapore)
				case 5124:	//Chinese (Macao S.A.R.)
				case 31748:	//Chinese (Traditional)
					return "ch";
			}
		}

		public SetApproveConferenceRequest(MyVrmService service)
			: base(service)
		{
		}

		#region Overrides of ServiceRequestBase<SetApproveConferenceResponse>

		internal override string GetXmlElementName()
		{
			return "approveConference";
		}

		internal override string GetCommandName()
		{
			return Constants.SetApproveConferenceCommandName; 
		}

		internal override string GetResponseXmlElementName()
		{
			return "success";
		}

		internal override SetApproveConferenceResponse ParseResponse(MyVrmServiceXmlReader reader)
		{
			var response = new SetApproveConferenceResponse();
			response.ReadElementsFromXml(reader);
			return response;
		}
        
		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			//if (OrganizationId != null)
			//    OrganizationId.WriteToXml(writer, "organizationID");
			if (UserId != null)
				UserId.WriteToXml(writer, "userID");

			writer.WriteElementValue(XmlNamespace.NotSpecified, "language", GetUILangAbbr());

			writer.WriteStartElement(XmlNamespace.NotSpecified, "conferences");
			foreach (ConferenceToApprove approvalConference in _confList)
			{
				approvalConference.WriteElementsToXml(writer);
			}

			writer.WriteEndElement();
		}
		#endregion
	}

	//Response
	public class SetApproveConferenceResponse : ServiceResponse
	{
		internal bool Success { get; set; }

		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{
			Success = Utilities.BoolStringToBool(reader.ReadValue());
		}
	}
}