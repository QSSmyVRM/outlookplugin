﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace MyVrm.WebServices.Data
{
    public class LdapSyncSchedule : ComplexProperty
    {
        public TimeSpan Time { get; set; }
        public string Days { get; set; }

		public override object Clone()
		{
			LdapSyncSchedule copy = new LdapSyncSchedule();
			copy.Days = string.Copy(Days);
			copy.Time = new TimeSpan(Time.Ticks);

			return copy;
		}
        internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
        {
            switch(reader.LocalName)
            {
                case "Time":
                {
                    Time = Utilities.TimeOfDayStringToTimeSpan(reader.ReadElementValue());
                    return true;
                }
                case "Days":
                {
                    Days = reader.ReadElementValue();
                    return true;
                }
            }
            return false;
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            writer.WriteElementValue(XmlNamespace.NotSpecified, "Time", Utilities.TimeSpanToTimeOfDayString(Time));
            writer.WriteElementValue(XmlNamespace.NotSpecified, "Days", Days);
        }
    }
}
