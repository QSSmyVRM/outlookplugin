﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;

namespace MyVrm.WebServices.Data
{
    internal class GetInventoryDetailsRequest : ServiceRequestBase<GetInventoryDetailsResponse>
    {
        internal GetInventoryDetailsRequest(MyVrmService service) : base(service)
        {
        }

        internal int SetId { get; set; }
        
        internal RoomSetType SetType { get; set; }


        #region Overrides of ServiceRequestBase

        internal override string GetXmlElementName()
        {
            return "login";
        }

        internal override string GetCommandName()
        {
            return "GetInventoryDetails";
        }

        internal override string GetResponseXmlElementName()
        {
            return "Inventory";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            UserId.WriteToXml(writer);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "Type", SetType);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "ID", SetId);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "StartByDate", "");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "StartByTime", "");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "CompleteByDate", "");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "CompleteByTime", "");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "Timezone", "");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "WorkorderID", "");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "PreselectedItems", "");
        }

        #endregion
    }
}
