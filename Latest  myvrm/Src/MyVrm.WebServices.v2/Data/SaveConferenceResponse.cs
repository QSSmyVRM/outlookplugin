﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    internal class SaveConferenceResponse : ServiceResponse
    {
        internal ConferenceId ConferenceId { get; private set; }
		public ParticipantCollection OutExternalParticipants { get; private set; }
        public ConfGuestRoomCollection OutConfRoomCollection { get; private set; }// ZD 101343 

        internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
        {
            reader.ReadStartElement(XmlNamespace.NotSpecified, "conferences");
            reader.ReadStartElement(XmlNamespace.NotSpecified, "conference");
            do
            {
                reader.Read();
                if (reader.IsStartElement(XmlNamespace.NotSpecified, "confID"))
                {
                    var conferenceId = new ConferenceId();
                    conferenceId.LoadFromXml(reader, "confID");
                    ConferenceId = conferenceId;
                }
				else 
				{
                    if (reader.IsStartElement(XmlNamespace.NotSpecified, "invited"))
                    {
                        OutExternalParticipants = new ParticipantCollection();
                        while (!reader.IsEndElement(XmlNamespace.NotSpecified, "invited"))
                        {
                            reader.Read();
                            if (reader.IsStartElement(XmlNamespace.NotSpecified, "party"))
                            {
                                Participant user = new Participant();
                                user.LoadFromXml(reader, "party");
                                OutExternalParticipants.Add(user);
                            }
                        }
                    }
                    // ZD 101343 starts
                    
                    if (reader.IsStartElement(XmlNamespace.NotSpecified, "GuestRooms"))
                    {
                            OutConfRoomCollection = new ConfGuestRoomCollection();
                            while (!reader.IsEndElement(XmlNamespace.NotSpecified, "GuestRooms"))
                            {
                                reader.Read();
                                if (reader.IsStartElement(XmlNamespace.NotSpecified, "Endpoint" ))                                                                                             
                                {
                                    ConfGuestRoom guestRm = new ConfGuestRoom();
                                    guestRm.LoadFromXml(reader, "Endpoint");
                                    OutConfRoomCollection.Add(guestRm);
                                }
                            }
                      }
                       else
                        {
                            reader.SkipCurrentElement();
                        }

                    // ZD 101343 Ends
                     
				}
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "conference"));
            reader.ReadEndElement(XmlNamespace.NotSpecified, "conferences");
        }
    }
}
