﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrmAddin2007
{
	partial class MainRibbon
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.tabMyVrm = this.Factory.CreateRibbonTab();
            this.groupShow = this.Factory.CreateRibbonGroup();
            this.conferenceButton = this.Factory.CreateRibbonButton();
            this.templatesGroup = this.Factory.CreateRibbonGroup();
            this.templateMenu = this.Factory.CreateRibbonMenu();
            this.generalGroup = this.Factory.CreateRibbonGroup();
            this.approveConferenceButton = this.Factory.CreateRibbonButton();
            this.roomsCalendarButton = this.Factory.CreateRibbonButton();
            this.preferencesGroup = this.Factory.CreateRibbonGroup();
            this.favoriteRoomsButton = this.Factory.CreateRibbonButton();
            this.optionsGroup = this.Factory.CreateRibbonGroup();
            this.optionsButton = this.Factory.CreateRibbonButton();
			this.tabMyVrm.SuspendLayout();
			this.groupShow.SuspendLayout();
			this.templatesGroup.SuspendLayout();
			this.generalGroup.SuspendLayout();
			this.preferencesGroup.SuspendLayout();
			this.optionsGroup.SuspendLayout();
			this.SuspendLayout();
			// 
			// tabMyVrm
			// 
			this.tabMyVrm.Groups.Add(this.groupShow);
			this.tabMyVrm.Groups.Add(this.templatesGroup);
			this.tabMyVrm.Groups.Add(this.generalGroup);
			this.tabMyVrm.Groups.Add(this.preferencesGroup);
			this.tabMyVrm.Groups.Add(this.optionsGroup);
			this.tabMyVrm.Label = "myVRM";
			this.tabMyVrm.Name = "tabMyVrm";
			// 
			// groupShow
			// 
			this.groupShow.Items.Add(this.conferenceButton);
			this.groupShow.Label = "Show";
			this.groupShow.Name = "groupShow";
			// 
			// conferenceButton
			// 
			this.conferenceButton.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
			this.conferenceButton.Image = global::MyVrmAddin2007.Properties.Resources.Logo65x65;
			this.conferenceButton.Label = "Conference";
			this.conferenceButton.Name = "conferenceButton";
			this.conferenceButton.ShowImage = true;
			this.conferenceButton.Click +=conferenceButton_Click;
			// 
			// templatesGroup
			// 
			this.templatesGroup.Items.Add(this.templateMenu);
			this.templatesGroup.Label = "Templates";
			this.templatesGroup.Name = "templatesGroup";
			// 
			// templateMenu
			// 
			this.templateMenu.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
			this.templateMenu.Dynamic = true;
			this.templateMenu.Label = "Create From Template";
			this.templateMenu.Name = "templateMenu";
			this.templateMenu.OfficeImageId = "CreateForm";
			this.templateMenu.ShowImage = true;
			this.templateMenu.ItemsLoading +=templateMenu_ItemsLoading;
			// 
			// generalGroup
			// 
			this.generalGroup.Items.Add(this.approveConferenceButton);
			this.generalGroup.Items.Add(this.roomsCalendarButton);
			this.generalGroup.Label = "General";
			this.generalGroup.Name = "generalGroup";
			// 
			// approveConferenceButton
			// 
			this.approveConferenceButton.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
			this.approveConferenceButton.Image = global::MyVrmAddin2007.Properties.Resources.approveConf32;
			this.approveConferenceButton.Label = "Conference Approval";
			this.approveConferenceButton.Name = "approveConferenceButton";
			this.approveConferenceButton.OfficeImageId = "GroupTracking";
			this.approveConferenceButton.ShowImage = true;
			this.approveConferenceButton.Click +=approveConferenceButton_Click;
			// 
			// roomsCalendarButton
			// 
			this.roomsCalendarButton.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
			this.roomsCalendarButton.Label = "Rooms Calendar";
			this.roomsCalendarButton.Name = "roomsCalendarButton";
			this.roomsCalendarButton.OfficeImageId = "OpenAttachedCalendar";
			this.roomsCalendarButton.ShowImage = true;
			this.roomsCalendarButton.Click +=roomsCalendarButton_Click;
			// 
			// preferencesGroup
			// 
			this.preferencesGroup.Items.Add(this.favoriteRoomsButton);
			this.preferencesGroup.Label = "Preferences";
			this.preferencesGroup.Name = "preferencesGroup";
			// 
			// favoriteRoomsButton
			// 
			this.favoriteRoomsButton.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
			this.favoriteRoomsButton.Image = global::MyVrmAddin2007.Properties.Resources.Favorites;
			this.favoriteRoomsButton.Label = "Favorite Rooms";
			this.favoriteRoomsButton.Name = "favoriteRoomsButton";
			this.favoriteRoomsButton.ShowImage = true;
			this.favoriteRoomsButton.Click +=favoriteRoomsButton_Click;
			// 
			// optionsGroup
			// 
			this.optionsGroup.Items.Add(this.optionsButton);
			this.optionsGroup.Label = "Options";
			this.optionsGroup.Name = "optionsGroup";
			// 
			// optionsButton
			// 
			this.optionsButton.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
			this.optionsButton.Label = "Options";
			this.optionsButton.Name = "optionsButton";
			this.optionsButton.OfficeImageId = "ControlsGallery";
			this.optionsButton.ShowImage = true;
			this.optionsButton.Click +=optionsButton_Click;
			// 
			// MainRibbon
			// 
			this.Name = "MainRibbon";
			this.RibbonType = "Microsoft.Outlook.Explorer";
			this.Tabs.Add(this.tabMyVrm);
			this.Load +=MainRibbon_Load;
			this.tabMyVrm.ResumeLayout(false);
			this.tabMyVrm.PerformLayout();
			this.groupShow.ResumeLayout(false);
			this.groupShow.PerformLayout();
			this.templatesGroup.ResumeLayout(false);
			this.templatesGroup.PerformLayout();
			this.generalGroup.ResumeLayout(false);
			this.generalGroup.PerformLayout();
			this.preferencesGroup.ResumeLayout(false);
			this.preferencesGroup.PerformLayout();
			this.optionsGroup.ResumeLayout(false);
			this.optionsGroup.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private Microsoft.Office.Tools.Ribbon.RibbonTab tabMyVrm;
		internal Microsoft.Office.Tools.Ribbon.RibbonGroup groupShow;
		internal Microsoft.Office.Tools.Ribbon.RibbonButton favoriteRoomsButton;
		internal Microsoft.Office.Tools.Ribbon.RibbonButton roomsCalendarButton;
		internal Microsoft.Office.Tools.Ribbon.RibbonButton approveConferenceButton;
		internal Microsoft.Office.Tools.Ribbon.RibbonButton conferenceButton;
		internal Microsoft.Office.Tools.Ribbon.RibbonMenu templateMenu;
		internal Microsoft.Office.Tools.Ribbon.RibbonButton optionsButton;
		internal Microsoft.Office.Tools.Ribbon.RibbonGroup templatesGroup;
		internal Microsoft.Office.Tools.Ribbon.RibbonGroup generalGroup;
		internal Microsoft.Office.Tools.Ribbon.RibbonGroup preferencesGroup;
		internal Microsoft.Office.Tools.Ribbon.RibbonGroup optionsGroup;

	}

	partial class ThisRibbonCollection : Microsoft.Office.Tools.Ribbon.RibbonReadOnlyCollection
	{
		internal MainRibbon MainRibbon
		{
			get { return this.GetRibbon<MainRibbon>(); }
		}
	}
}
