﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Runtime.Serialization;

namespace MyVrm.WebServices.Data
{
    [Serializable]
    public class ServiceRequestException : Exception
    {
        public ServiceRequestException()
        {
        }

        public ServiceRequestException(string message) : base(message)
        {
        }

        public ServiceRequestException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected ServiceRequestException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
