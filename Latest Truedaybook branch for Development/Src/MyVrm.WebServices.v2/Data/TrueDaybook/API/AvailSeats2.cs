﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	public class AvailSeats2
	{
		public DateTime Start;
		public DateTime End;
		public int Seats;
	}

	public class AvailSeatsCollection2 : ICollection<AvailSeats2>
	{
		private readonly List<AvailSeats2> _availSeats = new List<AvailSeats2>();

		#region Implementation of IEnumerable

		public IEnumerator<AvailSeats2> GetEnumerator()
		{
			return _availSeats.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		#endregion

		#region Implementation of ICollection<AvailSeats>

		public void Add(AvailSeats2 item)
		{
			_availSeats.Add(item);
		}

		public void Clear()
		{
			_availSeats.Clear();
		}

		public bool Contains(AvailSeats2 item)
		{
			return _availSeats.Contains(item);
		}

		public void CopyTo(AvailSeats2[] array, int arrayIndex)
		{
			_availSeats.CopyTo(array, arrayIndex);
		}

		public bool Remove(AvailSeats2 item)
		{
			return _availSeats.Remove(item);
		}

		public int Count
		{
			get { return _availSeats.Count; }
		}

		public bool IsReadOnly
		{
			get { return false; }
		}

		#endregion
	}
}
