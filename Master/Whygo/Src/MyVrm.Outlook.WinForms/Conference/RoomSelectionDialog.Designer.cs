﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.Outlook.WinForms.Conference
{
	partial class RoomSelectionDialog
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RoomSelectionDialog));
			this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
			this.calendarButton = new DevExpress.XtraEditors.SimpleButton();
			this.changeViewButton = new DevExpress.XtraEditors.DropDownButton();
			this.dataLoadingLabelControl = new DevExpress.XtraEditors.LabelControl();
			this.removeRoomButton = new DevExpress.XtraEditors.SimpleButton();
			this.selectRoomButton = new DevExpress.XtraEditors.SimpleButton();
			this.selectedRoomsListBox = new DevExpress.XtraEditors.ListBoxControl();
			this.showOnlyFavoritesCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.roomsTreeList = new CustomDevExpressTreeList();
			this.advancedInfoColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.advancedInforepositoryItem = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
			this.approval2Column = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.repositoryApproval = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
			this.imageCollection = new DevExpress.Utils.ImageCollection(this.components);
			this.roomNameColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.addressColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.floorColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.cityColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.stateColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.zipCodeColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.phoneNumberColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.mediaTypeColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.assistantColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.middleTierColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.capacityColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.searchByComboBox = new DevExpress.XtraEditors.ComboBoxEdit();
			this.checkEditShowOnlyPublic = new DevExpress.XtraEditors.CheckEdit();
			this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.searchByLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
			this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.dataLoadingLayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
			this.searchValueLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
			this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.layoutRoomsCalendarItem = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItemShowOnlyPublic = new DevExpress.XtraLayout.LayoutControlItem();
			this.roomDetailsWorker = new System.ComponentModel.BackgroundWorker();
			((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).BeginInit();
			this.ContentPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
			this.layoutControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.selectedRoomsListBox)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.showOnlyFavoritesCheckEdit.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.roomsTreeList)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.advancedInforepositoryItem)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.repositoryApproval)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imageCollection)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.searchByComboBox.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.checkEditShowOnlyPublic.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.searchByLayoutControlItem)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataLoadingLayoutItem)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.searchValueLayoutControlItem)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutRoomsCalendarItem)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItemShowOnlyPublic)).BeginInit();
			this.SuspendLayout();
			// 
			// ContentPanel
			// 
			this.ContentPanel.Controls.Add(this.layoutControl1);
			this.ContentPanel.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
			this.ContentPanel.Size = new System.Drawing.Size(704, 490);
			// 
			// layoutControl1
			// 
			this.layoutControl1.AllowCustomizationMenu = false;
			this.layoutControl1.Controls.Add(this.calendarButton);
			this.layoutControl1.Controls.Add(this.changeViewButton);
			this.layoutControl1.Controls.Add(this.dataLoadingLabelControl);
			this.layoutControl1.Controls.Add(this.removeRoomButton);
			this.layoutControl1.Controls.Add(this.selectRoomButton);
			this.layoutControl1.Controls.Add(this.selectedRoomsListBox);
			this.layoutControl1.Controls.Add(this.showOnlyFavoritesCheckEdit);
			this.layoutControl1.Controls.Add(this.roomsTreeList);
			this.layoutControl1.Controls.Add(this.searchByComboBox);
			this.layoutControl1.Controls.Add(this.checkEditShowOnlyPublic);
			this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.layoutControl1.Location = new System.Drawing.Point(0, 0);
			this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.layoutControl1.Name = "layoutControl1";
			this.layoutControl1.Root = this.layoutControlGroup1;
			this.layoutControl1.Size = new System.Drawing.Size(704, 490);
			this.layoutControl1.TabIndex = 0;
			this.layoutControl1.Text = "layoutControl1";
			// 
			// calendarButton
			// 
			this.calendarButton.Image = global::MyVrm.Outlook.WinForms.Properties.Resources.Calendar_scheduleHS;
			this.calendarButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
			this.calendarButton.Location = new System.Drawing.Point(584, 428);
			this.calendarButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.calendarButton.Name = "calendarButton";
			this.calendarButton.Size = new System.Drawing.Size(118, 44);
			this.calendarButton.StyleController = this.layoutControl1;
			this.calendarButton.TabIndex = 21;
			this.calendarButton.Text = "[Calendar]";
			this.calendarButton.Click += new System.EventHandler(this.calendarButton_Click);
			// 
			// changeViewButton
			// 
			this.changeViewButton.Location = new System.Drawing.Point(590, 77);
			this.changeViewButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.changeViewButton.Name = "changeViewButton";
			this.changeViewButton.Size = new System.Drawing.Size(112, 23);
			this.changeViewButton.StyleController = this.layoutControl1;
			this.changeViewButton.TabIndex = 20;
			this.changeViewButton.Text = "[Change View]";
			this.changeViewButton.Click += new System.EventHandler(this.changeViewButton_Click);
			// 
			// dataLoadingLabelControl
			// 
			this.dataLoadingLabelControl.Appearance.BackColor = System.Drawing.SystemColors.Info;
			this.dataLoadingLabelControl.Appearance.BorderColor = System.Drawing.SystemColors.ActiveBorder;
			this.dataLoadingLabelControl.Appearance.Image = global::MyVrm.Outlook.WinForms.Properties.Resources.information;
			this.dataLoadingLabelControl.Appearance.Options.UseBackColor = true;
			this.dataLoadingLabelControl.Appearance.Options.UseBorderColor = true;
			this.dataLoadingLabelControl.Appearance.Options.UseImage = true;
			this.dataLoadingLabelControl.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
			this.dataLoadingLabelControl.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
			this.dataLoadingLabelControl.Location = new System.Drawing.Point(2, 53);
			this.dataLoadingLabelControl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.dataLoadingLabelControl.Name = "dataLoadingLabelControl";
			this.dataLoadingLabelControl.Size = new System.Drawing.Size(700, 20);
			this.dataLoadingLabelControl.StyleController = this.layoutControl1;
			this.dataLoadingLabelControl.TabIndex = 19;
			this.dataLoadingLabelControl.Text = "[Updating...]";
			// 
			// removeRoomButton
			// 
			this.removeRoomButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
			this.removeRoomButton.Location = new System.Drawing.Point(584, 349);
			this.removeRoomButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.removeRoomButton.Name = "removeRoomButton";
			this.removeRoomButton.Size = new System.Drawing.Size(118, 23);
			this.removeRoomButton.StyleController = this.layoutControl1;
			this.removeRoomButton.TabIndex = 15;
			this.removeRoomButton.Text = "[Remove]";
			this.removeRoomButton.Click += new System.EventHandler(this.removeRoomButton_Click);
			// 
			// selectRoomButton
			// 
			this.selectRoomButton.Location = new System.Drawing.Point(2, 349);
			this.selectRoomButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.selectRoomButton.Name = "selectRoomButton";
			this.selectRoomButton.Size = new System.Drawing.Size(118, 23);
			this.selectRoomButton.StyleController = this.layoutControl1;
			this.selectRoomButton.TabIndex = 14;
			this.selectRoomButton.Text = "[Rooms->]";
			this.selectRoomButton.Click += new System.EventHandler(this.selectRoomButton_Click);
			// 
			// selectedRoomsListBox
			// 
			this.selectedRoomsListBox.Location = new System.Drawing.Point(124, 349);
			this.selectedRoomsListBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.selectedRoomsListBox.MultiColumn = true;
			this.selectedRoomsListBox.Name = "selectedRoomsListBox";
			this.selectedRoomsListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
			this.selectedRoomsListBox.Size = new System.Drawing.Size(456, 123);
			this.selectedRoomsListBox.StyleController = this.layoutControl1;
			this.selectedRoomsListBox.TabIndex = 13;
			this.selectedRoomsListBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.selectedRoomsListBox_KeyDown);
			// 
			// showOnlyFavoritesCheckEdit
			// 
			this.showOnlyFavoritesCheckEdit.Location = new System.Drawing.Point(521, 2);
			this.showOnlyFavoritesCheckEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.showOnlyFavoritesCheckEdit.Name = "showOnlyFavoritesCheckEdit";
			this.showOnlyFavoritesCheckEdit.Properties.Caption = "[Show Only Favorites]";
			this.showOnlyFavoritesCheckEdit.Size = new System.Drawing.Size(181, 21);
			this.showOnlyFavoritesCheckEdit.StyleController = this.layoutControl1;
			this.showOnlyFavoritesCheckEdit.TabIndex = 10;
			this.showOnlyFavoritesCheckEdit.CheckedChanged += new System.EventHandler(this.showOnlyFavoritesCheckEdit_CheckedChanged);
			// 
			// roomsTreeList
			// 
			this.roomsTreeList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.advancedInfoColumn,
            this.approval2Column,
            this.roomNameColumn,
            this.addressColumn,
            this.floorColumn,
            this.cityColumn,
            this.stateColumn,
            this.zipCodeColumn,
            this.phoneNumberColumn,
            this.mediaTypeColumn,
            this.assistantColumn,
            this.middleTierColumn,
            this.capacityColumn});
			this.roomsTreeList.FixedLineWidth = 1;
			this.roomsTreeList.Location = new System.Drawing.Point(2, 104);
			this.roomsTreeList.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.roomsTreeList.Name = "roomsTreeList";
			this.roomsTreeList.OptionsBehavior.PopulateServiceColumns = true;
			this.roomsTreeList.OptionsLayout.LayoutVersion = "5";
			this.roomsTreeList.OptionsLayout.RemoveOldColumns = true;
			this.roomsTreeList.OptionsSelection.EnableAppearanceFocusedCell = false;
			this.roomsTreeList.OptionsSelection.MultiSelect = true;
			this.roomsTreeList.OptionsView.AutoWidth = false;
			this.roomsTreeList.OptionsView.ShowFocusedFrame = false;
			this.roomsTreeList.OptionsView.ShowHorzLines = false;
			this.roomsTreeList.OptionsView.ShowIndicator = false;
			this.roomsTreeList.OptionsView.ShowRoot = false;
			this.roomsTreeList.OptionsView.ShowVertLines = false;
			this.roomsTreeList.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.advancedInforepositoryItem,
            this.repositoryApproval});
			this.roomsTreeList.SelectImageList = this.imageCollection;
			this.roomsTreeList.Size = new System.Drawing.Size(700, 241);
			this.roomsTreeList.TabIndex = 7;
			this.roomsTreeList.LayoutUpgrade +=roomsTreeList_LayoutUpgrade;
			this.roomsTreeList.DoubleClick += new System.EventHandler(this.roomsTreeList_DoubleClick);
			this.roomsTreeList.ShowTreeListMenu += new DevExpress.XtraTreeList.TreeListMenuEventHandler(this.roomsTreeList_ShowTreeListMenu);
			// 
			// advancedInfoColumn
			// 
			this.advancedInfoColumn.Caption = " ";
			this.advancedInfoColumn.ColumnEdit = this.advancedInforepositoryItem;
			this.advancedInfoColumn.FieldName = "MediaType";
			this.advancedInfoColumn.MinWidth = 50;
			this.advancedInfoColumn.Name = "advancedInfoColumn";
			this.advancedInfoColumn.OptionsColumn.AllowMove = false;
			this.advancedInfoColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
			this.advancedInfoColumn.OptionsColumn.AllowSize = false;
			this.advancedInfoColumn.OptionsColumn.FixedWidth = true;
			this.advancedInfoColumn.OptionsColumn.ShowInCustomizationForm = false;
			this.advancedInfoColumn.Visible = true;
			this.advancedInfoColumn.VisibleIndex = 0;
			this.advancedInfoColumn.Width = 50;
			// 
			// advancedInforepositoryItem
			// 
			this.advancedInforepositoryItem.AutoHeight = false;
			this.advancedInforepositoryItem.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, global::MyVrm.Outlook.WinForms.Conference.Strings.RoomSelectionDialogShowOnlyFavoritesCheckEditText, -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::MyVrm.Outlook.WinForms.Properties.Resources.information, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Click here to view advanced room information", null, null, false)});
			this.advancedInforepositoryItem.Name = "advancedInforepositoryItem";
			this.advancedInforepositoryItem.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
			this.advancedInforepositoryItem.ButtonPressed += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.advancedInforepositoryItem_ButtonPressed);
			// 
			// approval2Column
			// 
			this.approval2Column.Caption = "[Approval]";
			this.approval2Column.ColumnEdit = this.repositoryApproval;
			this.approval2Column.FieldName = "Approval";
			this.approval2Column.Name = "approval2Column";
			this.approval2Column.OptionsColumn.AllowEdit = false;
			this.approval2Column.OptionsColumn.AllowFocus = false;
			this.approval2Column.OptionsColumn.ReadOnly = true;
			this.approval2Column.Visible = true;
			this.approval2Column.VisibleIndex = 1;
			// 
			// repositoryApproval
			// 
			this.repositoryApproval.AutoHeight = false;
			this.repositoryApproval.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.UserDefined;
			this.repositoryApproval.ImageIndexChecked = 3;
			this.repositoryApproval.ImageIndexUnchecked = 4;
			this.repositoryApproval.Images = this.imageCollection;
			this.repositoryApproval.Name = "repositoryApproval";
			// 
			// imageCollection
			// 
			this.imageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection.ImageStream")));
			this.imageCollection.Images.SetKeyName(0, "users.ico");
			this.imageCollection.Images.SetKeyName(1, "information.png");
			this.imageCollection.Images.SetKeyName(2, "video.ico");
			this.imageCollection.Images.SetKeyName(3, "checkbox.png");
			this.imageCollection.Images.SetKeyName(4, "empty.png");
			// 
			// roomNameColumn
			// 
			this.roomNameColumn.Caption = "[Room Name]";
			this.roomNameColumn.FieldName = "Name";
			this.roomNameColumn.Name = "roomNameColumn";
			this.roomNameColumn.OptionsColumn.AllowEdit = false;
			this.roomNameColumn.OptionsColumn.AllowFocus = false;
			this.roomNameColumn.OptionsColumn.AllowMove = false;
			this.roomNameColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
			this.roomNameColumn.OptionsColumn.ReadOnly = true;
			this.roomNameColumn.OptionsColumn.ShowInCustomizationForm = false;
			this.roomNameColumn.SortOrder = System.Windows.Forms.SortOrder.Ascending;
			this.roomNameColumn.Visible = true;
			this.roomNameColumn.VisibleIndex = 2;
			this.roomNameColumn.Width = 250;
			// 
			// addressColumn
			// 
			this.addressColumn.Caption = "[Street address]";
			this.addressColumn.FieldName = "Address";
			this.addressColumn.Name = "addressColumn";
			this.addressColumn.OptionsColumn.AllowEdit = false;
			this.addressColumn.OptionsColumn.AllowFocus = false;
			this.addressColumn.OptionsColumn.ReadOnly = true;
			this.addressColumn.Visible = true;
			this.addressColumn.VisibleIndex = 6;
			this.addressColumn.Width = 100;
			// 
			// floorColumn
			// 
			this.floorColumn.Caption = "[Floor]";
			this.floorColumn.FieldName = "Floor";
			this.floorColumn.Name = "floorColumn";
			this.floorColumn.OptionsColumn.AllowEdit = false;
			this.floorColumn.OptionsColumn.AllowFocus = false;
			this.floorColumn.OptionsColumn.ReadOnly = true;
			this.floorColumn.Visible = true;
			this.floorColumn.VisibleIndex = 7;
			this.floorColumn.Width = 50;
			// 
			// cityColumn
			// 
			this.cityColumn.Caption = "[City]";
			this.cityColumn.FieldName = "City";
			this.cityColumn.Name = "cityColumn";
			this.cityColumn.OptionsColumn.AllowEdit = false;
			this.cityColumn.OptionsColumn.AllowFocus = false;
			this.cityColumn.OptionsColumn.ReadOnly = true;
			this.cityColumn.Visible = true;
			this.cityColumn.VisibleIndex = 8;
			this.cityColumn.Width = 70;
			// 
			// stateColumn
			// 
			this.stateColumn.Caption = "[State]";
			this.stateColumn.FieldName = "State";
			this.stateColumn.Name = "stateColumn";
			this.stateColumn.OptionsColumn.AllowEdit = false;
			this.stateColumn.OptionsColumn.AllowFocus = false;
			this.stateColumn.OptionsColumn.ReadOnly = true;
			this.stateColumn.Visible = true;
			this.stateColumn.VisibleIndex = 9;
			this.stateColumn.Width = 50;
			// 
			// zipCodeColumn
			// 
			this.zipCodeColumn.Caption = "[Zip Code]";
			this.zipCodeColumn.FieldName = "ZipCode";
			this.zipCodeColumn.Name = "zipCodeColumn";
			this.zipCodeColumn.OptionsColumn.AllowEdit = false;
			this.zipCodeColumn.OptionsColumn.AllowFocus = false;
			this.zipCodeColumn.OptionsColumn.ReadOnly = true;
			this.zipCodeColumn.Visible = true;
			this.zipCodeColumn.VisibleIndex = 10;
			this.zipCodeColumn.Width = 70;
			// 
			// phoneNumberColumn
			// 
			this.phoneNumberColumn.Caption = "[Phone]";
			this.phoneNumberColumn.FieldName = "Phone";
			this.phoneNumberColumn.Name = "phoneNumberColumn";
			this.phoneNumberColumn.OptionsColumn.AllowEdit = false;
			this.phoneNumberColumn.OptionsColumn.AllowFocus = false;
			this.phoneNumberColumn.OptionsColumn.ReadOnly = true;
			this.phoneNumberColumn.Visible = true;
			this.phoneNumberColumn.VisibleIndex = 11;
			this.phoneNumberColumn.Width = 70;
			// 
			// mediaTypeColumn
			// 
			this.mediaTypeColumn.AppearanceCell.Options.UseTextOptions = true;
			this.mediaTypeColumn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.mediaTypeColumn.Caption = "[Room Type]";
			this.mediaTypeColumn.FieldName = "MediaType";
			this.mediaTypeColumn.Name = "mediaTypeColumn";
			this.mediaTypeColumn.OptionsColumn.AllowEdit = false;
			this.mediaTypeColumn.OptionsColumn.AllowFocus = false;
			this.mediaTypeColumn.OptionsColumn.ReadOnly = true;
			this.mediaTypeColumn.Visible = true;
			this.mediaTypeColumn.VisibleIndex = 4;
			this.mediaTypeColumn.Width = 50;
			// 
			// assistantColumn
			// 
			this.assistantColumn.Caption = "[Assistant]";
			this.assistantColumn.FieldName = "AssistantInCharge";
			this.assistantColumn.Name = "assistantColumn";
			this.assistantColumn.OptionsColumn.AllowEdit = false;
			this.assistantColumn.OptionsColumn.AllowFocus = false;
			this.assistantColumn.OptionsColumn.ReadOnly = true;
			this.assistantColumn.Visible = true;
			this.assistantColumn.VisibleIndex = 12;
			this.assistantColumn.Width = 70;
			// 
			// middleTierColumn
			// 
			this.middleTierColumn.Caption = "[Site]";
			this.middleTierColumn.FieldName = "MiddleTier";
			this.middleTierColumn.Name = "middleTierColumn";
			this.middleTierColumn.OptionsColumn.AllowEdit = false;
			this.middleTierColumn.OptionsColumn.AllowFocus = false;
			this.middleTierColumn.OptionsColumn.ReadOnly = true;
			this.middleTierColumn.Visible = true;
			this.middleTierColumn.VisibleIndex = 3;
			// 
			// capacityColumn
			// 
			this.capacityColumn.Caption = "[Capacity]";
			this.capacityColumn.FieldName = "Capacity";
			this.capacityColumn.Name = "capacityColumn";
			this.capacityColumn.OptionsColumn.AllowEdit = false;
			this.capacityColumn.OptionsColumn.AllowFocus = false;
			this.capacityColumn.OptionsColumn.ReadOnly = true;
			this.capacityColumn.Visible = true;
			this.capacityColumn.VisibleIndex = 5;
			// 
			// searchByComboBox
			// 
			this.searchByComboBox.Location = new System.Drawing.Point(81, 2);
			this.searchByComboBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.searchByComboBox.Name = "searchByComboBox";
			this.searchByComboBox.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.searchByComboBox.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
			this.searchByComboBox.Size = new System.Drawing.Size(436, 22);
			this.searchByComboBox.StyleController = this.layoutControl1;
			this.searchByComboBox.TabIndex = 17;
			this.searchByComboBox.SelectedIndexChanged += new System.EventHandler(this.searchByComboBox_SelectedIndexChanged);
			// 
			// checkEditShowOnlyPublic
			// 
			this.checkEditShowOnlyPublic.Location = new System.Drawing.Point(521, 28);
			this.checkEditShowOnlyPublic.Name = "checkEditShowOnlyPublic";
			this.checkEditShowOnlyPublic.Properties.Caption = "Show Only Public";
			this.checkEditShowOnlyPublic.Size = new System.Drawing.Size(181, 21);
			this.checkEditShowOnlyPublic.StyleController = this.layoutControl1;
			this.checkEditShowOnlyPublic.TabIndex = 22;
			this.checkEditShowOnlyPublic.CheckedChanged += new System.EventHandler(this.checkEditShowOnlyPublic_CheckedChanged);
			// 
			// layoutControlGroup1
			// 
			this.layoutControlGroup1.CustomizationFormText = "Root";
			this.layoutControlGroup1.GroupBordersVisible = false;
			this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.searchByLayoutControlItem,
            this.layoutControlItem4,
            this.layoutControlItem6,
            this.layoutControlItem5,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.emptySpaceItem2,
            this.dataLoadingLayoutItem,
            this.layoutControlItem11,
            this.searchValueLayoutControlItem,
            this.emptySpaceItem1,
            this.emptySpaceItem3,
            this.emptySpaceItem5,
            this.layoutRoomsCalendarItem,
            this.layoutControlItemShowOnlyPublic});
			this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup1.Name = "Root";
			this.layoutControlGroup1.Size = new System.Drawing.Size(704, 490);
			this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup1.Text = "Root";
			this.layoutControlGroup1.TextVisible = false;
			// 
			// searchByLayoutControlItem
			// 
			this.searchByLayoutControlItem.Control = this.searchByComboBox;
			this.searchByLayoutControlItem.CustomizationFormText = "Search:";
			this.searchByLayoutControlItem.Location = new System.Drawing.Point(0, 0);
			this.searchByLayoutControlItem.Name = "searchByLayoutControlItem";
			this.searchByLayoutControlItem.Size = new System.Drawing.Size(519, 26);
			this.searchByLayoutControlItem.Text = "[Search by:]";
			this.searchByLayoutControlItem.TextSize = new System.Drawing.Size(75, 16);
			// 
			// layoutControlItem4
			// 
			this.layoutControlItem4.Control = this.roomsTreeList;
			this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
			this.layoutControlItem4.Location = new System.Drawing.Point(0, 102);
			this.layoutControlItem4.Name = "layoutControlItem4";
			this.layoutControlItem4.Size = new System.Drawing.Size(704, 245);
			this.layoutControlItem4.Text = "layoutControlItem4";
			this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem4.TextToControlDistance = 0;
			this.layoutControlItem4.TextVisible = false;
			// 
			// layoutControlItem6
			// 
			this.layoutControlItem6.Control = this.showOnlyFavoritesCheckEdit;
			this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
			this.layoutControlItem6.Location = new System.Drawing.Point(519, 0);
			this.layoutControlItem6.Name = "layoutControlItem6";
			this.layoutControlItem6.Size = new System.Drawing.Size(185, 26);
			this.layoutControlItem6.Text = "layoutControlItem6";
			this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem6.TextToControlDistance = 0;
			this.layoutControlItem6.TextVisible = false;
			// 
			// layoutControlItem5
			// 
			this.layoutControlItem5.Control = this.selectedRoomsListBox;
			this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
			this.layoutControlItem5.Location = new System.Drawing.Point(122, 347);
			this.layoutControlItem5.MinSize = new System.Drawing.Size(54, 20);
			this.layoutControlItem5.Name = "layoutControlItem5";
			this.layoutControlItem5.Size = new System.Drawing.Size(460, 127);
			this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem5.Text = "layoutControlItem5";
			this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem5.TextToControlDistance = 0;
			this.layoutControlItem5.TextVisible = false;
			// 
			// layoutControlItem8
			// 
			this.layoutControlItem8.Control = this.selectRoomButton;
			this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
			this.layoutControlItem8.Location = new System.Drawing.Point(0, 347);
			this.layoutControlItem8.Name = "layoutControlItem8";
			this.layoutControlItem8.Size = new System.Drawing.Size(122, 27);
			this.layoutControlItem8.Text = "layoutControlItem8";
			this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem8.TextToControlDistance = 0;
			this.layoutControlItem8.TextVisible = false;
			// 
			// layoutControlItem9
			// 
			this.layoutControlItem9.Control = this.removeRoomButton;
			this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
			this.layoutControlItem9.Location = new System.Drawing.Point(582, 347);
			this.layoutControlItem9.Name = "layoutControlItem9";
			this.layoutControlItem9.Size = new System.Drawing.Size(122, 27);
			this.layoutControlItem9.Text = "layoutControlItem9";
			this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem9.TextToControlDistance = 0;
			this.layoutControlItem9.TextVisible = false;
			// 
			// emptySpaceItem2
			// 
			this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
			this.emptySpaceItem2.FillControlToClientArea = false;
			this.emptySpaceItem2.Location = new System.Drawing.Point(0, 374);
			this.emptySpaceItem2.MinSize = new System.Drawing.Size(104, 24);
			this.emptySpaceItem2.Name = "emptySpaceItem2";
			this.emptySpaceItem2.Size = new System.Drawing.Size(122, 100);
			this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.emptySpaceItem2.Text = "emptySpaceItem2";
			this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
			// 
			// dataLoadingLayoutItem
			// 
			this.dataLoadingLayoutItem.Control = this.dataLoadingLabelControl;
			this.dataLoadingLayoutItem.CustomizationFormText = "dataLoadingLayoutItem";
			this.dataLoadingLayoutItem.Location = new System.Drawing.Point(0, 51);
			this.dataLoadingLayoutItem.Name = "dataLoadingLayoutItem";
			this.dataLoadingLayoutItem.Size = new System.Drawing.Size(704, 24);
			this.dataLoadingLayoutItem.Text = "dataLoadingLayoutItem";
			this.dataLoadingLayoutItem.TextSize = new System.Drawing.Size(0, 0);
			this.dataLoadingLayoutItem.TextToControlDistance = 0;
			this.dataLoadingLayoutItem.TextVisible = false;
			// 
			// layoutControlItem11
			// 
			this.layoutControlItem11.Control = this.changeViewButton;
			this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
			this.layoutControlItem11.Location = new System.Drawing.Point(588, 75);
			this.layoutControlItem11.Name = "layoutControlItem11";
			this.layoutControlItem11.Size = new System.Drawing.Size(116, 27);
			this.layoutControlItem11.Text = "layoutControlItem11";
			this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem11.TextToControlDistance = 0;
			this.layoutControlItem11.TextVisible = false;
			// 
			// searchValueLayoutControlItem
			// 
			this.searchValueLayoutControlItem.CustomizationFormText = "searchValueLayoutControlItem";
			this.searchValueLayoutControlItem.Location = new System.Drawing.Point(0, 26);
			this.searchValueLayoutControlItem.MaxSize = new System.Drawing.Size(0, 35);
			this.searchValueLayoutControlItem.MinSize = new System.Drawing.Size(11, 25);
			this.searchValueLayoutControlItem.Name = "searchValueLayoutControlItem";
			this.searchValueLayoutControlItem.Size = new System.Drawing.Size(519, 25);
			this.searchValueLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.searchValueLayoutControlItem.Text = "[Search for:]";
			this.searchValueLayoutControlItem.TextSize = new System.Drawing.Size(75, 16);
			// 
			// emptySpaceItem1
			// 
			this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
			this.emptySpaceItem1.Location = new System.Drawing.Point(0, 75);
			this.emptySpaceItem1.Name = "emptySpaceItem1";
			this.emptySpaceItem1.Size = new System.Drawing.Size(588, 27);
			this.emptySpaceItem1.Text = "emptySpaceItem1";
			this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
			// 
			// emptySpaceItem3
			// 
			this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
			this.emptySpaceItem3.Location = new System.Drawing.Point(582, 374);
			this.emptySpaceItem3.MinSize = new System.Drawing.Size(104, 24);
			this.emptySpaceItem3.Name = "emptySpaceItem3";
			this.emptySpaceItem3.Size = new System.Drawing.Size(122, 52);
			this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.emptySpaceItem3.Text = "emptySpaceItem3";
			this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
			// 
			// emptySpaceItem5
			// 
			this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
			this.emptySpaceItem5.Location = new System.Drawing.Point(0, 474);
			this.emptySpaceItem5.Name = "emptySpaceItem5";
			this.emptySpaceItem5.Size = new System.Drawing.Size(704, 16);
			this.emptySpaceItem5.Text = "emptySpaceItem5";
			this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
			// 
			// layoutRoomsCalendarItem
			// 
			this.layoutRoomsCalendarItem.Control = this.calendarButton;
			this.layoutRoomsCalendarItem.CustomizationFormText = "layoutRoomsCalendarItem";
			this.layoutRoomsCalendarItem.Location = new System.Drawing.Point(582, 426);
			this.layoutRoomsCalendarItem.MinSize = new System.Drawing.Size(95, 46);
			this.layoutRoomsCalendarItem.Name = "layoutRoomsCalendarItem";
			this.layoutRoomsCalendarItem.Size = new System.Drawing.Size(122, 48);
			this.layoutRoomsCalendarItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutRoomsCalendarItem.Text = "layoutRoomsCalendarItem";
			this.layoutRoomsCalendarItem.TextSize = new System.Drawing.Size(0, 0);
			this.layoutRoomsCalendarItem.TextToControlDistance = 0;
			this.layoutRoomsCalendarItem.TextVisible = false;
			// 
			// layoutControlItemShowOnlyPublic
			// 
			this.layoutControlItemShowOnlyPublic.Control = this.checkEditShowOnlyPublic;
			this.layoutControlItemShowOnlyPublic.CustomizationFormText = "layoutControlItemShowOnlyPublic";
			this.layoutControlItemShowOnlyPublic.Location = new System.Drawing.Point(519, 26);
			this.layoutControlItemShowOnlyPublic.MinSize = new System.Drawing.Size(125, 25);
			this.layoutControlItemShowOnlyPublic.Name = "layoutControlItemShowOnlyPublic";
			this.layoutControlItemShowOnlyPublic.Size = new System.Drawing.Size(185, 25);
			this.layoutControlItemShowOnlyPublic.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItemShowOnlyPublic.Text = "layoutControlItemShowOnlyPublic";
			this.layoutControlItemShowOnlyPublic.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItemShowOnlyPublic.TextToControlDistance = 0;
			this.layoutControlItemShowOnlyPublic.TextVisible = false;
			// 
			// roomDetailsWorker
			// 
			this.roomDetailsWorker.WorkerSupportsCancellation = true;
			this.roomDetailsWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.roomDetailsWorker_DoWork);
			this.roomDetailsWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.roomDetailsWorker_RunWorkerCompleted);
			// 
			// RoomSelectionDialog
			// 
			this.ApplyEnabled = true;
			this.ApplyVisible = true;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
			this.ClientSize = new System.Drawing.Size(728, 546);
			this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
			this.MinimumSize = new System.Drawing.Size(640, 480);
			this.Name = "RoomSelectionDialog";
			this.Text = "[Room Selection]";
			this.Load += new System.EventHandler(this.RoomSelectionDialog_Load);
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RoomSelectionDialog_FormClosing);
			((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).EndInit();
			this.ContentPanel.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
			this.layoutControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.selectedRoomsListBox)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.showOnlyFavoritesCheckEdit.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.roomsTreeList)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.advancedInforepositoryItem)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.repositoryApproval)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imageCollection)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.searchByComboBox.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.checkEditShowOnlyPublic.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.searchByLayoutControlItem)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataLoadingLayoutItem)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.searchValueLayoutControlItem)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutRoomsCalendarItem)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItemShowOnlyPublic)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private DevExpress.XtraLayout.LayoutControl layoutControl1;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private DevExpress.XtraEditors.ComboBoxEdit searchByComboBox;
		private DevExpress.XtraLayout.LayoutControlItem searchByLayoutControlItem;
		private CustomDevExpressTreeList roomsTreeList;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
		private DevExpress.XtraTreeList.Columns.TreeListColumn roomNameColumn;
		private DevExpress.XtraTreeList.Columns.TreeListColumn addressColumn;
		private DevExpress.XtraTreeList.Columns.TreeListColumn floorColumn;
		private DevExpress.XtraTreeList.Columns.TreeListColumn cityColumn;
		private DevExpress.XtraTreeList.Columns.TreeListColumn stateColumn;
		private DevExpress.XtraTreeList.Columns.TreeListColumn zipCodeColumn;
		private DevExpress.XtraTreeList.Columns.TreeListColumn phoneNumberColumn;
		private DevExpress.XtraEditors.CheckEdit showOnlyFavoritesCheckEdit;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
		private System.ComponentModel.BackgroundWorker roomDetailsWorker;
		private DevExpress.Utils.ImageCollection imageCollection;
		private DevExpress.XtraEditors.ListBoxControl selectedRoomsListBox;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
		private DevExpress.XtraEditors.SimpleButton selectRoomButton;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
		private DevExpress.XtraEditors.SimpleButton removeRoomButton;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
		private DevExpress.XtraTreeList.Columns.TreeListColumn advancedInfoColumn;
		private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit advancedInforepositoryItem;
		private DevExpress.XtraTreeList.Columns.TreeListColumn mediaTypeColumn;
		private DevExpress.XtraTreeList.Columns.TreeListColumn assistantColumn;
		private DevExpress.XtraEditors.LabelControl dataLoadingLabelControl;
		private DevExpress.XtraLayout.LayoutControlItem dataLoadingLayoutItem;
		private DevExpress.XtraEditors.DropDownButton changeViewButton;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
		private DevExpress.XtraLayout.LayoutControlItem searchValueLayoutControlItem;
		private DevExpress.XtraTreeList.Columns.TreeListColumn middleTierColumn;
		private DevExpress.XtraTreeList.Columns.TreeListColumn capacityColumn;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
		private DevExpress.XtraTreeList.Columns.TreeListColumn approval2Column;
		private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryApproval;
		private DevExpress.XtraEditors.SimpleButton calendarButton;
		private DevExpress.XtraLayout.LayoutControlItem layoutRoomsCalendarItem;
		private DevExpress.XtraEditors.CheckEdit checkEditShowOnlyPublic;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemShowOnlyPublic;
	}
}
