﻿using System;
using System.ComponentModel;
using System.Net;
using System.Windows.Forms;
using MyVrm.WebServices.Data;

namespace MyVrmExcelImportTemplate
{
    public partial class ConnectionDialog : Form
    {
        public ConnectionDialog()
        {
            AuthenticationMode = MyVrm.WebServices.Data.AuthenticationMode.Custom;
            InitializeComponent();
            myVrmUserRadioButton.Checked = true;
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false),
         EditorBrowsable(EditorBrowsableState.Never)]
        public string Url { get; set; }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false),
         EditorBrowsable(EditorBrowsableState.Never)]
        public string User { get; set; }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false),
         EditorBrowsable(EditorBrowsableState.Never)]
        public string Password { get; set; }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false),
         EditorBrowsable(EditorBrowsableState.Never)]
        public string Domain { get; set; }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false),
         EditorBrowsable(EditorBrowsableState.Never)]
        public AuthenticationMode AuthenticationMode
        {
            get; set;
        }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false),
         EditorBrowsable(EditorBrowsableState.Never)]
        public bool UseDefaultCredentials { get; set; }

        private void myVrmUserRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            myVrmUserLoginText.Enabled = myVrmUserPwdText.Enabled = myVrmUserRadioButton.Checked;
            if (myVrmUserRadioButton.Checked)
            {
                AuthenticationMode = AuthenticationMode.Custom;
                UseDefaultCredentials = false;
            }
        }

        private void diffWindowsUserRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            winUserDomainText.Enabled =
                winUserNameText.Enabled = winUserPwdText.Enabled = diffWindowsUserRadioButton.Checked;
            if (diffWindowsUserRadioButton.Checked)
            {
                AuthenticationMode = AuthenticationMode.Windows;
                UseDefaultCredentials = false;
            }
        }

        private void curWindowsUserRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if(curWindowsUserRadioButton.Checked)
            {
                AuthenticationMode = AuthenticationMode.Windows;
                UseDefaultCredentials = true;
            }
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            if (DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                Url = urlText.Text;
                switch (AuthenticationMode)
                {
                    case AuthenticationMode.Windows:
                    {
                        if (!UseDefaultCredentials)
                        {
                            User = winUserNameText.Text;
                            Domain = winUserDomainText.Text;
                            Password = winUserPwdText.Text;
                        }
                        break;
                    }
                    case AuthenticationMode.Custom:
                    {
                        User = myVrmUserLoginText.Text;
                        Password = myVrmUserPwdText.Text;
                        break;
                    }
                    case AuthenticationMode.Unknown:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                try
                {
                    MyVrmService.TestConnection(Url, AuthenticationMode,
                                                AuthenticationMode == AuthenticationMode.Windows
                                                    ? UseDefaultCredentials
                                                          ? new NetworkCredential(User, Password,
                                                                                  Domain)
                                                          : null
                                                    : new NetworkCredential(User, Password));
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, "Logon failed", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    e.Cancel = true;
                }
            }
            base.OnClosing(e);
        }

        private void ConnectionDialog_Load(object sender, EventArgs e)
        {
            urlText.Text = Url;
            switch(AuthenticationMode)
            {
                case AuthenticationMode.Windows:
                {
                    if (UseDefaultCredentials)
                    {
                        curWindowsUserRadioButton.Checked = true;
                    }
                    else
                    {
                        diffWindowsUserRadioButton.Checked = true;
                        winUserNameText.Text = User;
                        winUserDomainText.Text = Domain;
                        winUserPwdText.Text = Password;
                    }
                    break;
                }
                case AuthenticationMode.Custom:
                {
                    myVrmUserRadioButton.Checked = true;
                    myVrmUserLoginText.Text = User;
                    myVrmUserPwdText.Text = Password;
                    break;
                }
                case AuthenticationMode.Unknown:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
