﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System.Collections.Generic;

namespace MyVrm.WebServices.Data
{
    public class LoggedOnUserGlobalInfo : ComplexProperty
    {
        public EmailSystemSettings EmailSystemSettings { get; set; }

        public UserId UserId { get; private set; }
        public UserName UserName { get; set; }
        public string UserEmail { get; set; }
        public OrganizationId OrganizationId { get; private set; }

		public override object Clone()
		{
			LoggedOnUserGlobalInfo copy = new LoggedOnUserGlobalInfo();
			copy.UserId = new UserId(UserId.Id);
			copy.UserEmail = string.Copy(UserEmail);
			copy.UserName = (UserName)UserName.Clone();
			copy.OrganizationId = (OrganizationId)OrganizationId.Clone();
			copy.EmailSystemSettings = (EmailSystemSettings)EmailSystemSettings.Clone();

			return copy;
		}

        internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
        {
            switch(reader.LocalName)
            {
                case "userID":
                {
                    UserId = new UserId(reader.ReadValue());
                    return true;
                }
                case "userName":
                {
                    UserName = new UserName();
                    UserName.LoadFromXml(reader, reader.LocalName);
                    return true;
                }
                case "userEmail":
                {
                    UserEmail = reader.ReadValue();
                    return true;
                }
                case "emailSystem":
                {
                    EmailSystemSettings = new EmailSystemSettings();
                    EmailSystemSettings.LoadFromXml(reader, reader.LocalName);
                    return true;
                }
                case "organizationID":
                {
                    OrganizationId = new OrganizationId(reader.ReadValue());
                    return true;
                }
            }
            return false;
        }
    }
}
