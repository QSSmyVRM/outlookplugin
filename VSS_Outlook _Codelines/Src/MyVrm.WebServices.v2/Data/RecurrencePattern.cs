﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Xml;

namespace MyVrm.WebServices.Data
{
    public enum RecurrenceType
    {
        Daily = 0,
        Weekly = 1,
        Monthly = 2,
        MonthNth = 3,
        Yearly = 5,
        YearNth = 6,
        Custom = 7
    }

    [Flags]
    public enum DaysOfWeek
    {
        None = 0,
        Sunday = 1,
        Monday = 2,
        Tuesday = 4,
        Wednesday = 8,
        Thursday = 0x10,
        Friday = 0x20,
        Saturday = 0x40,
        AllDays = Sunday | Monday | Tuesday | Wednesday | Thursday | Friday | Saturday,
        Weekdays = Monday | Tuesday | Wednesday | Thursday | Friday,
        WeekendDays = Saturday | Sunday
    }
    /// <summary>
    /// Represents a recurrence pattern
    /// </summary>
    public sealed class RecurrencePattern : ComplexProperty
    {
        private readonly TimeZoneInfo _timeZone;
        private static readonly Calendar GregorianCalendar;
        private List<DateTime> _customDates;
        private int? _occurrences;
        private DateTime _startDate;
        private DateTime? _endDate;
        private int _dayOfMonth;
        private int _interval;
        private int _monthOfYear;

		public override object Clone()//CopyTo(RecurrencePattern pattern)
		{
			//if (pattern == null)
			RecurrencePattern pattern = new RecurrencePattern(_timeZone);

			if (_customDates != null )
			{
				pattern._customDates = new List<DateTime>(_customDates);
			}

			pattern._dayOfMonth = _dayOfMonth;
			if (_endDate != null)
				pattern._endDate = new DateTime(_endDate.Value.Ticks);
			pattern._interval = _interval;
			pattern._monthOfYear = _monthOfYear;
			pattern._occurrences = _occurrences;
			pattern._startDate = new DateTime(_startDate.Ticks) ;
			pattern.DaysOfWeek = DaysOfWeek;
			pattern.Instance = Instance;
			if (!(RecurrenceType == RecurrenceType.Custom && Interval == 0))
				pattern.Interval = Interval;
			pattern.RecurrenceType = this.RecurrenceType;
			//pattern.StartDate = new DateTime(StartDate.Ticks);
			pattern.StartTime = new TimeSpan(StartTime.Ticks);
			pattern.EndTime = new TimeSpan(EndTime.Ticks);
           // pattern.RecurrenceRange = this.RecurrenceRange;

			return pattern;
		}

		//public override bool Equals(object obj)
		public bool Equals(RecurrencePattern pattern)//object obj)
		{
			//RecurrencePattern pattern = obj as RecurrencePattern;
			if (ReferenceEquals(this, pattern)) 
				return true;

			if (pattern == null)
				return false;

			if (pattern.CustomDates != null && CustomDates == null || pattern.CustomDates == null && CustomDates != null )
				return false;
			if (CustomDates != null && pattern.CustomDates != null/*<- extra condition indeed*/)
			{
				if (pattern.CustomDates.Count != CustomDates.Count)
					return false;
				for( int i = 0; i < CustomDates.Count ; i++ )
				{
					if (pattern.CustomDates[i] != CustomDates[i])
						return false;
				}
			}
			if (pattern._dayOfMonth != _dayOfMonth)
				return false;
			if (pattern._endDate != _endDate)
				return false;
			if (pattern._interval != _interval)
				return false;
			if (pattern._monthOfYear != _monthOfYear)
				return false;
			if (pattern._occurrences != _occurrences)
				return false;
			if (pattern._startDate != _startDate )
				return false;
			if (pattern.DaysOfWeek != DaysOfWeek)
				return false;
			if (pattern.Instance != Instance)
				return false;
			if (pattern.Interval != Interval)
				return false;
			if (pattern.RecurrenceType != RecurrenceType)
				return false;
			//if (pattern.StartDate != StartDate)
			//    return false;
			if (pattern.StartTime != StartTime)
				return false;
			if (pattern.EndTime != EndTime)
				return false;
			if (!pattern.TimeZone.Equals(TimeZone))
				return false;
           
			return true;
		}
               

    	static RecurrencePattern()
        {
            GregorianCalendar = new GregorianCalendar();
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="RecurrencePattern"/> class.
        /// </summary>
        /// <param name="timeZone"></param>
        public RecurrencePattern(TimeZoneInfo timeZone)
        {
            EndDate = DateTime.MaxValue;
            StartDate = DateTime.MinValue;
            StartTime = TimeSpan.MinValue;
            EndTime = TimeSpan.MaxValue;
            _timeZone = timeZone;
        }
        /// <summary>
        /// Gets or sets recurrence pattern type.
        /// </summary>
        public RecurrenceType RecurrenceType { get; set; }
       // public RecurrenceRange RecurrenceRange { get; set; }
        /// <summary>
        /// Gets or sets the day of the month when each occurrence happens. DayOfMonth must be between 1 and 31.
        /// </summary>
        /// <remarks>This property is only valid for recurrences of the <see cref="RecurrenceType.Monthly"/></remarks>
        public int DayOfMonth
        {
            get { return _dayOfMonth; }
            set
            {
                if (value < 1 || value > 31)
                {
                    throw new ArgumentOutOfRangeException("value", Strings.DayOfMonthMustBeBetween1And31);
                }
                _dayOfMonth = value;
            }
        }
        /// <summary>
        /// Gets the list of the days of the week when occurrences happen.
        /// </summary>
        /// <remarks>This property is only valid for recurrences of the <see cref="RecurrenceType.Weekly"/>, <see cref="RecurrenceType.MonthNth"/> and <see cref="RecurrenceType.YearNth"/></remarks>
        public DaysOfWeek DaysOfWeek { get; set; }
        /// <summary>
        /// Gets or sets the interval between occurrences.
        /// </summary>
        public int Interval
        {
            get { return _interval; }
            set
            {
                if ((RecurrenceType == RecurrenceType.Yearly || RecurrenceType == RecurrenceType.YearNth) && value > 1)
                {
                    throw new ArgumentOutOfRangeException("value", Strings.IntervalCannotBeGreaterThanOne);
                }
                if (value < 1)
                {
                    throw new ArgumentOutOfRangeException("value", Strings.IntervalMustBeGreaterOrEqualToOne);
                }
                _interval = value;
            }
        }
        /// <summary>
        /// Returns or sets value specifying the count for which the recurrence pattern is valid for a given interval. 
        /// </summary>
        /// <remarks>This property is only valid for recurrences of the <see cref="RecurrenceType.MonthNth"/> and <see cref="RecurrenceType.YearNth"/></remarks>
        public int Instance { get; set; }
        /// <summary>
        /// Gets or sets the month of the year when each occurrence happens. MonthOfYear must be betwen 1 and 12.
        /// </summary>
        /// <remarks>This property is only valid for recurrences of the <see cref="RecurrenceType.Yearly"/> and <see cref="RecurrenceType.YearNth"/></remarks>
        public int MonthOfYear
        {
            get { return _monthOfYear; }
            set
            {
                if (value < 1 || value > 12)
                {
                    throw new ArgumentOutOfRangeException("value", Strings.MonthOfYearMustBeBetween1And12);
                }
                _monthOfYear = value;
            }
        }
        /// <summary>
        /// Gets a value indicating whether the pattern has a fixed number of occurrences or an end date.
        /// </summary>
        public bool HasEnd
        {
            get { return _occurrences.HasValue || _endDate.HasValue; }
        }
        /// <summary>
        /// Gets or sets the number of occurrences after which the recurrence ends. Setting Occurrences resets EndDate.
        /// </summary>
        public int? Occurrences
        {
            get
            {
                return _occurrences;
            }
            set
            {
                if (value < 1)
                {
                    throw new ArgumentException(Strings.NumberOfOccurrencesMustBeGreaterThanZero);
                }
                SetFieldValue(ref _occurrences, value);
            }
        }
        /// <summary>
        /// Gets or sets the date and time when the recurrence start.
        /// </summary>
        public DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }
        /// <summary>
        /// Gets or sets the date after which the recurrence ends. Setting EndDate resets Occurrences.
        /// </summary>
        public DateTime? EndDate
        {
            get
            {
                return _endDate;
            }
            set
            {
                SetFieldValue(ref _endDate, value);
                _occurrences = null;
            }

        }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }

        public TimeZoneInfo TimeZone
        {
            get
            {
                return _timeZone;
            }
        }

        public List<DateTime> CustomDates
        {
            get { return _customDates ?? (_customDates = new List<DateTime>()); }
        }

    	//public List<DateTime> InitialOriginalDates = new List<DateTime>();
		
        public void NeverEnds()
        {
            _occurrences = null;
            _endDate = null;
            Changed();
        }

        internal override void LoadFromXml(MyVrmServiceXmlReader reader, XmlNamespace xmlNamespace, string xmlElementName)
        {
            reader.EnsureCurrentNodeIsStartElement(xmlNamespace, xmlElementName);
            using (var subReader = reader.ReadSubtree())
            {
                var doc = new XmlDocument();
                doc.Load(subReader);
                var patternNode = doc.DocumentElement;
                var recurTypeNode = patternNode.SelectSingleNode("child::recurType");

                switch (recurTypeNode.InnerText)
                {
                    case "1": //daily
                    {
                        var dailyType = patternNode.SelectSingleNode("child::dailyType");
                        switch (dailyType.InnerText)
                        {
                            case "1": // every day
                            {
                                RecurrenceType = RecurrenceType.Daily;
                                DaysOfWeek = DaysOfWeek.AllDays;
                                Interval = Utilities.Parse<int>(patternNode.SelectSingleNode("child::dayGap").InnerText);
                                break;
                            }
                            case "2": // every weekday
                            {
                                RecurrenceType = RecurrenceType.Daily;
                                DaysOfWeek = DaysOfWeek.Weekdays;
								Interval = 1; 
                                break;
                            }
                            default:
                            {
                                throw new NotSupportedException();
                            }
                        }
                        break;
                    }
                    case "2": // weekly
                    {
                        RecurrenceType = RecurrenceType.Weekly;
                        DaysOfWeek =
                            Utilities.StringToDaysOfWeek(patternNode.SelectSingleNode("child::weekDay").InnerText);
                        Interval = Utilities.Parse<int>(patternNode.SelectSingleNode("child::weekGap").InnerText);
                        break;
                    }
                    case "3": //monthly
                    {
                        var monthlyType = patternNode.SelectSingleNode("child::monthlyType");
                        switch (monthlyType.InnerText)
                        {
                            case "1":
                            {
                                RecurrenceType = RecurrenceType.Monthly;
                                DayOfMonth =
                                    Utilities.Parse<int>(patternNode.SelectSingleNode("child::monthDayNo").InnerText);
                                Interval = Utilities.Parse<int>(patternNode.SelectSingleNode("child::monthGap").InnerText);
                                break;
                            }
                            case "2":
                            {
                                RecurrenceType = RecurrenceType.MonthNth;
                                Instance =
                                    Utilities.Parse<int>(patternNode.SelectSingleNode("child::monthWeekDayNo").InnerText);
                                DaysOfWeek =
                                    Utilities.StringToWeekDay(
                                        patternNode.SelectSingleNode("child::monthWeekDay").InnerText);
                                Interval = Utilities.Parse<int>(patternNode.SelectSingleNode("child::monthGap").InnerText);
                                break;
                            }
                            default:
                            {
                                throw new NotSupportedException();
                            }
                        }
                        break;
                    }
                    case "4": //yearly
                    {
                        var yearlyType = patternNode.SelectSingleNode("child::yearlyType");
                        switch (yearlyType.InnerText)
                        {
                            case "1":
                            {
                                RecurrenceType = RecurrenceType.Yearly;
                                MonthOfYear =
                                    Utilities.Parse<int>(patternNode.SelectSingleNode("child::yearMonth").InnerText);
                                DayOfMonth =
                                    Utilities.Parse<int>(patternNode.SelectSingleNode("child::yearMonthDay").InnerText);
                                Interval = 1; // myVRM supports every year only 
                                break;
                            }
                            case "2":
                            {
                                RecurrenceType = RecurrenceType.YearNth;
                                MonthOfYear =
                                    Utilities.Parse<int>(patternNode.SelectSingleNode("child::yearMonth").InnerText);
                                Instance =
                                    Utilities.Parse<int>(
                                        patternNode.SelectSingleNode("child::yearMonthWeekDayNo").InnerText);
                                DaysOfWeek =
                                    Utilities.StringToWeekDay(
                                        patternNode.SelectSingleNode("child::yearMonthWeekDay").InnerText);
                                Interval = 1; // myVRM supports every year only
                                break;
                            }
                        }
                        break;
                    }
                    case "5": // custom
                    {
                        RecurrenceType = RecurrenceType.Custom;
                        var dateNodes = patternNode.SelectNodes("child::startDates/startDate");
                        foreach (XmlNode dateNode in dateNodes)
                        {
                            CustomDates.Add(Utilities.StringToDate(dateNode.InnerText));
                        }
                        break;
                    }
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            switch (RecurrenceType)
            {
                case RecurrenceType.Daily:
                    {
                        writer.WriteElementValue(Namespace, "recurType", "1");
                        writer.WriteElementValue(Namespace, "dailyType", "1");
                        writer.WriteElementValue(Namespace, "dayGap", Interval == 0 ? 1 : Interval);
                        break;
                    }
                case RecurrenceType.Weekly:
                    {
                        if (DaysOfWeek == DaysOfWeek.Weekdays && Interval == 0)
                        {
                            writer.WriteElementValue(Namespace, "recurType", "1");
                            writer.WriteElementValue(Namespace, "dailyType", "2");
                            writer.WriteElementValue(Namespace, "dayGap", Interval);
                        }
                        else
                        {
                            writer.WriteElementValue(Namespace, "recurType", "2");
                            writer.WriteElementValue(Namespace, "weekDay", Utilities.DaysOfWeekToString(DaysOfWeek));
                            writer.WriteElementValue(Namespace, "weekGap", Interval);
                        }
                        break;
                    }
                case RecurrenceType.Monthly:
                    {
                        writer.WriteElementValue(Namespace, "recurType", "3");
                        writer.WriteElementValue(Namespace, "monthlyType", "1");
                        writer.WriteElementValue(Namespace, "monthDayNo", DayOfMonth);
                        writer.WriteElementValue(Namespace, "monthGap", Interval);
                        break;
                    }
                case RecurrenceType.MonthNth:
                    {
                        writer.WriteElementValue(Namespace, "recurType", "3");
                        writer.WriteElementValue(Namespace, "monthlyType", "2");
                        writer.WriteElementValue(Namespace, "monthWeekDayNo", Instance);
                        writer.WriteElementValue(Namespace, "monthWeekDay", Utilities.WeekDayToString(DaysOfWeek));
                        writer.WriteElementValue(Namespace, "monthGap", Interval);
                        break;
                    }
                case RecurrenceType.Yearly:
                    {
                        writer.WriteElementValue(Namespace, "recurType", "4");
                        writer.WriteElementValue(Namespace, "yearlyType", "1");
                        writer.WriteElementValue(Namespace, "yearMonth", MonthOfYear);
                        writer.WriteElementValue(Namespace, "yearMonthDay", DayOfMonth);
                        break;
                    }
                case RecurrenceType.YearNth:
                    {
                        writer.WriteElementValue(Namespace, "recurType", "4");
                        writer.WriteElementValue(Namespace, "yearlyType", "2");
                        writer.WriteElementValue(Namespace, "yearMonthWeekDayNo", Instance);
                        writer.WriteElementValue(Namespace, "yearMonthWeekDay", Utilities.WeekDayToString(DaysOfWeek));
                        writer.WriteElementValue(Namespace, "yearMonth", MonthOfYear);
                        break;
                    }
                case RecurrenceType.Custom:
                {
                    writer.WriteElementValue(Namespace, "recurType", "5");
                    writer.WriteStartElement(Namespace, "startDates");
                    foreach (var customDate in CustomDates)
                    {
                        writer.WriteElementValue(Namespace, "startDate", Utilities.DateToString(customDate));
                    }
                    writer.WriteEndElement();
                    break;
                }
                default:
                    throw new ArgumentOutOfRangeException();
            }
            RecurrenceRange range;
            if (!HasEnd)
            {
                range = new NoEndDateRecurrenceRange(StartDate);
            }
            else if (Occurrences.HasValue)
            {
                range = new NumberedRecurrenceRange(StartDate, Occurrences);
            }
            else
            {
                range = new EndDateRecurrenceRange(StartDate, EndDate.Value);
            }
            range.WriteToXml(writer, "recurrenceRange");
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj.GetType() == typeof(RecurrencePattern) && Equals((RecurrencePattern)obj);
        }

        public ReadOnlyCollection<OccurrenceInfo> GetOccurrenceInfoList(DateTime startDate, DateTime endDate)
        {
            if (endDate < startDate)
            {
                throw new ArgumentException();
            }
            if (RecurrenceType == RecurrenceType.Custom)
            {
                return
                    new ReadOnlyCollection<OccurrenceInfo>(
                        CustomDates.Where(dt => dt >= startDate && dt <= endDate).Select(
                            dt => GetOccurrenceInfoByDate(dt)).ToList());
            }
            if ((HasEnd && (EndDate < DateTime.MaxValue)) && GetLastOccurrence().End < startDate)
            {
                return new ReadOnlyCollection<OccurrenceInfo>(new List<OccurrenceInfo>());
            }
            var date = TimeZoneInfo.ConvertTime(startDate, TimeZone).Date;
            do
            {
                date = GetPreviousOccurrenceDate(date);
            } while (date != DateTime.MinValue && GetOccurrenceInfoByDate(date).End >= startDate);
            if (date == DateTime.MinValue)
            {
                date = GetNextOccurrenceDate(date);
            }
            var occurrenceInfos = new List<OccurrenceInfo>();
            while(date != DateTime.MaxValue)
            {
                var occurrenceInfo = GetOccurrenceInfoByDate(date);
                if ((occurrenceInfo.Start < endDate && occurrenceInfo.End >= startDate) || occurrenceInfo.Start == startDate)
                {
                    occurrenceInfos.Add(occurrenceInfo);    
                }
                else if (occurrenceInfo.Start >= endDate)
                {
                    return new ReadOnlyCollection<OccurrenceInfo>(occurrenceInfos);
                }
                date = GetNextOccurrenceDate(date);
            }
            return new ReadOnlyCollection<OccurrenceInfo>(occurrenceInfos);
        }


        public OccurrenceInfo GetLastOccurrence()
        {
            if (!EndDate.HasValue)
            {
                throw new InvalidOperationException(Strings.NoLastOccurrenceForNoEndDateRecurrenceRange);
            }
            if (RecurrenceType == RecurrenceType.Custom)
            {
                return GetOccurrenceInfoByDate(CustomDates.Max());
            }
            var previousOccurrenceDate = GetPreviousOccurrenceDate(EndDate.Value.AddDays(1));
            if (previousOccurrenceDate == DateTime.MinValue)
            {
                throw new Exception(Strings.NoOccurrencesFoundInRecurrence);
            }
            return GetOccurrenceInfoByDate(previousOccurrenceDate);
        }

        private OccurrenceInfo GetOccurrenceInfoByDate(DateTime occurrenceDate)
        {
            var originalStartTime = TimeZoneInfo.ConvertTime(occurrenceDate.Date.Add(StartTime), TimeZone);
            return new OccurrenceInfo(originalStartTime, TimeZoneInfo.ConvertTime(occurrenceDate.Date.Add(EndTime), TimeZone));
        }

        private DateTime GetNextOccurrenceDate(DateTime date)
        {
            date = date.Date;
            if (date >= EndDate)
            {
                return DateTime.MaxValue;
            }
            if (date < StartDate)
            {
                date = StartDate;
            }
            else if (date < DateTime.MaxValue)
            {
                date = date.AddDays(1.0);
            }
            var nthOccurrence = GetNthOccurrence(date, 1);
            if (nthOccurrence > EndDate)
            {
                nthOccurrence = DateTime.MaxValue;
            }
            return nthOccurrence;
        }

        private DateTime GetPreviousOccurrenceDate(DateTime date)
        {
            if (date <= StartDate)
            {
                return DateTime.MinValue;
            }
            if (HasEnd && date > EndDate)
            {
                date = EndDate.Value;
            }
            else
            {
                date = date.AddDays(-1);
            }
            var numberOfOccurrencesSinceStart = GetNumberOfOccurrencesSinceStart(date);
            if (numberOfOccurrencesSinceStart < 1)
            {
                return DateTime.MinValue;
            }
            var nthOccurrence = GetNthOccurrence(StartDate, numberOfOccurrencesSinceStart);
            if (nthOccurrence < StartDate)
            {
                nthOccurrence = DateTime.MinValue;
            }
            return nthOccurrence;
        }

        private DateTime GetNthOccurrence(DateTime date, int nthOcc)
        {
            var time4 = DateTime.MaxValue;
            try
            {
                if (RecurrenceType == RecurrenceType.Daily)
                {
                    var span = date.AddDays(-1) - StartDate;
                    var days = span.Days;
                    var interval = Interval;
                    var daysPerInterval = Convert.ToInt32(Math.Floor(days/(double)interval));
                    return StartDate.AddDays(interval*(nthOcc + daysPerInterval));
                }
                if (RecurrenceType == RecurrenceType.Weekly)
                {
                    var span = date - StartDate;
                    var daysFromFirstDayOfWeek = StartDate.DayOfWeek - Constants.FirstDayOfWeek;
                    if (daysFromFirstDayOfWeek < 0)
                    {
                        daysFromFirstDayOfWeek += 7;
                    }
                    if (daysFromFirstDayOfWeek != 0)
                    {
                        span += TimeSpan.FromDays(daysFromFirstDayOfWeek);
                    }
                    // Special case for daily every weekday
                    var interval = (Interval == 0 && DaysOfWeek == DaysOfWeek.Weekdays) ? 1 : Interval;
                    if ((span.Days % (7 * interval)) < 7)
                    {
                        int numOccurrences = OccurrencesBetween(date.DayOfWeek, Constants.FirstDayOfWeek, DaysOfWeek);
                        if (numOccurrences >= nthOcc)
                        {
                            return GetOccurrenceInCurrentWeek(date, Constants.FirstDayOfWeek, DaysOfWeek, nthOcc);
                        }
                        nthOcc -= numOccurrences;
                    }
                    var numOccurrencesPerWeek = OccurrencesPerWeek(DaysOfWeek);
                    var num9 = 1 + (nthOcc/numOccurrencesPerWeek);
                    var num10 = nthOcc%numOccurrencesPerWeek;
                    num9 = (num10 == 0) ? (num9 - 1) : num9;
                    num10 = (num10 == 0) ? numOccurrencesPerWeek : num10;
                    return
                        GetOccurrenceInCurrentWeek(
                            StartDate.AddDays((((7 * interval) * (num9 + (span.Days / (7 * interval)))) - daysFromFirstDayOfWeek)), Constants.FirstDayOfWeek, DaysOfWeek, num10);
                }
                if (RecurrenceType == RecurrenceType.Monthly || RecurrenceType == RecurrenceType.MonthNth)
                {
                    var interval = Interval;
                    var calendar = GregorianCalendar;
                    var numberOfMonths = GetNumberOfMonthsBetween(StartDate, date, calendar);
                    if ((numberOfMonths % interval) == 0 
                        && ((RecurrenceType == RecurrenceType.Monthly && calendar.GetDayOfMonth(date) <= DayOfMonth)
                            || (RecurrenceType == RecurrenceType.MonthNth && AdjustToDayWeekWithOrderInCurrentMonth(date, DaysOfWeek, Instance, calendar) >= date)))
                    {
                        nthOcc--;
                    }
                    var months = interval*(nthOcc + (numberOfMonths/interval));
                    var occurrenceDate = calendar.AddMonths(StartDate, months);
                    return RecurrenceType == RecurrenceType.Monthly ? AdjustToDayOfMonthInCurrentMonth(occurrenceDate, DayOfMonth, calendar) : AdjustToDayWeekWithOrderInCurrentMonth(occurrenceDate, DaysOfWeek, Instance, calendar);
                }
                if (RecurrenceType != RecurrenceType.Yearly && RecurrenceType != RecurrenceType.YearNth)
                {
                    return DateTime.MaxValue;
                }
                var calendar2 = GregorianCalendar;
                var month = MonthOfYear;
                var curMonth = calendar2.GetMonth(date);
                var isLeapMonth = calendar2.IsLeapMonth(calendar2.GetYear(StartDate), calendar2.GetMonth(StartDate));
                var leapMonth = calendar2.GetLeapMonth(calendar2.GetYear(date), calendar2.GetEra(date));
                leapMonth = (leapMonth == 0) ? 0x7fffffff : leapMonth;
                var month2 = month;
                if (leapMonth <= month2 || (isLeapMonth && (leapMonth == (month2 + 1))))
                {
                    month2++;
                }
                var flag = false;
                if (curMonth > month2)
                {
                    flag = true;
                }
                else if (curMonth == month2)
                {
                    if (RecurrenceType == RecurrenceType.Yearly && calendar2.GetDayOfMonth(date) > DayOfMonth)
                    {
                        flag = true;
                    }
                    else if (RecurrenceType == RecurrenceType.YearNth && (AdjustToDayWeekWithOrderInCurrentMonth(date, DaysOfWeek, Interval, calendar2) < date))
                    {
                        flag = true;
                    }
                }
                if (flag)
                {
                    nthOcc++;
                }
                date = calendar2.AddMonths(date, 1 - month);
                var recurrenceInterval = Interval;
                date = calendar2.AddYears(date, (nthOcc - 1)*recurrenceInterval);
                //month = calendar2.GetMonth(date);
                leapMonth = calendar2.GetLeapMonth(calendar2.GetYear(date), calendar2.GetEra(date));
                leapMonth = (leapMonth == 0) ? 0x7fffffff : leapMonth;
                month2 = MonthOfYear;
                if (leapMonth <= month2 || (flag && (leapMonth == (month2 + 1))))
                {
                    month2++;
                }
                if (RecurrenceType == RecurrenceType.YearNth)
                {
                    return
                        AdjustToDayWeekWithOrderInCurrentMonth(
                            calendar2.AddMonths(date, month2 - calendar2.GetMonth(date)), DaysOfWeek, Instance, calendar2);
                }
                time4 = AdjustToDayOfMonthInCurrentMonth(calendar2.AddMonths(date, month2 - calendar2.GetMonth(date)),
                                                         DayOfMonth, calendar2);
            }
            catch (ArgumentOutOfRangeException)
            {
            }
            return time4;
        }

        private static DateTime AdjustToDayOfMonthInCurrentMonth(DateTime date, int dayOfMonth, Calendar calendar)
        {
            var time = calendar.AddDays(date, dayOfMonth - calendar.GetDayOfMonth(date));
            if (calendar.GetMonth(time) != calendar.GetMonth(date))
            {
                time = calendar.AddDays(time, -calendar.GetDayOfMonth(time));
            }
            return time;
        }

        private int GetNumberOfOccurrencesSinceStart(DateTime curDate)
        {
            var span = curDate - StartDate;
            var num = 0;
            if (RecurrenceType == RecurrenceType.Daily)
            {
                var reccurrenceInterval = Interval;
                return num + (1 + span.Days/reccurrenceInterval);
            }
            if (RecurrenceType == RecurrenceType.Weekly)
            {
                var daysFromFirstDayOfWeek = StartDate.DayOfWeek - Constants.FirstDayOfWeek;
                if (daysFromFirstDayOfWeek < 0)
                {
                    daysFromFirstDayOfWeek += 7;
                }
                if (daysFromFirstDayOfWeek != 0)
                {
                    span += TimeSpan.FromDays(daysFromFirstDayOfWeek);
                    num -= OccurrencesBetween(Constants.FirstDayOfWeek, StartDate.DayOfWeek, DaysOfWeek);
                }
                // Special case for daily every weekday
                var interval = (Interval == 0 && DaysOfWeek == DaysOfWeek.Weekdays) ? 1 : Interval;
                if ((span.Days % (7 * interval)) < 7)
                {
                    if (curDate.DayOfWeek != Constants.FirstDayOfWeek)
                    {
                        num += OccurrencesBetween(Constants.FirstDayOfWeek, curDate.DayOfWeek, DaysOfWeek);
                    }
                    if (((1 << (int)curDate.DayOfWeek) & (int)DaysOfWeek) != 0)
                    {
                        num++;
                    }
                }
                else
                {
                    num++;
                }
                return num + (OccurrencesPerWeek(DaysOfWeek) * (span.Days / (7 * interval)));
            }
            if (RecurrenceType == RecurrenceType.Monthly || RecurrenceType == RecurrenceType.MonthNth)
            {
                var interval = Interval;
                var calendar = GregorianCalendar;
                var numberOfMonths = GetNumberOfMonthsBetween(StartDate, curDate, calendar);
                if ((numberOfMonths % interval) == 0)
                {
                    if ((RecurrenceType == RecurrenceType.Monthly 
                        && (calendar.GetDayOfMonth(curDate) >= Math.Min(DayOfMonth, 
                            calendar.GetDaysInMonth(calendar.GetYear(curDate), 
                                calendar.GetMonth(curDate), calendar.GetEra(curDate)))))
                        || (RecurrenceType == RecurrenceType.MonthNth && AdjustToDayWeekWithOrderInCurrentMonth(curDate, DaysOfWeek, Instance, calendar) <= curDate))
                    {
                        num++;
                    }
                }
                else
                {
                    num++;
                }
                return num + (numberOfMonths/interval);
            }
            if (RecurrenceType != RecurrenceType.Yearly && RecurrenceType != RecurrenceType.YearNth)
            {
                return num;
            }
            var calendar2 = GregorianCalendar;
            var month = MonthOfYear;
            var curMonth = calendar2.GetMonth(curDate);
            var isLeapMonth = calendar2.IsLeapMonth(calendar2.GetYear(StartDate), calendar2.GetMonth(StartDate));
            var leapMonth = calendar2.GetLeapMonth(calendar2.GetYear(curDate), calendar2.GetEra(curDate));
            leapMonth = (leapMonth == 0) ? 0x7fffffff : leapMonth;
            var month2 = month;
            if (leapMonth <= month2 || (isLeapMonth && (leapMonth == (month2 + 1))))
            {
                month2++;
            }
            var flag = false;
            if (curMonth > month2)
            {
                flag = true;
            }
            else if (curMonth == month2)
            {
                if (RecurrenceType == RecurrenceType.Yearly)
                {
                    var daysInMonth = calendar2.GetDaysInMonth(calendar2.GetYear(curDate), curMonth);
                    if (DayOfMonth < daysInMonth)
                    {
                        daysInMonth = DayOfMonth;
                    }
                    if (calendar2.GetDayOfMonth(curDate) >= daysInMonth)
                    {
                        flag = true;
                    }
                }
                else if(RecurrenceType == RecurrenceType.YearNth && (AdjustToDayWeekWithOrderInCurrentMonth(curDate, DaysOfWeek, Instance, calendar2) <= curDate))
                {
                    flag = true;
                }
            }
            if (flag)
            {
                num++;
            }
            return num + (GetNumberOfYearsBetween(StartDate, curDate, calendar2)/Interval);
        }

        private static int GetNumberOfYearsBetween(DateTime firstDate, DateTime secondDate, Calendar calendar)
        {
            if (firstDate > secondDate)
            {
                throw new ArgumentException(Strings.FirstDateShouldBeLessThanSecondDate, "firstDate");
            }
            var firstDateYear = calendar.GetYear(firstDate);
            var firstDateEra = calendar.GetEra(firstDate);
            var secondDateYear = calendar.GetYear(secondDate);
            var secondDateEra = calendar.GetEra(secondDate);
            var years = secondDate.Year - firstDate.Year;
            var prevYear = years - 1;
            var num7 = (years*13)/12;
            if (firstDateEra == secondDateEra)
            {
                return (secondDateYear - firstDateYear);
            }
            while(true)
            {
                try
                {
                    var time = calendar.AddYears(firstDate, years);
                    var year = calendar.GetYear(time);
                    var era = calendar.GetEra(time);
                    if (era < secondDateEra)
                    {
                        prevYear = years;
                    }
                    else if (era > secondDateEra)
                    {
                        num7 = years;
                    }
                    else
                    {
                        return (years + (secondDateYear - year));
                    }
                }
                catch (ArgumentOutOfRangeException)
                {
                    num7 = years - 1;
                }
                years = (prevYear + num7 + 1)/2;
            }
        }

        private static DateTime AdjustToDayWeekWithOrderInCurrentMonth(DateTime date, DaysOfWeek daysOfWeek, int order, Calendar calendar)
        {
            date = calendar.AddDays(date, 1 - calendar.GetDayOfMonth(date));
            if (order != -1 )
            {
                var occurrencesPerWeek = OccurrencesPerWeek(daysOfWeek);
                var num = (order - 1)/occurrencesPerWeek;
                date = calendar.AddDays(date, 7*num);
                order -= num*occurrencesPerWeek;
                return GetOccurrenceInCurrentWeek(date, date.DayOfWeek, daysOfWeek, order);
            }
            date = calendar.AddMonths(calendar.MaxSupportedDateTime.Date, -1) > date
                       ? calendar.AddDays(calendar.AddMonths(date, 1), -1)
                       : calendar.MaxSupportedDateTime.Date;
            return GetOccurrenceInCurrentWeek(date, date.DayOfWeek, daysOfWeek, 1, -1);
        }

        private static DateTime GetOccurrenceInCurrentWeek(DateTime curDate, DayOfWeek endDayOfWeek, DaysOfWeek mask, int nthOcc, int step)
        {
            do
            {
                if ((((1 << (int)curDate.DayOfWeek) & (int)mask) != 0) && (nthOcc-- == 1))
                {
                    return curDate;
                }
                curDate = curDate.AddDays(step);
            }
            while (curDate.DayOfWeek != endDayOfWeek);
            return DateTime.MaxValue;
        }

        private static DateTime GetOccurrenceInCurrentWeek(DateTime curDate, DayOfWeek firstDayOfWeek, DaysOfWeek mask, int nthOcc)
        {
            return GetOccurrenceInCurrentWeek(curDate, firstDayOfWeek, mask, nthOcc, 1);
        }

        private static int GetNumberOfMonthsBetween(DateTime firstDate, DateTime secondDate, Calendar calendar)
        {
            if (firstDate > secondDate)
            {
                throw new ArgumentException("", "firstDate");
            }
            var firstDateYear = calendar.GetYear(firstDate);
            var firstDateEra = calendar.GetEra(firstDate);
            var firstDateMonth = calendar.GetMonth(firstDate);
            var secondDateYear = calendar.GetYear(secondDate);
            var secondDateEra = calendar.GetEra(secondDate);
            var secondDateMonth = calendar.GetMonth(secondDate);
            var numberOfYears = GetNumberOfYearsBetween(firstDate, secondDate, calendar);
            var num8 = (numberOfYears * 12 + secondDateMonth) - firstDateMonth;
            var num9 = (numberOfYears * 13 + secondDateMonth) - firstDateMonth;
            var months = num8;
            if ((firstDateEra == secondDateEra) && (firstDateYear == secondDateYear))
            {
                return secondDateMonth - firstDateMonth;
            }
            while (true)
            {
                try
                {
                    var time = calendar.AddMonths(firstDate, months);
                    var era = calendar.GetEra(time);
                    var year = calendar.GetYear(time);
                    var month = calendar.GetMonth(time);
                    if ((era < secondDateEra) || (era == secondDateEra && secondDateYear < year))
                    {
                        num8 = months;
                    }
                    else if ((era > secondDateEra) || (era == secondDateEra && secondDateYear > year))
                    {
                        num9 = months;
                    }
                    else
                    {
                        return months + (secondDateMonth - month);
                    }
                }
                catch (ArgumentOutOfRangeException)
                {
                    num9 = months - 1;
                }
                months = (num8 + num9 + 1)/2;
            }
        }

        private static int OccurrencesPerWeek(DaysOfWeek daysOfWeek)
        {
            return OccurrencesBetween(DayOfWeek.Sunday, DayOfWeek.Sunday, daysOfWeek);
        }

        private static int OccurrencesBetween(DayOfWeek curDay, DayOfWeek firstDay, DaysOfWeek mask)
        {
            var num = 0;
            do
            {
                if (((1 << (int)curDay) & (int)mask) != 0)
                {
                    num++;
                }
                curDay += 1;
                curDay = (DayOfWeek)(((int)curDay)%((int)(DayOfWeek.Saturday | DayOfWeek.Monday)));
            } while (firstDay != curDay);
            return num;
        }

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
    }
}
