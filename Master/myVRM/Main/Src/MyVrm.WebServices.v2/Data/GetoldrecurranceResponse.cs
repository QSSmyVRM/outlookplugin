﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;


namespace MyVrm.WebServices.Data
{
    internal class GetoldRecurranceRequest : ServiceRequestBase<GetoldRecurranceResponse>
    {
        public GetoldRecurranceRequest(MyVrmService service)
            : base(service)
        {
        }

       
      internal ConferenceId ConferenceId { get; set; }

        #region Overrides of ServiceRequestBase

        internal override string GetXmlElementName()
        {
            return "login";
        }

        internal override string GetCommandName()
        {
            return "GetOldConference";
        }

        internal override string GetResponseXmlElementName()
        {
            return "conference";
        }

        internal override GetoldRecurranceResponse ParseResponse(MyVrmServiceXmlReader reader)
        {

            var response = new GetoldRecurranceResponse();
            response.LoadFromXml(reader, GetResponseXmlElementName());
            return response;
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            UserId.WriteToXml(writer, "userID");
            ConferenceId.WriteToXml(writer, "selectID");
			writer.WriteElementValue(XmlNamespace.NotSpecified, "utcEnabled", Service.IsUtcEnabled);
        }

        #endregion
    }

    public class GetoldRecurranceResponse : ServiceResponse
    {
        internal Conference Conference { get; set; }
        public string endtype { get; set; }
        public string startDate { get; set; }
        public string occurrence { get; set; }

        //internal void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
        //{
        //    try
        //    {
        //        do
        //        {
        //            reader.Read();
        //            if (reader.LocalName != "" && reader.LocalName != null)
        //            {
        //                switch (reader.LocalName)
        //                {
        //                    case "confInfo":
        //                        LoadFromXml(reader, "confInfo");
        //                        //reader.Read();
        //                        break;

        //                    case "recurrenceRange":
        //                        LoadFromXml(reader, "recurrenceRange");
        //                        //reader.Read();
        //                        break;
        //                    case "startDate":
        //                        startDate = reader.ReadElementValue();
        //                        //reader.Read();
        //                        break;
        //                    case "endType":
        //                        endtype = reader.ReadElementValue();
        //                        break;
        //                    case "occurrence":
        //                        occurrence = reader.ReadElementValue();
        //                        break;
        //                    default:
        //                        reader.SkipCurrentElement();
        //                        break;
        //                }
        //            }

        //        } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "recurrenceRange"));
        //    }
        //    catch (Exception ex)
        //    {
        //        MyVrmService.TraceSource.TraceError(reader.LocalName + ": " + ex.Message, null);
        //        //reader.Read();
        //        //throw;
        //    }


        //}

        internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
        {
            do
            {
                reader.Read();
                if (reader.IsStartElement(XmlNamespace.NotSpecified, "confInfo"))
                {
                    Conference.LoadFromXml(reader, true);
                    if (reader.IsStartElement(XmlNamespace.NotSpecified, "recurrenceRange"))
                    {
                        do
                        {
                            reader.Read();
                            if (reader.LocalName != "" && reader.LocalName != null)
                            {
                                switch (reader.LocalName)
                                {
                                    case "recurrenceRange":
                                        LoadFromXml(reader, "recurrenceRange");
                                        break;
                                    case "startDate":
                                        startDate = reader.ReadElementValue();
                                        //reader.Read();
                                        break;
                                    case "endType":
                                        endtype = reader.ReadElementValue();
                                        break;
                                    case "occurrence":
                                        occurrence = reader.ReadElementValue();
                                        break;
                                    default:
                                        reader.SkipCurrentElement();
                                        break;
                                }
                            }

                        } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "recurrenceRange"));

                    }
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "conference"));
        }

    }

}