﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.Outlook.WinForms.Conference
{
    partial class CateringWorkOrderDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.workOrderDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.commentsEdit = new DevExpress.XtraEditors.MemoEdit();
            this.menusList = new DevExpress.XtraTreeList.TreeList();
            this.menuNameColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.menuQuantityColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.quantityRepositoryItemSpinEdit = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.serviceTypeEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.roomNameLabel = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.workOrderTimeEdit = new MyVrm.Outlook.WinForms.TimeEdit();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).BeginInit();
            this.ContentPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.workOrderDateEdit.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.workOrderDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.commentsEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.menusList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.quantityRepositoryItemSpinEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.serviceTypeEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.workOrderTimeEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            this.SuspendLayout();
            // 
            // ContentPanel
            // 
            this.ContentPanel.Controls.Add(this.layoutControl1);
            this.ContentPanel.Size = new System.Drawing.Size(510, 255);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.commentsEdit);
            this.layoutControl1.Controls.Add(this.menusList);
            this.layoutControl1.Controls.Add(this.serviceTypeEdit);
            this.layoutControl1.Controls.Add(this.roomNameLabel);
            this.layoutControl1.Controls.Add(this.workOrderTimeEdit);
            this.layoutControl1.Controls.Add(this.workOrderDateEdit);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(510, 255);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // workOrderDateEdit
            // 
            this.workOrderDateEdit.EditValue = null;
            //this.workOrderDateEdit.MinimumSize = new System.Drawing.Size(140, 20);
            //this.workOrderDateEdit.MaximumSize = new System.Drawing.Size(140, 20);
            this.workOrderDateEdit.Location = new System.Drawing.Point(68, 233);
            this.workOrderDateEdit.Name = "workOrderDateEdit";
            this.workOrderDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.workOrderDateEdit.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.workOrderDateEdit.Size = new System.Drawing.Size(138, 20);
            this.workOrderDateEdit.StyleController = this.layoutControl1;
            this.workOrderDateEdit.TabIndex = 8;
            // 
            // commentsEdit
            // 
            this.commentsEdit.Location = new System.Drawing.Point(2, 158);
            this.commentsEdit.Name = "commentsEdit";
            this.commentsEdit.Size = new System.Drawing.Size(506, 71);
            this.commentsEdit.StyleController = this.layoutControl1;
            this.commentsEdit.TabIndex = 7;
            // 
            // menusList
            // 
            this.menusList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.menuNameColumn,
            this.menuQuantityColumn});
            this.menusList.Location = new System.Drawing.Point(2, 46);
            this.menusList.Name = "menusList";
            this.menusList.OptionsView.ShowButtons = false;
            this.menusList.OptionsView.ShowIndicator = false;
            this.menusList.OptionsView.ShowRoot = false;
            this.menusList.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.quantityRepositoryItemSpinEdit});
            this.menusList.Size = new System.Drawing.Size(506, 92);
            this.menusList.TabIndex = 6;
            // 
            // menuNameColumn
            // 
            this.menuNameColumn.Caption = "Name";
            this.menuNameColumn.FieldName = "Name";
            this.menuNameColumn.Name = "menuNameColumn";
            this.menuNameColumn.OptionsColumn.AllowEdit = false;
            this.menuNameColumn.OptionsColumn.AllowFocus = false;
            this.menuNameColumn.OptionsColumn.AllowMove = false;
            this.menuNameColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.menuNameColumn.OptionsColumn.ReadOnly = true;
            this.menuNameColumn.OptionsColumn.ShowInCustomizationForm = false;
            this.menuNameColumn.Visible = true;
            this.menuNameColumn.VisibleIndex = 0;
            // 
            // menuQuantityColumn
            // 
            this.menuQuantityColumn.AppearanceCell.Options.UseTextOptions = true;//ZD 102171
            this.menuQuantityColumn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;//ZD 102171
            this.menuQuantityColumn.Caption = "Quantity";
            this.menuQuantityColumn.ColumnEdit = this.quantityRepositoryItemSpinEdit;
            this.menuQuantityColumn.FieldName = "Quantity";
            this.menuQuantityColumn.Name = "menuQuantityColumn";
            this.menuQuantityColumn.OptionsColumn.AllowMove = false;
            this.menuQuantityColumn.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.menuQuantityColumn.OptionsColumn.ShowInCustomizationForm = false;
            this.menuQuantityColumn.Visible = true;
            this.menuQuantityColumn.VisibleIndex = 1;
            // 
            // quantityRepositoryItemSpinEdit
            // 
            this.quantityRepositoryItemSpinEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near; //ZD 102171
            this.quantityRepositoryItemSpinEdit.AutoHeight = false;
            this.quantityRepositoryItemSpinEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.quantityRepositoryItemSpinEdit.IsFloatValue = false;
            this.quantityRepositoryItemSpinEdit.Mask.EditMask = "N00";
            this.quantityRepositoryItemSpinEdit.Name = "quantityRepositoryItemSpinEdit";
            this.quantityRepositoryItemSpinEdit.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.quantityRepositoryItemSpinEdit_EditValueChanging);
            // 
            // serviceTypeEdit
            // 
            this.serviceTypeEdit.Location = new System.Drawing.Point(68, 22);
            this.serviceTypeEdit.Name = "serviceTypeEdit";
            this.serviceTypeEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.serviceTypeEdit.Properties.NullText = "Select one...";
            this.serviceTypeEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.serviceTypeEdit.Size = new System.Drawing.Size(188, 20);
            this.serviceTypeEdit.StyleController = this.layoutControl1;
            this.serviceTypeEdit.TabIndex = 5;
            this.serviceTypeEdit.SelectedIndexChanged += new System.EventHandler(this.serviceTypeEdit_SelectedIndexChanged);
            // 
            // roomNameLabel
            // 
            this.roomNameLabel.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.roomNameLabel.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.roomNameLabel.Appearance.Options.UseBackColor = true;
            this.roomNameLabel.Appearance.Options.UseFont = true;
            this.roomNameLabel.Location = new System.Drawing.Point(68, 2);
            this.roomNameLabel.Name = "roomNameLabel";
            this.roomNameLabel.Size = new System.Drawing.Size(48, 16);
            this.roomNameLabel.StyleController = this.layoutControl1;
            this.roomNameLabel.TabIndex = 4;
            this.roomNameLabel.Text = "[Room]";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem7});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(510, 255);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.Control = this.roomNameLabel;
            this.layoutControlItem1.CustomizationFormText = "Room";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(510, 20);
            this.layoutControlItem1.Text = "Room";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(62, 16);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.serviceTypeEdit;
            this.layoutControlItem2.CustomizationFormText = "Service Type";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 20);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(258, 24);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(258, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(258, 24);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "Service Type";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(62, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.menusList;
            this.layoutControlItem3.CustomizationFormText = "Menu Items";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 44);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(510, 96);
            this.layoutControlItem3.Text = "Menu Items";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.commentsEdit;
            this.layoutControlItem4.CustomizationFormText = "Comments";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 140);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(510, 91);
            this.layoutControlItem4.Text = "Comments";
            this.layoutControlItem4.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(62, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.workOrderDateEdit;
            this.layoutControlItem5.CustomizationFormText = "Delivery by";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 231);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(208, 24);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(208, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(208, 24);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "Deliver By";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(62, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(304, 231);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(206, 24);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(258, 20);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(252, 24);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // workOrderTimeEdit
            // 
            this.workOrderTimeEdit.EditValue = new System.DateTime(2010, 6, 1, 0, 0, 0, 0);
            this.workOrderTimeEdit.Location = new System.Drawing.Point(210, 233);
            this.workOrderTimeEdit.Name = "workOrderTimeEdit";
            this.workOrderTimeEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.workOrderTimeEdit.Size = new System.Drawing.Size(92, 20);
            this.workOrderTimeEdit.StyleController = this.layoutControl1;
            this.workOrderTimeEdit.TabIndex = 10;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.workOrderTimeEdit;
            this.layoutControlItem7.CustomizationFormText = "Work Order Time";
            this.layoutControlItem7.Location = new System.Drawing.Point(208, 231);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(96, 24);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(96, 24);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(96, 24);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "Work Order Time";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // CateringWorkOrderDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(534, 312);
            this.MinimumSize = new System.Drawing.Size(550, 350);
            this.Name = "CateringWorkOrderDialog";
            this.Text = "Catering Work Order";
            this.Load += new System.EventHandler(this.CateringWorkOrderDialog_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CateringWorkOrderDialog_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.ContentPanel)).EndInit();
            this.ContentPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.workOrderDateEdit.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.workOrderDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.commentsEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.menusList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.quantityRepositoryItemSpinEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.serviceTypeEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.workOrderTimeEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.LabelControl roomNameLabel;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.ComboBoxEdit serviceTypeEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraTreeList.TreeList menusList;
        private DevExpress.XtraTreeList.Columns.TreeListColumn menuNameColumn;
        private DevExpress.XtraTreeList.Columns.TreeListColumn menuQuantityColumn;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.MemoEdit commentsEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.DateEdit workOrderDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit quantityRepositoryItemSpinEdit;
        private TimeEdit workOrderTimeEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
    }
}
