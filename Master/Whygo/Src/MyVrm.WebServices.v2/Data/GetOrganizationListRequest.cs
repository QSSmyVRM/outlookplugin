﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.ObjectModel;

namespace MyVrm.WebServices.Data
{
    internal class GetOrganizationListRequest : ServiceRequestBase<GetOrganizationListResponse>
    {
        public GetOrganizationListRequest(MyVrmService service) : base(service)
        {
        }

        #region Overrides of ServiceRequestBase

        internal override string GetXmlElementName()
        {
            return "Organization";
        }

        internal override string GetCommandName()
        {
            return "GetOrganizationList";
        }

        internal override string GetResponseXmlElementName()
        {
            return "GetOrganizationList";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            UserId.WriteToXml(writer);
        }

        #endregion
    }
}
