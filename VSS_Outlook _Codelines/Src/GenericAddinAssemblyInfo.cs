﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Reflection;

[assembly: AssemblyCompany("myVRM")] // When build need to change myvrm =>myVRM RMT => RMT
[assembly: AssemblyProduct("myVRM Outlook Add-in")]  // When build need to change myvrm =>myVRM Outlook Add-in  RMT => RMT Outlook Add-in
[assembly: AssemblyCopyright("Copyright © myVRM 2009-2016")]
[assembly: AssemblyTrademark("")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2.9.0.0")]
[assembly: AssemblyFileVersion("2.92.163.7")]

