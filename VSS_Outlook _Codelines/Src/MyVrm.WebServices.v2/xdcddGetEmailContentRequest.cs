﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
      internal class GetEmailContentRequest : ServiceRequestBase<GetEmailContentResponse>
      {
        public GetEmailContentRequest(MyVrmService service) : base(service)
        {
        }

        public ConferenceId contentid { get; set; }
       // public string ICalId { get; set; }
       
        public string subject { get; set; }
        public string placeholders { get; set; }
        public string emailtype { get; set; }
        public string emaillanguage { get; set; }
        public string body { get; set; }

        #region Overrides of ServiceRequestBase<GetEmailContentResponse>

        internal override string GetXmlElementName()
        {
            return "emailcontent";
        }

        internal override string GetCommandName()
        {
            return Constants.GetEmailContentCommandName;
        }

        internal override string GetResponseXmlElementName()
        {
            return "emailcontent";
        }

        internal override GetEmailContentResponse ParseResponse(MyVrmServiceXmlReader reader)
        {
            var response = new GetEmailContentResponse();
            response.LoadFromXml(reader, GetResponseXmlElementName());
            return response;
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            UserId.WriteToXml(writer, "UserID");
           // writer.WriteElementValue(XmlNamespace.NotSpecified, "conferenceId", ConferenceId);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "subject", subject);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "body", body);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "emailtype", emailtype);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "emaillanguage", emaillanguage);
        }

        #endregion
      }

        public class GetEmailContentResponse : ServiceResponse
        {
            //Tags
            private const string template = "template";
            private const string emailcontent = "emailcontent";
            private const string body = "body";
            
            private const string confInfo = "confInfo";
            public string ConferenceiCalbody { get; private set; }
            public Conference Conference;

            private const string iCalBodyInfo = "iCalBodyInfo";
            //private const string confInfo = "confInfo";
            //public emailcontent Emailcontent;
            //public Conference Conference;

            //Read template info and template conference instance 
            internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
            {

                try
				{
					switch (reader.LocalName)
					{
                        case "body":
							body = reader.ReadElementValue();
							break;
                        case "externalId":
                            ExternalId = reader.ReadElementValue<long>();
                            break;
                        case "conferenceUrl":
                           ConferenceUrl = reader.ReadElementValue();
                           break;
						case "retCode":
							RetCode = reader.ReadElementValue<int>();
							break;
						case "retMessage":
							RetMessage = reader.ReadElementValue();
							break;
						case "settingsVersion":
							SettingsVersion = reader.ReadElementValue<long>();
							break;
						case "error":
							XMLError = new XMLError();
							XMLError.LoadFromXml(reader, XmlNamespace.NotSpecified, "error");
							break;
					}
					reader.Read();
				}
            }
           
        }

        //public class emailcontent : ComplexProperty
        //{
        //    private const string const_emailcontent = "emailcontent";
        //    private const string const_body = "body";


        //    public string icalbody;
        //    //public string Name;
        //    //public int IsPublic;
        //    //public string OwnerFirstName;
        //    //public string OwnerLastName;
        //    //public bool SetDefault;
        //    //public string Description;

        //    public override object Clone()
        //    {
        //        emailcontent copy = new emailcontent();
        //        copy.icalbody = string.Copy(icalbody);
        //        return copy;
        //    }

        //    internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
        //    {
        //        bool bRet = false;
        //        switch (reader.LocalName)
        //        {
        //            case const_body:
        //                {
        //                    icalbody = reader.ReadElementValue();
        //                    bRet = true;
        //                    break;
        //                }
        //            default:
        //                break;
        //        }
        //        reader.Read();
        //        return bRet;
        //    }

        //    //internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        //    //{
        //    //    writer.WriteStartElement(Namespace, const_TemplateInfo);
        //    //    if (ID != null)
        //    //        writer.WriteElementValue(Namespace, const_ID, ID);
        //    //    if (Name != null)
        //    //        writer.WriteElementValue(Namespace, const_Name, Name);
        //    //    writer.WriteElementValue(Namespace, const_IsPublic, IsPublic.ToString());
        //    //    writer.WriteStartElement(Namespace, const_Owner);
        //    //    if (OwnerFirstName != null)
        //    //        writer.WriteElementValue(Namespace, const_OwnerFirstName, OwnerFirstName);
        //    //    if (OwnerLastName != null)
        //    //        writer.WriteElementValue(Namespace, const_OwnerLastName, OwnerLastName);
        //    //    writer.WriteEndElement();
        //    //    writer.WriteElementValue(Namespace, const_SetDefault, SetDefault.ToString().ToLower());
        //    //    if (Description != null)
        //    //        writer.WriteElementValue(Namespace, const_Description, Description);
        //    //    writer.WriteEndElement();
        //    //}
        //}
  
     
}
