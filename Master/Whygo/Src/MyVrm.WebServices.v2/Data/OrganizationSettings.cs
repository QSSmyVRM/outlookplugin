﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.WebServices.Data
{
    public class OrganizationSettings
    {
        public class Preferences : ComplexProperty
        {
            /// <summary>
            /// If true Audio video work order is available.
            /// </summary>
            public bool IsFacilitiesEnabled { get; set; }
            /// <summary>
            /// If true catering work order is available.
            /// </summary>
            public bool IsCateringEnabled { get; set; }
            /// <summary>
            /// if true house keeping work order is available.
            /// </summary>
            public bool IsHouseKeepingEnabled { get; set; }
			/// <summary>
			/// if true AV Tab is available.
            /// </summary>
			//public bool IsAVTabEnabled { get; set; }
			
			public override object Clone()
			{
				Preferences copy = new Preferences();
				copy.IsCateringEnabled = IsCateringEnabled;
				copy.IsFacilitiesEnabled = IsFacilitiesEnabled;
				copy.IsHouseKeepingEnabled = IsHouseKeepingEnabled;
				//copy.IsAVTabEnabled = IsAVTabEnabled;

				return copy;
			}

            internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
            {
                switch(reader.LocalName)
                {
                    case "EnableFacilites":
                    {
                        IsFacilitiesEnabled = reader.ReadElementValue<bool>();
                        return true;
                    }
                    case "EnableCatering":
                    {
                        IsCateringEnabled = reader.ReadElementValue<bool>();
                        return true;
                    }
                    case "EnableHouseKeeping":
                    {
                        IsHouseKeepingEnabled = reader.ReadElementValue<bool>();
                        return true;
                    }
					//case "AVTabEnable":
					//{
					//    IsAVTabEnabled = reader.ReadElementValue<bool>();
					//    return true;
					//}
                }
                return false;
            }
        }

        public Preferences Preference { get; private set; }
        public OrganizationId OrganizationId { get; private set; }

        internal void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
        {
            do
            {
                reader.Read();
                switch (reader.LocalName)
                {
                    case "preference":
                        Preference = new Preferences();
                        Preference.LoadFromXml(reader, reader.LocalName);
                        break;
                    case "organizationID":
                        OrganizationId = new OrganizationId();
                        OrganizationId.LoadFromXml(reader, reader.LocalName);
                        break;
                    default:
                        reader.SkipCurrentElement();
                        break;
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, xmlElementName));
        }
    }
}
