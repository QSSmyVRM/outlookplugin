/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.ComponentModel;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout.Utils;

namespace MyVrm.Outlook.WinForms
{
    /// <summary>
    /// Represents base dialog class
    /// </summary>
    public partial class Dialog : Form
    {
        /// <summary>
        /// Initializes <see cref="Dialog"/> class.
        /// </summary>
        public Dialog()
        {
            InitializeComponent();
            okButton.Text = Strings.DialogOkButtonText;
            cancelButton.Text = Strings.DialogCancelButtonText;
        	Text = string.Empty; //??
        	applyButton.Text = Strings.ApplyBtnText;
        	applyButton.Visible = false;
        	applyButton.Enabled = false;
        }

        /// <summary>
        /// Holds controls on the dialog form
        /// </summary>
        public PanelControl ContentPanel
        {
            get { return contentPanel; }
        }

        /// <summary>
        /// Enables or disables Cancel button
        /// </summary>
        [DefaultValue(true)]
        public bool CancelEnabled
        {
            get { return cancelButton.Enabled; }
            set { cancelButton.Enabled = value; }
        }

		/// <summary>
		/// Enables or disables OK button
		/// </summary>
		[DefaultValue(true)]
		public bool OkEnabled
		{
			get { return okButton.Enabled; }
			set { okButton.Enabled = value; }
		}

		/// <summary>
		/// Enables or disables Applay button
		/// </summary>
		[DefaultValue(true)]
		public bool ApplyEnabled
		{
			get { return applyButton.Enabled; }
			set { applyButton.Enabled = value; }
		}

        /// <summary>
        /// Shows or hides Cancel button
        /// </summary>
        [DefaultValue(true)]
        public bool CancelVisible
        {
            get { return cancelButtonLayoutControlItem.Visible; }
            set { cancelButtonLayoutControlItem.Visibility = value ? LayoutVisibility.Always : LayoutVisibility.Never; }
        }

               
		/// <summary>
		/// Shows or hides OK button
		/// </summary>
		[DefaultValue(true)]
		public bool OkVisible
		{
			get { return okButtonLayoutControlItem.Visible; }
			set { okButtonLayoutControlItem.Visibility = value ? LayoutVisibility.Always : LayoutVisibility.Never; }
		}

		[DefaultValue(true)]
		public string CancelBnText
		{
			get { return cancelButton.Text; }
			set { cancelButton.Text = value; }
		}
		/// <summary>
		/// Shows or hides OK button
		/// </summary>
		[DefaultValue(true)]
		public bool ApplyVisible
		{
			get { return applyButtonLayoutControlItem.Visible; }
			set { applyButtonLayoutControlItem.Visibility = value ? LayoutVisibility.Always : LayoutVisibility.Never; }
		}

        /// <summary>
        /// Gets or sets OK button text.
        /// </summary>
        [DefaultValue("OK")]
        public string OkText
        {
            get { return okButton.Text; }
            set { okButton.Text = value; }
        }
        /// <summary>
        /// Gets or sets Cancel button text.
        /// </summary>
        [DefaultValue("Cancel")]
        public string CancelText
        {
            get { return cancelButton.Text; }
            set { cancelButton.Text = value; }
        }

		/// <summary>
		/// Gets or sets Cancel button text.
		/// </summary>
		[DefaultValue("Apply")]
		public string ApplyText
		{
			get { return applyButton.Text; }
			set { applyButton.Text = value; }
		}

        private void CloseOnClick(object sender, EventArgs e)
        {
            Close();
        }
    }
}