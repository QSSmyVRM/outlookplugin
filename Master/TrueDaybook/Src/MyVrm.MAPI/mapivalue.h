#pragma once

#include "Stdafx.h"

namespace MyVrm 
{
	namespace MAPI
	{
		ref class MapiPropValue;

		public value class MapiValue
		{		
			MapiTags tag;
			Object ^value;
			void ReadPropValue(LPSPropValue pPropValue);
		internal:
			int WriteNativeData(LPSPropValue pPropValue, void *pBuf);
		public:
			MapiValue(MapiTags tag, Object ^value);
			MapiValue(IntPtr pPropValue);
			static MapiValue Create(IntPtr pPropValue);
			void ThrowIfError();

			virtual String ^ToString() override;
			virtual array<Byte>^ ToByteArray();
			virtual Int32 ToInt32();
			virtual Int64 ToInt64();
			virtual Boolean ToBoolean();

			virtual DateTime ToDateTime();
			virtual operator String ^();
			virtual operator array<Byte> ^();
			virtual operator int();

			virtual array<array<Byte>^>^ ToByteArrayMV();
			virtual array<String^>^ ToStringMV();
			virtual array<Int32>^ ToInt32MV();

			property MapiTags Tag { MapiTags get() { return tag; }; void set(MapiTags value) { tag = value; } };
			property bool IsError { bool get() { return PROP_TYPE(ULONG(tag)) == PT_ERROR; } }
			property bool IsNull {bool get() { return value == nullptr;} }
			property System::Object^ Value { 
				System::Object^ get() 
				{
					if (IsError)
					{
						return nullptr;
					}
					return this->value;
				};
			}
		};
	}
}
