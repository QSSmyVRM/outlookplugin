﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace MyVrm.WebServices.Data
{
    internal class EditRecurringConferenceInstanceRequest : ServiceRequestBase<SaveConferenceResponse>
    {
        public EditRecurringConferenceInstanceRequest(MyVrmService service) : base(service)
        {
        }

        internal DateTime OrigOccurrenceDate { get; set; }
        internal Conference ConferenceOccurrence { get; set; }

        #region Overrides of ServiceRequestBase<EditRecurringConferenceInstanceResponse>

        internal override string GetXmlElementName()
        {
            return "editrecurinstance";
        }

        internal override string GetCommandName()
        {
            return "EditRecurInstance";
        }

        internal override string GetResponseXmlElementName()
        {
            return "setConference";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            writer.WriteElementValue(XmlNamespace.NotSpecified, "confdate", OrigOccurrenceDate);
            writer.WriteStartElement(XmlNamespace.NotSpecified, "conference");
            {
                UserId.WriteToXml(writer);
                ConferenceOccurrence.WriteToXml(writer);
            }
            writer.WriteEndElement();
        }

        #endregion
    }
}
