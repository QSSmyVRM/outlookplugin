﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;

namespace CallWebApi.Classes
{
    /// <summary>
    /// Response from Web Api actions with information about added/updated conference
    /// </summary>
    public class ConferenceResponse : BaseResponse
    {
        /// <summary>
        /// Conference Id 
        /// </summary>
        public int Id { set; get; }

        /// <summary>
        /// Radvision ID
        /// </summary>
        public int ExternalId { set; get; }

		/// <summary>
		/// Conference link to add to a message
		/// </summary>
		public string ConferenceUrl { set; get; }
    }
}