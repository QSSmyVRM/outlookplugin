﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyVrm.WebServices.Data;
using Microsoft.Office.Interop.Outlook;
using System.ComponentModel;
namespace MyVrm.Outlook
{
    public class Utils
    {
        private static string EMAIL1_ENTRYID  = "http://schemas.microsoft.com/mapi/id/{00062004-0000-0000-C000-000000000046}/80850102" ;

        public static BindingList<UsersDatasource> GetOutlookContacts(object application)
        {
            BindingList<UsersDatasource> outlookUsers = new BindingList<UsersDatasource>();
            Microsoft.Office.Interop.Outlook.Application applicationClass = application as Microsoft.Office.Interop.Outlook.Application;
            if (applicationClass != null)
            {
                MyVrmAddin.TraceSource.TraceInformation("......................Into the app class..app class not null.....................");//101677
                MAPIFolder folderContacts = applicationClass.Session.GetDefaultFolder(OlDefaultFolders.olFolderContacts);
                MyVrmAddin.TraceSource.TraceInformation("......................Getting Folder contacts Sucessfull.....................");//101677
                Items contacts = folderContacts.Items;
                MyVrmAddin.TraceSource.TraceInformation("......................Into the items(contacts).....................");//101677
                foreach (ContactItem item in contacts)
                {
                    MyVrmAddin.TraceSource.TraceInformation("......................into each contact.....................");//101677
                    UsersDatasource us = new UsersDatasource();
                    us.FirstName = item.FirstName;
                    us.LastName = item.LastName;
                    if (item.Email1AddressType != "EX")
                    {
                        us.Email = item.Email1Address;
                        MyVrmAddin.TraceSource.TraceInformation("......................email id not in EX.....................");//101677
                    
                    }
                    else
                    {
                        MyVrmAddin.TraceSource.TraceInformation("......................email id in EX.....................");//101677
                        PropertyAccessor propertyAccessor = item.PropertyAccessor;
                        object rawPropertyValue = propertyAccessor.GetProperty(EMAIL1_ENTRYID);
                        string recipientEntryID = propertyAccessor.BinaryToString(rawPropertyValue);
                        Recipient recipient = item.Application.Session.GetRecipientFromID(recipientEntryID);
                        if (null == recipient)
                            continue;

                        bool wasResolved = recipient.Resolve();
                        if (!wasResolved)
                            continue;
                        ExchangeUser exchangeUser = recipient.AddressEntry.GetExchangeUser();
                        MyVrmAddin.TraceSource.TraceInformation(".....................getting exchange user sucess.....................");//101677

                        us.Email = exchangeUser.PrimarySmtpAddress;
                        MyVrmAddin.TraceSource.TraceInformation("..................... " + us.Email.ToString() + " .....................");//101677

                    }
                    
                    outlookUsers.Add(us);

                }

                
            }
            return outlookUsers;
        }
    }
}
