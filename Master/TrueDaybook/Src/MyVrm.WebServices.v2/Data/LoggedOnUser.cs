﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.WebServices.Data
{
    [ServiceObjectDefinition("user")]
    public class LoggedOnUser : ServiceObject
    {
        internal LoggedOnUser(MyVrmService service) : base(service)
        {
        }

        public bool IsNewUser
        {
            get
            {
				bool bRet = false;
				if (PropertyBag[LoggedOnUserSchema.IsNew] != null)
					bool.TryParse(PropertyBag[LoggedOnUserSchema.IsNew].ToString(), out bRet);
				return bRet;//(bool)PropertyBag[LoggedOnUserSchema.IsNew];
            }
        }

        public LoggedOnUserGlobalInfo GlobalInfo
        {
            get
            {
                return (LoggedOnUserGlobalInfo) PropertyBag[LoggedOnUserSchema.GlobalInfo];
            }
        }

        #region Overrides of ServiceObject

        internal override ServiceObjectSchema GetSchema()
        {
            return LoggedOnUserSchema.Instance;
        }

        #endregion

    }
}
