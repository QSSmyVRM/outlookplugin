﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Windows.Forms;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook.WinForms
{
    public partial class AboutDialog : Dialog
    {
        public AboutDialog()
        {
            InitializeComponent();
            versionLayoutControlItem.Text = Strings.AboutDialogVersionText;
            supportContactLayoutControlItem.Text = Strings.AboutDialogSupportContactText;
			supportContactLayoutControlItem.CustomizationFormText = Strings.AboutDialogSupportContactText;
            supportEmailLayoutControlItem.Text = Strings.AboutDialogSupportEmailText;
			supportEmailLayoutControlItem.CustomizationFormText = Strings.AboutDialogSupportEmailText;
            supportPhoneLayoutControlItem.Text = Strings.AboutDialogSupportPhoneText;
			supportPhoneLayoutControlItem.CustomizationFormText = Strings.AboutDialogSupportPhoneText;
            infoLayoutControlItem.Text = Strings.AboutDialogInfoText;
			infoLayoutControlItem.CustomizationFormText = Strings.AboutDialogInfoText;

            CancelVisible = false;
			productLabel.Text = MyVrmAddin.ProductDisplayName;//ProductName;
            versionLabel.Text = MyVrmAddin.ProductVersion;
            copyrightEdit.Text = MyVrmAddin.Copyright;
			Text = Strings.AboutLableText;
			ApplyEnabled = false;
			ApplyVisible = false;
        }

        private void AboutDialog_Load(object sender, EventArgs e)
        {
            var cursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var contactInfo = MyVrmService.Service.OrganizationOptions.Contact;
                contactNameEdit.Text = contactInfo.Name;
                contactEmailEdit.Text = "emailto:" + contactInfo.Email;
                contactEmailEdit.Properties.Caption = contactInfo.Email;
                contactPhoneEdit.Text = contactInfo.Phone;
                additionInfoEdit.Text = contactInfo.AdditionInfo;
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }
            finally
            {
                Cursor.Current = cursor;
            }
        }
    }
}
