#include "stdafx.h"
#include "MapiValue.h"
#include "MapiException.h"

using namespace MyVrm::MAPI;
using namespace System::Runtime::InteropServices;

MapiValue::MapiValue(IntPtr pPropValue)
{
	LPSPropValue lpProp = (LPSPropValue)pPropValue.ToPointer();
	ReadPropValue(lpProp);	
}

MapiValue::MapiValue(MapiTags tag, Object ^value)
{
	this->tag = tag;
	this->value = value;
}

void MapiValue::ThrowIfError()
{
	if (IsError)
	{
		try
		{
			HRESULT hRes = safe_cast<HRESULT>(value);
			MapiException::ThrowIfError(hRes, String::Empty);
		}
		catch(InvalidCastException^)
		{
			// if for some reason value doesn't contain error code
			MapiException::ThrowIfError(MAPI_E_CALL_FAILED, "Error value.");
		}
	}
}

void MapiValue::ReadPropValue(LPSPropValue pPropValue)
{
	tag = (MapiTags)pPropValue->ulPropTag;

	switch (PROP_TYPE(pPropValue->ulPropTag))
	{
		case PT_UNSPECIFIED:
		case PT_NULL:
			break;
		case PT_I2:
			value = pPropValue->Value.i;
			break;
		case PT_LONG:
			value = pPropValue->Value.l;
			break;
		case PT_R4:
			value = pPropValue->Value.flt;
			break;
		case PT_DOUBLE:
			value = pPropValue->Value.dbl;
			break;
		case PT_CURRENCY:
			value = pPropValue->Value.cur.int64;
			break;
		case PT_APPTIME:
			value = pPropValue->Value.at;
			break;
		case PT_BOOLEAN:
			value = pPropValue->Value.b;
			break;
		case PT_ERROR:
			value = pPropValue->Value.err;
			break;
		case PT_OBJECT:
			break;
		case PT_I8:
			value = pPropValue->Value.li.QuadPart;
			break;
		case PT_STRING8:
			value = gcnew String(pPropValue->Value.lpszA);
			break;
		case PT_UNICODE:
			value = gcnew String(pPropValue->Value.lpszW);
			break;
		case PT_SYSTIME:
		{
			DWORD h = pPropValue->Value.ft.dwHighDateTime;
			DWORD l = pPropValue->Value.ft.dwLowDateTime;
			value = DateTime::FromFileTimeUtc(((__int64)h << 32) + l);
			break;
		}
		case PT_CLSID:
			throw gcnew Exception("PT_CLSID not supported.");
			break;
		case PT_BINARY:
		{
			array<Byte> ^arr = gcnew array<Byte>(pPropValue->Value.bin.cb);
			if (arr->Length > 0)
				Marshal::Copy(IntPtr(pPropValue->Value.bin.lpb), arr, 0, pPropValue->Value.bin.cb);
			value = arr;
			break;
		}
		// Multi-value types
		case PT_MV_I2:
		{
			array<short> ^arr = gcnew array<short>(pPropValue->Value.MVi.cValues);
			for (ULONG i = 0; i < pPropValue->Value.MVi.cValues; i++)
				arr[i] = pPropValue->Value.MVi.lpi[i];
			value = arr;
			break;
		}
		case PT_MV_LONG:
		{
			array<LONG> ^arr = gcnew array<LONG>(pPropValue->Value.MVl.cValues);
			for (ULONG i = 0; i < pPropValue->Value.MVl.cValues; i++)
				arr[i] = pPropValue->Value.MVl.lpl[i];
			value = arr;
			break;
		}
		case PT_MV_R4:
		{
			array<float> ^arr = gcnew array<float>(pPropValue->Value.MVflt.cValues);
			for (ULONG i = 0; i < pPropValue->Value.MVflt.cValues; i++)
				arr[i] = pPropValue->Value.MVflt.lpflt[i];
			value = arr;
			break;
		}
		case PT_MV_DOUBLE:
		{
			array<double> ^arr = gcnew array<double>(pPropValue->Value.MVdbl.cValues);
			for (ULONG i = 0; i < pPropValue->Value.MVdbl.cValues; i++)
				arr[i] = pPropValue->Value.MVdbl.lpdbl[i];
			value = arr;
			break;
		}
		case PT_MV_CURRENCY:
		{
			array<LONGLONG> ^arr = gcnew array<LONGLONG>(pPropValue->Value.MVcur.cValues);
			for (ULONG i = 0; i < pPropValue->Value.MVcur.cValues; i++)
				arr[i] = pPropValue->Value.MVcur.lpcur[i].int64;
			value = arr;
			break;
		}
		case PT_MV_APPTIME:
		{
			array<double> ^arr = gcnew array<double>(pPropValue->Value.MVat.cValues);
			for (ULONG i = 0; i < pPropValue->Value.MVat.cValues; i++)
				arr[i] = pPropValue->Value.MVat.lpat[i];
			value = arr;
			break;
		}
		case PT_MV_SYSTIME:
		{
			array<System::DateTime> ^arr = gcnew array<System::DateTime>(pPropValue->Value.MVft.cValues);
			for (ULONG i = 0; i < pPropValue->Value.MVft.cValues; i++)
			{
				DWORD h = pPropValue->Value.MVft.lpft[i].dwHighDateTime;
				DWORD l = pPropValue->Value.MVft.lpft[i].dwLowDateTime;
				arr[i] = DateTime::FromFileTimeUtc(((__int64)h << 32) + l);
			}
			value = arr;
			break;
		}
		case PT_MV_BINARY:
		{
			array<array<Byte> ^> ^arr = gcnew array<array<Byte> ^>(pPropValue->Value.MVbin.cValues);
			for (ULONG i = 0; i < pPropValue->Value.MVbin.cValues; i++)
			{
				array<Byte> ^arr2 = gcnew array<Byte>(pPropValue->Value.MVbin.lpbin[i].cb);
				if (arr2->Length > 0)
					Marshal::Copy(IntPtr(pPropValue->Value.MVbin.lpbin[i].lpb), arr2, 0, pPropValue->Value.MVbin.lpbin[i].cb);
				arr[i] = arr2;
			}
			value = arr;
			break;
		}
		case PT_MV_STRING8:
		{
			array<String ^> ^arr = gcnew array<String ^>(pPropValue->Value.MVszA.cValues);
			for (ULONG i = 0; i < pPropValue->Value.MVszA.cValues; i++)
				arr[i] = gcnew String(pPropValue->Value.MVszA.lppszA[i]);
			value = arr;
			break;
		}
		case PT_MV_UNICODE:
		{
			array<String ^> ^arr = gcnew array<String ^>(pPropValue->Value.MVszW.cValues);
			for (ULONG i = 0; i < pPropValue->Value.MVszW.cValues; i++)
				arr[i] = gcnew String(pPropValue->Value.MVszW.lppszW[i]);
			value = arr;
			break;
		}
		case PT_MV_CLSID:
			throw gcnew Exception("PT_MV_CLSID is not supported.");
		case PT_MV_I8:
		{
			array<LONGLONG> ^arr = gcnew array<LONGLONG>(pPropValue->Value.MVli.cValues);
			for (ULONG i = 0; i < pPropValue->Value.MVli.cValues; i++)
				arr[i] = pPropValue->Value.MVli.lpli[i].QuadPart;
			value = arr;
			break;
		}
	};
}

MapiValue MapiValue::Create(IntPtr pPropValue)
{
	return MapiValue(pPropValue);
}

MapiValue::operator String ^()
{
	return ToString();
}

MapiValue::operator array<Byte> ^()
{
	return ToByteArray();
}

MapiValue::operator int()
{
	return ToInt32();
}

String^ MapiValue::ToString()
{
	ThrowIfError();
	return value->ToString();
}

array<Byte>^ MapiValue::ToByteArray()
{
	ThrowIfError();
	if (value->GetType() == array<Byte>::typeid)
	{
		 return safe_cast<array<Byte>^>(value);
	}
	throw gcnew Exception("Cannot convert to ByteArray");
}

int MapiValue::ToInt32()
{
	ThrowIfError();
	return System::Convert::ToInt32(value);
}

Int64 MapiValue::ToInt64()
{
	ThrowIfError();
	return System::Convert::ToInt64(value);
}

Boolean MapiValue::ToBoolean()
{
	ThrowIfError();
	return System::Convert::ToBoolean(value);
}

DateTime MapiValue::ToDateTime()
{
	ThrowIfError();
	if (value->GetType() == System::Int64::typeid)
	{
		try
		{
			return DateTime::FromFileTimeUtc(safe_cast<System::Int64>(value));
		}
		catch(ArgumentOutOfRangeException^)
		{
			return DateTime::MaxValue;
		}
	}
	return safe_cast<DateTime>(value);
}

array<String^>^ MapiValue::ToStringMV()
{
	ThrowIfError();
	if (value->GetType() == array<String^>::typeid)
	{
		 return safe_cast<array<String^>^>(value);
	}
	throw gcnew Exception("Cannot convert to StringMV");
}

array<Int32>^ MapiValue::ToInt32MV()
{
	ThrowIfError();
	if (value->GetType() == array<Int32>::typeid)
	{
		 return safe_cast<array<Int32>^>(value);
	}
	throw gcnew Exception("Cannot convert to Int32MV");
}

array<array<Byte>^>^ MapiValue::ToByteArrayMV()
{
	ThrowIfError();
	if (value->GetType() == array<array<Byte>^>::typeid)
	{
		 return safe_cast<array<array<Byte>^>^>(value);
	}
	throw gcnew Exception("Cannot convert to ByteArrayMV");
}


int MapiValue::WriteNativeData(LPSPropValue pPropValue, void *pBuf)
{
	int size = 0;

	if (pPropValue)
	{
		pPropValue->dwAlignPad = 0;
		pPropValue->ulPropTag = (ULONG)tag;
	}

	switch (PROP_TYPE(ULONG(tag)))
	{
	case PT_UNSPECIFIED:
	case PT_NULL:
		break;
	case PT_I2:
		if (pPropValue)
			pPropValue->Value.i = ToInt32();
		break;
	case PT_LONG:
		if (pPropValue)
			pPropValue->Value.l = ToInt32();
		break;
	case PT_R4:
	case PT_DOUBLE:
		throw gcnew Exception("Saving floating point properties is not supported.");
		break;
	case PT_CURRENCY:
		if (pPropValue)
			pPropValue->Value.cur.int64 = ToInt64();
		break;
	case PT_BOOLEAN:
		if (pPropValue)
			pPropValue->Value.b = ToBoolean();
		break;
	case PT_APPTIME:
	case PT_ERROR:
	case PT_OBJECT:
		throw gcnew Exception("Saving "+tag.ToString()+" property is not supported.");
		break;
	case PT_I8:
		if (pPropValue)
			pPropValue->Value.li.QuadPart = ToInt64();
		break;
	case PT_STRING8:
		{
			char *pString = (char *)Marshal::StringToHGlobalAnsi(ToString()).ToPointer();
			try
			{
				size = strlen(pString)+1;
				if (pBuf)
				{
					memcpy(pBuf, pString, size);
					if (pPropValue)
						pPropValue->Value.lpszA = (LPSTR)pBuf;
				}
			}
			finally
			{
				Marshal::FreeHGlobal(IntPtr(pString));
			}
		}
		break;
	case PT_UNICODE:
		{
			WCHAR *pString = (WCHAR *)Marshal::StringToHGlobalUni(ToString()).ToPointer();
			try
			{
				size = (wcslen(pString)+1)*sizeof(WCHAR);
				if (pBuf)
				{
					memcpy(pBuf, pString, size);
					if (pPropValue)
						pPropValue->Value.lpszW = (LPWSTR)pBuf;
				}
			}
			finally
			{
				Marshal::FreeHGlobal(IntPtr(pString));
			}
		}
		break;
	case PT_SYSTIME:
		{
			if (pPropValue && value)
			{
				if (value->GetType() == (DateTime::typeid))
				{
					DateTime ^time = static_cast<DateTime^>(value);
					if (time != DateTime::MinValue)
					{
						try
						{
							__int64 ft = time->ToFileTimeUtc();
							pPropValue->Value.ft.dwHighDateTime = HIDWORD(ft);
							pPropValue->Value.ft.dwLowDateTime = LODWORD(ft);
						}
						catch(ArgumentOutOfRangeException^)
						{
							pPropValue->Value.ft.dwHighDateTime = 0;
							pPropValue->Value.ft.dwLowDateTime = 0;
						}
					}
					else
					{
						pPropValue->Value.ft.dwHighDateTime = 0;
						pPropValue->Value.ft.dwLowDateTime = 0;
					}
				}
				else if ((value->GetType() == (System::Int64::typeid)))
				{
					__int64 ft = ToInt64();
					pPropValue->Value.ft.dwHighDateTime = HIDWORD(ft);
					pPropValue->Value.ft.dwLowDateTime = LODWORD(ft);
				}
				else
				{
					throw gcnew Exception("PropType.SysTime can only be of DateTime or long format.");
				}
			}
			break;
		}
	case PT_CLSID:
		throw gcnew Exception("Saving "+tag.ToString()+" property is not supported.");
		break;
	case PT_BINARY:
		{
			array<Byte> ^tmpArray = ToByteArray();
			size = tmpArray->Length;
			if (pPropValue)
			{
				pPropValue->Value.bin.cb = size;
				pPropValue->Value.bin.lpb = 0;
			}
			if (pBuf && size)
			{
				pPropValue->Value.bin.lpb = (LPBYTE)pBuf;
				Marshal::Copy(tmpArray, 0, IntPtr(pBuf), size);
			}
			break;
		}
	case PT_MV_I2:
		{
			array<short> ^arr = (array<short> ^)value;
			size = arr->Length * sizeof(short);
			if (pPropValue)
			{
				pPropValue->Value.MVi.cValues = arr->Length;
				pPropValue->Value.MVi.lpi = (short *)pBuf;
			}
			if (pBuf && size)
				Marshal::Copy(arr, 0, IntPtr(pBuf), arr->Length);
			break;
		}
	case PT_MV_LONG:
		{
			array<int> ^arr = (array<int> ^)value;
			size = arr->Length * sizeof(int);
			if (pPropValue)
			{
				pPropValue->Value.MVl.cValues = arr->Length;
				pPropValue->Value.MVl.lpl = (LONG *)pBuf;
			}
			if (pBuf && size)
				Marshal::Copy(arr, 0, IntPtr(pBuf), arr->Length);
			break;
		}
	case PT_MV_R4:
		{
			array<float> ^arr = (array<float> ^)value;
			size = arr->Length * sizeof(float);
			if (pPropValue)
			{
				pPropValue->Value.MVflt.cValues = arr->Length;
				pPropValue->Value.MVflt.lpflt = (float *)pBuf;
			}
			if (pBuf && size)
				Marshal::Copy(arr, 0, IntPtr(pBuf), arr->Length);
			break;
		}
	case PT_MV_DOUBLE:
		{
			array<double> ^arr = (array<double> ^)value;
			size = arr->Length * sizeof(double);
			if (pPropValue)
			{
				pPropValue->Value.MVdbl.cValues = arr->Length;
				pPropValue->Value.MVdbl.lpdbl = (double *)pBuf;
			}
			if (pBuf && size)
				Marshal::Copy(arr, 0, IntPtr(pBuf), arr->Length);
			break;
		}
	case PT_MV_CURRENCY:
		{
			array<Int64> ^arr = (array<Int64> ^)value;
			size = arr->Length * sizeof(Int64);
			if (pPropValue)
			{
				pPropValue->Value.MVcur.cValues = arr->Length;
				pPropValue->Value.MVcur.lpcur = (CURRENCY *)pBuf;
			}
			if (pBuf && size)
				Marshal::Copy(arr, 0, IntPtr(pBuf), arr->Length);
			break;
		}
	case PT_MV_APPTIME:
		{
			array<double> ^arr = (array<double> ^)value;
			size = arr->Length * sizeof(double);
			if (pPropValue)
			{
				pPropValue->Value.MVat.cValues = arr->Length;
				pPropValue->Value.MVat.lpat = (double *)pBuf;
			}
			if (pBuf && size)
				Marshal::Copy(arr, 0, IntPtr(pBuf), arr->Length);
			break;
		}
	case PT_MV_SYSTIME:
		{
			Array ^arr = (Array ^)value;
			size = arr->Length * sizeof(::FILETIME);
			if (pPropValue)
			{
				pPropValue->Value.MVft.cValues = arr->Length;
				pPropValue->Value.MVft.lpft = (::FILETIME *)pBuf;
				::FILETIME *current = (::FILETIME *)pBuf;
				if (pBuf && size)
					for (int i = 0; i < arr->Length; i++, current += sizeof(::FILETIME))
					{
						if (arr->GetValue(i)->GetType() == (DateTime::typeid))
						{
							DateTime ^time = static_cast<DateTime^>(arr->GetValue(i));
							if (time != DateTime::MinValue)
							{
								try
								{
									__int64 ft = time->ToFileTimeUtc();
									current->dwHighDateTime = HIDWORD(ft);
									current->dwLowDateTime = LODWORD(ft);
								}
								catch(ArgumentOutOfRangeException^)
								{
									current->dwHighDateTime = 0;
									current->dwLowDateTime = 0;
								}
							}
							else
							{
								current->dwHighDateTime = 0;
								current->dwLowDateTime = 0;
							}
						}
						else if ((arr->GetValue(i)->GetType() == (System::Int64::typeid)))
						{
							__int64 ft = System::Convert::ToInt64(arr->GetValue(i));
							current->dwHighDateTime = HIDWORD(ft);
							current->dwLowDateTime = LODWORD(ft);
						}
						else
						{
							throw gcnew Exception("PropType.SysTime can only be of DateTime or long format.");
						}
					}
			}
			break;
		}
		case PT_MV_STRING8:
		{
			array<String ^> ^arr = (array<String ^> ^)value;
			size = arr->Length * sizeof(LPSTR);
			if (pPropValue)
			{
				pPropValue->Value.MVszA.cValues = arr->Length;
				pPropValue->Value.MVszA.lppszA = (LPSTR *)pBuf;
				pBuf = (char *)pBuf + size;
			}
			for (int i = 0; i < arr->Length; i++)
			{
				char *pString = (char *)Marshal::StringToHGlobalAnsi(arr[i]).ToPointer();
				try
				{
					int strSize = strlen(pString)+1;
					size += strSize;
					if (pBuf)
					{
						memcpy(pBuf, pString, strSize);
						if (pPropValue)
							pPropValue->Value.MVszA.lppszA[i] = (LPSTR)pBuf;
						pBuf = (char *)pBuf + strSize;
					}
				}
				finally
				{
					Marshal::FreeHGlobal(IntPtr(pString));
				}
			}
			break;
		}
		case PT_MV_UNICODE:
		{
			array<String ^> ^arr = (array<String ^> ^)value;
			size = arr->Length * sizeof(LPWSTR);
			if (pPropValue)
			{
				pPropValue->Value.MVszW.cValues = arr->Length;
				pPropValue->Value.MVszW.lppszW = (LPWSTR *)pBuf;
				pBuf = (char *)pBuf + size;
			}
			for (int i = 0; i < arr->Length; i++)
			{
				WCHAR *pString = (WCHAR *)Marshal::StringToHGlobalUni(arr[i]).ToPointer();
				try
				{
					int strSize = (wcslen(pString)+1)*sizeof(WCHAR);
					size += strSize;
					if (pBuf)
					{
						memcpy(pBuf, pString, strSize);
						if (pPropValue)
							pPropValue->Value.MVszW.lppszW[i] = (LPWSTR)pBuf;
						pBuf = (char *)pBuf + strSize;
					}
				}
				finally
				{
					Marshal::FreeHGlobal(IntPtr(pString));
				}
			}
			break;
		}
		case PT_MV_CLSID:
			throw gcnew Exception("Saving "+tag.ToString()+" property is not supported.");
			break;
		case PT_MV_I8:
		{
			array<Int64> ^arr = (array<Int64> ^)value;
			size = arr->Length * sizeof(Int64);
			if (pPropValue)
			{
				pPropValue->Value.MVli.cValues = arr->Length;
				pPropValue->Value.MVli.lpli = (LARGE_INTEGER *)pBuf;
			}
			if (pBuf && size)
				Marshal::Copy(arr, 0, IntPtr(pBuf), arr->Length);
			break;
		}
		case PT_MV_BINARY:
		{
			array<array<Byte>^> ^arr = (array<array<Byte>^> ^)value;
			size = arr->Length * sizeof(SBinary);
			if (pPropValue)
			{
				pPropValue->Value.MVbin.cValues = arr->Length;
				pPropValue->Value.MVbin.lpbin = (SBinary *)pBuf;
				pBuf = (char *)pBuf + size;
			}
			for (int i = 0; i < arr->Length; i++)
			{
				size += arr[i]->Length;
				if (pPropValue)
				{
					pPropValue->Value.MVbin.lpbin[i].cb = arr[i]->Length;
					pPropValue->Value.MVbin.lpbin[i].lpb = (LPBYTE)pBuf;
				}
				if (pBuf && (arr[i]->Length > 0))
				{
					Marshal::Copy(arr[i], 0, IntPtr(pBuf), arr[i]->Length);
					pBuf = (char *)pBuf + arr[i]->Length;
				}
			}
			break;
		}
	};

	return size;
}