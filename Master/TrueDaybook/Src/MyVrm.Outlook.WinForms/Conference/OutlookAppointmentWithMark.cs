﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;

namespace MyVrm.Outlook.WinForms.Conference
{
	class OutlookAppointmentWithMark
	{
		public DateTime AppointmentStartTime{ get; set; }
		public DateTime AppointmentEndTime { get; set; }
		public DateTime OriginalStart { get; set; }

		public bool IsDeleted { get; set; }
	}
}
