﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	internal class GetAllOrgSettingsRequest : ServiceRequestBase<GetAllOrgSettingsResponse>
	{
		public GetAllOrgSettingsRequest(MyVrmService service)
			: base(service)
		{
		}

		#region Overrides of ServiceRequestBase<GetAllOrgSettingsResponse>

		internal override string GetXmlElementName()
		{
			return "GetAllOrgSettings";
		}

		internal override string GetCommandName()
		{
			return "GetAllOrgSettings";
		}

		internal override string GetResponseXmlElementName()
		{
			return "GetAllOrgSettings";
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			OrganizationId.WriteToXml(writer);
		}

		#endregion
	}

	internal class GetAllOrgSettingsResponse : ServiceResponse
	{
		public GetAllOrgSettingsResponse()
		{
			Settings = new OrganizationSettingsNew();
		}

		public OrganizationSettingsNew Settings { get; private set; }

		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{
			Settings.LoadFromXml(reader, "GetAllOrgSettings");
		}
	}


	public class OrganizationSettingsNew
	{
		public class Preferences
		{
			/// <summary>
			/// If true Audio video work order is available.
			/// </summary>
			public bool IsFacilitiesEnabled { get; set; }
			/// <summary>
			/// If true catering work order is available.
			/// </summary>
			public bool IsCateringEnabled { get; set; }
			/// <summary>
			/// if true house keeping work order is available.
			/// </summary>
			public bool IsHouseKeepingEnabled { get; set; }

			/// <summary>
			/// if true Blue Jeans conference is available.
			/// </summary>
			public bool IsBlueJeansEnabled { get; set; }
			/// <summary>
			/// if true Jabber conference is available.
			/// </summary>
			public bool IsJabberEnabled { get; set; }
			/// <summary>
			/// if true Lync conference is available.
			/// </summary>
			public bool IsLyncEnabled { get; set; }
			/// <summary>
			/// if true Vidtel conference is available.
			/// </summary>
			public bool IsVidtelEnabled { get; set; }
			/// <summary>
			/// if true Cloud conference is available.
			/// </summary>
			public bool IsCloudEnabled { get; set; }


            public bool EnablePublicConferencing { get; set; }

            public int NetworkSwitching { get; set; }
			/// <summary>
			/// if true Profile Selection is available.
			/// </summary>
			public bool IsProfileSelectionEnabled { get; set; }
			/// <summary>
			/// if true Secure connection is available.
			/// </summary>
			public bool IsSecureSwitchEnabled { get; set; }
			
			public bool EnableOnsiteAV { get; set; }
			public bool EnableMeetandGreet{ get; set; }
			public bool EnableConciergeMonitoring { get; set; }
			public bool EnableDedicatedVNOC { get; set; }
			public int MCUEnchancedLimit { get; set; }
            public bool EnableImmConf { get; set; }
            public bool enableAdditionalOption { get; set; }
		}

		public Preferences Preference { get; private set; }
		public OrganizationId OrganizationId { get; private set; }

		internal void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
		{
			if (Preference == null)
				Preference = new Preferences();

			do
			{
				reader.Read();
				switch (reader.LocalName)
				{
					case "EnableFacilites":
						{
							Preference.IsFacilitiesEnabled = reader.ReadElementValue<bool>();
							break;
						}
					case "EnableCatering":
						{
							Preference.IsCateringEnabled = reader.ReadElementValue<bool>();
							break;
						}
					case "EnableHouseKeeping":
						{
							Preference.IsHouseKeepingEnabled = reader.ReadElementValue<bool>();
							break;
						}
					case "EnableBlueJeans":
						{
							Preference.IsBlueJeansEnabled = reader.ReadElementValue<bool>();
							break;
						}
					case "EnableJabber":
						{
							Preference.IsJabberEnabled = reader.ReadElementValue<bool>();
							break;
						}
					case "EnableLync":
						{
							Preference.IsLyncEnabled = reader.ReadElementValue<bool>();
							break;
						}
					case "EnableVidtel":
						{
							Preference.IsVidtelEnabled = reader.ReadElementValue<bool>();
							break;
						}
					case "EnableCloud":
						{
							Preference.IsCloudEnabled = reader.ReadElementValue<bool>();
							break;
						}
					case "EnableProfileSelection":
						{
							Preference.IsProfileSelectionEnabled = reader.ReadElementValue<bool>();
							break;
						}
					case "SecureSwitch":
						{
							Preference.IsSecureSwitchEnabled = reader.ReadElementValue<bool>();
							break;
						}
					 case "EnableOnsiteAV":
						{
							Preference.EnableOnsiteAV = reader.ReadElementValue<bool>();
							break;
						}
					case "EnableMeetandGreet":
						{
							Preference.EnableMeetandGreet = reader.ReadElementValue<bool>();
							break;
						}
					case "EnableConciergeMonitoring":
						{
							Preference.EnableConciergeMonitoring = reader.ReadElementValue<bool>();
							break;
						} 
					case "EnableDedicatedVNOC":
						{
							Preference.EnableDedicatedVNOC = reader.ReadElementValue<bool>();
							break;
						} 
					case "organizationID":
						OrganizationId = new OrganizationId();
						OrganizationId.LoadFromXml(reader, reader.LocalName);
						break;
					case "MCUEnchancedLimit":
						{
							Preference.MCUEnchancedLimit = reader.ReadElementValue<int>();
							break;
						}
                    case "EnablePublicConference":
                        {
                            Preference.EnablePublicConferencing = reader.ReadElementValue<bool>();
                            break;
                        }
                    case "NetworkSwitching":
                        {
                            Preference.NetworkSwitching = reader.ReadElementValue<int>();
                            break;
                        }
                    case "EnableImmConf":
                        {
                            Preference.EnableImmConf = reader.ReadElementValue<bool>();
                            break;
                        }
                    case "enableAdditionalOption":
                        {
                            Preference.enableAdditionalOption = reader.ReadElementValue<bool>();
                            break;
                        }
					default:
						reader.SkipCurrentElement();
						break;
				}
			} while (!reader.IsEndElement(XmlNamespace.NotSpecified, xmlElementName));
		}
	}
}



