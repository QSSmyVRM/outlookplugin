﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	[Serializable] //??
	public class RoomFlat
	{
		public RoomId Id;
		public string Name { get; set; }
		public string ZipCode { get; set; }
		public string City { get; set; }
		public int Capacity { get; set; }
		public string pubISDNIP { get; set; }
		public string pubSDHD { get; set; }
		public string pubBusinessHrsPrice { get; set; }
		public string pubCurrency { get; set; }
		public System.Drawing.Image Picture { get; set; } //only for public
		public string State { get; set; }
		public string Country { get; set; }
		public string Phone { get; set; }
		public string pubAutomated { get; set; }
		public bool Approval { get; set; }
		public string Site { get; set; }
		public string Type { get; set; }
		public string Address { get; set; }
		public string Assistant { get; set; }
		public string Floor { get; set; }
		public bool IsPublic { get; set; }
		public bool IsFavorite { get; set; }
		public override string ToString()
		{
			return Name;
		}
	}
}
