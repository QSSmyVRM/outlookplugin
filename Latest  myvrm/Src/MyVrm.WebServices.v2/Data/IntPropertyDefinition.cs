﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    internal class IntPropertyDefinition : TypedPropertyDefinition
    {
        internal IntPropertyDefinition(string xmlElementName) : base(xmlElementName)
        {
            PropertyType = typeof(int);
        }

        internal IntPropertyDefinition(string xmlElementName, PropertyDefinitionFlags flags)
            : base(xmlElementName, flags)
        {
            PropertyType = typeof(int);
        }

        internal IntPropertyDefinition(string xmlElementName, PropertyDefinitionFlags flags, bool isNullable)
            : base(xmlElementName, flags, isNullable)
        {
            PropertyType = typeof(int);
        }

        internal IntPropertyDefinition(string xmlElementName, string xmlElementForWrite, PropertyDefinitionFlags flags)
            : base(xmlElementName, xmlElementForWrite, flags)
        {
            PropertyType = typeof(int);
        }

        #region Overrides of TypedPropertyDefinition

        internal override object Parse(string value)
        {
            if (string.IsNullOrEmpty(value) && IsNullable)
                return null;
            return int.Parse(value);
        }

        #endregion
    }
}
