﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    public class BridgeSchema : ServiceObjectSchema
    {
        public static readonly PropertyDefinition Id;
        public static readonly PropertyDefinition Name;
        public static readonly PropertyDefinition Login;
        public static readonly PropertyDefinition Password;
        public static readonly PropertyDefinition TimeZone;
        public static readonly PropertyDefinition MaxAudioCalls;
        public static readonly PropertyDefinition MaxVideoCalls;
        public static readonly PropertyDefinition Vendor;
        public static readonly PropertyDefinition Status;
        public static readonly PropertyDefinition IsVirtual;
        public static readonly PropertyDefinition Administrator;
        public static readonly PropertyDefinition FirmwareVersion;
        public static readonly PropertyDefinition PercentOfReservedPorts;
        public static readonly PropertyDefinition Approvers;
        public static readonly PropertyDefinition Details;
        public static readonly PropertyDefinition IsdnThresholdAlert;
        public static readonly PropertyDefinition MalfunctionAlert;
        internal static readonly BridgeSchema Instance;

        static BridgeSchema()
        {
            Id = new ComplexPropertyDefinition<BridgeId>("bridgeID", () => new BridgeId());
            Name = new StringPropertyDefinition("name", PropertyDefinitionFlags.CanSet);
            Login = new StringPropertyDefinition("login", PropertyDefinitionFlags.CanSet);
            Password = new StringPropertyDefinition("password", PropertyDefinitionFlags.CanSet);
            TimeZone = new TimeZonePropertyDefinition("timeZone", PropertyDefinitionFlags.CanSet);
            MaxAudioCalls = new IntPropertyDefinition("maxAudioCalls", PropertyDefinitionFlags.CanSet);
            MaxVideoCalls = new IntPropertyDefinition("maxVideoCalls", PropertyDefinitionFlags.CanSet);
            Vendor = new IntPropertyDefinition("bridgeType", PropertyDefinitionFlags.CanSet);
            Status = new GenericPropertyDefinition<BridgeStatus>("bridgeStatus", PropertyDefinitionFlags.CanSet);
            IsVirtual = new BoolPropertyDefinition("virtualBridge", PropertyDefinitionFlags.CanSet);
            Administrator = new ComplexPropertyDefinition<BridgeAdministrator>("bridgeAdmin",
                                                                               PropertyDefinitionFlags.
                                                                                   AutoInstantiateOnRead |
                                                                               PropertyDefinitionFlags.CanSet,
                                                                               () => new BridgeAdministrator());
            FirmwareVersion = new StringPropertyDefinition("firmwareVersion", PropertyDefinitionFlags.CanSet);
            PercentOfReservedPorts = new IntPropertyDefinition("percentReservedPort", PropertyDefinitionFlags.CanSet);
            Approvers = new ComplexPropertyDefinition<ApproverIdCollection>("approvers",
                                                                            PropertyDefinitionFlags.
                                                                                AutoInstantiateOnRead |
                                                                            PropertyDefinitionFlags.CanSet,
                                                                            () => new ApproverIdCollection());
            Details = new ComplexPropertyDefinition<BridgeDetails>("bridgeDetails",
                                                                   PropertyDefinitionFlags.AutoInstantiateOnRead |
                                                                   PropertyDefinitionFlags.CanSet,
                                                                   () => new BridgeDetails());
            IsdnThresholdAlert = new BoolPropertyDefinition("ISDNThresholdAlert", PropertyDefinitionFlags.CanSet);
            MalfunctionAlert = new BoolPropertyDefinition("malfunctionAlert", PropertyDefinitionFlags.CanSet);

            Instance = new BridgeSchema();
        }

        internal BridgeSchema()
        {
        }

        internal override void RegisterProperties()
        {
            base.RegisterProperties();
            RegisterProperty(Id);
            RegisterProperty(Name);
            RegisterProperty(Login);
            RegisterProperty(Password);
            RegisterProperty(TimeZone);
            RegisterProperty(MaxAudioCalls);
            RegisterProperty(MaxVideoCalls);
            RegisterProperty(Vendor);
            RegisterProperty(Status);
            RegisterProperty(IsVirtual);
            RegisterProperty(Administrator);
            RegisterProperty(FirmwareVersion);
            RegisterProperty(PercentOfReservedPorts);
            RegisterProperty(Approvers);
            RegisterProperty(Details);
            RegisterProperty(IsdnThresholdAlert);
            RegisterProperty(MalfunctionAlert);
        }
    }
}
