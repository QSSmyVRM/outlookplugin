﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using MyVrm.Common;

namespace MyVrm.WebServices.Data
{
    public enum ConferenceEndpointType
    {
        Room,
        User
    }

    public enum ConferenceEndpointCallMode
    {
        None = -1,
        [LocalizedDescription("ConferenceEndpointCallModeCallee", typeof (Strings))] Callee = 0,
        [LocalizedDescription("ConferenceEndpointCallModeCaller", typeof (Strings))] Caller = 1
    }

	public enum BridgeProfileSelectedNone
	{
		[LocalizedDescription("WinForms.Strings.TxtNone", typeof(Strings))]
		None = -1,
		[LocalizedDescription("WinForms.Strings.TxtNoItems", typeof(Strings))]
		NoItems = 0
	}

    public class ConferenceEndpoint : ComplexProperty
    {
		public override object Clone()
		{
			ConferenceEndpoint endpoint = new ConferenceEndpoint();

			endpoint.Address = Address;
			endpoint.AddressType = AddressType;
			endpoint.ApiPortNumber = ApiPortNumber;
			endpoint.Bandwidth = Bandwidth;
			endpoint.BridgeId = BridgeId;
			endpoint.BridgeProfileID = BridgeProfileID;
			endpoint.Caller = Caller;
			endpoint.ConferenceCode = ConferenceCode;
			endpoint.Connection = Connection;
			endpoint.ConnectionType = ConnectionType;
			endpoint.DefaultProtocol = DefaultProtocol;
			endpoint.EndpointId = EndpointId;
			endpoint.Id = Id;
			endpoint.IsLecturer = IsLecturer;
			endpoint.IsOutside = IsOutside;
			endpoint.LeaderPin = LeaderPin;
			endpoint.ProfileId = ProfileId;
			endpoint.Type = Type;
			endpoint.Url = Url;
			endpoint.UseDefault = UseDefault;
			endpoint.VideoEquipment = VideoEquipment;
            endpoint.EndPointName = EndPointName; //101343
			return endpoint;
		}

    	public override bool Equals(object obj)
    	{
			if (ReferenceEquals(this, obj))
				return true;

    		ConferenceEndpoint endpoint = obj as ConferenceEndpoint;
			if (endpoint == null)
				return false;
			if (endpoint.Address != Address)
				return false;
			if (endpoint.AddressType != AddressType)
				return false;
			if (endpoint.ApiPortNumber != ApiPortNumber)
				return false;
			if (endpoint.Bandwidth != Bandwidth)
				return false;
			if (endpoint.BridgeId != BridgeId)
				return false;
			if (endpoint.BridgeProfileID != BridgeProfileID)
				return false;
			if (endpoint.Caller != Caller)
				return false;
			if (endpoint.ConferenceCode != ConferenceCode)
				return false;
			if (endpoint.Connection != Connection)
				return false;
			if (endpoint.ConnectionType != ConnectionType)
				return false;
			if (endpoint.DefaultProtocol != DefaultProtocol)
				return false;
			if (endpoint.EndpointId != EndpointId)
				return false;
			if (endpoint.Id != Id)
				return false;
			if (endpoint.IsLecturer != IsLecturer)
				return false;
			if (endpoint.IsOutside != IsOutside)
				return false;
			if (endpoint.LeaderPin != LeaderPin)
				return false;
			if (endpoint.ProfileId != ProfileId)
				return false;
			if (endpoint.Type != Type)
				return false;
			if (endpoint.Url != Url)
				return false;
			if (endpoint.UseDefault != UseDefault)
				return false;
			if (endpoint.VideoEquipment != VideoEquipment)
				return false;
			
			return true;
		}
        private EndpointAddress _address = new EndpointAddress();

        public ConferenceEndpointType Type { get; set; }
        public ServiceId Id { get; set; }
        public bool UseDefault { get; set; }
        public bool IsLecturer { get; set; }
        public EndpointId EndpointId { get; set; }
        public EndpointProfileId ProfileId { get; set; }
        public BridgeId BridgeId { get; set; }
		public MCUProfileId BridgeProfileID { get; set; }
        public AddressType? AddressType { get; set; }
        public String EndPointName { get; set; }//101343

        public string Address
        {
            get { return _address.Address; }
            set { _address.Address = value; }
        }

    	public string ExternalUserEmail = string.Empty;
        public int? VideoEquipment { get; set; }
        public int? ConnectionType { get; set; }
        public int? Bandwidth { get; set; }
        public bool? IsOutside { get; set; }
        public int? DefaultProtocol { get; set; }
        public MediaType Connection { get; set; }
        public string Url { get; set; }
        public string ConfGRoomID { get; set; }//101343
        public ConferenceEndpointCallMode Caller { get; set; }

        public string ConferenceCode
        {
            get { return _address.ConferenceCode; }
            set { _address.ConferenceCode = value; }
        }

        public string LeaderPin
        {
            get { return _address.LeaderPin; }
            set { _address.LeaderPin = value; }
        }

        public int ApiPortNumber { get; set; }

        internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
        {
            switch (reader.LocalName)
            {
                case "Type":
                {
                    string typeStr = reader.ReadElementValue();
                    switch (typeStr)
                    {
                        case "R":
                            Type = ConferenceEndpointType.Room;
                            break;
                        case "U":
                            Type = ConferenceEndpointType.User;
                            break;
                    }
                    return true;
                }
                case "ID":
                {
                    switch (Type)
                    {
                        case ConferenceEndpointType.Room:
                            Id = new RoomId();
                            Id.LoadFromXml(reader, "ID");
                            break;
                        case ConferenceEndpointType.User:
                            Id = new UserId();
                            Id.LoadFromXml(reader, "ID");
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                    return true;
                }
                case "UseDefault":
                {
                    UseDefault = Utilities.BoolStringToBool(reader.ReadElementValue());
                    return true;
                }
                case "IsLecturer":
                {
                    IsLecturer = Utilities.BoolStringToBool(reader.ReadElementValue());
                    return true;
                }
                case "EndpointID":
                {
                    var value = reader.ReadElementValue();
                    if (string.IsNullOrEmpty(value))
                    {
                        EndpointId = null;
                        return true;
                    }
                    EndpointId = new EndpointId(value);
                    return true;
                }
                case "ProfileID":
                {
                    var value = reader.ReadElementValue();
                    if (string.IsNullOrEmpty(value))
                    {
                        ProfileId = null;
                        return true;
                    }
                    ProfileId = new EndpointProfileId(value);
                    return true;
                }
                case "BridgeID":
                {
                    var value = reader.ReadElementValue();
                    if (string.IsNullOrEmpty(value))
                    {
                        BridgeId = null;
                        return true;
                    }
                    BridgeId = new BridgeId(value);
                    return true;
                }
				case "BridgeProfileID":
        		{
        			BridgeProfileID = new MCUProfileId(reader.ReadElementValue());
        			return true;
        		}
            	case "AddressType":
                {
                    AddressType = reader.ReadElementValue<AddressType>();
                    return true;
                }
                case "Address":
                {
                    _address = EndpointAddress.Parse(reader.ReadElementValue());
                    return true;
                }
                case "VideoEquipment":
                {
                    VideoEquipment = reader.ReadElementValue<int>();
                    return true;
                }
                case "connectionType":
                {
                    ConnectionType = reader.ReadElementValue<int>();
                    return true;
                }
                case "Bandwidth":
                {
                    Bandwidth = reader.ReadElementValue<int>();
                    return true;
                }
                case "IsOutside":
                {
                    IsOutside = reader.ReadElementValue<bool>();
                    return true;
                }
                case "DefaultProtocol":
                {
                    DefaultProtocol = reader.ReadElementValue<int>();
                    return true;
                }
                case "Connection":
                {
                    Connection = reader.ReadElementValue<MediaType>();
                    return true;
                }
                case "URL":
                {
                    Url = reader.ReadElementValue();
                    return true;
                }
                case "ApiPortno":
                {
                    ApiPortNumber = reader.ReadElementValue<int>();
                    return true;
                }
                case "Connect2":
                {
                    Caller = reader.ReadElementValue<ConferenceEndpointCallMode>();
                    return true;
                }
//101343 starts
                case "EndPointName":
                {
                    EndPointName = reader.ReadElementValue();
                    return true;
                }
//101343 Ends
            }
            return false;
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            string typeStr;
            switch (Type)
            {
                case ConferenceEndpointType.Room:
                    typeStr = "R";
                    break;
                case ConferenceEndpointType.User:
                    typeStr = "U";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            writer.WriteElementValue(XmlNamespace.NotSpecified, "Type", typeStr);
            if (ConfGRoomID != null)//101343
                writer.WriteElementValue(XmlNamespace.NotSpecified, "ID", ConfGRoomID);
            else if (Id != null)
                Id.WriteToXml(writer, "ID");
            else
                writer.WriteElementValue(XmlNamespace.NotSpecified, "ID", null);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "UseDefault", UseDefault);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "IsLecturer", IsLecturer);
            if (EndpointId != null)
            {
                EndpointId.WriteToXml(writer);
            }
            else
            {
                writer.WriteElementValue(XmlNamespace.NotSpecified, "EndpointID", null);
            }
            if (ProfileId != null)
            {
                ProfileId.WriteToXml(writer);
            }
            else
            {
                writer.WriteElementValue(XmlNamespace.NotSpecified, "ProfileID", null);
            }
            if (BridgeId != null)
            {
                BridgeId.WriteToXml(writer, "BridgeID");
            }
            else
            {
                writer.WriteElementValue(XmlNamespace.NotSpecified, "BridgeID", null);
            }
			writer.WriteElementValue(XmlNamespace.NotSpecified, "BridgeProfileID", BridgeProfileID != null ? BridgeProfileID.Id : null );
            writer.WriteElementValue(XmlNamespace.NotSpecified, "AddressType", AddressType);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "Address", _address.ToString());
            writer.WriteElementValue(XmlNamespace.NotSpecified, "VideoEquipment", VideoEquipment);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "connectionType", ConnectionType);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "Bandwidth", Bandwidth);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "IsOutside", IsOutside);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "DefaultProtocol", DefaultProtocol);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "Connection", Connection);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "URL", Url);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "Connect2", Caller);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "ExchangeID", null);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "APIPortNo", ApiPortNumber);
        }

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
    }
}
