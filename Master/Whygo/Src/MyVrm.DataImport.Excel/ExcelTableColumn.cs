﻿using System;
using Microsoft.Office.Interop.Excel;

namespace MyVrm.DataImport.Excel
{
    public class ExcelTableColumn
    {
        public ExcelTableColumn(ListColumn listColumn) : this(listColumn, typeof(string))
        {
        }

        public ExcelTableColumn(ListColumn listColumn, Type type)
        {
            Index = listColumn.Index;
            Name = listColumn.Name;
            Type = type;
        }

        public int Index { get; private set; }
        public string Name { get; private set; }
        public Type Type { get; private set; }
        public bool IsReadOnly { get; set; }
        public bool IsRequired { get; set; }
        public bool HasFixedSetOfValues { get; set; }
        public string ValuesRangeName { get; set; }
    }
}
