﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.WebServices.Data
{
    public class ManagedUser
    {
        internal ManagedUser()
        {
        }

        public UserId Id { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string Login { get; private set; }
        public string Email { get; private set; }
        public string Telephone { get; private set; }
        public string Password { get; private set; }

        internal void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
        {
            do
            {
                reader.Read();
                if (reader.LocalName == "userID")
                {
                    Id = new UserId(reader.ReadValue());
                }
                else if (reader.LocalName == "firstName")
                {
                    FirstName = reader.ReadValue();
                }
                else if (reader.LocalName == "lastName")
                {
                    LastName = reader.ReadValue();
                }
                else if (reader.LocalName == "login")
                {
                    Login = reader.ReadValue();
                }
                else if (reader.LocalName == "email")
                {
                    Email = reader.ReadValue();
                }
                else if (reader.LocalName == "password")
                {
                    Password = reader.ReadValue();
                }
                else
                {
                    reader.SkipCurrentElement();
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, xmlElementName));
        }
    }
}
