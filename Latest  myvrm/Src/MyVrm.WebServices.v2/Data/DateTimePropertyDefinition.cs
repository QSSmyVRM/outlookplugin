﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;

namespace MyVrm.WebServices.Data
{
    public class DateTimePropertyDefinition : PropertyDefinition
    {
        internal DateTimePropertyDefinition(string xmlElementName) : base(xmlElementName)
        {
            PropertyType = typeof(DateTime);
        }

        internal DateTimePropertyDefinition(string xmlElementName, PropertyDefinitionFlags flags)
            : base(xmlElementName, flags)
        {
            PropertyType = typeof(DateTime);
        }

        #region Overrides of PropertyDefinition

        internal override void LoadPropertyValueFromXml(MyVrmServiceXmlReader reader, PropertyBag propertyBag)
        {
            throw new NotImplementedException();
        }

        internal override void WritePropertyValueToXml(MyVrmXmlWriter writer, PropertyBag propertyBag)
        {
            //throw new NotImplementedException();
        }

        #endregion
    }
}
