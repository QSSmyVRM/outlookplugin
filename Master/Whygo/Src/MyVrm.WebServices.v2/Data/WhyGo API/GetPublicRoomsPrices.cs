﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	//enum CurrencyType
	//{
	//    AUD,
	//    USD
	//}

	//Request
	internal class GetPublicRoomsPricesRequest : ServiceRequestBase<GetPublicRoomsPricesResponse>
	{
		private RoomIdCollection _roomList;
		private int _isRecurring;
		private ConferenceId _confID;
		private bool _immediate;
		private CurrencyAbbr.CurrencyType _currencyType;
		public RoomIdCollection RoomList
		{
			set { _roomList = value; }
			get { return _roomList; }
		}
		public int IsRecurring
		{
			set { _isRecurring = value; }
			get { return _isRecurring; }
		}

		public ConferenceId ConfID
		{
			set { _confID = value; }
			get { return _confID; }
		}

		public bool IsImmediate
		{
			set { _immediate = value; }
			get { return _immediate; }
		}

		public CurrencyAbbr.CurrencyType CurrencyType
		{
			set { _currencyType = value; }
			get { return _currencyType; }
		}
		
		internal GetPublicRoomsPricesRequest(MyVrmService service)
			: base(service)
		{
		}

		#region Overrides of ServiceRequestBase<GetPublicRoomsPricesResponse>

		internal override string GetXmlElementName()
		{
			return Constants.GetPublicRoomsPricesCommandName;
		}

		internal override string GetCommandName()
		{
			return Constants.GetPublicRoomsPricesCommandName;
		}

		internal override string GetResponseXmlElementName()
		{
			return "GetPublicRoomsPrices";
		}

		internal override GetPublicRoomsPricesResponse ParseResponse(MyVrmServiceXmlReader reader)
		{
			var response = new GetPublicRoomsPricesResponse();
			response.ReadElementsFromXml(reader);
			return response;
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			if (UserId != null)
				UserId.WriteToXml(writer, "userID");
			ConfID.WriteToXml(writer, "confID");
			writer.WriteElementValue(XmlNamespace.NotSpecified, "immediate", IsImmediate ? 1 : 0);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "recurring", IsRecurring);

			string rooms = string.Empty;
			foreach (RoomId room in RoomList)
			{
				if (rooms.Length > 0)
					rooms += ",";
				rooms += room.Id;
			}
			writer.WriteElementValue(XmlNamespace.NotSpecified, "selectedLocationID", rooms);
			writer.WriteElementValue(XmlNamespace.NotSpecified, "CurrencyType", CurrencyAbbr.GetCurrencyAbbr( CurrencyType));
		}

		#endregion
	}

	//Response
	public class GetPublicRoomsPricesResponse : ServiceResponse
	{
		//Result 
		private readonly List<RoomPrice> _roomPriceList = new List<RoomPrice>();
		public List<RoomPrice> RoomPriceList
		{
			get { return _roomPriceList; }
		}

		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{
			do
			{
				//Forward till "Location" element 
				while (!reader.IsEndElement(XmlNamespace.NotSpecified, "Location"))
				{
					while (!reader.IsStartElement(XmlNamespace.NotSpecified, "Location") //new entry or
						   && !reader.IsEndElement(XmlNamespace.NotSpecified, "Locationlist")) //empty list
						reader.Read();
					if (reader.IsEndElement(XmlNamespace.NotSpecified, "Locationlist"))
						break;
					if (reader.LocalName == "Location")
					{
						RoomPrice roomPrice = new RoomPrice();

						while (!reader.IsEndElement(XmlNamespace.NotSpecified, "Location"))
						{
							if (roomPrice != null)
								roomPrice.TryReadElementFromXml(reader);
						}
						if (roomPrice != null)
							_roomPriceList.Add(roomPrice);
					}
				}
				if (!reader.IsEndElement(XmlNamespace.NotSpecified, "Locationlist"))
					reader.Read();

			} while (!reader.IsEndElement(XmlNamespace.NotSpecified, "Locationlist")); //read till the "Locationlist" node end 
		}
	}
}
