﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;

namespace MyVrm.WebServices.Data
{
    internal class DeleteConferenceRequest : ServiceRequestBase<DeleteConferenceResponse>
    {
        internal DeleteConferenceRequest(MyVrmService service) : base(service)
        {
        }

        internal ConferenceId ConferenceId { get; set; }

        #region Overrides of ServiceRequestBase

        internal override string GetXmlElementName()
        {
            return "login";
        }

        internal override string GetCommandName()
        {
            return "DeleteConference";
        }

        internal override string GetResponseXmlElementName()
        {
            return "conferences";
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            UserId.WriteToXml(writer, "userID");
            writer.WriteStartElement(XmlNamespace.NotSpecified, "delconference");
			writer.WriteStartElement(XmlNamespace.NotSpecified, "conference");
            ConferenceId.WriteToXml(writer, "confID");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "reason", null);
            writer.WriteEndElement();
        }

        #endregion
    }
}
