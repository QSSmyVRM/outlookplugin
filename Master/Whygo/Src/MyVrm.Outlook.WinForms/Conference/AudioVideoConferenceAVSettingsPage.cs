﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using MyVrm.Common.ComponentModel;
using MyVrm.Common.EventBroker;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook.WinForms.Conference
{
    public partial class AudioVideoConferenceAVSettingsPage : ConferencePage
    {
		public enum AVPageType
    	{
    		AudioVideo,
    		PointToPoint,
			VMR
    	} ;

    	public AVPageType PageType { get; set; }

    	//private ConferenceAdvancedAudioVideoSettings _conferenceAdvancedAudioVideoSettings;
        private readonly DataList<RoomEndpoint> _roomEndpoints = new DataList<RoomEndpoint>();

        public AudioVideoConferenceAVSettingsPage()
        {
            InitializeComponent();
			roomAudioVideoSettingsListControl.BridgeNameColumnVisibility = true;
			roomAudioVideoSettingsListControl.useDefaultColumnVisibility = true;//false;
            Text = Strings.AudioVideoConferenceAVSettingsPageText;

        	commonAVSettingsGroup.CustomizationFormText = Strings.CommonSettingsLableText;
			commonAVSettingsGroup.Text = Strings.CommonSettingsLableText;
			layoutControlItem22.CustomizationFormText = Strings.MaximumLineRateLableText;
			layoutControlItem22.Text = Strings.MaximumLineRateLableText;
			layoutControlItem19.CustomizationFormText = Strings.VideoCodecsLableText;
			layoutControlItem19.Text = Strings.VideoCodecsLableText;
			layoutControlItem17.CustomizationFormText = Strings.RestrictNetworkAccessToLableText;
			layoutControlItem17.Text = Strings.RestrictNetworkAccessToLableText;
			layoutControlItem23.Text = Strings.RestrictUsageToLableText;
			layoutControlItem23.CustomizationFormText = Strings.RestrictUsageToLableText;
        	layoutControlItem25.Text = Strings.AudioCodecsLableText;
        	layoutControlItem27.Text = Strings.VideoDisplayLableText;
			layoutControlItem27.CustomizationFormText = Strings.VideoDisplayLableText;
        	layoutControlItem11.Text = Strings.MaximumVideoPortsLableText;
			layoutControlItem11.CustomizationFormText = Strings.MaximumVideoPortsLableText;
        	layoutControlItem12.Text = Strings.MaximumAudioPortsLableText;
			layoutControlItem12.CustomizationFormText = Strings.MaximumAudioPortsLableText;
			polycomSettingsGroup.Text = Strings.PolycomSpecificSettingsLableText;
			polycomSettingsGroup.CustomizationFormText = Strings.PolycomSpecificSettingsLableText;
			layoutControlItem28.Text = Strings.VideoModeLableText;
			layoutControlGroup4.Text = Strings.RoomsLableText;
			layoutControlGroup4.CustomizationFormText = Strings.RoomsLableText;
			layoutControlGroup5.Text = Strings.UsersLableText;
			layoutControlGroup5.CustomizationFormText = Strings.UsersLableText;
			conferenceOnPortEdit.Properties.Caption = Strings.ConferenceOnPortCaptionText;
			lectureModeEdit.Properties.Caption = Strings.LectureModeCaptionText;
			singleDialinEdit.Properties.Caption = Strings.SingleDialInNumberCaptionText;
			encryptedEdit.Properties.Caption = Strings.EncryptionCaptionText;
			dualStreamModeEdit.Properties.Caption = Strings.DualStreamModeCaptionText;

            roomAudioVideoSettingsListControl.DataSource = _roomEndpoints;

        	roomAudioVideoSettingsListControl.useDefaultColumnVisibility = true; // false;
        	SetPageType(AVPageType.AudioVideo);
        }

		public void SetPageType( AVPageType pageType)
		{
			PageType = pageType;
			if (PageType == AVPageType.PointToPoint)
			{
				roomAudioVideoSettingsListControl.CallerColumnVisibility = true;
				roomAudioVideoSettingsListControl.BridgeNameColumnVisibility = false;
			}
			else
			{
				roomAudioVideoSettingsListControl.CallerColumnVisibility = false;
				roomAudioVideoSettingsListControl.BridgeNameColumnVisibility = true;
			}
		}

		EventBroker _broker ;
		public void SetEventBroker(EventBroker broker)
		{
			_broker = broker;
			roomAudioVideoSettingsListControl.SetEventBroker(_broker);
		}

    	internal ReadOnlyCollection<RoomEndpoint> Endpoints
        {
            get
            {
                return new ReadOnlyCollection<RoomEndpoint>(_roomEndpoints);
            }
        }

        protected override void OnReadOnlyChanged(ReadOnlyChangedEventArgs e)
        {
            roomAudioVideoSettingsListControl.Enabled = !e.ReadOnly;
        }

        private void ConferenceBindingSource_DataSourceChanged(object sender, EventArgs e)
        {
            ResetData();
        }

        private void ResetData()
        {
            Conference.LocationIds.ListChanged += LocationIds_ListChanged;
            ResetRoomEndpoints();
        }

        private void AudioVideoConferenceAVSettingsPage_Load(object sender, EventArgs e)
        {
            ConferenceBindingSource.DataSourceChanged += ConferenceBindingSource_DataSourceChanged;
            ResetData();
        }

        private void ResetRoomEndpoints()
        {
            var query = from roomId in Conference.LocationIds
                        join endpoint in Conference.RoomEndpoints on roomId equals endpoint.Id
                            into conferenceEndpoints
                        from conferenceEndpoint in conferenceEndpoints.DefaultIfEmpty()
                        select NewRoomEndpoint(roomId);
            _roomEndpoints.CopyFrom(query);

            if (PageType == AVPageType.PointToPoint)
            {
                if ((_roomEndpoints.Count >= 2 && _roomEndpoints[0].Caller != ConferenceEndpointCallMode.Caller &&
                    _roomEndpoints[1].Caller != ConferenceEndpointCallMode.Caller) || (_roomEndpoints.Count == 1))
                    _roomEndpoints[0].Caller = ConferenceEndpointCallMode.Caller;
            }
        }

        void LocationIds_ListChanged(object sender, ListChangedEventArgs e)
        {
            switch (e.ListChangedType)
            {
                case ListChangedType.Reset:
                    {
                        ResetRoomEndpoints();
                        break;
                    }
                case ListChangedType.ItemAdded:
                    {
                        var roomId = Conference.LocationIds[e.NewIndex];
                        AddRoomEndpoint(e.NewIndex, roomId);
                        break;
                    }
                case ListChangedType.ItemDeleted:
                    {
                        if (e.NewIndex >= 0 && _roomEndpoints.Count > 0)
                        {
                            var roomEndpoint = _roomEndpoints[e.NewIndex];
                            Conference.RoomEndpoints.Remove(roomEndpoint.ConferenceEndpoint);
                            _roomEndpoints.RemoveAt(e.NewIndex);
                        }
                        break;
                    }
                case ListChangedType.ItemMoved:
                {
                    var roomEndpoint = _roomEndpoints[e.OldIndex];
                    _roomEndpoints.Move(roomEndpoint, e.NewIndex);
                    break;
                }
                case ListChangedType.ItemChanged:
                    break;
            }

			if (PageType == AVPageType.PointToPoint)
			{
				if (_roomEndpoints.Count >= 2 && _roomEndpoints[0].Caller != ConferenceEndpointCallMode.Caller &&
				    _roomEndpoints[1].Caller != ConferenceEndpointCallMode.Caller || _roomEndpoints.Count == 1)
					_roomEndpoints[0].Caller = ConferenceEndpointCallMode.Caller;
			}
        }

        private void AddRoomEndpoint(int index, RoomId roomId)
        {
            if (roomId == null) return;

			RoomEndpoint roomEndpoint = NewRoomEndpoint(roomId);
            _roomEndpoints.Insert(index, roomEndpoint);
        }

		private RoomEndpoint NewRoomEndpoint(RoomId roomId)
		{
			Room roomDetails = MyVrmAddin.Instance.GetRoomFromCache(roomId);
			Endpoint endpoint = null;
			var conferenceEndpoint = Conference.RoomEndpoints.FirstOrDefault(endp => endp.Id == roomDetails.Id);
			if (conferenceEndpoint == null)
			{
				conferenceEndpoint = new ConferenceEndpoint
				{
					Id = roomId,
					UseDefault = false,
					EndpointId = roomDetails.EndpointId,
					Connection = MediaType.AudioVideo
				};
				if ((roomDetails.Media == MediaType.AudioOnly || roomDetails.Media == MediaType.AudioVideo)
					&& roomDetails.EndpointId != null)
				{
					Conference.RoomEndpoints.Add(conferenceEndpoint);
				}
			}
			if (roomDetails.EndpointId != null)
			{
				endpoint = MyVrmService.Service.GetEndpoint(roomDetails.EndpointId);
			}
			return new RoomEndpoint(roomDetails, conferenceEndpoint, endpoint);
		}

		protected override void OnApplying(CancelEventArgs e)
		{
			base.OnApplying(e);
			if (e.Cancel || Conference == null)
				return;

			roomAudioVideoSettingsListControl.CloseEditor();

			//If room endpoints where not read (i.e. page was not loaded) - get them
			//if (Conference.Conference.Rooms.Count > 0 && Conference.Conference.Rooms.Count >_roomEndpoints.Count )
			{
				ResetRoomEndpoints();
			}

			//Check integrity: in case of page was not loaded and a room was removed or replaced with another room
			if (Conference.RoomEndpoints.Count != _roomEndpoints.Count)
			{
				List<ConferenceEndpoint> RoomEndpointsCopy =
					_roomEndpoints.Where(
						roomEndpoint => Conference.RoomEndpoints.FirstOrDefault(roomEndP => roomEndP.Id.Id == roomEndpoint.Id.Id) != null)
						.Select(roomEndpoint => roomEndpoint.ConferenceEndpoint).ToList();
				Conference.RoomEndpoints.Clear();
				Conference.RoomEndpoints.AddRange(RoomEndpointsCopy); 
			}
		}
    }
}
