﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System.Collections.Generic;
namespace MyVrm.WebServices.Data
{
    public class InventorySetItem : ComplexProperty
    {
        public int Id { get; internal set; }
        public string Name { get; set; }
        public Image Image { get; set; }
        public uint Quantity { get; set; }
        public decimal Price { get; set; }
        public string SerialNumber { get; set; }
        public bool Portable { get; set; }
        public string Comments { get; set; }
        public string Description { get; set; }
        public decimal ServiceCharge { get; set; }
        public int DeliveryType { get; set; }
        public decimal DeliveryCost { get; set; }
        public bool Deleted { get; set; }
        //public object Charges { get; private set; }
        public RoomIdCollection Rooms { get; private set; }
        public List<InventoryCharges> InventoryCharges {get;set;}
		public override object Clone()
		{
			InventorySetItem copy = new InventorySetItem();
			copy.Image = new Image(Image.Name, Image.Data);
			copy.Name = string.Copy(Name);
			copy.SerialNumber = string.Copy(SerialNumber);
			copy.Comments = string.Copy(Comments);
			copy.Description = string.Copy(Description);
			copy.Rooms = (RoomIdCollection)Rooms.Clone();
			copy.Id = Id;
			copy.Deleted = Deleted;
			copy.DeliveryCost = DeliveryCost;
			copy.DeliveryType = DeliveryType;
			copy.Portable = Portable;
			copy.Price = Price;
			copy.Quantity = Quantity;
			copy.ServiceCharge = ServiceCharge;
            copy.InventoryCharges = new List<InventoryCharges>();
            foreach (InventoryCharges charge in InventoryCharges)
                copy.InventoryCharges.Add(charge);

			return copy;
		}

        internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
        {
            switch (reader.LocalName)
            {
                case "ID":
                    {
                        Id = reader.ReadElementValue<int>();
                        return true;
                    }
                case "Name":
                    {
                        Name = reader.ReadElementValue();
                        return true;
                    }
                case "ImageName":
                    {
                        if (Image == null)
                        {
                            Image = new Image();
                        }
                        Image.Name = reader.ReadElementValue();
                        return true;
                    }
                case "Image":
                    {
                        if (Image == null)
                        {
                            Image = new Image();
                        }
                        Image.Data = reader.ReadBase64ElementValue();
                        return true;
                    }
                case "Quantity":
                    {
                        Quantity = reader.ReadElementValue<uint>();
                        return true;
                    }
                case "Charges":
                    {
                        InventoryCharges = ReadChargesFromXml(reader, "Charges");
                    }
                    return true;
                case "Price":
                    {
                        Price = reader.ReadElementValue<decimal>();
                        return true;
                    }
                case "SerialNumber":
                    {
                        SerialNumber = reader.ReadElementValue();
                        return true;
                    }
                case "Portable":
                    {
                        Portable = Utilities.BoolStringToBool(reader.ReadElementValue());
                        return true;
                    }
                case "Comments":
                    {
                        Comments = reader.ReadElementValue();
                        return true;
                    }
                case "Description":
                    {
                        Description = reader.ReadElementValue();
                        return true;
                    }
                case "ServiceCharge":
                    {
                        ServiceCharge = reader.ReadElementValue<decimal>();
                        return true;
                    }
                case "DeliveryType":
                    {
                        DeliveryType = reader.ReadElementValue<int>();
                        return true;
                    }
                case "DeliveryCost":
                    {
                        DeliveryCost = reader.ReadElementValue<decimal>();
                        return true;
                    }
                case "Deleted":
                    {
                        Deleted = Utilities.BoolStringToBool(reader.ReadElementValue());
                        return true;
                    }
            }
            return false;
        }

        private List<InventoryCharges> ReadChargesFromXml(MyVrmServiceXmlReader reader,string endElementName)
        {
            List<InventoryCharges> charges = new List<InventoryCharges>();

            do
            {
                reader.Read();

                InventoryCharges charge = new InventoryCharges();
                switch (reader.LocalName)
                {

                    case "Charge":
                
                        charge = ReadChargeFromXml(reader, "Charge");
                        charges.Add(charge);
                        
                        break;
                    default:
                        reader.SkipCurrentElement();
                        break;

                }


            }
            while (!reader.IsEndElement(XmlNamespace.NotSpecified, endElementName));
            return charges;

        }

        private Data.InventoryCharges ReadChargeFromXml(MyVrmServiceXmlReader reader, string endElementName)
        {
            InventoryCharges charge = new InventoryCharges();

            do
            {
                reader.Read();


                switch (reader.LocalName)
                {
                    case "DeliveryTypeID":
                        charge.DeliveryType = reader.ReadElementValue<int>();
                        break;
                    
                    case "DeliveryCost":
                        charge.DeliveryCost = reader.ReadElementValue<decimal>();
                        break;
                    case "ServiceCharge":
                        charge.ServiceCharge = reader.ReadElementValue<decimal>();
                        break;
                    default:
                        reader.SkipCurrentElement();
                        break;
                }
            }
            while (!reader.IsEndElement(XmlNamespace.NotSpecified, endElementName));
            return charge;
        }
    }
}
