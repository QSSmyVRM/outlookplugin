﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.ObjectModel;
using System.Windows.Forms;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook.WinForms.Conference
{
	public partial class FavoriteRoomSelectionDialog : RoomSelectionDialog
	{
		private const string RoomsViewLayoutFile = "FavoriteRoomSelectionDialog_RoomsViewLayout.xml";

		public FavoriteRoomSelectionDialog()
		{
			InitializeComponent();
			Text = Strings.FavoriteRoomSelectionDialogCaption;
			OkEnabled = true;
			AdditionalWorkForFavoriteRoomSelectionDialog();
			FormClosing += FavoriteRoomSelectionDialog_FormClosing;
			Load += FavoriteRoomSelectionDialog_Load;
		}

		void FavoriteRoomSelectionDialog_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
		{
			SaveRoomsViewLayoutToFile(RoomsViewLayoutFile);
			OnFormClosing();
		}

		void FavoriteRoomSelectionDialog_Load(object sender, EventArgs e)
		{
			var cursor = Cursor.Current;
			Cursor.Current = Cursors.WaitCursor;
			RestoreRoomsViewLayoutWithFileName(RoomsViewLayoutFile);
			OnLoad(cursor);
		}

        public override ReadOnlyCollection<ManagedRoom> GetRooms()
        {
            return MyVrmService.Service.GetActiveManagedRooms();
        }
	}
}
