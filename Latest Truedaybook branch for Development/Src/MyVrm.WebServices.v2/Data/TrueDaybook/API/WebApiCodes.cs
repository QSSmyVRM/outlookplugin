﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.ComponentModel;

namespace CallWebApi.Classes
{
    /// <summary>
    /// Class with codes, returned by Web Api actions
    /// </summary>
    public static class WebApiCodes
    {
        /// <summary>
        /// Enum with codes for Authenticate action in UserController (also use in other actions)
        /// </summary>
        public enum AuthenticateCode
        {
            [Description("Success")]
            Success = 0,
            [Description("Invalid user name or password")]
            InvalidUserOrPassword = 1,
            [Description("User role must be 'Power User' or 'User'")]
            AccessDenied = 2,
            [Description("Unhandled exception")]
            UnhandledException = 3
        }

        /// <summary>
        /// Enum with codes for GetDefaultSettings action in SettingsController
        /// </summary>
        public enum GetDefaultSettingsCode
        {
            [Description("Success")]
            Success = 0,
            [Description("Invalid authentification ticket")]
            InvalidTicket = 1,
            [Description("Unhandled exception")]
            UnhandledException = 2,
            [Description("Authentification ticket is expired")]
            ExpiredTicket = 6
        }

        /// <summary>
        /// Enum with codes for GetMaximumAvailableSeats action in SettingsController
        /// </summary>
        public enum GetMaximumAvailableSeatsCode
        {
            [Description("Success")]
            Success = 0,
            [Description("Invalid authentification ticket")]
            InvalidTicket = 1,
            [Description("Unhandled exception")]
            UnhandledException = 2,
            [Description("Authentification ticket is expired")]
            ExpiredTicket = 6
        }

        /// <summary>
        /// Enum with codes for GetAvailableSeats action in SettingsController
        /// </summary>
        public enum GetAvailableSeatsCode
        {
            [Description("Success")]
            Success = 0,
            [Description("Invalid authentification ticket")]
            InvalidTicket = 1,
            [Description("Unhandled exception")]
            UnhandledException = 2,
            [Description("Start Date must be less, than End Date")]
            InvalidDatesRange = 3,
            [Description("Authentification ticket is expired")]
            ExpiredTicket = 6
        }

        /// <summary>
        /// Enum with codes for PutConference action in ConferenceController
        /// </summary>
        public enum PutConferenceCode
        {
            [Description("Success")]
            Success = 0,
            [Description("Invalid authentification ticket")]
            InvalidTicket = 1,
            [Description("Unhandled exception")]
            UnhandledException = 2,
            [Description("Start Date must be less, than End Date")]
            InvalidDatesRange = 3,
            [Description("Conference with such Id does not exist")]
            ConferenceNotExist = 5,
            [Description("Conference is in incorrect format")]
            IncorrectConference = 4,
            [Description("Authentification ticket is expired")]
            ExpiredTicket = 6
        }

        /// <summary>
        /// Enum with codes for PostConference action in ConferenceController
        /// </summary>
        public enum PostConferenceCode
        {
            [Description("Success")]
            Success = 0,
            [Description("Invalid authentification ticket")]
            InvalidTicket = 1,
            [Description("Unhandled exception")]
            UnhandledException = 2,
            [Description("Start Date must be less, than End Date")]
            InvalidDatesRange = 3,
            [Description("Conference is in incorrect format")]
            IncorrectConference = 4,
            [Description("Authentification ticket is expired")]
            ExpiredTicket = 6
        }

        /// <summary>
        /// Enum with codes for DeleteConference action in ConferenceController
        /// </summary>
        public enum DeleteConferenceCode
        {
            [Description("Success")]
            Success = 0,
            [Description("Invalid authentification ticket")]
            InvalidTicket = 1,
            [Description("Unhandled exception")]
            UnhandledException = 2,
            [Description("Conference with such Id does not exist")]
            ConferenceNotExist = 5,
            [Description("Authentification ticket is expired")]
            ExpiredTicket = 6
        }
    }
}

