﻿namespace MyVrm.WebServices.Data
{
    internal class OrganizationSchema : ServiceObjectSchema
    {
        internal static readonly PropertyDefinition Id;
        internal static readonly PropertyDefinition Name;

        public static readonly OrganizationSchema Instance;

        static OrganizationSchema()
        {
            Id = new ComplexPropertyDefinition<OrganizationId>("OrgId", () => new OrganizationId());
            Name = new StringPropertyDefinition("OrganizationName", PropertyDefinitionFlags.CanSet);

            Instance = new OrganizationSchema();
        }

        internal override void RegisterProperties()
        {
            base.RegisterProperties();
            RegisterProperty(Id);
            RegisterProperty(Name);
        }
    }
}
