﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System.Collections.Generic;

namespace MyVrm.WebServices.Data
{
    public class ParticipantCollection : ComplexPropertyCollection<Participant>
    {
		public override object Clone()
		{
			ParticipantCollection copy = new ParticipantCollection();
			foreach (var participant in Items)
			{
				Participant copyPart = (Participant)participant.Clone();
				copy.Add(copyPart);
			}
			return copy;
		}

        public void Add(Participant participant)
        {
            InternalAdd(participant);
        }

        public void AddRange(IEnumerable<Participant> participants)
        {
            InternalAddRange(participants);
        }

        public void Insert(int index, Participant participant)
        {
            //Items.Insert(index, participant);
			InternalInsert(index, participant);
        }

        public void Remove(Participant participant)
        {
            InternalRemove(participant);
        }

        public void RemoveAt(int index)
        {
            var participant = Items[index];
            Remove(participant);
        }

        public void Clear()
        {
            InternalClear();
        }

        #region Overrides of ComplexPropertyCollection<Participant>

        internal override Participant CreateComplexProperty(string xmlElementName)
        {
            return new Participant();
        }

        internal override string GetCollectionItemXmlElementName(Participant complexProperty)
        {
            return "party";
        }

        #endregion
    }
}
