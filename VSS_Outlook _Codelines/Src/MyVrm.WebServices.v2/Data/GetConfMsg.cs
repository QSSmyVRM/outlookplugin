﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	internal class GetConfMsgRequest : ServiceRequestBase<GetConfMsgResponse>
	{
		public GetConfMsgRequest(MyVrmService service)
			: base(service)
		{
		}

		#region Overrides of ServiceRequestBase

		internal override string GetXmlElementName()
		{
			return "GetConfMsg";
		}

		internal override string GetCommandName()
		{
			return "GetConfMsg";
		}

		internal override string GetResponseXmlElementName()
		{
			return "GetConfMsgList";
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			UserId.WriteToXml(writer, "UserID");
			OrganizationId.WriteToXml(writer);
		}

		#endregion
	}
	//
	public class GetConfMsgResponse : ServiceResponse
	{
		private readonly List<OverlayMessagesDropboxContent> _confMsgs = new List<OverlayMessagesDropboxContent>();

		public ReadOnlyCollection<OverlayMessagesDropboxContent> DropboxContent
		{
			get
			{
				return new ReadOnlyCollection<OverlayMessagesDropboxContent>(_confMsgs);
			}
		}

		internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
		{
			base.ReadElementsFromXml(reader);
			_confMsgs.Clear();
			do
			{
				reader.Read();
				if (reader.IsStartElement(XmlNamespace.NotSpecified, "GetConfMsg"))
				{
					var msg = new OverlayMessagesDropboxContent();
					msg.LoadFromXml(reader, "GetConfMsg");
					_confMsgs.Add(msg);
				}
				else
				{
					reader.SkipCurrentElement();
				}
			} while (!reader.IsEndElement(XmlNamespace.NotSpecified, "GetConfMsgList"));
		}
	}
	public class OverlayMessagesDropboxContent : ComplexProperty
	{
		public enum MsgType
		{
			User = 0,
			System = 1
		}

		public int Id { get; private set; }
		public string Text { get; set; }
		public MsgType Type { get; set; } //Not used

		public override object Clone()
		{
			OverlayMessagesDropboxContent copy = new OverlayMessagesDropboxContent();
			copy.Id = Id;
			copy.Text = string.Copy(Text);
			copy.Type = Type;
			return copy;
		}

		internal override void LoadFromXml(MyVrmServiceXmlReader reader, string xmlElementName)
		{
			do
			{
				reader.Read();
				switch(reader.LocalName)
				{
					case "ConfMsgID":
						{
							Id = reader.ReadElementValue<int>(XmlNamespace.NotSpecified, "ConfMsgID");
							break;
						}
					case "ConfMsg":
						{
							Text = reader.ReadElementValue(XmlNamespace.NotSpecified, "ConfMsg");
							break;
						}
					case "MsgType":
						{
							Type = reader.ReadElementValue<MsgType>(XmlNamespace.NotSpecified, "MsgType");
							break;
						}
					default:
						{
							reader.SkipCurrentElement();
							break;
						}
				}
			} while (!reader.IsEndElement(XmlNamespace.NotSpecified, xmlElementName));
		}
	}
}
