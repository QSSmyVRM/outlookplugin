﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
namespace MyVrm.WebServices.Data
{
    public class BridgeDetails : ComplexProperty
    {
        public string ControlPortIpAddress { get; set; }
        public string PortA { get; set; }
        public string PortB { get; set; }

		public override object Clone()
		{
			BridgeDetails copy = new BridgeDetails();
			copy.ControlPortIpAddress = string.Copy(ControlPortIpAddress);
			copy.PortA = string.Copy(PortA);
			copy.PortB = string.Copy(PortB);

			return copy;
		}

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            base.WriteElementsToXml(writer);
            if (!string.IsNullOrEmpty(ControlPortIpAddress))
                writer.WriteElementValue(XmlNamespace.NotSpecified, "controlPortIPAddress", ControlPortIpAddress);
            if (!string.IsNullOrEmpty(PortA))
                writer.WriteElementValue(XmlNamespace.NotSpecified, "portA", PortA);
            if (!string.IsNullOrEmpty(PortB))
                writer.WriteElementValue(XmlNamespace.NotSpecified, "portB", PortB);
            writer.WriteStartElement(XmlNamespace.NotSpecified, "IPServices");
            writer.WriteEndElement();
            writer.WriteStartElement(XmlNamespace.NotSpecified, "ISDNServices");
            writer.WriteEndElement();
        }
    }
}