﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyVrm.WebServices.Data
{
	public class XMLError: ComplexProperty
	{
		public const int RetSuccess = -1;

		public int ErrorCode { set; get; }
		public string ErrorMessage { set; get; }
		public string ErrorDescription { set; get; }
		public string ErrorLevel { set; get; } 

		public override object Clone()
		{
			XMLError copy = new XMLError();
			copy.ErrorCode = ErrorCode;
			copy.ErrorDescription = ErrorDescription;
			copy.ErrorMessage = ErrorMessage;
			copy.ErrorLevel = ErrorLevel;
			return copy;
		}

		internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
		{
			try
			{
				switch (reader.LocalName)
				{
					case "errorCode":
						{
							ErrorCode = reader.ReadElementValue<int>();
							return true;
						}
					case "message":
						{
							ErrorMessage = reader.ReadElementValue();
							return true;
						}
					case "Description":
						{
							ErrorDescription = reader.ReadElementValue();
							return true;
						}
					case "level":
						{
							ErrorLevel = reader.ReadElementValue();
							return true;
						}
				}
			}
			catch (Exception ex)
			{
				MyVrmService.TraceSource.TraceError(reader.LocalName + ": " + ex.Message, null);
				reader.Read();
				//throw;
			}
			return false;
		}

  //  <error>
  //<errorCode>223</errorCode> 
  //<message>Please enter a valid conference start date / time.</message> 
  //<Description /> 
  //<level>E</level> 
  //</error>
	}
}
