﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace MyVrm.WebServices.Data
{
    internal class FindModifiedRoomsResponse : ServiceResponse
    {
        readonly List<RoomId> _roomIds = new List<RoomId>();

        internal ReadOnlyCollection<RoomId> Rooms
        {
            get
            {
                return new ReadOnlyCollection<RoomId>(_roomIds);
            }
        }

        internal override void ReadElementsFromXml(MyVrmServiceXmlReader reader)
        {
            do
            {
                reader.Read();
                if (reader.IsStartElement(XmlNamespace.NotSpecified, "RoomID"))
                {
                    var roomId = new RoomId();
                    roomId.LoadFromXml(reader, "RoomID");
                    _roomIds.Add(roomId);
                }
                else
                {
                    reader.SkipCurrentElement();
                }
            } while (!reader.IsEndElement(XmlNamespace.NotSpecified, "SearchRooms"));
        }
    }
}
