﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook.WinForms.Options
{
    public partial class UsersDialog : MyVrm.Outlook.WinForms.Dialog
    {

        BindingList<MyVrm.WebServices.Data.UsersDatasource> users = new BindingList<MyVrm.WebServices.Data.UsersDatasource>();
        BindingList<MyVrm.WebServices.Data.UsersDatasource> allTDBUsers = new BindingList<MyVrm.WebServices.Data.UsersDatasource>();
        List<MyVrm.WebServices.Data.UsersDatasource> _selectedUsers = new List<UsersDatasource>();
        BindingList<MyVrm.WebServices.Data.UsersDatasource> outlookUsers = new BindingList<UsersDatasource>();
        BindingList<MyVrm.WebServices.Data.UsersDatasource> allOutlookUsers = new BindingList<MyVrm.WebServices.Data.UsersDatasource>();
        int icont = 10;
        int flag = 0;
        public List<MyVrm.WebServices.Data.UsersDatasource> SelectedUsers
        {
            get { return _selectedUsers; }
            set { _selectedUsers = value; }
        }
        public UsersDialog(BindingList<UsersDatasource> lstUsers)
        {
           try
            {
                InitializeComponent();
                outlookUsers = lstUsers;
                this.Text = Strings.UserDlgText;
                // Make the grid read-only.
                gridView1.OptionsBehavior.Editable = false;
                // Prevent the focused cell from being highlighted.
                gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
                // Draw a dotted focus rectangle around the entire row.
                gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;

                // Make the grid read-only.
                gvOutlookContacts.OptionsBehavior.Editable = false;
                // Prevent the focused cell from being highlighted.
                gvOutlookContacts.OptionsSelection.EnableAppearanceFocusedCell = false;
                // Draw a dotted focus rectangle around the entire row.
                gvOutlookContacts.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            }
            catch (Exception ex)
            {
                MyVrmAddin.TraceSource.TraceException(ex);
                UIHelper.ShowError(ex.Message);
            }
        }

        private void gridControl1_Click(object sender, EventArgs e)
        {

        }

        private void UsersDialog_Load(object sender, EventArgs e)
        {
            try
            {
                this.ApplyVisible = false;
                this.ApplyEnabled = false;
                if (outlookUsers.Count > 10 )
                {
                    flag = 1;
                    ShowMoreButton.Visible = true;
                }
                else
                {
                    flag = 0;
                    ShowMoreButton.Visible = false;
                }
                for (int i = 0; i < 10;i++ )
                {
                    if (flag == 1)
                    {
                        outlookUsers[i].Selected = false;
                        allOutlookUsers.Add(outlookUsers[i]);
                    }
                    else
                    {
                        if (i < outlookUsers.Count)
                        {
                            outlookUsers[i].Selected = false;
                            allOutlookUsers.Add(outlookUsers[i]);
                        }
                        else
                            break;
                    }
                }
                gridControl2.DataSource = allOutlookUsers;
                gridControl1.DataSource = users;

                FindUsersResults userResults = MyVrmService.Service.GetEmailList();
                foreach (ManagedUser mUser in userResults.Users)
                {
                    MyVrm.WebServices.Data.UsersDatasource indUser = new UsersDatasource();
                    indUser.FirstName = mUser.FirstName;
                    indUser.LastName = mUser.LastName;

                    indUser.Email = mUser.Email;
                    indUser.Selected = false;
                    users.Add(indUser);
                    allTDBUsers.Add(indUser);
                }

                for (int i = 0; i <= 2; i++)
                {
                    gridView1.Columns[i].OptionsColumn.ReadOnly = true;
                    gvOutlookContacts.Columns[i].OptionsColumn.ReadOnly = true;
                }
            }
            catch (Exception ex)
            {
                MyVrmAddin.TraceSource.TraceException(ex);
                UIHelper.ShowError(ex.Message);
            }
            
        }

        private void UsersDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                var vselectedUsers = from m in users where m.Selected == true select m;
                _selectedUsers = vselectedUsers.ToList();

                var vSelectedOutlookUsers = from m in outlookUsers where m.Selected == true select m;
                List<UsersDatasource> lstOutlookUsers = vSelectedOutlookUsers.ToList();
                _selectedUsers.AddRange(lstOutlookUsers);
            }
            catch (Exception ex)
            {
                MyVrmAddin.TraceSource.TraceException(ex);
                UIHelper.ShowError(ex.Message); 
            }
        } 

        public void ClearSelectedUsers()
        {
            _selectedUsers.Clear();
            users.Clear();
            
        }

        private void txtSearchTDBContacts_EditValueChanged(object sender, EventArgs e)
        {
            try
            {

                if (txtSearchTDBContacts.Text != null && txtSearchTDBContacts.Text.Trim().Length > 0)
                {
                    string strSearch = txtSearchTDBContacts.Text.ToLower().Trim();

                    var query = from m in allTDBUsers where ((m.Email != null && m.Email.ToLower().Contains(strSearch)) || (m.FirstName != null && m.FirstName.ToLower().Contains(strSearch)) || (m.LastName != null && m.LastName.ToLower().Contains(strSearch))) select m;
                    users = new BindingList<UsersDatasource>(query.ToList());
                    gridControl1.DataSource = users;
                }
                else
                {
                    users = new BindingList<UsersDatasource>(allTDBUsers);
                    gridControl1.DataSource = users;
                }
            }
            catch (Exception ex)
            {
                MyVrmAddin.TraceSource.TraceException(ex);
                UIHelper.ShowError(ex.Message);
            }
        }

        private void txtOCSearch_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtOCSearch.Text != null && txtOCSearch.Text.Trim().Length > 0)
                {
                    string strSearch = txtOCSearch.Text.ToLower().Trim();

                    var query = from m in allOutlookUsers where ((m.Email != null && m.Email.ToLower().Contains(strSearch)) || (m.FirstName != null && m.FirstName.ToLower().Contains(strSearch)) || (m.LastName != null && m.LastName.ToLower().Contains(strSearch))) select m;


                    outlookUsers = new BindingList<UsersDatasource>(query.ToList());
                    gridControl2.DataSource = outlookUsers;
                }
                else
                {
                    outlookUsers = new BindingList<UsersDatasource>(allOutlookUsers);
                    gridControl2.DataSource = outlookUsers;
                }
            }
            catch (Exception ex)
            {
                MyVrmAddin.TraceSource.TraceException(ex);
                UIHelper.ShowError(ex.Message);
            }
        }

        private void txtSearchTDBContacts_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {

        }

        private void gridControl1_DoubleClick(object sender, EventArgs e)
        {

        }

        private void gridControl1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView view = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            if (view != null)
            {
                Point pt = view.GridControl.PointToClient(Control.MousePosition);

                DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo info = view.CalcHitInfo(pt);
                if (info.InRow || info.InRowCell)
                {
                    MyVrm.WebServices.Data.UsersDatasource user = gridView1.GetRow(info.RowHandle) as MyVrm.WebServices.Data.UsersDatasource;
                    if (user != null)
                    {
                        user.Selected = !user.Selected;
                        gridView1.UpdateCurrentRow();
                    }
                }
            }

        }

        private void gvOutlookContacts_DoubleClick(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView view = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            if (view != null)
            {
                Point pt = view.GridControl.PointToClient(Control.MousePosition);

                DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo info = view.CalcHitInfo(pt);
                if (info.InRow || info.InRowCell)
                {
                    MyVrm.WebServices.Data.UsersDatasource user = gvOutlookContacts.GetRow(info.RowHandle) as MyVrm.WebServices.Data.UsersDatasource;
                    if (user != null)
                    {
                        user.Selected = !user.Selected;
                        gvOutlookContacts.UpdateCurrentRow();
                    }
                }
            }

        }

        private void ShowMoreButton_Click(object sender, EventArgs e)
        {
            try
            {
                int fl = 0;
               // icont
                if (flag == 1)
                {
                    if (outlookUsers.Count > icont + 10)
                        fl = 1;
                    for (int i = icont; i < icont + 10; i++)
                    {
                       
                        if (fl== 1)
                        {
                            outlookUsers[i].Selected = false;
                            allOutlookUsers.Add(outlookUsers[i]);
                        }
                        else
                        {
                            if (i < outlookUsers.Count)
                            {
                                outlookUsers[i].Selected = false;
                                allOutlookUsers.Add(outlookUsers[i]);
                            }
                            else
                                break;
                        }
                    }
                    gridControl2.DataSource = allOutlookUsers;
                    icont = icont + 10;
                    if (outlookUsers.Count > icont)
                    {
                        flag = 1;
                        ShowMoreButton.Visible = true;
                    }
                    else
                    {
                        flag = 0;
                        ShowMoreButton.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                UIHelper.ShowError(ex.Message);
                MyVrmAddin.TraceSource.TraceException(ex);
            }
        }
    }
}
