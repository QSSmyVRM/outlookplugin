﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;

namespace MyVrm.WebServices.Data
{
    internal abstract class GetRoomCalendarRequest : ServiceRequestBase<GetRoomCalendarResponse>
    {
        internal GetRoomCalendarRequest(MyVrmService service) : base(service)
        {
        }

        private RoomId _roomId;
        internal RoomId RoomId
        {
            get { return _roomId; }
            set
            {
                if (value == null) throw new ArgumentNullException("value");
                _roomId = value;
            }
        }

        internal DateTime FromDate { get; set; }

        #region Overrides of ServiceRequestBase

        internal override string GetXmlElementName()
        {
            return "calendarView";
        }

        internal override GetRoomCalendarResponse ParseResponse(MyVrmServiceXmlReader reader)
        {
            var response = new GetRoomCalendarResponse();
            response.ResponseXmlElementName = GetResponseXmlElementName();
            response.LoadFromXml(reader, GetResponseXmlElementName());
            return response;
        }

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            UserId.WriteToXml(writer, "userID");
            writer.WriteElementValue(XmlNamespace.NotSpecified, "date", Utilities.DateToString(FromDate));
            RoomId.WriteToXml(writer, "room");
			writer.WriteElementValue(XmlNamespace.NotSpecified, "utcEnabled", Service.IsUtcEnabled);
        }

        #endregion
    }

    internal class GetRoomDaylyCalendarRequest : GetRoomCalendarRequest
    {
        internal GetRoomDaylyCalendarRequest(MyVrmService service) : base(service)
        {
        }

        #region Overrides of ServiceRequestBase

        internal override string GetCommandName()
        {
            return "GetRoomDailyView";
        }

        internal override string GetResponseXmlElementName()
        {
            return "dailyView";
        }

        #endregion
    }

    internal class GetRoomWeeklyCalendarRequest : GetRoomCalendarRequest
    {
        internal GetRoomWeeklyCalendarRequest(MyVrmService service) : base(service)
        {
        }

        #region Overrides of ServiceRequestBase

        internal override string GetCommandName()
        {
            return "GetRoomWeeklyView";
        }

        internal override string GetResponseXmlElementName()
        {
            return "weeklyView";
        }

        #endregion
    }

    internal class GetRoomMonthlyCalendarRequest : GetRoomCalendarRequest
    {
        internal GetRoomMonthlyCalendarRequest(MyVrmService service) : base(service)
        {
        }

        #region Overrides of ServiceRequestBase

        internal override string GetCommandName()
        {
            return "GetRoomMonthlyView";
        }

        internal override string GetResponseXmlElementName()
        {
            return "monthlyView";
        }

        #endregion
    }
}
