﻿namespace MyVrmExcelImportTemplate
{
    partial class ConnectionDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cancelButton = new System.Windows.Forms.Button();
            this.okButton = new System.Windows.Forms.Button();
            this.urlText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.myVrmUserPwdText = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.myVrmUserLoginText = new System.Windows.Forms.TextBox();
            this.myVrmUserRadioButton = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.winUserPwdText = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.winUserNameText = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.diffWindowsUserRadioButton = new System.Windows.Forms.RadioButton();
            this.winUserDomainText = new System.Windows.Forms.TextBox();
            this.curWindowsUserRadioButton = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(311, 281);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 4;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(230, 280);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 3;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // urlText
            // 
            this.urlText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.urlText.Location = new System.Drawing.Point(116, 12);
            this.urlText.Name = "urlText";
            this.urlText.Size = new System.Drawing.Size(270, 20);
            this.urlText.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Web Service URL:";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.myVrmUserPwdText);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.myVrmUserLoginText);
            this.groupBox1.Controls.Add(this.myVrmUserRadioButton);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.winUserPwdText);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.winUserNameText);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.diffWindowsUserRadioButton);
            this.groupBox1.Controls.Add(this.winUserDomainText);
            this.groupBox1.Controls.Add(this.curWindowsUserRadioButton);
            this.groupBox1.Location = new System.Drawing.Point(15, 48);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(371, 226);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Connect As";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 196);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Password:";
            // 
            // myVrmUserPwdText
            // 
            this.myVrmUserPwdText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.myVrmUserPwdText.Location = new System.Drawing.Point(73, 193);
            this.myVrmUserPwdText.Name = "myVrmUserPwdText";
            this.myVrmUserPwdText.PasswordChar = '*';
            this.myVrmUserPwdText.Size = new System.Drawing.Size(292, 20);
            this.myVrmUserPwdText.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 170);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "User:";
            // 
            // myVrmUserLoginText
            // 
            this.myVrmUserLoginText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.myVrmUserLoginText.Location = new System.Drawing.Point(73, 167);
            this.myVrmUserLoginText.Name = "myVrmUserLoginText";
            this.myVrmUserLoginText.Size = new System.Drawing.Size(292, 20);
            this.myVrmUserLoginText.TabIndex = 10;
            // 
            // myVrmUserRadioButton
            // 
            this.myVrmUserRadioButton.AutoSize = true;
            this.myVrmUserRadioButton.Location = new System.Drawing.Point(7, 144);
            this.myVrmUserRadioButton.Name = "myVrmUserRadioButton";
            this.myVrmUserRadioButton.Size = new System.Drawing.Size(87, 17);
            this.myVrmUserRadioButton.TabIndex = 8;
            this.myVrmUserRadioButton.Text = "myVRM User";
            this.myVrmUserRadioButton.UseVisualStyleBackColor = true;
            this.myVrmUserRadioButton.CheckedChanged += new System.EventHandler(this.myVrmUserRadioButton_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 121);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Password:";
            // 
            // winUserPwdText
            // 
            this.winUserPwdText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.winUserPwdText.Enabled = false;
            this.winUserPwdText.Location = new System.Drawing.Point(73, 118);
            this.winUserPwdText.Name = "winUserPwdText";
            this.winUserPwdText.PasswordChar = '*';
            this.winUserPwdText.Size = new System.Drawing.Size(292, 20);
            this.winUserPwdText.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "User:";
            // 
            // winUserNameText
            // 
            this.winUserNameText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.winUserNameText.Enabled = false;
            this.winUserNameText.Location = new System.Drawing.Point(73, 92);
            this.winUserNameText.Name = "winUserNameText";
            this.winUserNameText.Size = new System.Drawing.Size(292, 20);
            this.winUserNameText.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Domain:";
            // 
            // diffWindowsUserRadioButton
            // 
            this.diffWindowsUserRadioButton.AutoSize = true;
            this.diffWindowsUserRadioButton.Location = new System.Drawing.Point(7, 43);
            this.diffWindowsUserRadioButton.Name = "diffWindowsUserRadioButton";
            this.diffWindowsUserRadioButton.Size = new System.Drawing.Size(135, 17);
            this.diffWindowsUserRadioButton.TabIndex = 1;
            this.diffWindowsUserRadioButton.Text = "Different Windows user";
            this.diffWindowsUserRadioButton.UseVisualStyleBackColor = true;
            this.diffWindowsUserRadioButton.CheckedChanged += new System.EventHandler(this.diffWindowsUserRadioButton_CheckedChanged);
            // 
            // winUserDomainText
            // 
            this.winUserDomainText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.winUserDomainText.Enabled = false;
            this.winUserDomainText.Location = new System.Drawing.Point(73, 66);
            this.winUserDomainText.Name = "winUserDomainText";
            this.winUserDomainText.Size = new System.Drawing.Size(292, 20);
            this.winUserDomainText.TabIndex = 3;
            // 
            // curWindowsUserRadioButton
            // 
            this.curWindowsUserRadioButton.AutoSize = true;
            this.curWindowsUserRadioButton.Location = new System.Drawing.Point(7, 20);
            this.curWindowsUserRadioButton.Name = "curWindowsUserRadioButton";
            this.curWindowsUserRadioButton.Size = new System.Drawing.Size(129, 17);
            this.curWindowsUserRadioButton.TabIndex = 0;
            this.curWindowsUserRadioButton.Text = "Current Windows user";
            this.curWindowsUserRadioButton.UseVisualStyleBackColor = true;
            this.curWindowsUserRadioButton.CheckedChanged += new System.EventHandler(this.curWindowsUserRadioButton_CheckedChanged);
            // 
            // ConnectionDialog
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(398, 316);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.urlText);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.cancelButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConnectionDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Connect to Web services";
            this.Load += new System.EventHandler(this.ConnectionDialog_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.TextBox urlText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton curWindowsUserRadioButton;
        private System.Windows.Forms.RadioButton diffWindowsUserRadioButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox winUserPwdText;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox winUserNameText;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox winUserDomainText;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox myVrmUserPwdText;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox myVrmUserLoginText;
        private System.Windows.Forms.RadioButton myVrmUserRadioButton;
    }
}