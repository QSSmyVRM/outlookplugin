﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using MyVrm.Common.Collections;
using MyVrm.WebServices.Data;

namespace MyVrm.Outlook.WinForms.Conference
{
    public partial class InventoryWorkOrderDialog : Dialog
    {
        private class WorkOrderItemRecord
        {
            private readonly WorkOrderItem _workOrderItem;
            private readonly InventorySetItem _inventorySetItem;
            private System.Drawing.Image _itemImage;

            internal WorkOrderItemRecord(WorkOrderItem workOrderItem, InventorySetItem inventorySetItem)
            {
                _workOrderItem = workOrderItem;
                _inventorySetItem = inventorySetItem;
                RequestedQuantity = _workOrderItem.Quantity;
            }

            public int InventoryItemId
            {
                get
                {
                    return _inventorySetItem.Id;
                }
            }
            public int Id
            {
                get
                {
                    return WorkOrderItem.Id;
                }
            }
            public string Name
            {
                get
                {
                    return _inventorySetItem.Name;
                }
            }
            public System.Drawing.Image Image
            {
                get {
                    return _itemImage ??
                           (_itemImage = System.Drawing.Image.FromStream(_inventorySetItem.Image.AsStream()));
                }
            }
            public string Comments
            {
                get
                {
                    return _inventorySetItem.Comments;
                }
            }
            public string Description
            {
                get
                {
                    return _inventorySetItem.Description;
                }
            }
            public uint Quantity
            {
                get
                {
                    return _inventorySetItem.Quantity;
                }
            }

            public uint RequestedQuantity { get; set; }

            internal WorkOrderItem WorkOrderItem
            {
                get { return _workOrderItem; }
            }
        }

        public InventoryWorkOrderDialog()
        {
            WorkOrderItemRecords = new List<WorkOrderItemRecord>();
            InitializeComponent();
        	layoutControlItem1.Text = Strings.RoomLableText;
        	layoutControlItem2.Text = Strings.SetNameLableText;
			layoutControlItem4.Text = Strings.DeliveryTypeLableText;
			layoutControlItem1.CustomizationFormText = Strings.RoomLableText;
			layoutControlItem2.CustomizationFormText = Strings.SetNameLableText;
			layoutControlItem4.CustomizationFormText = Strings.DeliveryTypeLableText;
			roomSetComboBox.Properties.NullText = Strings.SelectOneText;
        	itemNameColumn.Caption = Strings.NameColumnText;
			serialNumberColumn.Caption = Strings.SerialColumnText;
			itemImageColumn.Caption = Strings.ImageColumnText;
			commentsColumn.Caption = Strings.CommentsColumnText;
			descriptionColumn.Caption = Strings.DescriptionColumnText;
			itemQuantityColumn.Caption = Strings.QuantityInHandColumnText;
			requestedQuantityColumn.Caption = Strings.RequestedQuantityColumnText;

        	Text = Strings.WorkOrderLableText;
			ApplyEnabled = false;
			ApplyVisible = false;
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
         Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        public WorkOrder WorkOrder { get; set; }

        private List<WorkOrderItemRecord> WorkOrderItemRecords { get; set; }

        private void AudioVisualWorkOrderDialog_Load(object sender, EventArgs e)
        {
            var cursor = Cursor;
            Cursor = Cursors.WaitCursor;
            try
            {
                Text = string.Format("{0} - {1}", Text, WorkOrder.Name);
                deliveryTypeComboBox.Properties.Items.AddRange(
                    new EnumListSource {EnumType = typeof (WorkOrderDeliveryType)}.GetList());
                deliveryTypeComboBox.EditValue = WorkOrder.DeliveryType;
                roomNameLabel.Text = MyVrmAddin.Instance.GetRoomFromCache(WorkOrder.RoomId).Name;
                var roomSets =MyVrmService.Service.GetRoomSets(WorkOrder.RoomId, RoomSetType.Inventory);
                roomSetComboBox.Properties.Items.BeginUpdate();
                try
                {
                    foreach (var roomSet in roomSets)
                    {
                        roomSetComboBox.Properties.Items.Add(roomSet);
                    }
                    var workOrderRoomSet = roomSets.FirstOrDefault(s => s.Id == WorkOrder.SetId);
                    roomSetComboBox.SelectedItem = workOrderRoomSet;
                }
                finally
                {
                    roomSetComboBox.Properties.Items.EndUpdate();
                }
            }
            finally
            {
                Cursor = cursor;
            }
        }

        private void roomSetComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var cursor = Cursor;
            Cursor = Cursors.WaitCursor;
            try
            {
                var roomSet = roomSetComboBox.SelectedItem as RoomSet;
                if (roomSet != null)
                {
                    var inventorySet = MyVrmService.Service.GetInventoryDetails(roomSet.Id, RoomSetType.Inventory);
                    var query = from inventoryItem in inventorySet.Items
                                join workOrderItem in WorkOrder.Items on inventoryItem.Id equals workOrderItem.Id into
                                    woItems
                                from woItem in woItems.DefaultIfEmpty(new WorkOrderItem {Id = inventoryItem.Id})
                                select
                                    new WorkOrderItemRecord(woItem, inventoryItem);
                    WorkOrderItemRecords = query.ToList();
                    workOrderItemsList.DataSource = WorkOrderItemRecords;
                }
            }
            finally
            {
                Cursor = cursor;
            }
        }

        private void quantityRepositoryItemSpinEdit_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            //var newValue = Convert.ToInt32(e.NewValue);
			int newValue = 0;
			Int32.TryParse(e.NewValue.ToString(), out newValue);
        	int iQuantityColumn = 0;
			Int32.TryParse(workOrderItemsList.FocusedNode[itemQuantityColumn].ToString(), out iQuantityColumn);
			if (newValue < 0 || newValue > iQuantityColumn/*Convert.ToInt32(workOrderItemsList.FocusedNode[itemQuantityColumn])*/)
            {
                e.Cancel = true;
            }
        }

        private void AudioVisualWorkOrderDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult != DialogResult.OK) return;
            WorkOrderItemRecords.ForEach(rec => rec.WorkOrderItem.Quantity = rec.RequestedQuantity);
            var workOrderItems = from itemRecord in WorkOrderItemRecords
                                 where itemRecord.RequestedQuantity > 0
                                 select itemRecord.WorkOrderItem;
            if (roomSetComboBox.EditValue == null)
            {
                ShowMessage(Strings.SelectAudioVideoInventorySet);
                e.Cancel = true;
                return;
            }
            if (deliveryTypeComboBox.EditValue == null)
            {
                ShowMessage(Strings.SelectAudioVideoWorkOrderDeliveryType);
                e.Cancel = true;
                return;
            }
            if (workOrderItems.Count() == 0)
            {
                ShowMessage(Strings.SpecifyWorkOrderItemRequestQuantity);
                e.Cancel = true;
                return;
            }
            WorkOrder.SetId = ((RoomSet) roomSetComboBox.SelectedItem).Id;
            WorkOrder.DeliveryType = (WorkOrderDeliveryType)deliveryTypeComboBox.EditValue;
            WorkOrder.Items.Clear();
            WorkOrder.Items.AddRange(workOrderItems);
        }
    }
}
