﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyVrm.WebServices.Data;

namespace MyVrm.WebServices.Data
{
	public class WhyGoManagedRoom : ComplexProperty//ServiceObject
	{
		public enum PersonType
		{
			SiteCordinator,
			Manager,
			TechnicalContact
		}
		public static string GetPerson(PersonType personType)
		{
			switch (personType)
			{
				case PersonType.SiteCordinator:
					return "SiteCordinator";
				case PersonType.Manager:
					return "Manager";
				case PersonType.TechnicalContact:
					return "TechnicalContact";
			}
			return string.Empty;
		}

		public Room _room;

		//<RoomID>26</RoomID>
      //<RoomName>This is whygo ivate</RoomName>
      //Capacity=<MaximumCapacity>100</MaximumCapacity>
      //Media = <Video>2</Video>
      //<City></City>
	/*	public int State;//<State>34</State>
      //<ZipCode></ZipCode>
      //CountryId=<Country>225</Country>
		public string Address; //<Address>This is address</Address>
      //TimeZone =<TimezoneID>26</TimezoneID>
      //<Longitude></Longitude>
      //<Latitude></Latitude>
		public string DefaultEquipment;//String!! List<int>????????? <DefaultEquipmentID></DefaultEquipmentID>
		public List<int> CateringOptions;//<CateringOptions>1,2,3</CateringOptions>
		public List<int> AUXEquipment;//<AUXEquipment>1,2,3</AUXEquipment>
		public int Layout; //<Layout>1</Layout>
		public ResponsiblePerson SiteCordinator = new ResponsiblePerson(GetPerson(PersonType.SiteCordinator));
	  //<SiteCordinator>
	  //  <Name>test</Name>
	  //  <Email>test@test</Email>
	  //  <Phone>123</Phone>
	  //</SiteCordinator>
		public ResponsiblePerson Manager = new ResponsiblePerson(GetPerson(PersonType.Manager));
	  //<Manager>
	  //  <Name>tes</Name>
	  //  <Email></Email>
	  //  <Phone>5623</Phone>
	  //</Manager>
		public ResponsiblePerson TechnicalContact = new ResponsiblePerson(GetPerson(PersonType.TechnicalContact));
	  //<TechnicalContact>
	  //  <Name></Name>
	  //  <Email></Email>
	  //  <Phone>25698</Phone>
	  //</TechnicalContact>
		
      //<IP>
		public string ipSpeed; //<Speed>768</Speed>
		public string ipAddress; //<Address>127.0.0.1</Address>
      //</IP>
      //<ISDN>
		public string isdnSpeed; //<Speed>768</Speed>
		public string isdnAddress; //<Address>300054563258</Address>
      //</ISDN>
		public string Description;//>Test room 1</Description>
		public string ExtraNotes; //<ExtraNotes>This room is jsut a test 1</ExtraNotes>
		public string Type;// <Type>Video COnfernece or Telepresnce</Type>
      //<RoomImageName></RoomImageName>
		*/
		private string ListToString(List<int> list)
		{
			string ret = string.Empty;
			
			foreach (var entry in list)
			{
				if (ret.Length > 0)
					ret += ",";
				ret += entry.ToString().Trim();
			}
			return ret;
		}

		internal override void WriteElementsToXml(MyVrmXmlWriter writer)
		{
			_room.Id.WriteToXml(writer); //<RoomID>26</RoomID>
			writer.WriteElementValue(Namespace, "RoomName", _room.Name); //<RoomName>This is whygo ivate</RoomName>
			writer.WriteElementValue(Namespace, "MaximumCapacity", _room.Capacity);
				//Capacity=<MaximumCapacity>100</MaximumCapacity>
			writer.WriteElementValue(Namespace, "Video", _room.Media); ////Media = <Video>2</Video>
			writer.WriteElementValue(Namespace, "City", _room.City); ////<City></City>
			writer.WriteElementValue(Namespace, "State", _room.PublicRoomData.State); ////<State>34</State>
			writer.WriteElementValue(Namespace, "ZipCode", _room.ZipCode); ////<ZipCode></ZipCode>
			writer.WriteElementValue(Namespace, "Country", _room.CountryId); ////CountryId=<Country>225</Country>
			writer.WriteElementValue(Namespace, "Address", _room.PublicRoomData.Address);
				//public string Address; //<Address>This is address</Address>
			writer.WriteElementValue(Namespace, "TimezoneID", TimeZoneConvertion.ConvertToTimeZoneId(_room.TimeZone)); ////TimeZone =<TimezoneID>26</TimezoneID>
			writer.WriteElementValue(Namespace, "Longitude", _room.Longitude); ////<Longitude></Longitude>
			writer.WriteElementValue(Namespace, "Latitude", _room.Latitude); ////<Latitude></Latitude>
			writer.WriteElementValue(Namespace, "DefaultEquipment", _room.PublicRoomData.DefaultEquipment);
				//public int DefaultEquipmentID;// List<int>????????? <DefaultEquipmentID></DefaultEquipmentID>
			writer.WriteElementValue(Namespace, "CateringOptions", _room.PublicRoomData.CateringOptions);
				//public List<int> CateringOptions;//<CateringOptions>1,2,3</CateringOptions>
			writer.WriteElementValue(Namespace, "AUXEquipment", _room.PublicRoomData.AUXEquipment); //public List<int> AUXEquipment;//<AUXEquipment>1,2,3</AUXEquipment>
			writer.WriteElementValue(Namespace, "Layout", _room.PublicRoomData.Layout); //public int Layout; //<Layout>1</Layout>
			_room.PublicRoomData.SiteCordinator.WriteElementsToXml(writer);
			_room.PublicRoomData.Manager.WriteElementsToXml(writer);
			_room.PublicRoomData.TechnicalContact.WriteElementsToXml(writer);

			//<IP>
			writer.WriteStartElement(Namespace, "IP");
			writer.WriteElementValue(Namespace, "Speed", _room.PublicRoomData.ipSpeed); //public string ipSpeed; //<Speed>768</Speed>
			writer.WriteElementValue(Namespace, "Address", _room.PublicRoomData.ipAddress); //public string ipSpeed; //<Address>127.0.0.1</Address>
			writer.WriteEndElement();
			//</IP>
			//<ISDN>
			writer.WriteStartElement(Namespace, "ISDN");
			writer.WriteElementValue(Namespace, "Speed", _room.PublicRoomData.isdnSpeed); //	public string isdnSpeed; //<Speed>768</Speed>
			writer.WriteElementValue(Namespace, "Address", _room.PublicRoomData.isdnAddress); //public string isdnAddress; //<Address>300054563258</Address>
			writer.WriteEndElement();
			//</ISDN>
			writer.WriteElementValue(Namespace, "Description", _room.PublicRoomData.Description);
				//public string Description;//>Test room 1</Description>
			writer.WriteElementValue(Namespace, "ExtraNotes", _room.PublicRoomData.ExtraNotes);
				//public string ExtraNotes; //<ExtraNotes>This room is jsut a test 1</ExtraNotes>
			writer.WriteElementValue(Namespace, "Type", _room.PublicRoomData.Type);
				//public string Type;// <Type>Video COnfernece or Telepresnce</Type>
			writer.WriteElementValue(Namespace, "RoomImageName", _room.PublicRoomData.RoomImageName); ////<RoomImageName></RoomImageName>
			writer.WriteStartElement(Namespace, "RoomImages");
			writer.WriteStartElement(Namespace, "Image");
			writer.WriteElementValue(Namespace, "ActualImage", _room.PublicRoomData.RoomImages);
			writer.WriteEndElement();
			writer.WriteEndElement();
		}

		public override object Clone()
		{
			WhyGoManagedRoom dest = new WhyGoManagedRoom();
			dest._room = (Room)_room.Clone();
			//dest.DefaultEquipment = DefaultEquipment;
			//dest.CateringOptions = CateringOptions;
			//dest.AUXEquipment = AUXEquipment;
			//dest.Layout = Layout;
			//dest.SiteCordinator = (ResponsiblePerson)SiteCordinator.Clone();
			//dest.Manager = (ResponsiblePerson)Manager.Clone();
			//dest.TechnicalContact = (ResponsiblePerson)TechnicalContact.Clone();
			//dest.ipSpeed = ipSpeed;
			//dest.ipAddress = ipAddress;
			//dest.isdnSpeed = isdnSpeed;
			//dest.isdnAddress = isdnAddress;
			//dest.Description = Description;
			//dest.ExtraNotes = ExtraNotes;
			//dest.Type = Type;

			return dest;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(this, obj))
				return true;

			WhyGoManagedRoom managedRoom = obj as WhyGoManagedRoom;
			if (managedRoom == null)
				return false;
			if (!managedRoom._room.Equals(_room))
			    return false;
			//if( managedRoom.DefaultEquipment != DefaultEquipment)
			//    return false;
			//if( managedRoom.CateringOptions != CateringOptions)
			//    return false;
			//if( managedRoom.AUXEquipment != AUXEquipment)
			//    return false;
			//if( managedRoom.Layout != Layout)
			//    return false;
			//if( !managedRoom.SiteCordinator.Equals(SiteCordinator))
			//    return false;
			//if( managedRoom.Manager.Equals(Manager))
			//    return false;
			//if( managedRoom.TechnicalContact.Equals(TechnicalContact))
			//    return false;
			//if( managedRoom.ipSpeed != ipSpeed)
			//    return false;
			//if( managedRoom.ipAddress != ipAddress)
			//    return false;
			//if( managedRoom.isdnSpeed != isdnSpeed)
			//    return false;
			//if( managedRoom.isdnAddress != isdnAddress)
			//    return false;
			//if( managedRoom.Description != Description)
			//    return false;
			//if( managedRoom.ExtraNotes != ExtraNotes)
			//    return false;
			//if( managedRoom.Type != Type)
			//    return false;

			return true;
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
	}
}
