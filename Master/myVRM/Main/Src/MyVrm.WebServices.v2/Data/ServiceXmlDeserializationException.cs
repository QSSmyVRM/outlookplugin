﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Runtime.Serialization;

namespace MyVrm.WebServices.Data
{
    /// <summary>
    /// Represents an error that occurs when the XML for a response cannot be deserialized.
    /// </summary>
    [Serializable]
    public class ServiceXmlDeserializationException : MyVrmServiceLocalException
    {
        public ServiceXmlDeserializationException()
        {
        }

        public ServiceXmlDeserializationException(string message) : base(message)
        {
        }

        public ServiceXmlDeserializationException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public ServiceXmlDeserializationException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
