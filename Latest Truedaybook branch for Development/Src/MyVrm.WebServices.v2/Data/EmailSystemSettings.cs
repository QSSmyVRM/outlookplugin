﻿/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

namespace MyVrm.WebServices.Data
{
    public sealed class EmailSystemSettings : ComplexProperty
    {
        public string CompanyEmail { get; set; }
        public string DisplayName { get; set; }
        public string MessageTemplate { get; set; }
        public bool IsRemoteServer { get; set; }
        public string ServerAddress { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public int Port { get; set; }
        public int Timeout { get; set; }

		public override object Clone()
		{
			EmailSystemSettings copy = new EmailSystemSettings();
			copy.IsRemoteServer = IsRemoteServer;
			copy.Port = Port;
			copy.Timeout = Timeout;
			copy.CompanyEmail = string.Copy(CompanyEmail);
			copy.DisplayName = string.Copy(DisplayName);
			copy.MessageTemplate = string.Copy(MessageTemplate);
			copy.ServerAddress = string.Copy(ServerAddress);
			copy.Login = string.Copy(Login);
			copy.Password = string.Copy(Password);

			return copy;
		}

        internal override void WriteElementsToXml(MyVrmXmlWriter writer)
        {
            writer.WriteElementValue(XmlNamespace.NotSpecified, "companyEmail", CompanyEmail);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "displayName", DisplayName);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "messageTemplate", MessageTemplate);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "remoteServer", IsRemoteServer);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "serverAddress", ServerAddress);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "accountLogin", Login);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "accountPwd", Password);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "portNo", Port);
            writer.WriteElementValue(XmlNamespace.NotSpecified, "connectionTimeout", Timeout);
        }

        internal override bool TryReadElementFromXml(MyVrmServiceXmlReader reader)
        {
            switch(reader.LocalName)
            {
                case "companyEmail":
                {
                    CompanyEmail = reader.ReadElementValue();
                    return true;
                }
                case "displayName":
                {
                    DisplayName = reader.ReadElementValue();
                    return true;
                }
                case "messageTemplate":
                {
                    MessageTemplate = reader.ReadElementValue();
                    return true;
                }
                case "remoteServer":
                {
                    IsRemoteServer = Utilities.BoolStringToBool(reader.ReadElementValue());
                    return true;
                }
                case "serverAddress":
                {
                    ServerAddress = reader.ReadElementValue();
                    return true;
                }
                case "accountLogin":
                {
                    Login = reader.ReadElementValue();
                    return true;
                }
                case "accountPwd":
                {
                    Password = reader.ReadElementValue();
                    return true;
                }
                case "portNo":
                {
                    Port = reader.ReadElementValue<int>();
                    return true;
                }
                case "connectionTimeout":
                {
                    Timeout = reader.ReadElementValue<int>();
                    return true;
                }
            }
            return false;
        }
    }
}
